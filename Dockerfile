# Check out https://hub.docker.com/_/node to select a new base image
FROM node:12.18.4-slim
USER node
RUN mkdir -p /home/node/app
WORKDIR /home/node/app
COPY --chown=node package*.json ./
RUN npm install

# Bundle app source code
COPY --chown=node . .

RUN npm run build

# Bind to all network interfaces so that it can be mapped to the host OS
ENV HOST=0.0.0.0 PORT=3002

EXPOSE ${PORT}
CMD [ "node", "." ]
