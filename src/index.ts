import { /*ApplicationConfig,*/ MasterApplication} from './application';
// export * from './application';
// import {ApplicationConfig, ExpressServer} from './server';
// export {ApplicationConfig, ExpressServer, MasterApplication};
import {ApplicationConfig, ExpressServer} from './server';
export {ApplicationConfig, ExpressServer, MasterApplication};
require('dotenv').config();

export async function main(options: ApplicationConfig = {}) {
  // const app = new MasterApplication(options);
  const app = new ExpressServer(options);

  await app.boot();
  await app.start();

  console.log('VERSION 1.0.0 : Server is running at https://127.0.0.1:' + process.env.APP_PORT_APP + "/" + process.env.API_BASE_PATH_APP);

  // const url = app.restServer.url;
  // console.log(`Server is running at ${url}`);
  // console.log(`Try ${url}/ping`);

  return app;
}

if (require.main === module) {
  // Run the application
  const config = {
    rest: {
      // port: +(process.env.PORT ?? 3000),
      port: process.env.APP_PORT_APP,
      host: process.env.HOST,
      basePath: process.env.API_BASE_PATH_APP,
      gracePeriodForClose: 5000, // 5 seconds
      openApiSpec: {
        setServersFromRequest: true,
      },
      listenOnStart: false,
      protocol: 'http',
    },
  };
  main(config).catch(err => {
    console.error('Cannot start the application.', err);
    process.exit(1);
  });
}
