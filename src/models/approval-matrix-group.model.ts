import {Entity, model, property} from '@loopback/repository';

@model()
export class ApprovalMatrixGroup extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  amg_id?: number;

  @property({
    type: 'string',
    required: true,
  })
  amg_label: string;


  constructor(data?: Partial<ApprovalMatrixGroup>) {
    super(data);
  }
}

export interface ApprovalMatrixGroupRelations {
  // describe navigational properties here
}

export type ApprovalMatrixGroupWithRelations = ApprovalMatrixGroup & ApprovalMatrixGroupRelations;
