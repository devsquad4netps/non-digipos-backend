import {Entity, model, property} from '@loopback/repository';

@model({
  name: 'MERCHANT_PRODUCT_SUMMARY'
})
export class MerchantProductSummary extends Entity {

  @property({
    type: 'string',
    id: true,
    generated: false,
    defaultFn: 'uuidv4',
  })
  SUMMARY_ID?: string;

  @property({
    type: 'string',
    required: true,
    mysql: {
      dataType: 'varchar',
      dataLength: 10,
    }
  })
  PERIODE: string;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  LINKAJA_COUNT_TRX: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  LINKAJA_FEE: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  LINKAJA_AMOUNT: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  LINKAJA_MDR: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  LINKAJA_DPP: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  LINKAJA_PPN: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  LINKAJA_PPH: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  LINKAJA_TOTAL: number;


  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  APP_COUNT_TRX: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  APP_FEE: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  APP_AMOUNT: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  APP_MDR: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  APP_DPP: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  APP_PPN: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  APP_PPH: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  APP_TOTAL: number;

  @property({
    type: 'number',
    default: 0
  })
  HAS_SELISIH: number;

  @property({
    type: 'string',
    required: true,
    mysql: {
      dataType: 'text',
    }
  })
  START_TIME_RUN: string;

  @property({
    type: 'number',
    required: true,
  })
  M_ID: number;

  @property({
    type: 'number',
    required: true,
  })
  MP_ID: number;

  @property({
    type: 'number',
    required: true,
  })
  MPG_ID: number;

  @property({
    type: 'string',
  })
  MPM_ID: string;

  @property({
    type: 'string',
  })
  MPM_SELISIH_ID: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'text',
    }
  })
  MPS_LABEL: string;

  @property({
    type: 'number',
  })
  TYPE_DATA?: number;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 35,
    }
  })
  AT_CREATE?: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 35,
    }
  })
  AT_UPDATE?: string;

  @property({
    type: 'number',
    default: 1,
  })
  AT_FLAG?: number;

  @property({
    type: 'string',
  })
  AT_WHO?: string;


  constructor(data?: Partial<MerchantProductSummary>) {
    super(data);
  }
}

export interface MerchantProductSummaryRelations {
  // describe navigational properties here
}

export type MerchantProductSummaryWithRelations = MerchantProductSummary & MerchantProductSummaryRelations;
