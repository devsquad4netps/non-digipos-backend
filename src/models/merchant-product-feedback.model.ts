import {Entity, model, property} from '@loopback/repository';

@model({
  name: 'MERCHANT_PRODUCT_FEEDBACK'
})
export class MerchantProductFeedback extends Entity {

  @property({
    type: 'string',
    id: true,
    generated: false,
    defaultFn: 'uuidv4'
  })
  MPF_ID?: string;

  @property({
    type: 'string',
    required: true,
    mysql: {
      dataType: 'TEXT',
    }
  })
  MPF_MESSAGE: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'TEXT',
    }
  })
  FILE_BAR: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'TEXT',
    }
  })
  FILE_SUMMARY: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'TEXT',
    }
  })
  FILE_INVOICE: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'TEXT',
    }
  })
  FILE_BUKPOT: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'TEXT',
    }
  })
  FILE_FAKTUR: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'TEXT',
    }
  })
  FILE_BUKTIBAYAR: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'TEXT',
    }
  })
  FILE_OTHERS: string;

  @property({
    type: 'number',
    default: 0
  })
  REQ_FILE_BAR: number;

  @property({
    type: 'number',
    default: 0
  })
  REQ_FILE_SUMMARY: number;

  @property({
    type: 'number',
    default: 0
  })
  REQ_FILE_INVOICE: number;

  @property({
    type: 'number',
    default: 0
  })
  REQ_FILE_BUKPOT: number;

  @property({
    type: 'number',
    default: 0
  })
  REQ_FILE_FAKTUR: number;

  @property({
    type: 'number',
    default: 0
  })
  REQ_FILE_BUKTI_BAYAR: number;

  @property({
    type: 'number',
    default: 1
  })
  TYPE_FEEDBACK: number;

  @property({
    type: 'string',
    required: true,
    mysql: {
      dataType: 'varchar',
      dataLength: 150,
    }
  })
  TOKEN_KEY: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'TEXT',
    }
  })
  SEND_TO: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'TEXT',
    }
  })
  SEND_CC: string;

  @property({
    type: 'number',
    default: 1
  })
  SEND_FLAG: number;

  @property({
    type: 'string',
    mysql: {
      dataType: 'TEXT',
    }
  })
  SEND_MESSAGE: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 35,
    }
  })
  AT_SEND?: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 512,
    }
  })
  MPM_ID?: string;

  @property({
    type: 'string',
    required: true,
    mysql: {
      dataType: 'text',
    }
  })
  START_TIME_RUN: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 35,
    }
  })
  AT_CREATE?: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 35,
    }
  })
  AT_UPDATE?: string;

  @property({
    type: 'number',
    default: 1,
  })
  AT_FLAG?: number;

  @property({
    type: 'string',
  })
  AT_WHO?: string;


  constructor(data?: Partial<MerchantProductFeedback>) {
    super(data);
  }
}

export interface MerchantProductFeedbackRelations {
  // describe navigational properties here
}

export type MerchantProductFeedbackWithRelations = MerchantProductFeedback & MerchantProductFeedbackRelations;
