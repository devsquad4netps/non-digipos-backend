import {Entity, model, property} from '@loopback/repository';

@model({
  name: 'MERCHANT'
})
export class Merchant extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  M_ID?: number;

  @property({
    type: 'string',
    required: true,
    mysql: {
      dataType: 'varchar',
      dataLength: 20,
    },
    index: {
      unique: true
    }
  })
  M_CODE: string;

  @property({
    type: 'string',
    required: true,
    mysql: {
      dataType: 'varchar',
      dataLength: 512,
    },
    index: {
      unique: true
    }
  })
  M_NAME: string;

  @property({
    type: 'string',
    required: true,
    mysql: {
      dataType: 'varchar',
      dataLength: 512,
    }
  })
  M_LEGAL_NAME: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 512,
    }
  })
  M_PIC_NAME: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 512,
    }
  })
  M_PIC_EMAIL?: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 35,
    }
  })
  M_PIC_PHONE?: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 250,
    }
  })
  M_PIC_JABATAN?: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 250,
    }
  })
  PT_REKENING_NAME?: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 250,
    }
  })
  PT_ACOUNT_NUMBER?: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 250,
    }
  })
  PT_BANK_NAME?: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 250,
    }
  })
  PT_BANK_BRANCH?: string;


  @property({
    type: 'string',
    required: true,
    mysql: {
      dataType: 'TEXT',
    }
  })
  M_ALAMAT_KANTOR: string;

  @property({
    type: 'string',
    required: true,
    mysql: {
      dataType: 'varchar',
      dataLength: 3,
    }
  })
  M_NO_PERFIX?: string;

  @property({
    type: 'number',
    required: true,
  })
  M_CLASS: number;

  @property({
    type: 'string',
    required: true,
    mysql: {
      dataType: 'varchar',
      dataLength: 50,
    },
    index: {
      unique: true
    }
  })
  M_NPWP: string;

  @property({
    type: 'string',
    required: true,
    mysql: {
      dataType: 'TEXT',
    }
  })
  M_ALAMAT_NPWP?: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'TEXT',
    }
  })
  M_NPWP_ATTACH?: string;

  @property({
    type: 'string',
    required: true,
    mysql: {
      dataType: 'varchar',
      dataLength: 150,
    },
    index: {
      unique: true
    }
  })
  M_NO_CONTRACT: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'TEXT',
    }
  })
  M_PKS_ATTACH?: string;

  @property({
    type: 'date',
  })
  M_TGL_GABUNG?: Date;

  @property({
    type: 'string',
    required: true,
    mysql: {
      dataType: 'varchar',
      dataLength: 150,
    }
  })
  M_CITY?: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 35,
    }
  })
  AT_CREATE?: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 35,
    }
  })
  AT_UPDATE?: string;

  @property({
    type: 'number',
    default: 1,
  })
  AT_FLAG?: number;

  @property({
    type: 'string',
  })
  AT_WHO?: string;

  constructor(data?: Partial<Merchant>) {
    super(data);
  }
}

export interface MerchantRelations {
  // describe navigational properties here
}

export type MerchantWithRelations = Merchant & MerchantRelations;
