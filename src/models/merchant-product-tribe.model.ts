import {Entity, model, property} from '@loopback/repository';

@model({
  name: 'MERCHANT_PRODUCT_TRIBE'
})
export class MerchantProductTribe extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  MPT_ID?: number;

  @property({
    type: 'string',
    required: true,
    mysql: {
      dataType: 'varchar',
      dataLength: 15,
    }
  })
  MPT_CODE: string;

  @property({
    type: 'string',
    required: true,
    mysql: {
      dataType: 'varchar',
      dataLength: 250,
    }
  })
  MPT_LABEL: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 35,
    }
  })
  AT_CREATE?: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 35,
    }
  })
  AT_UPDATE?: string;

  @property({
    type: 'number',
    default: 1,
  })
  AT_FLAG?: number;

  @property({
    type: 'string',
  })
  AT_WHO?: string;


  constructor(data?: Partial<MerchantProductTribe>) {
    super(data);
  }
}

export interface MerchantProductTribeRelations {
  // describe navigational properties here
}

export type MerchantProductTribeWithRelations = MerchantProductTribe & MerchantProductTribeRelations;
