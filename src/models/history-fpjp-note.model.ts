import {Entity, model, property} from '@loopback/repository';

@model()
export class HistoryFpjpNote extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'number',
    required: true,
  })
  fpjp_id: number;

  @property({
    type: 'number',
  })
  reason_id?: number;

  @property({
    type: 'string',
    required: true,
  })
  note: string;

  @property({
    type: 'string',
    required: true,
  })
  at_create: string;

  @property({
    type: 'string',
    required: true,
  })
  by_create: string;

  @property({
    type: 'number',
    default: 0,
  })
  state?: number;



  constructor(data?: Partial<HistoryFpjpNote>) {
    super(data);
  }
}

export interface HistoryFpjpNoteRelations {
  // describe navigational properties here
}

export type HistoryFpjpNoteWithRelations = HistoryFpjpNote & HistoryFpjpNoteRelations;
