import {Entity, model, property} from '@loopback/repository';

@model()
export class DownloadGoogleDrive extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  gd_id?: number;

  @property({
    type: 'string',
    required: true,
  })
  gd_file_name: string;

  @property({
    type: 'string',
    required: true,
  })
  gd_file_code: string;

  @property({
    type: 'number',
    required: true,
  })
  gd_file_group: number;

  @property({
    type: 'number',
    default: 0,
  })
  gd_file_state?: number;

  @property({
    type: 'number',
    default: 0,
  })
  gd_file_run?: number;

  @property({
    type: 'string',
    required: true,
  })
  ad_code: string;

  @property({
    type: 'string'
  })
  gd_date: string;

  @property({
    type: 'string'
  })
  nomor_file: string;

  @property({
    type: 'string'
  })
  nomor_bast: string;

  @property({
    type: 'string'
  })
  nomor_perjanjian: string;

  constructor(data?: Partial<DownloadGoogleDrive>) {
    super(data);
  }
}

export interface DownloadGoogleDriveRelations {
  // describe navigational properties here
}

export type DownloadGoogleDriveWithRelations = DownloadGoogleDrive & DownloadGoogleDriveRelations;
