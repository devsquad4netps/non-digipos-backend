import {Entity, model, property} from '@loopback/repository';

@model({
  name: 'TRANSAKSI_TYPE'
})
export class TransaksiType extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  TT_ID?: number;

  @property({
    type: 'string',
    required: true,
  })
  TT_LABEL: string;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  TT_BOBOT_A: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  TT_BOBOT_DPP_A: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  TT_BOBOT_PPN_A: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  TT_BOBOT_PPH23_A: number;

  @property({
    type: 'number',
    default: 0,
  })
  TT_AD_A_HAS?: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  TT_BOBOT_B: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  TT_BOBOT_DPP_B: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  TT_BOBOT_PPN_B: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  TT_BOBOT_PPH23_B: number;

  @property({
    type: 'number',
    default: 0,
  })
  TT_AD_B_HAS?: number;

  @property({
    type: 'string',
  })
  TT_TABLE?: string;

  @property({
    type: 'string',
  })
  TT_TABLE_KEY: string;

  @property({
    type: 'string',
  })
  TT_TABLE_DATE: string;

  @property({
    type: 'string',
  })
  TT_SALES_A?: string;

  @property({
    type: 'string',
  })
  TT_COUNT_TRX_A?: string;

  @property({
    type: 'string',
  })
  TT_SALES_B?: string;

  @property({
    type: 'string',
  })
  TT_COUNT_TRX_B?: string;


  @property({
    type: 'string',
  })
  TT_TABLE_FILTER?: string;

  @property({
    type: 'string',
  })
  TT_TABLE_FILTER_HAVING?: string;

  @property({
    type: 'number',
    default: 1,
  })
  TT_FLAG?: number;

  @property({
    type: 'string',
  })
  AT_CREATE?: string;

  @property({
    type: 'string',
  })
  AT_UPDATE?: string;

  @property({
    type: 'string',
  })
  AT_USER?: string;

  constructor(data?: Partial<TransaksiType>) {
    super(data);
  }
}

export interface TransaksiTypeRelations {
  // describe navigational properties here
}

export type TransaksiTypeWithRelations = TransaksiType & TransaksiTypeRelations;
