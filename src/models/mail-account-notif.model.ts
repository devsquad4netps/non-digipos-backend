import {Entity, model, property} from '@loopback/repository';


@model({
  name: 'MailAccountNotif'
})
export class MailAccountNotif extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  MAN_ID?: number;

  @property({
    type: 'string',
  })
  MAN_GROUP?: string;

  @property({
    type: 'string',
  })
  MAN_ACCOUNT?: string;

  @property({
    type: 'number',
    default: 0,
  })
  MAN_FLAG?: number;


  constructor(data?: Partial<MailAccountNotif>) {
    super(data);
  }
}

export interface MailAccountNotifRelations {
  // describe navigational properties here
}

export type MailAccountNotifWithRelations = MailAccountNotif & MailAccountNotifRelations;
