import {Entity, model, property} from '@loopback/repository';

@model()
export class Dataconfig extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 7
    }
  })
  config_code?: string;

  @property({
    type: 'string',
    required: true,
  })
  config_desc: string;

  @property({
    type: 'string',
  })
  label_code: string;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'float'
    }
  })
  config_value: number;

  @property({
    type: 'number',
    required: true
  })
  config_group_id: number;

  @property({
    type: 'number',
    required: true
  })
  state?: number;

  constructor(data?: Partial<Dataconfig>) {
    super(data);
  }

}

export interface DataconfigRelations {
  // describe navigational properties here
}

export type DataconfigWithRelations = Dataconfig & DataconfigRelations;
