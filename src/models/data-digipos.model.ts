import {Entity, model, property} from '@loopback/repository';

@model()
export class DataDigipos extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  digipost_id?: number;

  @property({
    type: 'string',
    required: true,
  })
  trx_date_group: string;

  @property({
    type: 'string',
    required: true,
  })
  ad_code: string;

  @property({
    type: 'string',
    required: true,
  })
  ad_name: string;

  @property({
    type: 'string',
  })
  inv_number?: string;

  @property({
    type: 'string',
  })
  inv_attacment?: string;

  @property({
    type: 'string',
  })
  ba_number_a?: string;

  @property({
    type: 'string',
  })
  ba_attachment_a?: string;

  @property({
    type: 'string',
  })
  ba_number_b?: string;

  @property({
    type: 'string',
  })
  ba_attachment_b?: string;

  @property({
    type: 'string',
  })
  vat_number?: string;

  @property({
    type: 'string',
  })
  vat_attachment?: string;

  @property({
    type: 'number',
    default: 0
  })
  vat_distribute: number;

  @property({
    type: 'string',
    default: ""
  })
  at_vat_distibute: string;

  @property({
    type: 'string',
  })
  inv_desc?: string;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  sales_fee: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  sales_paid: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  trx_cluster_out: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  mdr_fee_amount: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  dpp_fee_amount: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  vat_fee_amount: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  total_fee_amount: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  pph_fee_amount: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  trx_paid_invoice: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  paid_inv_amount: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  inv_adj_amount: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  dpp_paid_amount: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  vat_paid_amount: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  total_paid_amount: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  pph_paid_amount: number;

  @property({
    type: 'string',
    required: true,
  })
  remark: string;

  @property({
    type: 'number',
  })
  ar_stat: number;

  @property({
    type: 'number',
  })
  ar_valid_a: number;

  @property({
    type: 'number',
  })
  ar_valid_b: number;

  @property({
    type: 'boolean',
    required: true,
  })
  response_stat: boolean;

  @property({
    type: 'number',
    default: 0
  })
  generate_file_a: number;

  @property({
    type: 'number',
    default: 0
  })
  generate_file_b: number;

  @property({
    type: 'number',
    default: 0
  })
  google_drive_a: number;

  @property({
    type: 'number',
    default: 0
  })
  google_drive_b: number;

  @property({
    type: 'number',
    default: 0
  })
  sign_off_a: number;

  @property({
    type: 'number',
    default: 0
  })
  sign_off_b: number;

  @property({
    type: 'number',
    default: 0
  })
  acc_file_ba_a: number;
  @property({
    type: 'number',
    default: 0
  })
  acc_file_ba_b: number;

  @property({
    type: 'number',
    default: 0
  })
  acc_file_invoice: number;

  @property({
    type: 'number',
    default: 0
  })
  generate_file_invoice: number;

  @property({
    type: 'number',
    default: 0
  })
  distribute_file_invoice: number;

  @property({
    type: 'string',
    default: ""
  })
  attach_invoice_ttd: string;

  @property({
    type: 'number',
    default: 0
  })
  distribute_invoice_ttd: number;

  @property({
    type: 'string',
    default: ""
  })
  at_distribute_invoice_ttd: string;

  @property({
    type: 'number',
    default: 0
  })
  acc_recv: number;

  @property({
    type: 'string'
  })
  at_generate_csv: string;

  @property({
    type: 'number',
    default: 0
  })
  flag_generate_csv: number;

  @property({
    type: 'string'
  })
  oracle_id: string;

  @property({
    type: 'string',
    default: "NEW",
  })
  interface_status: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'text',
    }
  })
  error_message: string;

  constructor(data?: Partial<DataDigipos>) {
    super(data);
  }


}

export interface DataDigiposRelations {
  // describe navigational properties here
}

export type DataDigiposWithRelations = DataDigipos & DataDigiposRelations;
