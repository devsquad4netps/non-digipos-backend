import {Entity, model, property} from '@loopback/repository';

@model({settings: {strict: false}})
export class Rolemapping extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  UserId: string;

  @property({
    type: 'number',
    required: true,
  })
  RoleId: number;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Rolemapping>) {
    super(data);
  }
}

export interface RolemappingRelations {
  // describe navigational properties here
}

export type RolemappingWithRelations = Rolemapping & RolemappingRelations;
