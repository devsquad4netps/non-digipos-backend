import {Entity, model, property} from '@loopback/repository';

@model({
  name: 'MASTER_BANK'
})
export class MasterBank extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  ID?: number;

  @property({
    type: 'string',
    required: true,
  })
  NAME_BANK: string;

  @property({
    type: 'string',
    required: true,
    mysql: {
      dataType: 'varchar',
      dataLength: 10,
    }
  })
  CODE_BANK: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 35,
    }
  })
  AT_CREATE?: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 35,
    }
  })
  AT_UPDATE?: string;

  @property({
    type: 'number',
    default: 1,
  })
  AT_FLAG?: number;

  @property({
    type: 'string',
  })
  AT_WHO?: string;

  constructor(data?: Partial<MasterBank>) {
    super(data);
  }
}

export interface MasterBankRelations {
  // describe navigational properties here
}

export type MasterBankWithRelations = MasterBank & MasterBankRelations;
