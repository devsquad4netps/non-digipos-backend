import {Entity, model, property} from '@loopback/repository';

@model({
  name: 'MERCHANT_CLASS'
})
export class MerchantClass extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  MC_ID?: number;

  @property({
    type: 'string',
    required: true,
  })
  MC_LABEL: string;


  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 35,
    }
  })
  AT_CREATE?: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 35,
    }
  })
  AT_UPDATE?: string;

  @property({
    type: 'number',
    default: 1,
  })
  AT_FLAG?: number;

  @property({
    type: 'string',
  })
  AT_WHO?: string;

  constructor(data?: Partial<MerchantClass>) {
    super(data);
  }
}

export interface MerchantClassRelations {
  // describe navigational properties here
}

export type MerchantClassWithRelations = MerchantClass & MerchantClassRelations;
