import {Entity, model, property} from '@loopback/repository';

@model({
  name: 'TRANSAKSI_TYPE_DETAIL'
})
export class TransaksiTypeDetail extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  TTD_ID?: number;

  @property({
    type: 'string',
    required: true,
  })
  TTD_KEY: string;

  @property({
    type: 'string',
    required: true,
  })
  TTD_LABEL: string;

  @property({
    type: 'number',
    default: 0,
  })
  TTD_FLAG?: number;

  @property({
    type: 'string',
  })
  AT_CREATE?: string;

  @property({
    type: 'string',
  })
  AT_UPDATE?: string;

  @property({
    type: 'string',
  })
  AT_USER?: string;

  @property({
    type: 'number',
    required: true,
  })
  TT_ID?: number;


  constructor(data?: Partial<TransaksiTypeDetail>) {
    super(data);
  }
}

export interface TransaksiTypeDetailRelations {
  // describe navigational properties here
}

export type TransaksiTypeDetailWithRelations = TransaksiTypeDetail & TransaksiTypeDetailRelations;
