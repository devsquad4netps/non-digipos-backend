import {Entity, model, property} from '@loopback/repository';


@model({
  name: 'INVOICE_TYPE'
})
export class InvoiceType extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  ID?: number;

  @property({
    type: 'string',
    required: true,
  })
  INVOICE_TYPE_LABEL: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 35,
    }
  })
  AT_CREATE?: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 35,
    }
  })
  AT_UPDATE?: string;

  @property({
    type: 'number',
    default: 1,
  })
  AT_FLAG?: number;

  @property({
    type: 'string',
  })
  AT_WHO?: string;

  constructor(data?: Partial<InvoiceType>) {
    super(data);
  }
}

export interface InvoiceTypeRelations {
  // describe navigational properties here
}

export type InvoiceTypeWithRelations = InvoiceType & InvoiceTypeRelations;
