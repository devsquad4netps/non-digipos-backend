import {Entity, hasMany, model, property} from '@loopback/repository';
import {AdCluster} from './ad-cluster.model';

@model()
export class AdMaster extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: false,
    required: true,
  })
  ad_code: string;

  @property({
    type: 'string',
    required: true,
  })
  ad_name: string;

  @property({
    type: 'string',
  })
  legal_name: string;

  @property({
    type: 'string',
  })
  global_name: string;

  @property({
    type: 'string',
    required: true,
  })
  address: string;

  @property({
    type: 'string',
    required: true,
  })
  city: string;

  @property({
    type: 'number',
    required: true,
  })
  post_code: number;

  @property({
    type: 'string',
    required: true,
  })
  npwp: string;

  @property({
    type: 'string',
    required: true,
  })
  npwp_attachment: string;


  @property({
    type: 'string',
    required: true,
  })
  npwp_address: string;

  @property({
    type: 'string',
    required: true,
  })
  pks_attachment: string;

  @property({
    type: 'string',
    required: true,
  })
  attention: string;

  @property({
    type: 'string',
    required: true,
  })
  attention_position: string;

  @property({
    type: 'string',
    required: true,
  })
  phone_number: string;

  @property({
    type: 'string',
    required: true,
  })
  email: string;

  @property({
    type: 'string',
    required: true,
  })
  contract_number: string;

  @property({
    type: 'date',
    required: true,
    // jsonSchema: {
    //   format: 'date',
    // },
    mysql: {
      dataType: 'date'
    }
  })
  join_date: string;

  @property({
    type: 'date',
    // jsonSchema: {
    //   format: 'date',
    // },
    mysql: {
      dataType: 'date'
    }
  })
  expire_date: string;

  @property({
    type: 'boolean',
    required: true,
  })
  ad_status: boolean;

  @property({
    type: 'string',
    required: true
  })
  prefix: string;

  @property({
    type: 'string',
  })
  name_rekening: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 30
    }
  })
  no_rek?: string;

  @property({
    type: 'string'
  })
  name_bank: string;

  @property({
    type: 'string',
  })
  gd_bar_a: string;

  @property({
    type: 'string',
  })
  gd_summary_a: string;

  @property({
    type: 'string',
  })
  gd_bukpot: string;

  @property({
    type: 'string',
  })
  gd_bar_b: string;

  @property({
    type: 'string',
  })
  gd_summary_b: string;

  @property({
    type: 'string',
  })
  gd_invoice: string;

  @property({
    type: 'string',
  })
  gd_faktur_pajak: string;



  @property({
    type: 'string',
  })
  interface_status: string;

  @property({
    type: 'string',
  })
  organization_id: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'text',
    }
  })
  error_message: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'text',
    }
  })
  alias_name: string;


  @hasMany(() => AdCluster, {keyTo: 'ad_code'})
  cluster: AdCluster[];

  constructor(data?: Partial<AdMaster>) {
    super(data);
  }

}

export interface AdMasterRelations {
  // describe navigational properties here
}

export type AdMasterWithRelations = AdMaster & AdMasterRelations;
