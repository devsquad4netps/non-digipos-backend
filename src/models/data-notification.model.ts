import {Entity, model, property} from '@loopback/repository';

@model()
export class DataNotification extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  types: string;

  @property({
    type: 'string',
  })
  target?: string;

  @property({
    type: 'string',
  })
  to?: string;

  @property({
    type: 'string',
    required: true,
  })
  from_who: string;

  @property({
    type: 'string',
    required: true,
  })
  subject: string;

  @property({
    type: 'string',
    required: true,
  })
  body: string;

  @property({
    type: 'string',
  })
  path_url?: string;

  @property({
    type: 'string',
    required: true,
  })
  app: string;

  @property({
    type: 'string',
    required: true,
  })
  code: string;

  @property({
    type: 'string',
  })
  code_label: string;

  @property({
    type: 'string',
  })
  at_create?: string;

  @property({
    type: 'string',
  })
  at_read?: string;

  @property({
    type: 'string',
  })
  periode?: string;

  @property({
    type: 'number',
    default: 0
  })
  at_flag?: number;

  constructor(data?: Partial<DataNotification>) {
    super(data);
  }
}

export interface DataNotificationRelations {
  // describe navigational properties here
}

export type DataNotificationWithRelations = DataNotification & DataNotificationRelations;
