import {Entity, model, property} from '@loopback/repository';

@model({settings: {strict: false}})
export class ScheduleData extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  time_date: string;

  @property({
    type: 'number',
    required: true,
  })
  flag: number;

  @property({
    type: 'number',
    required: true,
  })
  status: number;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<ScheduleData>) {
    super(data);
  }
}

export interface ScheduleDataRelations {
  // describe navigational properties here
}

export type ScheduleDataWithRelations = ScheduleData & ScheduleDataRelations;
