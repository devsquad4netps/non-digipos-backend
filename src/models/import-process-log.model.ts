import {Entity, model, property} from '@loopback/repository';

@model({
  name: 'IMPORT_PROCESS_LOG'
})
export class ImportProcessLog extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  IMPORT_ID?: number;

  @property({
    type: 'string',
    required: true,
  })
  IM_PERIODE: string;

  @property({
    type: 'number',
    required: true,
  })
  M_ID: number;

  @property({
    type: 'number',
    required: true,
  })
  MP_ID: number;

  @property({
    type: 'number',
  })
  IM_COUNT_TRX?: number;

  @property({
    type: 'number',
    default: 0,
  })
  IM_TRX_AMOUNT?: number;

  @property({
    type: 'number',
    default: 0,
  })
  IM_MDR?: number;

  @property({
    type: 'string',
    required: true,
    mysql: {
      dataType: 'text',
    }
  })
  IM_PATH: string;

  @property({
    type: 'string',
    required: true,
    mysql: {
      dataType: 'text',
    }
  })
  IM_NAME: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 35,
    }
  })
  AT_CREATE?: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 35,
    }
  })
  AT_UPDATE?: string;

  @property({
    type: 'number',
    default: 1,
  })
  AT_FLAG?: number;

  @property({
    type: 'string',
  })
  AT_WHO?: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'text',
    }
  })
  IM_MESSAGE?: string;

  @property({
    type: 'number',
    default: 0,
  })
  IM_PROCESS_COUNT?: number;

  @property({
    type: 'number',
    default: 0,
  })
  IM_PROCESS_NOW?: number;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 150,
    }
  })
  IM_MIME_TYPE?: string;


  constructor(data?: Partial<ImportProcessLog>) {
    super(data);
  }
}

export interface ImportProcessLogRelations {
  // describe navigational properties here
}

export type ImportProcessLogWithRelations = ImportProcessLog & ImportProcessLogRelations;
