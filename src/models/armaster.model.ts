import {Entity, model, property} from '@loopback/repository';

@model()
export class Armaster extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  trx_date_group: string;

  @property({
    type: 'string',
    required: true,
  })
  ad_code: string;

  @property({
    type: 'string',
    required: true,
  })
  ad_name: string;

  @property({
    type: 'string',
  })
  inv_number?: string;

  @property({
    type: 'string',
  })
  inv_attacment?: string;

  @property({
    type: 'string',
  })
  ba_number_a?: string;

  @property({
    type: 'string',
  })
  ba_attachment_a?: string;

  @property({
    type: 'string',
  })
  ba_number_b?: string;

  @property({
    type: 'string',
  })
  ba_attachment_b?: string;

  @property({
    type: 'string',
  })
  vat_number?: string;

  @property({
    type: 'string',
  })
  vat_attachment?: string;

  @property({
    type: 'string',
  })
  inv_desc?: string;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'float',
      precision: 20,
      scale: 4
    }
  })
  sales_fee: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'float',
      precision: 20,
      scale: 4
    }
  })
  trx_cluster_out: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'float',
      precision: 20,
      scale: 4
    }
  })
  mdr_fee_amount: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'float',
      precision: 20,
      scale: 4
    }
  })
  dpp_fee_amount: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'float',
      precision: 20,
      scale: 4
    }
  })
  vat_fee_amount: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'float',
      precision: 20,
      scale: 4
    }
  })
  total_fee_amount: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'float',
      precision: 20,
      scale: 4
    }
  })
  pph_fee_amount: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'float',
      precision: 20,
      scale: 4
    }
  })
  trx_paid_invoice: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'float',
      precision: 20,
      scale: 4
    }
  })
  paid_inv_amount: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'float',
      precision: 20,
      scale: 4
    }
  })
  inv_adj_amount: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'float',
      precision: 20,
      scale: 4
    }
  })
  dpp_paid_amount: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'float',
      precision: 20,
      scale: 4
    }
  })
  vat_paid_amount: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'float',
      precision: 20,
      scale: 4
    }
  })
  total_paid_amount: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'float',
      precision: 20,
      scale: 4
    }
  })
  pph_paid_amount: number;

  @property({
    type: 'string',
    required: true,
  })
  remark: string;

  @property({
    type: 'boolean',
    required: true,
  })
  ar_stat: boolean;

  @property({
    type: 'boolean',
    required: true,
  })
  response_stat: boolean;



  constructor(data?: Partial<Armaster>) {
    super(data);
  }
}

export interface ArmasterRelations {
  // describe navigational properties here
}

export type ArmasterWithRelations = Armaster & ArmasterRelations;
