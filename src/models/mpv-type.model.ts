import {Entity, model, property} from '@loopback/repository';


@model({
  name: 'MPV_TYPE'
})
export class MpvType extends Entity {

  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  MPV_TYPE_ID?: number;

  @property({
    type: 'string',
    required: true,
    mysql: {
      dataType: 'varchar',
      dataLength: 100,
    }
  })
  MPV_TYPE_LABEL: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 35,
    }
  })
  AT_CREATE?: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 35,
    }
  })
  AT_UPDATE?: string;

  @property({
    type: 'number',
    default: 1,
  })
  AT_FLAG?: number;

  @property({
    type: 'string',
  })
  AT_WHO?: string;


  constructor(data?: Partial<MpvType>) {
    super(data);
  }
}

export interface MpvTypeRelations {
  // describe navigational properties here
}

export type MpvTypeWithRelations = MpvType & MpvTypeRelations;
