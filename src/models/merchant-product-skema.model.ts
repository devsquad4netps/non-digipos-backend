import {Entity, model, property} from '@loopback/repository';

@model({
  name: 'MERCHANT_PRODUCT_SKEMA'
})
export class MerchantProductSkema extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  MPS_ID?: number;

  @property({
    type: 'string',
    required: true,
    mysql: {
      dataType: 'varchar',
      dataLength: 100,
    }
  })
  MPS_LABEL: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'TEXT',
    }
  })
  MPS_PATH_TEMPLATE?: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'TEXT',
    }
  })
  MPS_PATH_TEMPLATE_PLUS?: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'TEXT',
    }
  })
  MPS_PATH_TEMPLATE_MINUS?: string;

  @property({
    type: 'number',
  })
  MPS_COUNT_PIHAK: number;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 35,
    }
  })
  AT_CREATE?: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 35,
    }
  })
  AT_UPDATE?: string;

  @property({
    type: 'number',
    default: 1,
  })
  AT_FLAG?: number;

  @property({
    type: 'string',
  })
  AT_WHO?: string;


  constructor(data?: Partial<MerchantProductSkema>) {
    super(data);
  }
}

export interface MerchantProductSkemaRelations {
  // describe navigational properties here
}

export type MerchantProductSkemaWithRelations = MerchantProductSkema & MerchantProductSkemaRelations;
