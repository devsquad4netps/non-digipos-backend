import {Entity, hasOne, model, property} from '@loopback/repository';
import {DataUserCredentials} from './data-user-credentials.model';


@model()
export class DataUser extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: false,
    defaultFn: 'uuidv4',
  })
  id: string;

  @property({
    type: 'string',
    required: true,
  })
  nik: string;

  @property({
    type: 'string',
    required: true,
  })
  fullname: string;

  @property({
    type: 'string',
    required: true,
    index: {
      unique: true,
    },
  })
  email: string;

  @property({
    type: 'string',
    required: true,
  })
  jabatan: string;

  @property({
    type: 'string',
    required: true,
  })
  divisi: string;

  @property({
    type: 'boolean',
    default: false,
  })
  email_verified?: boolean;

  @property({
    type: 'string',
  })
  verification_code?: string;

  @property({
    type: 'string',
  })
  at_sender_email?: string;


  @property({
    type: 'number',
    required: true,
    default: 0,
  })
  state: number;

  @property({
    type: 'string',
  })
  at_create?: string;

  @property({
    type: 'string',
  })
  at_update?: string;

  @property({
    type: 'string',
  })
  at_banned?: string;

  @property({
    type: 'string',
  })
  at_key?: string;

  @property({
    type: 'string',
    required: true,
  })
  company_code?: string;

  @property({
    type: 'string',
  })
  log_code?: string;



  @hasOne(() => DataUserCredentials, {keyTo: 'userId'})
  userCredentials: DataUserCredentials;

  constructor(data?: Partial<DataUser>) {
    super(data);
  }
}

export interface DataUserRelations {
  // describe navigational properties here
}

export type DataUserWithRelations = DataUser & DataUserRelations;
