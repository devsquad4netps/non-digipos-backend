import {Entity, model, property} from '@loopback/repository';

@model({
  name: 'MERCHANT_PRODUCT'
})
export class MerchantProduct extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  MP_ID: number;

  @property({
    type: 'string',
    required: true,
    mysql: {
      dataType: 'varchar',
      dataLength: 10,
    }
  })
  MP_CODE: string;

  @property({
    type: 'string',
    required: true,
    mysql: {
      dataType: 'varchar',
      dataLength: 250,
    }
  })
  MP_BRAND: string;

  @property({
    type: 'string',
    required: true,
  })
  MP_LABEL: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'text',
    }
  })
  MP_DESCRIPTION: string;

  @property({
    type: 'number',
    required: true,
  })
  MP_TYPE_INVOICE: number;

  @property({
    type: 'number',
    required: true,
  })
  MP_TYPE_SKEMA: number;

  @property({
    type: 'number',
    required: true,
  })
  MPT_ID: number;

  @property({
    type: 'number',
  })
  M_ID?: number;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 35,
    }
  })
  AT_CREATE?: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 35,
    }
  })
  AT_UPDATE?: string;

  @property({
    type: 'number',
    default: 1,
  })
  AT_FLAG?: number;

  @property({
    type: 'string',
  })
  AT_WHO?: string;

  constructor(data?: Partial<MerchantProduct>) {
    super(data);
  }
}

export interface MerchantProductRelations {
  // describe navigational properties here
}

export type MerchantProductWithRelations = MerchantProduct & MerchantProductRelations;
