import {Entity, model, property} from '@loopback/repository';

@model()
export class DataRoleMap extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  UserId: string;

  @property({
    type: 'number',
    required: true,
  })
  RoleId: number;

  constructor(data?: Partial<DataRoleMap>) {
    super(data);
  }

}

export interface DataRoleMapRelations {
  // describe navigational properties here
}

export type DataRoleMapWithRelations = DataRoleMap & DataRoleMapRelations;
