import {Entity, model, property} from '@loopback/repository';

@model()
export class DataConfigGroup extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  config_group_id?: number;

  @property({
    type: 'string',
    required: true,
  })
  group_label: string;

  @property({
    type: 'number',
    default: 0,
  })
  state?: number;

  constructor(data?: Partial<DataConfigGroup>) {
    super(data);
  }
}

export interface DataConfigGroupRelations {
  // describe navigational properties here
}

export type DataConfigGroupWithRelations = DataConfigGroup & DataConfigGroupRelations;
