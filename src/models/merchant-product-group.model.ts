import {Entity, model, property} from '@loopback/repository';

@model({
  name: 'MERCHANT_PRODUCT_GROUP'
})
export class MerchantProductGroup extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  MPG_ID?: number;

  @property({
    type: 'string',
    required: true,
    mysql: {
      dataType: 'varchar',
      dataLength: 150,
    }
  })
  MPG_LABEL: string;

  @property({
    type: 'number',
    required: true,
  })
  MPG_MDR_TYPE: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  MPG_MDR_AMOUNT: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  MPG_DPP: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  MPG_PPN: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  MPG_PPH: number;

  // @property({
  //   type: 'string',
  //   mysql: {
  //     dataType: 'TEXT',
  //   }
  // })
  // MPG_QUERY: string;

  @property({
    type: 'number',
    required: true,
  })
  M_ID: number;

  @property({
    type: 'number',
    required: true,
  })
  MP_ID: number;


  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 35,
    }
  })
  AT_CREATE?: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 35,
    }
  })
  AT_UPDATE?: string;

  @property({
    type: 'number',
    default: 1,
  })
  AT_FLAG?: number;

  @property({
    type: 'string',
  })
  AT_WHO?: string;



  constructor(data?: Partial<MerchantProductGroup>) {
    super(data);
  }
}

export interface MerchantProductGroupRelations {
  // describe navigational properties here
}

export type MerchantProductGroupWithRelations = MerchantProductGroup & MerchantProductGroupRelations;
