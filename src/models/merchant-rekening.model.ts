import {Entity, model, property} from '@loopback/repository';

@model({
  name: 'MERCHANT_REKENING'
})
export class MerchantRekening extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  MR_ID?: number;

  @property({
    type: 'string',
    required: true,
  })
  MR_REKENING_NAME: string;

  @property({
    type: 'string',
    required: true,
    index: {
      unique: true,
    },
  })
  MR_ACOUNT_NUMBER: string;

  @property({
    type: 'string',
    required: true,
  })
  MR_BANK_NAME: string;

  @property({
    type: 'string',
    required: true,
  })
  MR_BANK_BRANCH: string;


  @property({
    type: 'number',
    required: true,
  })
  M_ID?: number;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 35,
    }
  })
  AT_CREATE?: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 35,
    }
  })
  AT_UPDATE?: string;

  @property({
    type: 'number',
    default: 1,
  })
  AT_FLAG?: number;

  @property({
    type: 'string',
  })
  AT_WHO?: string;

  constructor(data?: Partial<MerchantRekening>) {
    super(data);
  }
}

export interface MerchantRekeningRelations {
  // describe navigational properties here
}

export type MerchantRekeningWithRelations = MerchantRekening & MerchantRekeningRelations;
