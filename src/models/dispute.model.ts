import {Entity, model, property} from '@loopback/repository';

@model()
export class Dispute extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  dispute_id?: number;

  @property({
    type: 'string',
    required: true,
  })
  ad_code: string;

  @property({
    type: 'string',
    required: true,
  })
  ad_name: string;

  @property({
    type: 'string',
    required: true,
  })
  sales_fee: string;

  @property({
    type: 'number',
    required: true,
  })
  trx_a: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 3
    }
  })
  tot_mdr_a: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 3
    }
  })
  dpp_fee_amount: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 3
    }
  })
  ppn_fee_amount: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 3
    }
  })
  pph_fee_amount: number;

  @property({
    type: 'number',
    required: true,
  })
  trx_b: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 3
    }
  })
  tot_mdr_b: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 3
    }
  })
  dpp_paid_amount: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 3
    }
  })
  ppn_paid_amount: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 3
    }
  })
  pph_paid_amount: number;

  @property({
    type: 'string',
  })
  remarks?: string;

  @property({
    type: 'number',
    default: 0,
  })
  acc_ba?: number;

  @property({
    type: 'string',
    required: true,
  })
  periode: string;

  @property({
    type: 'string',
    required: true,
  })
  at_create: string;

  @property({
    type: 'string',
  })
  at_acc: string;

  @property({
    type: 'string',
    required: true,
  })
  trx_trx: string;


  constructor(data?: Partial<Dispute>) {
    super(data);
  }
}

export interface DisputeRelations {
  // describe navigational properties here
}

export type DisputeWithRelations = Dispute & DisputeRelations;
