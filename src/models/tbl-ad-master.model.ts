import {Entity, model, property} from '@loopback/repository';

@model({settings: {strict: false}})
export class TblAdMaster extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: false,
    required: true,
  })
  ad_code: string;

  @property({
    type: 'string',
    required: true,
  })
  ad_name: string;

  @property({
    type: 'string',
    required: true,
  })
  address: string;

  @property({
    type: 'string',
    required: true,
  })
  city: string;

  @property({
    type: 'number',
    required: true,
  })
  post_code: number;

  @property({
    type: 'string',
    required: true,
  })
  npwp: string;

  @property({
    type: 'string',
    required: true,
  })
  attention: string;

  @property({
    type: 'string',
    required: true,
  })
  attention_position: string;

  @property({
    type: 'string',
    required: true,
  })
  phone_number: string;

  @property({
    type: 'string',
    required: true,
  })
  email: string;

  @property({
    type: 'string',
    required: true,
  })
  contract_number: string;

  @property({
    type: 'date',
    required: true,
  })
  join_date: string;

  @property({
    type: 'boolean',
    required: true,
  })
  ad_status: boolean;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<TblAdMaster>) {
    super(data);
  }
}

export interface TblAdMasterRelations {
  // describe navigational properties here
}

export type TblAdMasterWithRelations = TblAdMaster & TblAdMasterRelations;
