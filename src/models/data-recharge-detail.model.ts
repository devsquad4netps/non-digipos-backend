import {Entity, model, property} from '@loopback/repository';

@model()
export class DataRechargeDetail extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: false,
    defaultFn: 'uuidv4',
  })
  id?: string;

  @property({
    type: 'string',
  })
  periode?: string;

  @property({
    type: 'string',
    required: true,
  })
  dealer_code: string;

  @property({
    type: 'string',
    required: true,
  })
  location: string;

  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'string',
    required: true,
  })
  type_trx: string;

  @property({
    type: 'number',
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  paid_in_amount?: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  withdraw_amount: number;

  @property({
    type: 'number',
    required: true,
  })
  reason_type_id: number;

  @property({
    type: 'string',
  })
  reason_type_name?: string;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  trx_a: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  tot_mdr_a: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  trx_b: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  tot_mdr_b: number;

  @property({
    type: 'number',
    default: 0,
  })
  status?: number;

  @property({
    type: 'string'
  })
  create_at?: string;

  @property({
    type: 'number'
  })
  state?: number;

  @property({
    type: 'number'
  })
  pos_index?: number;

  @property({
    type: 'string'
  })
  code_ad?: string;

  constructor(data?: Partial<DataRechargeDetail>) {
    super(data);
  }

}

export interface DataRechargeDetailRelations {
  // describe navigational properties here
}

export type DataRechargeDetailWithRelations = DataRechargeDetail & DataRechargeDetailRelations;
