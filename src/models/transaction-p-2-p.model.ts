import {Entity, model, property} from '@loopback/repository';

@model({
  name: 'TRANSACTION_P2P'
})
export class TransactionP2P extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  ID?: number;


  constructor(data?: Partial<TransactionP2P>) {
    super(data);
  }
}

export interface TransactionP2PRelations {
  // describe navigational properties here
}

export type TransactionP2PWithRelations = TransactionP2P & TransactionP2PRelations;
