import {Entity, model, property} from '@loopback/repository';

@model({settings: {strict: false}})

export class Menu extends Entity {

  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
  })
  M_Icon?: string;

  @property({
    type: 'string',
  })
  M_Name?: string;

  @property({
    type: 'string',
    required: true,
  })
  M_Label: string;

  @property({
    type: 'string',
    required: true,
  })
  M_Link: string;

  @property({
    type: 'string',
  })
  M_Class?: string;

  @property({
    type: 'string',
  })
  M_Alt?: string;

  @property({
    type: 'number',
    default: 1,
  })
  M_Status?: number;

  @property({
    type: 'number',
    required: true,
  })
  M_Parent: number;

  @property({
    type: 'number',
    required: true,
  })
  M_Index: number;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Menu>) {
    super(data);
  }
}

export interface MenuRelations {
  // describe navigational properties here
}

export type MenuWithRelations = Menu & MenuRelations;
