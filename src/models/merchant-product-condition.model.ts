import {Entity, model, property} from '@loopback/repository';

@model({
  name: 'MERCHANT_PRODUCT_CONDITION'
})
export class MerchantProductCondition extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  MPC_ID?: number;

  @property({
    type: 'number',
    required: true,
  })
  MPV_ID: number;

  @property({
    type: 'string',
    required: true,
  })
  MPC_OPERATOR: string;

  @property({
    type: 'string',
    required: true,
    mysql: {
      dataType: 'text',
    }
  })
  MPC_VALUE: string;

  @property({
    type: 'number',
    required: true,
    default: 1,
  })
  MPC_GROUP: number;

  @property({
    type: 'number',
    required: true,
  })
  M_ID: number;

  @property({
    type: 'number',
    required: true,
  })
  MP_ID: number;

  @property({
    type: 'number',
    required: true,
  })
  MPG_ID: number;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 35,
    }
  })
  AT_CREATE?: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 35,
    }
  })
  AT_UPDATE?: string;

  @property({
    type: 'number',
    default: 1,
  })
  AT_FLAG?: number;

  @property({
    type: 'string',
  })
  AT_WHO?: string;


  constructor(data?: Partial<MerchantProductCondition>) {
    super(data);
  }
}

export interface MerchantProductConditionRelations {
  // describe navigational properties here
}

export type MerchantProductConditionWithRelations = MerchantProductCondition & MerchantProductConditionRelations;
