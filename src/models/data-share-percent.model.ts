import {Entity, model, property} from '@loopback/repository';

@model()
export class DataSharePercent extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  periode: string;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  fee_recharce_share: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  paid_recharge_share: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  dpp_recharge_share_fee: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  ppn_recharge_share_fee: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  pph_recharge_share_fee: number;



  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  dpp_recharge_share_paid: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  ppn_recharge_share_paid: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  pph_recharge_share_paid: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  fee_ppob_share: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  paid_ppob_share: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  dpp_ppob_share: number;


  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  ppn_ppob_share: number;


  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  pph_ppob_share: number;


  constructor(data?: Partial<DataSharePercent>) {
    super(data);
  }
}

export interface DataSharePercentRelations {
  // describe navigational properties here
}

export type DataSharePercentWithRelations = DataSharePercent & DataSharePercentRelations;
