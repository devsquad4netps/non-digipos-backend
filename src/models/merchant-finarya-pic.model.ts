import {Entity, model, property} from '@loopback/repository';

@model({
  name: 'MERCHANT_FINARYA_PIC'
})
export class MerchantFinaryaPic extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  ID?: number;

  @property({
    type: 'string',
    required: true,
  })
  PIC_NAME: string;

  @property({
    type: 'string',
    required: true,
  })
  PIC_JABATAN: string;

  @property({
    type: 'string',
    required: true,
  })
  PIC_ALAMAT: string;

  @property({
    type: 'string',
    required: true,
  })
  PIC_EMAIL: string;

  @property({
    type: 'string',
    required: true,
  })
  PIC_SIGN: string;

  @property({
    type: 'string',
  })
  PIC_NOTELP?: string;

  @property({
    type: 'string',
    required: true,
  })
  PT_NAME: string;

  @property({
    type: 'string',
    required: true,
  })
  PT_ALAMAT: string;

  @property({
    type: 'string',
  })
  PT_NPWP?: string;

  @property({
    type: 'string',
  })
  PT_NOTELP?: string;


  @property({
    type: 'string',
    required: true,
  })
  PT_ACOUNT_NAME: string;

  @property({
    type: 'string',
    required: true,
  })
  PT_ACOUNT_NUMBER: string;

  @property({
    type: 'string',
    required: true,
  })
  PT_BANK_NAME: string;

  @property({
    type: 'string',
    required: true,
  })
  PT_BRANCH: string;



  @property({
    type: 'number',
  })
  PIC_TYPE?: number;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 35,
    }
  })
  AT_CREATE?: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 35,
    }
  })
  AT_UPDATE?: string;

  @property({
    type: 'number',
    default: 1,
  })
  AT_FLAG?: number;

  @property({
    type: 'string',
  })
  AT_WHO?: string;

  constructor(data?: Partial<MerchantFinaryaPic>) {
    super(data);
  }
}

export interface MerchantFinaryaPicRelations {
  // describe navigational properties here
}

export type MerchantFinaryaPicWithRelations = MerchantFinaryaPic & MerchantFinaryaPicRelations;
