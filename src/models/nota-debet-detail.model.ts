import {Entity, model, property} from '@loopback/repository';

@model()
export class NotaDebetDetail extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  nota_debet_detail_id?: number;

  @property({
    type: 'string',
    required: true,
  })
  description: string;

  @property({
    type: 'number',
    required: true,
  })
  qty: number;

  @property({
    type: 'string',
  })
  unit?: string;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'float',
      precision: 20,
      scale: 4
    }
  })
  unit_price: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'float',
      precision: 20,
      scale: 4
    }
  })
  total_price: number;

  @property({
    type: 'number',
    default: 0,
    required: true,
  })
  nota_debet_id: number;

  @property({
    type: 'number',
    default: 0,
  })
  state?: number;


  constructor(data?: Partial<NotaDebetDetail>) {
    super(data);
  }
}

export interface NotaDebetDetailRelations {
  // describe navigational properties here
}

export type NotaDebetDetailWithRelations = NotaDebetDetail & NotaDebetDetailRelations;
