import {Entity, model, property} from '@loopback/repository';

@model()
export class DataUserCredentials extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: false,
    defaultFn: 'uuidv4',
  })
  id: string;

  @property({
    type: 'string',
    required: true,
  })
  password: string;

  @property({
    type: 'string',
    required: true,
  })
  userId: string;


  constructor(data?: Partial<DataUserCredentials>) {
    super(data);
  }
}

export interface DataUserCredentialsRelations {
  // describe navigational properties here
}

export type DataUserCredentialsWithRelations = DataUserCredentials & DataUserCredentialsRelations;
