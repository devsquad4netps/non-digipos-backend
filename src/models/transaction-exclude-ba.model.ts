import {Entity, model, property} from '@loopback/repository';

@model({
  name: 'TRANSACTION_EXCLUDE_BA'
})
export class TransactionExcludeBa extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  ID?: number;


  constructor(data?: Partial<TransactionExcludeBa>) {
    super(data);
  }
}

export interface TransactionExcludeBaRelations {
  // describe navigational properties here
}

export type TransactionExcludeBaWithRelations = TransactionExcludeBa & TransactionExcludeBaRelations;
