import {Entity, model, property} from '@loopback/repository';

@model({
  name: 'MERCHANT_PRODUCT_GROUP_MDR_CONDITION'
})
export class MerchantProductMdrCondition extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  MDRCON_ID?: number;

  @property({
    type: 'number',
    required: true,
  })
  MPV_ID: number;

  @property({
    type: 'string',
    required: true,
  })
  MDRCON_OPERATOR: string;

  @property({
    type: 'string',
    required: true,
    mysql: {
      dataType: 'text',
    }
  })
  MDRCON_VALUE: string;

  @property({
    type: 'number',
    required: true,
    default: 1,
  })
  MDRCON_GROUP: number;

  @property({
    type: 'number',
    required: true,
  })
  MDR_ID: number;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 35,
    }
  })
  AT_CREATE?: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 35,
    }
  })
  AT_UPDATE?: string;

  @property({
    type: 'number',
    default: 1,
  })
  AT_FLAG?: number;

  @property({
    type: 'string',
  })
  AT_WHO?: string;

  constructor(data?: Partial<MerchantProductMdrCondition>) {
    super(data);
  }
}

export interface MerchantProductMdrConditionRelations {
  // describe navigational properties here
}

export type MerchantProductMdrConditionWithRelations = MerchantProductMdrCondition & MerchantProductMdrConditionRelations;
