import {Entity, model, property} from '@loopback/repository';

@model()
export class DataDispute extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  dispute_id?: number;

  @property({
    type: 'string',
    required: true,
  })
  periode: string;

  @property({
    type: 'number',
    default: 0
  })
  type_trx: number;

  @property({
    type: 'string',
    required: true,
  })
  ad_code: string;

  @property({
    type: 'string',
    required: true,
  })
  ad_name: string;

  @property({
    type: 'string',
  })
  no_bar?: string;

  @property({
    type: 'string',
  })
  remarks?: string;

  @property({
    type: 'string',
  })
  sales_fee?: string;

  @property({
    type: 'number',
    default: 0,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  qty_trx?: number;

  @property({
    type: 'number',
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  tot_mdr?: number;

  @property({
    type: 'number',
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  dpp_amount?: number;

  @property({
    type: 'number',
    default: 0,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  ppn_amount?: number;

  @property({
    type: 'number',
    default: 0,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  pph_amount?: number;

  @property({
    type: 'number',
    default: 0,
  })
  dispute_status?: number;

  @property({
    type: 'string',
  })
  at_submit: string;

  @property({
    type: 'string',
    default: ""
  })
  by_acc?: string;

  @property({
    type: 'string',
    default: ""
  })
  at_acc: string;

  @property({
    type: 'string',
    default: ""
  })
  note: string;




  constructor(data?: Partial<DataDispute>) {
    super(data);
  }
}

export interface DataDisputeRelations {
  // describe navigational properties here
}

export type DataDisputeWithRelations = DataDispute & DataDisputeRelations;
