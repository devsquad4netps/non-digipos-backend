import {Entity, model, property} from '@loopback/repository';

@model({
  name: 'MERCHANT_PRODUCT_DISTRIBUSI'
})
export class MerchantProductDistribusi extends Entity {

  @property({
    type: 'string',
    id: true,
    generated: false,
    defaultFn: 'uuidv4'
  })
  MPDIST_ID?: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'TEXT',
    }
  })
  MPDIST_MESSAGE: string;

  @property({
    type: 'number',
    default: 0
  })
  SEND_BAR: number;

  @property({
    type: 'number',
    default: 0
  })
  SEND_BAR_SIGN: number;

  @property({
    type: 'number',
    default: 0
  })
  SEND_INVOICE: number;

  @property({
    type: 'number',
    default: 0
  })
  SEND_INVOICE_SIGN: number;

  @property({
    type: 'number',
    default: 0
  })
  SEND_FAKTUR: number;

  @property({
    type: 'number',
    default: 0
  })
  SEND_BUKPOT: number;

  @property({
    type: 'number',
    default: 0
  })
  SEND_OTHERS: number;

  @property({
    type: 'string',
    mysql: {
      dataType: 'TEXT',
    }
  })
  SEND_TO: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'TEXT',
    }
  })
  SEND_CC: string;

  @property({
    type: 'number',
    default: 0
  })
  SEND_FLAG: number;

  @property({
    type: 'string',
    mysql: {
      dataType: 'TEXT',
    }
  })
  SEND_MESSAGE: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 35,
    }
  })
  AT_SEND?: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 35,
    }
  })
  CREATE_SEND?: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 512,
    }
  })
  MPM_ID?: string;

  @property({
    type: 'string',
    required: true,
    mysql: {
      dataType: 'TEXT',
    }
  })
  START_TIME_RUN: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 35,
    }
  })
  AT_CREATE?: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 35,
    }
  })
  AT_UPDATE?: string;

  @property({
    type: 'number',
    default: 1,
  })
  AT_FLAG?: number;

  @property({
    type: 'string',
  })
  AT_WHO?: string;

  @property({
    type: 'number',
  })
  AT_FEEDBACK_MERCHANT?: number;

  @property({
    type: 'number',
  })
  AT_POSISI_BEFORE?: number;

  @property({
    type: 'number',
  })
  AT_FLAG_BEFORE?: number;


  constructor(data?: Partial<MerchantProductDistribusi>) {
    super(data);
  }
}

export interface MerchantProductDistribusiRelations {
  // describe navigational properties here
}

export type MerchantProductDistribusiWithRelations = MerchantProductDistribusi & MerchantProductDistribusiRelations;
