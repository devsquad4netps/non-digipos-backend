import {Entity, model, property} from '@loopback/repository';

@model()
export class DataFpjp extends Entity {

  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    default: ""
  })
  transaction_code: string;

  @property({
    type: 'string',
    required: true,
  })
  periode: string;

  @property({
    type: 'string',
  })
  ad_code: string;

  @property({
    type: 'string',
  })
  ad_name: string;

  @property({
    type: 'string',
  })
  inv_number: string;

  @property({
    type: 'string',
  })
  inv_attahment: string;

  @property({
    type: 'string'
  })
  bar_attachment: string;

  @property({
    type: 'string'
  })
  summary_attachment: string;

  @property({
    type: 'string',
  })
  fp_number: string;

  @property({
    type: 'string',
  })
  fp_attachment?: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'text',
    }
  })
  description: string;

  @property({
    type: 'number',
  })
  qty_trx: number;

  @property({
    type: 'number',
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  price: number;

  @property({
    type: 'number',
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  pph_amount?: number;

  @property({
    type: 'number',
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  total_amount?: number;

  @property({
    type: 'string',
    mysql: {
      dataType: 'text',
    }
  })
  remarks?: string;

  @property({
    type: 'number',
    required: true,
  })
  type_trx: number;

  @property({
    type: 'string',
  })
  bb_attachment?: string;

  @property({
    type: 'string',
  })
  wht_attachment?: string;

  @property({
    type: 'string',
    required: true,
  })
  at_create: string;

  @property({
    type: 'string',
    required: true,
  })
  by_create: string;

  @property({
    type: 'number',
    default: 0,
  })
  acc_bu_state?: number;

  @property({
    type: 'string',
  })
  acc_bu_by?: string;

  @property({
    type: 'string',
  })
  acc_bu_at?: string;

  @property({
    type: 'number',
    default: 0,
  })
  acc_ba_state?: number;

  @property({
    type: 'string',
  })
  acc_ba_by?: string;

  @property({
    type: 'string',
  })
  acc_ba_at?: string;

  @property({
    type: 'number',
    default: 0
  })
  state?: number;

  @property({
    type: 'string'
  })
  fpjp_attach?: string;

  @property({
    type: 'number',
    default: 0
  })
  fpjp_generate?: number;

  @property({
    type: 'string',
    default: ""
  })
  at_ad_submit_fpjp?: string;

  @property({
    type: 'string',
    default: ""
  })
  date_submit_fpjp?: string;

  @property({
    type: 'string',
    default: ""
  })
  by_submit_fpjp?: string;

  @property({
    type: 'string'
  })
  fpjp_generate_err?: string;

  @property({
    type: 'number',
    default: 1
  })
  fpjp_hold_state?: number;

  @property({
    type: 'number',
    default: 0
  })
  fpjp_note_state?: number;

  @property({
    type: 'string'
  })
  fpjp_note?: string;

  @property({
    type: 'string',
    default: "",
  })
  at_verify?: string;

  @property({
    type: 'string',
    default: "",
  })
  doc_ref?: string;

  @property({
    type: 'string',
    default: "",
  })
  invoice_id?: string;

  @property({
    type: 'string',
    default: "NEW",
  })
  interface_status?: string;

  @property({
    type: 'string',
    default: "",
    mysql: {
      dataType: 'text',
    }
  })
  error_message?: string;

  @property({
    type: 'string'
  })
  inv_date?: string;

  @property({
    type: 'string'
  })
  nota_number?: string;

  @property({
    type: 'string'
  })
  at_fpjp_generate?: string;

  constructor(data?: Partial<DataFpjp>) {
    super(data);
  }

}

export interface DataFpjpRelations {
  // describe navigational properties here
}

export type DataFpjpWithRelations = DataFpjp & DataFpjpRelations;
