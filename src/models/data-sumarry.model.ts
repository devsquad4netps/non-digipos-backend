import {Entity, model, property} from '@loopback/repository';

@model()
export class DataSumarry extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  dealer_code: string;

  @property({
    type: 'string',
    required: true,
  })
  location: string;

  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'string',
    required: true,
  })
  type_trx: string;

  @property({
    type: 'number',
  })
  paid_in_amount?: number;

  @property({
    type: 'number',
    required: true,
  })
  withdraw_amount: number;

  @property({
    type: 'string',
  })
  reason_type_id: string;

  @property({
    type: 'string',
  })
  reason_type_name?: string;

  @property({
    type: 'number',
    required: true,
  })
  trx_a: number;

  @property({
    type: 'number',
    required: true,
  })
  tot_mdr_a: number;

  @property({
    type: 'number',
    required: true,
  })
  trx_b: number;

  @property({
    type: 'number',
    required: true,
  })
  tot_mdr_b: number;

  @property({
    type: 'number',
    default: 0,
  })
  status?: number;

  @property({
    type: 'string',
  })
  periode?: string;

  @property({
    type: 'string'
  })
  create_at?: string;

  @property({
    type: 'string'
  })
  code_ad?: string;

  constructor(data?: Partial<DataSumarry>) {
    super(data);
  }

}

export interface DataSumarryRelations {
  // describe navigational properties here
}

export type DataSumarryWithRelations = DataSumarry & DataSumarryRelations;
