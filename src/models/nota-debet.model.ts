import {Entity, model, property} from '@loopback/repository';

@model()
export class NotaDebet extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  nota_debet_id?: number;

  @property({
    type: 'string',
  })
  fpjp_number: string;

  @property({
    type: 'string',
    required: true,
  })
  periode: string;

  @property({
    type: 'string',
    required: true,
  })
  nota_date: string;

  @property({
    type: 'string',
    required: true
  })
  nota_number: string;

  @property({
    type: 'string',
    required: true,
  })
  bast_number: string;

  @property({
    type: 'string',
    required: true,
  })
  perjanjian_number: string;

  @property({
    type: 'number',
    mysql: {
      dataType: 'float',
      precision: 20,
      scale: 4
    }
  })
  total_price: number;

  @property({
    type: 'string',
    required: true,
  })
  ad_code: string;

  @property({
    type: 'number',
    default: 0,
  })
  nota_state?: number;

  @property({
    type: 'string',
    required: true,
  })
  type_ad: string;

  @property({
    type: 'number',
    default: 0
  })
  acc_ba: number;

  @property({
    type: 'number',
    default: 0
  })
  acc_bu: number;

  @property({
    type: 'number',
    default: 0
  })
  req_acc_fpjp: number;

  @property({
    type: 'number',
    default: 0
  })
  generate_fpjp: number;

  @property({
    type: 'string',
  })
  file_fpjp: string;



  constructor(data?: Partial<NotaDebet>) {
    super(data);
  }


}

export interface NotaDebetRelations {
  // describe navigational properties here
}

export type NotaDebetWithRelations = NotaDebet & NotaDebetRelations;
