import {Entity, model, property} from '@loopback/repository';

@model()
export class AlertPortalAd extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  periode: string;

  @property({
    type: 'string',
  })
  at_update?: string;

  @property({
    type: 'string',
    required: true,
  })
  ad_code: string;

  @property({
    type: 'number',
    default: 0,
  })
  share_bar_a?: number;

  @property({
    type: 'string',
  })
  at_share_bar_a?: string;

  @property({
    type: 'number',
    default: 0,
  })
  share_bar_b?: number;

  @property({
    type: 'string',
  })
  at_share_bar_b?: string;

  @property({
    type: 'number',
  })
  share_inv?: number;

  @property({
    type: 'string',
  })
  at_share_inv?: string;

  @property({
    type: 'number',
    default: 0,
  })
  share_inv_sign?: number;

  @property({
    type: 'string',
  })
  at_share_inv_sign?: string;

  @property({
    type: 'number',
    default: 0,
  })
  share_bukpot?: number;

  @property({
    type: 'string',
  })
  at_share_bukpot?: string;

  @property({
    type: 'number',
    default: 0,
  })
  share_fp?: number;

  @property({
    type: 'string',
  })
  at_share_fp?: string;

  @property({
    type: 'number',
    default: 0,
  })
  submit_inv?: number;

  @property({
    type: 'number',
    default: 0,
  })
  send_msg_inv_success?: number;

  @property({
    type: 'number',
    default: 0,
  })
  send_msg_failed?: number;

  @property({
    type: 'string',
  })
  at_send_msg_inv?: string;

  @property({
    type: 'number',
    default: 0,
  })
  warning_inv?: number;

  @property({
    type: 'number',
    default: 0,
  })
  level_inv?: number;

  @property({
    type: 'number',
    default: 0,
  })
  submit_nota?: number;

  @property({
    type: 'number',
    default: 0,
  })
  send_msg_nota_success?: number;

  @property({
    type: 'number',
    default: 0,
  })
  send_msg_nota_failed?: number;

  @property({
    type: 'string',
  })
  at_send_msg_nota?: string;

  @property({
    type: 'number',
    default: 0,
  })
  warning_nota?: number;

  @property({
    type: 'number',
    default: 0,
  })
  level_nota?: number;


  constructor(data?: Partial<AlertPortalAd>) {
    super(data);
  }
}

export interface AlertPortalAdRelations {
  // describe navigational properties here
}

export type AlertPortalAdWithRelations = AlertPortalAd & AlertPortalAdRelations;
