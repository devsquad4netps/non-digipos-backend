import {Entity, model, property} from '@loopback/repository';

@model()
export class NoFakturPajak extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  no_faktur_pajak: string;

  @property({
    type: 'number',
    required: true,
  })
  number_fp: number;

  @property({
    type: 'string',
    required: true,
  })
  periode: string;

  @property({
    type: 'string'
  })
  at_create: string;

  @property({
    type: 'string',
    required: true,
  })
  remarks: string;

  @property({
    type: 'number',
  })
  fp_state?: number;

  @property({
    type: 'boolean',
  })
  fp_buffer?: boolean;

  @property({
    type: 'string',
    required: true,
  })
  fp_quota_alocate: string;

  @property({
    type: 'string',
  })
  no_invoice_link: string;

  @property({
    type: 'string',
  })
  no_faktur_link?: string;

  @property({
    type: 'number',
    default: 1
  })
  type_faktur?: number;

  @property({
    type: 'string',
  })
  description?: string;

  @property({
    type: 'string',
  })
  at_used?: string;






  constructor(data?: Partial<NoFakturPajak>) {
    super(data);
  }
}

export interface NoFakturPajakRelations {
  // describe navigational properties here
}

export type NoFakturPajakWithRelations = NoFakturPajak & NoFakturPajakRelations;
