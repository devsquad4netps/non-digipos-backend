import {Entity, model, property} from '@loopback/repository';

@model({
  name: 'SYCHRONIZE_PROCESS_LOG'
})
export class SycronizeProcessLog extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  SPL_ID?: number;

  @property({
    type: 'string',
    required: true,
  })
  SPL_PERIODE: string;

  @property({
    type: 'number',
    required: true,
  })
  M_ID: number;

  @property({
    type: 'number',
    required: true,
  })
  MP_ID: number;

  @property({
    type: 'number',
    required: true,
  })
  MPG_ID: number;

  @property({
    type: 'number',
    default: 0,
  })
  SPL_COUNT_TRX?: number;

  @property({
    type: 'number',
    default: 0,
  })
  SPL_TRX_AMOUNT?: number;

  @property({
    type: 'number',
    default: 0,
  })
  SPL_MDR?: number;

  @property({
    type: 'string',
    mysql: {
      dataType: 'text',
    }
  })
  SPL_MESSAGE?: string;

  @property({
    type: 'number',
    default: 0,
  })
  SPL_PROCESS_COUNT?: number;

  @property({
    type: 'number',
    default: 0,
  })
  SPL_PROCESS_NOW?: number;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 35,
    }
  })
  AT_CREATE?: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 35,
    }
  })
  AT_UPDATE?: string;

  @property({
    type: 'number',
    default: 1,
  })
  AT_FLAG?: number;

  @property({
    type: 'string',
  })
  AT_WHO?: string;

  constructor(data?: Partial<SycronizeProcessLog>) {
    super(data);
  }
}

export interface SycronizeProcessLogRelations {
  // describe navigational properties here
}

export type SycronizeProcessLogWithRelations = SycronizeProcessLog & SycronizeProcessLogRelations;
