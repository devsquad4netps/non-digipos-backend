import {Entity, model, property} from '@loopback/repository';

@model()
export class TransaksiCalculation extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  ID?: number;

  @property({
    type: 'string',
    required: true,
  })
  PERIODE: string;

  @property({
    type: 'number',
    required: true,
  })
  TYPE_TRX: number;

  @property({
    type: 'number',
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  BOBOT_A?: number;

  @property({
    type: 'number',
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  BOBOT_DPP_A?: number;

  @property({
    type: 'number',
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  BOBOT_PPN_A?: number;

  @property({
    type: 'number',
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  BOBOT_PPH23_A?: number;

  @property({
    type: 'number',
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  COUNT_AD_A?: number;

  @property({
    type: 'number',
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  AMOUNT_AD_A?: number;

  @property({
    type: 'number',
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  BOBOT_B?: number;

  @property({
    type: 'number',
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  BOBOT_DPP_B?: number;

  @property({
    type: 'number',
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  BOBOT_PPN_B?: number;

  @property({
    type: 'number',
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  BOBOT_PPH23_B?: number;

  @property({
    type: 'number',
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  COUNT_AD_B?: number;

  @property({
    type: 'number',
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  AMOUNT_AD_B?: number;

  @property({
    type: 'string',
  })
  AT_CREATE?: string;

  @property({
    type: 'string',
  })
  AT_UPDATE?: string;

  @property({
    type: 'string',
  })
  AT_USER?: string;


  constructor(data?: Partial<TransaksiCalculation>) {
    super(data);
  }
}

export interface TransaksiCalculationRelations {
  // describe navigational properties here
}

export type TransaksiCalculationWithRelations = TransaksiCalculation & TransaksiCalculationRelations;
