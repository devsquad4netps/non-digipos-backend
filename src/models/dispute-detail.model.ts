import {Entity, model, property} from '@loopback/repository';

@model()
export class DisputeDetail extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  dispute_detail_id?: number;

  @property({
    type: 'number',
    required: true,
  })
  ad_cluster_id: number;

  @property({
    type: 'string',
    required: true,
  })
  ad_cluster_name: string;

  @property({
    type: 'string',
    required: true,
  })
  periode: string;

  @property({
    type: 'number',
    required: true,
  })
  sales_fee: number;

  @property({
    type: 'number',
    required: true,
  })
  trx_a: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 3
    }
  })
  tot_mdr_a: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 3
    }
  })
  dpp_fee_amount: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 3
    }
  })
  ppn_fee_amount: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 3
    }
  })
  pph_fee_amount: number;

  @property({
    type: 'number',
    required: true,
  })
  trx_b: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 3
    }
  })
  tot_mdr_b: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 3
    }
  })
  dpp_paid_amount: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 3
    }
  })
  ppn_paid_amount: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 3
    }
  })
  pph_paid_amount: number;

  @property({
    type: 'number',
    required: true,
  })
  dispute_id: number;


  constructor(data?: Partial<DisputeDetail>) {
    super(data);
  }
}

export interface DisputeDetailRelations {
  // describe navigational properties here
}

export type DisputeDetailWithRelations = DisputeDetail & DisputeDetailRelations;
