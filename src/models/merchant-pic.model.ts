import {Entity, model, property} from '@loopback/repository';

@model({
  name: 'MERCHANT_PIC'
})
export class MerchantPic extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  ID?: number;

  @property({
    type: 'string',
    required: true,
  })
  PIC_NAME: string;

  @property({
    type: 'string',
    required: true,
  })
  PIC_PHONE: string;

  @property({
    type: 'string',
    required: true,
  })
  PIC_JABATAN: string;

  @property({
    type: 'string',
    required: true,
  })
  PIC_EMAIL: string;

  @property({
    type: 'number',
    required: true,
  })
  M_ID?: number;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 35,
    }
  })
  AT_CREATE?: string;


  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 35,
    }
  })
  AT_UPDATE?: string;

  @property({
    type: 'number',
    default: 1,
  })
  AT_FLAG?: number;

  @property({
    type: 'string',
  })
  AT_WHO?: string;

  constructor(data?: Partial<MerchantPic>) {
    super(data);
  }
}

export interface MerchantPicRelations {
  // describe navigational properties here
}

export type MerchantPicWithRelations = MerchantPic & MerchantPicRelations;
