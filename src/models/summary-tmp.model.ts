import {Entity, model, property} from '@loopback/repository';

@model()
export class SummaryTmp extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id: number;

  @property({
    type: 'string',
    required: true,
  })
  dealer_code: string;

  @property({
    type: 'string',
    required: true,
  })
  location: string;

  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'string',
    required: true,
  })
  type_trx: string;

  @property({
    type: 'number',
  })
  paid_in_amount?: number;

  @property({
    type: 'number',
    required: true,
  })
  withdraw_amount: number;

  @property({
    type: 'number',
    required: true,
  })
  reason_type_id: number;

  @property({
    type: 'string',
  })
  reason_type_name?: string;

  @property({
    type: 'number',
    required: true,
  })
  trx_a: number;

  @property({
    type: 'number',
    required: true,
  })
  tot_mdr_a: number;

  @property({
    type: 'number',
    required: true,
  })
  trx_b: number;

  @property({
    type: 'number',
    required: true,
  })
  tot_mdr_b: number;

  @property({
    type: 'number',
    default: 0,
  })
  status?: number;

  @property({
    type: 'string',
    required: true,
  })
  runnig_group: string;

  @property({
    type: 'date',
    required: true,
    jsonSchema: {
      format: 'date',
    },
    mysql: {
      "dataType": "date",
    }
  })
  runnig_date: string;

  constructor(data?: Partial<SummaryTmp>) {
    super(data);
  }

}

export interface SummaryTmpRelations {
  // describe navigational properties here
}

export type SummaryTmpWithRelations = SummaryTmp & SummaryTmpRelations;
