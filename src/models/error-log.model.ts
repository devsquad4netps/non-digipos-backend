import {Entity, model, property} from '@loopback/repository';

@model()
export class ErrorLog extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  error_log_id?: number;

  @property({
    type: 'string',
  })
  create_at?: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'text'
    }
  })
  error_log_message?: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'longtext'
    }
  })
  error_log_info?: string;

  @property({
    type: 'string',
  })
  error_log_type?: string;

  @property({
    type: 'number',
  })
  error_log_source?: number;


  constructor(data?: Partial<ErrorLog>) {
    super(data);
  }
}

export interface ErrorLogRelations {
  // describe navigational properties here
}

export type ErrorLogWithRelations = ErrorLog & ErrorLogRelations;
