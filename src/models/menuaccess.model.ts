import {Entity, model, property} from '@loopback/repository';

@model({settings: {strict: false}})

export class Menuaccess extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'number',
    required: true,
  })
  MenuId: number;

  @property({
    type: 'number',
    required: true,
  })
  RoleId: number;

  @property({
    type: 'string',
    required: true,
  })
  Action: string;


  constructor(data?: Partial<Menuaccess>) {
    super(data);
  }
}

export interface MenuaccessRelations {
  // describe navigational properties here
}

export type MenuaccessWithRelations = Menuaccess & MenuaccessRelations;
