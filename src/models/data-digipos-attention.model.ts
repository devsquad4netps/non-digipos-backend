import {Entity, model, property} from '@loopback/repository';

@model()
export class DataDigiposAttention extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  attention_id?: number;

  @property({
    type: 'string',
    required: true,
  })
  attention_name: string;

  @property({
    type: 'string',
    required: true,
  })
  attention_position: string;

  @property({
    type: 'string',
    required: true,
  })
  phone_number: string;

  @property({
    type: 'string',
    format: 'email',
    required: true,
  })
  email: string;

  @property({
    type: 'string',
    required: true,
  })
  company: string;

  @property({
    type: 'string',
    required: true,
  })
  npwp: string;

  @property({
    type: 'string',
    required: true,
  })
  address: string;

  @property({
    type: 'string',
    required: true,
  })
  account_number: string;

  @property({
    type: 'string',
    required: true,
  })
  bank_name: string;

  @property({
    type: 'string',
    required: true,
  })
  brach: string;

  @property({
    type: 'string',
    required: true,
  })
  sign_attention: string;

  @property({
    type: 'boolean',
    default: false,
  })
  set_attention?: boolean;

  @property({
    type: 'number',
    default: 0,
  })
  sign_state: number;

  @property({
    type: 'number',
    required: true,
  })
  group: number;

  constructor(data?: Partial<DataDigiposAttention>) {
    super(data);
  }
}

export interface DataDigiposAttentionRelations {
  // describe navigational properties here
}

export type DataDigiposAttentionWithRelations = DataDigiposAttention & DataDigiposAttentionRelations;
