import {Entity, model, property} from '@loopback/repository';

@model()
export class DataDigiposDetail extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  digipos_detail_id?: number;

  @property({
    type: 'string',
  })
  trx_date_group: string;

  @property({
    type: 'string',
    required: true,
  })
  ad_code: string;

  @property({
    type: 'string',
    required: true,
  })
  ad_name: string;

  @property({
    type: 'string',
    required: true,
  })
  type_trx: string;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  sales_fee: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  sales_paid: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  trx_cluster_out: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  mdr_fee_amount: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  dpp_fee_amount: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  vat_fee_amount: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  total_fee_amount: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  pph_fee_amount: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  trx_paid_invoice: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  paid_inv_amount: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  inv_adj_amount: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  dpp_paid_amount: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  vat_paid_amount: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  total_paid_amount: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  pph_paid_amount: number;

  @property({
    type: 'number',
  })
  digipost_id?: number;

  constructor(data?: Partial<DataDigiposDetail>) {
    super(data);
  }
}

export interface DataDigiposDetailRelations {
  // describe navigational properties here
}

export type DataDigiposDetailWithRelations = DataDigiposDetail & DataDigiposDetailRelations;
