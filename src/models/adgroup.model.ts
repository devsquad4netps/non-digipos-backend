import {Entity, model, property} from '@loopback/repository';

@model()
export class Adgroup extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  ad_code: string;

  @property({
    type: 'string',
    required: true,
  })
  dealer_code: string;

  @property({
    type: 'string',
    required: true,
  })
  ad_name: string;


  constructor(data?: Partial<Adgroup>) {
    super(data);
  }
}

export interface AdgroupRelations {
  // describe navigational properties here
}

export type AdgroupWithRelations = Adgroup & AdgroupRelations;
