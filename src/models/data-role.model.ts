import {Entity, model, property} from '@loopback/repository';

@model()
export class DataRole extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'date',
  })
  created?: string;

  @property({
    type: 'string',
  })
  modified?: string;



  constructor(data?: Partial<DataRole>) {
    super(data);
  }
}

export interface DataRoleRelations {
  // describe navigational properties here
}

export type DataRoleWithRelations = DataRole & DataRoleRelations;
