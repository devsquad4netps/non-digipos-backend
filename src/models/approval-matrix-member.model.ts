import {Entity, model, property} from '@loopback/repository';

@model()
export class ApprovalMatrixMember extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  amm_id?: number;

  @property({
    type: 'string',
    required: true,
  })
  amm_userid: string;

  @property({
    type: 'number',
    required: true,
  })
  amg_id: number;

  @property({
    type: 'string',
    required: true,
  })
  at_create: string;

  @property({
    type: 'boolean',
    default: false
  })
  amm_state: boolean;

  constructor(data?: Partial<ApprovalMatrixMember>) {
    super(data);
  }
}

export interface ApprovalMatrixMemberRelations {
  // describe navigational properties here
}

export type ApprovalMatrixMemberWithRelations = ApprovalMatrixMember & ApprovalMatrixMemberRelations;
