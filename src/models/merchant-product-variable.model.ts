import {Entity, model, property} from '@loopback/repository';

@model({
  name: 'MERCHANT_PRODUCT_VARIABLE'
})
export class MerchantProductVariable extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  MPV_ID?: number;

  @property({
    type: 'string',
    required: true,
    mysql: {
      dataType: 'varchar',
      dataLength: 250,
    }
  })
  MPV_KEY: string;

  @property({
    type: 'string',
    required: true,
    mysql: {
      dataType: 'varchar',
      dataLength: 512,
    }
  })
  MPV_LABEL: string;

  @property({
    type: 'number',
    required: true,
    default: 1
  })
  MPV_TYPE_ID: number;

  @property({
    type: 'number',
    required: true,
    default: 1
  })
  MPV_TYPE_KEY_ID: number;

  @property({
    type: 'number',
    required: true,
  })
  M_ID: number;

  @property({
    type: 'number',
    required: true,
  })
  MP_ID: number;



  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 35,
    }
  })
  AT_CREATE?: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 35,
    }
  })
  AT_UPDATE?: string;

  @property({
    type: 'number',
    default: 1,
  })
  AT_FLAG?: number;

  @property({
    type: 'string',
  })
  AT_WHO?: string;

  constructor(data?: Partial<MerchantProductVariable>) {
    super(data);
  }
}

export interface MerchantProductVariableRelations {
  // describe navigational properties here
}

export type MerchantProductVariableWithRelations = MerchantProductVariable & MerchantProductVariableRelations;
