import {Entity, model, property} from '@loopback/repository';


@model({
  name: 'MERCHANT_PRODUCT_QUERY'
})
export class MerchantProductQuery extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  MPQ_ID?: number;

  @property({
    type: 'string',
    mysql: {
      dataType: 'text',
    }
  })
  MPQ_QUERY?: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'text',
    }
  })
  MPQ_GROUP_SUMMARY?: string;

  @property({
    type: 'number',
    required: true,
  })
  M_ID: number;

  @property({
    type: 'number',
    required: true,
  })
  MP_ID: number;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 35,
    }
  })
  AT_CREATE?: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 35,
    }
  })
  AT_UPDATE?: string;

  @property({
    type: 'number',
    default: 1,
  })
  AT_FLAG?: number;

  @property({
    type: 'string',
  })
  AT_WHO?: string;


  constructor(data?: Partial<MerchantProductQuery>) {
    super(data);
  }
}

export interface MerchantProductQueryRelations {
  // describe navigational properties here
}

export type MerchantProductQueryWithRelations = MerchantProductQuery & MerchantProductQueryRelations;
