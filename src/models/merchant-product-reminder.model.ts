import {Entity, model, property} from '@loopback/repository';


@model({
  name: 'MERCHANT_PRODUCT_REMINDER'
})
export class MerchantProductReminder extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  MPR_ID?: number;

  @property({
    type: 'string',
    required: true,
  })
  MPR_DATETIME: string;

  @property({
    type: 'string',
    required: true,
    mysql: {
      dataType: 'text',
    }
  })
  MPR_NOTE: string;

  @property({
    type: 'string',
  })
  MPR_TO?: string;

  @property({
    type: 'string',
  })
  MPR_CC?: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 35,
    }
  })
  SEND_CREATE: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 35,
    }
  })
  SEND_SUCCESS?: string;

  @property({
    type: 'number',
    default: 0,
  })
  SEND_FLAG?: number;

  @property({
    type: 'string',
    mysql: {
      dataType: 'text',
    }
  })
  SEND_MESSAGE?: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 35,
    }
  })
  AT_CREATE?: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 35,
    }
  })
  AT_UPDATE?: string;

  @property({
    type: 'number',
    default: 1,
  })
  AT_FLAG?: number;

  @property({
    type: 'string',
  })
  AT_WHO?: string;


  constructor(data?: Partial<MerchantProductReminder>) {
    super(data);
  }
}

export interface MerchantProductReminderRelations {
  // describe navigational properties here
}

export type MerchantProductReminderWithRelations = MerchantProductReminder & MerchantProductReminderRelations;
