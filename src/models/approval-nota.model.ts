import {Entity, model, property} from '@loopback/repository';

@model()
export class ApprovalNota extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  user_id: string;

  @property({
    type: 'number',
    required: true,
  })
  nota_debet_id: number;

  @property({
    type: 'number',
    required: true,
  })
  state: number;

  @property({
    type: 'string',
  })
  at_create: string;

  @property({
    type: 'string',
  })
  at_update: string;

  @property({
    type: 'number',
    required: true,
  })
  ap_level: number;

  @property({
    type: 'string',
    default: ""
  })
  code_rand: string;



  constructor(data?: Partial<ApprovalNota>) {
    super(data);
  }
}

export interface ApprovalNotaRelations {
  // describe navigational properties here
}

export type ApprovalNotaWithRelations = ApprovalNota & ApprovalNotaRelations;
