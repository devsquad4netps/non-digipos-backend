import {Entity, model, property} from '@loopback/repository';

@model({
  name: 'CUSTOMER_P2P'
})
export class CustomerP2P extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  ID?: number;

  @property({
    type: 'string',
    required: true,
    unique: true,
  })
  CODE: string;

  @property({
    type: 'string',
    required: true,
    unique: true,
  })
  NAME: string;

  @property({
    type: 'string',
    required: true,
    unique: true,
  })
  ACCOUNT_DES: string;

  @property({
    type: 'string',
    required: true,
    unique: true,
  })
  LEGAL_NAME: string;

  @property({
    type: 'string',
    required: true,
  })
  CUSTOMER_CLASS: string;

  @property({
    type: 'string',
  })
  CUSTOMER_SOURCE: string;

  @property({
    type: 'string',
  })
  NPWP: string;

  @property({
    type: 'string',
  })
  PERFIX: string;

  @property({
    type: 'string',
  })
  SITE_NAME: string;

  @property({
    type: 'string',
  })
  ADDRESS: string;

  @property({
    type: 'string',
  })
  ADDRESS_NPWP: string;

  @property({
    type: 'string',
  })
  CITY: string;

  @property({
    type: 'string',
  })
  PROVINSI: string;

  @property({
    type: 'string',
  })
  POSTAL_CODE: string;

  @property({
    type: 'string',
  })
  CONTRY: string;

  @property({
    type: 'string',
  })
  BUSINES: string;

  @property({
    type: 'string',
  })
  BULL_TO: string;

  constructor(data?: Partial<CustomerP2P>) {
    super(data);
  }
}

export interface CustomerP2PRelations {
  // describe navigational properties here
}

export type CustomerP2PWithRelations = CustomerP2P & CustomerP2PRelations;
