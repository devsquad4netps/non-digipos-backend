import {Entity, model, property} from '@loopback/repository';

@model()
export class AdCluster extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: false,
    required: true,
  })
  ad_cluster_id: string;

  @property({
    type: 'string',
    required: true,
  })
  ad_cluster_name: string;

  @property({
    type: 'string',
    required: true,
  })
  ad_name: string;

  @property({
    type: 'string',
    required: true,
  })
  ad_code: string;

  @property({
    type: 'string',
  })
  remarks: string;

  @property({
    type: 'string',
  })
  end_date_settelment: string;

  @property({
    type: 'string',
  })
  start_date_settelment: string;

  @property({
    type: 'number',
    default: 1
  })
  adc_flag: number;


  constructor(data?: Partial<AdCluster>) {
    super(data);
  }
}

export interface AdClusterRelations {
  // describe navigational properties here
}

export type AdClusterWithRelations = AdCluster & AdClusterRelations;
