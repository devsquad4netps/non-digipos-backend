import {Entity, model, property} from '@loopback/repository';

@model({
  name: 'MERCHANT_PRODUCT_MASTER'
})
export class MerchantProductMaster extends Entity {

  @property({
    type: 'string',
    id: true,
    generated: false,
    defaultFn: 'uuidv4',
  })
  MPM_ID?: string;

  @property({
    type: 'string',
    required: true,
    mysql: {
      dataType: 'varchar',
      dataLength: 10,
    }
  })
  PERIODE: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 150,
    }
  })
  MPM_NO_BAR: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 150,
    }
  })
  MPM_NO_INVOICE: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 150,
    }
  })
  MPM_NO_FAKTUR: string;

  @property({
    type: 'number',
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  MPM_COUNT_TRX: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  MPM_AMOUNT: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  MPM_MDR: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  MPM_DPP: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  MPM_PPN: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  MPM_PPH: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  MPM_TOTAL: number;

  @property({
    type: 'number',
    default: 0
  })
  GENERATE_BAR: number;

  @property({
    type: 'number',
    default: 0
  })
  GENERATE_BAR_SIGN: number;

  @property({
    type: 'number',
    default: 0
  })
  GENERATE_INVOICE: number;

  @property({
    type: 'number',
    default: 0
  })
  GENERATE_INVOICE_SIGN: number;

  @property({
    type: 'number',
    default: 0
  })
  GENERATE_FAKTUR_PAJAK: number;

  @property({
    type: 'number',
    default: 0
  })
  GENERATE_BUKPOT: number;

  @property({
    type: 'number',
    default: 0
  })
  GENERATE_CSV_FAKTUR: number;

  @property({
    type: 'number',
    required: true,
  })
  M_ID: number;

  @property({
    type: 'number',
    required: true,
  })
  MP_ID: number;

  @property({
    type: 'number',
    default: 0
  })
  IS_SELISIH: number;

  @property({
    type: 'string',
    required: true,
    mysql: {
      dataType: 'TEXT',
    }
  })
  START_TIME_RUN: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 35,
    }
  })
  AT_CREATE?: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 35,
    }
  })
  AT_UPDATE?: string;

  @property({
    type: 'number',
    default: 1,
  })
  AT_FLAG?: number;

  @property({
    type: 'number',
    default: 1,
  })
  AT_POSISI?: number;

  @property({
    type: 'number',
    default: 0,
  })
  HAS_AR_REJECTED?: number;

  @property({
    type: 'number',
    default: 0,
  })
  WAITING_SEND_MAIL?: number;

  @property({
    type: 'string',
  })
  AT_WHO?: string;

  constructor(data?: Partial<MerchantProductMaster>) {
    super(data);
  }
}

export interface MerchantProductMasterRelations {
  // describe navigational properties here
}

export type MerchantProductMasterWithRelations = MerchantProductMaster & MerchantProductMasterRelations;
