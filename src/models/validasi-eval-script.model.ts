import {Entity, model, property} from '@loopback/repository';

@model()
export class ValidasiEvalScript extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  ID?: number;

  @property({
    type: 'string',
    required: true,
  })
  SCRIPTING: string;

  @property({
    type: 'number',
  })
  AT_FLAG?: number;

  @property({
    type: 'string',
  })
  AT_CREATE?: string;

  @property({
    type: 'string',
  })
  AT_UPDATE?: string;

  @property({
    type: 'string',
    required: true,
  })
  AT_WHO: string;

  @property({
    type: 'number',
  })
  MPS_ID?: number;


  constructor(data?: Partial<ValidasiEvalScript>) {
    super(data);
  }
}

export interface ValidasiEvalScriptRelations {
  // describe navigational properties here
}

export type ValidasiEvalScriptWithRelations = ValidasiEvalScript & ValidasiEvalScriptRelations;
