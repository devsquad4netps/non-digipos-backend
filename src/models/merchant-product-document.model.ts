import {Entity, model, property} from '@loopback/repository';

@model({
  name: 'MERCHANT_PRODUCT_DOCUMENT'
})
export class MerchantProductDocument extends Entity {

  @property({
    type: 'string',
    id: true,
    generated: false,
    defaultFn: 'uuidv4'
  })
  MPDOC_ID?: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'TEXT',
    }
  })
  MPDOC_NAME: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'TEXT',
    }
  })
  MPDOC_PATH: string;

  @property({
    type: 'number',
    default: 0
  })
  MPDOC_TYPE: number;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 512,
    }
  })
  MPM_ID?: string;

  @property({
    type: 'string',
    required: true,
    mysql: {
      dataType: 'TEXT',
    }
  })
  START_TIME_RUN: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 35,
    }
  })
  AT_CREATE?: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 35,
    }
  })
  AT_UPDATE?: string;

  @property({
    type: 'number',
    default: 1,
  })
  AT_FLAG?: number;

  @property({
    type: 'string',
  })
  AT_WHO?: string;

  constructor(data?: Partial<MerchantProductDocument>) {
    super(data);
  }
}

export interface MerchantProductDocumentRelations {
  // describe navigational properties here
}

export type MerchantProductDocumentWithRelations = MerchantProductDocument & MerchantProductDocumentRelations;
