import {Entity, model, property} from '@loopback/repository';

@model({
  name: 'MERCHANT_PRODUCT_GROUP_MDR'
})
export class MerchantProductMdr extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  MDR_ID?: number;

  @property({
    type: 'number',
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  MDR?: number;

  @property({
    type: 'number',
    required: true,
  })
  MDR_TYPE: number;

  @property({
    type: 'number',
    required: true,
  })
  M_ID: number;

  @property({
    type: 'number',
    required: true,
  })
  MP_ID: number;

  @property({
    type: 'number',
    required: true,
  })
  MPG_ID: number;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 35,
    }
  })
  AT_CREATE?: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 35,
    }
  })
  AT_UPDATE?: string;

  @property({
    type: 'number',
    default: 1,
  })
  AT_FLAG?: number;

  @property({
    type: 'string',
  })
  AT_WHO?: string;

  constructor(data?: Partial<MerchantProductMdr>) {
    super(data);
  }
}

export interface MerchantProductMdrRelations {
  // describe navigational properties here
}

export type MerchantProductMdrWithRelations = MerchantProductMdr & MerchantProductMdrRelations;
