import {Entity, model, property} from '@loopback/repository';

@model()
export class ApprovalMatrix extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  am_id?: number;

  @property({
    type: 'string',
  })
  am_label?: string;

  @property({
    type: 'number',
  })
  amg_id_root?: number;

  @property({
    type: 'number',
  })
  amg_id_level1?: number;

  @property({
    type: 'number',
  })
  amg_id_level2?: number;

  @property({
    type: 'string',
    required: true,
  })
  amd_operator: string;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'float',
      precision: 20,
      scale: 4
    }
  })
  amd_value: number;

  @property({
    type: 'string',
    required: true
  })
  amd_condition: string;

  @property({
    type: 'string',
    required: true,
  })
  amd_operator_2: string;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'float',
      precision: 20,
      scale: 4
    }
  })
  amd_value_2: number;

  @property({
    type: 'number',
  })
  amd_index: number;


  // @property({
  //   type: 'string',
  // })
  // amd_satuan: string;

  constructor(data?: Partial<ApprovalMatrix>) {
    super(data);
  }

}

export interface ApprovalMatrixRelations {
  // describe navigational properties here
}

export type ApprovalMatrixWithRelations = ApprovalMatrix & ApprovalMatrixRelations;
