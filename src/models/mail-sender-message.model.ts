import {Entity, model, property} from '@loopback/repository';

@model({
  name: 'MailSenderMessage'
})
export class MailSenderMessage extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  MSM_ID?: number;

  @property({
    type: 'string',
  })
  AT_CREATE?: string;

  @property({
    type: 'string',
  })
  AT_UPDATE?: string;

  @property({
    type: 'string',
  })
  AT_SEND?: string;

  @property({
    type: 'number',
    default: 0,
  })
  FLAG_SEND?: number;

  @property({
    type: 'string',
  })
  SENDER_TO?: string;

  @property({
    type: 'string',
  })
  MESSAGE?: string;

  @property({
    type: 'string',
  })
  MESSAGE_FOR?: string;

  @property({
    type: 'string',
  })
  SOURCE_DATA?: string;


  constructor(data?: Partial<MailSenderMessage>) {
    super(data);
  }
}

export interface MailSenderMessageRelations {
  // describe navigational properties here
}

export type MailSenderMessageWithRelations = MailSenderMessage & MailSenderMessageRelations;
