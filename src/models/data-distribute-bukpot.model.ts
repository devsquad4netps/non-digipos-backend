import {Entity, model, property} from '@loopback/repository';

@model()
export class DataDistributeBukpot extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  bukpot_id?: number;

  @property({
    type: 'string',
    required: true,
  })
  ad_code: string;


  @property({
    type: 'string',
    required: true,
  })
  ad_name: string;

  @property({
    type: 'string',
    required: true,
  })
  npwp: string;

  @property({
    type: 'string',
    required: true,
  })
  periode: string;

  @property({
    type: 'string',
  })
  bukpot_attachment_set?: string;

  @property({
    type: 'string',
  })
  bukpot_attachment?: string;

  @property({
    type: 'number',
    default: 0,
  })
  bukpot_status?: number;

  @property({
    type: 'string',
  })
  at_process?: string;

  @property({
    type: 'string',
  })
  at_distribute?: string;

  @property({
    type: 'string',
  })
  by_process?: string;


  constructor(data?: Partial<DataDistributeBukpot>) {
    super(data);
  }
}

export interface DataDistributeBukpotRelations {
  // describe navigational properties here
}

export type DataDistributeBukpotWithRelations = DataDistributeBukpot & DataDistributeBukpotRelations;
