import {Entity, model, property} from '@loopback/repository';

@model()
export class FileSycronize extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  file_syc_id?: number;

  @property({
    type: 'string',
    required: true,
  })
  periode: string;

  @property({
    type: 'string',
    required: true,
  })
  file_name: string;

  @property({
    type: 'number',
    default: 0,
  })
  mitra_ad?: number;

  @property({
    type: 'number',
    default: 0,
  })
  ad?: number;

  @property({
    type: 'number',
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    },
    default: 0,
  })
  total_fee?: number;

  @property({
    type: 'number',
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    },
    default: 0,
  })
  total_paid?: number;

  @property({
    type: 'number',
    required: true,
  })
  process_total: number;

  @property({
    type: 'number',
    required: true,
  })
  process_count: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  process_percent: number;

  @property({
    type: 'boolean',
    default: false,
  })
  error_state?: boolean;

  @property({
    type: 'string',
  })
  error_attach?: string;

  @property({
    type: 'string',
  })
  error_message?: string;

  @property({
    type: 'number',
    default: 1,
  })
  code_state?: number;

  @property({
    type: 'string',
  })
  create_at?: string;

  @property({
    type: 'string'
  })
  end_at?: string;

  @property({
    type: 'number',
    default: 0,
  })
  state?: number;

  @property({
    type: 'string',
  })
  type_excel?: string;



  constructor(data?: Partial<FileSycronize>) {
    super(data);
  }
}

export interface FileSycronizeRelations {
  // describe navigational properties here
}

export type FileSycronizeWithRelations = FileSycronize & FileSycronizeRelations;
