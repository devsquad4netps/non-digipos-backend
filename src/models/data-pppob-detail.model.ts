import {Entity, model, property} from '@loopback/repository';

@model()
export class DataPppobDetail extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: false,
    defaultFn: 'uuidv4',
  })
  id?: string;

  @property({
    type: 'string',
    required: true,
  })
  periode: string;


  @property({
    type: 'string',
  })
  ddate?: string;

  @property({
    type: 'string',
  })
  orderid?: string;

  @property({
    type: 'number',
    default: 0,
  })
  count_trx?: number;

  @property({
    type: 'string',
  })
  billrefnumber?: string;

  @property({
    type: 'string',
  })
  reason_type_id?: string;

  @property({
    type: 'string',
  })
  reason_type_name?: string;

  @property({
    type: 'string',
  })
  dealer_code?: string;

  @property({
    type: 'string',
  })
  location?: string;

  @property({
    type: 'string',
  })
  entity_name?: string;

  @property({
    type: 'number',
    default: 0,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  trx_amount?: number;

  @property({
    type: 'string',
  })
  shortcode?: string;

  @property({
    type: 'string',
  })
  services?: string;

  @property({
    type: 'string',
  })
  reseller?: string;

  @property({
    type: 'string',
  })
  entity_name_reseler?: string;

  @property({
    type: 'number',
    default: 0,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  biller?: number;

  @property({
    type: 'number',
    default: 0,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  finarya?: number;

  @property({
    type: 'number',
    default: 0,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  tsel?: number;

  @property({
    type: 'number',
    default: 0,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  ad?: number;

  @property({
    type: 'number',
    default: 0,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  net_finarya?: number;

  @property({
    type: 'string'
  })
  create_at?: string;

  @property({
    type: 'number'
  })
  state?: number;

  @property({
    type: 'number'
  })
  pos_index?: number;

  @property({
    type: 'string'
  })
  code_ad?: string;

  constructor(data?: Partial<DataPppobDetail>) {
    super(data);
  }
}

export interface DataPppobDetailRelations {
  // describe navigational properties here
}

export type DataPppobDetailWithRelations = DataPppobDetail & DataPppobDetailRelations;
