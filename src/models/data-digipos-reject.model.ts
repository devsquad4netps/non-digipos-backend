import {Entity, model, property} from '@loopback/repository';

@model()
export class DataDigiposReject extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  reject_id?: number;

  @property({
    type: 'string',
    required: true,
  })
  periode: string;

  @property({
    type: 'string',
    required: true,
  })
  ad_code: string;

  @property({
    type: 'string',
    required: true,
  })
  ad_name: string;

  @property({
    type: 'number',
  })
  config_id?: number;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 7
    }
  })
  reject_code?: string;

  @property({
    type: 'string',
    required: true,
  })
  reject_label: string;

  @property({
    type: 'string',
    required: true,
  })
  reject_description: string;

  @property({
    type: 'date',
    defaultFn: "now"
  })
  create_at: string;

  @property({
    type: 'string',
    required: true,
  })
  user_reject: string;

  @property({
    type: 'number',
  })
  digipos_id: number;


  constructor(data?: Partial<DataDigiposReject>) {
    super(data);
  }
}

export interface DataDigiposRejectRelations {
  // describe navigational properties here
}

export type DataDigiposRejectWithRelations = DataDigiposReject & DataDigiposRejectRelations;
