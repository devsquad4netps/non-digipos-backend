import {Entity, model, property} from '@loopback/repository';

@model()
export class DataDigiposAttentionGroup extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  name_label: string;


  constructor(data?: Partial<DataDigiposAttentionGroup>) {
    super(data);
  }
}

export interface DataDigiposAttentionGroupRelations {
  // describe navigational properties here
}

export type DataDigiposAttentionGroupWithRelations = DataDigiposAttentionGroup & DataDigiposAttentionGroupRelations;
