import {Entity, model, property} from '@loopback/repository';

@model()
export class ApprovalMatrixDetail extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  amd_id?: number;

  @property({
    type: 'string',
    required: true,
  })
  amd_operator: string;

  @property({
    type: 'number',
    required: true,
  })
  amd_value: number;

  @property({
    type: 'number',
    default: 0,
  })
  amd_group?: number;

  @property({
    type: 'number',
    required: true,
  })
  am_id: number;

  constructor(data?: Partial<ApprovalMatrixDetail>) {
    super(data);
  }
}

export interface ApprovalMatrixDetailRelations {
  // describe navigational properties here
}

export type ApprovalMatrixDetailWithRelations = ApprovalMatrixDetail & ApprovalMatrixDetailRelations;
