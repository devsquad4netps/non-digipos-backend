import {Entity, model, property} from '@loopback/repository';

@model({
  name: 'MERCHANT_PRODUCT_DATASTORE'
})
export class MerchantProductDatastore extends Entity {

  @property({
    type: 'string',
    id: true,
    generated: false,
    defaultFn: 'uuidv4',
  })
  MPD_ID?: string;

  @property({
    type: 'string',
    required: true,
    mysql: {
      dataType: 'varchar',
      dataLength: 10,
    }
  })
  PERIODE: string;

  @property({
    type: 'string',
    required: true,
    mysql: {
      dataType: 'varchar',
      dataLength: 520,
    }
  })
  DATA_ID: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'TEXT',
    }
  })
  MPD_VALUE: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'TEXT',
    }
  })
  MPD_TEXT: string;

  @property({
    type: 'string',
    required: true,
    mysql: {
      dataType: 'text',
    }
  })
  START_TIME_RUN: string;

  @property({
    type: 'number',
  })
  SPL_ID: number;

  @property({
    type: 'number',
    required: true,
  })
  M_ID: number;

  @property({
    type: 'number',
    required: true,
  })
  MP_ID: number;

  @property({
    type: 'number',
    required: true,
  })
  MPG_ID: number;

  @property({
    type: 'number',
    required: true,
  })
  MPV_ID: number;

  @property({
    type: 'number',
  })
  TYPE_DATA?: number;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 35,
    }
  })
  AT_CREATE?: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 35,
    }
  })
  AT_UPDATE?: string;

  @property({
    type: 'number',
    default: 1,
  })
  AT_FLAG?: number;

  @property({
    type: 'string',
  })
  AT_WHO?: string;


  constructor(data?: Partial<MerchantProductDatastore>) {
    super(data);
  }
}

export interface MerchantProductDatastoreRelations {
  // describe navigational properties here
}

export type MerchantProductDatastoreWithRelations = MerchantProductDatastore & MerchantProductDatastoreRelations;
