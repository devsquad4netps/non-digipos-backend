import {Entity, model, property} from '@loopback/repository';

@model()
export class DataDisputeDetail extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  dispute_det_id?: number;

  @property({
    type: 'string',
  })
  dealer_code?: string;

  @property({
    type: 'string',
  })
  dealer_name?: string;

  @property({
    type: 'number',
    default: 0,
  })
  sales_fee?: number;

  @property({
    type: 'number',
    default: 0,
  })
  qty_trx?: number;

  @property({
    type: 'number',
    default: 0,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  tot_mdr?: number;

  @property({
    type: 'number',
    default: 0,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  dpp_amount?: number;

  @property({
    type: 'number',
    default: 0,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  ppn_amount?: number;

  @property({
    type: 'number',
    default: 0,
    mysql: {
      dataType: 'decimal',
      dataPrecision: 30,
      dataScale: 2
    }
  })
  pph_amount?: number;

  @property({
    type: 'number',
    required: true,
  })
  dispute_id: number;




  constructor(data?: Partial<DataDisputeDetail>) {
    super(data);
  }
}

export interface DataDisputeDetailRelations {
  // describe navigational properties here
}

export type DataDisputeDetailWithRelations = DataDisputeDetail & DataDisputeDetailRelations;
