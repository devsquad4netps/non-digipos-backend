import {Entity, model, property} from '@loopback/repository';

@model()
export class HistoryFpjpReject extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'number',
    required: true,
  })
  fpjp_id: number;

  @property({
    type: 'number',
  })
  reason_id?: number;

  @property({
    type: 'string',
    required: true,
  })
  message: string;

  @property({
    type: 'string',
    required: true,
  })
  at_reject: string;

  @property({
    type: 'string',
    required: true,
  })
  by_reject: string;

  @property({
    type: 'string',
    required: true,
  })
  by_id_reject: string;


  constructor(data?: Partial<HistoryFpjpReject>) {
    super(data);
  }
}

export interface HistoryFpjpRejectRelations {
  // describe navigational properties here
}

export type HistoryFpjpRejectWithRelations = HistoryFpjpReject & HistoryFpjpRejectRelations;
