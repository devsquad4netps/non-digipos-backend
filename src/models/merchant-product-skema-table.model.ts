import {Entity, model, property} from '@loopback/repository';

@model({
  name: 'MERCHANT_PRODUCT_SKEMA_TABLE'
})
export class MerchantProductSkemaTable extends Entity {

  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  MPST_ID?: number;

  @property({
    type: 'string',
    required: true,
    mysql: {
      dataType: 'varchar',
      dataLength: 100,
    }
  })
  MPST_LABEL: string;

  @property({
    type: 'string',
    required: true,
    mysql: {
      dataType: 'TEXT',
      dataLength: 100,
    }
  })
  MPST_FORMULA: string;

  @property({
    type: 'number',
    default: 1,
  })
  MPST_GROUP?: number;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 35,
    }
  })
  AT_CREATE?: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 35,
    }
  })
  AT_UPDATE?: string;

  @property({
    type: 'number',
    default: 1,
  })
  AT_FLAG?: number;

  @property({
    type: 'string',
  })
  AT_WHO?: string;

  constructor(data?: Partial<MerchantProductSkemaTable>) {
    super(data);
  }
}

export interface MerchantProductSkemaTableRelations {
  // describe navigational properties here
}

export type MerchantProductSkemaTableWithRelations = MerchantProductSkemaTable & MerchantProductSkemaTableRelations;
