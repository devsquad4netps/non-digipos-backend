import {inject, lifeCycleObserver, LifeCycleObserver} from '@loopback/core';
import {juggler} from '@loopback/repository';
require('dotenv').config();

const config = {
  name: 'mysql',
  connector: 'mysql',
  url: '',
  host: process.env.HOST_DB_APP,//'10.14.10.9',//process.env.HOST_DB_APP,
  port: process.env.PORT_DB_APP,//3306,//process.env.PORT_DB_APP,
  user: process.env.USER_DB_APP,//'apps_digipos_dev',//process.env.USER_DB_APP,
  password: process.env.PASS_DB_APP,//'TxRotq0TylHdyjDS',//process.env.PASS_DB_APP,
  database: process.env.DB_APP,//'digipos'//process.env.DB_APP
};


// Observe application's life cycle to disconnect the datasource when
// application is stopped. This allows the application to be shut down
// gracefully. The `stop()` method is inherited from `juggler.DataSource`.
// Learn more at https://loopback.io/doc/en/lb4/Life-cycle.html
@lifeCycleObserver('datasource')
export class MysqlDataSource extends juggler.DataSource
  implements LifeCycleObserver {
  static dataSourceName = 'mysql';
  static readonly defaultConfig = config;

  constructor(
    @inject('datasources.config.mysql', {optional: true})
    dsConfig: object = config,
  ) {
    super(dsConfig);
  }
}
