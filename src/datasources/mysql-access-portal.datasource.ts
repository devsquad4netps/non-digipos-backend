import {inject, lifeCycleObserver, LifeCycleObserver} from '@loopback/core';
import {juggler} from '@loopback/repository';
require('dotenv').config();

const config = {
  name: 'MysqlAccessPortal',
  connector: 'mysql',
  url: '',
  host: process.env.HOST_DB_PORTAL,//'10.14.10.9',
  port: process.env.PORT_DB_PORTAL,//3306,
  user: process.env.USER_DB_PORTAL,//'apps_digipos_dev',
  password: process.env.PASS_DB_PORTAL,//'TxRotq0TylHdyjDS',
  database: process.env.DB_PORTAL//'access_portals'
};


// Observe application's life cycle to disconnect the datasource when
// application is stopped. This allows the application to be shut down
// gracefully. The `stop()` method is inherited from `juggler.DataSource`.
// Learn more at https://loopback.io/doc/en/lb4/Life-cycle.html
@lifeCycleObserver('datasource')
export class MysqlAccessPortalDataSource extends juggler.DataSource
  implements LifeCycleObserver {
  static dataSourceName = 'MysqlAccessPortal';
  static readonly defaultConfig = config;

  constructor(
    @inject('datasources.config.MysqlAccessPortal', {optional: true})
    dsConfig: object = config,
  ) {
    super(dsConfig);
  }
}
