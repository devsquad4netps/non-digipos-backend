import {inject, lifeCycleObserver, LifeCycleObserver} from '@loopback/core';
import {juggler} from '@loopback/repository';
require('dotenv').config();

const config = {
  name: 'NonDigipos',
  connector: 'mysql',
  url: '',
  host: process.env.HOST_DB_NON_DIGIPOS,//'10.14.10.9',
  port: process.env.PORT_DB_NON_DIGIPOS,//3306,
  user: process.env.USER_DB_NON_DIGIPOS,//'apps_digipos_dev',
  password: process.env.PASS_DB_NON_DIGIPOS,//'TxRotq0TylHdyjDS',
  database: process.env.DB_NON_DIGIPOS//'access_portals'
};



// const config = {
//   name: 'mysql',
//   connector: 'mysql',
//   url: '',
//   host: '45.32.39.203',
//   port: 3306,
//   user: 'digiposdev',
//   password: 'b4l4n4r4g4n1m',
//   database: 'non_digipos',
// };

// Observe application's life cycle to disconnect the datasource when
// application is stopped. This allows the application to be shut down
// gracefully. The `stop()` method is inherited from `juggler.DataSource`.
// Learn more at https://loopback.io/doc/en/lb4/Life-cycle.html
@lifeCycleObserver('datasource')
export class NonDigiposDataSource extends juggler.DataSource
  implements LifeCycleObserver {
  static dataSourceName = 'NonDigipos';
  static readonly defaultConfig = config;

  constructor(
    @inject('datasources.config.NonDigipos', {optional: true})
    dsConfig: object = config,
  ) {
    super(dsConfig);
  }
}
