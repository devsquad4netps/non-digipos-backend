// import https from "https";
import {once} from 'events';
import express, {Request, Response} from 'express';
// import pEvent from 'p-event';
import http from "http";
import cron from 'node-cron';
import pEvent from 'p-event';
import {ApplicationConfig, MasterApplication} from './application';
import {MailSenderMessageActionController} from './controllers';
import {AlertPortalAdControler} from './controllers/alert-portal-ad.controller';
import {ControllerConfig} from './controllers/controller.config';
import {MerchantMailSenderFeedback} from './controllers/merchant-mail-sender-feedback';
import {MysqlDataSource, NonDigiposDataSource} from './datasources';

require('dotenv').config();


var jwt = require('jsonwebtoken');

// const io = require('socket.io')(http);
var socketIO = require('socket.io'), server, io: any;

const db = new MysqlDataSource();
const db_non_digipos = new NonDigiposDataSource();

export {ApplicationConfig};

export class ExpressServer {
  public readonly app: express.Application;
  public readonly lbApp: MasterApplication;
  private server?: http.Server;
  // private servers?: https.Server;
  // @repository(AdMasterRepository) public adMasterRepository: AdMasterRepository;
  // @inject(SecurityBindings.USER) public currentUserProfile: UserProfile;


  // @inject(ControllerConfig) public c: ControllerConfig;
  constructor(options: ApplicationConfig = {}) {
    this.app = express();

    this.lbApp = new MasterApplication(options);
    this.app.use(express.static('public'));
    // io.on('connection', function (socket: any) {
    //   console.log('A user connected');
    //   //Whenever someone disconnects this piece of code executed
    //   socket.on('disconnect', function () {
    //     console.log('A user disconnected');
    //   });
    // });

    this.app.use(this.lbApp.requestHandler);
    this.app.get('/hello', async (_req: Request, res: Response) => {
      console.log(_req.query);
      const users = ControllerConfig.onCurrentTime();
      console.log(users);
      res.send(users.toString());
      // this.app.use(this.app.toke)
    });




  }

  async boot() {
    await this.lbApp.boot();
  }

  isValidJwt = (header: any) => {
    const token = header.split(' ')[1];
    if (token === '33!894bHncvbGFTY_op0@dfgUY_dFcBnfhuOP') {
      return true;
    } else {
      return false;
    }
  };

  public async start() {
    await this.lbApp.start();
    const port = this.lbApp.restServer.config.port ?? process.env.APP_PORT_APP;
    const host = this.lbApp.restServer.config.host || '127.0.0.1';
    // this.server = this.app.listen(port, host);
    this.server = this.app.listen(port, host);
    await once(this.server, 'listening');


    cron.schedule('*/10 * * * * *', async () => {
      AlertPortalAdControler.onCallProgressPortal(db);
      MailSenderMessageActionController.onRunLog(db);
      MerchantMailSenderFeedback.onServiceMailDocDistribusi(db_non_digipos);
    });

    io = socketIO(this.server, {
      path: "/notification",
      cors: {
        origin: "*",
        methods: ["GET", "POST"]
      }
    });

    // io.use(socketioJwt.authorize({
    //   secret: '22665362778878766524534',
    //   handshake: true,
    //   // auth_header_required: true
    // }));


    io.use((socket: any, next: any) => {
      const header = socket.handshake.headers['authorization'];
      // console.log("HEADER", header);
      if (header != undefined) {
        if (this.isValidJwt(header)) {
          return next();
        }
      }
      return next(new Error('authentication error'));
    });

    ControllerConfig.onSetNotificationSocket(io.sockets);

    io.on('connection', function (socket: any) {
      console.log("connected");
      socket.on('notification_app', function (message: any) {
        // socket.emit('notification_app', message);
        io.sockets.emit('notification_app', message);
      });
    });




  }

  // For testing purposes
  public async stop() {
    if (!this.server) return;
    await this.lbApp.stop();
    this.server.close();
    await pEvent(this.server, 'close');
    this.server = undefined;
  }

}
