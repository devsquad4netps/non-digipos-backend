import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {NonDigiposDataSource} from '../datasources';
import {MerchantProductReminder, MerchantProductReminderRelations} from '../models';

export class MerchantProductReminderRepository extends DefaultCrudRepository<
  MerchantProductReminder,
  typeof MerchantProductReminder.prototype.MPR_ID,
  MerchantProductReminderRelations
> {
  constructor(
    @inject('datasources.NonDigipos') dataSource: NonDigiposDataSource,
  ) {
    super(MerchantProductReminder, dataSource);
  }
}
