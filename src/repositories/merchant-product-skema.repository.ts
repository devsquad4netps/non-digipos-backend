import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {NonDigiposDataSource} from '../datasources';
import {MerchantProductSkema, MerchantProductSkemaRelations} from '../models';

export class MerchantProductSkemaRepository extends DefaultCrudRepository<
  MerchantProductSkema,
  typeof MerchantProductSkema.prototype.MPS_ID,
  MerchantProductSkemaRelations
> {
  constructor(
    @inject('datasources.NonDigipos') dataSource: NonDigiposDataSource,
  ) {
    super(MerchantProductSkema, dataSource);
  }
}
