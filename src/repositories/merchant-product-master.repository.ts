import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {NonDigiposDataSource} from '../datasources';
import {MerchantProductMaster, MerchantProductMasterRelations} from '../models';

export class MerchantProductMasterRepository extends DefaultCrudRepository<
  MerchantProductMaster,
  typeof MerchantProductMaster.prototype.MPM_ID,
  MerchantProductMasterRelations
> {
  constructor(
    @inject('datasources.NonDigipos') dataSource: NonDigiposDataSource,
  ) {
    super(MerchantProductMaster, dataSource);
  }
}
