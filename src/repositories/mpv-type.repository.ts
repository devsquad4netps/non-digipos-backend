import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {NonDigiposDataSource} from '../datasources';
import {MpvType, MpvTypeRelations} from '../models';

export class MpvTypeRepository extends DefaultCrudRepository<
  MpvType,
  typeof MpvType.prototype.MPV_TYPE_ID,
  MpvTypeRelations
> {
  constructor(
    @inject('datasources.NonDigipos') dataSource: NonDigiposDataSource,
  ) {
    super(MpvType, dataSource);
  }
}
