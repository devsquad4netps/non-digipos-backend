import {DefaultCrudRepository} from '@loopback/repository';
import {Dispute, DisputeRelations} from '../models';
import {MysqlDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class DisputeRepository extends DefaultCrudRepository<
  Dispute,
  typeof Dispute.prototype.dispute_id,
  DisputeRelations
> {
  constructor(
    @inject('datasources.mysql') dataSource: MysqlDataSource,
  ) {
    super(Dispute, dataSource);
  }
}
