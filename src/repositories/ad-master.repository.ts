import {DefaultCrudRepository} from '@loopback/repository';
import {AdMaster, AdMasterRelations} from '../models';
import {MysqlDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class AdMasterRepository extends DefaultCrudRepository<
  AdMaster,
  typeof AdMaster.prototype.ad_code,
  AdMasterRelations
> {
  constructor(
    @inject('datasources.mysql') dataSource: MysqlDataSource,
  ) {
    super(AdMaster, dataSource);
  }
}
