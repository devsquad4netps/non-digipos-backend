import {DefaultCrudRepository} from '@loopback/repository';
import {DataSumarry, DataSumarryRelations} from '../models';
import {MysqlDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class DataSumarryRepository extends DefaultCrudRepository<
  DataSumarry,
  typeof DataSumarry.prototype.id,
  DataSumarryRelations
> {
  constructor(
    @inject('datasources.mysql') dataSource: MysqlDataSource,
  ) {
    super(DataSumarry, dataSource);
  }
}
