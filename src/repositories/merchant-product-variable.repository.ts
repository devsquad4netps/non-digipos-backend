import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {NonDigiposDataSource} from '../datasources';
import {MerchantProductVariable, MerchantProductVariableRelations} from '../models';

export class MerchantProductVariableRepository extends DefaultCrudRepository<
  MerchantProductVariable,
  typeof MerchantProductVariable.prototype.MPV_ID,
  MerchantProductVariableRelations
> {
  constructor(
    @inject('datasources.NonDigipos') dataSource: NonDigiposDataSource,
  ) {
    super(MerchantProductVariable, dataSource);
  }
}
