import {Getter, inject} from '@loopback/core';
import {DefaultCrudRepository, HasOneRepositoryFactory, repository} from '@loopback/repository';
import {MysqlAccessPortalDataSource} from '../datasources';
import {DataUser, DataUserCredentials, DataUserRelations} from '../models';
import {DataUserCredentialsRepository} from './data-user-credentials.repository';

export class DataUserRepository extends DefaultCrudRepository<
  DataUser,
  typeof DataUser.prototype.id,
  DataUserRelations
> {


  public readonly userCredentials: HasOneRepositoryFactory<
    DataUserCredentials,
    typeof DataUser.prototype.id
  >;

  constructor(
    @inject('datasources.MysqlAccessPortal') dataSource: MysqlAccessPortalDataSource,
    @repository.getter('DataUserCredentialsRepository')
    protected userCredentialsRepositoryGetter: Getter<
      DataUserCredentialsRepository
    >,
  ) {
    super(DataUser, dataSource);
    this.userCredentials = this.createHasOneRepositoryFactoryFor(
      'userCredentials',
      userCredentialsRepositoryGetter,
    );
    this.registerInclusionResolver(
      'userCredentials',
      this.userCredentials.inclusionResolver,
    );
  }
}
