import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {NonDigiposDataSource} from '../datasources';
import {MasterBank, MasterBankRelations} from '../models';

export class MasterBankRepository extends DefaultCrudRepository<
  MasterBank,
  typeof MasterBank.prototype.ID,
  MasterBankRelations
> {
  constructor(
    @inject('datasources.NonDigipos') dataSource: NonDigiposDataSource,
  ) {
    super(MasterBank, dataSource);
  }
}
