import {DefaultCrudRepository} from '@loopback/repository';
import {DataDisputeDetail, DataDisputeDetailRelations} from '../models';
import {MysqlDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class DataDisputeDetailRepository extends DefaultCrudRepository<
  DataDisputeDetail,
  typeof DataDisputeDetail.prototype.dispute_det_id,
  DataDisputeDetailRelations
> {
  constructor(
    @inject('datasources.mysql') dataSource: MysqlDataSource,
  ) {
    super(DataDisputeDetail, dataSource);
  }
}
