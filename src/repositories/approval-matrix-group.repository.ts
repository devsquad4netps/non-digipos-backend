import {DefaultCrudRepository} from '@loopback/repository';
import {ApprovalMatrixGroup, ApprovalMatrixGroupRelations} from '../models';
import {MysqlDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class ApprovalMatrixGroupRepository extends DefaultCrudRepository<
  ApprovalMatrixGroup,
  typeof ApprovalMatrixGroup.prototype.amg_id,
  ApprovalMatrixGroupRelations
> {
  constructor(
    @inject('datasources.mysql') dataSource: MysqlDataSource,
  ) {
    super(ApprovalMatrixGroup, dataSource);
  }
}
