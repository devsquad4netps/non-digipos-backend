import {DefaultCrudRepository} from '@loopback/repository';
import {DataConfigGroup, DataConfigGroupRelations} from '../models';
import {MysqlDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class DataConfigGroupRepository extends DefaultCrudRepository<
  DataConfigGroup,
  typeof DataConfigGroup.prototype.config_group_id,
  DataConfigGroupRelations
> {
  constructor(
    @inject('datasources.mysql') dataSource: MysqlDataSource,
  ) {
    super(DataConfigGroup, dataSource);
  }
}
