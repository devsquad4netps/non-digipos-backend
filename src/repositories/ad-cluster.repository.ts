import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {MysqlDataSource} from '../datasources';
import {AdCluster, AdClusterRelations} from '../models';

export class AdClusterRepository extends DefaultCrudRepository<
  AdCluster,
  typeof AdCluster.prototype.ad_cluster_id,
  AdClusterRelations
  > {
  constructor(
    @inject('datasources.mysql') dataSource: MysqlDataSource,
  ) {
    super(AdCluster, dataSource);
  }
}
