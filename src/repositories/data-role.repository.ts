import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {MysqlAccessPortalDataSource} from '../datasources';
import {DataRole, DataRoleRelations} from '../models';

export class DataRoleRepository extends DefaultCrudRepository<
  DataRole,
  typeof DataRole.prototype.id,
  DataRoleRelations
> {
  constructor(
    @inject('datasources.MysqlAccessPortal') dataSource: MysqlAccessPortalDataSource,
  ) {
    super(DataRole, dataSource);
  }
}
