import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {NonDigiposDataSource} from '../datasources';
import {MerchantProductTribe, MerchantProductTribeRelations} from '../models';

export class MerchantProductTribeRepository extends DefaultCrudRepository<
  MerchantProductTribe,
  typeof MerchantProductTribe.prototype.MPT_ID,
  MerchantProductTribeRelations
> {
  constructor(
    @inject('datasources.NonDigipos') dataSource: NonDigiposDataSource,
  ) {
    super(MerchantProductTribe, dataSource);
  }
}
