import {DefaultCrudRepository} from '@loopback/repository';
import {NotaDebet, NotaDebetRelations} from '../models';
import {MysqlDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class NotaDebetRepository extends DefaultCrudRepository<
  NotaDebet,
  typeof NotaDebet.prototype.nota_debet_id,
  NotaDebetRelations
> {
  constructor(
    @inject('datasources.mysql') dataSource: MysqlDataSource,
  ) {
    super(NotaDebet, dataSource);
  }
}
