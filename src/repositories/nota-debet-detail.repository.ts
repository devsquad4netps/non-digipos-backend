import {DefaultCrudRepository} from '@loopback/repository';
import {NotaDebetDetail, NotaDebetDetailRelations} from '../models';
import {MysqlDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class NotaDebetDetailRepository extends DefaultCrudRepository<
  NotaDebetDetail,
  typeof NotaDebetDetail.prototype.nota_debet_detail_id,
  NotaDebetDetailRelations
> {
  constructor(
    @inject('datasources.mysql') dataSource: MysqlDataSource,
  ) {
    super(NotaDebetDetail, dataSource);
  }
}
