import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {NonDigiposDataSource} from '../datasources';
import {MerchantProductFeedback, MerchantProductFeedbackRelations} from '../models';

export class MerchantProductFeedbackRepository extends DefaultCrudRepository<
  MerchantProductFeedback,
  typeof MerchantProductFeedback.prototype.MPF_ID,
  MerchantProductFeedbackRelations
> {
  constructor(
    @inject('datasources.NonDigipos') dataSource: NonDigiposDataSource,
  ) {
    super(MerchantProductFeedback, dataSource);
  }
}
