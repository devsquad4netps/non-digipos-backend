import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {NonDigiposDataSource} from '../datasources';
import {MerchantRekening, MerchantRekeningRelations} from '../models';

export class MerchantRekeningRepository extends DefaultCrudRepository<
  MerchantRekening,
  typeof MerchantRekening.prototype.MR_ID,
  MerchantRekeningRelations
> {
  constructor(
    @inject('datasources.NonDigipos') dataSource: NonDigiposDataSource,
  ) {
    super(MerchantRekening, dataSource);
  }
}
