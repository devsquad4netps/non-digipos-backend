import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {MysqlAccessPortalDataSource} from '../datasources';
import {DataRoleMap, DataRoleMapRelations} from '../models';

export class DataRoleMapRepository extends DefaultCrudRepository<
  DataRoleMap,
  typeof DataRoleMap.prototype.id,
  DataRoleMapRelations
> {
  constructor(
    @inject('datasources.MysqlAccessPortal') dataSource: MysqlAccessPortalDataSource,
  ) {
    super(DataRoleMap, dataSource);
  }
}
