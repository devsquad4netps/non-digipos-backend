import {DefaultCrudRepository} from '@loopback/repository';
import {Armaster, ArmasterRelations} from '../models';
import {MysqlDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class ArmasterRepository extends DefaultCrudRepository<
  Armaster,
  typeof Armaster.prototype.id,
  ArmasterRelations
> {
  constructor(
    @inject('datasources.mysql') dataSource: MysqlDataSource,
  ) {
    super(Armaster, dataSource);
  }
}
