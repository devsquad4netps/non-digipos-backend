import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {NonDigiposDataSource} from '../datasources';
import {MerchantProductGroup, MerchantProductGroupRelations} from '../models';

export class MerchantProductGroupRepository extends DefaultCrudRepository<
  MerchantProductGroup,
  typeof MerchantProductGroup.prototype.MPG_ID,
  MerchantProductGroupRelations
> {
  constructor(
    @inject('datasources.NonDigipos') dataSource: NonDigiposDataSource,
  ) {
    super(MerchantProductGroup, dataSource);
  }
}
