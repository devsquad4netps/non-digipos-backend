import {DefaultCrudRepository} from '@loopback/repository';
import {Rolemapping, RolemappingRelations} from '../models';
import {MysqlDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class RolemappingRepository extends DefaultCrudRepository<
  Rolemapping,
  typeof Rolemapping.prototype.id,
  RolemappingRelations
> {
  constructor(
    @inject('datasources.mysql') dataSource: MysqlDataSource,
  ) {
    super(Rolemapping, dataSource);
  }
}
