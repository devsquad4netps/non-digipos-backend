import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {MysqlAccessPortalDataSource} from '../datasources';
import {DataUserCredentials, DataUserCredentialsRelations} from '../models';

export class DataUserCredentialsRepository extends DefaultCrudRepository<
  DataUserCredentials,
  typeof DataUserCredentials.prototype.id,
  DataUserCredentialsRelations
> {
  constructor(
    @inject('datasources.MysqlAccessPortal') dataSource: MysqlAccessPortalDataSource,
  ) {
    super(DataUserCredentials, dataSource);
  }
}
