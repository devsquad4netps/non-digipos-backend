import {DefaultCrudRepository} from '@loopback/repository';
import {ErrorLog, ErrorLogRelations} from '../models';
import {MysqlDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class ErrorLogRepository extends DefaultCrudRepository<
  ErrorLog,
  typeof ErrorLog.prototype.error_log_id,
  ErrorLogRelations
> {
  constructor(
    @inject('datasources.mysql') dataSource: MysqlDataSource,
  ) {
    super(ErrorLog, dataSource);
  }
}
