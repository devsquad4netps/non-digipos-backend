import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {MysqlDataSource} from '../datasources';
import {AlertPortalAd, AlertPortalAdRelations} from '../models';

export class AlertPortalAdRepository extends DefaultCrudRepository<
  AlertPortalAd,
  typeof AlertPortalAd.prototype.id,
  AlertPortalAdRelations
> {
  constructor(
    @inject('datasources.mysql') dataSource: MysqlDataSource,
  ) {
    super(AlertPortalAd, dataSource);
  }
}
