import {DefaultCrudRepository} from '@loopback/repository';
import {SummaryTmp, SummaryTmpRelations} from '../models';
import {MysqlDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class SummaryTmpRepository extends DefaultCrudRepository<
  SummaryTmp,
  typeof SummaryTmp.prototype.id,
  SummaryTmpRelations
> {
  constructor(
    @inject('datasources.mysql') dataSource: MysqlDataSource,
  ) {
    super(SummaryTmp, dataSource);
  }
}
