import {DefaultCrudRepository} from '@loopback/repository';
import {DataDigiposAttentionGroup, DataDigiposAttentionGroupRelations} from '../models';
import {MysqlDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class DataDigiposAttentionGroupRepository extends DefaultCrudRepository<
  DataDigiposAttentionGroup,
  typeof DataDigiposAttentionGroup.prototype.id,
  DataDigiposAttentionGroupRelations
> {
  constructor(
    @inject('datasources.mysql') dataSource: MysqlDataSource,
  ) {
    super(DataDigiposAttentionGroup, dataSource);
  }
}
