import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {NonDigiposDataSource} from '../datasources';
import {MpvTypeKey, MpvTypeKeyRelations} from '../models';

export class MpvTypeKeyRepository extends DefaultCrudRepository<
  MpvTypeKey,
  typeof MpvTypeKey.prototype.MPV_TYPE_KEY_ID,
  MpvTypeKeyRelations
> {
  constructor(
    @inject('datasources.NonDigipos') dataSource: NonDigiposDataSource,
  ) {
    super(MpvTypeKey, dataSource);
  }
}
