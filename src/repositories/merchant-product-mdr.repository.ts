import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {NonDigiposDataSource} from '../datasources';
import {MerchantProductMdr, MerchantProductMdrRelations} from '../models';

export class MerchantProductMdrRepository extends DefaultCrudRepository<
  MerchantProductMdr,
  typeof MerchantProductMdr.prototype.MDR_ID,
  MerchantProductMdrRelations
> {
  constructor(
    @inject('datasources.NonDigipos') dataSource: NonDigiposDataSource,
  ) {
    super(MerchantProductMdr, dataSource);
  }
}
