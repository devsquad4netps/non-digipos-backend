import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {MysqlDataSource} from '../datasources';
import {DataDigiposAttention, DataDigiposAttentionRelations} from '../models';

export class DataDigiposAttentionRepository extends DefaultCrudRepository<
  DataDigiposAttention,
  typeof DataDigiposAttention.prototype.attention_id,
  DataDigiposAttentionRelations
  > {
  constructor(
    @inject('datasources.mysql') dataSource: MysqlDataSource,
  ) {
    super(DataDigiposAttention, dataSource);
  }
}
