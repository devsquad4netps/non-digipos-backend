import {DefaultCrudRepository} from '@loopback/repository';
import {DataDigiposReject, DataDigiposRejectRelations} from '../models';
import {MysqlDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class DataDigiposRejectRepository extends DefaultCrudRepository<
  DataDigiposReject,
  typeof DataDigiposReject.prototype.reject_id,
  DataDigiposRejectRelations
> {
  constructor(
    @inject('datasources.mysql') dataSource: MysqlDataSource,
  ) {
    super(DataDigiposReject, dataSource);
  }
}
