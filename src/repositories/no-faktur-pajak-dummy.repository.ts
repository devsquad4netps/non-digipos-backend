import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {MysqlDataSource} from '../datasources';
import {NoFakturPajakDummy, NoFakturPajakDummyRelations} from '../models';

export class NoFakturPajakDummyRepository extends DefaultCrudRepository<
  NoFakturPajakDummy,
  typeof NoFakturPajakDummy.prototype.id,
  NoFakturPajakDummyRelations
> {
  constructor(
    @inject('datasources.mysql') dataSource: MysqlDataSource,
  ) {
    super(NoFakturPajakDummy, dataSource);
  }
}
