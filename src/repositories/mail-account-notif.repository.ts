import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {MysqlDataSource} from '../datasources';
import {MailAccountNotif, MailAccountNotifRelations} from '../models';

export class MailAccountNotifRepository extends DefaultCrudRepository<
  MailAccountNotif,
  typeof MailAccountNotif.prototype.MAN_ID,
  MailAccountNotifRelations
> {
  constructor(
    @inject('datasources.mysql') dataSource: MysqlDataSource,
  ) {
    super(MailAccountNotif, dataSource);
  }
}
