import {DefaultCrudRepository} from '@loopback/repository';
import {Adgroup, AdgroupRelations} from '../models';
import {MysqlDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class AdgroupRepository extends DefaultCrudRepository<
  Adgroup,
  typeof Adgroup.prototype.id,
  AdgroupRelations
> {
  constructor(
    @inject('datasources.mysql') dataSource: MysqlDataSource,
  ) {
    super(Adgroup, dataSource);
  }
}
