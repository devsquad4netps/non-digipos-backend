import {DefaultCrudRepository} from '@loopback/repository';
import {TblAdMaster, TblAdMasterRelations} from '../models';
import {MysqlDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class TblAdMasterRepository extends DefaultCrudRepository<
  TblAdMaster,
  typeof TblAdMaster.prototype.ad_code,
  TblAdMasterRelations
> {
  constructor(
    @inject('datasources.mysql') dataSource: MysqlDataSource,
  ) {
    super(TblAdMaster, dataSource);
  }
}
