import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {NonDigiposDataSource} from '../datasources';
import {MerchantProductSummary, MerchantProductSummaryRelations} from '../models';

export class MerchantProductSummaryRepository extends DefaultCrudRepository<
  MerchantProductSummary,
  typeof MerchantProductSummary.prototype.SUMMARY_ID,
  MerchantProductSummaryRelations
> {
  constructor(
    @inject('datasources.NonDigipos') dataSource: NonDigiposDataSource,
  ) {
    super(MerchantProductSummary, dataSource);
  }
}
