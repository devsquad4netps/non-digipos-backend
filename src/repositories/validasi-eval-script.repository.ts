import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {NonDigiposDataSource} from '../datasources';
import {ValidasiEvalScript, ValidasiEvalScriptRelations} from '../models';

export class ValidasiEvalScriptRepository extends DefaultCrudRepository<
  ValidasiEvalScript,
  typeof ValidasiEvalScript.prototype.ID,
  ValidasiEvalScriptRelations
> {
  constructor(
    @inject('datasources.NonDigipos') dataSource: NonDigiposDataSource,
  ) {
    super(ValidasiEvalScript, dataSource);
  }
}
