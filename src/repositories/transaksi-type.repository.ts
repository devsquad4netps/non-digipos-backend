import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {MysqlDataSource} from '../datasources';
import {TransaksiType, TransaksiTypeRelations} from '../models';

export class TransaksiTypeRepository extends DefaultCrudRepository<
  TransaksiType,
  typeof TransaksiType.prototype.TT_ID,
  TransaksiTypeRelations
> {
  constructor(
    @inject('datasources.mysql') dataSource: MysqlDataSource,
  ) {
    super(TransaksiType, dataSource);
  }
}
