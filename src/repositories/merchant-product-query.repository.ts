import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {NonDigiposDataSource} from '../datasources';
import {MerchantProductQuery, MerchantProductQueryRelations} from '../models';

export class MerchantProductQueryRepository extends DefaultCrudRepository<
  MerchantProductQuery,
  typeof MerchantProductQuery.prototype.MPQ_ID,
  MerchantProductQueryRelations
> {
  constructor(
    @inject('datasources.NonDigipos') dataSource: NonDigiposDataSource,
  ) {
    super(MerchantProductQuery, dataSource);
  }
}
