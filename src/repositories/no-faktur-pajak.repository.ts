import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {MysqlDataSource} from '../datasources';
import {NoFakturPajak, NoFakturPajakRelations} from '../models';

export class NoFakturPajakRepository extends DefaultCrudRepository<
  NoFakturPajak,
  typeof NoFakturPajak.prototype.id,
  NoFakturPajakRelations
  > {
  constructor(
    @inject('datasources.mysql') dataSource: MysqlDataSource,
  ) {
    super(NoFakturPajak, dataSource);
  }

}
