import {DefaultCrudRepository} from '@loopback/repository';
import {ApprovalMatrix, ApprovalMatrixRelations} from '../models';
import {MysqlDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class ApprovalMatrixRepository extends DefaultCrudRepository<
  ApprovalMatrix,
  typeof ApprovalMatrix.prototype.am_id,
  ApprovalMatrixRelations
> {
  constructor(
    @inject('datasources.mysql') dataSource: MysqlDataSource,
  ) {
    super(ApprovalMatrix, dataSource);
  }
}
