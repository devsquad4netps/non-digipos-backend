import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {NonDigiposDataSource} from '../datasources';
import {SycronizeProcessLog, SycronizeProcessLogRelations} from '../models';

export class SycronizeProcessLogRepository extends DefaultCrudRepository<
  SycronizeProcessLog,
  typeof SycronizeProcessLog.prototype.SPL_ID,
  SycronizeProcessLogRelations
> {
  constructor(
    @inject('datasources.NonDigipos') dataSource: NonDigiposDataSource,
  ) {
    super(SycronizeProcessLog, dataSource);
  }
}
