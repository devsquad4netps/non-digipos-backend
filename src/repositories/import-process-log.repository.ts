import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {NonDigiposDataSource} from '../datasources';
import {ImportProcessLog, ImportProcessLogRelations} from '../models';

export class ImportProcessLogRepository extends DefaultCrudRepository<
  ImportProcessLog,
  typeof ImportProcessLog.prototype.IMPORT_ID,
  ImportProcessLogRelations
> {
  constructor(
    @inject('datasources.NonDigipos') dataSource: NonDigiposDataSource,
  ) {
    super(ImportProcessLog, dataSource);
  }
}
