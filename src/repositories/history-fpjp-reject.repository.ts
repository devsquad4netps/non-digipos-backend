import {DefaultCrudRepository} from '@loopback/repository';
import {HistoryFpjpReject, HistoryFpjpRejectRelations} from '../models';
import {MysqlDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class HistoryFpjpRejectRepository extends DefaultCrudRepository<
  HistoryFpjpReject,
  typeof HistoryFpjpReject.prototype.id,
  HistoryFpjpRejectRelations
> {
  constructor(
    @inject('datasources.mysql') dataSource: MysqlDataSource,
  ) {
    super(HistoryFpjpReject, dataSource);
  }
}
