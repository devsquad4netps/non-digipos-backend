import {DefaultCrudRepository} from '@loopback/repository';
import {DataDigipos, DataDigiposRelations} from '../models';
import {MysqlDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class DataDigiposRepository extends DefaultCrudRepository<
  DataDigipos,
  typeof DataDigipos.prototype.digipost_id,
  DataDigiposRelations
> {
  constructor(
    @inject('datasources.mysql') dataSource: MysqlDataSource,
  ) {
    super(DataDigipos, dataSource);
  }
}
