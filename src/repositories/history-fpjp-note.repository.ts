import {DefaultCrudRepository} from '@loopback/repository';
import {HistoryFpjpNote, HistoryFpjpNoteRelations} from '../models';
import {MysqlDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class HistoryFpjpNoteRepository extends DefaultCrudRepository<
  HistoryFpjpNote,
  typeof HistoryFpjpNote.prototype.id,
  HistoryFpjpNoteRelations
> {
  constructor(
    @inject('datasources.mysql') dataSource: MysqlDataSource,
  ) {
    super(HistoryFpjpNote, dataSource);
  }
}
