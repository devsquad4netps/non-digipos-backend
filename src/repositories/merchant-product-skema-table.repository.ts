import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {NonDigiposDataSource} from '../datasources';
import {MerchantProductSkemaTable, MerchantProductSkemaTableRelations} from '../models';

export class MerchantProductSkemaTableRepository extends DefaultCrudRepository<
  MerchantProductSkemaTable,
  typeof MerchantProductSkemaTable.prototype.MPST_ID,
  MerchantProductSkemaTableRelations
> {
  constructor(
    @inject('datasources.NonDigipos') dataSource: NonDigiposDataSource,
  ) {
    super(MerchantProductSkemaTable, dataSource);
  }
}
