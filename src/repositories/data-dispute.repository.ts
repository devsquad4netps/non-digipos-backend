import {DefaultCrudRepository} from '@loopback/repository';
import {DataDispute, DataDisputeRelations} from '../models';
import {MysqlDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class DataDisputeRepository extends DefaultCrudRepository<
  DataDispute,
  typeof DataDispute.prototype.dispute_id,
  DataDisputeRelations
> {
  constructor(
    @inject('datasources.mysql') dataSource: MysqlDataSource,
  ) {
    super(DataDispute, dataSource);
  }
}
