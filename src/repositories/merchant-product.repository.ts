import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {NonDigiposDataSource} from '../datasources';
import {MerchantProduct, MerchantProductRelations} from '../models';

export class MerchantProductRepository extends DefaultCrudRepository<
  MerchantProduct,
  typeof MerchantProduct.prototype.MP_ID,
  MerchantProductRelations
> {
  constructor(
    @inject('datasources.NonDigipos') dataSource: NonDigiposDataSource,
  ) {
    super(MerchantProduct, dataSource);
  }
}
