import {DefaultCrudRepository} from '@loopback/repository';
import {DataSharePercent, DataSharePercentRelations} from '../models';
import {MysqlDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class DataSharePercentRepository extends DefaultCrudRepository<
  DataSharePercent,
  typeof DataSharePercent.prototype.id,
  DataSharePercentRelations
> {
  constructor(
    @inject('datasources.mysql') dataSource: MysqlDataSource,
  ) {
    super(DataSharePercent, dataSource);
  }
}
