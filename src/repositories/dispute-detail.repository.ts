import {DefaultCrudRepository} from '@loopback/repository';
import {DisputeDetail, DisputeDetailRelations} from '../models';
import {MysqlDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class DisputeDetailRepository extends DefaultCrudRepository<
  DisputeDetail,
  typeof DisputeDetail.prototype.dispute_detail_id,
  DisputeDetailRelations
> {
  constructor(
    @inject('datasources.mysql') dataSource: MysqlDataSource,
  ) {
    super(DisputeDetail, dataSource);
  }
}
