import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {NonDigiposDataSource} from '../datasources';
import {MerchantProductDatastore, MerchantProductDatastoreRelations} from '../models';

export class MerchantProductDatastoreRepository extends DefaultCrudRepository<
  MerchantProductDatastore,
  typeof MerchantProductDatastore.prototype.MPD_ID,
  MerchantProductDatastoreRelations
> {
  constructor(
    @inject('datasources.NonDigipos') dataSource: NonDigiposDataSource,
  ) {
    super(MerchantProductDatastore, dataSource);
  }
}
