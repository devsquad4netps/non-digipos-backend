import {DefaultCrudRepository} from '@loopback/repository';
import {DataRechargeDetail, DataRechargeDetailRelations} from '../models';
import {MysqlDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class DataRechargeDetailRepository extends DefaultCrudRepository<
  DataRechargeDetail,
  typeof DataRechargeDetail.prototype.id,
  DataRechargeDetailRelations
> {
  constructor(
    @inject('datasources.mysql') dataSource: MysqlDataSource,
  ) {
    super(DataRechargeDetail, dataSource);
  }
}
