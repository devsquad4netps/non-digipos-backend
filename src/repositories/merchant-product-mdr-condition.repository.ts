import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {NonDigiposDataSource} from '../datasources';
import {MerchantProductMdrCondition, MerchantProductMdrConditionRelations} from '../models';

export class MerchantProductMdrConditionRepository extends DefaultCrudRepository<
  MerchantProductMdrCondition,
  typeof MerchantProductMdrCondition.prototype.MDRCON_ID,
  MerchantProductMdrConditionRelations
> {
  constructor(
    @inject('datasources.NonDigipos') dataSource: NonDigiposDataSource,
  ) {
    super(MerchantProductMdrCondition, dataSource);
  }
}
