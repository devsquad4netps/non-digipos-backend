import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {MysqlDataSource} from '../datasources';
import {TransaksiCalculation, TransaksiCalculationRelations} from '../models';

export class TransaksiCalculationRepository extends DefaultCrudRepository<
  TransaksiCalculation,
  typeof TransaksiCalculation.prototype.ID,
  TransaksiCalculationRelations
> {
  constructor(
    @inject('datasources.mysql') dataSource: MysqlDataSource,
  ) {
    super(TransaksiCalculation, dataSource);
  }
}
