import {DefaultCrudRepository} from '@loopback/repository';
import {ApprovalMatrixDetail, ApprovalMatrixDetailRelations} from '../models';
import {MysqlDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class ApprovalMatrixDetailRepository extends DefaultCrudRepository<
  ApprovalMatrixDetail,
  typeof ApprovalMatrixDetail.prototype.amd_id,
  ApprovalMatrixDetailRelations
> {
  constructor(
    @inject('datasources.mysql') dataSource: MysqlDataSource,
  ) {
    super(ApprovalMatrixDetail, dataSource);
  }
}
