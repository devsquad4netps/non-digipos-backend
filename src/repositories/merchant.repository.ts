import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {NonDigiposDataSource} from '../datasources';
import {Merchant, MerchantRelations} from '../models';

export class MerchantRepository extends DefaultCrudRepository<
  Merchant,
  typeof Merchant.prototype.M_ID,
  MerchantRelations
> {
  constructor(
    @inject('datasources.NonDigipos') dataSource: NonDigiposDataSource,
  ) {
    super(Merchant, dataSource);
  }
}
