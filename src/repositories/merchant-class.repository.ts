import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {NonDigiposDataSource} from '../datasources';
import {MerchantClass, MerchantClassRelations} from '../models';

export class MerchantClassRepository extends DefaultCrudRepository<
  MerchantClass,
  typeof MerchantClass.prototype.MC_ID,
  MerchantClassRelations
> {
  constructor(
    @inject('datasources.NonDigipos') dataSource: NonDigiposDataSource,
  ) {
    super(MerchantClass, dataSource);
  }
}
