import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {NonDigiposDataSource} from '../datasources';
import {MerchantProductSummaryGroup, MerchantProductSummaryGroupRelations} from '../models';

export class MerchantProductSummaryGroupRepository extends DefaultCrudRepository<
  MerchantProductSummaryGroup,
  typeof MerchantProductSummaryGroup.prototype.MPSG_ID,
  MerchantProductSummaryGroupRelations
> {
  constructor(
    @inject('datasources.NonDigipos') dataSource: NonDigiposDataSource,
  ) {
    super(MerchantProductSummaryGroup, dataSource);
  }
}
