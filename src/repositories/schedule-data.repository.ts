import {DefaultCrudRepository} from '@loopback/repository';
import {ScheduleData, ScheduleDataRelations} from '../models';
import {MysqlDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class ScheduleDataRepository extends DefaultCrudRepository<
  ScheduleData,
  typeof ScheduleData.prototype.id,
  ScheduleDataRelations
> {
  constructor(
    @inject('datasources.mysql') dataSource: MysqlDataSource,
  ) {
    super(ScheduleData, dataSource);
  }
}
