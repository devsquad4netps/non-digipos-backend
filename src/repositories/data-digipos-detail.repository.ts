import {DefaultCrudRepository} from '@loopback/repository';
import {DataDigiposDetail, DataDigiposDetailRelations} from '../models';
import {MysqlDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class DataDigiposDetailRepository extends DefaultCrudRepository<
  DataDigiposDetail,
  typeof DataDigiposDetail.prototype.digipos_detail_id,
  DataDigiposDetailRelations
> {
  constructor(
    @inject('datasources.mysql') dataSource: MysqlDataSource,
  ) {
    super(DataDigiposDetail, dataSource);
  }
}
