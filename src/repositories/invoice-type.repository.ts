import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {NonDigiposDataSource} from '../datasources';
import {InvoiceType, InvoiceTypeRelations} from '../models';

export class InvoiceTypeRepository extends DefaultCrudRepository<
  InvoiceType,
  typeof InvoiceType.prototype.ID,
  InvoiceTypeRelations
> {
  constructor(
    @inject('datasources.NonDigipos') dataSource: NonDigiposDataSource,
  ) {
    super(InvoiceType, dataSource);
  }
}
