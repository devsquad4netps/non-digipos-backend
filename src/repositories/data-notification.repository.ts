import {DefaultCrudRepository} from '@loopback/repository';
import {DataNotification, DataNotificationRelations} from '../models';
import {MysqlDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class DataNotificationRepository extends DefaultCrudRepository<
  DataNotification,
  typeof DataNotification.prototype.id,
  DataNotificationRelations
> {
  constructor(
    @inject('datasources.mysql') dataSource: MysqlDataSource,
  ) {
    super(DataNotification, dataSource);
  }
}
