import {DefaultCrudRepository} from '@loopback/repository';
import {ApprovalNota, ApprovalNotaRelations} from '../models';
import {MysqlDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class ApprovalNotaRepository extends DefaultCrudRepository<
  ApprovalNota,
  typeof ApprovalNota.prototype.id,
  ApprovalNotaRelations
> {
  constructor(
    @inject('datasources.mysql') dataSource: MysqlDataSource,
  ) {
    super(ApprovalNota, dataSource);
  }
}
