import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {NonDigiposDataSource} from '../datasources';
import {MerchantPic, MerchantPicRelations} from '../models';

export class MerchantPicRepository extends DefaultCrudRepository<
  MerchantPic,
  typeof MerchantPic.prototype.ID,
  MerchantPicRelations
> {
  constructor(
    @inject('datasources.NonDigipos') dataSource: NonDigiposDataSource,
  ) {
    super(MerchantPic, dataSource);
  }
}
