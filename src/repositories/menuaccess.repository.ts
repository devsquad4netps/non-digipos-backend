import {DefaultCrudRepository} from '@loopback/repository';
import {Menuaccess, MenuaccessRelations} from '../models';
import {MysqlDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class MenuaccessRepository extends DefaultCrudRepository<
  Menuaccess,
  typeof Menuaccess.prototype.id,
  MenuaccessRelations
> {
  constructor(
    @inject('datasources.mysql') dataSource: MysqlDataSource,
  ) {
    super(Menuaccess, dataSource);
  }
}
