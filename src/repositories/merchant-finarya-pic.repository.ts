import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {NonDigiposDataSource} from '../datasources';
import {MerchantFinaryaPic, MerchantFinaryaPicRelations} from '../models';

export class MerchantFinaryaPicRepository extends DefaultCrudRepository<
  MerchantFinaryaPic,
  typeof MerchantFinaryaPic.prototype.ID,
  MerchantFinaryaPicRelations
> {
  constructor(
    @inject('datasources.NonDigipos') dataSource: NonDigiposDataSource,
  ) {
    super(MerchantFinaryaPic, dataSource);
  }
}
