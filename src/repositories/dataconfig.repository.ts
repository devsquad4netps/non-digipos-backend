import {DefaultCrudRepository} from '@loopback/repository';
import {Dataconfig, DataconfigRelations} from '../models';
import {MysqlDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class DataconfigRepository extends DefaultCrudRepository<
  Dataconfig,
  typeof Dataconfig.prototype.id,
  DataconfigRelations
> {
  constructor(
    @inject('datasources.mysql') dataSource: MysqlDataSource,
  ) {
    super(Dataconfig, dataSource);
  }
}
