import {DefaultCrudRepository} from '@loopback/repository';
import {ApprovalMatrixMember, ApprovalMatrixMemberRelations} from '../models';
import {MysqlDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class ApprovalMatrixMemberRepository extends DefaultCrudRepository<
  ApprovalMatrixMember,
  typeof ApprovalMatrixMember.prototype.amm_id,
  ApprovalMatrixMemberRelations
> {
  constructor(
    @inject('datasources.mysql') dataSource: MysqlDataSource,
  ) {
    super(ApprovalMatrixMember, dataSource);
  }
}
