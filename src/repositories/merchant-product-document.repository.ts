import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {NonDigiposDataSource} from '../datasources';
import {MerchantProductDocument, MerchantProductDocumentRelations} from '../models';

export class MerchantProductDocumentRepository extends DefaultCrudRepository<
  MerchantProductDocument,
  typeof MerchantProductDocument.prototype.MPDOC_ID,
  MerchantProductDocumentRelations
> {
  constructor(
    @inject('datasources.NonDigipos') dataSource: NonDigiposDataSource,
  ) {
    super(MerchantProductDocument, dataSource);
  }
}
