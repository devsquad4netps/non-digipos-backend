import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {MysqlDataSource} from '../datasources';
import {MailSenderMessage, MailSenderMessageRelations} from '../models';

export class MailSenderMessageRepository extends DefaultCrudRepository<
  MailSenderMessage,
  typeof MailSenderMessage.prototype.MSM_ID,
  MailSenderMessageRelations
> {
  constructor(
    @inject('datasources.mysql') dataSource: MysqlDataSource,
  ) {
    super(MailSenderMessage, dataSource);
  }
}
