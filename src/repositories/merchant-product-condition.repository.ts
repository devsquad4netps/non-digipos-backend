import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {NonDigiposDataSource} from '../datasources';
import {MerchantProductCondition, MerchantProductConditionRelations} from '../models';

export class MerchantProductConditionRepository extends DefaultCrudRepository<
  MerchantProductCondition,
  typeof MerchantProductCondition.prototype.MPC_ID,
  MerchantProductConditionRelations
> {
  constructor(
    @inject('datasources.NonDigipos') dataSource: NonDigiposDataSource,
  ) {
    super(MerchantProductCondition, dataSource);
  }
}
