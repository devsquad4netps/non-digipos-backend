import {DefaultCrudRepository} from '@loopback/repository';
import {DownloadGoogleDrive, DownloadGoogleDriveRelations} from '../models';
import {MysqlDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class DownloadGoogleDriveRepository extends DefaultCrudRepository<
  DownloadGoogleDrive,
  typeof DownloadGoogleDrive.prototype.gd_id,
  DownloadGoogleDriveRelations
> {
  constructor(
    @inject('datasources.mysql') dataSource: MysqlDataSource,
  ) {
    super(DownloadGoogleDrive, dataSource);
  }
}
