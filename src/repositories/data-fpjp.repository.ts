import {DefaultCrudRepository} from '@loopback/repository';
import {DataFpjp, DataFpjpRelations} from '../models';
import {MysqlDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class DataFpjpRepository extends DefaultCrudRepository<
  DataFpjp,
  typeof DataFpjp.prototype.id,
  DataFpjpRelations
> {
  constructor(
    @inject('datasources.mysql') dataSource: MysqlDataSource,
  ) {
    super(DataFpjp, dataSource);
  }
}
