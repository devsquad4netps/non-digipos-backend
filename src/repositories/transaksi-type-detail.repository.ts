import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {MysqlDataSource} from '../datasources';
import {TransaksiTypeDetail, TransaksiTypeDetailRelations} from '../models';

export class TransaksiTypeDetailRepository extends DefaultCrudRepository<
  TransaksiTypeDetail,
  typeof TransaksiTypeDetail.prototype.TTD_ID,
  TransaksiTypeDetailRelations
> {
  constructor(
    @inject('datasources.mysql') dataSource: MysqlDataSource,
  ) {
    super(TransaksiTypeDetail, dataSource);
  }
}
