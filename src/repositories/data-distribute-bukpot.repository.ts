import {DefaultCrudRepository} from '@loopback/repository';
import {DataDistributeBukpot, DataDistributeBukpotRelations} from '../models';
import {MysqlDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class DataDistributeBukpotRepository extends DefaultCrudRepository<
  DataDistributeBukpot,
  typeof DataDistributeBukpot.prototype.bukpot_id,
  DataDistributeBukpotRelations
> {
  constructor(
    @inject('datasources.mysql') dataSource: MysqlDataSource,
  ) {
    super(DataDistributeBukpot, dataSource);
  }
}
