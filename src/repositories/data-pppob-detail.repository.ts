import {DefaultCrudRepository} from '@loopback/repository';
import {DataPppobDetail, DataPppobDetailRelations} from '../models';
import {MysqlDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class DataPppobDetailRepository extends DefaultCrudRepository<
  DataPppobDetail,
  typeof DataPppobDetail.prototype.id,
  DataPppobDetailRelations
> {
  constructor(
    @inject('datasources.mysql') dataSource: MysqlDataSource,
  ) {
    super(DataPppobDetail, dataSource);
  }
}
