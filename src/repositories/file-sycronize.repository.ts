import {DefaultCrudRepository} from '@loopback/repository';
import {FileSycronize, FileSycronizeRelations} from '../models';
import {MysqlDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class FileSycronizeRepository extends DefaultCrudRepository<
  FileSycronize,
  typeof FileSycronize.prototype.file_syc_id,
  FileSycronizeRelations
> {
  constructor(
    @inject('datasources.mysql') dataSource: MysqlDataSource,
  ) {
    super(FileSycronize, dataSource);
  }
}
