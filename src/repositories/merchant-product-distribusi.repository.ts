import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {NonDigiposDataSource} from '../datasources';
import {MerchantProductDistribusi, MerchantProductDistribusiRelations} from '../models';

export class MerchantProductDistribusiRepository extends DefaultCrudRepository<
  MerchantProductDistribusi,
  typeof MerchantProductDistribusi.prototype.MPDIST_ID,
  MerchantProductDistribusiRelations
> {
  constructor(
    @inject('datasources.NonDigipos') dataSource: NonDigiposDataSource,
  ) {
    super(MerchantProductDistribusi, dataSource);
  }
}
