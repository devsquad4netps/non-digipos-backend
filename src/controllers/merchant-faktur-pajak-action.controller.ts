import {authenticate} from '@loopback/authentication';
import {inject} from '@loopback/core';
import {Filter, repository} from '@loopback/repository';
import {get, getModelSchemaRef, param, post, requestBody, ResponseObject} from '@loopback/rest';
import {SecurityBindings, securityId, UserProfile} from '@loopback/security';
import {MerchantProductMaster, NoFakturPajak} from '../models';
import {InvoiceTypeRepository, MerchantFinaryaPicRepository, MerchantProductDatastoreRepository, MerchantProductDistribusiRepository, MerchantProductDocumentRepository, MerchantProductFeedbackRepository, MerchantProductGroupRepository, MerchantProductMasterRepository, MerchantProductSkemaRepository, MerchantProductSummaryRepository, MerchantProductTribeRepository, MerchantProductVariableRepository, MpvTypeKeyRepository, MpvTypeRepository, NoFakturPajakRepository, SycronizeProcessLogRepository, ValidasiEvalScriptRepository} from '../repositories';
import {MerchantProductRepository} from '../repositories/merchant-product.repository';
import {MerchantRepository} from '../repositories/merchant.repository';
import {UserRepository} from '../repositories/user.repository';
import {ControllerConfig} from './controller.config';
var createHTML = require('create-html');


export const RegisterNoFakturMerchantRequestBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['QUOTE_ALLOCATION', 'START_NUMBER', 'END_NUMBER', 'REMARKS', 'PERIODE', 'FP_BUFFER'],
        properties: {
          QUOTE_ALLOCATION: {
            type: 'string'
          },
          START_NUMBER: {
            type: 'number'
          },
          END_NUMBER: {
            type: 'number'
          },
          REMARKS: {
            type: 'string'
          },
          PERIODE: {
            type: 'string'
          },
          FP_BUFFER: {
            type: "boolean"
          }
        }
      }
    },
  },
};


export const CancelRequestBody: ResponseObject = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ["ID", "COUNT_CANCEL"],
        properties: {
          ID: {
            type: 'string'
          },
          COUNT_CANCEL: {
            type: 'number',
          },
        }
      }
    },
  },
};



var thisContent: any;
@authenticate('jwt')
export class MerchantFakturPajakActionController {
  constructor(
    @repository(MerchantRepository)
    public merchantRepository: MerchantRepository,
    @repository(MerchantProductRepository)
    public merchantProductRepository: MerchantProductRepository,
    @repository(MerchantProductGroupRepository)
    public merchantProductGroupRepository: MerchantProductGroupRepository,
    @repository(MerchantProductVariableRepository)
    public merchantProductVariableRepository: MerchantProductVariableRepository,
    @repository(MerchantProductDatastoreRepository)
    public merchantProductDatastoreRepository: MerchantProductDatastoreRepository,
    @repository(MerchantProductSummaryRepository)
    public merchantProductSummaryRepository: MerchantProductSummaryRepository,
    @repository(MpvTypeRepository)
    public mpvTypeRepository: MpvTypeRepository,
    @repository(MerchantProductMasterRepository)
    public merchantProductMasterRepository: MerchantProductMasterRepository,
    @repository(MpvTypeKeyRepository)
    public mpvTypeKeyRepository: MpvTypeKeyRepository,
    @repository(SycronizeProcessLogRepository)
    public sycronizeProcessLogRepository: SycronizeProcessLogRepository,
    @repository(MerchantProductFeedbackRepository)
    public merchantProductFeedbackRepository: MerchantProductFeedbackRepository,
    @repository(MerchantProductSkemaRepository)
    public merchantProductSkemaRepository: MerchantProductSkemaRepository,
    @repository(MerchantProductDocumentRepository)
    public merchantProductDocumentRepository: MerchantProductDocumentRepository,
    @repository(MerchantProductTribeRepository)
    public merchantProductTribeRepository: MerchantProductTribeRepository,
    @repository(ValidasiEvalScriptRepository)
    public validasiEvalScriptRepository: ValidasiEvalScriptRepository,
    @repository(MerchantFinaryaPicRepository)
    public merchantFinaryaPicRepository: MerchantFinaryaPicRepository,
    @repository(MerchantProductDistribusiRepository)
    public merchantProductDistribusiRepository: MerchantProductDistribusiRepository,
    @repository(NoFakturPajakRepository)
    public noFakturPajakRepository: NoFakturPajakRepository,
    @repository(InvoiceTypeRepository)
    public invoiceTypeRepository: InvoiceTypeRepository,
    @inject(SecurityBindings.USER)
    public currentUserProfile: UserProfile,
    @repository(UserRepository)
    public userRepository: UserRepository,
  ) {
    thisContent = this;
  }


  @get('/faktur-pajak/summary/total', {
    responses: {
      '200': {
        description: 'Array of Merchant Product model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(NoFakturPajak, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async findMerchantNoFakturSummaryTotal(
    @param.filter(NoFakturPajak) filter?: Filter<NoFakturPajak>,
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};

    var FAKTUR_PAJAK = await this.noFakturPajakRepository.find(filter);

    var DATAS: any; DATAS = {};
    for (var i in FAKTUR_PAJAK) {
      var RAW_FAKTUR = FAKTUR_PAJAK[i];
      if (DATAS[RAW_FAKTUR.periode] == undefined) {
        DATAS[RAW_FAKTUR.periode] = {NO_TERAKHIR: 0, QTY_USED: 0, QTY_BUFFER: 0, QTY_NOT_USED: 0, TOTAL: 0};
      }
      if (RAW_FAKTUR.fp_state == 1) {
        DATAS[RAW_FAKTUR.periode]["NO_TERAKHIR"] = RAW_FAKTUR.no_faktur_link;
        DATAS[RAW_FAKTUR.periode]["QTY_USED"] = DATAS[RAW_FAKTUR.periode]["QTY_USED"] + 1;
      }
      if (RAW_FAKTUR.fp_buffer == true) {
        DATAS[RAW_FAKTUR.periode]["QTY_BUFFER"] = DATAS[RAW_FAKTUR.periode]["QTY_BUFFER"] + 1;
      }
      if (RAW_FAKTUR.fp_state == 0) {
        DATAS[RAW_FAKTUR.periode]["QTY_NOT_USED"] = DATAS[RAW_FAKTUR.periode]["QTY_NOT_USED"] + 1;
      }
      DATAS[RAW_FAKTUR.periode]["TOTAL"] = DATAS[RAW_FAKTUR.periode]["TOTAL"] + 1;

    }
    result.message = "Success !";
    result.datas = DATAS;

    return result;
  }

  @get('/faktur-pajak/summary/request', {
    responses: {
      '200': {
        description: 'Array of Merchant Product model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(NoFakturPajak, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async findMerchantNoFakturSummaryReq(
    @param.filter(NoFakturPajak) filter?: Filter<NoFakturPajak>,
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};

    var FAKTUR_PAJAK = await this.noFakturPajakRepository.find(filter);
    var DATAS: any; DATAS = {};
    for (var i in FAKTUR_PAJAK) {
      var RAW_FAKTUR = FAKTUR_PAJAK[i];
      var DATE_TIME = RAW_FAKTUR.at_create.split("-")[1];

      if (DATAS[RAW_FAKTUR.at_create] == undefined) {
        DATAS[RAW_FAKTUR.at_create] = {ID: RAW_FAKTUR.at_create, PERIODE: RAW_FAKTUR.periode, QTY_USED: 0, QTY_BUFFER: 0, QTY_NOT_BUFFER: 0, TOTAL: 0, TANGGAL: DATE_TIME};
      }

      if (RAW_FAKTUR.fp_state == 1) {
        DATAS[RAW_FAKTUR.at_create]["QTY_USED"] = DATAS[RAW_FAKTUR.at_create]["QTY_USED"] + 1;
      }
      if (RAW_FAKTUR.fp_buffer == true) {
        DATAS[RAW_FAKTUR.at_create]["QTY_BUFFER"] = DATAS[RAW_FAKTUR.at_create]["QTY_BUFFER"] + 1;
      } else {
        DATAS[RAW_FAKTUR.at_create]["QTY_NOT_BUFFER"] = DATAS[RAW_FAKTUR.at_create]["QTY_NOT_BUFFER"] + 1;
      }

      DATAS[RAW_FAKTUR.at_create]["TOTAL"] = DATAS[RAW_FAKTUR.at_create]["TOTAL"] + 1;

    }
    result.message = "Success !";
    result.datas = DATAS;
    return result;
  }


  @post('/merchant-faktur-pajak/register-nomor/cancel-no-faktur', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: CancelRequestBody
          }
        },
      },
    },
  })
  async cancelNoFakturMerchant(
    @requestBody(CancelRequestBody) cancelNoFaktur: {ID: string, COUNT_CANCEL: number, FP_BUFFER: boolean},
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};


    var FAKTUR_PAJAK = await this.noFakturPajakRepository.find({order: ["id DESC"], where: {and: [{at_create: cancelNoFaktur.ID}, {fp_buffer: cancelNoFaktur.FP_BUFFER}, {fp_state: 0}]}});

    var ErrorCount = 0;
    var SuccessCount = 0;
    for (var i = 0; i < cancelNoFaktur.COUNT_CANCEL; i++) {
      var RAW_FAKTUR_PAJAK = FAKTUR_PAJAK[i];
      try {
        await this.noFakturPajakRepository.deleteById(RAW_FAKTUR_PAJAK.id);
        SuccessCount++;
      } catch (error) {
        ErrorCount++;
      }
    }
    result.succes = true;
    result.message = "Success Cancel No Faktur : " + SuccessCount + ", Error Cancel : " + ErrorCount;
    return result;
  }


  @post('/merchant-faktur-pajak/register-nomor', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: RegisterNoFakturMerchantRequestBody
          }
        },
      },
    },
  })
  async registerNoFakturMerchant(
    @requestBody(RegisterNoFakturMerchantRequestBody) registerNoFaktur: {QUOTE_ALLOCATION: string, START_NUMBER: number, END_NUMBER: number, REMARKS: string, PERIODE: string, FP_BUFFER: boolean},
  ): Promise<Object> {

    var returnMs: any; returnMs = {success: true, message: ""};
    var CreateAllow = true;
    var DuplicateNo = "";
    var DateCreate = ControllerConfig.onCurrentTime();
    for (var i = registerNoFaktur.START_NUMBER; i <= registerNoFaktur.END_NUMBER; i++) {
      var Label = registerNoFaktur.QUOTE_ALLOCATION + "-" + registerNoFaktur.PERIODE.substring(registerNoFaktur.PERIODE.length - 2) + "." + i;
      var infoFaktur = await this.noFakturPajakRepository.count({and: [{periode: registerNoFaktur.PERIODE}, {no_faktur_pajak: Label}]});
      if (infoFaktur.count > 0) {
        CreateAllow = false;
        DuplicateNo = Label;
        break;
      }
    }

    var maxNumber = await this.noFakturPajakRepository.findOne({order: ['number_fp DESC'], where: {and: [{periode: registerNoFaktur.PERIODE}, {fp_quota_alocate: registerNoFaktur.QUOTE_ALLOCATION}]}})

    var RANDOME_ID = ControllerConfig.RANDOME_ID(8);
    var CreateDates = DateCreate.toString();
    if (CreateAllow) {
      var no = 0;
      for (var i = registerNoFaktur.START_NUMBER; i <= registerNoFaktur.END_NUMBER; i++) {
        var Label = registerNoFaktur.QUOTE_ALLOCATION + "-" + registerNoFaktur.PERIODE.substring(registerNoFaktur.PERIODE.length - 2) + "." + i;
        await this.noFakturPajakRepository.create({
          no_faktur_pajak: Label,
          number_fp: i,
          periode: registerNoFaktur.PERIODE,
          at_create: RANDOME_ID + "-" + CreateDates,
          remarks: registerNoFaktur.REMARKS,
          fp_state: 0,
          fp_buffer: registerNoFaktur.FP_BUFFER,
          fp_quota_alocate: registerNoFaktur.QUOTE_ALLOCATION,
          type_faktur: 1,
          no_faktur_link: undefined,
          no_invoice_link: undefined,
          description: undefined,
          at_used: undefined
        });
        no++;
        returnMs.message = "success register no faktur pajak, count register is [" + no + "]";
      }
    } else {
      returnMs.success = false;
      returnMs.message = "error register no faktur pajak, because no faktur pajak [" + DuplicateNo + "] is duplicate, Last no faktur pajak is : [" + maxNumber?.number_fp + "]";
    }
    return returnMs;
  }



  @get('/faktur-pajak/transaksi/views', {
    responses: {
      '200': {
        description: 'Array of Merchant Product model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(MerchantProductMaster, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async findMerchantFakturView(
    @param.filter(MerchantProductMaster) filter?: Filter<MerchantProductMaster>,
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};

    var MERCHANT = await this.merchantRepository.find();
    var DATA_MERCHANT: any; DATA_MERCHANT = {};
    for (var MI in MERCHANT) {
      var RAW_MERCHANT = MERCHANT[MI];
      var ID = RAW_MERCHANT.M_ID == undefined ? -1 : RAW_MERCHANT.M_ID;
      if (DATA_MERCHANT[ID] == undefined) {
        DATA_MERCHANT[ID] = {};
      }
      DATA_MERCHANT[ID] = RAW_MERCHANT;
    }

    var MERCHANT_PRODUCT = await this.merchantProductRepository.find();
    var DATA_MERCHANT_PRODUCT: any; DATA_MERCHANT_PRODUCT = {};
    for (var MP_I in MERCHANT_PRODUCT) {
      var RAW_MERCHANT_PRODUCT = MERCHANT_PRODUCT[MP_I];
      var MP_ID = RAW_MERCHANT_PRODUCT.MP_ID == undefined ? -1 : RAW_MERCHANT_PRODUCT.MP_ID;
      if (DATA_MERCHANT_PRODUCT[MP_ID] == undefined) {
        DATA_MERCHANT_PRODUCT[MP_ID] = {};
      }
      DATA_MERCHANT_PRODUCT[MP_ID] = RAW_MERCHANT_PRODUCT;
    }


    var TRANSAKSI = await this.merchantProductMasterRepository.find(filter);
    var DATAS: any; DATAS = [];
    var NO = 1;
    for (var i in TRANSAKSI) {
      var RAW_TRANSAKSI = TRANSAKSI[i];
      var DATA: any; DATA = {};

      var DOC = await this.merchantProductDocumentRepository.find({where: {and: [{MPM_ID: RAW_TRANSAKSI.MPM_ID}, {AT_FLAG: 1}]}});
      var DATA_DOC: any; DATA_DOC = {};
      for (var DOC_I in DOC) {
        var RAW_DOC = DOC[DOC_I];
        if (DATA_DOC[RAW_DOC.MPDOC_TYPE] == undefined) {
          DATA_DOC[RAW_DOC.MPDOC_TYPE] = [];
        }
        DATA_DOC[RAW_DOC.MPDOC_TYPE].push({ID: RAW_DOC.MPDOC_ID, MPDOC_NAME: RAW_DOC.MPDOC_NAME, MPM_ID: RAW_DOC.MPDOC_ID});
      }


      DATA["INFO"] = {
        PERIODE: RAW_TRANSAKSI.PERIODE,
        M_CODE: DATA_MERCHANT[RAW_TRANSAKSI.M_ID].M_CODE,
        MERCHANT: DATA_MERCHANT[RAW_TRANSAKSI.M_ID].M_LEGAL_NAME.toUpperCase(),
        INVOICE_NUMBER: RAW_TRANSAKSI.MPM_NO_INVOICE,
        INVOICE_ATTACH: DATA_DOC[3] == undefined ? [] : DATA_DOC[3],
        FAKTUR_NUMBER: RAW_TRANSAKSI.MPM_NO_FAKTUR,
        FAKTUR_ATTACH: DATA_DOC[5] == undefined ? [] : DATA_DOC[5],
        BAR_NUMBER: RAW_TRANSAKSI.MPM_NO_BAR,
        BAR_ATTACH: DATA_DOC[2] == undefined ? [] : DATA_DOC[2],
      }

      var perfix: string; perfix = "";
      var fakturNo: string; fakturNo = "";
      var fakturTxt = RAW_TRANSAKSI.MPM_NO_FAKTUR == null || RAW_TRANSAKSI.MPM_NO_FAKTUR == undefined || RAW_TRANSAKSI.MPM_NO_FAKTUR == "" ? "" : RAW_TRANSAKSI.MPM_NO_FAKTUR;
      if (fakturTxt != "") {
        var explodeNoFaktur = fakturTxt.split(".");
        if (explodeNoFaktur.length > 0) {
          perfix = explodeNoFaktur[0];
        }
        fakturNo = fakturTxt.substring(perfix.length, fakturTxt.length).split(".").join("").split("-").join("");
      }

      var PeriodeTrx = RAW_TRANSAKSI.PERIODE.split("-");
      var d = new Date(RAW_TRANSAKSI.PERIODE + "-" + "26");
      var dateLast = new Date(parseInt(PeriodeTrx[0]), parseInt(PeriodeTrx[1]), 0);
      var dateInvZero = "00";
      var dateInv = dateLast.getDate();
      var dateInvoice = dateInvZero.substring(dateInv.toString().length, 2) + dateLast.getDate() + "/" + dateInvZero.substring((dateLast.getMonth() + 1).toString().length, 2) + (dateLast.getMonth() + 1) + "/" + dateLast.getFullYear();

      var PERIODE_SPLIT = RAW_TRANSAKSI.PERIODE.split("-");
      DATA["FK"] = {
        PERFIX: perfix.substring(0, 2),
        NO_FAKTUR: fakturNo,
        BULAN: PERIODE_SPLIT[1],
        TAHUN: PERIODE_SPLIT[0],
        TANGGAL: dateInvoice,
        NPWP: DATA_MERCHANT[RAW_TRANSAKSI.M_ID].M_NPWP,
        MERCHANT: DATA_MERCHANT[RAW_TRANSAKSI.M_ID].M_LEGAL_NAME.toUpperCase(),
        ADDRESS: DATA_MERCHANT[RAW_TRANSAKSI.M_ID].M_ALAMAT_NPWP.toUpperCase(),
        DPP: RAW_TRANSAKSI.MPM_DPP,
        PPN: RAW_TRANSAKSI.MPM_PPN,
        REFERENSI: RAW_TRANSAKSI.MPM_NO_INVOICE
      }

      var monthSet = parseInt(PERIODE_SPLIT[1]) - 1;
      var dateText = ControllerConfig.onGetMonth(monthSet);
      DATA["OF"] = {
        NO: NO,
        NAME: DATA_MERCHANT_PRODUCT[RAW_TRANSAKSI.MP_ID].MP_LABEL + dateText.toString().toUpperCase() + " " + PERIODE_SPLIT[0],
        SATUAN: RAW_TRANSAKSI.MPM_DPP,
        JUMLAH: 1,
        TOTAL: RAW_TRANSAKSI.MPM_TOTAL,
        DPP: RAW_TRANSAKSI.MPM_TOTAL,
        PPN: RAW_TRANSAKSI.MPM_PPN
      }
      DATAS.push(DATA);
      NO++;
    }

    result.message = "Success !";
    result.datas = DATAS;
    return result;
  }


}
