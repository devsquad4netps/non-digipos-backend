// Uncomment these imports to begin using these cool features!

import {repository} from '@loopback/repository';
import {post, requestBody} from '@loopback/rest';
import {AdMasterRepository, DataDigiposDetailRepository, DataDigiposRepository, DataSharePercentRepository, DataSumarryRepository} from '../repositories';

// import {inject} from '@loopback/core';
export const ProcessDashboardPeriode = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['periode'],
        properties: {
          periode: {
            type: 'string',
          }
        }
      }
    },
  },
};

export class DashboardActionController {
  constructor(
    @repository(DataSumarryRepository)
    public dataSumarryRepository: DataSumarryRepository,
    @repository(DataDigiposDetailRepository)
    public dataDigiposDetailRepository: DataDigiposDetailRepository,
    @repository(DataDigiposRepository)
    public dataDigiposRepository: DataDigiposRepository,
    @repository(AdMasterRepository)
    public adMasterRepository: AdMasterRepository,
    @repository(DataSharePercentRepository)
    public dataSharePercentRepository: DataSharePercentRepository,
  ) { }

  @post('/dashboard/dashboard-model-1', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: ProcessDashboardPeriode
          }
        },
      },
    },
  })
  async updateValidateBa(
    @requestBody(ProcessDashboardPeriode) dashboarModel: {periode: string},
  ): Promise<Object> {
    var result: any; result = {
      ImportRow: 0,
      ImportAD: 0,
      MigrasiRecharge: 0,
      MigrasiPPOB: 0,
      ShareRechargeA: 0,
      ShareRechargeB: 0,
      SharePpobA: 0,
      SharePpobB: 0,
      SharePercentA: 0,
      SharePercentB: 0,
      SharePercentPpobA: 0,
      SharePercentPpobB: 0,
      VALID_DIGIPOS_BA_A: 0,
      VALID_DIGIPOS_BA_b: 0,
      VALID_DIGIPOS_BA: 0,
      REJECT_DIGIPOS_BA_A: 0,
      REJECT_DIGIPOS_BA_B: 0,
      REJECT_DIGIPOS_BA: 0,
      VALID_DIGIPOS_BU_A: 0,
      VALID_DIGIPOS_BU_B: 0,
      VALID_DIGIPOS_BU: 0,
      REJECT_DIGIPOS_BU_A: 0,
      REJECT_DIGIPOS_BU_B: 0,
      REJECT_DIGIPOS_BU: 0,
      GENERATE_BAR_A: 0,
      GENERATE_BAR_B: 0,
      VALID_BAR_A: 0,
      VALID_BAR_B: 0,
      SIGN_BAR_A: 0,
      SIGN_BAR_B: 0,
      SHARE_BAR_A: 0,
      SHARE_BAR_B: 0,
      VALID_INVOICE: 0,
      GENERATE_INVOICE_BEFORE_SIGN: 0,
      SHARE_INVOICE_BEFORE_SIGN: 0,
      SET_INVOICE_SIGN: 0,
      SHARE_INVOICE_SIGN: 0,
      MAPPING_FAKTUR_PAJAK: 0,
      SET_FAKTUR_PAJAK: 0,
      SHARE_FAKTUR_PAJAK: 0,
      COUNT_FPJP: 0,
      VALID_FPJP_INVOICE: 0,
      GENERATE_NO_FPJP_INVOICE: 0,
      VALID_FPJP_PAY_INVOICE: 0,
      NEED_REVIEW_FPJP_PAY_INVOICE: 0,
      PAYMENT_FPJP_INVOICE: 0,
      VALID_FPJP_NOTA: 0,
      GENERATE_NO_FPJP_NOTA: 0,
      VALID_FPJP_PAY_NOTA: 0,
      NEED_REVIEW_FPJP_PAY_NOTA: 0,
      PAYMENT_FPJP_NOTA: 0,
      COUNT_BUKPOT_AD: 0,
      NOT_SET_BUKPOT: 0,
      SET_BUKPOT: 0,
      SHARE_BUKPOT: 0,
      COUNT_DISPUTE_AD: 0,
      COUNT_DISPUTE: 0,
      COUNT_DISPUTE_RECHARGE_A: 0,
      COUNT_DISPUTE_RECHARGE_B: 0,
      COUNT_DISPUTE_PPOB_B: 0,
      NEED_VALIDATE_DISPUTE_RECHARGE_A: 0,
      VALID_DISPUTE_RECHARGE_A: 0,
      REJECT_DISPUTE_RECHARGE_A: 0,
      NEED_VALIDATE_DISPUTE_RECHARGE_B: 0,
      VALID_DISPUTE_RECHARGE_B: 0,
      REJECT_DISPUTE_RECHARGE_B: 0,
      NEED_VALIDATE_DISPUTE_PPOB_A: 0,
      VALID_DISPUTE_PPOB_A: 0,
      REJECT_DISPUTE_PPOB_A: 0,
      MASTER_ACTIVE: 0,
      MASTER_DEACTIVE: 0,
      MIGRASI_DATA: 0,
      TO_DO: []
    };

    var migrasiPPOB = await this.dataDigiposDetailRepository.count({and: [{type_trx: "PPOB"}, {trx_date_group: dashboarModel.periode}]});
    var migrasiRECHARGE = await this.dataDigiposDetailRepository.count({and: [{type_trx: "RECHARGE"}, {trx_date_group: dashboarModel.periode}]});


    result.MigrasiRecharge = migrasiRECHARGE.count;
    result.MigrasiPPOB = migrasiPPOB.count;

    //Import Row
    var rawImportSummary = await this.dataSumarryRepository.count({periode: dashboarModel.periode});
    result.ImportRow = rawImportSummary.count;
    var dataDigiposJmlAd = await this.dataDigiposRepository.execute("SELECT trx_date_group , type_trx, SUM(mdr_fee_amount) MDR_A, SUM(paid_inv_amount) MDR_B, COUNT(DISTINCT(ad_code)) ImportAD FROM DataDigiposDetail WHERE trx_date_group = '" + dashboarModel.periode + "' GROUP BY type_trx, trx_date_group");
    var ImportAd = 0;
    for (var i in dataDigiposJmlAd) {
      var rawDigipos = dataDigiposJmlAd[i];
      if (ImportAd == 0) {
        ImportAd = rawDigipos.ImportAD;
      }
      if (rawDigipos.ImportAD > ImportAd) {
        ImportAd = rawDigipos.ImportAD;
      }
      result.ImportAD = ImportAd;
      if (rawDigipos.type_trx.toUpperCase() == "RECHARGE") {
        result.ShareRechargeA = rawDigipos.MDR_A;
        result.ShareRechargeB = rawDigipos.MDR_B;
      }
      if (rawDigipos.type_trx.toUpperCase() == "PPOB") {
        result.SharePpobA = rawDigipos.MDR_A;
        result.SharePpobB = rawDigipos.MDR_B;
      }
    }

    var SharePercent = await this.dataSharePercentRepository.findOne({where: {periode: dashboarModel.periode}});
    if (SharePercent != null) {
      result.SharePercentA = SharePercent.fee_recharce_share * 100;
      result.SharePercentB = SharePercent.paid_recharge_share * 100;
      result.SharePercentPpobA = SharePercent.fee_ppob_share * 100;
      result.SharePercentPpobB = SharePercent.paid_ppob_share * 100;
    }

    // REPORT DIGIPOS
    var SQL_REPORT_DIGIPOS = "SELECT " +
      "trx_date_group ," +
      "ar_stat," +
      "SUM(if(ar_valid_a >= 1,1,0)) VALID_DIGIPOS_BA_A," +
      "SUM(if(ar_valid_b >= 1,1,0)) VALID_DIGIPOS_BA_B," +
      "SUM(if(ar_stat >= 1,1,0)) VALID_DIGIPOS_BA," +
      "SUM(if(ar_valid_a = -1,1,0)) REJECT_DIGIPOS_BA_A," +
      "SUM(if(ar_valid_b = -1,1,0)) REJECT_DIGIPOS_BA_B," +
      "SUM(if(ar_stat = -1,1,0)) REJECT_DIGIPOS_BA," +
      "SUM(if(ar_valid_a >= 2,1,0)) VALID_DIGIPOS_BU_A," +
      "SUM(if(ar_valid_b >= 2,1,0)) VALID_DIGIPOS_BU_B," +
      "SUM(if(ar_stat >= 2,1,0)) VALID_DIGIPOS_BU," +
      "SUM(if(ar_valid_a = -2,1,0)) REJECT_DIGIPOS_BU_A," +
      "SUM(if(ar_valid_b = -2,1,0)) REJECT_DIGIPOS_BU_B," +
      "SUM(if(ar_stat = -2,1,0)) REJECT_DIGIPOS_BU," +
      "SUM(if(generate_file_a = 1,1,0)) GENERATE_BAR_A," +
      "SUM(if(generate_file_b = 1,1,0)) GENERATE_BAR_B," +
      "SUM(if(acc_file_ba_a = 1,1,0)) VALID_BAR_A," +
      "SUM(if(acc_file_ba_b = 1,1,0)) VALID_BAR_B," +
      "SUM(if(sign_off_a = 1,1,0)) SIGN_BAR_A," +
      "SUM(if(sign_off_b = 1,1,0)) SIGN_BAR_B," +
      "SUM(if(google_drive_a = 1,1,0)) SHARE_BAR_A," +
      "SUM(if(google_drive_b = 1,1,0)) SHARE_BAR_B," +
      "SUM(if(acc_file_invoice = 1,1,0)) VALID_INVOICE," +
      "SUM(if(generate_file_invoice = 1,1,0)) GENERATE_INVOICE_BEFORE_SIGN," +
      "SUM(if(distribute_file_invoice = 1,1,0)) SHARE_INVOICE_BEFORE_SIGN," +
      "SUM(if(distribute_invoice_ttd = 1,1,0)) SET_INVOICE_SIGN," +
      "SUM(if(distribute_invoice_ttd = 2,1,0)) SHARE_INVOICE_SIGN," +
      "SUM(if(vat_number != '' || vat_number != null,1,0)) MAPPING_FAKTUR_PAJAK," +
      "SUM(if(vat_distribute = 1,1,0)) SET_FAKTUR_PAJAK," +
      "SUM(if(vat_distribute = 2,1,0)) SHARE_FAKTUR_PAJAK " +
      "FROM " +
      "DataDigipos " +
      "WHERE " +
      "trx_date_group = '" + dashboarModel.periode + "' " +
      "GROUP BY " +
      "trx_date_group";
    var dataReportDigipos = await this.dataDigiposRepository.execute(SQL_REPORT_DIGIPOS);
    for (var i in dataReportDigipos) {
      var rawReportDigipos = dataReportDigipos[i];
      for (var j in rawReportDigipos) {
        result[j] = rawReportDigipos[j];
      }
    }

    result.VALID_DIGIPOS_BA = (result.VALID_DIGIPOS_BA_A + result.VALID_DIGIPOS_BA_B) / 2;
    result.REJECT_DIGIPOS_BA = (result.REJECT_DIGIPOS_BA_A + result.REJECT_DIGIPOS_BA_B) / 2;
    result.VALID_DIGIPOS_BU = (result.VALID_DIGIPOS_BU_A + result.VALID_DIGIPOS_BU_B) / 2;
    result.REJECT_DIGIPOS_BU = (result.REJECT_DIGIPOS_BU_A + result.REJECT_DIGIPOS_BU_B) / 2;

    //FPJP Report
    var SQL_REPORT_FPJP = "SELECT " +
      "periode," +
      "type_trx," +
      "COUNT(type_trx) COUNT_FPJP," +
      "SUM(if(state=0 or state=1 or state=5,0,1)) VALID_FPJP," +
      "SUM(if(transaction_code = '' or transaction_code = null,0,1)) GENERATE_NO_FPJP," +
      "SUM(if(fpjp_hold_state = 1,0,1)) VALID_FPJP_PAY," +
      "SUM(if(fpjp_hold_state = 1 and fpjp_note_state != 1,1,0)) NEED_REVIEW_FPJP_PAY," +
      "SUM(if(state = 4,1,0)) PAYMENT_FPJP " +
      "FROM " +
      "DataFpjp " +
      "WHERE " +
      "periode = '" + dashboarModel.periode + "' AND state <> 0 " +
      "GROUP BY  " +
      "type_trx," +
      "periode";
    var dataReportFpjp = await this.dataDigiposRepository.execute(SQL_REPORT_FPJP);
    for (var i in dataReportFpjp) {

      var rawFpjp = dataReportFpjp[i];
      result.COUNT_FPJP = rawFpjp.COUNT_FPJP;

      //INVOICE FPJP
      if (rawFpjp.type_trx == 1) {
        result.VALID_FPJP_INVOICE = rawFpjp.VALID_FPJP;
        result.GENERATE_NO_FPJP_INVOICE = rawFpjp.GENERATE_NO_FPJP;
        result.VALID_FPJP_PAY_INVOICE = rawFpjp.VALID_FPJP_PAY;
        result.NEED_REVIEW_FPJP_PAY_INVOICE = rawFpjp.NEED_REVIEW_FPJP_PAY;
        result.PAYMENT_FPJP_INVOICE = rawFpjp.PAYMENT_FPJP;
      }

      //NOTA FPJP
      if (rawFpjp.type_trx == 2) {
        result.VALID_FPJP_NOTA = rawFpjp.VALID_FPJP;
        result.GENERATE_NO_FPJP_NOTA = rawFpjp.GENERATE_NO_FPJP;
        result.VALID_FPJP_PAY_NOTA = rawFpjp.VALID_FPJP_PAY;
        result.NEED_REVIEW_FPJP_PAY_NOTA = rawFpjp.NEED_REVIEW_FPJP_PAY;
        result.PAYMENT_FPJP_NOTA = rawFpjp.PAYMENT_FPJP;
      }
    }

    //DISTRIBUTE Bukpot
    var SQL_REPORT_BUKPOT = "SELECT " +
      "periode, " +
      "COUNT(DISTINCT ad_code) COUNT_BUKPOT_AD, " +
      "SUM(if(bukpot_status=0 ,1,0)) NOT_SET_BUKPOT, " +
      "SUM(if(bukpot_status=1 or bukpot_attachment_set ,1,0)) SET_BUKPOT, " +
      "SUM(if(bukpot_status=2 ,1,0)) SHARE_BUKPOT " +
      "FROM  " +
      "DataDistributeBukpot " +
      "WHERE " +
      "periode = '" + dashboarModel.periode + "' " +
      "GROUP BY  " +
      "periode ";

    var dataReportBukpot = await this.dataDigiposRepository.execute(SQL_REPORT_BUKPOT);
    for (var i in dataReportBukpot) {
      var rawBukpot = dataReportBukpot[i];
      for (var j in rawBukpot) {
        result[j] = rawBukpot[j];
      }
    }

    //DIPUTE Report
    var SQL_REPORT_DISPUTE = "SELECT  " +
      "periode, " +
      "type_trx, " +
      "COUNT(DISTINCT ad_code) COUNT_DISPUTE_AD, " +
      "SUM(if(type_trx !=0 ,1,0)) COUNT_DISPUTE, " +
      "SUM(if(type_trx =1 ,1,0)) COUNT_DISPUTE_RECHARGE_A, " +
      "SUM(if(type_trx =2 ,1,0)) COUNT_DISPUTE_RECHARGE_B, " +
      "SUM(if(type_trx =3 ,1,0)) COUNT_DISPUTE_PPOB_B, " +
      "SUM(if(type_trx =1 and dispute_status=1 ,1,0)) NEED_VALIDATE_DISPUTE_RECHARGE_A, " +
      "SUM(if(type_trx =1 and dispute_status=2 ,1,0)) VALID_DISPUTE_RECHARGE_A, " +
      "SUM(if(type_trx =1 and dispute_status=3 ,1,0)) REJECT_DISPUTE_RECHARGE_A, " +
      "SUM(if(type_trx =2 and dispute_status=1 ,1,0)) NEED_VALIDATE_DISPUTE_RECHARGE_B, " +
      "SUM(if(type_trx =2 and dispute_status=2 ,1,0)) VALID_DISPUTE_RECHARGE_B, " +
      "SUM(if(type_trx =2 and dispute_status=3 ,1,0)) REJECT_DISPUTE_RECHARGE_B, " +
      "SUM(if(type_trx =3 and dispute_status=1 ,1,0)) NEED_VALIDATE_DISPUTE_PPOB_A, " +
      "SUM(if(type_trx =3 and dispute_status=2 ,1,0)) VALID_DISPUTE_PPOB_A, " +
      "SUM(if(type_trx =3 and dispute_status=3 ,1,0)) REJECT_DISPUTE_PPOB_A " +
      "FROM  " +
      "DataDispute " +
      "WHERE " +
      "periode = '" + dashboarModel.periode + "' and dispute_status != 0 " +
      "GROUP BY  " +
      "periode";
    var dataReportDispute = await this.dataDigiposRepository.execute(SQL_REPORT_DISPUTE);
    for (var i in dataReportDispute) {
      var rawDispute = dataReportDispute[i];
      for (var j in rawDispute) {
        result[j] = rawDispute[j];
      }
    }


    // MASTER_ACTIVE : 0,
    // MASTER_DEACTIVE : 0,
    // MIGRASI_DATA: 0,
    var ad_actived = await this.adMasterRepository.count({and: [{ad_status: true}]});
    var ad_dactived = await this.adMasterRepository.count({and: [{ad_status: false}]});
    var migrasi = await this.dataDigiposRepository.count({and: [{trx_date_group: dashboarModel.periode}]});

    result.MASTER_ACTIVE = ad_actived.count;
    result.MASTER_DEACTIVE = ad_dactived.count;
    result.MIGRASI_DATA = migrasi.count;


    if (result.NEED_VALIDATE_DISPUTE_RECHARGE_A != 0) {
      result.TO_DO.push({type: 1, message: "Dispute Recharge AD A Butuh Di Validasi (" + result.NEED_VALIDATE_DISPUTE_RECHARGE_A + ")"});
    }

    if (result.NEED_VALIDATE_DISPUTE_RECHARGE_B != 0) {
      result.TO_DO.push({type: 2, message: "Dispute Recharge AD B Butuh Di Validasi (" + result.NEED_VALIDATE_DISPUTE_RECHARGE_B + ")"});
    }

    if (result.NEED_VALIDATE_DISPUTE_PPOB_A != 0) {
      result.TO_DO.push({type: 3, message: "Dispute PPOB AD A Butuh Di Validasi (" + result.NEED_VALIDATE_DISPUTE_PPOB_A + ")"});
    }

    if (result.NEED_REVIEW_FPJP_PAY_INVOICE != 0) {
      result.TO_DO.push({type: 4, message: "Data FPJP Invoice Butuh Di Checking (" + result.NEED_REVIEW_FPJP_PAY_INVOICE + ")"});
    }

    if (result.NEED_REVIEW_FPJP_PAY_INVOICE != 0) {
      result.TO_DO.push({type: 5, message: "Data FPJP Nota Debit Butuh Di Checking (" + result.NEED_REVIEW_FPJP_PAY_NOTA + ")"});
    }

    if (result.REJECT_DIGIPOS_BA != 0) {
      result.TO_DO.push({type: 6, message: "Lakukan Migrasi, Data Digipos Rejected : BA (" + result.REJECT_DIGIPOS_BA + ")"});
    }

    if (result.REJECT_DIGIPOS_BU != 0) {
      result.TO_DO.push({type: 7, message: "Lakukan Migrasi, Data Digipos Rejected : BU (" + result.REJECT_DIGIPOS_BU + ")"});
    }

    if ((result.ImportAD - result.VALID_DIGIPOS_BA) != 0) {
      result.TO_DO.push({type: 8, message: "Lakukan Validasi Data Di BA (" + (result.ImportAD - result.VALID_DIGIPOS_BA) + ")"});
    }





    return result;
  }


}
