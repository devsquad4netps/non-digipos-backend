import {authenticate} from '@loopback/authentication';
import {inject} from '@loopback/core';
import {Filter, FilterExcludingWhere, repository} from '@loopback/repository';
import {get, getModelSchemaRef, param, post, requestBody} from '@loopback/rest';
import {SecurityBindings, securityId, UserProfile} from '@loopback/security';
import {MerchantProductCondition} from '../models';
import {MerchantProductConditionRepository, MerchantProductDatastoreRepository, MerchantProductGroupRepository, MerchantProductVariableRepository, MpvTypeKeyRepository, MpvTypeRepository} from '../repositories';
import {MerchantProductQueryRepository} from '../repositories/merchant-product-query.repository';
import {MerchantProductRepository} from '../repositories/merchant-product.repository';
import {MerchantRepository} from '../repositories/merchant.repository';
import {UserRepository} from '../repositories/user.repository';
import {ControllerConfig} from './controller.config';


@authenticate('jwt')
export class MerchantConditionActionController {
  constructor(
    @repository(MerchantRepository)
    public merchantRepository: MerchantRepository,
    @repository(MerchantProductRepository)
    public merchantProductRepository: MerchantProductRepository,
    @repository(MerchantProductGroupRepository)
    public merchantProductGroupRepository: MerchantProductGroupRepository,
    @repository(MerchantProductVariableRepository)
    public merchantProductVariableRepository: MerchantProductVariableRepository,
    @repository(MerchantProductDatastoreRepository)
    public merchantProductDatastoreRepository: MerchantProductDatastoreRepository,
    @repository(MpvTypeRepository)
    public mpvTypeRepository: MpvTypeRepository,
    @repository(MpvTypeKeyRepository)
    public mpvTypeKeyRepository: MpvTypeKeyRepository,
    @repository(MerchantProductQueryRepository)
    public merchantProductQueryRepository: MerchantProductQueryRepository,
    @repository(MerchantProductConditionRepository)
    public merchantProductConditionRepository: MerchantProductConditionRepository,
    @inject(SecurityBindings.USER)
    public currentUserProfile: UserProfile,
    @repository(UserRepository)
    public userRepository: UserRepository,
  ) { }


  @post('/merchant-condition-action/create', {
    responses: {
      '200': {
        description: 'Create merchant product condition model instance',
        content: {'application/json': {schema: getModelSchemaRef(MerchantProductCondition)}},
      },
    },
  })
  async createMerchantProductCondition(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(MerchantProductCondition, {
            title: 'MERCHANT_CONDITION_CREATE',
            exclude: ['MPC_ID', 'AT_CREATE', 'AT_UPDATE', 'AT_WHO'],
          }),
        },
      },
    })
    dataMerchantProductCondition: any,
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};

    var REQUIRED_FIELDS = ['MPV_ID', 'MPC_OPERATOR', 'MPC_VALUE', 'MPC_GROUP', 'M_ID', 'MP_ID', 'MPG_ID', 'AT_FLAG'];
    var BODY_PARAM: any; BODY_PARAM = {};
    for (var i in dataMerchantProductCondition) {
      if (REQUIRED_FIELDS.indexOf(i) != -1) {
        REQUIRED_FIELDS.splice(REQUIRED_FIELDS.indexOf(i), 1);
      }
      BODY_PARAM[i] = dataMerchantProductCondition[i];
    }

    if (REQUIRED_FIELDS.length == 0) {
      var MERCHANT = await this.merchantRepository.count({M_ID: BODY_PARAM["M_ID"]});
      var MERCHANT_PRODUCT = await this.merchantProductRepository.count({and: [{MP_ID: BODY_PARAM["MP_ID"]}, {M_ID: BODY_PARAM["M_ID"]}]});
      var MERCHANT_PRODUCT_GROUP = await this.merchantProductGroupRepository.count({and: [{MPG_ID: BODY_PARAM["MPG_ID"]}, {MP_ID: BODY_PARAM["MP_ID"]}, {M_ID: BODY_PARAM["M_ID"]}]});
      var MERCHANT_VARIABLE = await this.merchantProductVariableRepository.count({and: [{MP_ID: BODY_PARAM["MP_ID"]}, {M_ID: BODY_PARAM["M_ID"]}, {MPV_ID: BODY_PARAM.MPV_ID}]});


      if (MERCHANT.count > 0) {
        if (MERCHANT_PRODUCT.count > 0) {
          if (MERCHANT_PRODUCT_GROUP.count > 0) {
            if (MERCHANT_VARIABLE.count > 0) {
              // if (MVP_TYPE_KEY.count > 0) {
              BODY_PARAM["AT_CREATE"] = ControllerConfig.onCurrentTime().toString();
              BODY_PARAM["AT_UPDATE"] = undefined;
              BODY_PARAM["AT_WHO"] = USER_ID;
              var ACC_OPERATOR = ["=", "<=", ">=", "<>", "<", ">", "LIKE", "NOT IN", "IN"];
              if (ACC_OPERATOR.indexOf(BODY_PARAM["MPC_OPERATOR"]) != -1) {

                if (BODY_PARAM["MPC_OPERATOR"] == "NOT IN" || BODY_PARAM["MPC_OPERATOR"] == "IN") {
                  try {
                    var LIST_IN = BODY_PARAM["MPC_VALUE"].split(";");
                    if (LIST_IN.length == 0) {
                      result.success = false;
                      result.message = "Error, please select format value '1';'2';'dst'";
                      return result;
                    }
                  } catch (e) {
                    result.success = false;
                    result.message = "Error, please select format value ['1','2','dst']";
                    return result;
                  }
                }

                try {

                  var PRODUCT_CONDITION = await this.merchantProductConditionRepository.create(BODY_PARAM);
                  result.success = true;
                  result.message = "Success create condition !";
                  result.datas = PRODUCT_CONDITION;

                } catch (error) {
                  result.success = false;
                  result.message = error;
                }

              } else {
                result.success = false;
                result.message = "Error, Operator  " + BODY_PARAM["MPC_OPERATOR"] + " not found !";
              }
            } else {
              result.success = false;
              result.message = "Error, merchant variable  " + BODY_PARAM["MPV_ID"] + " not found !";
            }
          } else {
            result.success = false;
            result.message = "Error, merchant product group " + BODY_PARAM["MP_ID"] + " not found !";
          }
        } else {
          result.success = false;
          result.message = "Error, merchant product " + BODY_PARAM["MP_ID"] + " not found !";
        }
      } else {
        result.success = false;
        result.message = "Error, merchant " + BODY_PARAM["M_ID"] + " not found !";
      }
    } else {
      result.success = false;
      result.message = "Error, Field " + JSON.stringify(REQUIRED_FIELDS) + " must entry !";
    }
    return result;
  }

  @post('/merchant-condition-action/update/{id}', {
    responses: {
      '200': {
        description: 'PRODUCT CONDITION UPDATE model instance',
        content: {'application/json': {schema: getModelSchemaRef(MerchantProductCondition)}},
      },
    },
  })
  async updateMerchantProductCondition(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(MerchantProductCondition, {
            title: 'CONDITION_UPDATE',
            exclude: ['MPC_ID', 'AT_CREATE', 'AT_UPDATE', 'AT_WHO'],
          }),
        },
      },
    })
    dataMerchantProductCondition: any,
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};
    var REQUIRED_FIELDS = ['MPV_ID', 'MPC_OPERATOR', 'MPC_VALUE', 'MPC_GROUP', 'M_ID', 'MP_ID', 'MPG_ID', 'AT_FLAG'];
    var BODY_PARAM: any; BODY_PARAM = {};
    for (var i in dataMerchantProductCondition) {
      if (REQUIRED_FIELDS.indexOf(i) != -1) {
        REQUIRED_FIELDS.splice(REQUIRED_FIELDS.indexOf(i), 1);
      }
      BODY_PARAM[i] = dataMerchantProductCondition[i];
    }

    if (REQUIRED_FIELDS.length == 0) {
      var MERCHANT = await this.merchantRepository.count({M_ID: BODY_PARAM["M_ID"]});
      var MERCHANT_PRODUCT = await this.merchantProductRepository.count({and: [{MP_ID: BODY_PARAM["MP_ID"]}, {M_ID: BODY_PARAM["M_ID"]}]});
      var MERCHANT_PRODUCT_GROUP = await this.merchantProductGroupRepository.count({and: [{MPG_ID: BODY_PARAM["MPG_ID"]}, {MP_ID: BODY_PARAM["MP_ID"]}, {M_ID: BODY_PARAM["M_ID"]}]});
      var MERCHANT_VARIABLE = await this.merchantProductVariableRepository.count({and: [{MPV_ID: BODY_PARAM.MPV_ID}]});

      var CONDITION = await this.merchantProductConditionRepository.count({and: [{MPC_ID: id}]});

      if (MERCHANT.count > 0) {
        if (MERCHANT_PRODUCT.count > 0) {
          if (MERCHANT_PRODUCT_GROUP.count > 0) {
            if (MERCHANT_VARIABLE.count > 0) {
              if (CONDITION.count > 0) {
                BODY_PARAM["AT_CREATE"] = ControllerConfig.onCurrentTime().toString();
                BODY_PARAM["AT_UPDATE"] = undefined;
                BODY_PARAM["AT_WHO"] = USER_ID;
                var ACC_OPERATOR = ["=", "<=", ">=", "<>", "<", ">", "LIKE", "NOT IN", "IN"];
                if (ACC_OPERATOR.indexOf(BODY_PARAM["MPC_OPERATOR"]) != -1) {

                  if (BODY_PARAM["MPC_OPERATOR"] == "NOT IN" || BODY_PARAM["MPC_OPERATOR"] == "IN") {
                    try {
                      var LIST_IN = BODY_PARAM["MPC_VALUE"].split(";");
                      if (LIST_IN.length == 0) {
                        result.success = false;
                        result.message = "Error, please select format value '1';'2';'dst'";
                        return result;
                      }
                    } catch (e) {
                      result.success = false;
                      result.message = "Error, please select format value '1';'2';'dst'";
                      return result;
                    }
                  }

                  try {

                    var PRODUCT_CONDITION = await this.merchantProductConditionRepository.updateById(id, BODY_PARAM);
                    result.success = true;
                    result.message = "Success update condition !";
                    result.datas = PRODUCT_CONDITION;

                  } catch (error) {
                    result.success = false;
                    result.message = error;
                  }

                } else {
                  result.success = false;
                  result.message = "Error, Operator  " + BODY_PARAM["MPC_OPERATOR"] + " not found !";
                }
              } else {
                result.success = false;
                result.message = "Error, Condition  " + id + " not found !";
              }
            } else {
              result.success = false;
              result.message = "Error, merchant variable  " + BODY_PARAM["MPV_ID"] + " not found !";
            }
          } else {
            result.success = false;
            result.message = "Error, merchant product group " + BODY_PARAM["MP_ID"] + " not found !";
          }
        } else {
          result.success = false;
          result.message = "Error, merchant product " + BODY_PARAM["MP_ID"] + " not found !";
        }
      } else {
        result.success = false;
        result.message = "Error, merchant " + BODY_PARAM["M_ID"] + " not found !";
      }
    } else {
      result.success = false;
      result.message = "Error, Field " + JSON.stringify(REQUIRED_FIELDS) + " must entry !";
    }

    return result;
  }


  @post('/merchant-condition-action/delete/{id}', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {'application/json': {schema: getModelSchemaRef(MerchantProductCondition)}},
      },
    },
  })
  async deleteMerchantProductCondition(
    @param.path.number('id') id: number
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};

    var PRODUCT_CONDITION = await this.merchantProductConditionRepository.count({MPC_ID: id});
    if (PRODUCT_CONDITION.count != 0) {
      try {
        await this.merchantProductConditionRepository.deleteById(id);
        result.success = true;
        result.message = "Success delete merchant condition  !";
      } catch (error) {
        result.success = false;
        result.message = error;
      }
    } else {
      result.success = false;
      result.message = "Error, merchant product condition not found !";
    }

    return result;
  }


  @get('/merchant-condition-action/{MPG_ID}/views', {
    responses: {
      '200': {
        description: 'Array of Merchant Product variable model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(MerchantProductCondition, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async findMerchantProductCondition(
    @param.path.number('MPG_ID') id: number,
    @param.filter(MerchantProductCondition) filter?: Filter<MerchantProductCondition>,
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};

    var MERCHANT_GROUP_COUNT = await this.merchantProductGroupRepository.count({MPG_ID: id});
    if (MERCHANT_GROUP_COUNT.count > 0) {
      // var MERCHANT_PRODUCT = await this.merchantProductGroupRepository.findById(id);
      var MERCHANT_CONDITION: any; MERCHANT_CONDITION = await this.merchantProductConditionRepository.find({
        where: {and: [{MPG_ID: id}]}, fields: {
          MPC_ID: true,
          MPV_ID: true,
          MPC_OPERATOR: true,
          MPC_VALUE: true,
          MPC_GROUP: true,
          M_ID: true,
          MP_ID: true,
          MPG_ID: true,
          AT_FLAG: true,
        }
      });

      var CONDITION_STORE: any; CONDITION_STORE = {}

      for (var i in MERCHANT_CONDITION) {
        var RAW: any; RAW = {};
        var INFO_CONDITION: any; INFO_CONDITION = MERCHANT_CONDITION[i];
        for (var j in INFO_CONDITION) {
          RAW[j] = INFO_CONDITION[j];
        }

        RAW["MERCHANT"] = await this.merchantRepository.findById(RAW.M_ID, {
          fields: {
            M_ID: true,
            M_CODE: true,
            M_NAME: true,
            M_LEGAL_NAME: true,
          }
        });

        RAW["MERCHANT_PRODUCT"] = await this.merchantProductRepository.findById(RAW.MP_ID, {
          fields: {
            MP_ID: true,
            MP_CODE: true,
            MP_BRAND: true,
          }
        });

        RAW["MERCHANT_PRODUCT_GROUP"] = await this.merchantProductGroupRepository.findById(RAW.MPG_ID, {
          fields: {
            MPG_ID: true,
            MPG_LABEL: true
          }
        });

        if (CONDITION_STORE[RAW.MPC_GROUP] == undefined) {
          CONDITION_STORE[RAW.MPC_GROUP] = [];
        }

        CONDITION_STORE[RAW.MPC_GROUP].push(RAW);
      }
      result.success = true;
      result.message = "Success !";
      result.datas = CONDITION_STORE;
    } else {
      result.success = false;
      result.message = "Error, merchant product group " + id + " not found !";
    }

    return result;
  }


  @get('/merchant-condition-action/view/{id}', {
    responses: {
      '200': {
        description: 'Array of Merchant Product condition model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(MerchantProductCondition, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async findMerchantProductConditionById(
    @param.path.number('id') id: number,
    @param.filter(MerchantProductCondition, {exclude: 'where'}) filter?: FilterExcludingWhere<MerchantProductCondition>
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};

    var MERCHANT_CONDITION_COUNT = await this.merchantProductConditionRepository.count({MPC_ID: id});
    if (MERCHANT_CONDITION_COUNT.count > 0) {

      var MERCHANT_CONDITION: any; MERCHANT_CONDITION = await this.merchantProductConditionRepository.findOne({
        where: {and: [{MPC_ID: id}]}, fields: {
          MPC_ID: true,
          MPV_ID: true,
          MPC_OPERATOR: true,
          MPC_VALUE: true,
          MPC_GROUP: true,
          M_ID: true,
          MP_ID: true,
          MPG_ID: true,
          AT_FLAG: true,
        }
      });

      var RAW: any; RAW = {};
      for (var i in MERCHANT_CONDITION) {
        RAW[i] = MERCHANT_CONDITION[i];
      }

      RAW["MERCHANT"] = await this.merchantRepository.findById(RAW.M_ID, {
        fields: {
          M_ID: true,
          M_CODE: true,
          M_NAME: true,
          M_LEGAL_NAME: true,
        }
      });

      RAW["MERCHANT_PRODUCT"] = await this.merchantProductRepository.findById(RAW.MP_ID, {
        fields: {
          MP_ID: true,
          MP_CODE: true,
          MP_BRAND: true,
        }
      });

      RAW["MERCHANT_PRODUCT_GROUP"] = await this.merchantProductGroupRepository.findById(RAW.MPG_ID, {
        fields: {
          MPG_ID: true,
          MPG_LABEL: true
        }
      });

      result.success = true;
      result.message = "Success !";
      result.datas = RAW;
    } else {
      result.success = false;
      result.message = "Error, merchant product condition " + id + " not found !";
    }
    return result;
  }

}
