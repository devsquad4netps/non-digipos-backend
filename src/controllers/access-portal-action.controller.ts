// Uncomment these imports to begin using these cool features!

import {model, property, repository} from '@loopback/repository';
import {post, requestBody} from '@loopback/rest';
import {genSalt, hash} from 'bcryptjs';
import {DataUser} from '../models';
import {AdMasterRepository, DataRoleMapRepository, DataUserRepository} from '../repositories';
import {ControllerConfig} from './controller.config';

@model()
export class NewDataUserRequest extends DataUser {
  @property({
    type: 'string',
    required: true,
  })
  password: string;
}

export const NewDataUserRequestBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['ad_code', 'password'],
        properties: {
          ad_code: {
            type: 'string',
          },
          password: {
            type: 'string',
          },
        }
      }
    },
  },
};


export class AccessPortalActionController {
  constructor(
    @repository(AdMasterRepository)
    public adMasterRepository: AdMasterRepository,
    @repository(DataUserRepository) protected dataUserRepository: DataUserRepository,
    @repository(DataRoleMapRepository) protected dataRoleMapRepository: DataRoleMapRepository,
  ) { }

  @post('access-portal/register-ad', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: NewDataUserRequestBody
          }
        },
      },
    },
  })
  async registerAD(
    @requestBody(NewDataUserRequestBody) newDataUserRequest: {
      ad_code: string, password: string
    },
  ): Promise<Object> {
    var result: any; result = {success: false, message: "", data: {}};
    const password = await hash(newDataUserRequest.password, await genSalt());

    var dataAd = await this.adMasterRepository.findById(newDataUserRequest.ad_code);
    if (dataAd.ad_code != null && dataAd.ad_code != undefined && dataAd.ad_code != "") {

      if (dataAd.email != null && dataAd.email != undefined && dataAd.email != "") {
        var registerAD = await this.dataUserRepository.findOne({where: {and: [{company_code: dataAd.ad_code}, {id: {neq: "63c9e94b-5b94-4850-a2a9-05fc11c800f3"}}]}});
        if (registerAD == null || registerAD.company_code == undefined) {

          const savedUser = await this.dataUserRepository.create({
            nik: dataAd.npwp,
            fullname: dataAd.ad_name,
            email: dataAd.email,
            jabatan: dataAd.attention_position,
            divisi: dataAd.attention_position,
            email_verified: true,
            verification_code: "",
            state: 2,
            at_create: ControllerConfig.onCurrentTime().toString(),
            at_update: "",
            at_banned: "",
            at_key: "",
            company_code: dataAd.ad_code
          });
          await this.dataRoleMapRepository.create({UserId: savedUser.id, RoleId: 3})
          await this.dataUserRepository.userCredentials(savedUser.id).create({password});

          result.success = true;
          result.message = "success";
          result.data = savedUser;
        } else {
          const updateUser = await this.dataUserRepository.updateById(registerAD.id, {
            nik: dataAd.npwp,
            fullname: dataAd.ad_name,
            email: dataAd.email,
            jabatan: dataAd.attention_position,
            divisi: dataAd.attention_position,
            at_update: ControllerConfig.onCurrentTime().toString(),
          });
          result.success = true;
          result.message = "success, update information code ad !";
          result.data = updateUser;
        }
      } else {
        result.success = false;
        result.message = "error, Email Ad can't exities / empty !"
      }
    } else {
      result.success = false;
      result.message = "Code Ad not found, please add in Master AD before register to portal ad !"
    }

    return result;
  }


  // @post('/register-ad', {
  //   responses: {
  //     '200': {
  //       description: 'User',
  //       content: {
  //         'application/json': {
  //           schema: {
  //             'x-ts-type': DataUser,
  //           },
  //         },
  //       },
  //     },
  //   },
  // })
  // async registerAD(
  //   @requestBody({
  //     content: {
  //       'application/json': {
  //         schema: getModelSchemaRef(NewDataUserRequest, {
  //           title: 'NewUser',
  //         }),
  //       },
  //     },
  //   })
  //   newDataUserRequest: NewDataUserRequest,
  // ): Promise<Object> {
  //   var result: any; result = {};
  //   const password = await hash(newDataUserRequest.password, await genSalt());

  //   const savedUser = await this.dataUserRepository.create(
  //     _.omit(newDataUserRequest, 'password'),
  //   );

  //   await this.dataRoleMapRepository.create({UserId: savedUser.id, RoleId: 3})
  //   await this.dataUserRepository.userCredentials(savedUser.id).create({password});
  //   // // return savedUser;
  //   return savedUser;
  // }


}
