// Uncomment these imports to begin using these cool features!

import {authenticate} from '@loopback/authentication';
import {inject} from '@loopback/core';
import {Filter, FilterExcludingWhere, repository} from '@loopback/repository';
import {get, getModelSchemaRef, param, post, requestBody} from '@loopback/rest';
import {SecurityBindings, securityId, UserProfile} from '@loopback/security';
import {MerchantProductMdr, MerchantProductMdrCondition} from '../models';
import {MerchantProductGroupRepository, MerchantProductMdrConditionRepository, MerchantProductMdrRepository, MerchantProductVariableRepository} from '../repositories';
import {MerchantProductRepository} from '../repositories/merchant-product.repository';
import {MerchantRepository} from '../repositories/merchant.repository';
import {UserRepository} from '../repositories/user.repository';
import {ControllerConfig} from './controller.config';


// import {inject} from '@loopback/core';

@authenticate('jwt')
export class MerchantProductMdrActionController {
  constructor(
    @repository(MerchantRepository)
    public merchantRepository: MerchantRepository,
    @repository(MerchantProductRepository)
    public merchantProductRepository: MerchantProductRepository,
    @repository(MerchantProductGroupRepository)
    public merchantProductGroupRepository: MerchantProductGroupRepository,
    @repository(MerchantProductVariableRepository)
    public merchantProductVariableRepository: MerchantProductVariableRepository,
    @repository(MerchantProductMdrConditionRepository)
    public merchantProductMdrConditionRepository: MerchantProductMdrConditionRepository,
    @repository(MerchantProductMdrRepository)
    public merchantProductMdrRepository: MerchantProductMdrRepository,
    @inject(SecurityBindings.USER)
    public currentUserProfile: UserProfile,
    @repository(UserRepository)
    public userRepository: UserRepository,
  ) { }

  //condition mdr
  @post('/merchant-condition-mdr/create', {
    responses: {
      '200': {
        description: 'Create merchant product condition mdr model instance',
        content: {'application/json': {schema: getModelSchemaRef(MerchantProductMdrCondition)}},
      },
    },
  })
  async createMerchantProductConditionMdr(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(MerchantProductMdrCondition, {
            title: 'MERCHANT_CONDITION_MDR_CREATE',
            exclude: ['MDRCON_ID', 'AT_CREATE', 'AT_UPDATE', 'AT_WHO'],
          }),
        },
      },
    })
    dataMerchantProductConditionMdr: any,
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};

    var REQUIRED_FIELDS = ['MPV_ID', 'MDRCON_OPERATOR', 'MDRCON_VALUE', 'MDRCON_GROUP', 'MDR_ID', 'AT_FLAG'];
    var BODY_PARAM: any; BODY_PARAM = {};
    for (var i in dataMerchantProductConditionMdr) {
      if (REQUIRED_FIELDS.indexOf(i) != -1) {
        REQUIRED_FIELDS.splice(REQUIRED_FIELDS.indexOf(i), 1);
      }
      BODY_PARAM[i] = dataMerchantProductConditionMdr[i];
    }

    if (REQUIRED_FIELDS.length == 0) {
      var MERCHANT_MDR = await this.merchantProductMdrRepository.count({and: [{AT_FLAG: 1}, {MDR_ID: dataMerchantProductConditionMdr.MDR_ID}]})
      var MERCHANT_VARIABLE = await this.merchantProductVariableRepository.count({and: [{MP_ID: BODY_PARAM["MP_ID"]}, {M_ID: BODY_PARAM["M_ID"]}, {MPV_ID: BODY_PARAM.MPV_ID}]});


      if (MERCHANT_MDR.count > 0) {
        if (MERCHANT_VARIABLE.count > 0) {
          // if (MVP_TYPE_KEY.count > 0) {
          BODY_PARAM["AT_CREATE"] = ControllerConfig.onCurrentTime().toString();
          BODY_PARAM["AT_UPDATE"] = undefined;
          BODY_PARAM["AT_WHO"] = USER_ID;
          var ACC_OPERATOR = ["=", "<=", " >=", "<>", "<", ">", "LIKE", "NOT IN", "IN"];
          if (ACC_OPERATOR.indexOf(BODY_PARAM["MDRCON_OPERATOR"]) != -1) {

            if (BODY_PARAM["MDRCON_OPERATOR"] == "NOT IN" || BODY_PARAM["MDRCON_OPERATOR"] == "IN") {
              try {
                var LIST_IN = BODY_PARAM["MDRCON_VALUE"].split(";");
                if (LIST_IN.length == 0) {
                  result.success = false;
                  result.message = "Error, please select format value '1';'2';'dst'";
                  return result;
                }
              } catch (e) {
                result.success = false;
                result.message = "Error, please select format value '1';'2';'dst'";
                return result;
              }
            }

            try {

              var PRODUCT_CONDITION = await this.merchantProductMdrConditionRepository.create(BODY_PARAM);
              result.success = true;
              result.message = "Success create condition mdr !";
              result.datas = PRODUCT_CONDITION;

            } catch (error) {
              result.success = false;
              result.message = error;
            }

          } else {
            result.success = false;
            result.message = "Error, Operator  " + BODY_PARAM["MPC_OPERATOR"] + " not found !";
          }
        } else {
          result.success = false;
          result.message = "Error, merchant variable  " + BODY_PARAM["MPV_ID"] + " not found !";
        }
      } else {
        result.success = false;
        result.message = "Error, merchant mdr " + BODY_PARAM["MP_ID"] + " not found !";
      }
    } else {
      result.success = false;
      result.message = "Error, Field " + JSON.stringify(REQUIRED_FIELDS) + " must entry !";
    }
    return result;
  }


  @post('/merchant-condition-mdr/update/{id}', {
    responses: {
      '200': {
        description: 'PRODUCT CONDITION MDR UPDATE  model instance',
        content: {'application/json': {schema: getModelSchemaRef(MerchantProductMdrCondition)}},
      },
    },
  })
  async updateMerchantProductConditionMDR(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(MerchantProductMdrCondition, {
            title: 'CONDITION_MDR_UPDATE',
            exclude: ['MDRCON_ID', 'AT_CREATE', 'AT_UPDATE', 'AT_WHO'],
          }),
        },
      },
    })
    dataMerchantProductConditionMDR: any,
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};
    var REQUIRED_FIELDS = ['MPV_ID', 'MDRCON_OPERATOR', 'MDRCON_VALUE', 'MDRCON_GROUP', 'MDR_ID', 'AT_FLAG'];
    var BODY_PARAM: any; BODY_PARAM = {};
    for (var i in dataMerchantProductConditionMDR) {
      if (REQUIRED_FIELDS.indexOf(i) != -1) {
        REQUIRED_FIELDS.splice(REQUIRED_FIELDS.indexOf(i), 1);
      }
      BODY_PARAM[i] = dataMerchantProductConditionMDR[i];
    }

    if (REQUIRED_FIELDS.length == 0) {
      var MERCHANT_MDR = await this.merchantProductMdrRepository.count({MDR_ID: BODY_PARAM["MDR_ID"]});
      var MERCHANT_VARIABLE = await this.merchantProductVariableRepository.count({and: [{MPV_ID: BODY_PARAM.MPV_ID}]});

      var CONDITION = await this.merchantProductMdrConditionRepository.count({and: [{MDRCON_ID: id}]});

      if (MERCHANT_MDR.count > 0) {
        if (MERCHANT_VARIABLE.count > 0) {
          if (CONDITION.count > 0) {
            BODY_PARAM["AT_CREATE"] = ControllerConfig.onCurrentTime().toString();
            BODY_PARAM["AT_UPDATE"] = undefined;
            BODY_PARAM["AT_WHO"] = USER_ID;
            var ACC_OPERATOR = ["=", "<=", " >=", "<>", "<", ">", "LIKE", "NOT IN", "IN"];
            if (ACC_OPERATOR.indexOf(BODY_PARAM["MDRCON_OPERATOR"]) != -1) {

              if (BODY_PARAM["MDRCON_OPERATOR"] == "NOT IN" || BODY_PARAM["MDRCON_OPERATOR"] == "IN") {
                try {
                  var LIST_IN = BODY_PARAM["MDRCON_VALUE"].split(";");
                  if (LIST_IN.length == 0) {
                    result.success = false;
                    result.message = "Error, please select format value '1';'2';'dst'";
                    return result;
                  }
                } catch (e) {
                  result.success = false;
                  result.message = "Error, please select format value '1';'2';'dst'";
                  return result;
                }
              }

              try {

                var PRODUCT_CONDITION = await this.merchantProductMdrConditionRepository.updateById(id, BODY_PARAM);
                result.success = true;
                result.message = "Success update condition !";
                result.datas = PRODUCT_CONDITION;

              } catch (error) {
                result.success = false;
                result.message = error;
              }

            } else {
              result.success = false;
              result.message = "Error, Operator  " + BODY_PARAM["MDRCON_OPERATOR"] + " not found !";
            }
          } else {
            result.success = false;
            result.message = "Error, Condition  " + id + " not found !";
          }
        } else {
          result.success = false;
          result.message = "Error, merchant variable  " + BODY_PARAM["MPV_ID"] + " not found !";
        }
      } else {
        result.success = false;
        result.message = "Error, merchant mdr " + BODY_PARAM["M_ID"] + " not found !";
      }
    } else {
      result.success = false;
      result.message = "Error, Field " + JSON.stringify(REQUIRED_FIELDS) + " must entry !";
    }

    return result;
  }

  @post('/merchant-condition-mdr/delete/{id}', {
    responses: {
      '200': {
        description: 'DataDigipos merchant condition MDR model instance',
        content: {'application/json': {schema: getModelSchemaRef(MerchantProductMdrCondition)}},
      },
    },
  })
  async deleteMerchantProductConditionMdr(
    @param.path.number('id') id: number
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};

    var PRODUCT_CONDITION = await this.merchantProductMdrConditionRepository.count({MDRCON_ID: id});
    if (PRODUCT_CONDITION.count != 0) {
      try {
        await this.merchantProductMdrConditionRepository.deleteById(id);
        result.success = true;
        result.message = "Success delete merchant condition  !";
      } catch (error) {
        result.success = false;
        result.message = error;
      }
    } else {
      result.success = false;
      result.message = "Error, merchant product mdr condition not found !";
    }

    return result;
  }


  @get('/merchant-condition-mdr/{MDR_ID}/views', {
    responses: {
      '200': {
        description: 'Array of Merchant Product Condition MDR  model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(MerchantProductMdrCondition, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async findMerchantProductConditionMDR(
    @param.path.number('MDR_ID') id: number,
    @param.filter(MerchantProductMdrCondition) filter?: Filter<MerchantProductMdrCondition>,
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};

    var MERCHANT_MDR = await this.merchantProductMdrRepository.count({MDR_ID: id});
    if (MERCHANT_MDR.count > 0) {
      // var MERCHANT_PRODUCT = await this.merchantProductGroupRepository.findById(id);
      var MERCHANT_CONDITION: any; MERCHANT_CONDITION = await this.merchantProductMdrConditionRepository.find({
        where: {and: [{MDR_ID: id}]}, fields: {
          MDRCON_ID: true,
          MPV_ID: true,
          MDRCON_OPERATOR: true,
          MDRCON_VALUE: true,
          MDRCON_GROUP: true,
          MDR_ID: true,
          AT_FLAG: true,
        }
      });

      var CONDITION_STORE: any; CONDITION_STORE = {}

      for (var i in MERCHANT_CONDITION) {
        var RAW: any; RAW = {};
        var INFO_CONDITION: any; INFO_CONDITION = MERCHANT_CONDITION[i];
        for (var j in INFO_CONDITION) {
          RAW[j] = INFO_CONDITION[j];
        }

        RAW["VARIABLE"] = await this.merchantProductVariableRepository.findById(RAW.MPV_ID, {
          fields: {
            MPV_ID: true,
            MPV_LABEL: true,
            MPV_KEY: true,
            MPV_TYPE_ID: true,
          }
        });



        if (CONDITION_STORE[RAW.MDRCON_GROUP] == undefined) {
          CONDITION_STORE[RAW.MDRCON_GROUP] = [];
        }

        CONDITION_STORE[RAW.MDRCON_GROUP].push(RAW);
      }
      result.success = true;
      result.message = "Success !";
      result.datas = CONDITION_STORE;
    } else {
      result.success = false;
      result.message = "Error, merchant product mdr " + id + " not found !";
    }

    return result;
  }


  @get('/merchant-condition-mdr/view/{id}', {
    responses: {
      '200': {
        description: 'Array of Merchant Product condition MDR model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(MerchantProductMdrCondition, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async findMerchantProductConditionMDRById(
    @param.path.number('id') id: number,
    @param.filter(MerchantProductMdrCondition, {exclude: 'where'}) filter?: FilterExcludingWhere<MerchantProductMdrCondition>
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};

    var MERCHANT_CONDITION_COUNT = await this.merchantProductMdrConditionRepository.count({MDRCON_ID: id});
    if (MERCHANT_CONDITION_COUNT.count > 0) {

      var MERCHANT_CONDITION: any; MERCHANT_CONDITION = await this.merchantProductMdrConditionRepository.findOne({
        where: {and: [{MDRCON_ID: id}]}, fields: {
          MDRCON_ID: true,
          MPV_ID: true,
          MDRCON_OPERATOR: true,
          MDRCON_VALUE: true,
          MDRCON_GROUP: true,
          MDR_ID: true,
          AT_FLAG: true,
        }
      });

      var RAW: any; RAW = {};
      for (var i in MERCHANT_CONDITION) {
        RAW[i] = MERCHANT_CONDITION[i];
      }

      RAW["VARIABLE"] = await this.merchantProductVariableRepository.findById(RAW.MPV_ID, {
        fields: {
          MPV_ID: true,
          MPV_LABEL: true,
          MPV_KEY: true,
          MPV_TYPE_ID: true,
        }
      });

      result.success = true;
      result.message = "Success !";
      result.datas = RAW;
    } else {
      result.success = false;
      result.message = "Error, merchant product condition mdr " + id + " not found !";
    }
    return result;
  }


  //============================================================================MDR FUNCTION

  @get('/merchant-mdr/{MPG_ID}/views', {
    responses: {
      '200': {
        description: 'Array of Merchant Product MDR  model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(MerchantProductMdr, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async findMerchantProductMDR(
    @param.path.number('MPG_ID') id: number,
    @param.filter(MerchantProductMdr) filter?: Filter<MerchantProductMdr>,
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};

    var MERCHANT_MDR = await this.merchantProductMdrRepository.find({
      where: {and: [{MPG_ID: id}]}, fields: {
        MDR_ID: true,
        MDR: true,
        MDR_TYPE: true,
        M_ID: true,
        MP_ID: true,
        MPG_ID: true,
        AT_FLAG: true,
      }
    });

    var MERCHANT_MDR_STORE: any; MERCHANT_MDR_STORE = []
    for (var i in MERCHANT_MDR) {
      var RAW_MERCHANT: any; RAW_MERCHANT = MERCHANT_MDR[i];
      var RAW: any; RAW = {};
      for (var i in RAW_MERCHANT) {
        RAW[i] = RAW_MERCHANT[i];
      }

      RAW["MERCHANT"] = await this.merchantRepository.findById(RAW.M_ID, {
        fields: {
          M_ID: true,
          M_CODE: true,
          M_NAME: true,
          M_LEGAL_NAME: true,
        }
      });

      RAW["MERCHANT_PRODUCT"] = await this.merchantProductRepository.findById(RAW.MP_ID, {
        fields: {
          MP_ID: true,
          MP_CODE: true,
          MP_BRAND: true,
        }
      });

      RAW["MERCHANT_PRODUCT_GROUP"] = await this.merchantProductGroupRepository.findById(RAW.MPG_ID, {
        fields: {
          MPG_ID: true,
          MPG_LABEL: true
        }
      });

      var CONDITION = await this.merchantProductMdrConditionRepository.find({
        where: {and: [{MDR_ID: RAW.MDR_ID}]}, fields: {
          MDRCON_ID: true,
          MDR_ID: true,
          MDRCON_OPERATOR: true,
          MDRCON_VALUE: true,
          MDRCON_GROUP: true,
          MPV_ID: true
        }
      });

      RAW["CONDITION"] = {};
      for (var I_CON in CONDITION) {
        var RAW_CONDITION = CONDITION[I_CON];

        if (RAW["CONDITION"][RAW_CONDITION.MDRCON_GROUP] == undefined) {
          RAW["CONDITION"][RAW_CONDITION.MDRCON_GROUP] = [];
        }

        var VARIABLE = await this.merchantProductVariableRepository.findOne({
          where: {MPV_ID: RAW_CONDITION.MPV_ID}, fields: {
            MPV_LABEL: true,
            MPV_KEY: true,
            MPV_TYPE_ID: true
          }
        });

        RAW["CONDITION"][RAW_CONDITION.MDRCON_GROUP].push({
          MPV_ID: RAW_CONDITION.MPV_ID,
          VARIABLE: VARIABLE,
          MDRCON_OPERATOR: RAW_CONDITION.MDRCON_OPERATOR,
          MDRCON_VALUE: RAW_CONDITION.MDRCON_VALUE,
        })
      }

      MERCHANT_MDR_STORE.push(RAW);
    }

    result.success = true;
    result.message = "Success !";
    result.datas = MERCHANT_MDR_STORE;
    return result;
  }


  @get('/merchant-mdr/view/{id}', {
    responses: {
      '200': {
        description: 'Array of Merchant Product condition model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(MerchantProductMdr, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async findMerchantProductMDRById(
    @param.path.number('id') id: number,
    @param.filter(MerchantProductMdr, {exclude: 'where'}) filter?: FilterExcludingWhere<MerchantProductMdr>
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};

    var MERCHANT_CONDITION_COUNT = await this.merchantProductMdrRepository.count({MDR_ID: id});
    if (MERCHANT_CONDITION_COUNT.count > 0) {

      var MERCHANT_CONDITION: any; MERCHANT_CONDITION = await this.merchantProductMdrRepository.findOne({
        where: {and: [{MDR_ID: id}]}, fields: {
          MDR_ID: true,
          MDR: true,
          MDR_TYPE: true,
          M_ID: true,
          MP_ID: true,
          MPG_ID: true,
          AT_FLAG: true,
        }
      });

      var RAW: any; RAW = {};
      for (var i in MERCHANT_CONDITION) {
        RAW[i] = MERCHANT_CONDITION[i];
      }

      RAW["MERCHANT"] = await this.merchantRepository.findById(RAW.M_ID, {
        fields: {
          M_ID: true,
          M_CODE: true,
          M_NAME: true,
          M_LEGAL_NAME: true,
        }
      });

      RAW["MERCHANT_PRODUCT"] = await this.merchantProductRepository.findById(RAW.MP_ID, {
        fields: {
          MP_ID: true,
          MP_CODE: true,
          MP_BRAND: true,
        }
      });

      RAW["MERCHANT_PRODUCT_GROUP"] = await this.merchantProductGroupRepository.findById(RAW.MPG_ID, {
        fields: {
          MPG_ID: true,
          MPG_LABEL: true
        }
      });

      result.success = true;
      result.message = "Success !";
      result.datas = RAW;
    } else {
      result.success = false;
      result.message = "Error, merchant product mdr " + id + " not found !";
    }
    return result;
  }




  @post('/merchant-mdr/delete/{id}', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {'application/json': {schema: getModelSchemaRef(MerchantProductMdr)}},
      },
    },
  })
  async deleteMerchantProductMDR(
    @param.path.number('id') id: number
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};

    var PRODUCT_MDR = await this.merchantProductMdrRepository.count({MDR_ID: id});
    if (PRODUCT_MDR.count != 0) {
      try {
        await this.merchantProductMdrRepository.deleteById(id);
        result.success = true;
        result.message = "Success delete merchant mdr  !";
      } catch (error) {
        result.success = false;
        result.message = error;
      }
    } else {
      result.success = false;
      result.message = "Error, merchant product mdr not found !";
    }
    return result;
  }


  @post('/merchant-mdr/create', {
    responses: {
      '200': {
        description: 'Create merchant product  mdr model instance',
        content: {'application/json': {schema: getModelSchemaRef(MerchantProductMdr)}},
      },
    },
  })
  async createMerchantProductMDR(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(MerchantProductMdr, {
            title: 'MERCHANT_MDR_CREATE',
            exclude: ['MDR_ID', 'AT_CREATE', 'AT_UPDATE', 'AT_WHO'],
          }),
        },
      },
    })
    dataMerchantProductMDR: any,
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};

    var REQUIRED_FIELDS = ['MDR', 'MDR_TYPE', 'M_ID', 'MP_ID', 'MPG_ID', 'AT_FLAG'];
    var BODY_PARAM: any; BODY_PARAM = {};
    for (var i in dataMerchantProductMDR) {
      if (REQUIRED_FIELDS.indexOf(i) != -1) {
        REQUIRED_FIELDS.splice(REQUIRED_FIELDS.indexOf(i), 1);
      }
      BODY_PARAM[i] = dataMerchantProductMDR[i];
    }

    if (REQUIRED_FIELDS.length == 0) {
      var MERCHANT = await this.merchantRepository.count({M_ID: BODY_PARAM["M_ID"]});
      var MERCHANT_PRODUCT = await this.merchantProductRepository.count({and: [{MP_ID: BODY_PARAM["MP_ID"]}, {M_ID: BODY_PARAM["M_ID"]}]});
      var MERCHANT_PRODUCT_GROUP = await this.merchantProductGroupRepository.count({and: [{MPG_ID: BODY_PARAM["MPG_ID"]}, {MP_ID: BODY_PARAM["MP_ID"]}, {M_ID: BODY_PARAM["M_ID"]}]});

      if (MERCHANT.count > 0) {
        if (MERCHANT_PRODUCT.count > 0) {
          if (MERCHANT_PRODUCT_GROUP.count > 0) {

            BODY_PARAM["AT_CREATE"] = ControllerConfig.onCurrentTime().toString();
            BODY_PARAM["AT_UPDATE"] = undefined;
            BODY_PARAM["AT_WHO"] = USER_ID;

            try {
              var PRODUCT_CONDITION = await this.merchantProductMdrRepository.create(BODY_PARAM);
              result.success = true;
              result.message = "Success create MDR !";
              result.datas = PRODUCT_CONDITION;
            } catch (error) {
              result.success = false;
              result.message = error;
            }

          } else {
            result.success = false;
            result.message = "Error, merchant product group " + BODY_PARAM["MP_ID"] + " not found !";
          }
        } else {
          result.success = false;
          result.message = "Error, merchant product " + BODY_PARAM["MP_ID"] + " not found !";
        }
      } else {
        result.success = false;
        result.message = "Error, merchant " + BODY_PARAM["M_ID"] + " not found !";
      }
    } else {
      result.success = false;
      result.message = "Error, Field " + JSON.stringify(REQUIRED_FIELDS) + " must entry !";
    }
    return result;
  }

  @post('/merchant-mdr/update/{id}', {
    responses: {
      '200': {
        description: 'PRODUCT MDR UPDATE model instance',
        content: {'application/json': {schema: getModelSchemaRef(MerchantProductMdr)}},
      },
    },
  })
  async updateMerchantProductMDR(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(MerchantProductMdr, {
            title: 'MDR_UPDATE',
            exclude: ['MDR_ID', 'AT_CREATE', 'AT_UPDATE', 'AT_WHO'],
          }),
        },
      },
    })
    dataMerchantProductMDR: any,
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};
    var REQUIRED_FIELDS = ['MDR', 'MDR_TYPE', 'M_ID', 'MP_ID', 'MPG_ID', 'AT_FLAG'];
    var BODY_PARAM: any; BODY_PARAM = {};
    for (var i in dataMerchantProductMDR) {
      if (REQUIRED_FIELDS.indexOf(i) != -1) {
        REQUIRED_FIELDS.splice(REQUIRED_FIELDS.indexOf(i), 1);
      }
      BODY_PARAM[i] = dataMerchantProductMDR[i];
    }

    if (REQUIRED_FIELDS.length == 0) {
      var MERCHANT = await this.merchantRepository.count({M_ID: BODY_PARAM["M_ID"]});
      var MERCHANT_PRODUCT = await this.merchantProductRepository.count({and: [{MP_ID: BODY_PARAM["MP_ID"]}, {M_ID: BODY_PARAM["M_ID"]}]});
      var MERCHANT_PRODUCT_GROUP = await this.merchantProductGroupRepository.count({and: [{MPG_ID: BODY_PARAM["MPG_ID"]}, {MP_ID: BODY_PARAM["MP_ID"]}, {M_ID: BODY_PARAM["M_ID"]}]});

      var CONDITION = await this.merchantProductMdrRepository.count({and: [{MDR_ID: id}]});

      if (MERCHANT.count > 0) {
        if (MERCHANT_PRODUCT.count > 0) {
          if (MERCHANT_PRODUCT_GROUP.count > 0) {
            if (CONDITION.count > 0) {
              BODY_PARAM["AT_CREATE"] = ControllerConfig.onCurrentTime().toString();
              BODY_PARAM["AT_UPDATE"] = undefined;
              BODY_PARAM["AT_WHO"] = USER_ID;
              try {

                var PRODUCT_CONDITION = await this.merchantProductMdrRepository.updateById(id, BODY_PARAM);
                result.success = true;
                result.message = "Success update condition !";
                result.datas = PRODUCT_CONDITION;

              } catch (error) {
                result.success = false;
                result.message = error;
              }


            } else {
              result.success = false;
              result.message = "Error, MDR  " + id + " not found !";
            }
          } else {
            result.success = false;
            result.message = "Error, merchant product group " + BODY_PARAM["MP_ID"] + " not found !";
          }
        } else {
          result.success = false;
          result.message = "Error, merchant product " + BODY_PARAM["MP_ID"] + " not found !";
        }
      } else {
        result.success = false;
        result.message = "Error, merchant " + BODY_PARAM["M_ID"] + " not found !";
      }
    } else {
      result.success = false;
      result.message = "Error, Field " + JSON.stringify(REQUIRED_FIELDS) + " must entry !";
    }

    return result;
  }

}
