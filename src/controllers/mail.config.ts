import fs from 'fs';
import handlebars from 'handlebars';
import nodemailer from 'nodemailer';
import path from 'path';

let emailAcount = "digipos@finopsfinarya.id";
let transporter = nodemailer.createTransport({
  host: "mail.finopsfinarya.id",
  port: 465,
  secure: true, // true for 465, false for other ports
  auth: {
    user: emailAcount, // generated ethereal user
    pass: 'b4l4n4r4g4n1m', // generated ethereal password
  },
});


// let emailAcount = "digipos@linkaja-ba.tools";
// let transporter = nodemailer.createTransport({
//   host: "smtp.gmail.com",
//   port: 465,
//   secure: true, // true for 465, false for other ports
//   auth: {
//     user: emailAcount, // generated ethereal user
//     pass: 'W3lc0m3#1234', // generated ethereal password
//   },
// });


export class MailConfig {



  constructor(
  ) {

  }


  static onReadHtmlFile = (path: string, callback: any) => {
    fs.readFile(path, {encoding: 'utf-8'}, function (err, html) {
      if (err) {
        throw err;
        callback(err);
      }
      else {
        callback(null, html);
      }
    });
  }

  static onSendMailForgotPassword = async (to: string, params: any, callback: any) => {
    MailConfig.onReadHtmlFile(path.join(__dirname, '../../.digipost/template_mail/forgot_pass.html'), async (err: any, html: any) => {
      if (err) {
        callback({success: false, message: err});
        return;
      }
      try {
        var template = handlebars.compile(html);
        var htmlToSend = template(params);
        // let testAccount = await nodemailer.createTestAccount();
        let info = await transporter.sendMail({
          from: '<' + emailAcount + '>', // sender address
          to: to, // list of receivers
          subject: "DIGIPOS FORGOT PASSWORD", // Subject line
          // text: "Hello world?", // plain text body
          html: htmlToSend, // html body
        });
        callback({success: true, message: info.messageId});
      } catch (error) {
        callback({success: false, message: error});
      }
    });
  }

  static onSendMailFPJP = async (to: string, params: any, callback: any) => {
    MailConfig.onReadHtmlFile(path.join(__dirname, '../../.digipost/template_mail/fpjp_app.html'), async (err: any, html: any) => {
      if (err) {
        callback({success: false, message: err});
        return;
      }
      try {
        var template = handlebars.compile(html);
        var htmlToSend = template(params);
        // let testAccount = await nodemailer.createTestAccount();
        let info = await transporter.sendMail({
          from: '<' + emailAcount + '>', // sender address
          to: to, // list of receivers
          subject: "DIGIPOS NEW APPROVAL FPJP Document", // Subject line
          // text: "Hello world?", // plain text body
          html: htmlToSend, // html body
        });
        callback({success: true, message: info.messageId});
      } catch (error) {
        callback({success: false, message: error});
      }
    });
  }

  static onSendMailDistributeDoc = async (to: string, params: any, callback: any) => {
    // MailConfig.onReadHtmlFile(path.join(__dirname, '../../.digipost/template_mail/distribusi_doc.html'), async (err: any, html: any) => {
    //   if (err) {
    //     callback({success: false, message: err});
    //     return;
    //   }
    //   try {
    //     var template = handlebars.compile(html);
    //     var htmlToSend = template(params);
    //     // let testAccount = await nodemailer.createTestAccount();
    //     let info = await transporter.sendMail({
    //       from: '<' + emailAcount + '>', // sender address
    //       to: to, // list of receivers
    //       subject: "DIGIPOS New Distribusi Document", // Subject line
    //       html: htmlToSend, // html body
    //     });
    //     callback({success: true, message: info.messageId});
    //   } catch (error) {
    //     callback({success: false, message: error});
    //   }
    // });
  }


  //CHECKED
  static onSendMailDocumentAD = async (title: string, to: any, params: any, attachments: any, callback: any) => {
    MailConfig.onReadHtmlFile(path.join(__dirname, '../../.digipost/template_mail/distribusi_doc_rangkap.html'), async (err: any, html: any) => {
      if (err) {
        callback({success: false, message: err});
        return;
      }
      try {
        var template = handlebars.compile(html);
        var htmlToSend = template(params);
        // let testAccount = await nodemailer.createTestAccount();
        let info = await transporter.sendMail({
          from: '<' + emailAcount + '>', // sender address
          to: to, // list of receivers
          subject: title, // Subject line
          html: htmlToSend, // html body
          attachments: attachments
        });
        callback({success: true, message: info.messageId});
      } catch (error) {
        callback({success: false, message: error});
      }
    });
  }

  static onSendMailDefault = async (id: any, title: string, to: any, params: any, callback: any) => {
    MailConfig.onReadHtmlFile(path.join(__dirname, '../../.digipost/template_mail/default_mail.html'), async (err: any, html: any) => {
      if (err) {
        callback({success: false, message: err});
        return;
      }
      try {
        var template = handlebars.compile(html);
        var htmlToSend = template(params);
        // let testAccount = await nodemailer.createTestAccount();
        let info = await transporter.sendMail({
          from: '<' + emailAcount + '>', // sender address
          to: to, // list of receivers
          subject: title, // Subject line
          html: htmlToSend
        });
        callback({success: true, message: info.messageId, id: id});
      } catch (error) {
        callback({success: false, message: error, id: id});
      }
    });
  }


  static onSendMailDocumentNonDigipos = async (title: string, to: any, params: any, attachments: any, callback: any) => {
    MailConfig.onReadHtmlFile(path.join(__dirname, '../../.non-digipos/template-mail/distribusi_doc.html'), async (err: any, html: any) => {
      if (err) {
        callback({success: false, message: err});
        return;
      }
      try {
        var template = handlebars.compile(html);
        var htmlToSend = template(params);
        // let testAccount = await nodemailer.createTestAccount();
        let info = await transporter.sendMail({
          from: '<' + emailAcount + '>', // sender address
          to: to, // list of receivers
          subject: title, // Subject line
          html: htmlToSend, // html body
          attachments: attachments
        });
        callback({success: true, message: info.messageId});
      } catch (error) {
        callback({success: false, message: error});
      }
    });
  }

}
