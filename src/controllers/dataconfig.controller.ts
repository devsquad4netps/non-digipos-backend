import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {Dataconfig} from '../models';
import {DataconfigRepository} from '../repositories';

export class DataconfigController {
  constructor(
    @repository(DataconfigRepository)
    public dataconfigRepository : DataconfigRepository,
  ) {}

  @post('/dataconfigs', {
    responses: {
      '200': {
        description: 'Dataconfig model instance',
        content: {'application/json': {schema: getModelSchemaRef(Dataconfig)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Dataconfig, {
            title: 'NewDataconfig',
            exclude: ['id'],
          }),
        },
      },
    })
    dataconfig: Omit<Dataconfig, 'id'>,
  ): Promise<Dataconfig> {
    return this.dataconfigRepository.create(dataconfig);
  }

  @get('/dataconfigs/count', {
    responses: {
      '200': {
        description: 'Dataconfig model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(Dataconfig) where?: Where<Dataconfig>,
  ): Promise<Count> {
    return this.dataconfigRepository.count(where);
  }

  @get('/dataconfigs', {
    responses: {
      '200': {
        description: 'Array of Dataconfig model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Dataconfig, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(Dataconfig) filter?: Filter<Dataconfig>,
  ): Promise<Dataconfig[]> {
    return this.dataconfigRepository.find(filter);
  }

  @patch('/dataconfigs', {
    responses: {
      '200': {
        description: 'Dataconfig PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Dataconfig, {partial: true}),
        },
      },
    })
    dataconfig: Dataconfig,
    @param.where(Dataconfig) where?: Where<Dataconfig>,
  ): Promise<Count> {
    return this.dataconfigRepository.updateAll(dataconfig, where);
  }

  @get('/dataconfigs/{id}', {
    responses: {
      '200': {
        description: 'Dataconfig model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Dataconfig, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(Dataconfig, {exclude: 'where'}) filter?: FilterExcludingWhere<Dataconfig>
  ): Promise<Dataconfig> {
    return this.dataconfigRepository.findById(id, filter);
  }

  @patch('/dataconfigs/{id}', {
    responses: {
      '204': {
        description: 'Dataconfig PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Dataconfig, {partial: true}),
        },
      },
    })
    dataconfig: Dataconfig,
  ): Promise<void> {
    await this.dataconfigRepository.updateById(id, dataconfig);
  }

  @put('/dataconfigs/{id}', {
    responses: {
      '204': {
        description: 'Dataconfig PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() dataconfig: Dataconfig,
  ): Promise<void> {
    await this.dataconfigRepository.replaceById(id, dataconfig);
  }

  @del('/dataconfigs/{id}', {
    responses: {
      '204': {
        description: 'Dataconfig DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.dataconfigRepository.deleteById(id);
  }
}
