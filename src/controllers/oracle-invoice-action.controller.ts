// Uncomment these imports to begin using these cool features!

import {authenticate} from '@loopback/authentication';
import {UserRepository} from '@loopback/authentication-jwt';
import {Filter, FilterExcludingWhere, repository} from '@loopback/repository';
import {get, getModelSchemaRef, param, post, requestBody} from '@loopback/rest';
import fs from 'fs';
import path from 'path';
import {AdMaster, DataDigipos, DataFpjp, NoFakturPajakDummy} from '../models';
import {AdMasterRepository, DataconfigRepository, DataDigiposRepository, DataFpjpRepository, DataUserRepository, NoFakturPajakDummyRepository, NoFakturPajakRepository, RolemappingRepository} from '../repositories';
import {ControllerConfig} from './controller.config';
// import {inject} from '@loopback/core';
const mergePDF = require('pdf-merge');

export const UpdateMasterRequestBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['ad_code', 'interface_status', "organization_id", "error_message"],
        properties: {
          interface_status: {
            type: 'string',
          },
          organization_id: {
            type: 'string',
          },
          ad_code: {
            type: 'string',
          },
          error_message: {
            type: 'string',
          }
        }
      }
    },
  },
};



export const UpdateInvoiceRequestBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['id', 'oracle_id', "interface_status", "error_message"],
        properties: {
          id: {
            type: 'number',
          },
          oracle_id: {
            type: 'string',
          },
          interface_status: {
            type: 'string',
          },
          error_message: {
            type: 'string',
          }
        }
      }
    },
  },
};



export const UpdateFPJPRequestBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['id', 'invoice_id', "interface_status", "error_message", "payment"],
        properties: {
          id: {
            type: 'number',
          },
          invoice_id: {
            type: 'string',
          },
          interface_status: {
            type: 'string',
          },
          error_message: {
            type: 'string',
          },
          payment: {
            type: 'string',
          },
        }
      }
    },
  },
};


export const RegisterFPNonDigiposRequestBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['id', 'invoice_number', "fp_number"],
        properties: {
          id: {
            type: 'string',
          },
          invoice_number: {
            type: 'string',
          },
          fp_number: {
            type: 'string',
          },
        }
      }
    },
  },
};



export const RequestFPNonDigiposRequestBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['count_req'],
        properties: {
          count_req: {
            type: 'number',
          },
          tahun: {
            type: 'string',
          },
        }
      }
    },
  },
};


export const CacelFPNonDigiposRequestBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['fp_number', 'invoice_number'],
        properties: {
          fp_number: {
            type: 'string',
          },
          invoice_number: {
            type: 'string',
          },
        }
      }
    },
  },
};

export const ChangeFPNonDigiposRequestBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['id', "fp_number"],
        properties: {
          id: {
            type: 'string',
          },
          fp_number: {
            type: 'string',
          },
        }
      }
    },
  },
};


export const UpdateFPNonDigiposRequestBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['id', "fp_number", "no_invoice", "tahun"],
        properties: {
          id: {
            type: 'string',
          },
          fp_number: {
            type: 'string',
          },
          no_invoice: {
            type: 'string',
          },
        }
      }
    },
  },
};

@authenticate('jwt')
export class OracleInvoiceActionController {
  constructor(
    @repository(DataDigiposRepository)
    public dataDigiposRepository: DataDigiposRepository,
    @repository(DataFpjpRepository)
    public dataFpjpRepository: DataFpjpRepository,
    @repository(DataUserRepository)
    public dataUserRepository: DataUserRepository,
    @repository(UserRepository)
    public userRepository: UserRepository,
    @repository(RolemappingRepository)
    public rolemappingRepository: RolemappingRepository,
    @repository(AdMasterRepository)
    public adMasterRepository: AdMasterRepository,
    @repository(DataconfigRepository)
    public dataconfigRepository: DataconfigRepository,
    @repository(NoFakturPajakRepository)
    public noFakturPajakRepository: NoFakturPajakRepository,
    @repository(NoFakturPajakDummyRepository)
    public noFakturPajakDummyRepository: NoFakturPajakDummyRepository,
  ) { }



  @get('/oracle/invoice', {
    responses: {
      '200': {
        description: 'Array of Invoice AR model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(DataDigipos, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(DataDigipos) filter?: Filter<DataDigipos>,
  ): Promise<DataDigipos[]> {

    var filterSet: any; filterSet = {}
    var filters: any; filters = filter;
    for (var i in filters) {
      filterSet[i] = filters[i];
    }

    if (filterSet.where != undefined) {
      var thisWhere = filterSet.where;
      filterSet.where = {and: [{acc_file_invoice: 1}, {vat_number: {neq: null}}, {vat_number: {neq: ""}}, thisWhere]};
    } else {
      filterSet.where = {and: [{acc_file_invoice: 1}, {vat_number: {neq: null}}, {vat_number: {neq: ""}}]}
    }

    var dataDigipos = await this.dataDigiposRepository.find(filterSet);
    var datas: any; datas = [];
    for (var i in dataDigipos) {
      var rawDigipos = dataDigipos[i];

      var PeriodeTrx = rawDigipos.trx_date_group.split("-");
      var dateLast = new Date(Number(PeriodeTrx[0]), Number(PeriodeTrx[1]), 0);
      var dateFaktur = new Date(Number(rawDigipos.at_vat_distibute));

      var monthPulsh1 = new Date(dateLast.getTime());
      var nextMounth = monthPulsh1.setMonth(monthPulsh1.getMonth() + 2);

      var invMounth = dateLast.getMonth() + 2;
      var fakturMounth = dateFaktur.getMonth() + 1;
      var dataPlushSet = monthPulsh1.getMonth();

      var dateInvoiceOracle = monthPulsh1.getFullYear() + "-" + "00".substring(dataPlushSet.toString().length) + dataPlushSet + "-" + "01";//dateLast.getDate();
      var datFakturOracle = dateFaktur.getFullYear() + "-" + "00".substring(fakturMounth.toString().length) + fakturMounth + "-" + dateFaktur.getDate();

      if (rawDigipos.at_vat_distibute == null || rawDigipos.at_vat_distibute == "") {
        datFakturOracle = "";
      }

      var users = await this.userRepository.find({where: {realm: "USERS"}});
      var emailAR: string; emailAR = "";
      var emailBU: string; emailBU = "";
      var lengthEmailBU = 0;
      var lengthEmailAR = 0;
      for (var iUsr in users) {
        var userRaw = users[iUsr];
        var userRole = await this.rolemappingRepository.findOne({where: {UserId: userRaw.id}});

        if (userRole == null) {
          continue;
        }
        if (userRole.RoleId != 6 && userRole.RoleId != 5) {
          continue;
        }
        if (userRole.RoleId == 6) {
          lengthEmailAR = emailAR.length + userRaw.email.length;
          if (lengthEmailAR <= 150) {
            emailAR += ";" + userRaw.email;
          }
        }
        if (userRole.RoleId == 5) {
          lengthEmailBU = emailBU.length + userRaw.email.length;
          if (lengthEmailBU <= 150) {
            emailBU += ";" + userRaw.email;
          }
        }

      }
      // var adInfo = await this.adMasterRepository.findById(rawDigipos.ad_code);
      // var EmailAd: string; EmailAd = "";
      // if (adInfo) {
      //   EmailAd = adInfo.email;
      // }

      var no_faktur = rawDigipos.vat_number?.substring(0, 3);
      var dataAD = await this.adMasterRepository.findById(rawDigipos.ad_code);
      var data: any; data = {
        "id": rawDigipos.digipost_id,
        "transaction_num": rawDigipos.inv_number,
        "transaction_date": dateInvoiceOracle,
        "customer_code": dataAD.ad_code == undefined ? "" : dataAD.ad_code.toUpperCase(),
        "customer": dataAD.global_name == undefined ? "" : dataAD.global_name.toUpperCase(),
        "jenus_cust": "Badan",
        "bulk": "N",
        "jenis_inv": "Paid Invoice",
        "syariah_non": "Revenue Trx Fee Regular",
        "faktur_pajak_number": rawDigipos.vat_number,
        "faktur_pajak_tanggal": datFakturOracle,
        "email_to": emailBU.substring(1),
        "email_cc": emailAR.substring(1),
        "line_desc": "Sales Fee " + ControllerConfig.onGetMonth(dateLast.getMonth()) + " " + dateLast.getFullYear(),
        "tribe": "LA-PDigi-TLCR",
        "dpp": Number(rawDigipos.dpp_paid_amount.toFixed(0)),
        "vat_code": "VAT OUT_A2_10%_" + no_faktur,
        "vat": Number(rawDigipos.vat_paid_amount.toFixed(0)),
        "pph_23": 0,//rawDigipos.pph_paid_amount,
        "total_amount": Number(rawDigipos.total_paid_amount.toFixed(0)),
        "oracle_id": rawDigipos.oracle_id,
        "interface_status": rawDigipos.interface_status,
        "error_message": rawDigipos.error_message,
        "doc_inv": {"name": rawDigipos.inv_attacment, "type": "pdf", "content": ""},
        "doc_inv_sign": {"name": rawDigipos.attach_invoice_ttd, "type": "pdf", "content": ""},
        "doc_faktur_pajak": {"name": rawDigipos.vat_attachment, "type": "pdf", "content": ""}
      };

      await new Promise<object>(async (resolve, reject) => {
        const filePathPaidInvoiceSign = path.join(__dirname, '../../.digipost/doc-distribute/invoice-sign/' + rawDigipos.attach_invoice_ttd);
        if (rawDigipos.attach_invoice_ttd != "" && rawDigipos.attach_invoice_ttd != undefined && rawDigipos.attach_invoice_ttd != null) {
          if (fs.existsSync(filePathPaidInvoiceSign)) {
            fs.readFile(filePathPaidInvoiceSign, function (PaidInvErr, PaidInvData) {
              if (PaidInvErr) {
                resolve({success: false});
              } else {
                data.doc_inv_sign.content = "/oracle/document/invoicesign/" + rawDigipos.digipost_id;
                resolve({success: true});
              }
            });
            resolve({success: false});
          } else {
            resolve({success: false});
          }
        } else {
          resolve({success: false});
        }
      });

      await new Promise<object>(async (resolve, reject) => {
        const filePathInvoice = path.join(__dirname, '../../.digipost/invoice/' + rawDigipos.inv_attacment);
        if (rawDigipos.inv_attacment != "" && rawDigipos.inv_attacment != undefined && rawDigipos.inv_attacment != null) {
          if (fs.existsSync(filePathInvoice)) {
            fs.readFile(filePathInvoice, function (InvErr, InvData) {
              if (InvErr) {
                resolve({success: false});
              } else {
                data.doc_inv.content = "/oracle/document/invoice/" + rawDigipos.digipost_id;
                resolve({success: true});
              }
            });
          } else {
            resolve({success: false});
          }
        } else {
          resolve({success: false});
        }
      });

      await new Promise<object>(async (resolve, reject) => {
        const filePathVat = path.join(__dirname, '../../.digipost/doc-distribute/faktur-pajak/' + rawDigipos.vat_attachment);
        if (rawDigipos.vat_attachment != "" && rawDigipos.vat_attachment != undefined && rawDigipos.vat_attachment != null) {
          if (fs.existsSync(filePathVat)) {
            fs.readFile(filePathVat, function (VatErr, VatData) {
              if (VatErr) {
                resolve({success: false});
              } else {
                data.doc_faktur_pajak.content = "/oracle/document/faktur/" + rawDigipos.digipost_id;
                resolve({success: true});
              }
            });
          } else {
            resolve({success: false});
          }
        } else {
          resolve({success: false});
        }
      });

      datas.push(data);
    }
    return datas
  }


  @get('/oracle/customer', {
    responses: {
      '200': {
        description: 'Array of Cistomer  model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(AdMaster, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async findCustomer(
    @param.filter(AdMaster) filter?: Filter<DataDigipos>,
  ): Promise<AdMaster[]> {


    var dataMaster = await this.adMasterRepository.find(filter);
    var datas: any; datas = [];
    for (var i in dataMaster) {
      var rawMaster = dataMaster[i];
      var data: any; data = {
        "intervace_status": rawMaster.interface_status,
        "organization_id": rawMaster.organization_id,
        "error_message": rawMaster.error_message,
        "ad_code": rawMaster.ad_code,
        "ad_name": rawMaster.global_name.toUpperCase(),//CV JADI DIBELAKANG
        "account_desc": rawMaster.ad_name.toUpperCase(),
        "legal_name": rawMaster.legal_name.toUpperCase(),
        "customer_class": "THIRD PARTY",
        "customer_source": "BA-TOOLS",
        "npwp": rawMaster.npwp,
        "perfix": rawMaster.prefix,
        "expire_date": rawMaster.expire_date,
        "site_name": rawMaster.city.toUpperCase(),//rawMaster.ad_code,
        "addres_npwp": rawMaster.npwp_address.toUpperCase(),
        "address": rawMaster.address.toUpperCase(),
        "city": rawMaster.city.toUpperCase(),
        "provinsi": "",
        "postal_code": rawMaster.post_code,
        "country_id": "ID",
        "business": "LinkAja",
        "bill_to": "10.00000.000000.00000000.11220001.0000.00000.00.0000.0000.0000",
        "first_name": rawMaster.attention.toUpperCase(),
        "last_name": "",
        "phone_area_code": "",
        "phone_number": "",
        "mobile_phone_code": "62",
        "mobile_phone_number": rawMaster.phone_number,
        "email": rawMaster.email
      }
      datas.push(data);
    }
    return datas;
  }


  @post('/oracle/customer/update', {
    responses: {
      '200': {
        description: 'Customer model instance',
        content: {
          'application/json': {
            schema: UpdateMasterRequestBody
          }
        },
      },
    },
  })
  async updateCustomer(
    @requestBody(UpdateMasterRequestBody) updateMaster: {interface_status: string, organization_id: string, ad_code: string, error_message: string},
  ): Promise<Object> {
    var returnMs: any; returnMs = {success: true, message: ""};
    try {
      await this.adMasterRepository.updateById(updateMaster.ad_code, {
        organization_id: updateMaster.organization_id,
        interface_status: updateMaster.interface_status,
        error_message: updateMaster.error_message
      });
      returnMs.success = true;
      returnMs.message = "SUCCESS !";
    } catch (error) {
      returnMs.success = false;
      returnMs.message = error;
    }
    return returnMs;
  }


  @post('/oracle/invoice/update', {
    responses: {
      '200': {
        description: 'Invoice model instance',
        content: {
          'application/json': {
            schema: UpdateInvoiceRequestBody
          }
        },
      },
    },
  })
  async updateInvoice(
    @requestBody(UpdateInvoiceRequestBody) updateMaster: {id: number, oracle_id: string, interface_status: string, error_message: string},
  ): Promise<Object> {
    var returnMs: any; returnMs = {success: true, message: ""};
    try {
      await this.dataDigiposRepository.updateById(updateMaster.id, {
        oracle_id: updateMaster.oracle_id,
        interface_status: updateMaster.interface_status,
        error_message: updateMaster.error_message
      });
      returnMs.success = true;
      returnMs.message = "SUCCESS !";
    } catch (error) {
      returnMs.success = false;
      returnMs.message = error;
    }
    return returnMs;
  }


  //======================================================FPJP ORACLE=========================


  @get('/oracle/fpjp', {
    responses: {
      '200': {
        description: 'Array of Cistomer  model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(DataFpjp, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async findFPJP(
    @param.filter(DataFpjp) filter?: Filter<DataFpjp>,
  ): Promise<DataFpjp[]> {

    var filterSet: any; filterSet = {}
    var filters: any; filters = filter;
    for (var i in filters) {
      filterSet[i] = filters[i];
    }


    if (filterSet.where != undefined) {
      var thisWhere = filterSet.where;
      filterSet.where = {and: [{state: 3}, {fpjp_hold_state: 0}, thisWhere]};
    } else {
      filterSet.where = {and: [{state: 3}, {fpjp_hold_state: 0}]}
    }


    //CONFIG



    var dataFPJP = await this.dataFpjpRepository.find(filterSet);

    var datas: any; datas = [];
    for (var i in dataFPJP) {
      var rawFPJP = dataFPJP[i];
      var rawAd = await this.adMasterRepository.findById(rawFPJP.ad_code);

      var dateInvoice = new Date(Number(rawFPJP.at_ad_submit_fpjp));
      var invMounth = dateInvoice.getMonth() + 1;
      var datFakturOracle = dateInvoice.getFullYear() + "-" + "00".substring(invMounth.toString().length) + invMounth + "-" + "00".substring(dateInvoice.getDate().toString().length) + dateInvoice.getDate();
      var dateAccounting = datFakturOracle;

      // == > 26 -> tanngal 1 bulan berikut
      if (Number(dateInvoice.getDate()) >= 26) {
        var invMounth = dateInvoice.getMonth() + 2;
        var dateInvoiceOracle = dateInvoice.getFullYear() + "-" + "00".substring(invMounth.toString().length) + invMounth + "-" + "01";//dateLast.getDate();
        dateAccounting = dateInvoiceOracle;
      }

      var USER_ID = rawFPJP.by_submit_fpjp == undefined ? "-1" : rawFPJP.by_submit_fpjp;
      var USER_SUBMIT = await this.userRepository.findById(USER_ID);

      var periodeFpjp = rawFPJP.periode;
      var splitPeriode = periodeFpjp.split("-");

      const configData = await this.dataconfigRepository.find({where: {config_group_id: 1}});
      var configNum: any;
      configNum = {};
      for (var i in configData) {
        var infoConfig = configData[i];
        if (infoConfig.config_code == "0001-01") {
          configNum["persen_fee"] = infoConfig.config_value;
        }
        if (infoConfig.config_code == "0001-02") {
          configNum["persen_paid"] = infoConfig.config_value;
        }

        if (infoConfig.config_code == "0001-03") {
          configNum["num_dpp_fee"] = infoConfig.config_value;
        }
        if (infoConfig.config_code == "0001-04") {
          configNum["num_ppn_fee"] = infoConfig.config_value;
        }
        if (infoConfig.config_code == "0001-05") {
          configNum["num_pph_fee"] = infoConfig.config_value;
        }

        if (infoConfig.config_code == "0001-11") {
          configNum["num_dpp_paid"] = infoConfig.config_value;
        }
        if (infoConfig.config_code == "0001-12") {
          configNum["num_ppn_paid"] = infoConfig.config_value;
        }
        if (infoConfig.config_code == "0001-13") {
          configNum["num_pph_paid"] = infoConfig.config_value;
        }

        if (infoConfig.config_code == "0001-06") {
          configNum["persen_fee_ppob"] = infoConfig.config_value;
        }
        if (infoConfig.config_code == "0001-07") {
          configNum["persen_paid_ppob"] = infoConfig.config_value;
        }
        if (infoConfig.config_code == "0001-08") {
          configNum["num_dpp_ppob"] = infoConfig.config_value;
        }
        if (infoConfig.config_code == "0001-09") {
          configNum["num_ppn_ppob"] = infoConfig.config_value;
        }
        if (infoConfig.config_code == "0001-10") {
          configNum["num_pph_ppob"] = infoConfig.config_value;
        }
      }

      var fpjp_type = rawFPJP.type_trx == 2 ? "Reimbursement PPh 23" : "Revenue Sharing";
      var lineAcount = rawFPJP.type_trx == 2 ? rawFPJP.total_amount : rawFPJP.price;
      var segment5 = rawFPJP.type_trx == 2 ? "11512004" : "41100001";
      var data: any; data = {
        "id": rawFPJP.id,
        "INVOICE_NUMBER": rawFPJP.inv_number,
        "INVOICE_CURRENCY": "IDR",
        "CONVERTION_RATE": "",
        "TOTAL_AMOUNT": rawFPJP.total_amount,
        "INVOICE_DATE": rawFPJP.inv_date == null || rawFPJP.inv_date == "" ? datFakturOracle : rawFPJP.inv_date,
        "SUPPLIER": rawAd.global_name.toUpperCase(),
        "SUPPLIER_SITE": rawAd.city.toUpperCase(),
        "DESCRIPTION": ((rawFPJP.type_trx == 2 ? "Pembayaran Reimbursement PPh 23 Digipos Periode " : "Pembayaran Sales Fee Digipos Periode ") + ControllerConfig.onGetMonth((Number(splitPeriode[1]) - 1)) + " " + splitPeriode[0]).toUpperCase(),//rawFPJP.description.toUpperCase(),
        "ACCOUNTING_DATE": "",// dateAccounting,
        "FPJP_NUMBER": rawFPJP.transaction_code,
        "FPJP_NAME": fpjp_type + " Periode " + ControllerConfig.onGetMonth((Number(splitPeriode[1]) - 1)) + " " + splitPeriode[0],
        "FPJP_TYPE": fpjp_type,
        "ACCT_NAME": rawAd.name_rekening.toUpperCase(),
        "SUPPLIER_BANK_NAME": rawAd.name_bank.toUpperCase(),
        "SUPPLIER_BANK_ACCT_NUMBER": rawAd.no_rek,
        "ONE_TIME_SUPPLIER_FLAG": "N",
        "INCLUDE_TAX": rawFPJP.type_trx == 2 ? "N" : "Y",
        "RATE": rawFPJP.type_trx == 2 ? 0 : (Number(configNum["num_ppn_fee"]) * 100),
        "TERMS": 14,
        "FPJP_NOTE": rawFPJP.remarks?.toUpperCase(),
        "LINE_NUMBER": lineAcount,
        "LINE_AMOUNT": rawFPJP.price,//lineAcount,
        "SEGMENT1": "10",
        "SEGMENT2": "00000",
        "SEGMENT3": "134004",
        "SEGMENT4": "00000000",
        "SEGMENT5": segment5,
        "SEGMENT6": "1001",
        "SEGMENT7": "00008",
        "SEGMENT8": "00",
        "SEGMENT9": "0000",
        "SEGMENT10": "0000",
        "SEGMENT11": "0000",
        "LINE_DESCRIPTION": ((rawFPJP.type_trx == 2 ? "Pembayaran Reimbursement PPh 23 Digipos Periode " : "Pembayaran Sales Fee Digipos Periode ") + ControllerConfig.onGetMonth((Number(splitPeriode[1]) - 1)) + " " + splitPeriode[0]).toUpperCase(),//rawFPJP.description.toUpperCase(),
        "PAYMENT": (rawFPJP.state == 4 ? "Y" : "N"),
        "DOC": [],
        "INVOICE_ID": rawFPJP.invoice_id,
        "INTERFACE_STATUS": rawFPJP.interface_status,
        "ERROR_MESSAGE": rawFPJP.error_message,
        "FPJP_SUBMITED": USER_SUBMIT == null ? "nita@linkaja.id;iis_istianah@linkaja.id;telcosaleschannel@linkaja.id;" : "nita@linkaja.id;iis_istianah@linkaja.id;telcosaleschannel@linkaja.id;" + (["nita@linkaja.id", "iis_istianah@linkaja.id", "telcosaleschannel@linkaja.id"].indexOf(USER_SUBMIT.email) == -1 ? USER_SUBMIT.email : "")
      }

      // '/oracle/document/fpjppel/{id}'
      // '/oracle/document/fpjp/{id}'

      await new Promise<object>(async (resolve, reject) => {
        const filePathFPJP = path.join(__dirname, '../../.digipost/fpjp/' + rawFPJP.fpjp_attach);
        if (rawFPJP.fpjp_attach != "" && rawFPJP.fpjp_attach != undefined && rawFPJP.fpjp_attach != null) {
          if (fs.existsSync(filePathFPJP)) {
            fs.readFile(filePathFPJP, function (FPJPErr, FPJPData) {
              if (FPJPErr) {
                resolve({success: false});
              } else {
                data.DOC.push({"name": "DOC_FPJP", "type": "pdf", "content": "/oracle/document/fpjp/" + rawFPJP.id});
                data.DOC.push({"name": "DOC_PENDUKUNG", "type": "pdf", "content": "/oracle/document/fpjppel/" + rawFPJP.id});
                resolve({success: true});
              }
            });
          } else {
            resolve({success: false});
          }
        } else {
          resolve({success: false});
        }
      });

      datas.push(data);
    }
    return datas;
  }

  @post('/oracle/fpjp/update', {
    responses: {
      '200': {
        description: 'Invoice model instance',
        content: {
          'application/json': {
            schema: UpdateFPJPRequestBody
          }
        },
      },
    },
  })
  async updateFPJP(
    @requestBody(UpdateFPJPRequestBody) updateFPJP: {id: number, invoice_id: string, interface_status: string, error_message: string, payment: string},
  ): Promise<Object> {
    var returnMs: any; returnMs = {success: true, message: ""};
    try {
      var data: any; data = {
        invoice_id: updateFPJP.invoice_id,
        interface_status: updateFPJP.interface_status,
        error_message: updateFPJP.error_message
      }

      if (updateFPJP.payment == "Y") {
        data = {
          invoice_id: updateFPJP.invoice_id,
          interface_status: updateFPJP.interface_status,
          error_message: updateFPJP.error_message,
          state: 4
        }
      }

      var readyFPJP = await this.dataFpjpRepository.findOne({where: {and: [{id: updateFPJP.id}, {state: 3}, {fpjp_hold_state: 0}]}});
      if (readyFPJP != null) {
        await this.dataFpjpRepository.updateById(updateFPJP.id, data);
        returnMs.success = true;
        returnMs.message = "SUCCESS !";
      } else {
        returnMs.success = true;
        returnMs.message = "document fpjp not found !";
      }

    } catch (error) {
      returnMs.success = false;
      returnMs.message = error;
    }
    return returnMs;
  }



  @get('/oracle/no_faktur/nondigipos', {
    responses: {
      '200': {
        description: 'Array of Cistomer  model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(NoFakturPajakDummy, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async findNoFaktur(
    @param.filter(NoFakturPajakDummy) filter?: Filter<NoFakturPajakDummy>,
  ): Promise<NoFakturPajakDummy[]> {

    var filterSet: any; filterSet = {}
    var filters: any; filters = filter;
    for (var i in filters) {
      filterSet[i] = filters[i];
    }


    if (filterSet.where != undefined) {
      var thisWhere = filterSet.where;
      filterSet.where = {and: [{type_faktur: 2}, {or: [{no_invoice_link: null}, {no_invoice_link: ""}]}, thisWhere]};
    } else {
      filterSet.where = {and: [{type_faktur: 2}, {or: [{no_invoice_link: null}, {no_invoice_link: ""}]}]}
    }


    var datas: any; datas = [];

    var dataNoFaktur = await this.noFakturPajakRepository.find(filterSet);
    for (var iNo in dataNoFaktur) {
      var rawFaktur = dataNoFaktur[iNo];
      var LabelSet = rawFaktur.description == undefined ? "".split("-") : rawFaktur.description.split("-");
      if (LabelSet[0].trim() != "NON_DIGIPOS") {
        continue;
      }
      datas.push({
        ID: rawFaktur.no_faktur_pajak,
        SEQUENTIAL_FP: rawFaktur.number_fp,
        NO_FAKTUR_PAJAK_DEFAULT: rawFaktur.no_faktur_link,
        DESCRIPTION: rawFaktur.description,
        PERIODE: rawFaktur.periode
      });
    }

    return datas;
  }


  @post('/oracle/no_faktur/nondigipos/request', {
    responses: {
      '200': {
        description: 'Invoice model instance',
        content: {
          'application/json': {
            schema: RequestFPNonDigiposRequestBody
          }
        },
      },
    },
  })
  async requestNoFaktur(
    @requestBody(RequestFPNonDigiposRequestBody) requestData: {count_req: number, tahun: string},
  ): Promise<Object> {
    var returnMs: any; returnMs = {success: true, message: ""};
    try {
      if (requestData.count_req > 0 && requestData.count_req != null && requestData.count_req != undefined) {
        var readyNoFP = await this.noFakturPajakRepository.find({where: {and: [{fp_state: 0}]}, limit: requestData.count_req, order: ["id ASC"]});
        if (requestData.tahun != "" || requestData.tahun != undefined) {
          readyNoFP = await this.noFakturPajakRepository.find({where: {and: [{fp_state: 0}, {periode: requestData.tahun}]}, limit: requestData.count_req, order: ["id ASC"]});
        }

        if (readyNoFP.length > 0) {
          for (var iNo in readyNoFP) {
            var infoNoFP = readyNoFP[iNo];
            await this.noFakturPajakRepository.updateById(infoNoFP.id, {
              description: "NON_DIGIPOS-Register Faktur From Oracle",
              type_faktur: 2,
              fp_state: 1,
              no_invoice_link: undefined,
              no_faktur_link: undefined,
              at_used: ControllerConfig.onCurrentTime().toString()
            });
          }
        } else {
          returnMs.success = true;
          returnMs.message = "not avaliable no faktur pajak, please confirm to tax team !";
        }
      } else {
        returnMs.success = true;
        returnMs.message = "not data requuest !";
      }

    } catch (error) {
      returnMs.success = false;
      returnMs.message = error;
    }
    return returnMs;
  }


  @post('/oracle/no_faktur/nondigipos/used', {
    responses: {
      '200': {
        description: 'Invoice model instance',
        content: {
          'application/json': {
            schema: RegisterFPNonDigiposRequestBody
          }
        },
      },
    },
  })
  async registerNoFaktur(
    @requestBody(RegisterFPNonDigiposRequestBody) registerNo: {id: string, invoice_number: string, fp_number: string},
  ): Promise<Object> {
    var returnMs: any; returnMs = {success: true, message: ""};
    try {

      var readyNoFP = await this.noFakturPajakRepository.findOne({where: {and: [{no_faktur_pajak: registerNo.id}]}});
      if (readyNoFP != null) {
        var LabelSet = readyNoFP.description == undefined ? "".split("-") : readyNoFP.description.split("-");
        if (LabelSet[0].trim() == "NON_DIGIPOS") {
          if (readyNoFP.no_invoice_link == null || readyNoFP.no_invoice_link == "") {

            var hasNoInv = await this.noFakturPajakRepository.findOne({where: {and: [{no_invoice_link: registerNo.invoice_number}]}});
            if (hasNoInv == null) {
              await this.noFakturPajakRepository.updateById(readyNoFP.id, {
                no_invoice_link: registerNo.invoice_number,
                no_faktur_link: registerNo.fp_number,
                at_used: ControllerConfig.onCurrentTime().toString()
              });
              returnMs.success = true;
              returnMs.message = "SUCCESS !";
            } else {
              returnMs.success = true;
              returnMs.message = "No invoice sudah memiliki no faktur !";
            }
          } else {
            returnMs.success = true;
            returnMs.message = "No Faktur Pajak tidak diperuntukan untuk non digipos !";
          }
        } else {
          returnMs.success = true;
          returnMs.message = "No Faktur Pajak Sudah Digunakan !";
        }
      } else {
        returnMs.success = true;
        returnMs.message = "document fpjp not found !";
      }

    } catch (error) {
      returnMs.success = false;
      returnMs.message = error;
    }
    return returnMs;
  }

  @post('/oracle/no_faktur/nondigipos/cancel', {
    responses: {
      '200': {
        description: 'Invoice model instance',
        content: {
          'application/json': {
            schema: CacelFPNonDigiposRequestBody
          }
        },
      },
    },
  })
  async cancelNoFaktur(
    @requestBody(CacelFPNonDigiposRequestBody) registerNo: {invoice_number: string, fp_number: string},
  ): Promise<Object> {
    var returnMs: any; returnMs = {success: true, message: ""};
    try {

      var readyNoFP = await this.noFakturPajakRepository.findOne({where: {and: [{no_faktur_link: registerNo.fp_number}, {no_invoice_link: registerNo.invoice_number}]}});
      if (readyNoFP != null) {
        var LabelSet = readyNoFP.description == undefined ? "".split("-") : readyNoFP.description.split("-");
        if (LabelSet[0].trim() == "NON_DIGIPOS") {
          try {
            await this.noFakturPajakRepository.updateById(readyNoFP.id, {
              no_invoice_link: undefined,
              type_faktur: 1,
              no_faktur_link: undefined,
              fp_state: 0,
              description: undefined,
              at_used: undefined
            });
            returnMs.success = true;
            returnMs.message = "Success No Faktur Pajak Cancel !";
          } catch (error) {
            returnMs.success = false;
            returnMs.message = error;
          }
        } else {
          returnMs.success = true;
          returnMs.message = "No Faktur Pajak not found !";
        }
      } else {
        returnMs.success = true;
        returnMs.message = "No Faktur Pajak not found !";
      }
    } catch (error) {
      returnMs.success = false;
      returnMs.message = error;
    }
    return returnMs;
  }



  @post('/oracle/no_faktur/nondigipos/update-info', {
    responses: {
      '200': {
        description: 'Invoice model instance',
        content: {
          'application/json': {
            schema: UpdateFPNonDigiposRequestBody
          }
        },
      },
    },
  })
  async updateInfoNoFaktur(
    @requestBody(UpdateFPNonDigiposRequestBody) registerNo: {id: string, fp_number: string, no_invoice: string},
  ): Promise<Object> {
    var returnMs: any; returnMs = {success: true, message: ""};
    try {
      var readyNoFP = await this.noFakturPajakRepository.findOne({where: {and: [{no_faktur_pajak: registerNo.id}]}});
      if (readyNoFP != null) {
        var LabelSet = readyNoFP.description == undefined ? "".split("-") : readyNoFP.description.split("-");
        if (LabelSet[0].trim() == "NON_DIGIPOS") {
          var infoInv = await this.noFakturPajakRepository.findOne({where: {and: [{no_invoice_link: registerNo.no_invoice}, {no_faktur_pajak: {neq: registerNo.id}}]}});

          if (infoInv == null) {
            try {
              await this.noFakturPajakRepository.updateById(readyNoFP.id, {
                no_faktur_link: registerNo.fp_number,
                no_invoice_link: registerNo.fp_number,
                at_used: ControllerConfig.onCurrentTime().toString()
              });
              returnMs.success = true;
              returnMs.message = "Success No Faktur Pajak Update !";
            } catch (error) {
              returnMs.success = false;
              returnMs.message = error;
            }
          } else {
            returnMs.success = true;
            returnMs.message = "Number Invoice already have no faktur !";
          }

        } else {
          returnMs.success = true;
          returnMs.message = "No Faktur Pajak not found !";
        }
      } else {
        returnMs.success = true;
        returnMs.message = "No Faktur Pajak not found !";
      }
    } catch (error) {
      returnMs.success = false;
      returnMs.message = error;
    }
    return returnMs;
  }

  @post('/oracle/no_faktur/nondigipos/change', {
    responses: {
      '200': {
        description: 'Invoice model instance',
        content: {
          'application/json': {
            schema: ChangeFPNonDigiposRequestBody
          }
        },
      },
    },
  })
  async changeNoFaktur(
    @requestBody(ChangeFPNonDigiposRequestBody) registerNo: {id: string, fp_number: string},
  ): Promise<Object> {
    var returnMs: any; returnMs = {success: true, message: ""};
    try {
      var readyNoFP = await this.noFakturPajakRepository.findOne({where: {and: [{no_faktur_pajak: registerNo.id}]}});
      if (readyNoFP != null) {
        var LabelSet = readyNoFP.description == undefined ? "".split("-") : readyNoFP.description.split("-");
        if (LabelSet[0].trim() == "NON_DIGIPOS") {
          var NoFakturChange = registerNo.fp_number.substring(4);
          if (NoFakturChange == registerNo.id) {
            try {
              await this.noFakturPajakRepository.updateById(readyNoFP.id, {
                no_faktur_link: registerNo.fp_number,
                at_used: ControllerConfig.onCurrentTime().toString()
              });
              returnMs.success = true;
              returnMs.message = "Success No Faktur Pajak Cancel !";
            } catch (error) {
              returnMs.success = false;
              returnMs.message = error;
            }
          } else {
            returnMs.success = true;
            returnMs.message = "Format No Faktur Pajak Change Vailed !";
          }

        } else {
          returnMs.success = true;
          returnMs.message = "No Faktur Pajak not found !";
        }
      } else {
        returnMs.success = true;
        returnMs.message = "No Faktur Pajak not found !";
      }
    } catch (error) {
      returnMs.success = false;
      returnMs.message = error;
    }
    return returnMs;
  }


  //VIEW FILE DOCUMENT END POINT

  //VIEW INVOICE SIGN
  @get('/oracle/document/invoicesign/{id}', {
    responses: {
      '200': {
        description: 'non digipos product model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(DataDigipos, {includeRelations: true}),
          },
        },
      },
    },
  })
  async viewDocumentInvoiceSign(
    @param.path.number('id') id: number,
    @param.filter(DataDigipos, {exclude: 'where'}) filter?: FilterExcludingWhere<DataDigipos>
  ): Promise<Object> {
    return new Promise<object>(async (resolve, reject) => {
      var result: any; result = {name: "", type: "pdf", content: ""}
      var rawDigipos = await this.dataDigiposRepository.findById(id);
      const filePathInvoice = path.join(__dirname, '../../.digipost/doc-distribute/invoice-sign/' + rawDigipos.attach_invoice_ttd);
      fs.readFile(filePathInvoice, function (err, data) {
        if (err) {
          resolve({name: "", type: "pdf", content: ""});
        } else {
          result.content = data.toString("base64");
          result.name = rawDigipos.attach_invoice_ttd;
          resolve(result);
        }
      });
    });
  }


  //VIEW INVOICE
  @get('/oracle/document/invoice/{id}', {
    responses: {
      '200': {
        description: 'non digipos product model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(DataDigipos, {includeRelations: true}),
          },
        },
      },
    },
  })
  async viewDocumentInv(
    @param.path.number('id') id: number,
    @param.filter(DataDigipos, {exclude: 'where'}) filter?: FilterExcludingWhere<DataDigipos>
  ): Promise<Object> {
    var result: any; result = {name: "", type: "pdf", content: ""}
    var rawDigipos = await this.dataDigiposRepository.findById(id);

    if (rawDigipos.digipost_id != undefined) {
      await new Promise<object>(async (resolve, reject) => {
        const filePathInvoice = path.join(__dirname, '../../.digipost/invoice/' + rawDigipos.inv_attacment);
        if (rawDigipos.inv_attacment != "" && rawDigipos.inv_attacment != undefined && rawDigipos.inv_attacment != null) {
          if (fs.existsSync(filePathInvoice)) {
            fs.readFile(filePathInvoice, function (InvErr, InvData) {
              if (InvErr) {
                resolve({success: false});
              } else {
                result.name = rawDigipos.inv_attacment;
                result.content = InvData.toString("base64");
                resolve({success: true});
              }
            });
          } else {
            resolve({success: false});
          }
        } else {
          resolve({success: false});
        }
      });
    }

    return result;
  }

  //VIEW FAKTUR PAJAK
  @get('/oracle/document/faktur/{id}', {
    responses: {
      '200': {
        description: 'non digipos product model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(DataDigipos, {includeRelations: true}),
          },
        },
      },
    },
  })
  async viewDocumentFaktur(
    @param.path.number('id') id: number,
    @param.filter(DataDigipos, {exclude: 'where'}) filter?: FilterExcludingWhere<DataDigipos>
  ): Promise<Object> {
    var result: any; result = {name: "", type: "pdf", content: ""}
    var rawDigipos = await this.dataDigiposRepository.findById(id);

    if (rawDigipos.digipost_id != undefined) {
      await new Promise<object>(async (resolve, reject) => {
        const filePathVat = path.join(__dirname, '../../.digipost/doc-distribute/faktur-pajak/' + rawDigipos.vat_attachment);
        if (rawDigipos.vat_attachment != "" && rawDigipos.vat_attachment != undefined && rawDigipos.vat_attachment != null) {
          if (fs.existsSync(filePathVat)) {
            fs.readFile(filePathVat, function (VatErr, VatData) {
              if (VatErr) {
                resolve({success: false});
              } else {
                result.name = rawDigipos.vat_attachment;
                result.content = VatData.toString("base64");
                resolve({success: true});
              }
            });
          } else {
            resolve({success: false});
          }
        } else {
          resolve({success: false});
        }
      });
    }

    return result;
  }


  //VIEW FPJP
  @get('/oracle/document/fpjp/{id}', {
    responses: {
      '200': {
        description: 'non digipos product model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(DataFpjp, {includeRelations: true}),
          },
        },
      },
    },
  })
  async viewDocumentFPJP(
    @param.path.number('id') id: number,
    @param.filter(DataFpjp, {exclude: 'where'}) filter?: FilterExcludingWhere<DataFpjp>
  ): Promise<Object> {
    var result: any; result = {name: "", type: "pdf", content: ""};
    var rawFPJP = await this.dataFpjpRepository.findById(id);

    if (rawFPJP.id != undefined) {
      await new Promise<object>(async (resolve, reject) => {
        const filePathFPJP = path.join(__dirname, '../../.digipost/fpjp/' + rawFPJP.fpjp_attach);
        if (rawFPJP.fpjp_attach != "" && rawFPJP.fpjp_attach != undefined && rawFPJP.fpjp_attach != null) {
          if (fs.existsSync(filePathFPJP)) {
            fs.readFile(filePathFPJP, function (FPJPErr, FPJPData) {
              if (FPJPErr) {
                resolve({success: false});
              } else {
                result.name = "DOC_FPJP";
                result.content = FPJPData.toString("base64");
                resolve({success: true});
              }
            });
          } else {
            resolve({success: false});
          }
        } else {
          resolve({success: false});
        }
      });
    }

    return result;
  }

  //VIEW PELENGKAP FPJP
  @get('/oracle/document/fpjppel/{id}', {
    responses: {
      '200': {
        description: 'non digipos product model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(DataFpjp, {includeRelations: true}),
          },
        },
      },
    },
  })
  async viewDocumentPelengkapFPJP(
    @param.path.number('id') id: number,
    @param.filter(DataFpjp, {exclude: 'where'}) filter?: FilterExcludingWhere<DataFpjp>
  ): Promise<Object> {
    return new Promise<object>(async (resolve, reject) => {

      var result: any; result = {name: "", type: "pdf", content: ""};

      var no = 0;
      var infoFPJP = await this.dataFpjpRepository.findOne({where: {id: id}});

      // const mergePDF = require('easy-pdf-merge');
      var MergePDFFile: any; MergePDFFile = [];

      //DOC FPJP
      if (infoFPJP != null) {


        if (infoFPJP.type_trx == 2) {
          //DOC NOTA DEBIT
          const filePathNota = path.join(__dirname, '../../../portal-endpoint/.uploads/nota_debit/nota/' + infoFPJP.inv_attahment);
          if (infoFPJP.inv_attahment != null && infoFPJP.inv_attahment != "" && infoFPJP.inv_attahment != undefined) {
            await new Promise((resolve, reject) => {
              if (fs.existsSync(filePathNota)) {
                MergePDFFile.push(filePathNota);
                // merger.add(filePathNota);
                resolve({success: true, message: filePathNota});
              } else {
                resolve({success: true, message: "Not Found " + filePathNota});
              }
            });
          }
          // DOcC NOTA BAR
          const filePathNotaBar = path.join(__dirname, '../../../portal-endpoint/.uploads/nota_debit/bar_recon/' + infoFPJP.bar_attachment);
          if (infoFPJP.bar_attachment != null && infoFPJP.bar_attachment != "" && infoFPJP.bar_attachment != undefined) {
            await new Promise((resolve, reject) => {
              if (fs.existsSync(filePathNotaBar)) {
                MergePDFFile.push(filePathNotaBar);
                // merger.add(filePathNotaBar);
                resolve({success: true, message: filePathNotaBar});
              } else {
                resolve({success: true, message: "Not Found " + filePathNotaBar});
              }
            });
          }

          //DOC BUKPOT
          const filePathNotaBukpot = path.join(__dirname, '../../../portal-endpoint/.uploads/nota_debit/bukpot/' + infoFPJP.wht_attachment);
          if (infoFPJP.wht_attachment != null && infoFPJP.wht_attachment != "" && infoFPJP.wht_attachment != undefined) {
            await new Promise((resolve, reject) => {
              if (fs.existsSync(filePathNotaBukpot)) {
                MergePDFFile.push(filePathNotaBukpot);
                // merger.add(filePathNotaBukpot);
                resolve({success: true, message: filePathNotaBukpot});
              } else {
                resolve({success: true, message: "Not Found " + filePathNotaBukpot});
              }
            });
          }

          //DOC BUKTI BAYAR
          const filePathNotaBB = path.join(__dirname, '../../../portal-endpoint/.uploads/nota_debit/bukti_bayar/' + infoFPJP.bb_attachment);
          if (infoFPJP.bb_attachment != null && infoFPJP.bb_attachment != "" && infoFPJP.bb_attachment != undefined) {
            await new Promise((resolve, reject) => {
              if (fs.existsSync(filePathNotaBB)) {
                MergePDFFile.push(filePathNotaBB);
                // merger.add(filePathNotaBB);
                resolve({success: true, message: filePathNotaBB});
              } else {
                resolve({success: true, message: "Not Found " + filePathNotaBB});
              }
            });
          }

          //DOC SUMMARY NOTA
          const filePathSummaryNota = path.join(__dirname, '../../../portal-endpoint/.uploads/nota_debit/summary/' + infoFPJP.summary_attachment);
          if (infoFPJP.summary_attachment != null && infoFPJP.summary_attachment != "" && infoFPJP.summary_attachment != undefined) {
            await new Promise((resolve, reject) => {
              if (fs.existsSync(filePathSummaryNota)) {
                MergePDFFile.push(filePathSummaryNota);
                // merger.add(filePathSummaryNota);
                resolve({success: true, message: filePathSummaryNota});
              } else {
                resolve({success: true, message: "Not Found " + filePathSummaryNota});
              }
            });
          }

        }


        if (infoFPJP.type_trx == 1) {
          //DOC INVOICE
          const filePathInv = path.join(__dirname, '../../../portal-endpoint/.uploads/invoice/paid/' + infoFPJP.inv_attahment);
          if (infoFPJP.inv_attahment != null && infoFPJP.inv_attahment != "" && infoFPJP.inv_attahment != undefined) {
            await new Promise((resolve, reject) => {
              if (fs.existsSync(filePathInv)) {
                MergePDFFile.push(filePathInv);
                // merger.add(filePathInv);
                resolve({success: true, message: filePathInv});
              } else {
                resolve({success: true, message: "Not Found " + filePathInv});
              }
            });
          }

          //DOC BAR INVOICE
          const filePathInvBar = path.join(__dirname, '../../../portal-endpoint/.uploads/invoice/bar_recon/' + infoFPJP.bar_attachment);
          if (infoFPJP.bar_attachment != null && infoFPJP.bar_attachment != "" && infoFPJP.bar_attachment != undefined) {
            await new Promise((resolve, reject) => {
              if (fs.existsSync(filePathInvBar)) {
                MergePDFFile.push(filePathInvBar);
                // merger.add(filePathInvBar);
                resolve({success: true, message: filePathInvBar});
              } else {
                resolve({success: true, message: "Not Found " + filePathInvBar});
              }
            });
          }

          //DOC FAKTUR
          const filePathInvFP = path.join(__dirname, '../../../portal-endpoint/.uploads/invoice/faktur_pajak/' + infoFPJP.fp_attachment);
          if (infoFPJP.fp_attachment != null && infoFPJP.fp_attachment != "" && infoFPJP.fp_attachment != undefined) {
            await new Promise((resolve, reject) => {
              if (fs.existsSync(filePathInvFP)) {
                MergePDFFile.push(filePathInvFP);
                // merger.add(filePathInvFP);
                resolve({success: true, message: filePathInvFP});
              } else {
                resolve({success: true, message: "Not Found " + filePathInvFP});
              }
            });
          }

          //DOC SUMMARY INVOICE
          const filePathSummaryInvoice = path.join(__dirname, '../../../portal-endpoint/.uploads/invoice/summary/' + infoFPJP.summary_attachment);
          if (infoFPJP.summary_attachment != null && infoFPJP.summary_attachment != "" && infoFPJP.summary_attachment != undefined) {
            await new Promise((resolve, reject) => {
              if (fs.existsSync(filePathSummaryInvoice)) {
                MergePDFFile.push(filePathSummaryInvoice);
                // merger.add(filePathSummaryInvoice);
                resolve({success: true, message: filePathSummaryInvoice});
              } else {
                resolve({success: true, message: "Not Found " + filePathSummaryInvoice});
              }
            });
          }
        }

        var infoAd = await this.adMasterRepository.findById(infoFPJP.ad_code);
        const filePathPKS = path.join(__dirname, '../../.digipost/profile_npwp/' + infoAd.pks_attachment);
        if (infoAd.pks_attachment != null && infoAd.pks_attachment != "" && infoAd.pks_attachment != undefined) {
          await new Promise((resolve, reject) => {
            if (fs.existsSync(filePathPKS)) {
              MergePDFFile.push(filePathPKS);
              // merger.add(filePathPKS);
              resolve({success: true, message: filePathPKS});
            } else {
              resolve({success: true, message: "Not Found " + filePathPKS});
            }
          });
        }

        const filePathNPWP = path.join(__dirname, '../../.digipost/profile_npwp/' + infoAd.npwp_attachment);
        if (infoAd.npwp_attachment != null && infoAd.npwp_attachment != "" && infoAd.npwp_attachment != undefined) {
          await new Promise((resolve, reject) => {
            if (fs.existsSync(filePathNPWP)) {
              MergePDFFile.push(filePathNPWP);
              // merger.add(filePathNPWP);
              resolve({success: true, message: filePathNPWP});
            } else {
              resolve({success: true, message: "Not Found " + filePathNPWP});
            }
          });
        }

        if (infoFPJP.type_trx == 2) {
          var infoDigipos = await this.dataDigiposRepository.findOne({where: {and: [{ad_code: infoFPJP.ad_code}, {trx_date_group: infoFPJP.periode}]}});
          if (infoDigipos != null) {
            const filePathPaidInvoice = path.join(__dirname, '../../.digipost/doc-distribute/invoice-sign/' + infoDigipos.attach_invoice_ttd);
            if (infoDigipos.attach_invoice_ttd != "" && infoDigipos.attach_invoice_ttd != undefined && infoDigipos.attach_invoice_ttd != null) {
              await new Promise((resolve, reject) => {
                if (fs.existsSync(filePathPaidInvoice)) {
                  MergePDFFile.push(filePathPaidInvoice);
                  // merger.add(filePathPaidInvoice);
                  resolve({success: true, message: filePathPaidInvoice});
                } else {
                  resolve({success: true, message: "Not Found " + filePathPaidInvoice});
                }
              });
            }
          }
        }

        // await merger.save(path.join(__dirname, '../../.digipost/fpjp/ALL_' + "FPJP_" + infoFPJP.transaction_code + "_ALL.pdf")); //save under given name
        var pathAll = path.join(__dirname, '../../.digipost/fpjp/ALL_' + "FPJP_" + infoFPJP.transaction_code + "_ALL.pdf");
        try {
          mergePDF(MergePDFFile, {output: pathAll})
            .then((buffer: any) => {
              fs.readFile(pathAll, function (errRead, dataALL) {
                if (errRead) {
                  console.log(errRead);
                  resolve(result);
                } else {
                  result = {"name": "DOC_PENDUKUNG", "type": "pdf", "content": dataALL.toString("base64")};
                  resolve(result);
                }
              });
            });
        } catch (error) {
          console.log(error);
          resolve(result);
        }
      } else {
        console.log("Fpjp Document Not Exities !");
        resolve(result);
      }
    });
  }

}
