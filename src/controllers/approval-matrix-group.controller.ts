import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where
} from '@loopback/repository';
import {
  del, get,
  getModelSchemaRef, param,


  patch, post,




  put,

  requestBody
} from '@loopback/rest';
import {ApprovalMatrixGroup} from '../models';
import {ApprovalMatrixGroupRepository} from '../repositories';

export class ApprovalMatrixGroupController {
  constructor(
    @repository(ApprovalMatrixGroupRepository)
    public approvalMatrixGroupRepository: ApprovalMatrixGroupRepository,
  ) { }

  @post('/approval-matrix-group', {
    responses: {
      '200': {
        description: 'ApprovalMatrixGroup model instance',
        content: {'application/json': {schema: getModelSchemaRef(ApprovalMatrixGroup)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ApprovalMatrixGroup, {
            title: 'NewApprovalMatrixGroup',
            exclude: ['amg_id'],
          }),
        },
      },
    })
    approvalMatrixGroup: Omit<ApprovalMatrixGroup, 'amg_id'>,
  ): Promise<ApprovalMatrixGroup> {
    return this.approvalMatrixGroupRepository.create(approvalMatrixGroup);
  }

  @get('/approval-matrix-group/count', {
    responses: {
      '200': {
        description: 'ApprovalMatrixGroup model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(ApprovalMatrixGroup) where?: Where<ApprovalMatrixGroup>,
  ): Promise<Count> {
    return this.approvalMatrixGroupRepository.count(where);
  }

  @get('/approval-matrix-group', {
    responses: {
      '200': {
        description: 'Array of ApprovalMatrixGroup model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(ApprovalMatrixGroup, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(ApprovalMatrixGroup) filter?: Filter<ApprovalMatrixGroup>,
  ): Promise<ApprovalMatrixGroup[]> {
    return this.approvalMatrixGroupRepository.find(filter);
  }

  @patch('/approval-matrix-group', {
    responses: {
      '200': {
        description: 'ApprovalMatrixGroup PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ApprovalMatrixGroup, {partial: true}),
        },
      },
    })
    approvalMatrixGroup: ApprovalMatrixGroup,
    @param.where(ApprovalMatrixGroup) where?: Where<ApprovalMatrixGroup>,
  ): Promise<Count> {
    return this.approvalMatrixGroupRepository.updateAll(approvalMatrixGroup, where);
  }

  @get('/approval-matrix-group/{id}', {
    responses: {
      '200': {
        description: 'ApprovalMatrixGroup model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(ApprovalMatrixGroup, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(ApprovalMatrixGroup, {exclude: 'where'}) filter?: FilterExcludingWhere<ApprovalMatrixGroup>
  ): Promise<ApprovalMatrixGroup> {
    return this.approvalMatrixGroupRepository.findById(id, filter);
  }

  @patch('/approval-matrix-group/{id}', {
    responses: {
      '204': {
        description: 'ApprovalMatrixGroup PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ApprovalMatrixGroup, {partial: true}),
        },
      },
    })
    approvalMatrixGroup: ApprovalMatrixGroup,
  ): Promise<void> {
    await this.approvalMatrixGroupRepository.updateById(id, approvalMatrixGroup);
  }

  @put('/approval-matrix-group/{id}', {
    responses: {
      '204': {
        description: 'ApprovalMatrixGroup PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() approvalMatrixGroup: ApprovalMatrixGroup,
  ): Promise<void> {
    await this.approvalMatrixGroupRepository.replaceById(id, approvalMatrixGroup);
  }

  @del('/approval-matrix-group/{id}', {
    responses: {
      '204': {
        description: 'ApprovalMatrixGroup DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.approvalMatrixGroupRepository.deleteById(id);
  }
}
