import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {DataDigiposDetail} from '../models';
import {DataDigiposDetailRepository} from '../repositories';

export class DataDigiposDetailActionController {
  constructor(
    @repository(DataDigiposDetailRepository)
    public dataDigiposDetailRepository : DataDigiposDetailRepository,
  ) {}

  @post('/data-digipos-details', {
    responses: {
      '200': {
        description: 'DataDigiposDetail model instance',
        content: {'application/json': {schema: getModelSchemaRef(DataDigiposDetail)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(DataDigiposDetail, {
            title: 'NewDataDigiposDetail',
            exclude: ['digipos_detail_id'],
          }),
        },
      },
    })
    dataDigiposDetail: Omit<DataDigiposDetail, 'digipos_detail_id'>,
  ): Promise<DataDigiposDetail> {
    return this.dataDigiposDetailRepository.create(dataDigiposDetail);
  }

  @get('/data-digipos-details/count', {
    responses: {
      '200': {
        description: 'DataDigiposDetail model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(DataDigiposDetail) where?: Where<DataDigiposDetail>,
  ): Promise<Count> {
    return this.dataDigiposDetailRepository.count(where);
  }

  @get('/data-digipos-details', {
    responses: {
      '200': {
        description: 'Array of DataDigiposDetail model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(DataDigiposDetail, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(DataDigiposDetail) filter?: Filter<DataDigiposDetail>,
  ): Promise<DataDigiposDetail[]> {
    return this.dataDigiposDetailRepository.find(filter);
  }

  @patch('/data-digipos-details', {
    responses: {
      '200': {
        description: 'DataDigiposDetail PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(DataDigiposDetail, {partial: true}),
        },
      },
    })
    dataDigiposDetail: DataDigiposDetail,
    @param.where(DataDigiposDetail) where?: Where<DataDigiposDetail>,
  ): Promise<Count> {
    return this.dataDigiposDetailRepository.updateAll(dataDigiposDetail, where);
  }

  @get('/data-digipos-details/{id}', {
    responses: {
      '200': {
        description: 'DataDigiposDetail model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(DataDigiposDetail, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(DataDigiposDetail, {exclude: 'where'}) filter?: FilterExcludingWhere<DataDigiposDetail>
  ): Promise<DataDigiposDetail> {
    return this.dataDigiposDetailRepository.findById(id, filter);
  }

  @patch('/data-digipos-details/{id}', {
    responses: {
      '204': {
        description: 'DataDigiposDetail PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(DataDigiposDetail, {partial: true}),
        },
      },
    })
    dataDigiposDetail: DataDigiposDetail,
  ): Promise<void> {
    await this.dataDigiposDetailRepository.updateById(id, dataDigiposDetail);
  }

  @put('/data-digipos-details/{id}', {
    responses: {
      '204': {
        description: 'DataDigiposDetail PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() dataDigiposDetail: DataDigiposDetail,
  ): Promise<void> {
    await this.dataDigiposDetailRepository.replaceById(id, dataDigiposDetail);
  }

  @del('/data-digipos-details/{id}', {
    responses: {
      '204': {
        description: 'DataDigiposDetail DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.dataDigiposDetailRepository.deleteById(id);
  }
}
