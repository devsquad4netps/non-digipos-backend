import {authenticate} from '@loopback/authentication';
import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where
} from '@loopback/repository';
import {
  del, get,
  getModelSchemaRef, param,


  patch, post,




  put,

  requestBody
} from '@loopback/rest';
import {DataDisputeDetail} from '../models';
import {DataDisputeDetailRepository} from '../repositories';


@authenticate('jwt')
export class DataDisputeDetailActionController {
  constructor(
    @repository(DataDisputeDetailRepository)
    public dataDisputeDetailRepository: DataDisputeDetailRepository,
  ) { }

  @post('/data-dispute-details', {
    responses: {
      '200': {
        description: 'DataDisputeDetail model instance',
        content: {'application/json': {schema: getModelSchemaRef(DataDisputeDetail)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(DataDisputeDetail, {
            title: 'NewDataDisputeDetail',
            exclude: ['dispute_det_id'],
          }),
        },
      },
    })
    dataDisputeDetail: Omit<DataDisputeDetail, 'dispute_det_id'>,
  ): Promise<DataDisputeDetail> {
    return this.dataDisputeDetailRepository.create(dataDisputeDetail);
  }

  @get('/data-dispute-details/count', {
    responses: {
      '200': {
        description: 'DataDisputeDetail model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(DataDisputeDetail) where?: Where<DataDisputeDetail>,
  ): Promise<Count> {
    return this.dataDisputeDetailRepository.count(where);
  }

  @get('/data-dispute-details', {
    responses: {
      '200': {
        description: 'Array of DataDisputeDetail model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(DataDisputeDetail, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(DataDisputeDetail) filter?: Filter<DataDisputeDetail>,
  ): Promise<DataDisputeDetail[]> {
    return this.dataDisputeDetailRepository.find(filter);
  }

  @patch('/data-dispute-details', {
    responses: {
      '200': {
        description: 'DataDisputeDetail PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(DataDisputeDetail, {partial: true}),
        },
      },
    })
    dataDisputeDetail: DataDisputeDetail,
    @param.where(DataDisputeDetail) where?: Where<DataDisputeDetail>,
  ): Promise<Count> {
    return this.dataDisputeDetailRepository.updateAll(dataDisputeDetail, where);
  }

  @get('/data-dispute-details/{id}', {
    responses: {
      '200': {
        description: 'DataDisputeDetail model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(DataDisputeDetail, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(DataDisputeDetail, {exclude: 'where'}) filter?: FilterExcludingWhere<DataDisputeDetail>
  ): Promise<DataDisputeDetail> {
    return this.dataDisputeDetailRepository.findById(id, filter);
  }

  @patch('/data-dispute-details/{id}', {
    responses: {
      '204': {
        description: 'DataDisputeDetail PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(DataDisputeDetail, {partial: true}),
        },
      },
    })
    dataDisputeDetail: DataDisputeDetail,
  ): Promise<void> {
    await this.dataDisputeDetailRepository.updateById(id, dataDisputeDetail);
  }

  @put('/data-dispute-details/{id}', {
    responses: {
      '204': {
        description: 'DataDisputeDetail PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() dataDisputeDetail: DataDisputeDetail,
  ): Promise<void> {
    await this.dataDisputeDetailRepository.replaceById(id, dataDisputeDetail);
  }

  @del('/data-dispute-details/{id}', {
    responses: {
      '204': {
        description: 'DataDisputeDetail DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.dataDisputeDetailRepository.deleteById(id);
  }
}
