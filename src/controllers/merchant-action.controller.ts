// Uncomment these imports to begin using these cool features!

import {authenticate} from '@loopback/authentication';
import {inject} from '@loopback/core';
import {Filter, FilterExcludingWhere, repository} from '@loopback/repository';
import {get, getModelSchemaRef, param, post, Request, requestBody, Response, ResponseObject, RestBindings} from '@loopback/rest';
import {SecurityBindings, securityId, UserProfile} from '@loopback/security';
import fs from 'fs';
import multer from 'multer';
import path from 'path';
import {Merchant, MerchantClass, MerchantPic, MerchantRekening} from '../models';
import {MerchantClassRepository, MerchantPicRepository, MerchantProductRepository, MerchantRekeningRepository} from '../repositories';
import {MerchantRepository} from '../repositories/merchant.repository';
import {UserRepository} from '../repositories/user.repository';
import {ControllerConfig} from './controller.config';


export const RekeningAktifRequestBody: ResponseObject = {
  description: 'The input of REKENING AKTIF function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ["AT_FLAG"],
        properties: {
          AT_FLAG: {
            type: 'number',
          },
        }
      }
    },
  },
};

@authenticate('jwt')
export class MerchantActionController {

  constructor(
    @repository(MerchantRepository)
    public merchantRepository: MerchantRepository,
    @repository(MerchantProductRepository)
    public merchantProductRepository: MerchantProductRepository,
    @repository(MerchantClassRepository)
    public merchantClassRepository: MerchantClassRepository,
    @repository(MerchantRekeningRepository)
    public merchantRekeningRepository: MerchantRekeningRepository,
    @repository(MerchantPicRepository)
    public merchantPicRepository: MerchantPicRepository,
    @inject(SecurityBindings.USER)
    public currentUserProfile: UserProfile,
    @repository(UserRepository)
    public userRepository: UserRepository,
  ) { }

  //GET ALL MERCHANT DATA
  @get('/merchant-action/views', {
    responses: {
      '200': {
        description: 'Array of Merchant model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Merchant, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async findMerchant(
    @param.filter(Merchant) filter?: Filter<Merchant>,
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: [], count: 0};
    var MerchantStore: any; MerchantStore = [];
    var MerchantInfo = await this.merchantRepository.find(filter);

    var COUNT_MERCHANT = {}
    if (filter?.where != undefined) {
      COUNT_MERCHANT = await this.merchantRepository.count(filter?.where);
    } else {
      COUNT_MERCHANT = await this.merchantRepository.count();
    }

    var MerchanClassData = await this.merchantClassRepository.find();
    var MERCHANT_CLASS: any; MERCHANT_CLASS = {};
    for (var i in MerchanClassData) {
      var DATA_MERCHANT_CLASS = MerchanClassData[i];
      var ID = DATA_MERCHANT_CLASS.MC_ID == undefined ? -1 : DATA_MERCHANT_CLASS.MC_ID;
      MERCHANT_CLASS[ID] = {
        MC_ID: ID,
        MC_LABEL: DATA_MERCHANT_CLASS.MC_LABEL
      }
    }

    for (var i in MerchantInfo) {
      var infoMerchant = MerchantInfo[i];
      var CLASS_MC = infoMerchant.M_CLASS == undefined ? -1 : infoMerchant.M_CLASS
      var RAW = {
        M_ID: infoMerchant.M_ID,
        M_NO_PERFIX: infoMerchant.M_NO_PERFIX,
        M_CODE: infoMerchant.M_CODE,
        M_NAME: infoMerchant.M_NAME,
        M_LEGAL_NAME: infoMerchant.M_LEGAL_NAME,
        M_CLASS: CLASS_MC == -1 ? null : (MERCHANT_CLASS[CLASS_MC] == undefined ? null : MERCHANT_CLASS[CLASS_MC]),
        M_PIC_NAME: infoMerchant.M_PIC_NAME,
        M_PIC_EMAIL: infoMerchant.M_PIC_EMAIL,
        M_PIC_PHONE: infoMerchant.M_PIC_PHONE,
        M_PIC_JABATAN: infoMerchant.M_PIC_JABATAN,

        PT_REKENING_NAME: infoMerchant.PT_REKENING_NAME,
        PT_ACOUNT_NUMBER: infoMerchant.PT_ACOUNT_NUMBER,
        PT_BANK_NAME: infoMerchant.PT_BANK_NAME,
        PT_BANK_BRANCH: infoMerchant.PT_BANK_BRANCH,

        M_ALAMAT_KANTOR: infoMerchant.M_ALAMAT_KANTOR,
        M_NPWP: infoMerchant.M_NPWP,
        M_ALAMAT_NPWP: infoMerchant.M_ALAMAT_NPWP,
        M_NPWP_ATTACH: infoMerchant.M_NPWP_ATTACH,
        M_NO_CONTRACT: infoMerchant.M_NO_CONTRACT,
        M_PKS_ATTACH: infoMerchant.M_PKS_ATTACH,
        M_TGL_GABUNG: infoMerchant.M_TGL_GABUNG,
        M_CITY: infoMerchant.M_CITY,
        AT_FLAG: infoMerchant.AT_FLAG
      };
      MerchantStore.push(RAW);
    }
    result.datas = MerchantStore;
    result.count = COUNT_MERCHANT;

    return result;
  }

  //GET  MERCHANT DATA BY ID
  @get('/merchant-action/view/{id}', {
    responses: {
      '200': {
        description: 'non digipos variable type model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Merchant, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findByIdMerchant(
    @param.path.number('id') id: number,
    @param.filter(Merchant, {exclude: 'where'}) filter?: FilterExcludingWhere<Merchant>
  ): Promise<Object> {
    var result: any; result = {success: true, message: '', datas: []};
    var infoMerchant = await this.merchantRepository.findOne({where: {and: [{M_ID: id}]}}, filter);
    if (infoMerchant != null) {
      var RAW = {
        M_ID: infoMerchant.M_ID,
        M_NO_PERFIX: infoMerchant.M_NO_PERFIX,
        M_CODE: infoMerchant.M_CODE,
        M_NAME: infoMerchant.M_NAME,
        M_LEGAL_NAME: infoMerchant.M_LEGAL_NAME,
        M_PIC_NAME: infoMerchant.M_PIC_NAME,
        M_PIC_EMAIL: infoMerchant.M_PIC_EMAIL,
        M_PIC_PHONE: infoMerchant.M_PIC_PHONE,
        M_PIC_JABATAN: infoMerchant.M_PIC_JABATAN,
        PT_REKENING_NAME: infoMerchant.PT_REKENING_NAME,
        PT_ACOUNT_NUMBER: infoMerchant.PT_ACOUNT_NUMBER,
        PT_BANK_NAME: infoMerchant.PT_BANK_NAME,
        PT_BANK_BRANCH: infoMerchant.PT_BANK_BRANCH,
        M_ALAMAT_KANTOR: infoMerchant.M_ALAMAT_KANTOR,
        M_NPWP: infoMerchant.M_NPWP,
        M_ALAMAT_NPWP: infoMerchant.M_ALAMAT_NPWP,
        M_NPWP_ATTACH: infoMerchant.M_NPWP_ATTACH,
        M_NO_CONTRACT: infoMerchant.M_NO_CONTRACT,
        M_PKS_ATTACH: infoMerchant.M_PKS_ATTACH,
        M_TGL_GABUNG: infoMerchant.M_TGL_GABUNG,
        M_CITY: infoMerchant.M_CITY,
        AT_FLAG: infoMerchant.AT_FLAG,
        M_CLASS: await this.merchantClassRepository.findOne({where: {and: [{MC_ID: infoMerchant.M_CLASS}]}, fields: {MC_LABEL: true, MC_ID: true}})
      };
      result.datas = RAW;
    } else {
      result.success = false;
      result.message = "Merchant not found  !";
    }
    return result;
  }


  //CREATE MERCHANT STORE
  @post('/merchant-action/create', {
    responses: {
      '200': {
        description: 'merchant create model instance',
        content: {'application/json': {schema: getModelSchemaRef(Merchant)}},
      },
    },
  })
  async createMerchant(
    @requestBody.file()
    requestFile: Request,
    @inject(RestBindings.Http.RESPONSE) response: Response,
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    return new Promise<object>(async (resolve, reject) => {

      //SET STORAGE DAN FILE NAME UPLOAD
      const STORAGE_CONFIG = multer.diskStorage({
        destination: path.join(__dirname, './../../.non-digipos/merchant/'),
        filename: (req, file, cb) => {
          cb(null, Date.now() + "-" + file.originalname);
        },
      });

      //CONFIG UPLOAD NAME FIELD
      const UPLOAD_ACTION = multer({
        storage: STORAGE_CONFIG,
        fileFilter: async function (req, file, callback) {
          var ext = path.extname(file.originalname).toLowerCase();
          if (ext !== '.pdf') {
            return callback(new Error('Only pdf are allowed file upload !'));
          }
          callback(null, true);
        }
      }).fields([{name: 'M_NPWP_ATTACH', maxCount: 1}, {name: 'M_PKS_ATTACH', maxCount: 1}]);

      UPLOAD_ACTION(requestFile, response, async (errUploadFile: any) => {
        if (errUploadFile) {
          resolve({success: false, message: errUploadFile, datas: []});
        } else {
          var result: any; result = {success: true, message: '', datas: []};

          //SET FIELD UPLOADED
          var BODY_PARAM: any; BODY_PARAM = {};
          for (var i in requestFile.body) {
            BODY_PARAM[i] = requestFile.body[i];
          }
          BODY_PARAM["M_NPWP_ATTACH"] = ControllerConfig.onCurrentTime().toString() + "_NPWP" + ".pdf";
          BODY_PARAM["M_PKS_ATTACH"] = ControllerConfig.onCurrentTime().toString() + "_PKS" + ".pdf";


          var FILE_UPLOAD: any; FILE_UPLOAD = requestFile.files;
          var NPWP_ATTACH: any; NPWP_ATTACH = "";
          var PKS_ATTACH: any; PKS_ATTACH = "";
          for (var iFile in FILE_UPLOAD) {
            var InfiFile = FILE_UPLOAD[iFile];
            for (var iFilePath in InfiFile) {
              if (InfiFile[iFilePath]["fieldname"] == 'M_NPWP_ATTACH') {
                NPWP_ATTACH = InfiFile[iFilePath];
              }
              if (InfiFile[iFilePath]["fieldname"] == 'M_PKS_ATTACH') {
                PKS_ATTACH = InfiFile[iFilePath];
              }
            }
          }

          var IS_UPLOADED = true;
          if (NPWP_ATTACH == "") {
            IS_UPLOADED = false;
            resolve({success: false, message: "Create merchant error, please upload npwp !", datas: []});
          }

          if (PKS_ATTACH == "") {
            IS_UPLOADED = false;
            resolve({success: false, message: "Create merchant error, please upload pks !", datas: []});
          }

          if (NPWP_ATTACH == "" || PKS_ATTACH == "") {
            if (NPWP_ATTACH != "") {
              try {
                fs.unlinkSync(NPWP_ATTACH.path);
              } catch (err) { }
            }
            if (PKS_ATTACH != "") {
              try {
                fs.unlinkSync(PKS_ATTACH.path);
              } catch (err) { }
            }
          } else {

            try {
              fs.rename(NPWP_ATTACH.path, NPWP_ATTACH.destination + "/" + BODY_PARAM["M_NPWP_ATTACH"], function (err) {
                if (err) {
                  IS_UPLOADED = false;
                  resolve({success: false, message: err, datas: []});
                }
              });
            } catch (error) {
              IS_UPLOADED = false;
              resolve({success: false, message: error, datas: []});
              try {
                fs.unlinkSync(NPWP_ATTACH.path);
              } catch (err) { }
            }

            try {
              fs.rename(PKS_ATTACH.path, PKS_ATTACH.destination + "/" + BODY_PARAM["M_PKS_ATTACH"], function (err) {
                if (err) {
                  IS_UPLOADED = false;
                  resolve({success: false, message: err, datas: []});
                }
              });
            } catch (error) {
              IS_UPLOADED = false;
              resolve({success: false, message: error, datas: []});
              try {
                fs.unlinkSync(PKS_ATTACH.path);
              } catch (err) { }
            }
          }

          if (IS_UPLOADED) {
            try {

              var AVALIABLE_CODE: boolean; AVALIABLE_CODE = false;
              var COUNT_MERCHANT = await this.merchantRepository.count();
              var NO = COUNT_MERCHANT.count + 1;
              var M_CODE = "";
              do {
                var MERCHANT_CODE = "0000000".substring(NO.toString().length) + NO;
                var VALID_CODE = await this.merchantRepository.findOne({where: {and: [{M_CODE: MERCHANT_CODE}]}});
                if (VALID_CODE == null) {
                  M_CODE = MERCHANT_CODE;
                  AVALIABLE_CODE = true;
                }
                NO++;
              }
              while (AVALIABLE_CODE == false);

              BODY_PARAM["M_CODE"] = M_CODE;
              BODY_PARAM["AT_CREATE"] = ControllerConfig.onCurrentTime().toString();
              BODY_PARAM["AT_UPDATE"] = undefined;
              BODY_PARAM["AT_FLAG"] = BODY_PARAM["AT_FLAG"] == null || BODY_PARAM["AT_FLAG"] == undefined ? 0 : BODY_PARAM["AT_FLAG"];
              BODY_PARAM["AT_WHO"] = USER_ID;

              var MERCHANT_INFO = await this.merchantRepository.create(BODY_PARAM);
              resolve({success: true, message: 'success create !', datas: MERCHANT_INFO});

            } catch (error) {
              try {
                fs.unlinkSync(NPWP_ATTACH.destination + "/" + BODY_PARAM["M_NPWP_ATTACH"]);
              } catch (err) { }
              try {
                fs.unlinkSync(PKS_ATTACH.destination + "/" + BODY_PARAM["M_PKS_ATTACH"]);
              } catch (err) { }
              resolve({success: false, message: error, datas: []});
            }
          }
        }
      });
    });
  }

  //UPDATE MERCHANT STORE
  @post('/merchant-action/update/{id}', {
    responses: {
      '200': {
        description: 'merchant create model instance',
        content: {'application/json': {schema: getModelSchemaRef(Merchant)}},
      },
    },
  })
  async updateMerchant(
    @param.path.number('id') id: number,
    @requestBody.file()
    requestFile: Request,
    @inject(RestBindings.Http.RESPONSE) response: Response,
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    return new Promise<object>(async (resolve, reject) => {


      var HAS_MERCHANT = await this.merchantRepository.findOne({where: {and: [{M_ID: id}]}});
      if (HAS_MERCHANT != null) {

        //SET STORAGE DAN FILE NAME UPLOAD
        const STORAGE_CONFIG = multer.diskStorage({
          destination: path.join(__dirname, './../../.non-digipos/merchant/'),
          filename: (req, file, cb) => {
            cb(null, Date.now() + "-" + file.originalname);
          },
        });

        //CONFIG UPLOAD NAME FIELD
        const UPLOAD_ACTION = multer({
          storage: STORAGE_CONFIG,
          fileFilter: async function (req, file, callback) {
            var ext = path.extname(file.originalname).toLowerCase();
            if (ext !== '.pdf') {
              return callback(new Error('Only pdf are allowed file upload !'));
            }
            callback(null, true);
          }
        }).fields([{name: 'M_NPWP_ATTACH', maxCount: 1}, {name: 'M_PKS_ATTACH', maxCount: 1}]);

        UPLOAD_ACTION(requestFile, response, async (errUploadFile: any) => {
          if (errUploadFile) {
            resolve({success: false, message: errUploadFile, datas: []});
          } else {
            var result: any; result = {success: true, message: '', datas: []};

            //SET FIELD UPLOADED
            var BODY_PARAM: any; BODY_PARAM = {};
            var DELETE_ARR = ["M_ID", "M_CODE", "AT_CREATE"];
            for (var i in requestFile.body) {
              BODY_PARAM[i] = requestFile.body[i];
              if (DELETE_ARR.indexOf(i) != -1) {
                delete BODY_PARAM[i];
              }
            }


            var FILE_UPLOAD: any; FILE_UPLOAD = requestFile.files;
            var NPWP_ATTACH: any; NPWP_ATTACH = "";
            var PKS_ATTACH: any; PKS_ATTACH = "";
            for (var iFile in FILE_UPLOAD) {
              var InfiFile = FILE_UPLOAD[iFile];
              for (var iFilePath in InfiFile) {
                if (InfiFile[iFilePath]["fieldname"] == 'M_NPWP_ATTACH') {
                  NPWP_ATTACH = InfiFile[iFilePath];
                }
                if (InfiFile[iFilePath]["fieldname"] == 'M_PKS_ATTACH') {
                  PKS_ATTACH = InfiFile[iFilePath];
                }
              }
            }

            BODY_PARAM["M_NPWP_ATTACH"] = NPWP_ATTACH == "" ? HAS_MERCHANT?.M_NPWP_ATTACH : ControllerConfig.onCurrentTime().toString() + "_NPWP" + ".pdf";
            BODY_PARAM["M_PKS_ATTACH"] = PKS_ATTACH == "" ? HAS_MERCHANT?.M_PKS_ATTACH : ControllerConfig.onCurrentTime().toString() + "_PKS" + ".pdf";



            var IS_UPLOADED = true;
            if (NPWP_ATTACH == "" || PKS_ATTACH == "") {
              if (NPWP_ATTACH != "") {
                try {
                  fs.unlinkSync(NPWP_ATTACH.path);
                } catch (err) { }
              }
              if (PKS_ATTACH != "") {
                try {
                  fs.unlinkSync(PKS_ATTACH.path);
                } catch (err) { }
              }
            } else {
              try {
                fs.rename(NPWP_ATTACH.path, NPWP_ATTACH.destination + "/" + BODY_PARAM["M_NPWP_ATTACH"], function (err) {
                  if (err) {
                    IS_UPLOADED = false;
                    resolve({success: false, message: err, datas: []});
                  }
                });
              } catch (error) {
                IS_UPLOADED = false;
                resolve({success: false, message: error, datas: []});
                try {
                  fs.unlinkSync(NPWP_ATTACH.path);
                } catch (err) { }
              }

              try {
                fs.rename(PKS_ATTACH.path, PKS_ATTACH.destination + "/" + BODY_PARAM["M_PKS_ATTACH"], function (err) {
                  if (err) {
                    IS_UPLOADED = false;
                    resolve({success: false, message: err, datas: []});
                  }
                });
              } catch (error) {
                IS_UPLOADED = false;
                resolve({success: false, message: error, datas: []});
                try {
                  fs.unlinkSync(PKS_ATTACH.path);
                } catch (err) { }
              }
            }


            if (IS_UPLOADED) {
              try {

                var AVALIABLE_CODE: boolean; AVALIABLE_CODE = false;
                var COUNT_MERCHANT = await this.merchantRepository.count();
                var NO = COUNT_MERCHANT.count + 1;
                var M_CODE = "";
                do {
                  var MERCHANT_CODE = "0000000".substring(NO.toString().length) + NO;
                  var VALID_CODE = await this.merchantRepository.findOne({where: {and: [{M_CODE: MERCHANT_CODE}]}});
                  if (VALID_CODE == null) {
                    M_CODE = MERCHANT_CODE;
                    AVALIABLE_CODE = true;
                  }
                  NO++;
                }
                while (AVALIABLE_CODE == false);

                BODY_PARAM["AT_UPDATE"] = ControllerConfig.onCurrentTime().toString();
                BODY_PARAM["AT_FLAG"] = BODY_PARAM["AT_FLAG"] == null || BODY_PARAM["AT_FLAG"] == undefined ? 0 : BODY_PARAM["AT_FLAG"];
                BODY_PARAM["AT_WHO"] = USER_ID;

                var MERCHANT_INFO = await this.merchantRepository.updateById(id, BODY_PARAM);
                resolve({success: true, message: "success update merchant !", datas: []});

              } catch (error) {
                try {
                  fs.unlinkSync(NPWP_ATTACH.path);
                } catch (err) { }
                try {
                  fs.unlinkSync(PKS_ATTACH.path);
                } catch (err) { }
                resolve({success: false, message: error, datas: []});
              }
            }
          }
        });
      } else {
        resolve({success: false, message: 'merchant no found !', datas: []});
      }

    });
  }


  //DELETE MERCHANT
  @post('/merchant-action/delete/{id}', {
    responses: {
      '200': {
        description: 'merchant delete model instance',
        content: {'application/json': {schema: getModelSchemaRef(Merchant)}},
      },
    },
  })
  async deleteMerchant(
    @param.path.number('id') id: number,
  ): Promise<Object> {
    var result: any; result = {success: true, message: '', datas: []};

    var HAS_MERCHANT = await this.merchantRepository.findOne({where: {and: [{M_ID: id}]}});
    if (HAS_MERCHANT != null) {

      var MERCHANT_PRODUCT = await this.merchantProductRepository.count({M_ID: id});
      if (MERCHANT_PRODUCT.count == 0) {

        try {
          var PATH_NPWP = path.join(__dirname, './../../.non-digipos/merchant/' + HAS_MERCHANT.M_NPWP_ATTACH);
          var PATH_PKS = path.join(__dirname, './../../.non-digipos/merchant/' + HAS_MERCHANT.M_PKS_ATTACH);

          try {
            fs.unlinkSync(PATH_NPWP);
          } catch (err) { }

          try {
            fs.unlinkSync(PATH_PKS);
          } catch (err) { }

          await this.merchantRepository.deleteById(id);
          result.succes = true;
          result.message = "success delete !";
        } catch (error) {
          result.succes = false;
          result.datas = [];
          result.message = error;
        }
      } else {
        result.succes = false;
        result.datas = [];
        result.message = "Delete error merchant have data product !";
      }
    } else {
      result.succes = false;
      result.datas = [];
      result.message = "Delete error merchant not found !";
    }
    return result;
  }


  //============================================================================MERCHANT CLASS
  @get('/merchant-action/merchant-class/views', {
    responses: {
      '200': {
        description: 'Array of Merchant Class model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(MerchantClass, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async findMerchantClass(
    @param.filter(MerchantClass) filter?: Filter<MerchantClass>,
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};
    var MerchantClassStore: any; MerchantClassStore = [];
    var MerchantClassInfo = await this.merchantClassRepository.find(filter);
    for (var i in MerchantClassInfo) {
      var RAW_MC = MerchantClassInfo[i]
      MerchantClassStore.push({
        MC_ID: RAW_MC.MC_ID,
        MC_LABEL: RAW_MC.MC_LABEL,
        AT_FLAG: RAW_MC.AT_FLAG
      });
    }
    result.message = "Success !";
    result.datas = MerchantClassStore;
    return result;
  }


  //GET  MERCHANT DATA BY ID
  @get('/merchant-action/merchant-class/view/{id}', {
    responses: {
      '200': {
        description: 'Merchant Class type model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(MerchantClass, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findByIdMerchantClass(
    @param.path.number('id') id: number,
    @param.filter(MerchantClass, {exclude: 'where'}) filter?: FilterExcludingWhere<MerchantClass>
  ): Promise<Object> {
    var result: any; result = {success: true, message: '', datas: []};
    var infoMerchantClass = await this.merchantClassRepository.findOne({where: {and: [{MC_ID: id}]}}, filter);
    if (infoMerchantClass != null) {
      var RAW = {
        MC_ID: infoMerchantClass.MC_ID,
        MC_LABEL: infoMerchantClass.MC_LABEL,
        AT_FLAG: infoMerchantClass.AT_FLAG
      };
      result.datas = RAW;
    } else {
      result.success = false;
      result.message = "Merchant Class not found  !";
    }
    return result
  }

  // CREATE MERCHANT CLASS
  @post('/merchant-action/merchant-class/create', {
    responses: {
      '200': {
        description: 'Merchant Class  model instance',
        content: {'application/json': {schema: getModelSchemaRef(MerchantClass)}},
      },
    },
  })
  async createMerchantClass(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(MerchantClass, {
            title: 'MerchantClassCreate',
            exclude: ['MC_ID', 'AT_CREATE', 'AT_UPDATE', 'AT_WHO'],
          }),
        },
      },
    })
    dataMerchantClass: any,
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};


    var BODY_PARAM: any; BODY_PARAM = {};
    for (var i in dataMerchantClass) {
      BODY_PARAM[i] = dataMerchantClass[i];
    }

    BODY_PARAM["AT_CREATE"] = ControllerConfig.onCurrentTime().toString();
    BODY_PARAM["AT_UPDATE"] = undefined;
    BODY_PARAM["AT_WHO"] = USER_ID;
    BODY_PARAM["AT_FLAG"] = BODY_PARAM["AT_FLAG"] == undefined || BODY_PARAM["AT_FLAG"] == null ? 0 : BODY_PARAM["AT_FLAG"];

    try {
      var CREATE_SKEMA = await this.merchantClassRepository.create(BODY_PARAM);
      result.success = true;
      result.message = "Success create !";
      result.data = CREATE_SKEMA;
    } catch (error) {
      result.success = false;
      result.message = error;
    }
    return result;
  }

  // UPDATE MERCHANT CLASS
  @post('/merchant-action/merchant-class/update/{id}', {
    responses: {
      '200': {
        description: 'Merchant Class Update model instance',
        content: {'application/json': {schema: getModelSchemaRef(MerchantClass)}},
      },
    },
  })
  async updateMerchantClass(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(MerchantClass, {
            title: 'UpdateMerchantClass',
            exclude: ['MC_ID', 'AT_CREATE', 'AT_UPDATE', 'AT_WHO'],
          }),
        },
      },
    })
    dataMerchantClass: any,
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};

    var BODY_PARAM: any; BODY_PARAM = {};

    var DELETE_ARR = ["MC_ID", "AT_CREATE", "AT_UPDATE", "AT_WHO"];
    for (var i in dataMerchantClass) {
      if (DELETE_ARR.indexOf(i) != -1) {
        continue;
      }
      BODY_PARAM[i] = dataMerchantClass[i];
    }

    BODY_PARAM["AT_UPDATE"] = ControllerConfig.onCurrentTime().toString();
    BODY_PARAM["AT_WHO"] = USER_ID;

    try {
      await this.merchantClassRepository.updateById(id, BODY_PARAM);
      result.success = true;
      result.message = "Success update merchant class !";
    } catch (error) {
      result.success = false;
      result.message = error;
    }
    return result;
  }

  //DELETE MERCHANT CLASS
  @post('/merchant-action/merchant-class/delete/{id}', {
    responses: {
      '200': {
        description: 'DELETE MERCHANT CLASS model instance',
        content: {'application/json': {schema: getModelSchemaRef(MerchantClass)}},
      },
    },
  })
  async deleteMerchantClass(
    @param.path.number('id') id: number
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};
    var MERCHANT_CLASS = await this.merchantClassRepository.count({MC_ID: id});

    if (MERCHANT_CLASS.count > 0) {
      try {
        await this.merchantClassRepository.deleteById(id);
        result.success = true;
        result.message = "Success delete merchant class !";
      } catch (error) {
        result.succes = false;
        result.message = "Error delete, merchant class not found !";
      }
    } else {
      result.success = false;
      result.message = "Error delete, merchant class not found !";
    }
    return result;
  }

  //REKENING
  @post('/merchant-action/merchant-rekening/{M_ID}/create', {
    responses: {
      '200': {
        description: 'Menuaccess model instance',
        content: {'application/json': {schema: getModelSchemaRef(MerchantRekening)}},
      },
    },
  })
  async createMerchantRekening(
    @param.path.number('M_ID') M_ID: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(MerchantRekening, {
            title: 'NewMerchantRekening',
            exclude: ['MR_ID', 'AT_FLAG', 'AT_CREATE', 'AT_UPDATE', 'AT_WHO', 'M_ID'],
          }),
        },
      },
    })
    REKENING_BODY: Omit<MerchantRekening, 'MR_ID'>,
  ): Promise<MerchantRekening> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};

    var MERCHANT_COUNT = await this.merchantRepository.count({and: [{AT_FLAG: 1}, {M_ID: M_ID}]});

    if (MERCHANT_COUNT.count == 1) {

      REKENING_BODY["AT_FLAG"] = 1;
      REKENING_BODY["AT_CREATE"] = ControllerConfig.onCurrentTime().toString();
      REKENING_BODY["AT_UPDATE"] = undefined;
      REKENING_BODY["AT_WHO"] = USER_ID;
      REKENING_BODY["M_ID"] = M_ID;

      try {
        await this.merchantRekeningRepository.updateAll({AT_FLAG: 0}, {and: [{M_ID: M_ID}]});
        var CREATE_REKENING = await this.merchantRekeningRepository.create(REKENING_BODY);

        try {
          var REKENING = await this.merchantRekeningRepository.findById(CREATE_REKENING.MR_ID);
          await this.merchantRepository.updateById(M_ID, {
            PT_REKENING_NAME: REKENING.MR_REKENING_NAME,
            PT_ACOUNT_NUMBER: REKENING.MR_ACOUNT_NUMBER,
            PT_BANK_NAME: REKENING.MR_BANK_NAME,
            PT_BANK_BRANCH: REKENING.MR_BANK_BRANCH
          });

          result.success = true;
          result.message = "Success !";
          result.datas = CREATE_REKENING;
        } catch (error) {
          result.success = false;
          result.message = error;
        }

      } catch (error) {
        result.success = false;
        result.message = error;
      }
    } else {
      result.success = false;
      result.message = "Error, merchant not found !";
    }

    return result;
  }

  @post('/merchant-action/merchant-rekening/{M_ID}/update/{id}', {
    responses: {
      '200': {
        description: 'Merchant Class Update model instance',
        content: {'application/json': {schema: getModelSchemaRef(MerchantRekening)}},
      },
    },
  })
  async updateMerchantRekening(
    @param.path.number('M_ID') M_ID: number,
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(MerchantRekening, {
            title: 'UpdateMerchantRekening',
            exclude: ['MR_ID', 'AT_FLAG', 'AT_CREATE', 'AT_UPDATE', 'AT_WHO', 'M_ID'],
          }),
        },
      },
    })
    REKENING_BODY: MerchantRekening,
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};


    var MERCHANT_COUNT = await this.merchantRepository.count({and: [{AT_FLAG: 1}, {M_ID: M_ID}]});

    if (MERCHANT_COUNT.count == 1) {

      var REKENING_COUNT = await this.merchantRekeningRepository.findOne({where: {and: [{MR_ID: id}, {M_ID: M_ID}]}});
      if (REKENING_COUNT != null) {

        REKENING_BODY["AT_UPDATE"] = ControllerConfig.onCurrentTime().toString();
        REKENING_BODY["AT_WHO"] = USER_ID;
        REKENING_BODY["M_ID"] = M_ID;

        try {
          await this.merchantRekeningRepository.updateById(id, REKENING_BODY);

          try {
            var RAW_REKENING = await this.merchantRekeningRepository.findById(id);
            if (RAW_REKENING.AT_FLAG == 1) {
              await this.merchantRepository.updateById(M_ID, {
                PT_REKENING_NAME: RAW_REKENING.MR_REKENING_NAME,
                PT_ACOUNT_NUMBER: RAW_REKENING.MR_ACOUNT_NUMBER,
                PT_BANK_NAME: RAW_REKENING.MR_BANK_NAME,
                PT_BANK_BRANCH: RAW_REKENING.MR_BANK_BRANCH
              });
            }

            result.success = true;
            result.message = "Success update merchant rekening !";
          } catch (error) {
            result.success = false;
            result.message = error;
          }

        } catch (error) {
          result.success = false;
          result.message = error;
        }
      } else {
        result.success = false;
        result.message = "Error, merchant rekening not found !";
      }

    } else {
      result.success = false;
      result.message = "Error, merchant not found !";
    }
    return result;
  }

  @get('/merchant-action/merchant-rekening/{M_ID}/views', {
    responses: {
      '200': {
        description: 'Merchant Rekening type model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(MerchantRekening, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findMerchantRekening(
    @param.path.number('M_ID') M_ID: number,
    @param.filter(MerchantRekening) filter?: Filter<MerchantRekening>,
  ): Promise<Object> {
    var result: any; result = {success: true, message: '', datas: []};
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};

    var MERCHANT_COUNT = await this.merchantRepository.count({and: [{AT_FLAG: 1}, {M_ID: M_ID}]});
    if (MERCHANT_COUNT.count == 1) {
      var REKEING = await this.merchantRekeningRepository.find(filter);
      var DATAS: any; DATAS = [];
      for (var i in REKEING) {
        var RAW_REKENING = REKEING[i];
        if (RAW_REKENING.M_ID != M_ID) {
          continue;
        }

        var DATA: any; DATA = {
          MR_ID: RAW_REKENING.MR_ID,
          MR_REKENING_NAME: RAW_REKENING.MR_REKENING_NAME,
          MR_ACOUNT_NUMBER: RAW_REKENING.MR_ACOUNT_NUMBER,
          MR_BANK_NAME: RAW_REKENING.MR_BANK_NAME,
          MR_BANK_BRANCH: RAW_REKENING.MR_BANK_BRANCH,
          AT_FLAG: RAW_REKENING.AT_FLAG,
          M_ID: RAW_REKENING.M_ID
        };
        DATAS.push(DATA);
      }

      result.success = true;
      result.datas = DATAS;
      result.message = "Success !";
    } else {
      result.success = false;
      result.message = "Error, merchant not found !";
    }
    return result
  }

  @get('/merchant-action/merchant-rekening/{M_ID}/view/{id}', {
    responses: {
      '200': {
        description: 'Merchant Rekening type model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(MerchantRekening, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findMerchantRekeningById(
    @param.path.number('M_ID') M_ID: number,
    @param.path.number('id') id: number,
    @param.filter(MerchantRekening, {exclude: 'where'}) filter?: FilterExcludingWhere<MerchantRekening>
  ): Promise<Object> {
    var result: any; result = {success: true, message: '', datas: [], count: 0};
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: [], count: 0};

    var MERCHANT_COUNT = await this.merchantRepository.count({and: [{AT_FLAG: 1}, {M_ID: M_ID}]});
    if (MERCHANT_COUNT.count == 1) {
      var REKEING = await this.merchantRekeningRepository.findById(id, filter);
      result.success = true;
      result.datas = REKEING;
      result.count = 1;
      result.message = "Success !";
    } else {
      result.success = false;
      result.message = "Error, merchant not found !";
    }
    return result
  }

  @post('/merchant-action/merchant-rekening/{M_ID}/delete/{id}', {
    responses: {
      '200': {
        description: 'Merchant Class Update model instance',
        content: {'application/json': {schema: getModelSchemaRef(MerchantRekening)}},
      },
    },
  })
  async deleteMerchantRekening(
    @param.path.number('M_ID') M_ID: number,
    @param.path.number('id') id: number,
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};


    var MERCHANT_COUNT = await this.merchantRepository.count({and: [{AT_FLAG: 1}, {M_ID: M_ID}]});

    if (MERCHANT_COUNT.count == 1) {

      var REKENING_COUNT = await this.merchantRekeningRepository.findOne({where: {and: [{MR_ID: id}, {M_ID: M_ID}]}});
      if (REKENING_COUNT != null) {
        if (REKENING_COUNT.AT_FLAG != 1) {
          try {
            await this.merchantRekeningRepository.deleteById(id);
            result.success = true;
            result.message = "Success delete merchant rekening product !";
          } catch (error) {
            result.success = false;
            result.message = error;
          }
        } else {
          result.success = false;
          result.message = "Error, merchant rekening is actived !";
        }
      } else {
        result.success = false;
        result.message = "Error, merchant rekening not found !";
      }

    } else {
      result.success = false;
      result.message = "Error, merchant not found !";
    }
    return result;
  }

  @post('/merchant-action/merchant-rekening/{M_ID}/actived/{id}', {
    responses: {
      '200': {
        description: 'REKENING AKTIF model instance',
        content: {
          'application/json': {
            schema: RekeningAktifRequestBody
          }
        },
      },
    },
  })
  async activedMerchantRekening(
    @param.path.number('M_ID') M_ID: number,
    @param.path.number('id') id: number
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};


    var MERCHANT_COUNT = await this.merchantRepository.count({and: [{AT_FLAG: 1}, {M_ID: M_ID}]});

    if (MERCHANT_COUNT.count == 1) {

      var REKENING_COUNT = await this.merchantRekeningRepository.count({and: [{MR_ID: id}, {M_ID: M_ID}]});
      if (REKENING_COUNT.count == 1) {
        try {
          await this.merchantRekeningRepository.updateAll({AT_FLAG: 0}, {and: [{M_ID: M_ID}]});
          await this.merchantRekeningRepository.updateAll({AT_FLAG: 1}, {and: [{M_ID: M_ID}, {MR_ID: id}]});

          try {
            var REKENING = await this.merchantRekeningRepository.findById(id)
            await this.merchantRepository.updateById(M_ID, {
              PT_REKENING_NAME: REKENING.MR_REKENING_NAME,
              PT_ACOUNT_NUMBER: REKENING.MR_ACOUNT_NUMBER,
              PT_BANK_NAME: REKENING.MR_BANK_NAME,
              PT_BANK_BRANCH: REKENING.MR_BANK_BRANCH
            });

            result.success = true;
            result.message = "Success actived merchant rekening product !";
          } catch (error) {
            result.success = false;
            result.message = error;
          }
        } catch (error) {
          result.success = false;
          result.message = error;
        }
      } else {
        result.success = false;
        result.message = "Error, merchant rekening not found !";
      }

    } else {
      result.success = false;
      result.message = "Error, merchant not found !";
    }

    return result;
  }

  //MERCHANT PIC
  @post('/merchant-action/merchant-pic/{M_ID}/create', {
    responses: {
      '200': {
        description: 'Merchant PIC model instance',
        content: {'application/json': {schema: getModelSchemaRef(MerchantPic)}},
      },
    },
  })
  async createMerchantPIC(
    @param.path.number('M_ID') M_ID: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(MerchantPic, {
            title: 'NewMerchantPIC',
            exclude: ['ID', 'AT_FLAG', 'AT_CREATE', 'AT_UPDATE', 'AT_WHO', 'M_ID'],
          }),
        },
      },
    })
    PIC_BODY: Omit<MerchantPic, 'ID'>,
  ): Promise<MerchantPic> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};

    var MERCHANT_COUNT = await this.merchantRepository.count({and: [{AT_FLAG: 1}, {M_ID: M_ID}]});

    if (MERCHANT_COUNT.count == 1) {

      PIC_BODY["AT_FLAG"] = 1;
      PIC_BODY["AT_CREATE"] = ControllerConfig.onCurrentTime().toString();
      PIC_BODY["AT_UPDATE"] = undefined;
      PIC_BODY["AT_WHO"] = USER_ID;
      PIC_BODY["M_ID"] = M_ID;

      try {
        await this.merchantPicRepository.updateAll({AT_FLAG: 0}, {and: [{M_ID: M_ID}]});
        var CREATE_PIC = await this.merchantPicRepository.create(PIC_BODY);

        var PIC_SET = await this.merchantPicRepository.findById(CREATE_PIC.ID);
        try {
          await this.merchantRepository.updateById(M_ID, {
            M_PIC_NAME: PIC_SET.PIC_NAME,
            M_PIC_EMAIL: PIC_SET.PIC_EMAIL,
            M_PIC_JABATAN: PIC_SET.PIC_JABATAN,
            M_PIC_PHONE: PIC_SET.PIC_PHONE
          });
          result.success = true;
          result.message = "Success !";
          result.datas = CREATE_PIC;
        } catch (error) {
          result.success = true;
          result.message = error;
        }
      } catch (error) {
        result.success = false;
        result.message = error;
      }
    } else {
      result.success = false;
      result.message = "Error, merchant not found !";
    }

    return result;
  }

  @post('/merchant-action/merchant-pic/{M_ID}/update/{id}', {
    responses: {
      '200': {
        description: 'Merchant PIC Update model instance',
        content: {'application/json': {schema: getModelSchemaRef(MerchantPic)}},
      },
    },
  })
  async updateMerchantPIC(
    @param.path.number('M_ID') M_ID: number,
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(MerchantPic, {
            title: 'UpdateMerchantPIC',
            exclude: ['ID', 'AT_FLAG', 'AT_CREATE', 'AT_UPDATE', 'AT_WHO', 'M_ID'],
          }),
        },
      },
    })
    PIC_BODY: MerchantPic,
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};


    var MERCHANT_COUNT = await this.merchantRepository.count({and: [{AT_FLAG: 1}, {M_ID: M_ID}]});

    if (MERCHANT_COUNT.count == 1) {

      var PIC_COUNT = await this.merchantPicRepository.count({and: [{ID: id}, {M_ID: M_ID}]});
      if (PIC_COUNT.count == 1) {

        PIC_BODY["AT_UPDATE"] = ControllerConfig.onCurrentTime().toString();
        PIC_BODY["AT_WHO"] = USER_ID;
        PIC_BODY["M_ID"] = M_ID;

        try {
          await this.merchantPicRepository.updateById(id, PIC_BODY);
          try {
            var RAW_PIC = await this.merchantPicRepository.findById(id);
            if (RAW_PIC.AT_FLAG == 1) {
              await this.merchantRepository.updateById(M_ID, {
                M_PIC_NAME: RAW_PIC.PIC_NAME,
                M_PIC_EMAIL: RAW_PIC.PIC_EMAIL,
                M_PIC_JABATAN: RAW_PIC.PIC_JABATAN,
                M_PIC_PHONE: RAW_PIC.PIC_PHONE
              });
            }
            result.success = true;
            result.message = "Success update merchant PIC !";
          } catch (error) {
            result.success = false;
            result.message = error;
          }

        } catch (error) {
          result.success = false;
          result.message = error;
        }
      } else {
        result.success = false;
        result.message = "Error, merchant PIC not found !";
      }

    } else {
      result.success = false;
      result.message = "Error, merchant not found !";
    }
    return result;
  }

  @get('/merchant-action/merchant-pic/{M_ID}/views', {
    responses: {
      '200': {
        description: 'Merchant PIC type model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(MerchantPic, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findMerchantPIC(
    @param.path.number('M_ID') M_ID: number,
    @param.filter(MerchantPic) filter?: Filter<MerchantPic>,
  ): Promise<Object> {
    var result: any; result = {success: true, message: '', datas: []};
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};

    var MERCHANT_COUNT = await this.merchantRepository.count({and: [{AT_FLAG: 1}, {M_ID: M_ID}]});
    if (MERCHANT_COUNT.count == 1) {
      var PIC = await this.merchantPicRepository.find(filter);
      var DATAS: any; DATAS = [];
      for (var i in PIC) {
        var RAW_PIC = PIC[i];
        if (RAW_PIC.M_ID != M_ID) {
          continue;
        }

        var DATA: any; DATA = {
          MR_ID: RAW_PIC.ID,
          PIC_NAME: RAW_PIC.PIC_NAME,
          PIC_JABATAN: RAW_PIC.PIC_JABATAN,
          PIC_PHONE: RAW_PIC.PIC_PHONE,
          PIC_EMAIL: RAW_PIC.PIC_EMAIL,
          AT_FLAG: RAW_PIC.AT_FLAG,
          M_ID: RAW_PIC.M_ID
        };
        DATAS.push(DATA);
      }

      result.success = true;
      result.datas = DATAS;
      result.message = "Success !";
    } else {
      result.success = false;
      result.message = "Error, merchant not found !";
    }
    return result
  }

  @get('/merchant-action/merchant-pic/{M_ID}/view/{id}', {
    responses: {
      '200': {
        description: 'Merchant PIC type model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(MerchantPic, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findMerchantPICById(
    @param.path.number('M_ID') M_ID: number,
    @param.path.number('id') id: number,
    @param.filter(MerchantPic, {exclude: 'where'}) filter?: FilterExcludingWhere<MerchantPic>
  ): Promise<Object> {
    var result: any; result = {success: true, message: '', datas: [], count: 0};
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: [], count: 0};

    var MERCHANT_COUNT = await this.merchantRepository.count({and: [{AT_FLAG: 1}, {M_ID: M_ID}]});
    if (MERCHANT_COUNT.count == 1) {
      var PIC = await this.merchantPicRepository.findById(id, filter);
      result.success = true;
      result.datas = PIC;
      result.count = 1;
      result.message = "Success !";
    } else {
      result.success = false;
      result.message = "Error, merchant not found !";
    }
    return result
  }

  @post('/merchant-action/merchant-pic/{M_ID}/delete/{id}', {
    responses: {
      '200': {
        description: 'Merchant PIC Update model instance',
        content: {'application/json': {schema: getModelSchemaRef(MerchantPic)}},
      },
    },
  })
  async deleteMerchantPIC(
    @param.path.number('M_ID') M_ID: number,
    @param.path.number('id') id: number,
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};


    var MERCHANT_COUNT = await this.merchantRepository.count({and: [{AT_FLAG: 1}, {M_ID: M_ID}]});

    if (MERCHANT_COUNT.count == 1) {

      var PIC_COUNT = await this.merchantPicRepository.findOne({where: {and: [{ID: id}, {M_ID: M_ID}]}});
      if (PIC_COUNT != null) {
        if (PIC_COUNT.AT_FLAG != 1) {
          try {
            await this.merchantPicRepository.deleteById(id);
            result.success = true;
            result.message = "Success delete merchant PIC  !";
          } catch (error) {
            result.success = false;
            result.message = error;
          }
        } else {
          result.success = false;
          result.message = "Error, merchant PIC is Actived !";
        }
      } else {
        result.success = false;
        result.message = "Error, merchant PIC not found !";
      }

    } else {
      result.success = false;
      result.message = "Error, merchant not found !";
    }
    return result;
  }

  @post('/merchant-action/merchant-pic/{M_ID}/actived/{id}', {
    responses: {
      '200': {
        description: 'PIC AKTIF model instance',
        content: {
          'application/json': {
            schema: RekeningAktifRequestBody
          }
        },
      },
    },
  })
  async activedMerchantPIC(
    @param.path.number('M_ID') M_ID: number,
    @param.path.number('id') id: number
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};


    var MERCHANT_COUNT = await this.merchantRepository.count({and: [{AT_FLAG: 1}, {M_ID: M_ID}]});

    if (MERCHANT_COUNT.count == 1) {

      var PIC_COUNT = await this.merchantPicRepository.count({and: [{ID: id}, {M_ID: M_ID}]});
      if (PIC_COUNT.count == 1) {
        try {
          await this.merchantPicRepository.updateAll({AT_FLAG: 0}, {and: [{M_ID: M_ID}]});
          await this.merchantPicRepository.updateAll({AT_FLAG: 1}, {and: [{M_ID: M_ID}, {ID: id}]});

          var PIC_SET = await this.merchantPicRepository.findById(id);
          try {
            await this.merchantRepository.updateById(M_ID, {
              M_PIC_NAME: PIC_SET.PIC_NAME,
              M_PIC_EMAIL: PIC_SET.PIC_EMAIL,
              M_PIC_JABATAN: PIC_SET.PIC_JABATAN,
              M_PIC_PHONE: PIC_SET.PIC_PHONE
            });
            result.success = true;
            result.message = "Success actived merchant PIC product !";
          } catch (error) {
            result.success = true;
            result.message = error;
          }

        } catch (error) {
          result.success = false;
          result.message = error;
        }
      } else {
        result.success = false;
        result.message = "Error, merchant PIC not found !";
      }

    } else {
      result.success = false;
      result.message = "Error, merchant not found !";
    }

    return result;
  }

}
