// Uncomment these imports to begin using these cool features!

import {authenticate} from '@loopback/authentication';
import {inject} from '@loopback/core';
import {repository} from '@loopback/repository';
import {
  post,

  Request, requestBody,
  Response,

  RestBindings
} from '@loopback/rest';
import {SecurityBindings, securityId, UserProfile} from '@loopback/security';
import fs from 'fs';
import multer from 'multer';
import path from 'path';
import progressStream from 'progress-stream';
import {AdMasterRepository, DataDigiposRepository, DataDistributeBukpotRepository, DataNotificationRepository, UserRepository} from '../repositories';
import {ControllerConfig} from './controller.config';
import {MailConfig} from './mail.config';

// import {inject} from '@loopback/core';

const GDCONFIG = path.join(__dirname, '../../.config/directory.json');

export const CalculateNotif = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['periode', 'type'],
        properties: {
          periode: {
            type: 'string',
          },
          type: {
            type: 'string'
          }
        }
      }
    },
  },
};



@authenticate('jwt')
export class DistributeDocActionController {
  constructor(
    @repository(DataDigiposRepository)
    public dataDigiposRepository: DataDigiposRepository,
    @repository(DataDistributeBukpotRepository)
    public dataDistributeBukpotRepository: DataDistributeBukpotRepository,
    @repository(AdMasterRepository)
    public adMasterRepository: AdMasterRepository,
    @repository(UserRepository)
    public userRepository: UserRepository,
    @repository(DataNotificationRepository)
    public dataNotificationRepository: DataNotificationRepository,
    @inject(SecurityBindings.USER)
    public currentUserProfile: UserProfile,
  ) { }

  @post('/distribute-doc-action/send-progress', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: CalculateNotif
          }
        },
      },
    },
  })
  async FinishFjp(
    @requestBody(CalculateNotif) CalculateReq: {
      periode: string, type: string
    },
  ): Promise<Object> {
    var returnMs: any; returnMs = {success: true, message: "success"};

    if (CalculateReq.type == "FAKTUR_PAJAK") {
      var raw = await this.dataDigiposRepository.count({and: [{trx_date_group: CalculateReq.periode}, {vat_distribute: 2}]});
      // 16010105
      var userId = this.currentUserProfile[securityId];
      var roleNotifSetADB = ControllerConfig.onGetRoleNotifGroup("16010105");
      var infoUser = await this.userRepository.findById(userId)
      var message = {
        types: "NOTIFICATION",
        target: roleNotifSetADB.role,
        to: "",
        from_who: infoUser.username,
        subject: roleNotifSetADB.label,
        body: "Document Faktur Pajak Berhasil Di Distribusikan, Jumlah terdistribusi : " + raw.count.toString(),
        path_url: "",
        app: "DIGIPOS",
        code: "16010105",
        code_label: "",
        at_create: ControllerConfig.onCurrentTime().toString(),
        at_read: "",
        periode: CalculateReq.periode,
        at_flag: 0
      }
      await this.dataNotificationRepository.create(message);
      ControllerConfig.onSendNotif(message);
    }

    if (CalculateReq.type == "BUKPOT") {
      var raw = await this.dataDistributeBukpotRepository.count({and: [{periode: CalculateReq.periode}, {bukpot_status: 2}]});
      // 16010106
      var userId = this.currentUserProfile[securityId];
      var roleNotifSetADB = ControllerConfig.onGetRoleNotifGroup("16010106");
      var infoUser = await this.userRepository.findById(userId)
      var message = {
        types: "NOTIFICATION",
        target: roleNotifSetADB.role,
        to: "",
        from_who: infoUser.username,
        subject: roleNotifSetADB.label,
        body: "Document Bukti Potong Berhasil Di Distribusikan, Jumlah terdistribusi : " + raw.count.toString(),
        path_url: "",
        app: "DIGIPOS",
        code: "16010106",
        code_label: "",
        at_create: ControllerConfig.onCurrentTime().toString(),
        at_read: "",
        periode: CalculateReq.periode,
        at_flag: 0
      }
      await this.dataNotificationRepository.create(message);
      ControllerConfig.onSendNotif(message);
    }

    if (CalculateReq.type == "INVOICE") {
      var raw = await this.dataDigiposRepository.count({and: [{trx_date_group: CalculateReq.periode}, {distribute_invoice_ttd: 2}]});
      // 16010107
      var userId = this.currentUserProfile[securityId];
      var roleNotifSetADB = ControllerConfig.onGetRoleNotifGroup("16010107");
      var infoUser = await this.userRepository.findById(userId)
      var message = {
        types: "NOTIFICATION",
        target: roleNotifSetADB.role,
        to: "",
        from_who: infoUser.username,
        subject: roleNotifSetADB.label,
        body: "Document Invoice After Sign Berhasil Di Distribusikan, Jumlah terdistribusi : " + raw.count.toString(),
        path_url: "",
        app: "DIGIPOS",
        code: "16010107",
        code_label: "",
        at_create: ControllerConfig.onCurrentTime().toString(),
        at_read: "",
        periode: CalculateReq.periode,
        at_flag: 0
      }
      await this.dataNotificationRepository.create(message);
      ControllerConfig.onSendNotif(message);
    }

    return returnMs;
  }

  @post('/distribute-doc-action/faktur-pajak/upload', {
    responses: {
      200: {
        content: {
          'application/json': {
            schema: {
              type: 'object',
            },
          },
        },
        description: 'Files and fields',
      },
    },
  })
  async fileUpload(
    @requestBody.file()
    requestFile: Request,
    @inject(RestBindings.Http.RESPONSE) response: Response,
  ): Promise<object> {
    return new Promise<object>((resolve, reject) => {
      let msg = [];
      const storage = multer.diskStorage({
        destination: path.join(__dirname, '../../.digipost/doc-distribute/faktur-pajak/'),
        // Use the original file name as is
        filename: (req, file, cb) => {
          cb(null, file.originalname);
        },
      });

      const upload = multer({
        storage: storage,
        fileFilter: async (req, file, cb: any) => {
          var ext = path.extname(file.originalname).toLowerCase();
          if (ext !== '.pdf') {
            cb({
              success: false,
              message: 'Only pdf are allowed !'
            }, false);
          }

          var NameFile = file.originalname.substring(0, file.originalname.length - 4);
          var SplitNameFile = NameFile.split("-");
          var NoFaktur = SplitNameFile[1] == undefined ? "0" : SplitNameFile[1];
          //SplitNameFile[(SplitNameFile.length - 1)];
          var dataSearchNoFaktur = await this.dataDigiposRepository.execute('SELECT * , REPLACE(REPLACE(vat_number, ".", ""),"-","") AS VAT_FILE_NUMBER FROM `DataDigipos` HAVING VAT_FILE_NUMBER = "' + NoFaktur + '"');
          var findNoFaktur: string; findNoFaktur = "";
          for (var j in dataSearchNoFaktur) {
            findNoFaktur = dataSearchNoFaktur[j]["vat_number"];
          }

          console.log("NOFAKTUR FILE : ", NoFaktur);
          console.log("NOFAKTUR : ", findNoFaktur);

          var InfoDigipos = await this.dataDigiposRepository.count({and: [{vat_number: findNoFaktur}]});
          if (InfoDigipos.count == 0) {
            cb({
              success: false,
              message: 'Data faktur pajak in file not exities !'
            }, false);
          }

          cb(null, true);
        }
      }).fields([{name: 'faktur_file', maxCount: 1}]);

      upload(requestFile, response, async (errUpload: any) => {
        if (errUpload)
          resolve({success: false, message: errUpload?.message})
        else {

          var progress: any; progress = progressStream({length: 0});
          requestFile.pipe(progress);
          progress.headers = requestFile.headers;

          progress.on('length', function nowIKnowMyLength(actualLength: any) {
            console.log('actualLength: %s', actualLength);
            progress.setLength(actualLength);
          });
          // get the upload progress
          progress.on('progress', (obj: any) => {
            console.log('progress: %s', obj.percentage);
          });

          var files: any; files = requestFile.files;
          var FileFaktur = files.faktur_file;
          var NameFileOrigin: string; NameFileOrigin = "";
          var NameFile: string; NameFile = "";
          var PathFile: string; PathFile = "";
          for (var i in FileFaktur) {
            var rawFile = FileFaktur[i];
            NameFileOrigin = rawFile.originalname;
            NameFile = rawFile.originalname.substring(0, rawFile.originalname.length - 4);
            PathFile = rawFile.path;
          }

          var SplitNameFile = NameFile.split("-");

          var NoFaktur = SplitNameFile[1] == undefined ? "0" : SplitNameFile[1];
          //SplitNameFile[(SplitNameFile.length - 1)];
          var dataSearchNoFaktur = await this.dataDigiposRepository.execute('SELECT * , REPLACE(REPLACE(vat_number, ".", ""),"-","") AS VAT_FILE_NUMBER FROM `DataDigipos` HAVING VAT_FILE_NUMBER = "' + NoFaktur + '"');
          var findNoFaktur: string; findNoFaktur = "";
          for (var j in dataSearchNoFaktur) {
            findNoFaktur = dataSearchNoFaktur[j]["vat_number"];
          }

          console.log("NOFAKTUR FILE : ", NoFaktur);
          console.log("NOFAKTUR : ", findNoFaktur);


          const InfoDigipos = await this.dataDigiposRepository.findOne({where: {vat_number: findNoFaktur}});
          if (InfoDigipos != null) {
            fs.readFile(GDCONFIG, async (errConfig, contentConfig: any) => {
              if (errConfig) resolve({success: false, message: errConfig});
              var configGD = JSON.parse(contentConfig);
              ControllerConfig.onAPIGoogleDrive(async (rConnect: any) => {
                if (rConnect.success == true) {
                  const drive = rConnect.drive;
                  const auth = rConnect.auth;
                  var masterAD = await this.adMasterRepository.findOne({where: {ad_code: InfoDigipos.ad_code}});
                  if (masterAD != null) {
                    var GDFakturPajak = masterAD.gd_faktur_pajak;
                    ControllerConfig.onGD_Search(auth, "'" + GDFakturPajak + "' in parents and	name = '" + NameFileOrigin + "'", async (rSearchFp: any) => {
                      var fileFp = rSearchFp.data.data.files;
                      if (fileFp.length == 0) {
                        ControllerConfig.onUploadPDFGD(auth, GDFakturPajak, PathFile, NameFileOrigin, async (rUp: any) => {
                          if (rUp.success == true) {
                            //SUCCESS DISTRIBUTE
                            await this.dataDigiposRepository.updateById(InfoDigipos.digipost_id, {
                              vat_distribute: 2,
                              vat_attachment: NameFileOrigin,
                              at_vat_distibute: ControllerConfig.onCurrentTime().toString()
                            });

                            var infoAD = await this.adMasterRepository.findById(InfoDigipos.ad_code);
                            if (infoAD.email != "" && infoAD.email != undefined || infoAD.email != null) {
                              MailConfig.onSendMailDistributeDoc(infoAD.email, {
                                type_doc: "Faktur Pajak",
                                ad_name: infoAD.ad_name,
                                name_doc: NameFileOrigin,
                                token_folder: GDFakturPajak
                              }, (r: any) => { });
                            }


                            var userId = this.currentUserProfile[securityId];
                            var roleNotifSetADB = ControllerConfig.onGetRoleNotifGroup("0902010203");
                            var infoUser = await this.userRepository.findById(userId)
                            await this.dataNotificationRepository.create({
                              types: "NOTIFICATION",
                              target: roleNotifSetADB.role,
                              to: InfoDigipos.ad_code,
                              from_who: infoUser.username,
                              subject: roleNotifSetADB.label,
                              body: "Document Faktur Pajak  telah didistribusikan untuk " + InfoDigipos.ad_code + ", " + InfoDigipos.ad_name + " periode " + InfoDigipos.trx_date_group,
                              path_url: "",
                              app: "DIGIPOS",
                              code: "0902010203",
                              code_label: "",
                              at_create: ControllerConfig.onCurrentTime().toString(),
                              at_read: "",
                              periode: InfoDigipos.trx_date_group,
                              at_flag: 0
                            });

                            ControllerConfig.onSendNotif({
                              types: "NOTIFICATION",
                              target: roleNotifSetADB.role,
                              to: InfoDigipos.ad_code,
                              from_who: infoUser.username,
                              subject: roleNotifSetADB.label,
                              body: "Document Faktur Pajak telah didistribusikan untuk " + InfoDigipos.ad_code + ", " + InfoDigipos.ad_name + " periode " + InfoDigipos.trx_date_group,
                              path_url: "",
                              app: "DIGIPOS",
                              code: "0902010203",
                              code_label: "",
                              at_create: ControllerConfig.onCurrentTime().toString(),
                              at_read: "",
                              periode: InfoDigipos.trx_date_group,
                              at_flag: 0
                            });

                            const InfoDigiposNew = await this.dataDigiposRepository.findOne({where: {vat_number: findNoFaktur}});
                            resolve({success: true, message: "Distribute document faktur pajak successed !", data: InfoDigiposNew});
                          } else {
                            fs.unlinkSync(PathFile);
                            resolve({success: false, message: "Error, failed distribute document faktur pajak  !"});
                          }
                        });
                      } else {
                        var field: string; field = "";
                        for (var j in fileFp) {
                          var rFileInv = fileFp[j];
                          field = rFileInv.id
                        }
                        ControllerConfig.onUploadPDFGDReplace(auth, field, GDFakturPajak, PathFile, NameFileOrigin, async (rUp: any) => {
                          if (rUp.success == true) {
                            //SUCCESS DISTRIBUTE
                            await this.dataDigiposRepository.updateById(InfoDigipos.digipost_id, {
                              vat_distribute: 2,
                              vat_attachment: NameFileOrigin,
                              at_vat_distibute: ControllerConfig.onCurrentTime().toString()
                            });

                            var infoAD = await this.adMasterRepository.findById(InfoDigipos.ad_code);
                            if (infoAD.email != "" && infoAD.email != undefined || infoAD.email != null) {
                              MailConfig.onSendMailDistributeDoc(infoAD.email, {
                                type_doc: "Faktur Pajak",
                                ad_name: infoAD.ad_name,
                                name_doc: NameFileOrigin,
                                token_folder: GDFakturPajak
                              }, (r: any) => { });
                            }

                            var userId = this.currentUserProfile[securityId];
                            var roleNotifSetADB = ControllerConfig.onGetRoleNotifGroup("0902010203");
                            var infoUser = await this.userRepository.findById(userId)
                            await this.dataNotificationRepository.create({
                              types: "NOTIFICATION",
                              target: roleNotifSetADB.role,
                              to: InfoDigipos.ad_code,
                              from_who: infoUser.username,
                              subject: roleNotifSetADB.label,
                              body: "Document Faktur Pajak  telah didistribusikan untuk " + InfoDigipos.ad_code + ", " + InfoDigipos.ad_name + " periode " + InfoDigipos.trx_date_group,
                              path_url: "",
                              app: "DIGIPOS",
                              code: "0902010203",
                              code_label: "",
                              at_create: ControllerConfig.onCurrentTime().toString(),
                              at_read: "",
                              periode: InfoDigipos.trx_date_group,
                              at_flag: 0
                            });

                            ControllerConfig.onSendNotif({
                              types: "NOTIFICATION",
                              target: roleNotifSetADB.role,
                              to: InfoDigipos.ad_code,
                              from_who: infoUser.username,
                              subject: roleNotifSetADB.label,
                              body: "Document Faktur Pajak telah didistribusikan untuk " + InfoDigipos.ad_code + ", " + InfoDigipos.ad_name + " periode " + InfoDigipos.trx_date_group,
                              path_url: "",
                              app: "DIGIPOS",
                              code: "0902010203",
                              code_label: "",
                              at_create: ControllerConfig.onCurrentTime().toString(),
                              at_read: "",
                              periode: InfoDigipos.trx_date_group,
                              at_flag: 0
                            });

                            const InfoDigiposNew = await this.dataDigiposRepository.findOne({where: {vat_number: findNoFaktur}});
                            resolve({success: true, message: "Distribute document faktur pajak successed !", data: InfoDigiposNew});
                          } else {
                            fs.unlinkSync(PathFile);
                            resolve({success: false, message: "Error, failed distribute document faktur pajak  !"});
                          }
                        });
                      }
                    });
                  } else {
                    fs.unlinkSync(PathFile);
                    resolve({success: false, message: "Can't Fount AD Code  !"});
                  }
                } else {
                  fs.unlinkSync(PathFile);
                  resolve({success: false, message: "Can't Connect Google Drive !"});
                }
              });
            })
          } else {
            fs.unlinkSync(PathFile);
            resolve({success: false, message: "Data No Faktur pajak no exities !"});
          }
        }
      });
    });
  }


  @post('/distribute-doc-action/bukpot/upload', {
    responses: {
      200: {
        content: {
          'application/json': {
            schema: {
              type: 'object',
            },
          },
        },
        description: 'Files and fields',
      },
    },
  })
  async fileUploadBukpot(
    @requestBody.file()
    requestFile: Request,
    @inject(RestBindings.Http.RESPONSE) response: Response,
  ): Promise<object> {
    return new Promise<object>((resolve, reject) => {
      let msg = [];
      var PeriodeData = ControllerConfig.onPeriodeDigipos();
      var UserID = this.currentUserProfile[securityId];

      const storage = multer.diskStorage({
        destination: path.join(__dirname, '../../.digipost/doc-distribute/bukpot/'),
        // Use the original file name as is
        filename: (req, file, cb) => {
          cb(null, PeriodeData + "_" + file.originalname);
        },
      });

      const upload = multer({
        storage: storage,
        fileFilter: async (req, file, cb: any) => {
          var ext = path.extname(file.originalname).toLowerCase();
          if (ext !== '.pdf') {
            cb({
              success: false,
              message: 'Only pdf are allowed !'
            }, false);
          }

          var NameFile = file.originalname.substring(0, file.originalname.length - 4);
          var SplitNameFile = NameFile.split("_");
          var NPWP = SplitNameFile[1];
          var npwpAva: string; npwpAva = "";

          var rawAD = await this.adMasterRepository.find();
          for (var i in rawAD) {
            var rawADInfo = rawAD[i];
            npwpAva = rawADInfo.npwp;
            var npwpAD = rawADInfo.npwp.split(".").join("").split("-").join("");
            if (npwpAD == NPWP) {
              break;
            }
          }
          console.log("NPWP File : ", NPWP);
          console.log("NPWP Found : ", npwpAva);
          var InfoAD = await this.adMasterRepository.count({and: [{npwp: npwpAva}, {ad_status: true}]});
          if (InfoAD.count == 0) {
            cb({
              success: false,
              message: 'Npwp in data not exities or npwp not actived !'
            }, false);
          }

          var nameSearch = '%' + NameFile + '%';
          var fileUploadList = await this.dataDistributeBukpotRepository.find({where: {bukpot_attachment: {like: nameSearch}}});
          if (fileUploadList.length != 0) {
            cb({
              success: false,
              message: 'Name file is duplicate, please upload others file unique name !'
            }, false);
          }

          cb(null, true);
        }
      }).fields([{name: 'bukpot_file', maxCount: 1}]);

      upload(requestFile, response, async (errUpload: any) => {
        if (errUpload) {
          console.log("UPLOAD ERROR ", errUpload);
          resolve({success: false, message: errUpload?.message})
        } else {

          var progress: any; progress = progressStream({length: 0});
          requestFile.pipe(progress);
          progress.headers = requestFile.headers;

          progress.on('length', function nowIKnowMyLength(actualLength: any) {
            console.log('actualLength: %s', actualLength);
            progress.setLength(actualLength);
          });
          // get the upload progress
          progress.on('progress', (obj: any) => {
            console.log('progress: %s', obj.percentage);
          });

          var DataBody: any; DataBody = {};
          for (var i in requestFile.body) {
            DataBody[i] = requestFile.body[i];
          }

          console.log("BODY POST ", DataBody);

          var files: any; files = requestFile.files;
          var FileFaktur = files.bukpot_file;
          var NameFile: string; NameFile = "";
          var PathFile: string; PathFile = "";
          var NameFileOrigin: string; NameFileOrigin = "";
          for (var i in FileFaktur) {
            var rawFile = FileFaktur[i];
            NameFileOrigin = PeriodeData + "_" + rawFile.originalname;
            NameFile = rawFile.originalname.substring(0, rawFile.originalname.length - 4);
            PathFile = rawFile.path;
          }
          var SplitNameFile = NameFile.split("_");
          var NPWP = SplitNameFile[1];
          var npwpAva: string; npwpAva = "";

          var rawAD = await this.adMasterRepository.find();
          for (var i in rawAD) {
            var rawADInfo = rawAD[i];
            npwpAva = rawADInfo.npwp;
            var npwpAD = rawADInfo.npwp.split(".").join("").split("-").join("");
            if (npwpAD == NPWP) {
              // NPWP = rawADInfo.npwp;
              break;
            }
          }


          if (DataBody.periode != null && DataBody.periode != "" && DataBody.periode != undefined) {
            const InfoAD = await this.adMasterRepository.findOne({where: {and: [{npwp: npwpAva}, {ad_status: true}]}});
            // console.log(InfoAD);
            // console.log(NPWP, DataBody.periode);

            if (InfoAD != null && NPWP != undefined) {
              fs.readFile(GDCONFIG, async (errConfig, contentConfig: any) => {
                if (errConfig) resolve({success: false, message: errConfig});
                var configGD = JSON.parse(contentConfig);
                ControllerConfig.onAPIGoogleDrive(async (rConnect: any) => {
                  if (rConnect.success == true) {
                    const drive = rConnect.drive;
                    const auth = rConnect.auth;
                    var GDBukPot = InfoAD.gd_bukpot;
                    ControllerConfig.onGD_Search(auth, "'" + GDBukPot + "' in parents and	name = '" + NameFileOrigin + "'", async (rSearchFp: any) => {
                      var fileFp = rSearchFp.data.data.files;
                      if (fileFp.length == 0) {
                        ControllerConfig.onUploadPDFGD(auth, GDBukPot, PathFile, NameFileOrigin, async (rUp: any) => {
                          if (rUp.success == true) {
                            //SUCCESS DISTRIBUTE
                            var infoBukpot = await this.dataDistributeBukpotRepository.findOne({where: {and: [{ad_code: InfoAD.ad_code}, {periode: DataBody.periode}, {bukpot_status: 0}]}});
                            var InfoDigiposNew: any; InfoDigiposNew = {};
                            try {
                              if (infoBukpot != null) {
                                //UPDATE NEW DATA
                                await this.dataDistributeBukpotRepository.updateById(infoBukpot.bukpot_id, {
                                  ad_code: InfoAD.ad_code,
                                  ad_name: InfoAD.ad_name,
                                  npwp: InfoAD.npwp,
                                  periode: DataBody.periode,
                                  bukpot_attachment: NameFileOrigin,
                                  bukpot_status: 2,
                                  at_process: ControllerConfig.onCurrentTime().toString(),
                                  at_distribute: ControllerConfig.onCurrentTime().toString(),
                                  by_process: UserID
                                });
                                console.log("Error Upload  :", "UPDATE  BUKPOT");
                              } else {
                                //ADD NEW DATA
                                await this.dataDistributeBukpotRepository.create({
                                  ad_code: InfoAD.ad_code,
                                  ad_name: InfoAD.ad_name,
                                  npwp: InfoAD.npwp,
                                  periode: DataBody.periode,
                                  bukpot_attachment: NameFileOrigin,
                                  bukpot_status: 2,
                                  at_process: ControllerConfig.onCurrentTime().toString(),
                                  at_distribute: ControllerConfig.onCurrentTime().toString(),
                                  by_process: UserID
                                });
                                console.log("Error Upload  :", "ADD NEW BUKPOT");
                              }

                              if (InfoAD.email != "" && InfoAD.email != undefined || InfoAD.email != null) {
                                MailConfig.onSendMailDistributeDoc(InfoAD.email, {
                                  type_doc: "Bukti Potong",
                                  ad_name: InfoAD.ad_name,
                                  name_doc: NameFileOrigin,
                                  token_folder: GDBukPot
                                }, (r: any) => { });
                              }

                              var userId = this.currentUserProfile[securityId];
                              var roleNotifSetADB = ControllerConfig.onGetRoleNotifGroup("0902010201");
                              var infoUser = await this.userRepository.findById(userId)
                              await this.dataNotificationRepository.create({
                                types: "NOTIFICATION",
                                target: roleNotifSetADB.role,
                                to: InfoAD.ad_code,
                                from_who: infoUser.username,
                                subject: roleNotifSetADB.label,
                                body: "Document Bukti Potong telah didistribusikan untuk " + InfoAD.ad_code + ", " + InfoAD.ad_name + " periode " + DataBody.periode,
                                path_url: "",
                                app: "DIGIPOS",
                                code: "0902010201",
                                code_label: "",
                                at_create: ControllerConfig.onCurrentTime().toString(),
                                at_read: "",
                                periode: DataBody.periode,
                                at_flag: 0
                              });

                              ControllerConfig.onSendNotif({
                                types: "NOTIFICATION",
                                target: roleNotifSetADB.role,
                                to: InfoAD.ad_code,
                                from_who: infoUser.username,
                                subject: roleNotifSetADB.label,
                                body: "Document Bukti Potong telah didistribusikan untuk " + InfoAD.ad_code + ", " + InfoAD.ad_name + " periode " + DataBody.periode,
                                path_url: "",
                                app: "DIGIPOS",
                                code: "0902010201",
                                code_label: "",
                                at_create: ControllerConfig.onCurrentTime().toString(),
                                at_read: "",
                                periode: DataBody.periode,
                                at_flag: 0
                              });

                              InfoDigiposNew = await this.dataDistributeBukpotRepository.findOne({where: {and: [{ad_code: InfoAD.ad_code}, {periode: DataBody.periode}]}});
                              resolve({success: true, message: "Distribute document bukti potong successed !", data: InfoDigiposNew});
                            } catch (error) {
                              fs.unlinkSync(PathFile);
                              console.log("Error Upload  :", error);
                              resolve({success: false, message: error});
                            }
                          } else {
                            fs.unlinkSync(PathFile);
                            resolve({success: false, message: "Error, failed distribute document bukti potong  !"});
                          }
                        });
                      } else {
                        var field: string; field = "";
                        for (var j in fileFp) {
                          var rFileInv = fileFp[j];
                          field = rFileInv.id
                        }
                        ControllerConfig.onUploadPDFGDReplace(auth, field, GDBukPot, PathFile, NameFileOrigin, async (rUp: any) => {
                          if (rUp.success == true) {
                            //SUCCESS DISTRIBUTE
                            var infoBukpot = await this.dataDistributeBukpotRepository.findOne({where: {and: [{ad_code: InfoAD.ad_code}, {periode: DataBody.periode}]}});
                            var InfoDigiposNew: any; InfoDigiposNew = {};
                            try {
                              if (infoBukpot != null) {
                                //UPDATE NEW DATA
                                await this.dataDistributeBukpotRepository.updateById(infoBukpot.bukpot_id, {
                                  ad_code: InfoAD.ad_code,
                                  ad_name: InfoAD.ad_name,
                                  npwp: InfoAD.npwp,
                                  periode: DataBody.periode,
                                  bukpot_attachment: NameFileOrigin,
                                  bukpot_status: 2,
                                  at_process: ControllerConfig.onCurrentTime().toString(),
                                  at_distribute: ControllerConfig.onCurrentTime().toString(),
                                  by_process: ""
                                });
                                console.log("Error Upload Replace :", "UPDATE  BUKPOT");
                              } else {
                                //ADD NEW DATA
                                await this.dataDistributeBukpotRepository.create({
                                  ad_code: InfoAD.ad_code,
                                  ad_name: InfoAD.ad_name,
                                  npwp: InfoAD.npwp,
                                  periode: DataBody.periode,
                                  bukpot_attachment: NameFileOrigin,
                                  bukpot_status: 2,
                                  at_process: ControllerConfig.onCurrentTime().toString(),
                                  at_distribute: ControllerConfig.onCurrentTime().toString(),
                                  by_process: ""
                                });
                                console.log("Error Upload Replace :", "ADD NEW BUKPOT");
                              }

                              if (InfoAD.email != "" && InfoAD.email != undefined || InfoAD.email != null) {
                                MailConfig.onSendMailDistributeDoc(InfoAD.email, {
                                  type_doc: "Bukti Potong",
                                  ad_name: InfoAD.ad_name,
                                  name_doc: NameFileOrigin,
                                  token_folder: GDBukPot
                                }, (r: any) => { });
                              }

                              var userId = this.currentUserProfile[securityId];
                              var roleNotifSetADB = ControllerConfig.onGetRoleNotifGroup("0902010201");
                              var infoUser = await this.userRepository.findById(userId)
                              await this.dataNotificationRepository.create({
                                types: "NOTIFICATION",
                                target: roleNotifSetADB.role,
                                to: InfoAD.ad_code,
                                from_who: infoUser.username,
                                subject: roleNotifSetADB.label,
                                body: "Document Bukti Potong telah didistribusikan untuk " + InfoAD.ad_code + ", " + InfoAD.ad_name + " periode " + DataBody.periode,
                                path_url: "",
                                app: "DIGIPOS",
                                code: "0902010201",
                                code_label: "",
                                at_create: ControllerConfig.onCurrentTime().toString(),
                                at_read: "",
                                periode: DataBody.periode,
                                at_flag: 0
                              });

                              ControllerConfig.onSendNotif({
                                types: "NOTIFICATION",
                                target: roleNotifSetADB.role,
                                to: InfoAD.ad_code,
                                from_who: infoUser.username,
                                subject: roleNotifSetADB.label,
                                body: "Document Bukti Potong telah didistribusikan untuk " + InfoAD.ad_code + ", " + InfoAD.ad_name + " periode " + DataBody.periode,
                                path_url: "",
                                app: "DIGIPOS",
                                code: "0902010201",
                                code_label: "",
                                at_create: ControllerConfig.onCurrentTime().toString(),
                                at_read: "",
                                periode: DataBody.periode,
                                at_flag: 0
                              });
                              InfoDigiposNew = await this.dataDistributeBukpotRepository.findOne({where: {and: [{ad_code: InfoAD.ad_code}, {periode: DataBody.periode}]}});
                              resolve({success: true, message: "Distribute document bukti potong successed !", data: InfoDigiposNew});
                            } catch (error) {
                              fs.unlinkSync(PathFile);
                              console.log("Error Upload Replace :", error);
                              resolve({success: false, message: error});
                            }
                          } else {
                            fs.unlinkSync(PathFile);
                            resolve({success: false, message: "Error, failed distribute document bukti potong !"});
                          }
                        });
                      }
                    });
                  } else {
                    fs.unlinkSync(PathFile);
                    resolve({success: false, message: "Can't Connect Google Drive !"});
                  }
                });
              })
            } else {
              fs.unlinkSync(PathFile);
              resolve({success: false, message: "Can't Fount AD Code  !"});
            }
          } else {
            fs.unlinkSync(PathFile);
            resolve({success: false, message: "Periode not exities, please input periode !"});
          }
        }
      });
    });
  }



  @post('/distribute-doc-action/invoice-sign/upload', {
    responses: {
      200: {
        content: {
          'application/json': {
            schema: {
              type: 'object',
            },
          },
        },
        description: 'Files and fields',
      },
    },
  })
  async fileUploadInvSign(
    @requestBody.file()
    requestFile: Request,
    @inject(RestBindings.Http.RESPONSE) response: Response,
  ): Promise<object> {
    return new Promise<object>((resolve, reject) => {
      let msg = [];
      const storage = multer.diskStorage({
        destination: path.join(__dirname, '../../.digipost/doc-distribute/invoice-sign/'),
        // Use the original file name as is
        filename: (req, file, cb) => {
          cb(null, "SIGN_" + file.originalname);
        },
      });

      const upload = multer({
        storage: storage,
        fileFilter: async (req, file, cb: any) => {
          var ext = path.extname(file.originalname).toLowerCase();
          if (ext !== '.pdf') {
            cb({
              success: false,
              message: 'Only pdf are allowed !'
            }, false);
          }

          var NameFile = file.originalname.substring(0, file.originalname.length - 4);
          var SplitNameFile = NameFile.split("_");
          var NoInv = SplitNameFile[0];
          var InfoDigipos = await this.dataDigiposRepository.count({and: [{inv_number: NoInv}]});
          if (InfoDigipos.count == 0) {
            cb({
              success: false,
              message: 'Data No Invoice in file not exities !'
            }, false);
          }

          cb(null, true);
        }
      }).fields([{name: 'invoice_file', maxCount: 1}]);

      upload(requestFile, response, async (errUpload: any) => {
        if (errUpload)
          resolve({success: false, message: errUpload?.message})
        else {

          var progress: any; progress = progressStream({length: 0});
          requestFile.pipe(progress);
          progress.headers = requestFile.headers;

          progress.on('length', function nowIKnowMyLength(actualLength: any) {
            console.log('actualLength: %s', actualLength);
            progress.setLength(actualLength);
          });
          // get the upload progress
          progress.on('progress', (obj: any) => {
            console.log('progress: %s', obj.percentage);
          });

          var files: any; files = requestFile.files;
          var FileFaktur = files.invoice_file;
          var NameFileOrigin: string; NameFileOrigin = "";
          var NameFile: string; NameFile = "";
          var PathFile: string; PathFile = "";
          for (var i in FileFaktur) {
            var rawFile = FileFaktur[i];
            NameFileOrigin = "SIGN_" + rawFile.originalname;
            NameFile = rawFile.originalname.substring(0, rawFile.originalname.length - 4);
            PathFile = rawFile.path;
          }
          var SplitNameFile = NameFile.split("_");
          var NoInv = SplitNameFile[0];
          const InfoDigipos = await this.dataDigiposRepository.findOne({where: {inv_number: NoInv}});
          if (InfoDigipos != null) {
            fs.readFile(GDCONFIG, async (errConfig, contentConfig: any) => {
              if (errConfig) resolve({success: false, message: errConfig});
              var configGD = JSON.parse(contentConfig);
              ControllerConfig.onAPIGoogleDrive(async (rConnect: any) => {
                if (rConnect.success == true) {
                  const drive = rConnect.drive;
                  const auth = rConnect.auth;
                  var masterAD = await this.adMasterRepository.findOne({where: {ad_code: InfoDigipos.ad_code}});
                  if (masterAD != null) {
                    var GDInvoice = masterAD.gd_invoice;
                    ControllerConfig.onGD_Search(auth, "'" + GDInvoice + "' in parents and	name = '" + NameFileOrigin + "'", async (rSearchFp: any) => {
                      var fileFp = rSearchFp.data.data.files;
                      if (fileFp.length == 0) {
                        ControllerConfig.onUploadPDFGD(auth, GDInvoice, PathFile, NameFileOrigin, async (rUp: any) => {
                          if (rUp.success == true) {
                            //SUCCESS DISTRIBUTE
                            await this.dataDigiposRepository.updateById(InfoDigipos.digipost_id, {
                              distribute_invoice_ttd: 2,
                              attach_invoice_ttd: NameFileOrigin,
                              at_distribute_invoice_ttd: ControllerConfig.onCurrentTime().toString()
                            });

                            var InfoAD = await this.adMasterRepository.findById(InfoDigipos.ad_code);
                            if (InfoAD.email != "" && InfoAD.email != undefined || InfoAD.email != null) {
                              MailConfig.onSendMailDistributeDoc(InfoAD.email, {
                                type_doc: "Invoice After Sign",
                                ad_name: InfoAD.ad_name,
                                name_doc: NameFileOrigin,
                                token_folder: GDInvoice
                              }, (r: any) => { });
                            }

                            var userId = this.currentUserProfile[securityId];
                            var roleNotifSetADB = ControllerConfig.onGetRoleNotifGroup("0902010202");
                            var infoUser = await this.userRepository.findById(userId)
                            await this.dataNotificationRepository.create({
                              types: "NOTIFICATION",
                              target: roleNotifSetADB.role,
                              to: InfoDigipos.ad_code,
                              from_who: infoUser.username,
                              subject: roleNotifSetADB.label,
                              body: "Document Invoice After Sign telah didistribusikan untuk " + InfoDigipos.ad_code + ", " + InfoDigipos.ad_name + " periode " + InfoDigipos.trx_date_group,
                              path_url: "",
                              app: "DIGIPOS",
                              code: "0902010202",
                              code_label: "",
                              at_create: ControllerConfig.onCurrentTime().toString(),
                              at_read: "",
                              periode: InfoDigipos.trx_date_group,
                              at_flag: 0
                            });

                            ControllerConfig.onSendNotif({
                              types: "NOTIFICATION",
                              target: roleNotifSetADB.role,
                              to: InfoDigipos.ad_code,
                              from_who: infoUser.username,
                              subject: roleNotifSetADB.label,
                              body: "Document Invoice After Sign telah didistribusikan untuk " + InfoDigipos.ad_code + ", " + InfoDigipos.ad_name + " periode " + InfoDigipos.trx_date_group,
                              path_url: "",
                              app: "DIGIPOS",
                              code: "0902010202",
                              code_label: "",
                              at_create: ControllerConfig.onCurrentTime().toString(),
                              at_read: "",
                              periode: InfoDigipos.trx_date_group,
                              at_flag: 0
                            });

                            const InfoDigiposNew = await this.dataDigiposRepository.findOne({where: {inv_number: NoInv}});
                            resolve({success: true, message: "Distribute document No Invoice successed !", data: InfoDigiposNew});
                          } else {
                            fs.unlinkSync(PathFile);
                            resolve({success: false, message: "Error, failed distribute document invoice after sign !"});
                          }
                        });
                      } else {
                        var field: string; field = "";
                        for (var j in fileFp) {
                          var rFileInv = fileFp[j];
                          field = rFileInv.id
                        }
                        ControllerConfig.onUploadPDFGDReplace(auth, field, GDInvoice, PathFile, NameFileOrigin, async (rUp: any) => {
                          if (rUp.success == true) {
                            //SUCCESS DISTRIBUTE
                            await this.dataDigiposRepository.updateById(InfoDigipos.digipost_id, {
                              distribute_invoice_ttd: 2,
                              attach_invoice_ttd: NameFileOrigin,
                              at_distribute_invoice_ttd: ControllerConfig.onCurrentTime().toString()
                            });


                            var InfoAD = await this.adMasterRepository.findById(InfoDigipos.ad_code);
                            if (InfoAD.email != "" && InfoAD.email != undefined || InfoAD.email != null) {
                              MailConfig.onSendMailDistributeDoc(InfoAD.email, {
                                type_doc: "Invoice After Sign",
                                ad_name: InfoAD.ad_name,
                                name_doc: NameFileOrigin,
                                token_folder: GDInvoice
                              }, (r: any) => { });
                            }

                            var userId = this.currentUserProfile[securityId];
                            var roleNotifSetADB = ControllerConfig.onGetRoleNotifGroup("0902010202");
                            var infoUser = await this.userRepository.findById(userId)
                            await this.dataNotificationRepository.create({
                              types: "NOTIFICATION",
                              target: roleNotifSetADB.role,
                              to: InfoDigipos.ad_code,
                              from_who: infoUser.username,
                              subject: roleNotifSetADB.label,
                              body: "Document Invoice After Sign telah didistribusikan untuk " + InfoDigipos.ad_code + ", " + InfoDigipos.ad_name + " periode " + InfoDigipos.trx_date_group,
                              path_url: "",
                              app: "DIGIPOS",
                              code: "0902010202",
                              code_label: "",
                              at_create: ControllerConfig.onCurrentTime().toString(),
                              at_read: "",
                              periode: InfoDigipos.trx_date_group,
                              at_flag: 0
                            });

                            ControllerConfig.onSendNotif({
                              types: "NOTIFICATION",
                              target: roleNotifSetADB.role,
                              to: InfoDigipos.ad_code,
                              from_who: infoUser.username,
                              subject: roleNotifSetADB.label,
                              body: "Document Invoice After Sign telah didistribusikan untuk " + InfoDigipos.ad_code + ", " + InfoDigipos.ad_name + " periode " + InfoDigipos.trx_date_group,
                              path_url: "",
                              app: "DIGIPOS",
                              code: "0902010202",
                              code_label: "",
                              at_create: ControllerConfig.onCurrentTime().toString(),
                              at_read: "",
                              periode: InfoDigipos.trx_date_group,
                              at_flag: 0
                            });
                            const InfoDigiposNew = await this.dataDigiposRepository.findOne({where: {inv_number: NoInv}});
                            resolve({success: true, message: "Distribute document No Invoice successed !", data: InfoDigiposNew});
                          } else {
                            fs.unlinkSync(PathFile);
                            resolve({success: false, message: "Error, failed distribute document invoice after sign !"});
                          }
                        });
                      }
                    });
                  } else {
                    fs.unlinkSync(PathFile);
                    resolve({success: false, message: "Can't Fount AD Code  !"});
                  }
                } else {
                  fs.unlinkSync(PathFile);
                  resolve({success: false, message: "Can't Connect Google Drive !"});
                }
              });
            })
          } else {
            fs.unlinkSync(PathFile);
            resolve({success: false, message: "Data No Invoice no exities !"});
          }
        }
      });
    });
  }


}
