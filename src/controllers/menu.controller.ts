import {authenticate} from '@loopback/authentication';
import {
  Count, CountSchema,
  Filter,
  FilterExcludingWhere, repository,
  Where
} from '@loopback/repository';
import {
  del, get,
  getModelSchemaRef, param,
  patch, post,
  put,
  requestBody
} from '@loopback/rest';
import {Menu} from '../models';
import {MenuaccessRepository, MenuRepository, RolemappingRepository} from '../repositories';



const RoleSchema = {
  type: 'object',
  required: ['role'],
  properties: {
    role: {
      type: 'string'
    },
  },
};


const UpdatePermissionSchema = {
  type: 'object',
  required: ['MenuId', 'RoleId', 'Action', 'Child'],
  properties: {
    MenuId: {
      type: 'number'
    },
    RoleId: {
      type: 'number'
    },
    Action: {
      type: 'string'
    },
    Child: {
      type: 'array'
    },
  },
};




@authenticate('jwt')
export class MenuController {
  constructor(
    @repository(MenuRepository)
    public menuRepository: MenuRepository,
    @repository(RolemappingRepository)
    public roleMappingRepository: RolemappingRepository,
    @repository(MenuaccessRepository)
    public menuAccessRepository: MenuaccessRepository,
  ) {}

  @post('/Menu', {
    responses: {
      '200': {
        description: 'Menu model instance',
        content: {'application/json': {schema: getModelSchemaRef(Menu)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Menu, {
            title: 'NewMenu',
            exclude: ['id'],
          }),
        },
      },
    })
    menu: Omit<Menu, 'id'>,
  ): Promise<Menu> {
    return this.menuRepository.create(menu);
  }

  @post('/Menu/getMenuRole', {
    responses: {
      '200': {
        description: 'Token',
        content: {
          'application/json': {
            schema: {
              type: 'object',
            },
          },
        },
      },
    },
  })
  async getMenuRole(
    @requestBody({
      description: 'Get Menu Role',
      required: true,
      content: {
        'application/json': {schema: RoleSchema},
      },
    }) credentials: {role: string},
  ): Promise<{info: Object}> {
    var roleMap = await this.roleMappingRepository.findOne({"where": {"UserId": credentials.role}});
    var RoleId = roleMap?.RoleId;
    var menuAccess = await this.menuAccessRepository.find({"where": {"RoleId": RoleId}, order: ['MenuId ASC']});


    var A = new Array();
    await Promise.all(menuAccess.map(async (item, index) => {
      var menuInfo = await this.menuRepository.findOne({"where": {"id": item.MenuId}});
      var infoAllow = {
        "Action": item.Action,
        "id": menuInfo?.id,
        "M_Icon": menuInfo?.M_Icon === null ? "" : menuInfo?.M_Icon,
        "M_Name": menuInfo?.M_Name,
        "M_Label": menuInfo?.M_Label,
        "M_Link": menuInfo?.M_Link,
        "M_Class": menuInfo?.M_Class === null ? "" : menuInfo?.M_Class,
        "M_Alt": menuInfo?.M_Alt === null ? "" : menuInfo?.M_Alt,
        "M_Status": menuInfo?.M_Status,
        "M_Parent": menuInfo?.M_Parent,
        "M_Index": menuInfo?.M_Index
      };
      A.push(infoAllow);
    }));

    return {
      info: A
    };

  }

  @post('/MenuAccess/updatePermission', {
    responses: {
      '200': {
        description: 'Token',
        content: {
          'application/json': {
            schema: {
              type: 'object',
            },
          },
        },
      },
    },
  })
  async updatePermission(
    @requestBody({
      description: 'Get Menu Role',
      required: true,
      content: {
        'application/json': {schema: UpdatePermissionSchema},
      },
    }) credentials: {MenuId: number, RoleId: number, Action: string, Child: []},
  ): Promise<{success: Object}> {

    // var roleMap = await this.menuAccessRepository.findOne({"where": {"UserId": credentials.role}});
    var dataParent = {MenuId: credentials.MenuId, RoleId: credentials.RoleId, Action: credentials.Action};
    var update = await this.menuAccessRepository.updateAll(dataParent, {"MenuId": credentials.MenuId, "RoleId": credentials.RoleId});
    var updateChild = await Promise.all(credentials.Child.map(async (item, index) => {
      var dataChild = {MenuId: item, RoleId: credentials.RoleId, Action: credentials.Action};
      await this.menuAccessRepository.updateAll(dataChild, {"MenuId": item, "RoleId": credentials.RoleId});
    }));

    var message = {"statusCode": 200, "name": "success", "message": "success", "details": "", "stack": ""};
    return {
      success: message
    };

  }


  @post('/Menu/getMenuRoleById', {
    responses: {
      '200': {
        description: 'Token',
        content: {
          'application/json': {
            schema: {
              type: 'object',
            },
          },
        },
      },
    },
  })
  async getMenuRoleById(
    @requestBody({
      description: 'Get Menu Role',
      required: true,
      content: {
        'application/json': {schema: RoleSchema},
      },
    }) credentials: {role: number},
  ): Promise<{info: Object}> {

    var menuAccess = await this.menuAccessRepository.find({"where": {"RoleId": credentials.role}, order: ['MenuId ASC']});
    var A = new Array();
    await Promise.all(menuAccess.map(async (item, index) => {
      var menuInfo = await this.menuRepository.findOne({"where": {"id": item.MenuId}});
      var infoAllow = {
        "Action": item.Action,
        "id": menuInfo?.id,
        "M_Icon": menuInfo?.M_Icon === null ? "" : menuInfo?.M_Icon,
        "M_Name": menuInfo?.M_Name,
        "M_Label": menuInfo?.M_Label,
        "M_Link": menuInfo?.M_Link,
        "M_Class": menuInfo?.M_Class === null ? "" : menuInfo?.M_Class,
        "M_Alt": menuInfo?.M_Alt === null ? "" : menuInfo?.M_Alt,
        "M_Status": menuInfo?.M_Status,
        "M_Parent": menuInfo?.M_Parent,
        "M_Index": menuInfo?.M_Index
      };
      A.push(infoAllow);
    }));

    return {
      info: A
    };

  }

  @get('/Menu/count', {
    responses: {
      '200': {
        description: 'Menu model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(Menu) where?: Where<Menu>,
  ): Promise<Count> {
    return this.menuRepository.count(where);
  }

  @get('/Menu', {
    responses: {
      '200': {
        description: 'Array of Menu model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Menu, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(Menu) filter?: Filter<Menu>,
  ): Promise<Menu[]> {
    return this.menuRepository.find(filter);
  }

  @patch('/Menu', {
    responses: {
      '200': {
        description: 'Menu PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Menu, {partial: true}),
        },
      },
    })
    menu: Menu,
    @param.where(Menu) where?: Where<Menu>,
  ): Promise<Count> {
    return this.menuRepository.updateAll(menu, where);
  }

  @get('/Menu/{id}', {
    responses: {
      '200': {
        description: 'Menu model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Menu, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(Menu, {exclude: 'where'}) filter?: FilterExcludingWhere<Menu>
  ): Promise<Menu> {
    return this.menuRepository.findById(id, filter);
  }

  @patch('/Menu/{id}', {
    responses: {
      '204': {
        description: 'Menu PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Menu, {partial: true}),
        },
      },
    })
    menu: Menu,
  ): Promise<void> {
    await this.menuRepository.updateById(id, menu);
  }

  @put('/Menu/{id}', {
    responses: {
      '204': {
        description: 'Menu PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() menu: Menu,
  ): Promise<void> {
    await this.menuRepository.replaceById(id, menu);
  }

  @del('/Menu/{id}', {
    responses: {
      '204': {
        description: 'Menu DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.menuRepository.deleteById(id);
  }
}
