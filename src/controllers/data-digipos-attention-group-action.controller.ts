import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {DataDigiposAttentionGroup} from '../models';
import {DataDigiposAttentionGroupRepository} from '../repositories';

export class DataDigiposAttentionGroupActionController {
  constructor(
    @repository(DataDigiposAttentionGroupRepository)
    public dataDigiposAttentionGroupRepository : DataDigiposAttentionGroupRepository,
  ) {}

  @post('/data-digipos-attention-groups', {
    responses: {
      '200': {
        description: 'DataDigiposAttentionGroup model instance',
        content: {'application/json': {schema: getModelSchemaRef(DataDigiposAttentionGroup)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(DataDigiposAttentionGroup, {
            title: 'NewDataDigiposAttentionGroup',
            exclude: ['id'],
          }),
        },
      },
    })
    dataDigiposAttentionGroup: Omit<DataDigiposAttentionGroup, 'id'>,
  ): Promise<DataDigiposAttentionGroup> {
    return this.dataDigiposAttentionGroupRepository.create(dataDigiposAttentionGroup);
  }

  @get('/data-digipos-attention-groups/count', {
    responses: {
      '200': {
        description: 'DataDigiposAttentionGroup model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(DataDigiposAttentionGroup) where?: Where<DataDigiposAttentionGroup>,
  ): Promise<Count> {
    return this.dataDigiposAttentionGroupRepository.count(where);
  }

  @get('/data-digipos-attention-groups', {
    responses: {
      '200': {
        description: 'Array of DataDigiposAttentionGroup model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(DataDigiposAttentionGroup, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(DataDigiposAttentionGroup) filter?: Filter<DataDigiposAttentionGroup>,
  ): Promise<DataDigiposAttentionGroup[]> {
    return this.dataDigiposAttentionGroupRepository.find(filter);
  }

  @patch('/data-digipos-attention-groups', {
    responses: {
      '200': {
        description: 'DataDigiposAttentionGroup PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(DataDigiposAttentionGroup, {partial: true}),
        },
      },
    })
    dataDigiposAttentionGroup: DataDigiposAttentionGroup,
    @param.where(DataDigiposAttentionGroup) where?: Where<DataDigiposAttentionGroup>,
  ): Promise<Count> {
    return this.dataDigiposAttentionGroupRepository.updateAll(dataDigiposAttentionGroup, where);
  }

  @get('/data-digipos-attention-groups/{id}', {
    responses: {
      '200': {
        description: 'DataDigiposAttentionGroup model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(DataDigiposAttentionGroup, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(DataDigiposAttentionGroup, {exclude: 'where'}) filter?: FilterExcludingWhere<DataDigiposAttentionGroup>
  ): Promise<DataDigiposAttentionGroup> {
    return this.dataDigiposAttentionGroupRepository.findById(id, filter);
  }

  @patch('/data-digipos-attention-groups/{id}', {
    responses: {
      '204': {
        description: 'DataDigiposAttentionGroup PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(DataDigiposAttentionGroup, {partial: true}),
        },
      },
    })
    dataDigiposAttentionGroup: DataDigiposAttentionGroup,
  ): Promise<void> {
    await this.dataDigiposAttentionGroupRepository.updateById(id, dataDigiposAttentionGroup);
  }

  @put('/data-digipos-attention-groups/{id}', {
    responses: {
      '204': {
        description: 'DataDigiposAttentionGroup PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() dataDigiposAttentionGroup: DataDigiposAttentionGroup,
  ): Promise<void> {
    await this.dataDigiposAttentionGroupRepository.replaceById(id, dataDigiposAttentionGroup);
  }

  @del('/data-digipos-attention-groups/{id}', {
    responses: {
      '204': {
        description: 'DataDigiposAttentionGroup DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.dataDigiposAttentionGroupRepository.deleteById(id);
  }
}
