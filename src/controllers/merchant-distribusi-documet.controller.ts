import {authenticate} from '@loopback/authentication';
import {inject} from '@loopback/core';
import {Filter, repository} from '@loopback/repository';
import {
  get,
  getModelSchemaRef,
  param,
  post,

  Request, requestBody,
  Response,

  RestBindings
} from '@loopback/rest';
import {SecurityBindings, securityId, UserProfile} from '@loopback/security';
import fs from 'fs';
import multer from 'multer';
import path from 'path';
import progressStream from 'progress-stream';
import {MerchantProductDistribusi} from '../models';
import {MerchantProductDistribusiRepository, MerchantProductDocumentRepository, MerchantProductMasterRepository, MerchantProductRepository, MerchantRepository, UserRepository} from '../repositories';
import {ControllerConfig} from './controller.config';


//1:BAR, 2:BAR_SIGN, 3:INVOICE, 4:INVOICE_SIGN, 5:FAKTUR, 6:BUKPOT, 7:OTHERS

@authenticate('jwt')
export class MerchantDistribusiDocumetController {
  constructor(

    @repository(MerchantProductMasterRepository)
    public merchantProductMasterRepository: MerchantProductMasterRepository,
    @repository(MerchantProductDistribusiRepository)
    public merchantProductDistribusiRepository: MerchantProductDistribusiRepository,
    @repository(MerchantProductDocumentRepository)
    public merchantProductDocumentRepository: MerchantProductDocumentRepository,
    @repository(MerchantRepository)
    public merchantRepository: MerchantRepository,
    @repository(MerchantProductRepository)
    public merchantProductRepository: MerchantProductRepository,

    @inject(SecurityBindings.USER)
    public currentUserProfile: UserProfile,
    @repository(UserRepository)
    public userRepository: UserRepository,
  ) { }


  @post('/distribusi-upload/merchant/upload-bulk/bar', {
    responses: {
      200: {
        content: {
          'application/json': {
            schema: {
              type: 'object',
            },
          },
        },
        description: 'Files BAR Uploaded and fields',
      },
    },
  })
  async fileUploadBAR(
    @requestBody.file()
    requestFile: Request,
    @inject(RestBindings.Http.RESPONSE) response: Response,
  ): Promise<object> {
    return new Promise<object>((resolve, reject) => {
      var USER_ID = this.currentUserProfile[securityId];
      var result: any; result = {success: true, message: '', datas: []};

      const storage = multer.diskStorage({
        destination: path.join(__dirname, '../../.non-digipos/document/tmp'),
        // Use the original file name as is
        filename: (req, file, cb) => {
          cb(null, ControllerConfig.onCurrentTime().toString() + "_" + file.originalname);
        },
      });

      const upload = multer({
        storage: storage,
        fileFilter: async (req, file, cb: any) => {
          var ext = path.extname(file.originalname).toLowerCase();
          if (ext !== '.pdf') {
            cb({
              success: false,
              message: 'Only pdf are allowed !'
            }, false);
            return;
          }

          var NameFile = ControllerConfig.onCurrentTime().toString() + "_" + file.originalname.substring(0, file.originalname.length - 4);
          var SplitNameFile = NameFile.split("_");
          var NoBAR = SplitNameFile.length == 2 ? SplitNameFile[1] : SplitNameFile[1];
          var NOBAR_SET = NoBAR.toString().split(".").join("/")

          var InfoDigipos = await this.merchantProductMasterRepository.count({and: [{MPM_NO_BAR: NOBAR_SET}]});
          if (InfoDigipos.count == 0) {
            cb({
              success: false,
              message: 'Data No BAR in file not exities !'
            }, false);
            return;
          }

          cb(null, true);
        }
      }).fields([{name: 'bar_file', maxCount: 1}]);

      upload(requestFile, response, async (errUpload: any) => {
        if (errUpload)
          resolve({success: false, message: errUpload?.message})
        else {

          var progress: any; progress = progressStream({length: 0});
          requestFile.pipe(progress);
          progress.headers = requestFile.headers;

          progress.on('length', function nowIKnowMyLength(actualLength: any) {
            console.log('actualLength: %s', actualLength);
            progress.setLength(actualLength);
          });
          // get the upload progress
          progress.on('progress', (obj: any) => {
            console.log('progress: %s', obj.percentage);
          });

          var files: any; files = requestFile.files;
          var FileBAR = files.bar_file;
          var NameFileOrigin: string; NameFileOrigin = "";
          var NameFile: string; NameFile = "";
          var PathFile: string; PathFile = "";

          if (FileBAR == undefined) {
            resolve({success: true, message: "File upload empty !"});
            return;
          }

          for (var i in FileBAR) {
            var rawFile = FileBAR[i];
            NameFileOrigin = rawFile.originalname;
            NameFile = rawFile.filename.substring(0, rawFile.filename.length - 4);
            PathFile = rawFile.path;
          }

          var SplitNameFile = NameFile.split("_");
          var NoBAR = SplitNameFile.length == 2 ? SplitNameFile[1] : SplitNameFile[1];
          var NOBAR_SET = NoBAR.toString().split(".").join("/");

          const InfoMerchantMaster = await this.merchantProductMasterRepository.findOne({where: {MPM_NO_BAR: NOBAR_SET}});

          var TIME_NAME = ControllerConfig.onCurrentTime().toString();
          var RENAMEFILE = NoBAR + "_" + TIME_NAME + ".pdf";

          if (InfoMerchantMaster != null) {

            try {

              var NEW_PATH = path.join(__dirname, '../../.non-digipos/document/' + InfoMerchantMaster.MPM_ID + '/bar/') + RENAMEFILE;
              var DIR_NEW_PATH = path.join(__dirname, '../../.non-digipos/document/' + InfoMerchantMaster.MPM_ID + '/bar/');
              if (!fs.existsSync(DIR_NEW_PATH)) {
                await new Promise<object>((resolve, reject) => {
                  fs.mkdir(DIR_NEW_PATH, {recursive: true}, (err) => {
                    if (err) {
                      resolve({success: true, err});
                    } else {
                      resolve({success: true, message: "rename success !"});
                    }
                  });
                });
              }

              fs.rename(PathFile, NEW_PATH, async (err) => {
                if (err) {
                  fs.unlink(PathFile, (errorDelete: any) => {
                    if (errorDelete) {
                      console.log(errorDelete);
                    }
                  });
                  resolve({success: false, message: err});
                } else {
                  try {
                    var FindDocInvoice = await this.merchantProductDocumentRepository.findOne({where: {and: [{MPDOC_NAME: RENAMEFILE}, {MPDOC_TYPE: 1}, {MPM_ID: InfoMerchantMaster.MPM_ID}]}});
                    await this.merchantProductDocumentRepository.updateAll({AT_FLAG: 0}, {and: [{MPDOC_TYPE: 1}, {MPM_ID: InfoMerchantMaster.MPM_ID}]});

                    if (FindDocInvoice != null) {
                      await this.merchantProductDocumentRepository.updateById(FindDocInvoice.MPDOC_ID, {
                        MPDOC_NAME: RENAMEFILE,
                        AT_FLAG: 1,
                        MPDOC_TYPE: 1,
                        MPDOC_PATH: NEW_PATH,
                        MPM_ID: InfoMerchantMaster.MPM_ID,
                        START_TIME_RUN: InfoMerchantMaster.START_TIME_RUN,
                        AT_UPDATE: ControllerConfig.onCurrentTime().toString(),
                        AT_WHO: USER_ID
                      });
                    } else {
                      await this.merchantProductDocumentRepository.create({
                        MPDOC_NAME: RENAMEFILE,
                        AT_FLAG: 1,
                        MPDOC_TYPE: 3,
                        MPDOC_PATH: NEW_PATH,
                        MPM_ID: InfoMerchantMaster.MPM_ID,
                        AT_CREATE: ControllerConfig.onCurrentTime().toString(),
                        AT_UPDATE: undefined,
                        START_TIME_RUN: InfoMerchantMaster.START_TIME_RUN,
                        AT_WHO: USER_ID
                      });
                    }

                    await this.merchantProductMasterRepository.updateById(InfoMerchantMaster.MPM_ID, {
                      GENERATE_BAR: 1
                    });

                    resolve({success: true, message: "Success upload bar !"});
                  } catch (error) {
                    fs.unlink(NEW_PATH, (errorDelete: any) => {
                      if (errorDelete) {
                        console.log(errorDelete);
                      }
                    });
                    resolve({success: false, message: error});
                  }
                }
              });

            } catch (error) {
              fs.unlink(PathFile, (errorDelete: any) => {
                if (errorDelete) {
                  console.log(errorDelete);
                }
              });
              resolve({success: false, message: error});
            }

          } else {
            fs.unlink(PathFile, (errorDelete: any) => {
              if (errorDelete) {
                console.log(errorDelete);
              }
            });
            resolve({success: false, message: "Error, failed upload document, no invoice not found !"});
          }

        }
      });
    });
  }


  @post('/distribusi-upload/merchant/upload-bulk/bar-sign', {
    responses: {
      200: {
        content: {
          'application/json': {
            schema: {
              type: 'object',
            },
          },
        },
        description: 'Files BAR SIGN Uploaded and fields',
      },
    },
  })
  async fileUploadBARSign(
    @requestBody.file()
    requestFile: Request,
    @inject(RestBindings.Http.RESPONSE) response: Response,
  ): Promise<object> {
    return new Promise<object>((resolve, reject) => {
      var USER_ID = this.currentUserProfile[securityId];
      var result: any; result = {success: true, message: '', datas: []};

      const storage = multer.diskStorage({
        destination: path.join(__dirname, '../../.non-digipos/document/tmp'),
        // Use the original file name as is
        filename: (req, file, cb) => {
          cb(null, ControllerConfig.onCurrentTime().toString() + "_" + file.originalname);
        },
      });

      const upload = multer({
        storage: storage,
        fileFilter: async (req, file, cb: any) => {
          var ext = path.extname(file.originalname).toLowerCase();
          if (ext !== '.pdf') {
            cb({
              success: false,
              message: 'Only pdf are allowed !'
            }, false);
            return;
          }

          var NameFile = ControllerConfig.onCurrentTime().toString() + "_" + file.originalname.substring(0, file.originalname.length - 4);
          var SplitNameFile = NameFile.split("_");
          var NoBAR = SplitNameFile.length == 2 ? SplitNameFile[1] : SplitNameFile[2];
          var NOBAR_SET = NoBAR.split(".").join("/")

          var InfoDigipos = await this.merchantProductMasterRepository.count({and: [{MPM_NO_BAR: NOBAR_SET}]});
          if (InfoDigipos.count == 0) {
            cb({
              success: false,
              message: 'Data No BAR in file not exities !'
            }, false);
            return;
          }

          cb(null, true);
        }
      }).fields([{name: 'bar_sign_file', maxCount: 1}]);

      upload(requestFile, response, async (errUpload: any) => {
        if (errUpload)
          resolve({success: false, message: errUpload?.message})
        else {

          var progress: any; progress = progressStream({length: 0});
          requestFile.pipe(progress);
          progress.headers = requestFile.headers;

          progress.on('length', function nowIKnowMyLength(actualLength: any) {
            console.log('actualLength: %s', actualLength);
            progress.setLength(actualLength);
          });
          // get the upload progress
          progress.on('progress', (obj: any) => {
            console.log('progress: %s', obj.percentage);
          });

          var files: any; files = requestFile.files;
          var FileFaktur = files.bar_sign_file;
          var NameFileOrigin: string; NameFileOrigin = "";
          var NameFile: string; NameFile = "";
          var PathFile: string; PathFile = "";

          if (FileFaktur == undefined) {
            resolve({success: true, message: "File upload empty !"});
            return;
          }

          for (var i in FileFaktur) {
            var rawFile = FileFaktur[i];
            NameFileOrigin = rawFile.originalname;
            NameFile = rawFile.filename.substring(0, rawFile.filename.length - 4);
            PathFile = rawFile.path;
          }
          var SplitNameFile = NameFile.split("_");
          var NoBAR = SplitNameFile.length == 2 ? SplitNameFile[1] : SplitNameFile[2];
          var NOBAR_SET = NoBAR.split(".").join("/")
          const InfoMerchantMaster = await this.merchantProductMasterRepository.findOne({where: {MPM_NO_BAR: NOBAR_SET}});

          var TIME_NAME = ControllerConfig.onCurrentTime().toString();
          var RENAMEFILE = "SIGN_" + NoBAR + "_" + TIME_NAME + ".pdf";

          if (InfoMerchantMaster != null) {
            if (InfoMerchantMaster.GENERATE_BAR != 0) {

              try {

                var NEW_PATH = path.join(__dirname, '../../.non-digipos/document/' + InfoMerchantMaster.MPM_ID + '/bar-sign/') + RENAMEFILE;
                var DIR_NEW_PATH = path.join(__dirname, '../../.non-digipos/document/' + InfoMerchantMaster.MPM_ID + '/bar-sign/');
                if (!fs.existsSync(DIR_NEW_PATH)) {
                  await new Promise<object>((resolve, reject) => {
                    fs.mkdir(DIR_NEW_PATH, {recursive: true}, (err) => {
                      if (err) {
                        resolve({success: true, err});
                      } else {
                        resolve({success: true, message: "rename success !"});
                      }
                    });
                  });
                }

                fs.rename(PathFile, NEW_PATH, async (err) => {
                  if (err) {
                    fs.unlink(PathFile, (errorDelete: any) => {
                      if (errorDelete) {
                        console.log(errorDelete);
                      }
                    });
                    resolve({success: false, message: err});
                  } else {
                    try {
                      var FindDocInvoice = await this.merchantProductDocumentRepository.findOne({where: {and: [{MPDOC_NAME: RENAMEFILE}, {MPDOC_TYPE: 2}, {MPM_ID: InfoMerchantMaster.MPM_ID}]}});
                      await this.merchantProductDocumentRepository.updateAll({AT_FLAG: 0}, {and: [{MPDOC_TYPE: 2}, {MPM_ID: InfoMerchantMaster.MPM_ID}]});

                      if (FindDocInvoice != null) {
                        await this.merchantProductDocumentRepository.updateById(FindDocInvoice.MPDOC_ID, {
                          MPDOC_NAME: RENAMEFILE,
                          AT_FLAG: 1,
                          MPDOC_TYPE: 2,
                          MPDOC_PATH: NEW_PATH,
                          MPM_ID: InfoMerchantMaster.MPM_ID,
                          START_TIME_RUN: InfoMerchantMaster.START_TIME_RUN,
                          AT_UPDATE: ControllerConfig.onCurrentTime().toString(),
                          AT_WHO: USER_ID
                        });
                      } else {
                        await this.merchantProductDocumentRepository.create({
                          MPDOC_NAME: RENAMEFILE,
                          AT_FLAG: 1,
                          MPDOC_TYPE: 2,
                          MPDOC_PATH: NEW_PATH,
                          MPM_ID: InfoMerchantMaster.MPM_ID,
                          AT_CREATE: ControllerConfig.onCurrentTime().toString(),
                          AT_UPDATE: undefined,
                          START_TIME_RUN: InfoMerchantMaster.START_TIME_RUN,
                          AT_WHO: USER_ID
                        });
                      }

                      await this.merchantProductMasterRepository.updateById(InfoMerchantMaster.MPM_ID, {
                        GENERATE_BAR_SIGN: 1
                      });

                      resolve({success: true, message: "Success upload bar sign !"});
                    } catch (error) {
                      fs.unlink(NEW_PATH, (errorDelete: any) => {
                        if (errorDelete) {
                          console.log(errorDelete);
                        }
                      });
                      resolve({success: false, message: error});
                    }
                  }
                });

              } catch (error) {
                fs.unlink(PathFile, (errorDelete: any) => {
                  if (errorDelete) {
                    console.log(errorDelete);
                  }
                });
                resolve({success: false, message: error});
              }

            } else {
              fs.unlink(PathFile, (errorDelete: any) => {
                if (errorDelete) {
                  console.log(errorDelete);
                }
              });
              resolve({success: false, message: "Error, failed upload document, please generate Document BAR  !"});
            }
          } else {
            fs.unlink(PathFile, (errorDelete: any) => {
              if (errorDelete) {
                console.log(errorDelete);
              }
            });
            resolve({success: false, message: "Error, failed upload document, no invoice not found !"});
          }

        }
      });
    });
  }


  @post('/distribusi-upload/merchant/upload-bulk/invoice', {
    responses: {
      200: {
        content: {
          'application/json': {
            schema: {
              type: 'object',
            },
          },
        },
        description: 'Files Invoice Uploaded and fields',
      },
    },
  })
  async fileUploadInvoice(
    @requestBody.file()
    requestFile: Request,
    @inject(RestBindings.Http.RESPONSE) response: Response,
  ): Promise<object> {
    return new Promise<object>((resolve, reject) => {
      var USER_ID = this.currentUserProfile[securityId];
      var result: any; result = {success: true, message: '', datas: []};

      const storage = multer.diskStorage({
        destination: path.join(__dirname, '../../.non-digipos/document/tmp'),
        // Use the original file name as is
        filename: (req, file, cb) => {
          cb(null, ControllerConfig.onCurrentTime().toString() + "_" + file.originalname);
        },
      });

      const upload = multer({
        storage: storage,
        fileFilter: async (req, file, cb: any) => {
          var ext = path.extname(file.originalname).toLowerCase();
          if (ext !== '.pdf') {
            cb({
              success: false,
              message: 'Only pdf are allowed !'
            }, false);
            return;
          }

          var NameFile = ControllerConfig.onCurrentTime().toString() + "_" + file.originalname.substring(0, file.originalname.length - 4);
          var SplitNameFile = NameFile.split("_");
          var NoInv = SplitNameFile.length == 2 ? SplitNameFile[1] : SplitNameFile[1];
          var InfoDigipos = await this.merchantProductMasterRepository.count({and: [{MPM_NO_INVOICE: NoInv}]});
          if (InfoDigipos.count == 0) {
            cb({
              success: false,
              message: 'Data No Invoice in file not exities !'
            }, false);
            return;
          }

          cb(null, true);
        }
      }).fields([{name: 'invoice_file', maxCount: 1}]);

      upload(requestFile, response, async (errUpload: any) => {
        if (errUpload)
          resolve({success: false, message: errUpload?.message})
        else {

          var progress: any; progress = progressStream({length: 0});
          requestFile.pipe(progress);
          progress.headers = requestFile.headers;

          progress.on('length', function nowIKnowMyLength(actualLength: any) {
            console.log('actualLength: %s', actualLength);
            progress.setLength(actualLength);
          });
          // get the upload progress
          progress.on('progress', (obj: any) => {
            console.log('progress: %s', obj.percentage);
          });

          var files: any; files = requestFile.files;
          var FileFaktur = files.invoice_file;
          var NameFileOrigin: string; NameFileOrigin = "";
          var NameFile: string; NameFile = "";
          var PathFile: string; PathFile = "";

          if (FileFaktur == undefined) {
            resolve({success: true, message: "File upload empty !"});
            return;
          }

          for (var i in FileFaktur) {
            var rawFile = FileFaktur[i];
            NameFileOrigin = rawFile.originalname;
            NameFile = rawFile.filename.substring(0, rawFile.filename.length - 4);
            PathFile = rawFile.path;
          }
          var SplitNameFile = NameFile.split("_");
          var NoInv = SplitNameFile.length == 2 ? SplitNameFile[1] : SplitNameFile[1];
          const InfoMerchantMaster = await this.merchantProductMasterRepository.findOne({where: {MPM_NO_INVOICE: NoInv}});

          var TIME_NAME = ControllerConfig.onCurrentTime().toString();
          var RENAMEFILE = NoInv + "_" + TIME_NAME + ".pdf";

          if (InfoMerchantMaster != null) {
            if (InfoMerchantMaster.GENERATE_BAR_SIGN != 0) {

              try {

                var NEW_PATH = path.join(__dirname, '../../.non-digipos/document/' + InfoMerchantMaster.MPM_ID + '/invoice/') + RENAMEFILE;
                var DIR_NEW_PATH = path.join(__dirname, '../../.non-digipos/document/' + InfoMerchantMaster.MPM_ID + '/invoice/');
                if (!fs.existsSync(DIR_NEW_PATH)) {
                  await new Promise<object>((resolve, reject) => {
                    fs.mkdir(DIR_NEW_PATH, {recursive: true}, (err) => {
                      if (err) {
                        resolve({success: true, err});
                      } else {
                        resolve({success: true, message: "rename success !"});
                      }
                    });
                  });
                }

                fs.rename(PathFile, NEW_PATH, async (err) => {
                  if (err) {
                    fs.unlink(PathFile, (errorDelete: any) => {
                      if (errorDelete) {
                        console.log(errorDelete);
                      }
                    });
                    resolve({success: false, message: err});
                  } else {
                    try {
                      var FindDocInvoice = await this.merchantProductDocumentRepository.findOne({where: {and: [{MPDOC_NAME: RENAMEFILE}, {MPDOC_TYPE: 3}, {MPM_ID: InfoMerchantMaster.MPM_ID}]}});
                      await this.merchantProductDocumentRepository.updateAll({AT_FLAG: 0}, {and: [{MPDOC_TYPE: 3}, {MPM_ID: InfoMerchantMaster.MPM_ID}]});

                      if (FindDocInvoice != null) {
                        await this.merchantProductDocumentRepository.updateById(FindDocInvoice.MPDOC_ID, {
                          MPDOC_NAME: RENAMEFILE,
                          AT_FLAG: 1,
                          MPDOC_TYPE: 3,
                          MPDOC_PATH: NEW_PATH,
                          MPM_ID: InfoMerchantMaster.MPM_ID,
                          START_TIME_RUN: InfoMerchantMaster.START_TIME_RUN,
                          AT_UPDATE: ControllerConfig.onCurrentTime().toString(),
                          AT_WHO: USER_ID
                        });
                      } else {
                        await this.merchantProductDocumentRepository.create({
                          MPDOC_NAME: RENAMEFILE,
                          AT_FLAG: 1,
                          MPDOC_TYPE: 3,
                          MPDOC_PATH: NEW_PATH,
                          MPM_ID: InfoMerchantMaster.MPM_ID,
                          AT_CREATE: ControllerConfig.onCurrentTime().toString(),
                          AT_UPDATE: undefined,
                          START_TIME_RUN: InfoMerchantMaster.START_TIME_RUN,
                          AT_WHO: USER_ID
                        });
                      }

                      await this.merchantProductMasterRepository.updateById(InfoMerchantMaster.MPM_ID, {
                        GENERATE_INVOICE: 1
                      });

                      resolve({success: true, message: "Success upload invoice !"});
                    } catch (error) {
                      fs.unlink(NEW_PATH, (errorDelete: any) => {
                        if (errorDelete) {
                          console.log(errorDelete);
                        }
                      });
                      resolve({success: false, message: error});
                    }
                  }
                });

              } catch (error) {
                fs.unlink(PathFile, (errorDelete: any) => {
                  if (errorDelete) {
                    console.log(errorDelete);
                  }
                });
                resolve({success: false, message: error});
              }

            } else {
              fs.unlink(PathFile, (errorDelete: any) => {
                if (errorDelete) {
                  console.log(errorDelete);
                }
              });
              resolve({success: false, message: "Error, failed upload document, please generate Document BAR sIGN !"});
            }
          } else {
            fs.unlink(PathFile, (errorDelete: any) => {
              if (errorDelete) {
                console.log(errorDelete);
              }
            });
            resolve({success: false, message: "Error, failed upload document, no invoice not found !"});
          }

        }
      });
    });
  }

  @post('/distribusi-upload/merchant/upload-bulk/invoice-sign', {
    responses: {
      200: {
        content: {
          'application/json': {
            schema: {
              type: 'object',
            },
          },
        },
        description: 'Files Invoice Sign Uploaded and fields',
      },
    },
  })
  async fileUploadInvoiceSign(
    @requestBody.file()
    requestFile: Request,
    @inject(RestBindings.Http.RESPONSE) response: Response,
  ): Promise<object> {
    return new Promise<object>((resolve, reject) => {
      var USER_ID = this.currentUserProfile[securityId];
      var result: any; result = {success: true, message: '', datas: []};

      const storage = multer.diskStorage({
        destination: path.join(__dirname, '../../.non-digipos/document/tmp'),
        // Use the original file name as is
        filename: (req, file, cb) => {
          cb(null, ControllerConfig.onCurrentTime().toString() + "_" + file.originalname);
        },
      });

      const upload = multer({
        storage: storage,
        fileFilter: async (req, file, cb: any) => {
          var ext = path.extname(file.originalname).toLowerCase();
          if (ext !== '.pdf') {
            cb({
              success: false,
              message: 'Only pdf are allowed !'
            }, false);
            return;
          }

          var NameFile = file.originalname.substring(0, file.originalname.length - 4);
          var SplitNameFile = NameFile.split("_");
          var NoInv = SplitNameFile.length == 2 ? SplitNameFile[1] : SplitNameFile[2];
          var InfoDigipos = await this.merchantProductMasterRepository.count({and: [{MPM_NO_INVOICE: NoInv}]});
          if (InfoDigipos.count == 0) {
            cb({
              success: false,
              message: 'Data No Invoice in file not exities !'
            }, false);
            return;
          }

          cb(null, true);
        }
      }).fields([{name: 'invoice_sign_file', maxCount: 1}]);

      upload(requestFile, response, async (errUpload: any) => {
        if (errUpload)
          resolve({success: false, message: errUpload?.message})
        else {

          var progress: any; progress = progressStream({length: 0});
          requestFile.pipe(progress);
          progress.headers = requestFile.headers;

          progress.on('length', function nowIKnowMyLength(actualLength: any) {
            console.log('actualLength: %s', actualLength);
            progress.setLength(actualLength);
          });
          // get the upload progress
          progress.on('progress', (obj: any) => {
            console.log('progress: %s', obj.percentage);
          });

          var files: any; files = requestFile.files;
          var FileFaktur = files.invoice_sign_file;
          var NameFileOrigin: string; NameFileOrigin = "";
          var NameFile: string; NameFile = "";
          var PathFile: string; PathFile = "";

          if (FileFaktur == undefined) {
            resolve({success: true, message: "File upload empty !"});
            return;
          }

          for (var i in FileFaktur) {
            var rawFile = FileFaktur[i];
            NameFileOrigin = rawFile.originalname;
            NameFile = rawFile.filename.substring(0, rawFile.filename.length - 4);
            PathFile = rawFile.path;
          }
          var SplitNameFile = NameFile.split("_");
          var NoInv = SplitNameFile.length == 2 ? SplitNameFile[1] : SplitNameFile[2];
          const InfoMerchantMaster = await this.merchantProductMasterRepository.findOne({where: {MPM_NO_INVOICE: NoInv}});

          var TIME_NAME = ControllerConfig.onCurrentTime().toString();
          var RENAMEFILE = "SIGN_" + NoInv + "_" + TIME_NAME + ".pdf";

          if (InfoMerchantMaster != null) {
            if (InfoMerchantMaster.GENERATE_INVOICE != 0) {

              try {

                var NEW_PATH = path.join(__dirname, '../../.non-digipos/document/' + InfoMerchantMaster.MPM_ID + '/invoice-sign/') + RENAMEFILE;
                var DIR_NEW_PATH = path.join(__dirname, '../../.non-digipos/document/' + InfoMerchantMaster.MPM_ID + '/invoice-sign/');
                if (!fs.existsSync(DIR_NEW_PATH)) {
                  await new Promise<object>((resolve, reject) => {
                    fs.mkdir(DIR_NEW_PATH, {recursive: true}, (err) => {
                      if (err) {
                        resolve({success: true, err});
                      } else {
                        resolve({success: true, message: "rename success !"});
                      }
                    });
                  });
                }

                fs.rename(PathFile, NEW_PATH, async (err) => {
                  if (err) {
                    fs.unlink(PathFile, (errorDelete: any) => {
                      if (errorDelete) {
                        console.log(errorDelete);
                      }
                    });
                    resolve({success: false, message: err});
                  } else {
                    try {
                      var FindDocInvoice = await this.merchantProductDocumentRepository.findOne({where: {and: [{MPDOC_NAME: RENAMEFILE}, {MPDOC_TYPE: 4}]}});

                      await this.merchantProductDocumentRepository.updateAll({AT_FLAG: 0}, {and: [{MPDOC_TYPE: 4}, {MPM_ID: InfoMerchantMaster.MPM_ID}]});
                      if (FindDocInvoice != null) {
                        await this.merchantProductDocumentRepository.updateById(FindDocInvoice.MPDOC_ID, {
                          MPDOC_NAME: RENAMEFILE,
                          AT_FLAG: 1,
                          MPDOC_TYPE: 4,
                          MPDOC_PATH: NEW_PATH,
                          MPM_ID: InfoMerchantMaster.MPM_ID,
                          START_TIME_RUN: InfoMerchantMaster.START_TIME_RUN,
                          AT_UPDATE: ControllerConfig.onCurrentTime().toString(),
                          AT_WHO: USER_ID
                        });
                      } else {
                        await this.merchantProductDocumentRepository.create({
                          MPDOC_NAME: RENAMEFILE,
                          AT_FLAG: 1,
                          MPDOC_TYPE: 4,
                          MPDOC_PATH: NEW_PATH,
                          MPM_ID: InfoMerchantMaster.MPM_ID,
                          AT_CREATE: ControllerConfig.onCurrentTime().toString(),
                          AT_UPDATE: undefined,
                          START_TIME_RUN: InfoMerchantMaster.START_TIME_RUN,
                          AT_WHO: USER_ID
                        });
                      }

                      await this.merchantProductMasterRepository.updateById(InfoMerchantMaster.MPM_ID, {
                        GENERATE_INVOICE_SIGN: 1
                      });

                      resolve({success: true, message: "Success upload invoice sign !"});
                    } catch (error) {
                      fs.unlink(NEW_PATH, (errorDelete: any) => {
                        if (errorDelete) {
                          console.log(errorDelete);
                        }
                      });
                      resolve({success: false, message: error});
                    }
                  }
                });

              } catch (error) {
                fs.unlink(PathFile, (errorDelete: any) => {
                  if (errorDelete) {
                    console.log(errorDelete);
                  }
                });
                resolve({success: false, message: error});
              }

            } else {
              fs.unlink(PathFile, (errorDelete: any) => {
                if (errorDelete) {
                  console.log(errorDelete);
                }
              });
              resolve({success: false, message: "Error, failed upload document, plesase generate Document Invoice Before sign !"});
            }
          } else {
            fs.unlink(PathFile, (errorDelete: any) => {
              if (errorDelete) {
                console.log(errorDelete);
              }
            });
            resolve({success: false, message: "Error, failed upload document, no invoice not found !"});
          }

        }
      });
    });
  }


  @post('/distribusi-upload/merchant/upload-bulk/faktur-pajak', {
    responses: {
      200: {
        content: {
          'application/json': {
            schema: {
              type: 'object',
            },
          },
        },
        description: 'Files Faktur pajak Uploaded and fields',
      },
    },
  })
  async fileUploadFakturPajak(
    @requestBody.file()
    requestFile: Request,
    @inject(RestBindings.Http.RESPONSE) response: Response,
  ): Promise<object> {
    return new Promise<object>((resolve, reject) => {
      var USER_ID = this.currentUserProfile[securityId];
      var result: any; result = {success: true, message: '', datas: []};

      const storage = multer.diskStorage({
        destination: path.join(__dirname, '../../.non-digipos/document/tmp'),
        // Use the original file name as is
        filename: (req, file, cb) => {
          cb(null, ControllerConfig.onCurrentTime().toString() + "_" + file.originalname);
        },
      });

      const upload = multer({
        storage: storage,
        fileFilter: async (req, file, cb: any) => {
          var ext = path.extname(file.originalname).toLowerCase();
          if (ext !== '.pdf') {
            cb({
              success: false,
              message: 'Only pdf are allowed !'
            }, false);
            return;
          }

          var NameFile = ControllerConfig.onCurrentTime().toString() + "_" + file.originalname.substring(0, file.originalname.length - 4);
          var SplitNameFile = NameFile.split("_")[1].split("-");
          var NoFaktur = SplitNameFile[1] == undefined ? "0" : SplitNameFile[1];
          var dataSearchNoFaktur = await this.merchantProductMasterRepository.execute('SELECT * , REPLACE(REPLACE(MPM_NO_FAKTUR, ".", ""),"-","") AS VAT_FILE_NUMBER FROM `MERCHANT_PRODUCT_MASTER` HAVING VAT_FILE_NUMBER = "' + NoFaktur + '"');
          var findNoFaktur: string; findNoFaktur = "";
          for (var j in dataSearchNoFaktur) {
            findNoFaktur = dataSearchNoFaktur[j]["MPM_NO_FAKTUR"];
          }

          var InfoDigipos = await this.merchantProductMasterRepository.count({and: [{MPM_NO_FAKTUR: findNoFaktur}]});
          if (InfoDigipos.count == 0) {
            cb({
              success: false,
              message: 'Data No Faktur in file not exities !'
            }, false);
            return;
          }

          cb(null, true);
        }
      }).fields([{name: 'faktur_pajak_file', maxCount: 1}]);

      upload(requestFile, response, async (errUpload: any) => {
        if (errUpload)
          resolve({success: false, message: errUpload?.message})
        else {

          var progress: any; progress = progressStream({length: 0});
          requestFile.pipe(progress);
          progress.headers = requestFile.headers;

          progress.on('length', function nowIKnowMyLength(actualLength: any) {
            console.log('actualLength: %s', actualLength);
            progress.setLength(actualLength);
          });
          // get the upload progress
          progress.on('progress', (obj: any) => {
            console.log('progress: %s', obj.percentage);
          });

          var files: any; files = requestFile.files;
          var FileFaktur = files.faktur_pajak_file;
          var NameFileOrigin: string; NameFileOrigin = "";
          var NameFile: string; NameFile = "";
          var PathFile: string; PathFile = "";

          if (FileFaktur == undefined) {
            resolve({success: true, message: "File upload empty !"});
            return;
          }

          for (var i in FileFaktur) {
            var rawFile = FileFaktur[i];
            NameFileOrigin = rawFile.originalname;
            NameFile = rawFile.filename.substring(0, rawFile.filename.length - 4);
            PathFile = rawFile.path;
          }

          var SplitNameFile = NameFile.split("_")[1].split("-");
          var NoFaktur = SplitNameFile[1] == undefined ? "0" : SplitNameFile[1];
          var dataSearchNoFaktur = await this.merchantProductMasterRepository.execute('SELECT * , REPLACE(REPLACE(MPM_NO_FAKTUR, ".", ""),"-","") AS VAT_FILE_NUMBER FROM `MERCHANT_PRODUCT_MASTER` HAVING VAT_FILE_NUMBER = "' + NoFaktur + '"');
          var findNoFaktur: string; findNoFaktur = "";
          for (var j in dataSearchNoFaktur) {
            findNoFaktur = dataSearchNoFaktur[j]["MPM_NO_FAKTUR"];
          }


          const InfoMerchantMaster = await this.merchantProductMasterRepository.findOne({where: {MPM_NO_FAKTUR: findNoFaktur}});
          var TIME_NAME = ControllerConfig.onCurrentTime().toString();
          var RENAMEFILE = "FAKTUR_" + findNoFaktur + "_" + TIME_NAME + ".pdf";

          if (InfoMerchantMaster != null) {
            if (InfoMerchantMaster.GENERATE_INVOICE != 0) {

              try {

                var NEW_PATH = path.join(__dirname, '../../.non-digipos/document/' + InfoMerchantMaster.MPM_ID + '/faktur-pajak/') + RENAMEFILE;
                var DIR_NEW_PATH = path.join(__dirname, '../../.non-digipos/document/' + InfoMerchantMaster.MPM_ID + '/faktur-pajak/');
                if (!fs.existsSync(DIR_NEW_PATH)) {
                  await new Promise<object>((resolve, reject) => {
                    fs.mkdir(DIR_NEW_PATH, {recursive: true}, (err) => {
                      if (err) {
                        resolve({success: true, err});
                      } else {
                        resolve({success: true, message: "rename success !"});
                      }
                    });
                  });
                }

                fs.rename(PathFile, NEW_PATH, async (err) => {
                  if (err) {
                    fs.unlink(PathFile, (errorDelete: any) => {
                      if (errorDelete) {
                        console.log(errorDelete);
                      }
                    });
                    resolve({success: false, message: err});
                  } else {
                    try {
                      var FindDocFaktur = await this.merchantProductDocumentRepository.findOne({where: {and: [{MPDOC_NAME: RENAMEFILE}, {MPDOC_TYPE: 5}]}});

                      await this.merchantProductDocumentRepository.updateAll({AT_FLAG: 0}, {and: [{MPDOC_TYPE: 5}, {MPM_ID: InfoMerchantMaster.MPM_ID}]});
                      if (FindDocFaktur != null) {
                        await this.merchantProductDocumentRepository.updateById(FindDocFaktur.MPDOC_ID, {
                          MPDOC_NAME: RENAMEFILE,
                          AT_FLAG: 1,
                          MPDOC_TYPE: 5,
                          MPDOC_PATH: NEW_PATH,
                          MPM_ID: InfoMerchantMaster.MPM_ID,
                          START_TIME_RUN: InfoMerchantMaster.START_TIME_RUN,
                          AT_UPDATE: ControllerConfig.onCurrentTime().toString(),
                          AT_WHO: USER_ID
                        });
                      } else {
                        await this.merchantProductDocumentRepository.create({
                          MPDOC_NAME: RENAMEFILE,
                          AT_FLAG: 1,
                          MPDOC_TYPE: 5,
                          MPDOC_PATH: NEW_PATH,
                          MPM_ID: InfoMerchantMaster.MPM_ID,
                          AT_CREATE: ControllerConfig.onCurrentTime().toString(),
                          AT_UPDATE: undefined,
                          START_TIME_RUN: InfoMerchantMaster.START_TIME_RUN,
                          AT_WHO: USER_ID
                        });
                      }

                      await this.merchantProductMasterRepository.updateById(InfoMerchantMaster.MPM_ID, {
                        GENERATE_FAKTUR_PAJAK: 1
                      });

                      resolve({success: true, message: "Success upload faktur pajak !"});
                    } catch (error) {
                      fs.unlink(NEW_PATH, (errorDelete: any) => {
                        if (errorDelete) {
                          console.log(errorDelete);
                        }
                      });
                      resolve({success: false, message: error});
                    }
                  }
                });

              } catch (error) {
                fs.unlink(PathFile, (errorDelete: any) => {
                  if (errorDelete) {
                    console.log(errorDelete);
                  }
                });
                resolve({success: false, message: error});
              }

            } else {
              fs.unlink(PathFile, (errorDelete: any) => {
                if (errorDelete) {
                  console.log(errorDelete);
                }
              });
              resolve({success: false, message: "Error, failed upload document, plesase generate Document Invoice Before sign !"});
            }
          } else {
            fs.unlink(PathFile, (errorDelete: any) => {
              if (errorDelete) {
                console.log(errorDelete);
              }
            });
            resolve({success: false, message: "Error, failed upload document, no faktur not found !"});
          }

        }
      });
    });
  }


  @post('/distribusi-upload/merchant/upload-bulk/bukti-potong', {
    responses: {
      200: {
        content: {
          'application/json': {
            schema: {
              type: 'object',
            },
          },
        },
        description: 'Files Bukti Potong Uploaded and fields',
      },
    },
  })
  async fileUploadBuktiPotong(
    @requestBody.file()
    requestFile: Request,
    @inject(RestBindings.Http.RESPONSE) response: Response,
  ): Promise<object> {
    return new Promise<object>((resolve, reject) => {
      var USER_ID = this.currentUserProfile[securityId];
      var result: any; result = {success: true, message: '', datas: []};

      const storage = multer.diskStorage({
        destination: path.join(__dirname, '../../.non-digipos/document/tmp'),
        // Use the original file name as is
        filename: (req, file, cb) => {
          cb(null, ControllerConfig.onCurrentTime().toString() + "_" + file.originalname);
        },
      });

      const upload = multer({
        storage: storage,
        fileFilter: async (req, file, cb: any) => {
          var ext = path.extname(file.originalname).toLowerCase();
          if (ext !== '.pdf') {
            cb({
              success: false,
              message: 'Only pdf are allowed !'
            }, false);
            return;
          }

          var NameFile = ControllerConfig.onCurrentTime().toString() + "_" + file.originalname.substring(0, file.originalname.length - 4);
          var SplitNameFile = NameFile.split("_");
          var PERIODE_FILE = SplitNameFile[1] == undefined ? "0" : SplitNameFile[1];
          var NPWP_FILE = SplitNameFile[3] == undefined ? "0" : SplitNameFile[3];

          var dataSearchNoNPWP = await this.merchantRepository.execute('SELECT * , REPLACE(REPLACE(M_NPWP, ".", ""),"-","") AS VAT_FILE_NUMBER FROM `MERCHANT` HAVING VAT_FILE_NUMBER = "' + NPWP_FILE + '"');
          var findNoNPWP: string; findNoNPWP = "";
          var MERCHANT: any; MERCHANT = {};
          for (var j in dataSearchNoNPWP) {
            findNoNPWP = dataSearchNoNPWP[j]["M_NPWP"];
            MERCHANT = dataSearchNoNPWP[j];
          }

          if (findNoNPWP == "") {
            cb({
              success: false,
              message: 'Data NPWP not avaliable in merchant !'
            }, false);
            return;
          }

          var InfoDigipos = await this.merchantProductMasterRepository.count({and: [{M_ID: MERCHANT.M_ID}, {PERIODE: PERIODE_FILE}]});
          if (InfoDigipos.count != 1) {
            if (InfoDigipos.count == 0) {
              cb({
                success: false,
                message: 'Data transaksi not found !'
              }, false);
              return;
            } else {
              cb({
                success: false,
                message: 'Data transaksi more than one please upload manual !'
              }, false);
              return;
            }
          }

          cb(null, true);
        }
      }).fields([{name: 'bukpot_file', maxCount: 1}]);

      upload(requestFile, response, async (errUpload: any) => {
        if (errUpload)
          resolve({success: false, message: errUpload?.message})
        else {

          var progress: any; progress = progressStream({length: 0});
          requestFile.pipe(progress);
          progress.headers = requestFile.headers;

          progress.on('length', function nowIKnowMyLength(actualLength: any) {
            console.log('actualLength: %s', actualLength);
            progress.setLength(actualLength);
          });
          // get the upload progress
          progress.on('progress', (obj: any) => {
            console.log('progress: %s', obj.percentage);
          });

          var files: any; files = requestFile.files;
          var FileFaktur = files.bukpot_file;
          var NameFileOrigin: string; NameFileOrigin = "";
          var NameFile: string; NameFile = "";
          var PathFile: string; PathFile = "";


          if (FileFaktur == undefined) {
            resolve({success: true, message: "File upload empty !"});
            return;
          }

          for (var i in FileFaktur) {
            var rawFile = FileFaktur[i];
            NameFileOrigin = rawFile.originalname;
            NameFile = rawFile.filename.substring(0, rawFile.filename.length - 4);
            PathFile = rawFile.path;
          }

          var SplitNameFile = NameFile.split("_");
          var PERIODE_FILE = SplitNameFile[1] == undefined ? "0" : SplitNameFile[1];
          var NPWP_FILE = SplitNameFile[3] == undefined ? "0" : SplitNameFile[3];

          var dataSearchNoNPWP = await this.merchantRepository.execute('SELECT * , REPLACE(REPLACE(M_NPWP, ".", ""),"-","") AS VAT_FILE_NUMBER FROM `MERCHANT` HAVING VAT_FILE_NUMBER = "' + NPWP_FILE + '"');
          var findNoNPWP: string; findNoNPWP = "";
          var MERCHANT: any; MERCHANT = {};
          for (var j in dataSearchNoNPWP) {
            findNoNPWP = dataSearchNoNPWP[j]["M_NPWP"];
            MERCHANT = dataSearchNoNPWP[j];
          }

          if (findNoNPWP == "") {
            resolve({success: true, message: "Data NPWP not avaliable in merchant !"});
            return;
          }


          const InfoMerchantMaster = await this.merchantProductMasterRepository.findOne({where: {and: [{M_ID: MERCHANT.M_ID}, {PERIODE: PERIODE_FILE}]}});
          var TIME_NAME = ControllerConfig.onCurrentTime().toString();
          var RENAMEFILE = PERIODE_FILE + "_" + NPWP_FILE + "_FAKTUR_" + TIME_NAME + ".pdf";

          if (InfoMerchantMaster != null) {
            if (InfoMerchantMaster.GENERATE_INVOICE != 0) {

              try {

                var NEW_PATH = path.join(__dirname, '../../.non-digipos/document/' + InfoMerchantMaster.MPM_ID + '/bukti-potong/') + RENAMEFILE;
                var DIR_NEW_PATH = path.join(__dirname, '../../.non-digipos/document/' + InfoMerchantMaster.MPM_ID + '/bukti-potong/');
                if (!fs.existsSync(DIR_NEW_PATH)) {
                  await new Promise<object>((resolve, reject) => {
                    fs.mkdir(DIR_NEW_PATH, {recursive: true}, (err) => {
                      if (err) {
                        resolve({success: true, err});
                      } else {
                        resolve({success: true, message: "rename success !"});
                      }
                    });
                  });
                }

                fs.rename(PathFile, NEW_PATH, async (err) => {
                  if (err) {
                    fs.unlink(PathFile, (errorDelete: any) => {
                      if (errorDelete) {
                        console.log(errorDelete);
                      }
                    });
                    resolve({success: false, message: err});
                  } else {
                    try {
                      var FindDocFaktur = await this.merchantProductDocumentRepository.findOne({where: {and: [{MPDOC_NAME: RENAMEFILE}, {MPDOC_TYPE: 6}]}});

                      await this.merchantProductDocumentRepository.updateAll({AT_FLAG: 0}, {and: [{MPDOC_TYPE: 6}, {MPM_ID: InfoMerchantMaster.MPM_ID}]});
                      if (FindDocFaktur != null) {
                        await this.merchantProductDocumentRepository.updateById(FindDocFaktur.MPDOC_ID, {
                          MPDOC_NAME: RENAMEFILE,
                          AT_FLAG: 1,
                          MPDOC_TYPE: 6,
                          MPDOC_PATH: NEW_PATH,
                          MPM_ID: InfoMerchantMaster.MPM_ID,
                          START_TIME_RUN: InfoMerchantMaster.START_TIME_RUN,
                          AT_UPDATE: ControllerConfig.onCurrentTime().toString(),
                          AT_WHO: USER_ID
                        });
                      } else {
                        await this.merchantProductDocumentRepository.create({
                          MPDOC_NAME: RENAMEFILE,
                          AT_FLAG: 1,
                          MPDOC_TYPE: 6,
                          MPDOC_PATH: NEW_PATH,
                          MPM_ID: InfoMerchantMaster.MPM_ID,
                          AT_CREATE: ControllerConfig.onCurrentTime().toString(),
                          AT_UPDATE: undefined,
                          START_TIME_RUN: InfoMerchantMaster.START_TIME_RUN,
                          AT_WHO: USER_ID
                        });
                      }

                      await this.merchantProductMasterRepository.updateById(InfoMerchantMaster.MPM_ID, {
                        GENERATE_BUKPOT: 1
                      });

                      resolve({success: true, message: "Success upload bukti potong !"});
                    } catch (error) {
                      fs.unlink(NEW_PATH, (errorDelete: any) => {
                        if (errorDelete) {
                          console.log(errorDelete);
                        }
                      });
                      resolve({success: false, message: error});
                    }
                  }
                });

              } catch (error) {
                fs.unlink(PathFile, (errorDelete: any) => {
                  if (errorDelete) {
                    console.log(errorDelete);
                  }
                });
                resolve({success: false, message: error});
              }

            } else {
              fs.unlink(PathFile, (errorDelete: any) => {
                if (errorDelete) {
                  console.log(errorDelete);
                }
              });
              resolve({success: false, message: "Error, failed upload document, plesase generate Document Invoice Before sign !"});
            }
          } else {
            fs.unlink(PathFile, (errorDelete: any) => {
              if (errorDelete) {
                console.log(errorDelete);
              }
            });
            resolve({success: false, message: "Error, failed upload document, not found transaksi !"});
          }

        }
      });
    });
  }



  @post('/distribusi-upload/merchant/upload/{id}/{type}', {
    responses: {
      200: {
        content: {
          'application/json': {
            schema: {
              type: 'object',
            },
          },
        },
        description: 'Files BAR Uploaded and fields',
      },
    },
  })
  async fileUploadDocumentSingle(
    @requestBody.file()
    requestFile: Request,
    @inject(RestBindings.Http.RESPONSE) response: Response,
    @param.path.string('id') id: string,
    @param.path.string('type') type: string,
  ): Promise<object> {
    return new Promise<object>((resolve, reject) => {
      var USER_ID = this.currentUserProfile[securityId];
      var result: any; result = {success: true, message: '', datas: []};

      const storage = multer.diskStorage({
        destination: path.join(__dirname, '../../.non-digipos/document/tmp'),
        // Use the original file name as is
        filename: (req, file, cb) => {
          cb(null, ControllerConfig.onCurrentTime().toString() + "_" + file.originalname);
        },
      });

      const upload = multer({
        storage: storage,
        fileFilter: async (req, file, cb: any) => {
          var ext = path.extname(file.originalname).toLowerCase();
          if (ext !== '.pdf') {
            cb({
              success: false,
              message: 'Only pdf are allowed !'
            }, false);
            return;
          }

          var InfoDigipos = await this.merchantProductMasterRepository.count({and: [{MPM_ID: id}]});
          if (InfoDigipos.count == 0) {
            cb({
              success: false,
              message: 'Data transaction not found !'
            }, false);
            return;
          }

          cb(null, true);
        }
      }).fields([{name: 'document', maxCount: 1}]);

      upload(requestFile, response, async (errUpload: any) => {
        if (errUpload)
          resolve({success: false, message: errUpload?.message})
        else {

          var progress: any; progress = progressStream({length: 0});
          requestFile.pipe(progress);
          progress.headers = requestFile.headers;

          progress.on('length', function nowIKnowMyLength(actualLength: any) {
            console.log('actualLength: %s', actualLength);
            progress.setLength(actualLength);
          });
          // get the upload progress
          progress.on('progress', (obj: any) => {
            console.log('progress: %s', obj.percentage);
          });

          var files: any; files = requestFile.files;
          var FileBAR = files.document;
          var NameFileOrigin: string; NameFileOrigin = "";
          var NameFile: string; NameFile = "";
          var PathFile: string; PathFile = "";
          for (var i in FileBAR) {
            var rawFile = FileBAR[i];
            NameFileOrigin = rawFile.originalname;
            NameFile = rawFile.filename.substring(0, rawFile.filename.length - 4);
            PathFile = rawFile.path;
          }

          if (FileBAR == undefined) {
            resolve({success: true, message: "File upload empty !"});
            return;
          }


          const InfoMerchantMaster = await this.merchantProductMasterRepository.findOne({where: {MPM_ID: id}});
          var TIME_NAME = ControllerConfig.onCurrentTime().toString();
          var RENAMEFILE = "";
          var MESSAGE = "";
          var NEW_PATH = "";
          var DIR_NEW_PATH = "";
          var TYPE_DOC = 0;

          switch (type) {
            case "bar":
              if (InfoMerchantMaster?.MPM_NO_BAR != "" && InfoMerchantMaster?.MPM_NO_BAR != null) {
                var NoBAR = InfoMerchantMaster.MPM_NO_BAR.split("/").join(".");
                RENAMEFILE = NoBAR + "_" + TIME_NAME + ".pdf";
                NEW_PATH = path.join(__dirname, '../../.non-digipos/document/' + InfoMerchantMaster.MPM_ID + '/bar/') + RENAMEFILE;
                DIR_NEW_PATH = path.join(__dirname, '../../.non-digipos/document/' + InfoMerchantMaster.MPM_ID + '/bar/');
                TYPE_DOC = 1;
              } else {
                MESSAGE = "Error, please generate no bar, before upload bar !"
              }
              break;
            case "bar-sign":
              if (InfoMerchantMaster?.MPM_NO_BAR != "" && InfoMerchantMaster?.MPM_NO_BAR != null) {
                if (InfoMerchantMaster.GENERATE_BAR == 0) {
                  MESSAGE = "Error, please generate bar, before upload bar sign !"
                } else {
                  var NoBAR = InfoMerchantMaster.MPM_NO_BAR.split("/").join(".");
                  RENAMEFILE = "SIGN_" + NoBAR + "_" + TIME_NAME + ".pdf";
                  NEW_PATH = path.join(__dirname, '../../.non-digipos/document/' + InfoMerchantMaster.MPM_ID + '/bar-sign/') + RENAMEFILE;
                  DIR_NEW_PATH = path.join(__dirname, '../../.non-digipos/document/' + InfoMerchantMaster.MPM_ID + '/bar-sign/');
                  TYPE_DOC = 2;
                }
              } else {
                MESSAGE = "Error, please generate no bar, before upload bar sign !"
              }
              break;
            case "invoice":
              if (InfoMerchantMaster?.MPM_NO_INVOICE != "" && InfoMerchantMaster?.MPM_NO_INVOICE != null) {
                if (InfoMerchantMaster.GENERATE_BAR_SIGN == 0) {
                  MESSAGE = "Error, please generate bar sign, before upload invoice !"
                } else {
                  var NoINVOICE = InfoMerchantMaster.MPM_NO_INVOICE;
                  RENAMEFILE = NoINVOICE + "_" + TIME_NAME + ".pdf";
                  NEW_PATH = path.join(__dirname, '../../.non-digipos/document/' + InfoMerchantMaster.MPM_ID + '/invoice/') + RENAMEFILE;
                  DIR_NEW_PATH = path.join(__dirname, '../../.non-digipos/document/' + InfoMerchantMaster.MPM_ID + '/invoice/');
                  TYPE_DOC = 3;
                }
              } else {
                MESSAGE = "Error, please generate no invoice, before upload invoice !"
              }
              break;
            case "invoice-sign":
              if (InfoMerchantMaster?.MPM_NO_INVOICE != "" && InfoMerchantMaster?.MPM_NO_INVOICE != null) {
                if (InfoMerchantMaster.GENERATE_INVOICE == 0) {
                  MESSAGE = "Error, please generate invoice, before upload invoice sign !"
                } else {
                  var NoINVOICE = InfoMerchantMaster.MPM_NO_INVOICE;
                  RENAMEFILE = "SIGN_" + NoINVOICE + "_" + TIME_NAME + ".pdf";
                  NEW_PATH = path.join(__dirname, '../../.non-digipos/document/' + InfoMerchantMaster.MPM_ID + '/invoice-sign/') + RENAMEFILE;
                  DIR_NEW_PATH = path.join(__dirname, '../../.non-digipos/document/' + InfoMerchantMaster.MPM_ID + '/invoice-sign/');
                  TYPE_DOC = 4;
                }
              } else {
                MESSAGE = "Error, please generate no invoice, before upload invoice sign !"
              }
              break;
            case "faktur-pajak":
              if (InfoMerchantMaster?.MPM_NO_FAKTUR != "" && InfoMerchantMaster?.MPM_NO_FAKTUR != null) {
                if (InfoMerchantMaster.GENERATE_INVOICE == 0) {
                  MESSAGE = "Error, please generate invoice, before upload faktur pajak !"
                } else {
                  var NoFAKTUR = InfoMerchantMaster.MPM_NO_FAKTUR;
                  RENAMEFILE = "FAKTUR_" + NoFAKTUR + "_" + TIME_NAME + ".pdf";
                  NEW_PATH = path.join(__dirname, '../../.non-digipos/document/' + InfoMerchantMaster.MPM_ID + '/faktur-pajak/') + RENAMEFILE;
                  DIR_NEW_PATH = path.join(__dirname, '../../.non-digipos/document/' + InfoMerchantMaster.MPM_ID + '/faktur-pajak/');
                  TYPE_DOC = 5;
                }
              } else {
                MESSAGE = "Error, please generate no faktur, before upload faktur pajak !"
              }
              break;
            case "bukti-potong":
              if (InfoMerchantMaster?.GENERATE_INVOICE_SIGN != 0) {
                var Merchants = await this.merchantRepository.findById(InfoMerchantMaster?.M_ID);
                var NPWP = Merchants.M_NPWP == "" || Merchants.M_NPWP == null || Merchants.M_NPWP == undefined ? "NONE" : Merchants.M_NPWP.split(".").join("").split("-").join("");
                RENAMEFILE = InfoMerchantMaster?.PERIODE + "_" + NPWP + "_" + TIME_NAME + ".pdf";
                NEW_PATH = path.join(__dirname, '../../.non-digipos/document/' + InfoMerchantMaster?.MPM_ID + '/bukti-potong/') + RENAMEFILE;
                DIR_NEW_PATH = path.join(__dirname, '../../.non-digipos/document/' + InfoMerchantMaster?.MPM_ID + '/bukti-potong/');
                TYPE_DOC = 6;
              } else {
                MESSAGE = "Error, please generate invoice, before upload bukti potong !"
              }
              break;
            case "others":
              RENAMEFILE = "OTHER_" + NameFile + ".pdf";
              NEW_PATH = path.join(__dirname, '../../.non-digipos/document/' + InfoMerchantMaster?.MPM_ID + '/others/') + RENAMEFILE;
              DIR_NEW_PATH = path.join(__dirname, '../../.non-digipos/document/' + InfoMerchantMaster?.MPM_ID + '/others/');
              TYPE_DOC = 7;
              break;
            default:
              MESSAGE = "Error, type path not found !";
              break;
          }

          if (RENAMEFILE == "") {
            resolve({success: true, message: MESSAGE});
            return;
          }


          if (InfoMerchantMaster != null) {
            try {
              if (!fs.existsSync(DIR_NEW_PATH)) {
                await new Promise<object>((resolve, reject) => {
                  fs.mkdir(DIR_NEW_PATH, {recursive: true}, (err) => {
                    if (err) {
                      resolve({success: true, err});
                    } else {
                      resolve({success: true, message: "rename success !"});
                    }
                  });
                });
              }

              fs.rename(PathFile, NEW_PATH, async (err) => {
                if (err) {
                  fs.unlink(PathFile, (errorDelete: any) => {
                    if (errorDelete) {
                      console.log(errorDelete);
                    }
                  });
                  resolve({success: false, message: err});
                } else {
                  try {
                    var FindDocInvoice = await this.merchantProductDocumentRepository.findOne({where: {and: [{MPDOC_NAME: RENAMEFILE}, {MPDOC_TYPE: TYPE_DOC}, {MPM_ID: InfoMerchantMaster.MPM_ID}]}});
                    if (TYPE_DOC != 7) {
                      await this.merchantProductDocumentRepository.updateAll({AT_FLAG: 0}, {and: [{MPDOC_TYPE: TYPE_DOC}, {MPM_ID: InfoMerchantMaster.MPM_ID}]});
                    }
                    if (FindDocInvoice != null) {
                      await this.merchantProductDocumentRepository.updateById(FindDocInvoice.MPDOC_ID, {
                        MPDOC_NAME: RENAMEFILE,
                        AT_FLAG: 1,
                        MPDOC_TYPE: TYPE_DOC,
                        MPDOC_PATH: NEW_PATH,
                        MPM_ID: InfoMerchantMaster.MPM_ID,
                        START_TIME_RUN: InfoMerchantMaster.START_TIME_RUN,
                        AT_UPDATE: ControllerConfig.onCurrentTime().toString(),
                        AT_WHO: USER_ID
                      });
                    } else {
                      await this.merchantProductDocumentRepository.create({
                        MPDOC_NAME: RENAMEFILE,
                        AT_FLAG: 1,
                        MPDOC_TYPE: TYPE_DOC,
                        MPDOC_PATH: NEW_PATH,
                        MPM_ID: InfoMerchantMaster.MPM_ID,
                        AT_CREATE: ControllerConfig.onCurrentTime().toString(),
                        AT_UPDATE: undefined,
                        START_TIME_RUN: InfoMerchantMaster.START_TIME_RUN,
                        AT_WHO: USER_ID
                      });
                    }

                    switch (type) {
                      case "bar":
                        await this.merchantProductMasterRepository.updateById(InfoMerchantMaster.MPM_ID, {
                          GENERATE_BAR: 1
                        });
                        break;
                      case "bar-sign":
                        await this.merchantProductMasterRepository.updateById(InfoMerchantMaster.MPM_ID, {
                          GENERATE_BAR_SIGN: 1
                        });
                        break;
                      case "invoice":
                        await this.merchantProductMasterRepository.updateById(InfoMerchantMaster.MPM_ID, {
                          GENERATE_INVOICE: 1
                        });
                        break;
                      case "invoice-sign":
                        await this.merchantProductMasterRepository.updateById(InfoMerchantMaster.MPM_ID, {
                          GENERATE_INVOICE_SIGN: 1
                        });
                        break;
                      case "faktur-pajak":
                        await this.merchantProductMasterRepository.updateById(InfoMerchantMaster.MPM_ID, {
                          GENERATE_FAKTUR_PAJAK: 1
                        });
                        break;
                      case "bukti-potong":
                        await this.merchantProductMasterRepository.updateById(InfoMerchantMaster.MPM_ID, {
                          GENERATE_BUKPOT: 1
                        });
                        break;
                      case "others":
                        break;
                      default:
                        MESSAGE = "Error, type path not found !";
                        break;
                    }

                    resolve({success: true, message: "Success upload document " + type + " !"});
                  } catch (error) {
                    fs.unlink(NEW_PATH, (errorDelete: any) => {
                      if (errorDelete) {
                        console.log(errorDelete);
                      }
                    });
                    resolve({success: false, message: error});
                  }
                }
              });

            } catch (error) {
              fs.unlink(PathFile, (errorDelete: any) => {
                if (errorDelete) {
                  console.log(errorDelete);
                }
              });
              resolve({success: false, message: error});
            }

          } else {
            fs.unlink(PathFile, (errorDelete: any) => {
              if (errorDelete) {
                console.log(errorDelete);
              }
            });
            resolve({success: false, message: "Error, failed upload document, transaction not found !"});
          }

        }
      });
    });
  }



  @get('/distribusi-upload/merchant/distribute-log/{MPM_ID}/views', {
    responses: {
      '200': {
        description: 'Merchant Distribusi Log type model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(MerchantProductDistribusi, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findMerchantDistributeDoc(
    @param.path.string('MPM_ID') MPM_ID: string,
    @param.filter(MerchantProductDistribusi) filter?: Filter<MerchantProductDistribusi>,
  ): Promise<Object> {
    var result: any; result = {success: true, message: '', datas: []};
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};


    var DOCUMENT_DISTRIBUTE = await this.merchantProductDistribusiRepository.find(filter);
    var DATAS: any; DATAS = [];
    for (var i in DOCUMENT_DISTRIBUTE) {
      var RAW_DOC_DISTRIBUTE = DOCUMENT_DISTRIBUTE[i];
      if (RAW_DOC_DISTRIBUTE.MPM_ID != MPM_ID) {
        continue;
      }

      var DATA: any; DATA = {
        MPDIST_ID: RAW_DOC_DISTRIBUTE.MPDIST_ID,
        MPDIST_MESSAGE: RAW_DOC_DISTRIBUTE.MPDIST_MESSAGE,
        MR_ACOUNT_NUMBER: RAW_DOC_DISTRIBUTE.SEND_TO,
        SEND_CC: RAW_DOC_DISTRIBUTE.SEND_CC,
        SEND_BAR: RAW_DOC_DISTRIBUTE.SEND_BAR,
        SEND_BAR_SIGN: RAW_DOC_DISTRIBUTE.SEND_BAR_SIGN,
        SEND_INVOICE: RAW_DOC_DISTRIBUTE.SEND_INVOICE,
        SEND_INVOICE_SIGN: RAW_DOC_DISTRIBUTE.SEND_INVOICE_SIGN,
        SEND_FAKTUR: RAW_DOC_DISTRIBUTE.SEND_FAKTUR,
        SEND_BUKPOT: RAW_DOC_DISTRIBUTE.SEND_BUKPOT,
        SEND_OTHERS: RAW_DOC_DISTRIBUTE.SEND_OTHERS,
        CREATE_SEND: RAW_DOC_DISTRIBUTE.CREATE_SEND,
        AT_SEND: RAW_DOC_DISTRIBUTE.AT_SEND,
        SEND_FLAG: RAW_DOC_DISTRIBUTE.SEND_FLAG,
        SEND_MESSAGE: RAW_DOC_DISTRIBUTE.SEND_MESSAGE,
        AT_FLAG: RAW_DOC_DISTRIBUTE.AT_FLAG
      };

      DATAS.push(DATA);
    }

    result.success = true;
    result.datas = DATAS;
    result.message = "Success !";

    return result
  }

  @get('/distribusi-upload/merchant/distribute-log/progres', {
    responses: {
      '200': {
        description: 'Merchant Distribusi Log progres type model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(MerchantProductDistribusi, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findMerchantDistributeDocLogProgres(
    @param.filter(MerchantProductDistribusi) filter?: Filter<MerchantProductDistribusi>,
  ): Promise<Object> {
    var result: any; result = {success: true, message: '', datas: []};
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};


    var DOCUMENT_DISTRIBUTE_PENDING = await this.merchantProductDistribusiRepository.count({and: [{AT_FLAG: 0}]});
    var DOCUMENT_DISTRIBUTE_PROCESS = await this.merchantProductDistribusiRepository.count({and: [{AT_FLAG: 1}]});
    var DOCUMENT_DISTRIBUTE_ERROR = await this.merchantProductDistribusiRepository.count({and: [{AT_FLAG: 3}]});

    var REPORT: any; REPORT = {};
    if (DOCUMENT_DISTRIBUTE_PENDING.count > 0) {
      REPORT["PENDING"] = DOCUMENT_DISTRIBUTE_PENDING.count;
    } else {
      delete REPORT["PENDING"];
    }

    if (DOCUMENT_DISTRIBUTE_PROCESS.count > 0) {
      REPORT["PROCESS"] = DOCUMENT_DISTRIBUTE_PROCESS.count;
    } else {
      delete REPORT["PROCESS"];
    }

    if (DOCUMENT_DISTRIBUTE_ERROR.count > 0) {
      REPORT["ERROR"] = DOCUMENT_DISTRIBUTE_PROCESS.count;
    }

    result.success = true;
    result.datas = {REPORT: REPORT, PROCESS: REPORT["PENDING"] == undefined && REPORT["PROCESS"] == undefined && REPORT["ERROR"] == undefined ? false : true};
    result.message = "Success !";

    return result
  }

}
