import {ControllerConfig} from './controller.config';

var NAMA_PIC_FINARYA = "";
var JABATAN_PIC_FINARYA = "";
var ALAMAT_FINARYA = "";

export class MerchantTemplateDocInv {
  constructor() { }

  //TEMPLATE BAR SKEMA 1

  static TEMPLATE_DOC_PAID(PIC_FINARYA: any, MERCHANT: any, MERCHANT_PRODUCT: any, MERCHANT_SKEMA: any, MERCHANT_TRIBE: any, MERCHANT_GROUP: any, MASTER_DATA: any, SUMMARY_DATA: any, MERCHANT_INVOICE_TYPE: any) {
    var HTML = '<div style="text-align:center; font-size:35px;padding:10px;margin-bottom:10px;">Invoice</div>';
    HTML += '<table width="100%" style="margin-bottom:20px;">';
    HTML += '  <tr>';
    HTML += '    <td width="130">Invoice Date</td>';
    HTML += '    <td width="10"> : </td>';
    HTML += '    <td></td>';
    HTML += '  </tr>';
    HTML += '  <tr>';
    HTML += '    <td>Due Date</td>';
    HTML += '    <td> : </td>';
    HTML += '    <td></td>';
    HTML += '  </tr>';
    HTML += '  <tr>';
    HTML += '    <td>Invoice Number</td>';
    HTML += '    <td> : </td>';
    HTML += '    <td>' + MASTER_DATA.MPM_NO_INVOICE + '</td>';
    HTML += '  </tr>';
    HTML += '</table>';

    var thisDateBAR = new Date(MASTER_DATA.PERIODE + "-01");
    var Mount = ControllerConfig.onGetMonth(thisDateBAR.getMonth());

    HTML += '<table class="tbl-trx-inv" width="100%" style="margin-bottom:20px;">';
    HTML += '  <tr  class="tbl-trx-inv-header" >';
    HTML += '    <td width="70%">Description</td>';
    HTML += '    <td width="30%"> Amount (IDR)</td>';
    HTML += '  </tr>';
    HTML += '  <tr>';
    HTML += '    <td>' + MERCHANT_PRODUCT.MP_BRAND + ' Periode ' + Mount + ' ' + thisDateBAR.getFullYear() + '</td>';
    HTML += '    <td class="text-number">' + ControllerConfig.onGeneratePuluhan(Number(MASTER_DATA.MPM_DPP.toFixed(0))) + '</td>';
    HTML += '  </tr>';
    HTML += '  <tr>';
    HTML += '    <td>Sub Total</td>';
    HTML += '    <td class="text-number">' + ControllerConfig.onGeneratePuluhan(Number(MASTER_DATA.MPM_DPP.toFixed(0))) + '</td>';
    HTML += '  </tr>';
    HTML += '  <tr>';
    HTML += '    <td>VAT OUT</td>';
    HTML += '    <td class="text-number">' + ControllerConfig.onGeneratePuluhan(Number(MASTER_DATA.MPM_PPN.toFixed(0))) + '</td>';
    HTML += '  </tr>';
    var TOTAL = MASTER_DATA.MPM_DPP + MASTER_DATA.MPM_PPN;
    HTML += '  <tr  class="tbl-trx-inv-footer">';
    HTML += '    <td>TOTAL</td>';
    HTML += '    <td class="text-number">' + ControllerConfig.onGeneratePuluhan(Number(TOTAL.toFixed(0))) + '</td>';
    HTML += '  </tr>';
    HTML += '</table>';
    HTML += '<p>';
    HTML += '     <b>Terbilang : </b>' + ControllerConfig.onAngkaTerbilang(TOTAL) + ' Rupiah'
    HTML += '</p>';

    HTML += '<div style="height:200px;">';
    HTML += '</div>';


    HTML += '<div>';
    HTML += ' <div style="width:300px; border-bottom:dashed 1px #000;">' + PIC_FINARYA.PIC_NAME + '</div>';
    HTML += ' <div>' + PIC_FINARYA.PIC_JABATAN + '</div>';
    HTML += '</div>';


    HTML += '<div style="margin-top:20px;">';
    HTML += "<table width='100%'>";
    HTML += "  <tr>";
    HTML += "     <td width='50%' style='vertical-align:top;'>"
    HTML += "       <div>For further information please contact</div>";
    HTML += "       <div>" + PIC_FINARYA.M_PIC_EMAIL + "</div>";
    HTML += "     </td>";
    HTML += "     <td width='50%'>";
    HTML += "         <table width='100%' style='margin-left:20px;'>";
    HTML += "           <tr>";
    HTML += "             <td width='140'> Account Name </td>";
    HTML += "             <td width='20'> : </td>";
    HTML += "             <td>" + PIC_FINARYA.PT_ACOUNT_NAME + "</td>";
    HTML += "           </tr>";
    HTML += "           <tr>";
    HTML += "             <td width='140'> Account Number </td>";
    HTML += "             <td width='20'> : </td>";
    HTML += "             <td>" + PIC_FINARYA.PT_ACOUNT_NUMBER + "</td>";
    HTML += "           </tr>";
    HTML += "           <tr>";
    HTML += "             <td width='140'> Bank Name </td>";
    HTML += "             <td width='20'> : </td>";
    HTML += "             <td >" + PIC_FINARYA.PT_BANK_NAME + "</td>";
    HTML += "           </tr>";
    HTML += "           <tr>";
    HTML += "             <td width='140'> Branch </td>";
    HTML += "             <td width='20'> : </td>";
    HTML += "             <td>" + PIC_FINARYA.PT_BRANCH + "</td>";
    HTML += "           </tr>";
    HTML += "         </table>";

    HTML += "     </td>";
    HTML += "  </tr>";
    HTML += "</table>";
    HTML += '</div>';


    HTML += '<div style="margin-top:30px;padding:30px;color:#858585; font-weight: bold; font-size:60px; text-align:center;">';
    HTML += ' PAID INVOICE'
    HTML += '</div>';

    return HTML;
  }

  static TEMPLATE_DOC_REGULAR(PIC_FINARYA: any, MERCHANT: any, MERCHANT_PRODUCT: any, MERCHANT_SKEMA: any, MERCHANT_TRIBE: any, MERCHANT_GROUP: any, MASTER_DATA: any, SUMMARY_DATA: any, MERCHANT_INVOICE_TYPE: any) {
    var HTML = '<div style="text-align:center; font-size:35px;padding:10px;margin-bottom:10px;">Invoice</div>';
    HTML += '<table width="100%" style="margin-bottom:20px;">';
    HTML += '  <tr>';
    HTML += '    <td width="130">Invoice Date</td>';
    HTML += '    <td width="10"> : </td>';
    HTML += '    <td></td>';
    HTML += '  </tr>';
    HTML += '  <tr>';
    HTML += '    <td>Due Date</td>';
    HTML += '    <td> : </td>';
    HTML += '    <td></td>';
    HTML += '  </tr>';
    HTML += '  <tr>';
    HTML += '    <td>Invoice Number</td>';
    HTML += '    <td> : </td>';
    HTML += '    <td>' + MASTER_DATA.MPM_NO_INVOICE + '</td>';
    HTML += '  </tr>';
    HTML += '</table>';

    var thisDateBAR = new Date(MASTER_DATA.PERIODE + "-01");
    var Mount = ControllerConfig.onGetMonth(thisDateBAR.getMonth());

    HTML += '<table class="tbl-trx-inv" width="100%" style="margin-bottom:20px;">';
    HTML += '  <tr  class="tbl-trx-inv-header" >';
    HTML += '    <td width="70%">Description</td>';
    HTML += '    <td width="30%"> Amount (IDR)</td>';
    HTML += '  </tr>';
    HTML += '  <tr>';
    HTML += '    <td>' + MERCHANT_PRODUCT.MP_BRAND + ' Periode ' + Mount + ' ' + thisDateBAR.getFullYear() + '</td>';
    HTML += '    <td class="text-number">' + ControllerConfig.onGeneratePuluhan(Number(MASTER_DATA.MPM_DPP.toFixed(0))) + '</td>';
    HTML += '  </tr>';
    HTML += '  <tr>';
    HTML += '    <td>Sub Total</td>';
    HTML += '    <td class="text-number">' + ControllerConfig.onGeneratePuluhan(Number(MASTER_DATA.MPM_DPP.toFixed(0))) + '</td>';
    HTML += '  </tr>';
    HTML += '  <tr>';
    HTML += '    <td>VAT OUT</td>';
    HTML += '    <td class="text-number">' + ControllerConfig.onGeneratePuluhan(Number(MASTER_DATA.MPM_PPN.toFixed(0))) + '</td>';
    HTML += '  </tr>';
    var TOTAL = MASTER_DATA.MPM_DPP + MASTER_DATA.MPM_PPN;
    HTML += '  <tr  class="tbl-trx-inv-footer">';
    HTML += '    <td>TOTAL</td>';
    HTML += '    <td class="text-number">' + ControllerConfig.onGeneratePuluhan(Number(TOTAL.toFixed(0))) + '</td>';
    HTML += '  </tr>';
    HTML += '</table>';
    HTML += '<p>';
    HTML += '     <b>Terbilang : </b>' + ControllerConfig.onAngkaTerbilang(TOTAL) + ' Rupiah'
    HTML += '</p>';

    HTML += '<div style="height:200px;">';
    HTML += '</div>';


    HTML += '<div>';
    HTML += ' <div style="width:300px; border-bottom:dashed 1px #000;">' + PIC_FINARYA.PIC_NAME + '</div>';
    HTML += ' <div>' + PIC_FINARYA.PIC_JABATAN + '</div>';
    HTML += '</div>';


    HTML += '<div style="margin-top:20px;">';
    HTML += "<table width='100%'>";
    HTML += "  <tr>";
    HTML += "     <td width='50%' style='vertical-align:top;'>"
    HTML += "       <div>For further information please contact</div>";
    HTML += "       <div>" + PIC_FINARYA.M_PIC_EMAIL + "</div>";
    HTML += "     </td>";
    HTML += "     <td width='50%'>";
    HTML += "         <table width='100%' style='margin-left:20px;'>";
    HTML += "           <tr>";
    HTML += "             <td width='140'> Account Name </td>";
    HTML += "             <td width='20'> : </td>";
    HTML += "             <td>" + PIC_FINARYA.PT_ACOUNT_NAME + "</td>";
    HTML += "           </tr>";
    HTML += "           <tr>";
    HTML += "             <td width='140'> Account Number </td>";
    HTML += "             <td width='20'> : </td>";
    HTML += "             <td>" + PIC_FINARYA.PT_ACOUNT_NUMBER + "</td>";
    HTML += "           </tr>";
    HTML += "           <tr>";
    HTML += "             <td width='140'> Bank Name </td>";
    HTML += "             <td width='20'> : </td>";
    HTML += "             <td >" + PIC_FINARYA.PT_BANK_NAME + "</td>";
    HTML += "           </tr>";
    HTML += "           <tr>";
    HTML += "             <td width='140'> Branch </td>";
    HTML += "             <td width='20'> : </td>";
    HTML += "             <td>" + PIC_FINARYA.PT_BRANCH + "</td>";
    HTML += "           </tr>";
    HTML += "         </table>";

    HTML += "     </td>";
    HTML += "  </tr>";
    HTML += "</table>";
    HTML += '</div>';


    HTML += '<div style="margin-top:30px;padding:30px;color:#858585; font-weight: bold; font-size:60px; text-align:center;">';
    HTML += ' REGULAR INVOICE'
    HTML += '</div>';

    return HTML;
  }

}
