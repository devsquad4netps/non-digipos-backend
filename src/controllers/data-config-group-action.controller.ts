import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {DataConfigGroup} from '../models';
import {DataConfigGroupRepository} from '../repositories';

export class DataConfigGroupActionController {
  constructor(
    @repository(DataConfigGroupRepository)
    public dataConfigGroupRepository : DataConfigGroupRepository,
  ) {}

  @post('/data-config-groups', {
    responses: {
      '200': {
        description: 'DataConfigGroup model instance',
        content: {'application/json': {schema: getModelSchemaRef(DataConfigGroup)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(DataConfigGroup, {
            title: 'NewDataConfigGroup',
            exclude: ['config_group_id'],
          }),
        },
      },
    })
    dataConfigGroup: Omit<DataConfigGroup, 'config_group_id'>,
  ): Promise<DataConfigGroup> {
    return this.dataConfigGroupRepository.create(dataConfigGroup);
  }

  @get('/data-config-groups/count', {
    responses: {
      '200': {
        description: 'DataConfigGroup model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(DataConfigGroup) where?: Where<DataConfigGroup>,
  ): Promise<Count> {
    return this.dataConfigGroupRepository.count(where);
  }

  @get('/data-config-groups', {
    responses: {
      '200': {
        description: 'Array of DataConfigGroup model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(DataConfigGroup, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(DataConfigGroup) filter?: Filter<DataConfigGroup>,
  ): Promise<DataConfigGroup[]> {
    return this.dataConfigGroupRepository.find(filter);
  }

  @patch('/data-config-groups', {
    responses: {
      '200': {
        description: 'DataConfigGroup PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(DataConfigGroup, {partial: true}),
        },
      },
    })
    dataConfigGroup: DataConfigGroup,
    @param.where(DataConfigGroup) where?: Where<DataConfigGroup>,
  ): Promise<Count> {
    return this.dataConfigGroupRepository.updateAll(dataConfigGroup, where);
  }

  @get('/data-config-groups/{id}', {
    responses: {
      '200': {
        description: 'DataConfigGroup model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(DataConfigGroup, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(DataConfigGroup, {exclude: 'where'}) filter?: FilterExcludingWhere<DataConfigGroup>
  ): Promise<DataConfigGroup> {
    return this.dataConfigGroupRepository.findById(id, filter);
  }

  @patch('/data-config-groups/{id}', {
    responses: {
      '204': {
        description: 'DataConfigGroup PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(DataConfigGroup, {partial: true}),
        },
      },
    })
    dataConfigGroup: DataConfigGroup,
  ): Promise<void> {
    await this.dataConfigGroupRepository.updateById(id, dataConfigGroup);
  }

  @put('/data-config-groups/{id}', {
    responses: {
      '204': {
        description: 'DataConfigGroup PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() dataConfigGroup: DataConfigGroup,
  ): Promise<void> {
    await this.dataConfigGroupRepository.replaceById(id, dataConfigGroup);
  }

  @del('/data-config-groups/{id}', {
    responses: {
      '204': {
        description: 'DataConfigGroup DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.dataConfigGroupRepository.deleteById(id);
  }
}
