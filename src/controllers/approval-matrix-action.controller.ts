import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {ApprovalMatrix} from '../models';
import {ApprovalMatrixRepository} from '../repositories';

export class ApprovalMatrixActionController {
  constructor(
    @repository(ApprovalMatrixRepository)
    public approvalMatrixRepository : ApprovalMatrixRepository,
  ) {}

  @post('/approval-matrices', {
    responses: {
      '200': {
        description: 'ApprovalMatrix model instance',
        content: {'application/json': {schema: getModelSchemaRef(ApprovalMatrix)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ApprovalMatrix, {
            title: 'NewApprovalMatrix',
            exclude: ['am_id'],
          }),
        },
      },
    })
    approvalMatrix: Omit<ApprovalMatrix, 'am_id'>,
  ): Promise<ApprovalMatrix> {
    return this.approvalMatrixRepository.create(approvalMatrix);
  }

  @get('/approval-matrices/count', {
    responses: {
      '200': {
        description: 'ApprovalMatrix model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(ApprovalMatrix) where?: Where<ApprovalMatrix>,
  ): Promise<Count> {
    return this.approvalMatrixRepository.count(where);
  }

  @get('/approval-matrices', {
    responses: {
      '200': {
        description: 'Array of ApprovalMatrix model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(ApprovalMatrix, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(ApprovalMatrix) filter?: Filter<ApprovalMatrix>,
  ): Promise<ApprovalMatrix[]> {
    return this.approvalMatrixRepository.find(filter);
  }

  @patch('/approval-matrices', {
    responses: {
      '200': {
        description: 'ApprovalMatrix PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ApprovalMatrix, {partial: true}),
        },
      },
    })
    approvalMatrix: ApprovalMatrix,
    @param.where(ApprovalMatrix) where?: Where<ApprovalMatrix>,
  ): Promise<Count> {
    return this.approvalMatrixRepository.updateAll(approvalMatrix, where);
  }

  @get('/approval-matrices/{id}', {
    responses: {
      '200': {
        description: 'ApprovalMatrix model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(ApprovalMatrix, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(ApprovalMatrix, {exclude: 'where'}) filter?: FilterExcludingWhere<ApprovalMatrix>
  ): Promise<ApprovalMatrix> {
    return this.approvalMatrixRepository.findById(id, filter);
  }

  @patch('/approval-matrices/{id}', {
    responses: {
      '204': {
        description: 'ApprovalMatrix PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ApprovalMatrix, {partial: true}),
        },
      },
    })
    approvalMatrix: ApprovalMatrix,
  ): Promise<void> {
    await this.approvalMatrixRepository.updateById(id, approvalMatrix);
  }

  @put('/approval-matrices/{id}', {
    responses: {
      '204': {
        description: 'ApprovalMatrix PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() approvalMatrix: ApprovalMatrix,
  ): Promise<void> {
    await this.approvalMatrixRepository.replaceById(id, approvalMatrix);
  }

  @del('/approval-matrices/{id}', {
    responses: {
      '204': {
        description: 'ApprovalMatrix DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.approvalMatrixRepository.deleteById(id);
  }
}
