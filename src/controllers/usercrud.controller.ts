import {authenticate} from '@loopback/authentication';
import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,


  model,
  property, repository,
  Where
} from '@loopback/repository';
import {
  del, get,
  getModelSchemaRef, param,


  patch, post,







  put, requestBody
} from '@loopback/rest';
import _ from 'lodash';
import {User} from '../models';
import {RolemappingRepository, RoleRepository, UserRepository} from '../repositories';

@model()
export class UpdateUserRequest extends User {
  @property({
    type: 'number',
    required: true,
  })
  role: number;
}




@authenticate('jwt')
export class UsercrudController {
  constructor(
    @repository(UserRepository)
    public userRepository: UserRepository,
    @repository(RolemappingRepository)
    public rolemapRepository: RolemappingRepository,
    @repository(RoleRepository)
    public roleRepository: RoleRepository,
  ) { }

  @post('/users', {
    responses: {
      '200': {
        description: 'User model instance',
        content: {'application/json': {schema: getModelSchemaRef(User)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(User, {
            title: 'NewUser',
            exclude: ['id'],
          }),
        },
      },
    })
    user: Omit<User, 'id'>,
  ): Promise<User> {
    return this.userRepository.create(user);
  }

  @get('/users/count', {
    responses: {
      '200': {
        description: 'User model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(User) where?: Where<User>,
  ): Promise<Count> {
    return this.userRepository.count(where);
  }

  @get('/users', {
    responses: {
      '200': {
        description: 'Array of User model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(User, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(User) filter?: Filter<User>,
  ): Promise<User[]> {
    var datas: any; datas = [];
    var userInfo = await this.userRepository.find(filter);
    var hideReal: any; hideReal = ["DELETED", "SYSTEM"];
    for (var i in userInfo) {
      var rawUser = userInfo[i];
      var realm = rawUser.realm == undefined ? 0 : rawUser.realm;
      if (hideReal.indexOf(realm) != -1) {
        continue;
      }
      datas.push(rawUser);
    }
    return datas;
  }

  @patch('/users', {
    responses: {
      '200': {
        description: 'User PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(User, {partial: true}),
        },
      },
    })
    user: User,
    @param.where(User) where?: Where<User>,
  ): Promise<Count> {
    return this.userRepository.updateAll(user, where);
  }

  @get('/users/{id}', {
    responses: {
      '200': {
        description: 'User model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(User, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(User, {exclude: 'where'}) filter?: FilterExcludingWhere<User>
  ): Promise<User> {
    var role = await this.rolemapRepository.findOne({"where": {"UserId": id}});
    var userInfo = await this.userRepository.findById(id, filter);
    userInfo["role"] = role?.RoleId;
    return userInfo;
  }

  @patch('/users/{id}', {
    responses: {
      '204': {
        description: 'User PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(User, {partial: true}),
        },
      },
    })
    user: User,
  ): Promise<void> {
    await this.userRepository.updateById(id, user);
  }

  @put('/users/{id}', {
    responses: {
      '200': {
        description: 'User',
        content: {
          'application/json': {
            schema: {
              'x-ts-type': User,
            },
          },
        },
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(UpdateUserRequest, {
            title: 'NewUser',
          }),
        },
      },
    })
    updateUserRequest: UpdateUserRequest,
  ): Promise<{data: Object}> {
    const updateUser = await this.userRepository.replaceById(id, _.omit(updateUserRequest, 'role'));
    var RoleMap = await this.rolemapRepository.findOne({"where": {"UserId": id}});
    var RoleMapId = RoleMap?.id;
    await this.rolemapRepository.updateById(RoleMapId, {RoleId: updateUserRequest.role});

    return {
      data: {updateUser}
    }
  }

  @del('/users/{id}', {
    responses: {
      '204': {
        description: 'User DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    // await this.userRepository.deleteById(id);
    await this.userRepository.updateById(id, {realm: "DELETED"});
  }
}
