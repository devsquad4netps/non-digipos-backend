import {authenticate} from '@loopback/authentication';
import {inject} from '@loopback/core';
import {Filter, repository} from '@loopback/repository';
import {get, getModelSchemaRef, param, post, Request, requestBody, Response, ResponseObject, RestBindings} from '@loopback/rest';
import {SecurityBindings, securityId, UserProfile} from '@loopback/security';
import csv from 'csv-parser';
import fs from 'fs';
import multer from 'multer';
import path from 'path';
import {ImportProcessLog} from '../models';
import {ImportProcessLogRepository, MerchantProductConditionRepository, MerchantProductDatastoreRepository, MerchantProductGroupRepository, MerchantProductMasterRepository, MerchantProductMdrConditionRepository, MerchantProductMdrRepository, MerchantProductQueryRepository, MerchantProductSummaryGroupRepository, MerchantProductSummaryRepository, MerchantProductVariableRepository, MpvTypeKeyRepository, MpvTypeRepository, SycronizeProcessLogRepository} from '../repositories';
import {MerchantProductRepository} from '../repositories/merchant-product.repository';
import {MerchantRepository} from '../repositories/merchant.repository';
import {UserRepository} from '../repositories/user.repository';
import {ControllerConfig} from './controller.config';
const readXlsxFile = require('read-excel-file/node');

export const ReRunningImportLogBody: ResponseObject = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ["IMPORT_ID"],
        properties: {
          SPL_ID: {
            type: 'array',
            items: {type: 'number'}
          },
        }
      }
    },
  },
};



export const CreateManualTrxBody: ResponseObject = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ["PERIODE", "COUNT_TRX", "AMOUNT", "MDR", "M_ID", "MP_ID", "MPG_ID"],
        properties: {
          PERIODE: {
            type: 'string',
          },
          COUNT_TRX: {
            type: 'number',
          },
          AMOUNT: {
            type: 'number',
          },
          MDR: {
            type: 'number',
          },
          M_ID: {
            type: 'number',
          },
          MP_ID: {
            type: 'number',
          },
          MPG_ID: {
            type: 'number',
          },
        }
      }
    },
  },
};

var PROCESS_SYNC: any; PROCESS_SYNC = {};
var PROCESS_SYNC_DATA: any; PROCESS_SYNC_DATA = {};


var DATA_CREATE_MANUAL: any; DATA_CREATE_MANUAL = {};

@authenticate('jwt')
export class ImportExcelActionController {
  constructor(
    @repository(MerchantRepository)
    public merchantRepository: MerchantRepository,
    @repository(MerchantProductRepository)
    public merchantProductRepository: MerchantProductRepository,
    @repository(MerchantProductGroupRepository)
    public merchantProductGroupRepository: MerchantProductGroupRepository,
    @repository(MerchantProductVariableRepository)
    public merchantProductVariableRepository: MerchantProductVariableRepository,
    @repository(MerchantProductDatastoreRepository)
    public merchantProductDatastoreRepository: MerchantProductDatastoreRepository,
    @repository(MerchantProductSummaryRepository)
    public merchantProductSummaryRepository: MerchantProductSummaryRepository,
    @repository(MpvTypeRepository)
    public mpvTypeRepository: MpvTypeRepository,
    @repository(MerchantProductMasterRepository)
    public merchantProductMasterRepository: MerchantProductMasterRepository,
    @repository(MpvTypeKeyRepository)
    public mpvTypeKeyRepository: MpvTypeKeyRepository,
    @repository(SycronizeProcessLogRepository)
    public sycronizeProcessLogRepository: SycronizeProcessLogRepository,
    @repository(MerchantProductQueryRepository)
    public merchantProductQueryRepository: MerchantProductQueryRepository,
    @repository(MerchantProductConditionRepository)
    public merchantProductConditionRepository: MerchantProductConditionRepository,
    @repository(MerchantProductSummaryGroupRepository)
    public merchantProductSummaryGroupRepository: MerchantProductSummaryGroupRepository,
    @repository(MerchantProductMdrConditionRepository)
    public merchantProductMdrConditionRepository: MerchantProductMdrConditionRepository,
    @repository(MerchantProductMdrRepository)
    public merchantProductMdrRepository: MerchantProductMdrRepository,
    @repository(ImportProcessLogRepository)
    public importProcessLogRepository: ImportProcessLogRepository,
    @inject(SecurityBindings.USER)
    public currentUserProfile: UserProfile,
    @repository(UserRepository)
    public userRepository: UserRepository,
  ) { }



  ON_PROCESS_CONDITION_MDR = async (MDR_ID: number, DATA_VALUES: any) => {
    return new Promise<object>(async (resolve, reject) => {

      var CONDITION_MDR = await this.merchantProductMdrConditionRepository.find({where: {and: [{MDR_ID: MDR_ID}, {AT_FLAG: 1}]}});
      var LOGIC_CON_MDR: any; LOGIC_CON_MDR = {};
      for (var MC_I in CONDITION_MDR) {
        var RAW_MC = CONDITION_MDR[MC_I];
        if (LOGIC_CON_MDR[RAW_MC.MDRCON_GROUP] == undefined) {
          LOGIC_CON_MDR[RAW_MC.MDRCON_GROUP] = [];
        }
        LOGIC_CON_MDR[RAW_MC.MDRCON_GROUP].push(RAW_MC);
      }

      var AND_STATE: any; AND_STATE = [];
      for (var LOGIC_I in LOGIC_CON_MDR) {
        var RAW_LOGIC_AND = LOGIC_CON_MDR[LOGIC_I];
        var OR_STATE: any; OR_STATE = [];
        for (var OR_I in RAW_LOGIC_AND) {
          var RAW_LOGIC_OR = RAW_LOGIC_AND[OR_I];
          if (DATA_VALUES[RAW_LOGIC_OR.MPV_ID] == undefined) {
            OR_STATE.push(0);
            continue;
          }
          switch (RAW_LOGIC_OR.MDRCON_OPERATOR) {
            case "=":
              if (DATA_VALUES[RAW_LOGIC_OR.MPV_ID] == RAW_LOGIC_OR.MDRCON_VALUE) {
                OR_STATE.push(1);
              } else {
                OR_STATE.push(0);
              }
              break;
            case "<=":
              if (Number(DATA_VALUES[RAW_LOGIC_OR.MPV_ID]) <= Number(RAW_LOGIC_OR.MDRCON_VALUE)) {
                OR_STATE.push(1);
              } else {
                OR_STATE.push(0);
              }
              break;
            case ">=":
              if (Number(DATA_VALUES[RAW_LOGIC_OR.MPV_ID]) >= Number(RAW_LOGIC_OR.MDRCON_VALUE)) {
                OR_STATE.push(1);
              } else {
                OR_STATE.push(0);
              }
              break;
            case "<>":
              if (DATA_VALUES[RAW_LOGIC_OR.MPV_ID] != RAW_LOGIC_OR.MDRCON_VALUE) {
                OR_STATE.push(1);
              } else {
                OR_STATE.push(0);
              }
              break;
            case "<":
              if (Number(DATA_VALUES[RAW_LOGIC_OR.MPV_ID]) < Number(RAW_LOGIC_OR.MDRCON_VALUE)) {
                OR_STATE.push(1);
              } else {
                OR_STATE.push(0);
              }
              break;
            case ">":
              if (Number(DATA_VALUES[RAW_LOGIC_OR.MPV_ID]) > Number(RAW_LOGIC_OR.MDRCON_VALUE)) {
                OR_STATE.push(1);
              } else {
                OR_STATE.push(0);
              }
              break;
            case "LIKE":
              if (RAW_LOGIC_OR.MDRCON_VALUE == "" || RAW_LOGIC_OR.MDRCON_VALUE == null) {
                if (DATA_VALUES[RAW_LOGIC_OR.MPV_ID] == null || DATA_VALUES[RAW_LOGIC_OR.MPV_ID] == "") {
                  OR_STATE.push(1);
                } else {
                  OR_STATE.push(0);
                }
              } else {
                if (DATA_VALUES[RAW_LOGIC_OR.MPV_ID].search(RAW_LOGIC_OR.MDRCON_VALUE) == -1) {
                  OR_STATE.push(0);
                } else {
                  OR_STATE.push(1);
                }
              }
              break;
            case "NOT IN":
              var TEXT_SEARCH = RAW_LOGIC_OR.MDRCON_VALUE.split(";");
              if (TEXT_SEARCH.indexOf(DATA_VALUES[RAW_LOGIC_OR.MPV_ID]) == -1) {
                OR_STATE.push(1);
              } else {
                OR_STATE.push(0);
              }
              break;
            case "IN":
              var TEXT_SEARCH = RAW_LOGIC_OR.MDRCON_VALUE.split(";");
              if (TEXT_SEARCH.indexOf(DATA_VALUES[RAW_LOGIC_OR.MPV_ID]) == -1) {
                OR_STATE.push(0);
              } else {
                OR_STATE.push(1);
              }
              break;
            default:
              OR_STATE.push(0);
              break;
          }
        }
        if (OR_STATE.indexOf(1) != -1) {
          AND_STATE.push(1);
        } else {
          AND_STATE.push(0);
        }
      }

      if (AND_STATE.indexOf(0) != -1) {
        resolve({status: false, MDR_ID: MDR_ID});
      } else {
        resolve({status: true, MDR_ID: MDR_ID});
      }
    });
  }

  ON_PROCESS_CREATE = async (PROCESS_KEY: any) => {

    //INFORMATION FILE UPLOAD
    var USER_ID = this.currentUserProfile[securityId];
    var RAW_PROCESS = PROCESS_SYNC_DATA[PROCESS_KEY];
    var MP_ID = RAW_PROCESS.MP_ID;
    var M_ID = RAW_PROCESS.M_ID;
    var IMPORT_ID = RAW_PROCESS.PROCESS.IMPORT_ID;
    var PERIODE = RAW_PROCESS.PERIODE;
    var PATH_FILE = RAW_PROCESS.PROCESS.IM_PATH;
    var TYPE_FILE = RAW_PROCESS.PROCESS.IM_MIME_TYPE;
    var MERCHANT_QUERY = await this.merchantProductQueryRepository.findOne({where: {and: [{MP_ID: MP_ID}]}});

    //READING FILE UPLOADS
    var TRANSAKSI_FILE: any; TRANSAKSI_FILE = [];
    var ERROR_READ = false;
    var ERROR_READ_MESSAGE: any; ERROR_READ_MESSAGE = "";
    if (TYPE_FILE == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") {
      await new Promise<object>(async (resolve, reject) => {
        try {
          readXlsxFile(path.join(__dirname, './../../.non-digipos/import/' + PATH_FILE)).then((rows: any) => {
            TRANSAKSI_FILE = rows;
            resolve({success: true});
          });
        } catch (error) {
          ERROR_READ = true;
          ERROR_READ_MESSAGE = JSON.stringify(error);
          resolve({success: false});
        }
      });
    }

    if (TYPE_FILE == "text/csv") {
      await new Promise<object>(async (resolve, reject) => {
        try {
          var DATA: any; DATA = [];
          fs.createReadStream(path.join(__dirname, './../../.non-digipos/import/' + PATH_FILE)).pipe(csv()).on('data', async (DATA_CSV_TRX: any) => {
            DATA.push(DATA_CSV_TRX);
          }).on('end', async () => {
            TRANSAKSI_FILE = DATA;
            resolve({success: true});
          });
        } catch (error) {
          ERROR_READ = true;
          ERROR_READ_MESSAGE = JSON.stringify(error);
          resolve({success: false});
        }
      });
    }

    if (ERROR_READ == false) {

      await this.importProcessLogRepository.updateById(IMPORT_ID, {
        IM_COUNT_TRX: 0,
        IM_TRX_AMOUNT: 0,
        IM_MDR: 0,
        IM_PROCESS_COUNT: TRANSAKSI_FILE.length,
        IM_PROCESS_NOW: 0,
        AT_FLAG: 1,
        IM_MESSAGE: "PROCESS.."
      });

      //VARIABLE STORE
      var VARIABLE = await this.merchantProductVariableRepository.find({where: {and: [{AT_FLAG: 1}, {MP_ID: MP_ID}]}});
      var VARIABLE_BY_KEY: any; VARIABLE_BY_KEY = {};
      var VARIABLE_BY_ID: any; VARIABLE_BY_ID = {};
      var FIELD_NOTFOUND: any; FIELD_NOTFOUND = [];
      var KEY_CONFIG = {KEY: "", AMOUNT: "", PERIODE: "", COUNT: "", MDR: ""};
      for (var INDEX_VAR in VARIABLE) {
        var RAW_VARIABLE = VARIABLE[INDEX_VAR];
        var VAR_ID = RAW_VARIABLE.MPV_ID == undefined ? -1 : RAW_VARIABLE.MPV_ID;
        if (VARIABLE_BY_KEY[RAW_VARIABLE.MPV_KEY] == undefined) {
          VARIABLE_BY_KEY[RAW_VARIABLE.MPV_KEY] = RAW_VARIABLE;
          VARIABLE_BY_ID[VAR_ID] = RAW_VARIABLE;
          FIELD_NOTFOUND.push(RAW_VARIABLE.MPV_KEY);
          switch (RAW_VARIABLE.MPV_TYPE_KEY_ID) {
            case 1:
              KEY_CONFIG.PERIODE = RAW_VARIABLE.MPV_KEY;
              break;
            case 2:
              KEY_CONFIG.AMOUNT = RAW_VARIABLE.MPV_KEY;
              break;
            case 3:
              KEY_CONFIG.COUNT = RAW_VARIABLE.MPV_KEY;
              break;
            case 4:
              KEY_CONFIG.MDR = RAW_VARIABLE.MPV_KEY;
              break;
            case 6:
              KEY_CONFIG.KEY = RAW_VARIABLE.MPV_KEY;
              break;
          }
        }
      }

      //CHECKING TEMPLATE
      for (var INDEX_TRX in TRANSAKSI_FILE) {
        var RAW_TRANSAKSI = TRANSAKSI_FILE[INDEX_TRX];
        for (var VAR_TRANSAKSI in RAW_TRANSAKSI) {
          if (FIELD_NOTFOUND.indexOf(VAR_TRANSAKSI) != -1) {
            FIELD_NOTFOUND.splice(FIELD_NOTFOUND.indexOf(VAR_TRANSAKSI), 1);
          }
        }
        break;
      }

      if (FIELD_NOTFOUND.length == 0) {

        var MERCHANT_PRODUCT_GROUP = await this.merchantProductGroupRepository.find({where: {and: [{AT_FLAG: 1}, {MP_ID: MP_ID}]}});


        var PROCESS_TRANSAKSI_COUNT = 0;
        var PROCESS_GROUP = 0;


        var DATA_BY_GROUP: any; DATA_BY_GROUP = {};
        var DATA_BY_SUMMARY: any; DATA_BY_SUMMARY = {};
        var DATA_BY_DATASOURCE: any; DATA_BY_DATASOURCE = {};


        var DATA_BY_GROUP_CPS: any; DATA_BY_GROUP_CPS = {};
        var DATA_BY_SUMMARY_CPS: any; DATA_BY_SUMMARY_CPS = {};

        var NO = 0;
        for (var INDEX_TRX in TRANSAKSI_FILE) {
          var RAW_TRANSAKSI = TRANSAKSI_FILE[INDEX_TRX];

          await this.importProcessLogRepository.updateById(IMPORT_ID, {
            IM_PROCESS_NOW: NO,
            AT_UPDATE: ControllerConfig.onCurrentTime().toString(),
          });
          NO++;

          var DATA_VALUES: any; DATA_VALUES = {};
          for (var INDEX_CHECK_VAR in VARIABLE_BY_KEY) {
            var VARIABLE_ID = Number(VARIABLE_BY_KEY[INDEX_CHECK_VAR]["MPV_ID"]);
            if (DATA_VALUES[VARIABLE_ID] == undefined) {
              DATA_VALUES[VARIABLE_ID] = "";
            }
            DATA_VALUES[VARIABLE_ID] = RAW_TRANSAKSI[INDEX_CHECK_VAR];
          }

          for (var I_MPG in MERCHANT_PRODUCT_GROUP) {
            var RAW_MPG = MERCHANT_PRODUCT_GROUP[I_MPG];

            //CONFIGURATION PRODUCT GROUP
            var MPG_ID = RAW_MPG.MPG_ID == undefined ? -1 : RAW_MPG.MPG_ID;
            var MDR_TYPE = RAW_MPG.MPG_MDR_TYPE;//1 : PERCENT - 2 : AMOUNT
            var MDR_AMOUNT = MDR_TYPE == 1 ? Number(RAW_MPG.MPG_MDR_AMOUNT) / 100 : Number(RAW_MPG.MPG_MDR_AMOUNT);
            var MDR_DPP = RAW_MPG.MPG_DPP;
            var MDR_PPN = RAW_MPG.MPG_PPN;
            var MDR_PPH = RAW_MPG.MPG_PPH;
            var MPG_LABEL = RAW_MPG.MPG_LABEL;

            //GET CONDITION CONFIGURATION
            var CONDITION = await this.merchantProductConditionRepository.find({where: {and: [{MPG_ID: MPG_ID}, {AT_FLAG: 1}]}});
            var LOGIC_CON: any; LOGIC_CON = {};
            for (var MC_I in CONDITION) {
              var RAW_MC = CONDITION[MC_I];
              if (LOGIC_CON[RAW_MC.MPC_GROUP] == undefined) {
                LOGIC_CON[RAW_MC.MPC_GROUP] = [];
              }
              LOGIC_CON[RAW_MC.MPC_GROUP].push(RAW_MC);
            }



            var AND_STATE: any; AND_STATE = [];
            for (var LOGIC_I in LOGIC_CON) {
              var RAW_LOGIC_AND = LOGIC_CON[LOGIC_I];
              var OR_STATE: any; OR_STATE = [];
              for (var OR_I in RAW_LOGIC_AND) {
                var RAW_LOGIC_OR = RAW_LOGIC_AND[OR_I];
                if (DATA_VALUES[RAW_LOGIC_OR.MPV_ID] == undefined) {
                  OR_STATE.push(0);
                  continue;
                }
                switch (RAW_LOGIC_OR.MPC_OPERATOR) {
                  case "=":
                    if (DATA_VALUES[RAW_LOGIC_OR.MPV_ID] == RAW_LOGIC_OR.MPC_VALUE) {
                      OR_STATE.push(1);
                    } else {
                      OR_STATE.push(0);
                    }
                    break;
                  case "<=":
                    if (Number(DATA_VALUES[RAW_LOGIC_OR.MPV_ID]) <= Number(RAW_LOGIC_OR.MPC_VALUE)) {
                      OR_STATE.push(1);
                    } else {
                      OR_STATE.push(0);
                    }
                    break;
                  case ">=":
                    if (Number(DATA_VALUES[RAW_LOGIC_OR.MPV_ID]) >= Number(RAW_LOGIC_OR.MPC_VALUE)) {
                      OR_STATE.push(1);
                    } else {
                      OR_STATE.push(0);
                    }
                    break;
                  case "<>":
                    if (DATA_VALUES[RAW_LOGIC_OR.MPV_ID] != RAW_LOGIC_OR.MPC_VALUE) {
                      OR_STATE.push(1);
                    } else {
                      OR_STATE.push(0);
                    }
                    break;
                  case "<":
                    if (Number(DATA_VALUES[RAW_LOGIC_OR.MPV_ID]) < Number(RAW_LOGIC_OR.MPC_VALUE)) {
                      OR_STATE.push(1);
                    } else {
                      OR_STATE.push(0);
                    }
                    break;
                  case ">":
                    if (Number(DATA_VALUES[RAW_LOGIC_OR.MPV_ID]) > Number(RAW_LOGIC_OR.MPC_VALUE)) {
                      OR_STATE.push(1);
                    } else {
                      OR_STATE.push(0);
                    }
                    break;
                  case "LIKE":
                    if (RAW_LOGIC_OR.MPC_VALUE == "" || RAW_LOGIC_OR.MPC_VALUE == null) {
                      if (DATA_VALUES[RAW_LOGIC_OR.MPV_ID] == null || DATA_VALUES[RAW_LOGIC_OR.MPV_ID] == "") {
                        OR_STATE.push(1);
                      } else {
                        OR_STATE.push(0);
                      }
                    } else {
                      if (DATA_VALUES[RAW_LOGIC_OR.MPV_ID].search(RAW_LOGIC_OR.MPC_VALUE) == -1) {
                        OR_STATE.push(0);
                      } else {
                        OR_STATE.push(1);
                      }
                    }
                    break;
                  case "NOT IN":
                    var TEXT_SEARCH = RAW_LOGIC_OR.MPC_VALUE.split(";");
                    if (TEXT_SEARCH.indexOf(DATA_VALUES[RAW_LOGIC_OR.MPV_ID]) == -1) {
                      OR_STATE.push(1);
                    } else {
                      OR_STATE.push(0);
                    }
                    break;
                  case "IN":
                    var TEXT_SEARCH = RAW_LOGIC_OR.MPC_VALUE.split(";");
                    if (TEXT_SEARCH.indexOf(DATA_VALUES[RAW_LOGIC_OR.MPV_ID]) == -1) {
                      OR_STATE.push(0);
                    } else {
                      OR_STATE.push(1);
                    }
                    break;
                  default:
                    OR_STATE.push(0);
                    break;
                }
              }
              if (OR_STATE.indexOf(1) != -1) {
                AND_STATE.push(1);
              } else {
                AND_STATE.push(0);
              }
            }

            if (AND_STATE.indexOf(0) != -1) {
              continue;
            }



            //CALCULATE MDR & AMOUNT GROUP IF CONDITION TRUE
            // DATA_BY_GROUP[MPG_ID] = {COUNT_TRX: 0, AMOUNT: 0, MDR: 0, FEE: MDR_AMOUNT};

            if (DATA_BY_GROUP[MPG_ID] == undefined) {
              DATA_BY_GROUP[MPG_ID] = {LABEL: RAW_MPG.MPG_LABEL, MDR_TYPE: RAW_MPG.MPG_MDR_TYPE, COUNT_TRX: 0, AMOUNT: 0, MDR: 0, FEE: MDR_AMOUNT, DPP: 0, PPN: 0, PPH: 0, HAS_SELISIH: 0};
              DATA_BY_GROUP_CPS[MPG_ID] = {LABEL: RAW_MPG.MPG_LABEL, MDR_TYPE: RAW_MPG.MPG_MDR_TYPE, COUNT_TRX: 0, AMOUNT: 0, MDR: 0, FEE: MDR_AMOUNT, DPP: 0, PPN: 0, PPH: 0, HAS_SELISIH: 0};
            }

            var KEY_GROUP = MERCHANT_QUERY?.MPQ_GROUP_SUMMARY == "" || MERCHANT_QUERY?.MPQ_GROUP_SUMMARY == undefined ? [] : JSON.parse(MERCHANT_QUERY.MPQ_GROUP_SUMMARY.split("'").join('"'));
            var KEY_SUMMARY_GROUP: any; KEY_SUMMARY_GROUP = MPG_ID;
            if (KEY_GROUP.length > 0) {
              KEY_SUMMARY_GROUP = "";
              for (var IG in KEY_GROUP) {
                var RAW_IG = KEY_GROUP[IG];
                if (RAW_TRANSAKSI[RAW_IG] != undefined) {
                  var GROUP_VAR_ID = VARIABLE_BY_KEY[RAW_IG]["MPV_ID"];
                  KEY_SUMMARY_GROUP += "_" + RAW_TRANSAKSI[RAW_IG];
                } else {
                  KEY_SUMMARY_GROUP += "_" + MPG_ID;
                }
              }
            }

            if (DATA_BY_SUMMARY[KEY_SUMMARY_GROUP] == undefined) {
              DATA_BY_SUMMARY[KEY_SUMMARY_GROUP] = {LABEL: KEY_SUMMARY_GROUP.substring(1), MDR_TYPE: RAW_MPG.MPG_MDR_TYPE, COUNT_TRX: 0, AMOUNT: 0, MDR: 0, FEE: MDR_AMOUNT, DPP: 0, PPN: 0, PPH: 0, HAS_SELISIH: 0};
              DATA_BY_SUMMARY_CPS[KEY_SUMMARY_GROUP] = {LABEL: KEY_SUMMARY_GROUP.substring(1), MDR_TYPE: RAW_MPG.MPG_MDR_TYPE, COUNT_TRX: 0, AMOUNT: 0, MDR: 0, FEE: MDR_AMOUNT, DPP: 0, PPN: 0, PPH: 0, HAS_SELISIH: 0};
            }

            var COUNT_NOW = 0;
            var COUNT_NOW_CPS = 0;
            //JIKA COLUMN COUNT NOT SET
            if (KEY_CONFIG.COUNT != "") {
              if (RAW_TRANSAKSI[KEY_CONFIG.COUNT] != undefined) {
                COUNT_NOW = Number(RAW_TRANSAKSI[KEY_CONFIG.COUNT]);
                COUNT_NOW_CPS = Number(RAW_TRANSAKSI[KEY_CONFIG.COUNT]);
              } else {
                COUNT_NOW = 1;
                COUNT_NOW_CPS = 1;
              }
            } else {
              COUNT_NOW = 1;
              COUNT_NOW_CPS = 1;
            }

            //SET CONFIGURATION COUNT TRANSAKSI
            DATA_BY_GROUP[MPG_ID].COUNT_TRX += COUNT_NOW;
            DATA_BY_GROUP_CPS[MPG_ID].COUNT_TRX += COUNT_NOW_CPS;
            DATA_BY_SUMMARY[KEY_SUMMARY_GROUP].COUNT_TRX += COUNT_NOW;
            DATA_BY_SUMMARY_CPS[KEY_SUMMARY_GROUP].COUNT_TRX += COUNT_NOW_CPS;


            // AMOUNT TRANSACTION
            var AMOUNT_NOW = 0;
            var AMOUNT_CPS = 0;
            if (KEY_CONFIG.AMOUNT != "") {
              if (RAW_TRANSAKSI[KEY_CONFIG.AMOUNT] != undefined) {
                AMOUNT_NOW = Number(RAW_TRANSAKSI[KEY_CONFIG.AMOUNT]);
                AMOUNT_CPS = Number(RAW_TRANSAKSI[KEY_CONFIG.AMOUNT]);
              } else {
                AMOUNT_NOW = 0;
                AMOUNT_CPS = 0;
              }
            } else {
              AMOUNT_NOW = 0;
              AMOUNT_CPS = 0;
            }
            DATA_BY_GROUP[MPG_ID].AMOUNT += AMOUNT_NOW;
            DATA_BY_GROUP_CPS[MPG_ID].AMOUNT += AMOUNT_CPS;
            DATA_BY_SUMMARY[KEY_SUMMARY_GROUP].AMOUNT += AMOUNT_NOW;
            DATA_BY_SUMMARY_CPS[KEY_SUMMARY_GROUP].AMOUNT += AMOUNT_CPS;

            var MERCHANT_MDR = await this.merchantProductMdrRepository.find({where: {and: [{AT_FLAG: 1}, {MPG_ID: MPG_ID}]}});
            var MDR_LOGIC = {MDR: 0, TYPE: 0};



            var MDR_NOW = 0;
            var MDR_NOW_CPS = 0;
            var HAS_SELISIH = 0;
            if (MDR_AMOUNT != 0) {
              if (KEY_CONFIG.MDR != "") {
                if (RAW_TRANSAKSI[KEY_CONFIG.MDR] != undefined) {

                  await new Promise<object>(async (resolve, reject) => {
                    for (var I_MDR in MERCHANT_MDR) {
                      var RAW_MDR = MERCHANT_MDR[I_MDR];
                      var MDR_ID = RAW_MDR.MDR_ID == undefined ? -1 : RAW_MDR.MDR_ID;
                      var RETUN_CONDITION_MDR: any; RETUN_CONDITION_MDR = await this.ON_PROCESS_CONDITION_MDR(MDR_ID, DATA_VALUES);
                      if (RETUN_CONDITION_MDR.status == true) {
                        if (RAW_MDR.MDR_TYPE == 1) {
                          MDR_LOGIC.MDR = (RAW_MDR.MDR == undefined ? 0 : Number(RAW_MDR.MDR)) / 100;
                          MDR_LOGIC.TYPE = RAW_MDR.MDR_TYPE;
                        }
                        if (RAW_MDR.MDR_TYPE == 2) {
                          MDR_LOGIC.MDR = (RAW_MDR.MDR == undefined ? 0 : Number(RAW_MDR.MDR));
                          MDR_LOGIC.TYPE = RAW_MDR.MDR_TYPE;
                        }
                      }
                    }
                    resolve({status: true});
                  });

                  if (Number(RAW_TRANSAKSI[KEY_CONFIG.MDR]) != 0) {
                    var MDR_TRANSAKSI = Number(Number(RAW_TRANSAKSI[KEY_CONFIG.MDR]).toFixed(0));

                    if (MDR_LOGIC.MDR != 0) {

                      if (MDR_LOGIC.TYPE == 1) {
                        MDR_NOW = (AMOUNT_NOW * Number(MDR_LOGIC.MDR));
                        if (Number(MDR_NOW.toFixed(0)) == MDR_TRANSAKSI) {
                          MDR_NOW_CPS = MDR_NOW;
                        } else {
                          HAS_SELISIH = 1;
                          MDR_NOW_CPS = MDR_TRANSAKSI;
                        }
                      }

                      if (MDR_LOGIC.TYPE == 2) {
                        MDR_NOW = (COUNT_NOW * Number(MDR_LOGIC.MDR));
                        if (Number(MDR_NOW.toFixed(0)) == MDR_TRANSAKSI) {
                          MDR_NOW_CPS = MDR_NOW;
                        } else {
                          HAS_SELISIH = 1;
                          MDR_NOW_CPS = MDR_TRANSAKSI;
                        }
                      }
                    } else {

                      if (MDR_TYPE == 1) {
                        MDR_NOW = (AMOUNT_NOW * Number(MDR_AMOUNT));
                        if (Number(MDR_NOW.toFixed(0)) == MDR_TRANSAKSI) {
                          MDR_NOW_CPS = MDR_NOW;
                        } else {
                          HAS_SELISIH = 1;
                          MDR_NOW_CPS = MDR_TRANSAKSI;
                        }
                      }

                      if (MDR_TYPE == 2) {
                        MDR_NOW = (COUNT_NOW * Number(MDR_AMOUNT));
                        if (Number(MDR_NOW.toFixed(0)) == MDR_TRANSAKSI) {
                          MDR_NOW_CPS = MDR_NOW;
                        } else {
                          HAS_SELISIH = 1;
                          MDR_NOW_CPS = MDR_TRANSAKSI;
                        }
                      }

                    }
                  } else {
                    if (MDR_TYPE == 1) {
                      MDR_NOW = (AMOUNT_NOW * Number(MDR_AMOUNT));
                      MDR_NOW_CPS = (AMOUNT_CPS * Number(MDR_AMOUNT));
                    }

                    if (MDR_TYPE == 2) {
                      MDR_NOW = (COUNT_NOW * Number(MDR_AMOUNT));
                      MDR_NOW_CPS = (COUNT_NOW_CPS * Number(MDR_AMOUNT));
                    }
                  }

                } else {
                  MDR_NOW = 0;
                  MDR_NOW_CPS = 0;
                }
              } else {
                MDR_NOW = 0;
                MDR_NOW_CPS = 0;
              }
            } else {
              if (KEY_CONFIG.MDR != "") {
                if (RAW_TRANSAKSI[KEY_CONFIG.MDR] != undefined) {
                  MDR_NOW = (COUNT_NOW * Number(RAW_TRANSAKSI[KEY_CONFIG.MDR]));
                  MDR_NOW_CPS = (COUNT_NOW_CPS * Number(RAW_TRANSAKSI[KEY_CONFIG.MDR]));
                } else {
                  MDR_NOW = 0;
                  MDR_NOW_CPS = 0;
                }
              } else {
                MDR_NOW = 0;
                MDR_NOW_CPS = 0;
              }
            }

            DATA_BY_GROUP[MPG_ID].MDR += MDR_NOW;
            DATA_BY_GROUP_CPS[MPG_ID].MDR += MDR_NOW_CPS;
            DATA_BY_SUMMARY[KEY_SUMMARY_GROUP].MDR += MDR_NOW;
            DATA_BY_SUMMARY_CPS[KEY_SUMMARY_GROUP].MDR += MDR_NOW_CPS;

            DATA_BY_GROUP[MPG_ID].HAS_SELISIH += HAS_SELISIH;
            DATA_BY_GROUP_CPS[MPG_ID].HAS_SELISIH += HAS_SELISIH;
            DATA_BY_SUMMARY[KEY_SUMMARY_GROUP].HAS_SELISIH += HAS_SELISIH;
            DATA_BY_SUMMARY_CPS[KEY_SUMMARY_GROUP].HAS_SELISIH += HAS_SELISIH;



            DATA_BY_GROUP[MPG_ID].DPP += MDR_NOW / MDR_DPP;
            DATA_BY_GROUP[MPG_ID].PPN += ((MDR_NOW / MDR_DPP) * (MDR_PPN / 100));
            DATA_BY_GROUP[MPG_ID].PPH += ((MDR_NOW / MDR_DPP) * (MDR_PPH / 100));

            DATA_BY_SUMMARY[KEY_SUMMARY_GROUP].DPP += MDR_NOW / MDR_DPP;
            DATA_BY_SUMMARY[KEY_SUMMARY_GROUP].PPN += ((MDR_NOW / MDR_DPP) * (MDR_PPN / 100));
            DATA_BY_SUMMARY[KEY_SUMMARY_GROUP].PPH += ((MDR_NOW / MDR_DPP) * (MDR_PPH / 100));

            DATA_BY_GROUP_CPS[MPG_ID].DPP += MDR_NOW_CPS / MDR_DPP;
            DATA_BY_GROUP_CPS[MPG_ID].PPN += ((MDR_NOW_CPS / MDR_DPP) * (MDR_PPN / 100));
            DATA_BY_GROUP_CPS[MPG_ID].PPH += ((MDR_NOW_CPS / MDR_DPP) * (MDR_PPH / 100));

            DATA_BY_SUMMARY_CPS[KEY_SUMMARY_GROUP].DPP += MDR_NOW_CPS / MDR_DPP;
            DATA_BY_SUMMARY_CPS[KEY_SUMMARY_GROUP].PPN += ((MDR_NOW_CPS / MDR_DPP) * (MDR_PPN / 100));
            DATA_BY_SUMMARY_CPS[KEY_SUMMARY_GROUP].PPH += ((MDR_NOW_CPS / MDR_DPP) * (MDR_PPH / 100));


            if (DATA_BY_DATASOURCE[MPG_ID] == undefined) {
              DATA_BY_DATASOURCE[MPG_ID] = {};
            }

            for (var INDEX_CHECK_VAR in VARIABLE_BY_KEY) {
              var TEXT_VALUE: any; TEXT_VALUE = "";
              switch (VARIABLE_BY_KEY[INDEX_CHECK_VAR]["MPV_TYPE_ID"]) {
                case 1:
                  var VALUE_NOMOR = RAW_TRANSAKSI[INDEX_CHECK_VAR].split(",").join("");
                  TEXT_VALUE = Number(VALUE_NOMOR);
                  break;
                case 2:
                  TEXT_VALUE = RAW_TRANSAKSI[INDEX_CHECK_VAR].toString();
                  break;
                case 3:
                  TEXT_VALUE = RAW_TRANSAKSI[INDEX_CHECK_VAR].toString();
                  break;
                case 4:
                  TEXT_VALUE = RAW_TRANSAKSI[INDEX_CHECK_VAR].toString();
                  break;
                case 5:
                  TEXT_VALUE = RAW_TRANSAKSI[INDEX_CHECK_VAR].toString();
                  break;
                case 6:
                  TEXT_VALUE = JSON.stringify(RAW_TRANSAKSI[INDEX_CHECK_VAR]);
                  break;
                default:
                  TEXT_VALUE = RAW_TRANSAKSI[INDEX_CHECK_VAR].toString();
                  break;
              }

              var KEY_DATA_SOURCE = PERIODE + "_" + M_ID + "_" + MP_ID + "_" + MPG_ID;
              var DATA_RAW: any; DATA_RAW = {};
              DATA_RAW["PERIODE"] = PERIODE;
              DATA_RAW["DATA_ID"] = RAW_TRANSAKSI[KEY_CONFIG.KEY];
              DATA_RAW["MPD_VALUE"] = TEXT_VALUE;// RAW_VARIABLES.TYPE == 4 ? RAW_BIGQUERY[RAW_VARIABLES.KEY]["value"] : RAW_BIGQUERY[RAW_VARIABLES.KEY];
              DATA_RAW["MPD_TEXT"] = TEXT_VALUE;//RAW_VARIABLES.TYPE == 4 ? RAW_BIGQUERY[RAW_VARIABLES.KEY]["value"] : RAW_BIGQUERY[RAW_VARIABLES.KEY];
              DATA_RAW["START_TIME_RUN"] = KEY_DATA_SOURCE;
              DATA_RAW["M_ID"] = M_ID;
              DATA_RAW["MP_ID"] = MP_ID;
              DATA_RAW["MPG_ID"] = MPG_ID;
              DATA_RAW["MPV_ID"] = Number(VARIABLE_BY_KEY[INDEX_CHECK_VAR]["MPV_ID"]);
              DATA_RAW["AT_CREATE"] = ControllerConfig.onCurrentTime().toString();
              DATA_RAW["AT_FLAG"] = 1;
              DATA_RAW["TYPE_DATA"] = 2;
              DATA_RAW["AT_WHO"] = USER_ID;
              DATA_RAW["SPL_ID"] = IMPORT_ID;

              PROCESS_TRANSAKSI_COUNT++;
              if (PROCESS_TRANSAKSI_COUNT % 500 == 0) {
                PROCESS_GROUP++;
              }
              if (DATA_BY_DATASOURCE[MPG_ID][PROCESS_GROUP] == undefined) {
                DATA_BY_DATASOURCE[MPG_ID][PROCESS_GROUP] = [];
              }
              DATA_BY_DATASOURCE[MPG_ID][PROCESS_GROUP].push(DATA_RAW);

            }
          }
        }

        // console.log("PRODUCT GROUP", DATA_BY_GROUP);
        // console.log("PRODUCT GROUP CPS", DATA_BY_GROUP_CPS);

        // console.log("SUMMARY GROUP", DATA_BY_SUMMARY);
        // console.log("SUMMARY GROUP CPS", DATA_BY_SUMMARY_CPS);



        //INSERT FOR BAR
        var TOTAL_TRANSAKCTION = {COUNT_TRX: 0, AMOUNT: 0, MDR: 0, DPP: 0, PPN: 0, PPH: 0};


        //CALCULATE TRANSACTION
        var SELISIH = 0;
        for (var I_PG in DATA_BY_GROUP) {
          var RAW_CALCULATE = DATA_BY_GROUP[I_PG];
          TOTAL_TRANSAKCTION.COUNT_TRX += RAW_CALCULATE.COUNT_TRX;
          TOTAL_TRANSAKCTION.AMOUNT += RAW_CALCULATE.AMOUNT;
          TOTAL_TRANSAKCTION.MDR += RAW_CALCULATE.MDR;
          TOTAL_TRANSAKCTION.DPP += RAW_CALCULATE.DPP;
          TOTAL_TRANSAKCTION.PPN += RAW_CALCULATE.PPN;
          TOTAL_TRANSAKCTION.PPH += RAW_CALCULATE.PPH;

          if (RAW_CALCULATE.HAS_SELISIH == 1) {
            SELISIH = 1;
          }
        }

        var TOTAL_TRANSAKCTION_CPS = {COUNT_TRX: 0, AMOUNT: 0, MDR: 0, DPP: 0, PPN: 0, PPH: 0};
        for (var I_PG in DATA_BY_GROUP_CPS) {
          var RAW_CALCULATE_CPS = DATA_BY_GROUP_CPS[I_PG];
          TOTAL_TRANSAKCTION_CPS.COUNT_TRX += RAW_CALCULATE_CPS.COUNT_TRX;
          TOTAL_TRANSAKCTION_CPS.AMOUNT += RAW_CALCULATE_CPS.AMOUNT;
          TOTAL_TRANSAKCTION_CPS.MDR += RAW_CALCULATE_CPS.MDR;
          TOTAL_TRANSAKCTION_CPS.DPP += RAW_CALCULATE_CPS.DPP;
          TOTAL_TRANSAKCTION_CPS.PPN += RAW_CALCULATE_CPS.PPN;
          TOTAL_TRANSAKCTION_CPS.PPH += RAW_CALCULATE_CPS.PPH;
        }

        var MASTER_SELISH: string; MASTER_SELISH = "";
        if (SELISIH == 1) {
          var CREATE_MASTER_SELISH = await this.merchantProductMasterRepository.create({
            PERIODE: PERIODE,
            MPM_COUNT_TRX: TOTAL_TRANSAKCTION.COUNT_TRX - TOTAL_TRANSAKCTION_CPS.COUNT_TRX,
            MPM_AMOUNT: TOTAL_TRANSAKCTION.AMOUNT - TOTAL_TRANSAKCTION_CPS.AMOUNT,
            MPM_MDR: TOTAL_TRANSAKCTION.MDR - TOTAL_TRANSAKCTION_CPS.MDR,
            MPM_DPP: TOTAL_TRANSAKCTION.DPP - TOTAL_TRANSAKCTION_CPS.DPP,
            MPM_PPN: TOTAL_TRANSAKCTION.PPN - TOTAL_TRANSAKCTION_CPS.PPN,
            MPM_PPH: TOTAL_TRANSAKCTION.PPH - TOTAL_TRANSAKCTION_CPS.PPH,
            MPM_TOTAL: TOTAL_TRANSAKCTION.MDR - TOTAL_TRANSAKCTION_CPS.MDR,
            M_ID: M_ID,
            MP_ID: MP_ID,
            IS_SELISIH: 1,
            START_TIME_RUN: "EXCEL_" + IMPORT_ID.toString(),
            AT_CREATE: ControllerConfig.onCurrentTime().toString(),
            AT_UPDATE: undefined,
            AT_WHO: USER_ID,
            AT_POSISI: 1,
            AT_FLAG: 0
          });
          MASTER_SELISH = CREATE_MASTER_SELISH.MPM_ID == undefined ? "" : CREATE_MASTER_SELISH.MPM_ID;
        }

        var CREATE_MASTER = await this.merchantProductMasterRepository.create({
          PERIODE: PERIODE,
          MPM_COUNT_TRX: TOTAL_TRANSAKCTION.COUNT_TRX,
          MPM_AMOUNT: TOTAL_TRANSAKCTION.AMOUNT,
          MPM_MDR: TOTAL_TRANSAKCTION.MDR,
          MPM_DPP: TOTAL_TRANSAKCTION.DPP,
          MPM_PPN: TOTAL_TRANSAKCTION.PPN,
          MPM_PPH: TOTAL_TRANSAKCTION.PPH,
          MPM_TOTAL: TOTAL_TRANSAKCTION.MDR,
          M_ID: M_ID,
          MP_ID: MP_ID,
          IS_SELISIH: 0,
          START_TIME_RUN: "EXCEL_" + IMPORT_ID.toString(),
          AT_CREATE: ControllerConfig.onCurrentTime().toString(),
          AT_UPDATE: undefined,
          AT_WHO: USER_ID,
          AT_POSISI: 1,
          AT_FLAG: 0
        });

        var INSERT_GROUP: any; INSERT_GROUP = [];
        var INSERT_GROUP_SELISIH: any; INSERT_GROUP_SELISIH = [];
        for (var I_PG in DATA_BY_GROUP) {
          var RAW_DBG = DATA_BY_GROUP[I_PG];
          var RAW_DBG_CPS = DATA_BY_GROUP_CPS[I_PG];

          if (SELISIH == 1) {
            INSERT_GROUP_SELISIH.push({
              PERIODE: PERIODE,
              LINKAJA_FEE: RAW_DBG.FEE,
              LINKAJA_COUNT_TRX: Math.round(Number(RAW_DBG.COUNT_TRX.toFixed(2))) - Math.round(Number(RAW_DBG_CPS.COUNT_TRX.toFixed(2))),
              LINKAJA_AMOUNT: Math.round(Number(RAW_DBG.AMOUNT.toFixed(2))) - Math.round(Number(RAW_DBG_CPS.AMOUNT.toFixed(2))),
              LINKAJA_MDR: Math.round(Number(RAW_DBG.MDR.toFixed(2))) - Math.round(Number(RAW_DBG_CPS.MDR.toFixed(2))),
              LINKAJA_DPP: Math.round(Number(RAW_DBG.DPP.toFixed(2))) - Math.round(Number(RAW_DBG_CPS.DPP.toFixed(2))),
              LINKAJA_PPN: Math.round(Number(RAW_DBG.PPN.toFixed(2))) - Math.round(Number(RAW_DBG_CPS.PPN.toFixed(2))),
              LINKAJA_PPH: Math.round(Number(RAW_DBG.PPH.toFixed(2))) - Math.round(Number(RAW_DBG_CPS.PPH.toFixed(2))),
              LINKAJA_TOTAL: Math.round(Number(RAW_DBG.MDR.toFixed(2))) - Math.round(Number(RAW_DBG_CPS.MDR.toFixed(2))),
              APP_FEE: Math.round(Number(RAW_DBG.FEE.toFixed(2))),
              APP_COUNT_TRX: Math.round(Number(RAW_DBG.COUNT_TRX.toFixed(2))) - Math.round(Number(RAW_DBG_CPS.COUNT_TRX.toFixed(2))),
              APP_AMOUNT: Math.round(Number(RAW_DBG.AMOUNT.toFixed(2))) - Math.round(Number(RAW_DBG_CPS.AMOUNT.toFixed(2))),
              APP_MDR: Math.round(Number(RAW_DBG.MDR.toFixed(2))) - Math.round(Number(RAW_DBG_CPS.MDR.toFixed(2))),
              APP_DPP: Math.round(Number(RAW_DBG.DPP.toFixed(2))) - Math.round(Number(RAW_DBG_CPS.DPP.toFixed(2))),
              APP_PPN: Math.round(Number(RAW_DBG.PPN.toFixed(2))) - Math.round(Number(RAW_DBG_CPS.PPN.toFixed(2))),
              APP_PPH: Math.round(Number(RAW_DBG.PPH.toFixed(2))) - Math.round(Number(RAW_DBG_CPS.PPH.toFixed(2))),
              APP_TOTAL: Math.round(Number(RAW_DBG.MDR.toFixed(2))) - Math.round(Number(RAW_DBG_CPS.MDR.toFixed(2))),
              HAS_SELISIH: 1,
              START_TIME_RUN: "MANUAL_" + IMPORT_ID.toString(),
              M_ID: M_ID,
              MP_ID: MP_ID,
              MPG_ID: I_PG,
              MPM_ID: MASTER_SELISH,
              MPM_SELISIH_ID: CREATE_MASTER.MPM_ID,
              TYPE_DATA: 1,
              AT_CREATE: ControllerConfig.onCurrentTime().toString(),
              AT_FLAG: 0,
              AT_WHO: USER_ID,
              MPS_LABEL: RAW_DBG.LABEL.toString()
            });
          }

          INSERT_GROUP.push({
            PERIODE: PERIODE,
            LINKAJA_FEE: RAW_DBG.FEE,
            LINKAJA_COUNT_TRX: Math.round(Number(RAW_DBG.COUNT_TRX.toFixed(2))),
            LINKAJA_AMOUNT: Math.round(Number(RAW_DBG.AMOUNT.toFixed(2))),
            LINKAJA_MDR: Math.round(Number(RAW_DBG.MDR.toFixed(2))),
            LINKAJA_DPP: Math.round(Number(RAW_DBG.DPP.toFixed(2))),
            LINKAJA_PPN: Math.round(Number(RAW_DBG.PPN.toFixed(2))),
            LINKAJA_PPH: Math.round(Number(RAW_DBG.PPH.toFixed(2))),
            LINKAJA_TOTAL: Math.round(Number(RAW_DBG.MDR.toFixed(2))),
            APP_FEE: Math.round(Number(RAW_DBG.FEE.toFixed(2))),
            APP_COUNT_TRX: Math.round(Number(RAW_DBG.COUNT_TRX.toFixed(2))),
            APP_AMOUNT: Math.round(Number(RAW_DBG.AMOUNT.toFixed(2))),
            APP_MDR: Math.round(Number(RAW_DBG.MDR.toFixed(2))),
            APP_DPP: Math.round(Number(RAW_DBG.DPP.toFixed(2))),
            APP_PPN: Math.round(Number(RAW_DBG.PPN.toFixed(2))),
            APP_PPH: Math.round(Number(RAW_DBG.PPH.toFixed(2))),
            APP_TOTAL: Math.round(Number(RAW_DBG.MDR.toFixed(2))),
            HAS_SELISIH: 0,
            START_TIME_RUN: "MANUAL_" + IMPORT_ID.toString(),
            M_ID: M_ID,
            MP_ID: MP_ID,
            MPG_ID: I_PG,
            MPM_ID: CREATE_MASTER.MPM_ID,
            MPM_SELISIH_ID: undefined,
            TYPE_DATA: 1,
            AT_CREATE: ControllerConfig.onCurrentTime().toString(),
            AT_FLAG: 0,
            AT_WHO: USER_ID,
            MPS_LABEL: RAW_DBG.LABEL.toString()
          });
        }

        //INSERT FOR LAMPIRAN
        var INSERT_SUMMARY: any; INSERT_SUMMARY = [];
        var INSERT_SUMMARY_SELISIH: any; INSERT_SUMMARY_SELISIH = [];
        for (var I_SG in DATA_BY_SUMMARY) {
          var RAW_DBS = DATA_BY_SUMMARY[I_SG];
          var RAW_DBS_CPS = DATA_BY_SUMMARY_CPS[I_SG];

          if (SELISIH == 1) {
            INSERT_SUMMARY_SELISIH.push({
              PERIODE: PERIODE,
              LINKAJA_FEE: RAW_DBS.FEE,
              LINKAJA_COUNT_TRX: Math.round(Number(RAW_DBS.COUNT_TRX.toFixed(2))) - Math.round(Number(RAW_DBS_CPS.COUNT_TRX.toFixed(2))),
              LINKAJA_AMOUNT: Math.round(Number(RAW_DBS.AMOUNT.toFixed(2))) - Math.round(Number(RAW_DBS_CPS.AMOUNT.toFixed(2))),
              LINKAJA_MDR: Math.round(Number(RAW_DBS.MDR.toFixed(2))) - Math.round(Number(RAW_DBS_CPS.MDR.toFixed(2))),
              LINKAJA_DPP: Math.round(Number(RAW_DBS.DPP.toFixed(2))) - Math.round(Number(RAW_DBS_CPS.DPP.toFixed(2))),
              LINKAJA_PPN: Math.round(Number(RAW_DBS.PPN.toFixed(2))) - Math.round(Number(RAW_DBS_CPS.PPN.toFixed(2))),
              LINKAJA_PPH: Math.round(Number(RAW_DBS.PPH.toFixed(2))) - Math.round(Number(RAW_DBS_CPS.PPH.toFixed(2))),
              LINKAJA_TOTAL: Math.round(Number(RAW_DBS.MDR.toFixed(2))) - Math.round(Number(RAW_DBS_CPS.MDR.toFixed(2))),
              APP_FEE: Math.round(Number(RAW_DBS.FEE.toFixed(2))),
              APP_COUNT_TRX: Math.round(Number(RAW_DBS.COUNT_TRX.toFixed(2))) - Math.round(Number(RAW_DBS_CPS.COUNT_TRX.toFixed(2))),
              APP_AMOUNT: Math.round(Number(RAW_DBS.AMOUNT.toFixed(2))) - Math.round(Number(RAW_DBS_CPS.AMOUNT.toFixed(2))),
              APP_MDR: Math.round(Number(RAW_DBS.MDR.toFixed(2))) - Math.round(Number(RAW_DBS_CPS.MDR.toFixed(2))),
              APP_DPP: Math.round(Number(RAW_DBS.DPP.toFixed(2))) - Math.round(Number(RAW_DBS_CPS.DPP.toFixed(2))),
              APP_PPN: Math.round(Number(RAW_DBS.PPN.toFixed(2))) - Math.round(Number(RAW_DBS_CPS.PPN.toFixed(2))),
              APP_PPH: Math.round(Number(RAW_DBS.PPH.toFixed(2))) - Math.round(Number(RAW_DBS_CPS.PPH.toFixed(2))),
              APP_TOTAL: Math.round(Number(RAW_DBS.MDR.toFixed(2))) - Math.round(Number(RAW_DBS_CPS.MDR.toFixed(2))),
              HAS_SELISIH: 0,
              START_TIME_RUN: "MANUAL_" + IMPORT_ID.toString(),
              M_ID: M_ID,
              MP_ID: MP_ID,
              MPM_ID: MASTER_SELISH,
              MPM_SELISIH_ID: CREATE_MASTER.MPM_ID,
              TYPE_DATA: 1,
              AT_CREATE: ControllerConfig.onCurrentTime().toString(),
              AT_FLAG: 0,
              AT_WHO: USER_ID,
              MPS_LABEL: RAW_DBS.LABEL.toString()
            });
          }

          INSERT_SUMMARY.push({
            PERIODE: PERIODE,
            LINKAJA_FEE: RAW_DBS.FEE,
            LINKAJA_COUNT_TRX: Math.round(Number(RAW_DBS.COUNT_TRX.toFixed(2))),
            LINKAJA_AMOUNT: Math.round(Number(RAW_DBS.AMOUNT.toFixed(2))),
            LINKAJA_MDR: Math.round(Number(RAW_DBS.MDR.toFixed(2))),
            LINKAJA_DPP: Math.round(Number(RAW_DBS.DPP.toFixed(2))),
            LINKAJA_PPN: Math.round(Number(RAW_DBS.PPN.toFixed(2))),
            LINKAJA_PPH: Math.round(Number(RAW_DBS.PPH.toFixed(2))),
            LINKAJA_TOTAL: Math.round(Number(RAW_DBS.MDR.toFixed(2))),
            APP_FEE: Math.round(Number(RAW_DBS.FEE.toFixed(2))),
            APP_COUNT_TRX: Math.round(Number(RAW_DBS.COUNT_TRX.toFixed(2))),
            APP_AMOUNT: Math.round(Number(RAW_DBS.AMOUNT.toFixed(2))),
            APP_MDR: Math.round(Number(RAW_DBS.MDR.toFixed(2))),
            APP_DPP: Math.round(Number(RAW_DBS.DPP.toFixed(2))),
            APP_PPN: Math.round(Number(RAW_DBS.PPN.toFixed(2))),
            APP_PPH: Math.round(Number(RAW_DBS.PPH.toFixed(2))),
            APP_TOTAL: Math.round(Number(RAW_DBS.MDR.toFixed(2))),
            HAS_SELISIH: 0,
            START_TIME_RUN: "MANUAL_" + IMPORT_ID.toString(),
            M_ID: M_ID,
            MP_ID: MP_ID,
            MPM_ID: CREATE_MASTER.MPM_ID,
            MPM_SELISIH_ID: undefined,
            TYPE_DATA: 1,
            AT_CREATE: ControllerConfig.onCurrentTime().toString(),
            AT_FLAG: 0,
            AT_WHO: USER_ID,
            MPS_LABEL: RAW_DBS.LABEL.toString()
          });
        }

        await this.merchantProductSummaryRepository.createAll(INSERT_GROUP);
        await this.merchantProductSummaryGroupRepository.createAll(INSERT_SUMMARY);

        if (SELISIH == 1) {
          if (INSERT_GROUP_SELISIH.length > 0) {
            await this.merchantProductSummaryRepository.createAll(INSERT_GROUP_SELISIH);
          }
          if (INSERT_SUMMARY_SELISIH.length > 0) {
            await this.merchantProductSummaryGroupRepository.createAll(INSERT_SUMMARY_SELISIH);
          }
        }

        await this.importProcessLogRepository.updateById(IMPORT_ID, {
          IM_COUNT_TRX: TOTAL_TRANSAKCTION.COUNT_TRX,
          IM_TRX_AMOUNT: TOTAL_TRANSAKCTION.AMOUNT,
          IM_MDR: TOTAL_TRANSAKCTION.MDR,
          IM_PROCESS_NOW: NO,
          AT_FLAG: 2,
          IM_MESSAGE: "SUCCESS !",
          AT_UPDATE: ControllerConfig.onCurrentTime().toString(),
        });

        delete PROCESS_SYNC_DATA[PROCESS_KEY];
        delete PROCESS_SYNC[PROCESS_KEY];

      } else {
        await this.importProcessLogRepository.updateById(IMPORT_ID, {
          IM_MDR: 0,
          IM_COUNT_TRX: 0,
          IM_TRX_AMOUNT: 0,
          IM_PROCESS_COUNT: 100,
          IM_PROCESS_NOW: 100,
          AT_FLAG: 3,
          AT_UPDATE: ControllerConfig.onCurrentTime().toString(),
          IM_MESSAGE: "Error, filed for : " + JSON.stringify(FIELD_NOTFOUND) + " not found in template !"
        });

        delete PROCESS_SYNC_DATA[PROCESS_KEY];
        delete PROCESS_SYNC[PROCESS_KEY];
      }
      //JIKA ERROR SAAT MEMBACA FILE
    } else {
      await this.importProcessLogRepository.updateById(IMPORT_ID, {
        IM_MDR: 0,
        IM_COUNT_TRX: 0,
        IM_TRX_AMOUNT: 0,
        IM_PROCESS_COUNT: 100,
        IM_PROCESS_NOW: 100,
        AT_FLAG: 3,
        AT_UPDATE: ControllerConfig.onCurrentTime().toString(),
        IM_MESSAGE: ERROR_READ_MESSAGE
      });
      delete PROCESS_SYNC_DATA[PROCESS_KEY];
      delete PROCESS_SYNC[PROCESS_KEY];
    }
  }

  ON_PROCESS_SYNC = async (PROCESS_KEY: any) => {
    setTimeout(() => {
      console.log("checking....");
      var NO = 0;
      for (var i in PROCESS_SYNC_DATA[PROCESS_KEY]) {NO++;}
      if (NO == 0) {
        delete PROCESS_SYNC_DATA[PROCESS_KEY];
        delete PROCESS_SYNC[PROCESS_KEY];
      } else {
        this.ON_PROCESS_SYNC(PROCESS_KEY);
      }
    }, 5000);
  }

  ON_RUNNING_SYNC = async (SYNC_ID: any) => {

    var Process: any; Process = [];
    if (SYNC_ID == null) {
      Process = await this.importProcessLogRepository.find({where: {and: [{AT_FLAG: 0}]}});
    } else {
      Process = await this.importProcessLogRepository.find({where: {and: [{AT_FLAG: 0}, {IMPORT_ID: {inq: SYNC_ID}}]}});
    }

    for (var I_PROCESS in Process) {
      var LOG_PROCESS = Process[I_PROCESS];
      var KEY_PROCESS = LOG_PROCESS.IM_PERIODE + "_" + LOG_PROCESS.M_ID + "_" + LOG_PROCESS.MP_ID;
      if (PROCESS_SYNC_DATA[KEY_PROCESS] == undefined) {
        PROCESS_SYNC_DATA[KEY_PROCESS] = {};
      }

      PROCESS_SYNC_DATA[KEY_PROCESS] = {PROCESS: LOG_PROCESS, M_ID: LOG_PROCESS.M_ID, MP_ID: LOG_PROCESS.MP_ID, PERIODE: LOG_PROCESS.IM_PERIODE};

      if (PROCESS_SYNC[KEY_PROCESS] == undefined) {
        PROCESS_SYNC[KEY_PROCESS] = new Promise(async (resolve, reject) => {
          var ID = KEY_PROCESS;
          this.ON_PROCESS_CREATE(KEY_PROCESS);
          resolve({success: true, message: "Done !"});
        });
      }
    }
  }


  @post('/import-excel-datasource/upload', {
    responses: {
      '200': {
        description: 'DataFpjp model instance',
        content: {'application/json': {schema: getModelSchemaRef(ImportProcessLog)}},
      },
    },
  })
  async importTransaksiExcel(
    @requestBody.file()
    requestFile: Request,
    @inject(RestBindings.Http.RESPONSE) response: Response,
  ): Promise<Object> {
    var UserId = this.currentUserProfile[securityId];
    return new Promise<object>(async (resolve, reject) => {
      var returnMs: any; returnMs = {success: true, message: "", data: []};
      var BODY_REQ: any; BODY_REQ = {};
      const storage = multer.diskStorage({
        destination: path.join(__dirname, './../../.non-digipos/import'),
        filename: (req, file, cb) => {
          // console.log(file);
          cb(null, ControllerConfig.onCurrentTime().toString() + ".csv");
        },
      });

      const upload = multer({
        storage: storage,
        fileFilter: async (req: any, file: any, callback: any) => {
          var ext = path.extname(file.originalname).toLowerCase();
          if (/*ext !== '.xlsx' && ext !== '.xls' &&*/ ext !== '.csv') {
            return callback("Only document exel, csv and txt are allowed !", false);
          }
          var countName = await this.importProcessLogRepository.count({and: [{IM_NAME: file.originalname}]});
          if (countName.count > 0) {
            return callback("Error, File upload is exities, please upload other file !", false);
          }
          return callback(null, true);
        }
      }).fields([{name: 'IMPORT_UPLOAD', maxCount: 1}]);

      upload(requestFile, response, async (errUpload: any) => {
        BODY_REQ = requestFile.body;
        if (errUpload) {
          resolve({error: true, message: errUpload})
        } else {
          var FILES: any; FILES = requestFile.files;
          var FILE_UPLOAD = FILES.IMPORT_UPLOAD;
          var ROW_BODY: any; ROW_BODY = {};
          var Uploaded = true;

          var REQUIRED_FIELD: any; REQUIRED_FIELD = ['IM_PERIODE', 'M_ID', 'MP_ID'];
          for (var i in requestFile.body) {
            ROW_BODY[i] = requestFile.body[i];
            if (REQUIRED_FIELD.indexOf(i) != -1) {
              REQUIRED_FIELD.splice(REQUIRED_FIELD.indexOf(i), 1);
            }
          }

          if (REQUIRED_FIELD.length == 0) {
            for (var I_UPLOAD in FILE_UPLOAD) {
              var FILE_UPLOADED = FILE_UPLOAD[I_UPLOAD];
              ROW_BODY["IM_NAME"] = FILE_UPLOADED.originalname;
              ROW_BODY["IM_PATH"] = FILE_UPLOADED.filename;
              ROW_BODY["IM_MIME_TYPE"] = FILE_UPLOADED.mimetype;
            }

            ROW_BODY["IM_COUNT_TRX"] = 0;
            ROW_BODY["IM_TRX_AMOUNT"] = 0;
            ROW_BODY["IM_MDR"] = 0;
            ROW_BODY["IM_PROCESS_COUNT"] = 0;
            ROW_BODY["IM_PROCESS_NOW"] = 0;
            ROW_BODY["AT_FLAG"] = 0;
            ROW_BODY["AT_CREATE"] = ControllerConfig.onCurrentTime().toString();
            ROW_BODY["AT_UPDATE"] = undefined;
            ROW_BODY["AT_WHO"] = UserId;
            try {
              var CRETE_UPLOAD_TRX = await this.importProcessLogRepository.create(ROW_BODY);
              returnMs.success = true;
              returnMs.data = CRETE_UPLOAD_TRX;
              returnMs.message = "SUCCESS !";
              this.ON_RUNNING_SYNC([CRETE_UPLOAD_TRX.IMPORT_ID]);
              resolve(returnMs);
            } catch (error) {
              returnMs.success = false;
              returnMs.message = error;
              resolve(returnMs);
            }
          } else {
            returnMs.success = false;
            returnMs.message = "Error, please required field : " + JSON.stringify(REQUIRED_FIELD);
            resolve(returnMs);
          }
        }
      });
    });
  }


  @post('/import-excel-datasource/re-running/not-process', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: ReRunningImportLogBody
          }
        },
      },
    },
  })
  async sychBigQueryRunLog(
    @requestBody(ReRunningImportLogBody) CreateSyncGroupRequestBody: {IMPORT_ID: []},
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};
    if (CreateSyncGroupRequestBody.IMPORT_ID.length == 0) {
      this.ON_RUNNING_SYNC(null);
    } else {
      this.ON_RUNNING_SYNC(CreateSyncGroupRequestBody.IMPORT_ID);
    }
    return result;
  }

  @get('/import-excel-datasource/views', {
    responses: {
      '200': {
        description: 'Array of Merchant Product Views model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(ImportProcessLog, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async findImportExcel(
    @param.filter(ImportProcessLog) filter?: Filter<ImportProcessLog>,
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: 'Success !', datas: []};
    var MERCHANT_IMPORT = await this.importProcessLogRepository.find(filter);
    result.datas = MERCHANT_IMPORT
    return result;
  }



  @post('/manual-transaksi/create-manual', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: CreateManualTrxBody
          }
        },
      },
    },
  })
  async createManualTransaksi(
    @requestBody(CreateManualTrxBody) createManualTrxBody: {PERIODE: string, COUNT_TRX: number, AMOUNT: number, MDR: number, M_ID: number, MP_ID: number, MPG_ID: number},
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};

    var MERCHANT = await this.merchantRepository.findOne({where: {M_ID: createManualTrxBody.M_ID}});
    var MERCHANT_PRODUCT = await this.merchantProductRepository.findOne({where: {MP_ID: createManualTrxBody.MP_ID}});
    var GROUP_TRX = await this.merchantProductGroupRepository.find({where: {and: [{AT_FLAG: 1}, {M_ID: createManualTrxBody.M_ID}, {MP_ID: createManualTrxBody.MP_ID}]}});

    var DATA_GROUP: any; DATA_GROUP = {};
    for (var I_GROUP in GROUP_TRX) {
      var RAW_GROUP = GROUP_TRX[I_GROUP];
      var GROUP_ID = RAW_GROUP.MPG_ID == undefined ? -1 : RAW_GROUP.MPG_ID;
      if (DATA_GROUP[GROUP_ID] == undefined) {
        DATA_GROUP[GROUP_ID] = RAW_GROUP;
      }
    }

    if (DATA_GROUP[createManualTrxBody.MPG_ID] != undefined) {

      if (DATA_CREATE_MANUAL[createManualTrxBody.PERIODE] == undefined) {
        DATA_CREATE_MANUAL[createManualTrxBody.PERIODE] = {};
      }

      if (DATA_CREATE_MANUAL[createManualTrxBody.PERIODE][createManualTrxBody.MPG_ID] == undefined) {
        DATA_CREATE_MANUAL[createManualTrxBody.PERIODE][createManualTrxBody.MPG_ID] = createManualTrxBody
      }

      var ALL_AVA: any; ALL_AVA = [];
      for (var I_GROUP in GROUP_TRX) {
        var RAW_GROUP = GROUP_TRX[I_GROUP];
        var GROUP_ID = RAW_GROUP.MPG_ID == undefined ? -1 : RAW_GROUP.MPG_ID;
        if (DATA_CREATE_MANUAL[createManualTrxBody.PERIODE] != undefined) {
          if (DATA_CREATE_MANUAL[createManualTrxBody.PERIODE][GROUP_ID] != undefined) {
            ALL_AVA.push(1);
          } else {
            ALL_AVA.push(0);
          }
        } else {
          ALL_AVA.push(0);
        }
      }

      if (ALL_AVA.length > 0) {
        if (ALL_AVA.indexOf(0) == -1) {
          //FINISHING DATA CREATE MANUALY

          var SUMMARY_STORE: any; SUMMARY_STORE = [];
          var SUMMARY_GROUP_STORE: any; SUMMARY_GROUP_STORE = [];


          var TOTAL_CALCULATE: any; TOTAL_CALCULATE = {COUNT: 0, MDR: 0, AMOUNT: 0, DPP: 0, PPN: 0, PPH: 0, TOTAL: 0, FEE: 0};
          for (var i in DATA_CREATE_MANUAL) {
            var RAW_TRANSAKSI_PERIODE = DATA_CREATE_MANUAL[i];
            for (var j in RAW_TRANSAKSI_PERIODE) {
              var RAW_TRANSAKSI = RAW_TRANSAKSI_PERIODE[j];

              var MDR_TYPE = DATA_GROUP[j].MPG_MDR_TYPE;//1 : PERCENT - 2 : AMOUNT
              var MDR_AMOUNT = DATA_GROUP[j].MPG_MDR_AMOUNT;
              var MDR_DPP = DATA_GROUP[j].MPG_DPP;
              var MDR_PPN = DATA_GROUP[j].MPG_PPN;
              var MDR_PPH = DATA_GROUP[j].MPG_PPH;



              var TRANSAKSI_COUNT = RAW_TRANSAKSI.COUNT_TRX;
              var TRANSAKSI_AMOUNT = RAW_TRANSAKSI.AMOUNT;
              var TRANSAKSI_MDR = RAW_TRANSAKSI.MDR;
              var TOTAL = TRANSAKSI_COUNT * TRANSAKSI_MDR;
              var TRANSAKSI_DPP = TOTAL / MDR_DPP;
              var TRANSAKSI_PPN = TRANSAKSI_DPP * (MDR_PPN / 100);
              var TRANSAKSI_PPH = TRANSAKSI_DPP * (MDR_PPH / 100);
              var TRANSAKSI_TOTAL = TRANSAKSI_DPP + TRANSAKSI_PPN;

              TOTAL_CALCULATE.COUNT += TRANSAKSI_COUNT;
              TOTAL_CALCULATE.MDR += TRANSAKSI_MDR;
              TOTAL_CALCULATE.AMOUNT += TRANSAKSI_AMOUNT;
              TOTAL_CALCULATE.DPP += TRANSAKSI_DPP;
              TOTAL_CALCULATE.PPN += TRANSAKSI_PPN;
              TOTAL_CALCULATE.PPH += TRANSAKSI_PPH;
              TOTAL_CALCULATE.TOTAL += TRANSAKSI_TOTAL;
              TOTAL_CALCULATE.FEE = MDR_AMOUNT;

              var CREATE_SUMMARY = await this.merchantProductSummaryRepository.create({
                PERIODE: i,
                LINKAJA_FEE: MDR_AMOUNT,
                LINKAJA_COUNT_TRX: Math.round(Number(TRANSAKSI_COUNT.toFixed(2))),
                LINKAJA_AMOUNT: Math.round(Number(TRANSAKSI_AMOUNT.toFixed(2))),
                LINKAJA_MDR: Math.round(Number(TRANSAKSI_MDR.toFixed(2))),
                LINKAJA_DPP: Math.round(Number(TRANSAKSI_DPP.toFixed(2))),
                LINKAJA_PPN: Math.round(Number(TRANSAKSI_PPN.toFixed(2))),
                LINKAJA_PPH: Math.round(Number(TRANSAKSI_PPH.toFixed(2))),
                LINKAJA_TOTAL: Math.round(Number(TRANSAKSI_TOTAL.toFixed(2))),
                APP_FEE: Math.round(Number(MDR_AMOUNT.toFixed(2))),
                APP_COUNT_TRX: Math.round(Number(TRANSAKSI_COUNT.toFixed(2))),
                APP_AMOUNT: Math.round(Number(TRANSAKSI_AMOUNT.toFixed(2))),
                APP_MDR: Math.round(Number(TRANSAKSI_MDR.toFixed(2))),
                APP_DPP: Math.round(Number(TRANSAKSI_DPP.toFixed(2))),
                APP_PPN: Math.round(Number(TRANSAKSI_PPN.toFixed(2))),
                APP_PPH: Math.round(Number(TRANSAKSI_PPH.toFixed(2))),
                APP_TOTAL: Math.round(Number(TRANSAKSI_TOTAL.toFixed(2))),
                HAS_SELISIH: 0,
                START_TIME_RUN: "MANUAL",
                M_ID: RAW_TRANSAKSI.M_ID,
                MP_ID: RAW_TRANSAKSI.MP_ID,
                MPG_ID: RAW_TRANSAKSI.MPG_ID,
                MPM_ID: undefined,
                MPM_SELISIH_ID: undefined,
                TYPE_DATA: 1,
                AT_CREATE: ControllerConfig.onCurrentTime().toString(),
                AT_FLAG: 0,
                AT_WHO: USER_ID,
                MPS_LABEL: DATA_GROUP[j]["MPG_LABEL"]
              });
              SUMMARY_STORE.push(CREATE_SUMMARY.SUMMARY_ID);

              var CREATE_SUMMARY_GROUP = await this.merchantProductSummaryGroupRepository.create({
                PERIODE: i,
                LINKAJA_FEE: MDR_AMOUNT,
                LINKAJA_COUNT_TRX: Math.round(Number(TRANSAKSI_COUNT.toFixed(2))),
                LINKAJA_AMOUNT: Math.round(Number(TRANSAKSI_AMOUNT.toFixed(2))),
                LINKAJA_MDR: Math.round(Number(TRANSAKSI_MDR.toFixed(2))),
                LINKAJA_DPP: Math.round(Number(TRANSAKSI_DPP.toFixed(2))),
                LINKAJA_PPN: Math.round(Number(TRANSAKSI_PPN.toFixed(2))),
                LINKAJA_PPH: Math.round(Number(TRANSAKSI_PPH.toFixed(2))),
                LINKAJA_TOTAL: Math.round(Number(TRANSAKSI_TOTAL.toFixed(2))),
                APP_FEE: Math.round(Number(MDR_AMOUNT.toFixed(2))),
                APP_COUNT_TRX: Math.round(Number(TRANSAKSI_COUNT.toFixed(2))),
                APP_AMOUNT: Math.round(Number(TRANSAKSI_AMOUNT.toFixed(2))),
                APP_MDR: Math.round(Number(TRANSAKSI_MDR.toFixed(2))),
                APP_DPP: Math.round(Number(TRANSAKSI_DPP.toFixed(2))),
                APP_PPN: Math.round(Number(TRANSAKSI_PPN.toFixed(2))),
                APP_PPH: Math.round(Number(TRANSAKSI_PPH.toFixed(2))),
                APP_TOTAL: Math.round(Number(TRANSAKSI_TOTAL.toFixed(2))),
                HAS_SELISIH: 0,
                START_TIME_RUN: "MANUAL",
                M_ID: RAW_TRANSAKSI.M_ID,
                MP_ID: RAW_TRANSAKSI.MP_ID,
                MPM_ID: undefined,
                MPM_SELISIH_ID: undefined,
                TYPE_DATA: 1,
                AT_CREATE: ControllerConfig.onCurrentTime().toString(),
                AT_FLAG: 0,
                AT_WHO: USER_ID,
                MPS_LABEL: DATA_GROUP[j]["MPG_LABEL"]
              });
              SUMMARY_GROUP_STORE.push(CREATE_SUMMARY_GROUP.MPSG_ID);
            }
          }

          var CREATE_MASTER = await this.merchantProductMasterRepository.create({
            PERIODE: createManualTrxBody.PERIODE,
            MPM_COUNT_TRX: TOTAL_CALCULATE.COUNT,
            MPM_AMOUNT: TOTAL_CALCULATE.AMOUNT,
            MPM_MDR: TOTAL_CALCULATE.MDR,
            MPM_DPP: TOTAL_CALCULATE.DPP,
            MPM_PPN: TOTAL_CALCULATE.PPN,
            MPM_PPH: TOTAL_CALCULATE.PPH,
            MPM_TOTAL: TOTAL_CALCULATE.TOTAL,
            M_ID: createManualTrxBody.M_ID,
            MP_ID: createManualTrxBody.MP_ID,
            IS_SELISIH: 0,
            START_TIME_RUN: "MANUAL",
            AT_CREATE: ControllerConfig.onCurrentTime().toString(),
            AT_UPDATE: undefined,
            AT_WHO: USER_ID,
            AT_POSISI: 1,
            AT_FLAG: 0
          });

          await this.merchantProductSummaryRepository.updateAll({MPM_ID: CREATE_MASTER.MPM_ID}, {and: [{SUMMARY_ID: {inq: SUMMARY_STORE}}]});
          await this.merchantProductSummaryGroupRepository.updateAll({MPM_ID: CREATE_MASTER.MPM_ID}, {and: [{MPSG_ID: {inq: SUMMARY_GROUP_STORE}}]});

        }
      }
      result.success = true;
      result.message = "Success !";
    } else {
      result.success = false;
      result.message = "Error, Product group not found !";
    }
    return result;
  }




}
