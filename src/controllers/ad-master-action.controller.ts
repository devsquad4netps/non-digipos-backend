import {authenticate} from '@loopback/authentication';
import {inject} from '@loopback/core';
import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where
} from '@loopback/repository';
import {
  del, get,
  getModelSchemaRef, param,


  patch, post,




  put,

  Request, requestBody, Response, RestBindings
} from '@loopback/rest';
import {SecurityBindings, UserProfile} from '@loopback/security';
import fs from 'fs';
import multer from 'multer';
import path from 'path';
import {FILE_UPLOAD_SERVICE} from '../keys';
import {AdMaster} from '../models';
import {AdMasterRepository, DataDigiposRepository, DataNotificationRepository, UserRepository} from '../repositories';
import {FileUploadHandler} from '../types';




export const UpdateActiveRequestBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['code_ad'],
        properties: {
          code_ad: {
            type: 'array',
            items: {type: 'string'},
          }
        }
      }
    },
  },
};



@authenticate('jwt')
export class AdMasterActionController {
  constructor(
    @repository(AdMasterRepository)
    public adMasterRepository: AdMasterRepository,
    @repository(DataNotificationRepository)
    public dataNotificationRepository: DataNotificationRepository,
    @inject(SecurityBindings.USER)
    public currentUserProfile: UserProfile,
    @repository(UserRepository)
    public userRepository: UserRepository,
    @repository(DataDigiposRepository)
    public dataDigiposRepository: DataDigiposRepository,
    @inject(FILE_UPLOAD_SERVICE) private handler: FileUploadHandler,
  ) { }

  @post('/AdMasterAction/active-status', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: UpdateActiveRequestBody
          }
        },
      },
    },
  })
  async updateStatus(
    @requestBody(UpdateActiveRequestBody) updateStatus: {code_ad: []},
  ): Promise<Object> {
    var result: any; result = {success: true, message: ""};
    var dataAd = await this.adMasterRepository.find({where: {ad_code: {inq: updateStatus.code_ad}}});
    for (var i in dataAd) {
      var raw = dataAd[i];
      try {
        await this.adMasterRepository.updateById(raw.ad_code, {
          ad_status: true
        });
        result.success = true;
        result.message = "success";
      } catch (error) {
        result.success = false;
        result.message = error;
      }
    }
    return result;
  }

  @post('/AdMasterAction', {
    responses: {
      '200': {
        description: 'AdMaster model instance',
        content: {'application/json': {schema: getModelSchemaRef(AdMaster)}},
      },
    },
  })
  async create(
    // @requestBody({
    //   content: {
    //     'application/json': {
    //       schema: getModelSchemaRef(AdMaster, {
    //         title: 'NewAdMaster',
    //         exclude: ['ad_code'],
    //       }),
    //     },
    //   },
    // })
    // adMaster: Omit<AdMaster, 'ad_code'>,
    @requestBody.file()
    requestFile: Request,
    @inject(RestBindings.Http.RESPONSE) response: Response,
  ): Promise<Object> {
    return new Promise<object>(async (resolve, reject) => {

      const storage = multer.diskStorage({
        destination: path.join(__dirname, './../../.digipost/profile_npwp'),
        filename: (req, file, cb) => {
          cb(null, Date.now() + "-" + file.originalname);
        },
      });

      const upload = multer({
        storage: storage,
        fileFilter: async function (req, file, callback) {
          var ext = path.extname(file.originalname).toLowerCase();
          if (ext !== '.pdf') {
            return callback(new Error('Only pdf are allowed'));
          }
          callback(null, true);
        }
      }).fields([{name: 'npwp_attachment', maxCount: 1}, {name: 'pks_attachment', maxCount: 1}]);//.array("npwp_attachment");

      await upload(requestFile, response, async (errUpload: any) => {
        // console.log(requestFile.body);
        if (errUpload) resolve({error: true, message: errUpload})
        else {
          var InfoBody: any; InfoBody = {};
          for (var i in requestFile.body) {
            InfoBody[i] = requestFile.body[i];
          }
          InfoBody["npwp_attachment"] = InfoBody["npwp"] + ".pdf";
          InfoBody["pks_attachment"] = InfoBody["npwp"] + "_PKS" + ".pdf";

          var files: any; files = requestFile.files;
          var file: any; file = "";
          var filePKS: any; filePKS = "";
          for (var iFile in files) {
            var InfiFile = files[iFile];
            for (var iFilePath in InfiFile) {
              if (InfiFile[iFilePath]["fieldname"] == 'npwp_attachment') {
                file = InfiFile[iFilePath];
              }
              if (InfiFile[iFilePath]["fieldname"] == 'pks_attachment') {
                filePKS = InfiFile[iFilePath];
              }
            }
          }

          var Uploaded = true;
          if (file == "") {
            Uploaded = false;
            resolve({error: true, message: "Add master ad error, please upload your npwp !!"});
          }

          if (filePKS == "") {
            Uploaded = false;
            resolve({error: true, message: "Add master ad error, please upload your pks !!"});
          }

          // console.log(file);
          // console.log(filePKS);
          if (filePKS == "" || file == "") {
            if (filePKS != "") {
              try {
                fs.unlinkSync(filePKS.path);
              } catch (err) {
              }
            }
            if (file != "") {
              try {
                fs.unlinkSync(file.path);
              } catch (err) {
              }
            }
          } else {
            try {
              fs.rename(file.path, file.destination + "/" + InfoBody["npwp_attachment"], function (err) {
                if (err) {console.log(err); return;}
                resolve({success: true, message: "success create"});
              });
            } catch (error) {
              Uploaded = false;
              resolve({error: true, message: error});
              try {
                fs.unlinkSync(file.path);
              } catch (err) {
              }
            }
            try {
              fs.rename(filePKS.path, filePKS.destination + "/" + InfoBody["pks_attachment"], function (err) {
                if (err) {console.log(err); return;}
                resolve({success: true, message: "success create"});
              });
            } catch (error) {
              Uploaded = false;
              resolve({error: true, message: error});
              try {
                fs.unlinkSync(filePKS.path);
              } catch (err) {
              }
            }
          }

          if (Uploaded) {
            try {
              console.log(InfoBody);
              await this.adMasterRepository.create(InfoBody);
            } catch (error) {
              try {
                fs.unlinkSync(file.path);
              } catch (err) {
              }
              try {
                fs.unlinkSync(filePKS.path);
              } catch (err) {
              }
              resolve({error: true, message: error});
            }
          }
        }
      });
    });
  }

  @get('/AdMasterAction/count', {
    responses: {
      '200': {
        description: 'AdMaster model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(AdMaster) where?: Where<AdMaster>,
  ): Promise<Count> {
    return this.adMasterRepository.count(where);
  }

  @get('/AdMasterAction', {
    responses: {
      '200': {
        description: 'Array of AdMaster model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(AdMaster, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(AdMaster) filter?: Filter<AdMaster>,
  ): Promise<Object> {
    var info = {"datas": new Array(), "size": 0};
    var filterOrder = {};
    if (filter?.where !== undefined) {
      filterOrder = {"where": filter?.where};
    }
    var infoMasterAD = await this.adMasterRepository.find(filter);
    var sizeMasterAD = await this.adMasterRepository.find(filterOrder);
    info["datas"] = infoMasterAD;
    info["size"] = sizeMasterAD.length;
    return info;

    // return this.adMasterRepository.find(filter);
  }

  @patch('/AdMasterAction', {
    responses: {
      '200': {
        description: 'AdMaster PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(AdMaster, {partial: true}),
        },
      },
    })
    adMaster: AdMaster,
    @param.where(AdMaster) where?: Where<AdMaster>,
  ): Promise<Count> {
    return this.adMasterRepository.updateAll(adMaster, where);
  }

  @get('/AdMasterAction/{id}', {
    responses: {
      '200': {
        description: 'AdMaster model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(AdMaster, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(AdMaster, {exclude: 'where'}) filter?: FilterExcludingWhere<AdMaster>
  ): Promise<AdMaster> {
    return this.adMasterRepository.findById(id, filter);
  }

  @get('/AdMasterAction/UpdateAllStatusZero', {
    responses: {
      '200': {
        description: 'AdMaster model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(AdMaster, {includeRelations: true}),
          },
        },
      },
    },
  })
  async updateAllUpdateZero(
  ): Promise<Object> {
    await this.adMasterRepository.updateAll({ad_status: false})
    return {statue: true, "message": "success"};
  }

  @get('/AdMasterAction/UpdateAllStatusTrue', {
    responses: {
      '200': {
        description: 'AdMaster model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(AdMaster, {includeRelations: true}),
          },
        },
      },
    },
  })
  async updateAllUpdateTrue(
  ): Promise<Object> {
    await this.adMasterRepository.updateAll({ad_status: true})
    return {statue: true, "message": "success"};
  }

  @patch('/AdMasterAction/{id}', {
    responses: {
      '204': {
        description: 'AdMaster PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(AdMaster, {partial: true}),
        },
      },
    })
    adMaster: AdMaster,
  ): Promise<void> {
    await this.adMasterRepository.updateById(id, adMaster);
  }

  @put('/AdMasterAction/{id}', {
    responses: {
      '204': {
        description: 'AdMaster PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    // @requestBody() adMaster: AdMaster,
    @requestBody.file()
    requestFile: Request,
    @inject(RestBindings.Http.RESPONSE) response: Response,
  ): Promise<Object> {
    return new Promise<object>(async (resolve, reject) => {
      // this.handler(request, response, err => {
      var infoMaster = await this.adMasterRepository.findById(id);

      // console.log(requestFile.file);
      // console.log(requestFile.body);

      const storage = multer.diskStorage({
        destination: path.join(__dirname, './../../.digipost/profile_npwp'),
        filename: (req, file, cb) => {
          cb(null, Date.now() + "-" + file.originalname);
        },
      });

      const upload = multer({
        storage: storage,
        fileFilter: async function (req, file, callback) {
          var ext = path.extname(file.originalname).toLowerCase();
          if (ext !== '.pdf') {
            return callback(new Error('Only pdf are allowed'));
          }
          callback(null, true);
        }
      }).fields([{name: 'npwp_attachment', maxCount: 1}, {name: 'pks_attachment', maxCount: 1}]);

      await upload(requestFile, response, async (errUpload: any) => {
        // if(requestFile.files.npwp_attachment ==)

        if (errUpload) resolve({error: true, message: errUpload})
        else {
          var InfoBody: any; InfoBody = {};
          for (var i in requestFile.body) {
            InfoBody[i] = requestFile.body[i];
          }
          InfoBody["npwp_attachment"] = InfoBody["npwp"] + ".pdf";
          InfoBody["pks_attachment"] = "PKS_" + InfoBody["npwp"] + ".pdf";

          var files: any; files = requestFile.files;
          var file: any; file = "";
          var filePKS: any; filePKS = "";
          for (var iFile in files) {
            var InfiFile = files[iFile];
            for (var iFilePath in InfiFile) {
              if (InfiFile[iFilePath]["fieldname"] == 'npwp_attachment') {
                file = InfiFile[iFilePath];
              }
              if (InfiFile[iFilePath]["fieldname"] == 'pks_attachment') {
                filePKS = InfiFile[iFilePath];
              }
            }
          }
          if (file == "") {
            delete InfoBody["npwp_attachment"];
          }
          if (filePKS == "") {
            delete InfoBody["pks_attachment"];
          }


          console.log(files);
          // console.log(filePKS);


          var Uploaded = true;
          if (file !== "") {
            try {
              fs.rename(file.path, file.destination + "/" + InfoBody["npwp_attachment"], function (err) {
                if (err) {console.log(err); return;}
                resolve({success: true, message: "success create"});
              });
            } catch (error) {
              Uploaded = false;
              resolve({error: true, message: error});
              try {
                fs.unlinkSync(file.path);
              } catch (err) {
                resolve({error: true, message: err});
              }
            }
          }

          if (filePKS != "") {
            try {
              fs.rename(filePKS.path, filePKS.destination + "/" + InfoBody["pks_attachment"], function (err) {
                if (err) {console.log(err); return;}
                resolve({success: true, message: "success create"});
              });
            } catch (error) {
              Uploaded = false;
              resolve({error: true, message: error});
              try {
                fs.unlinkSync(filePKS.path);
              } catch (err) {
                resolve({error: true, message: err});
              }
            }
          }



          if (Uploaded) {
            try {
              // console.log(id, InfoBody);
              var updateRaw = {
                npwp: InfoBody.npwp.trim(),
                contract_number: InfoBody.contract_number.trim(),
                ad_name: InfoBody.ad_name.trim(),
                attention: InfoBody.attention.trim(),
                address: InfoBody.address.trim(),
                attention_position: InfoBody.attention_position,
                city: InfoBody.city.trim(),
                email: InfoBody.email.trim(),
                post_code: parseInt(InfoBody.post_code),
                phone_number: InfoBody.phone_number.trim(),
                npwp_address: InfoBody.npwp_address.trim(),
                join_date: InfoBody.join_date,
                prefix: InfoBody.prefix,
                name_bank: InfoBody.name_bank.trim(),
                no_rek: InfoBody.no_rek,
                ad_status: InfoBody.ad_status,
                name_rekening: InfoBody.name_rekening == undefined ? "" : InfoBody.name_rekening,
                npwp_attachment: InfoBody.npwp_attachment,
                pks_attachment: InfoBody.pks_attachment,
              }
              if (updateRaw.npwp_attachment == undefined) {
                delete updateRaw["npwp_attachment"];
              }
              if (updateRaw.pks_attachment == undefined) {
                delete updateRaw["pks_attachment"];
              }

              var a = await this.adMasterRepository.updateById(id, updateRaw);

              // console.log(InfoBody.pks_attachment, InfoBody.npwp_attachment);


              // try {
              //   await this.dataDigiposRepository.updateAll({
              //     generate_file_a: 0,
              //     generate_file_b: 0,
              //     google_drive_a: 0,
              //     google_drive_b: 0,
              //     sign_off_a: 0,
              //     sign_off_b: 0,
              //     acc_file_ba_a: 0,
              //     acc_file_ba_b: 0,
              //     acc_file_invoice: 0,
              //     generate_file_invoice: 0,
              //     distribute_file_invoice: 0
              //   }, {and: [{trx_date_group: ControllerConfig.onPeriodeDigipos()}, {ad_code: infoMaster.ad_code}]});

              //   var userId = this.currentUserProfile[securityId];
              //   var roleNotifSetADB = ControllerConfig.onGetRoleNotifGroup("14010101");
              //   var infoUser = await this.userRepository.findById(userId)
              //   await this.dataNotificationRepository.create({
              //     types: "NOTIFICATION",
              //     target: roleNotifSetADB.role,
              //     to: "",
              //     from_who: infoUser.username,
              //     subject: roleNotifSetADB.label,
              //     body: "Update Data Master AD untuk  " + infoMaster.ad_code + " berhasil !",
              //     path_url: "",
              //     app: "DIGIPOS",
              //     code: "14010101",
              //     code_label: "",
              //     at_create: ControllerConfig.onCurrentTime().toString(),
              //     at_read: "",
              //     periode: ControllerConfig.onPeriodeDigipos(),
              //     at_flag: 0
              //   });

              //   ControllerConfig.onSendNotif({
              //     types: "NOTIFICATION",
              //     target: roleNotifSetADB.role,
              //     to: "",
              //     from_who: infoUser.username,
              //     subject: roleNotifSetADB.label,
              //     body: "Update Data Master AD untuk  " + infoMaster.ad_code + " berhasil !",
              //     path_url: "",
              //     app: "DIGIPOS",
              //     code: "14010101",
              //     code_label: "",
              //     at_create: ControllerConfig.onCurrentTime().toString(),
              //     at_read: "",
              //     periode: ControllerConfig.onPeriodeDigipos(),
              //     at_flag: 0
              //   });
              // } catch (error) { }

              resolve({success: true, message: "success update"});
            } catch (errorUpdate) {
              resolve({error: true, message: "Error Update !"});
              try {
                fs.unlinkSync(file.path);
              } catch (err) {
              }
              try {
                fs.unlinkSync(filePKS.path);
              } catch (err) {
              }

            }
          }
        }
      });
    });
  }

  @del('/AdMasterAction/{id}', {
    responses: {
      '204': {
        description: 'AdMaster DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.adMasterRepository.deleteById(id);
  }
}
