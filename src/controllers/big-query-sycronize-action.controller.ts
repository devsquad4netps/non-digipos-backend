// Uncomment these imports to begin using these cool features!

import {BigQuery, BigQueryDate} from '@google-cloud/bigquery';
import {authenticate} from '@loopback/authentication';
import {inject} from '@loopback/core';
import {repository} from '@loopback/repository';
import {get, param, post, requestBody, ResponseObject} from '@loopback/rest';
import {SecurityBindings, securityId, UserProfile} from '@loopback/security';
import {AdClusterRepository, AdMasterRepository, DataDigiposDetailRepository, DataDigiposRepository, DataSumarryRepository, TransaksiCalculationRepository, TransaksiTypeDetailRepository, TransaksiTypeRepository} from '../repositories';
import {ControllerConfig} from './controller.config';

export const BigQueryReqPeriode: ResponseObject = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['periode'],
        properties: {
          periode: {
            type: 'string',
          },
        }
      }
    },
  },
};


export const BigQueryReqCreateMultiClusterTmp: ResponseObject = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['cluster'],
        properties: {
          cluster: {
            type: 'array',
            items: {type: 'string'},
          }
        }
      }
    },
  },
};

export const BigQueryReqCreateClusterTmp: ResponseObject = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['before_cluster', 'after_cluster'],
        properties: {
          before_cluster: {
            type: 'string',
          },
          after_cluster: {
            type: 'string',
          },
        }
      }
    },
  },
};



export const BigQueryReqDeleteClusterTmp: ResponseObject = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['before_cluster'],
        properties: {
          before_cluster: {
            type: 'string',
          },
        }
      }
    },
  },
};


export const BigQueryReqClusterSkip: ResponseObject = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['cluster'],
        properties: {
          cluster: {
            type: 'string',
          },
        }
      }
    },
  },
};



var CLUSTER_DECLARE: any; CLUSTER_DECLARE = {};
var CLUSTER_SKIP: any; CLUSTER_SKIP = [];

var STORE_DATA: any; STORE_DATA = {};
var STORE_CALCULATION: any; STORE_CALCULATION = {};

@authenticate('jwt')
export class BigQuerySycronizeActionController {
  constructor(
    @repository(TransaksiCalculationRepository)
    public transaksiCalculationRepository: TransaksiCalculationRepository,
    @repository(DataSumarryRepository)
    public dataSumarryRepository: DataSumarryRepository,
    @repository(DataDigiposRepository)
    public dataDigiposRepository: DataDigiposRepository,
    @repository(DataDigiposDetailRepository)
    public dataDigiposDetailRepository: DataDigiposDetailRepository,
    @repository(AdClusterRepository)
    public adClusterRepository: AdClusterRepository,
    @repository(AdMasterRepository)
    public adMasterRepository: AdMasterRepository,
    @repository(TransaksiTypeRepository)
    public transaksiTypeRepository: TransaksiTypeRepository,
    @repository(TransaksiTypeDetailRepository)
    public transaksiTypeDetailRepository: TransaksiTypeDetailRepository,
    @inject(SecurityBindings.USER)
    public currentUserProfile: UserProfile,
  ) { }


  //============================================================================BIG QUERY PROGRESS
  @get('/big-query-sycronize/progress/{periode}', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: BigQueryReqPeriode
          }
        },
      },
    },
  })
  async findBigQueryProgress(
    @param.path.string('periode') periode: string,
  ): Promise<Object> {
    var result: any; result = {success: false, message: "", data: {}};

    var PERIODE_FILTER = periode;
    var PERIODE = PERIODE_FILTER.split("-");
    var TRANSAKSI_DATE = new Date(Number(PERIODE[0]), Number(PERIODE[1]), 0);
    var TRANSAKSI_MONTH = TRANSAKSI_DATE.getMonth() + 1;
    var END_DATE_TRANSAKSI = TRANSAKSI_DATE.getFullYear() + "-" + "00".substring(TRANSAKSI_MONTH.toString().length) + TRANSAKSI_MONTH + "-" + TRANSAKSI_DATE.getDate();
    var START_DATE_TRANSAKSI = TRANSAKSI_DATE.getFullYear() + "-" + "00".substring(TRANSAKSI_MONTH.toString().length) + TRANSAKSI_MONTH + "-" + "01";

    var TRANSAKSI_TYPE_STORE = await this.transaksiTypeRepository.find({where: {and: [{TT_FLAG: 1}]}});

    var DATA_RESULT: any; DATA_RESULT = {};

    const BIG_QUERY = new BigQuery();
    for (var iTTS in TRANSAKSI_TYPE_STORE) {
      var RAW_TTS = TRANSAKSI_TYPE_STORE[iTTS];

      var validConfig = true;
      var SQL_QUERY = "SELECT ";
      if (RAW_TTS.TT_TABLE != null && RAW_TTS.TT_TABLE != "") {
        var FIELD = "COUNT(GENERATE_UUID()) COUNT_TRX";

        if (RAW_TTS.TT_TABLE_KEY != null && RAW_TTS.TT_TABLE_KEY != "") {
          FIELD += ",COUNT(" + RAW_TTS.TT_TABLE_KEY + ") COUNT_AD, (" + RAW_TTS.TT_TABLE_KEY + ") NAME_AD ";
        } else {
          validConfig = false;
        }

        if (RAW_TTS.TT_TABLE_DATE != null && RAW_TTS.TT_TABLE_DATE != "") {
          FIELD += ",(" + RAW_TTS.TT_TABLE_DATE + ") DATE_DATA ";
        } else {
          validConfig = false;
        }

        SQL_QUERY += FIELD + " FROM " + RAW_TTS.TT_TABLE + " WHERE " + RAW_TTS.TT_TABLE_DATE + " BETWEEN  @START_DATE AND @END_DATE "
        SQL_QUERY += " GROUP BY  " + RAW_TTS.TT_TABLE_DATE + " , " + RAW_TTS.TT_TABLE_KEY;
      } else {
        validConfig = false;
      }

      if (validConfig == true) {
        const OPTION_BIG_QUERY = {
          query: SQL_QUERY,
          params: {START_DATE: new BigQueryDate(START_DATE_TRANSAKSI), END_DATE: new BigQueryDate(END_DATE_TRANSAKSI)},
        }
        const [RESULT_BIG_QUERY] = await BIG_QUERY.query(OPTION_BIG_QUERY);

        for (var iRESULT in RESULT_BIG_QUERY) {
          var RAW_RESULT = RESULT_BIG_QUERY[iRESULT];
          var DATE_TRX = RAW_RESULT.DATE_DATA.value;
          if (DATA_RESULT[DATE_TRX] == undefined) {
            DATA_RESULT[DATE_TRX] = {};
          }
          var ID = RAW_TTS.TT_ID == undefined ? "-1" : RAW_TTS.TT_ID;
          if (DATA_RESULT[DATE_TRX][ID] == undefined) {
            DATA_RESULT[DATE_TRX][ID] = {LABEL: RAW_TTS.TT_LABEL, COUNT_TRX: 0, COUNT_CLUSTER: 0};
          }
          DATA_RESULT[DATE_TRX][ID]["COUNT_TRX"] += RAW_RESULT.COUNT_TRX;
          DATA_RESULT[DATE_TRX][ID]["COUNT_CLUSTER"] += RAW_RESULT.COUNT_AD;

        }
      }
    }

    result = {success: true, message: "", data: DATA_RESULT};
    return result;
  }



  //============================================================================BIG QUERY RAW SUMMARY
  @get('/big-query-sycronize/raw/{type_id}/{periode}', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: BigQueryReqPeriode
          }
        },
      },
    },
  })
  async findBigQueryRaw(
    @param.path.number('type_id') type_id: number,
    @param.path.string('periode') periode: string,
  ): Promise<Object> {
    var userId = this.currentUserProfile[securityId];
    var result: any; result = {success: false, message: "", data: {}};

    var CLUSTER_DATA = await this.adClusterRepository.find();
    var CLUSTER: any; CLUSTER = {};
    for (var iCLUSTER in CLUSTER_DATA) {
      var RAW_CLUSTER = CLUSTER_DATA[iCLUSTER];
      CLUSTER[RAW_CLUSTER.ad_cluster_id] = RAW_CLUSTER;
    }

    //==========================================================================
    var PERIODE_FILTER = periode;
    var PERIODE = PERIODE_FILTER.split("-");
    var TRANSAKSI_DATE = new Date(Number(PERIODE[0]), Number(PERIODE[1]), 0);
    var TRANSAKSI_MONTH = TRANSAKSI_DATE.getMonth() + 1;
    var END_DATE_TRANSAKSI = TRANSAKSI_DATE.getFullYear() + "-" + "00".substring(TRANSAKSI_MONTH.toString().length) + TRANSAKSI_MONTH + "-" + TRANSAKSI_DATE.getDate();
    var START_DATE_TRANSAKSI = TRANSAKSI_DATE.getFullYear() + "-" + "00".substring(TRANSAKSI_MONTH.toString().length) + TRANSAKSI_MONTH + "-" + "01";

    var TRANSAKSI_TYPE_STORE = await this.transaksiTypeRepository.findById(type_id);
    var TRANSAKSI_FIELD = await this.transaksiTypeDetailRepository.find({where: {and: [{TT_ID: TRANSAKSI_TYPE_STORE.TT_ID}, {TTD_FLAG: 1}]}});

    var FIELD = "GENERATE_UUID() as UniqueKey"
    if (TRANSAKSI_TYPE_STORE.TT_ID != undefined) {
      FIELD += "," + TRANSAKSI_TYPE_STORE.TT_TABLE_DATE;
      FIELD += "," + TRANSAKSI_TYPE_STORE.TT_TABLE_KEY;

      for (var iField in TRANSAKSI_FIELD) {
        var RAW_FIELD = TRANSAKSI_FIELD[iField];
        FIELD += "," + RAW_FIELD.TTD_KEY + " AS " + RAW_FIELD.TTD_LABEL;
      }

      var FILTER = "";
      if (TRANSAKSI_TYPE_STORE.TT_TABLE_FILTER != null && TRANSAKSI_TYPE_STORE.TT_TABLE_FILTER != "") {
        FILTER = " AND " + TRANSAKSI_TYPE_STORE.TT_TABLE_FILTER + " ";
      }

      var SQL_QUERY = "SELECT " + FIELD + " FROM " + TRANSAKSI_TYPE_STORE.TT_TABLE + " WHERE   (" + TRANSAKSI_TYPE_STORE.TT_TABLE_DATE + "  BETWEEN  @START_DATE AND @END_DATE )" + FILTER;
      // SQL_QUERY += " GROUP BY  " + TRANSAKSI_TYPE_STORE.TT_TABLE_DATE + " , " + TRANSAKSI_TYPE_STORE.TT_TABLE_KEY;
      const SQL_OPTION = {
        query: SQL_QUERY,
        params: {START_DATE: new BigQueryDate(START_DATE_TRANSAKSI), END_DATE: new BigQueryDate(END_DATE_TRANSAKSI)},
      }

      const BIG_QUERY = new BigQuery();
      const [ROW_RESULT] = await BIG_QUERY.query(SQL_OPTION);

      var CLUSTER_SET: any; CLUSTER_SET = [];
      for (var iRESULT in ROW_RESULT) {
        var ROW = ROW_RESULT[iRESULT];
        var ROW_KEY = ROW[TRANSAKSI_TYPE_STORE.TT_TABLE_KEY];
        var CLUSTER_EXPLODE = ROW_KEY.split(" - ").join("-").split("-");
        var CLUSTER_ID = CLUSTER_EXPLODE[0];
        if (CLUSTER[CLUSTER_ID] != undefined) {
          if (CLUSTER_SET.indexOf(CLUSTER_ID) == -1) {
            CLUSTER_SET.push(CLUSTER_ID);
          }
        }

      }

      var DATAS: any; DATAS = {};
      var KEY_GENERATE: any; KEY_GENERATE = {};
      var no = 1;
      for (var iRESULT in ROW_RESULT) {
        var ROW = ROW_RESULT[iRESULT];
        var ROW_DATE = ROW[TRANSAKSI_TYPE_STORE.TT_TABLE_DATE]["value"].split("-");
        var ROW_PERIODE = ROW_DATE[0] + "-" + ROW_DATE[1];
        var ROW_KEY = ROW[TRANSAKSI_TYPE_STORE.TT_TABLE_KEY];
        var KEY = ROW_PERIODE + "-" + ROW_KEY;

        var CLUSTER_EXPLODE = ROW_KEY.split(" - ").join("-").split("-");
        var CLUSTER_ID = CLUSTER_EXPLODE[0];

        if (CLUSTER_SKIP.indexOf(ROW_KEY) != -1) {
          continue;
        }

        var FLAG_AVALIABBLE = 1;
        var MESSAGE = "";
        var D_CLUSTER_ID = "NONE";
        var D_CLUSTER_NAME = "";
        var D_CLUSTER_NAME_AD = "";
        var D_CLUSTER_CODE_AD = "";
        if (CLUSTER[CLUSTER_ID] == undefined) {
          if (CLUSTER_DECLARE[ROW_KEY] == undefined) {
            var SPLIT_NAME = ROW_KEY;//ROW_KEY.split(".").join("").split("PT ").join("").split("CV ").join("");
            var FIND_NAME = await this.adClusterRepository.find({where: {and: [{ad_name: {like: '%' + SPLIT_NAME + '%'}}]}});
            if (FIND_NAME.length > 1 || FIND_NAME.length == 0) {

              if (FIND_NAME.length > 1) {
                var FOUND = 0;
                var FOUND_CLUSTER: any; FOUND_CLUSTER = {}
                var check: any; check = [];
                for (var iNAME in FIND_NAME) {
                  var RAW_NAME = FIND_NAME[iNAME];
                  if (CLUSTER_SET.indexOf(RAW_NAME.ad_cluster_id) == -1) {
                    FOUND_CLUSTER = RAW_NAME;
                    FOUND++;
                  }
                }

                if (FOUND > 1) {
                  FLAG_AVALIABBLE = 2;
                  MESSAGE = "CLUSTER NAME not found !";
                } else {
                  D_CLUSTER_ID = FOUND_CLUSTER.ad_cluster_id;
                  D_CLUSTER_NAME = FOUND_CLUSTER.ad_cluster_name
                  D_CLUSTER_NAME_AD = FOUND_CLUSTER.ad_cluster_name;
                  D_CLUSTER_CODE_AD = FOUND_CLUSTER.ad_code;
                }

              } else {
                FLAG_AVALIABBLE = 2;
                MESSAGE = "CLUSTER NAME not found !";
              }
            } else {
              for (var iNAME in FIND_NAME) {
                var RAW_NAME = FIND_NAME[iNAME];
                D_CLUSTER_ID = RAW_NAME.ad_cluster_id;
                D_CLUSTER_NAME = RAW_NAME.ad_cluster_name
                D_CLUSTER_NAME_AD = RAW_NAME.ad_cluster_name;
                D_CLUSTER_CODE_AD = RAW_NAME.ad_code;
              }
            }

          } else {
            D_CLUSTER_ID = CLUSTER_DECLARE[ROW_KEY]["ad_cluster_id"];
            D_CLUSTER_NAME = CLUSTER_DECLARE[ROW_KEY]["ad_cluster_name"];
            D_CLUSTER_NAME_AD = CLUSTER_DECLARE[ROW_KEY]["ad_name"];
            D_CLUSTER_CODE_AD = CLUSTER_DECLARE[ROW_KEY]["ad_code"];
          }
        } else {
          D_CLUSTER_ID = CLUSTER[CLUSTER_ID]["ad_cluster_id"];
          D_CLUSTER_NAME = CLUSTER[CLUSTER_ID]["ad_cluster_name"];
          D_CLUSTER_NAME_AD = CLUSTER[CLUSTER_ID]["ad_name"];
          D_CLUSTER_CODE_AD = CLUSTER[CLUSTER_ID]["ad_code"];
        }


        if (KEY_GENERATE[KEY] == undefined) {
          var TIMES = ControllerConfig.onCurrentTime().toString();
          KEY_GENERATE[KEY] = TIMES.toString() + "-" + no.toString();
          no++;
        }

        var UniqueKey = KEY_GENERATE[KEY];
        if (DATAS[UniqueKey] == undefined) {
          DATAS[UniqueKey] = {};
          DATAS[UniqueKey]["FLAG"] = FLAG_AVALIABBLE;
          DATAS[UniqueKey]["MESSAGE"] = MESSAGE;
          DATAS[UniqueKey] = {
            "D_PERIODE": ROW_PERIODE,
            "D_KEY": ROW_KEY,
            "D_CLUSTER_ID": D_CLUSTER_ID,
            "D_CLUSTER_NAME": D_CLUSTER_NAME,
            "D_CLUSTER_NAME_AD": D_CLUSTER_NAME_AD,
            "D_CLUSTER_CODE_AD": D_CLUSTER_CODE_AD,
            D_SALES_AMOUNT_A: 0,
            D_COUNT_TRX_A: 0,
            D_SALES_AMOUNT_B: 0,
            D_COUNT_TRX_B: 0,
            D_FLAG: FLAG_AVALIABBLE,
            D_MESSAGE: MESSAGE
          };
        }

        if (TRANSAKSI_TYPE_STORE.TT_AD_A_HAS == 1) {
          if (TRANSAKSI_TYPE_STORE.TT_SALES_A != undefined && TRANSAKSI_TYPE_STORE.TT_COUNT_TRX_A != undefined) {
            var SALES_AMOUNT_A = TRANSAKSI_TYPE_STORE.TT_SALES_A == undefined ? "D_SALES_AMOUNT_A" : TRANSAKSI_TYPE_STORE.TT_SALES_A;
            var COUNT_TRX_A = TRANSAKSI_TYPE_STORE.TT_COUNT_TRX_A == undefined ? "D_COUNT_TRX_A" : TRANSAKSI_TYPE_STORE.TT_COUNT_TRX_A;
            DATAS[UniqueKey]["D_SALES_AMOUNT_A"] = Number(ROW[SALES_AMOUNT_A]);
            DATAS[UniqueKey]["D_COUNT_TRX_A"] += Number(ROW[COUNT_TRX_A]);
          }
        }

        if (TRANSAKSI_TYPE_STORE.TT_AD_B_HAS == 1) {
          if (TRANSAKSI_TYPE_STORE.TT_SALES_B != undefined && TRANSAKSI_TYPE_STORE.TT_COUNT_TRX_B != undefined) {
            var SALES_AMOUNT_B = TRANSAKSI_TYPE_STORE.TT_SALES_B == undefined ? "D_SALES_AMOUNT_B" : TRANSAKSI_TYPE_STORE.TT_SALES_B;
            var COUNT_TRX_B = TRANSAKSI_TYPE_STORE.TT_COUNT_TRX_B == undefined ? "D_COUNT_TRX_B" : TRANSAKSI_TYPE_STORE.TT_COUNT_TRX_B;
            DATAS[UniqueKey]["D_SALES_AMOUNT_B"] = Number(ROW[SALES_AMOUNT_B]);
            DATAS[UniqueKey]["D_COUNT_TRX_B"] += Number(ROW[COUNT_TRX_B]);
          }
        }

        for (var iFieldSet in TRANSAKSI_FIELD) {
          var RAW_FIELD_SET = TRANSAKSI_FIELD[iFieldSet];
          if (TRANSAKSI_TYPE_STORE.TT_AD_A_HAS == 1) {
            if (RAW_FIELD_SET.TTD_LABEL == TRANSAKSI_TYPE_STORE.TT_SALES_A || RAW_FIELD_SET.TTD_LABEL == TRANSAKSI_TYPE_STORE.TT_COUNT_TRX_A) {
              continue;
            }
          }
          if (TRANSAKSI_TYPE_STORE.TT_AD_B_HAS == 1) {
            if (RAW_FIELD_SET.TTD_LABEL == TRANSAKSI_TYPE_STORE.TT_SALES_B || RAW_FIELD_SET.TTD_LABEL == TRANSAKSI_TYPE_STORE.TT_COUNT_TRX_B) {
              continue;
            }
          }

          if (TRANSAKSI_TYPE_STORE.TT_AD_A_HAS == 1) {
            DATAS[UniqueKey][RAW_FIELD_SET.TTD_LABEL] = ROW[RAW_FIELD_SET.TTD_LABEL];
          }

          if (TRANSAKSI_TYPE_STORE.TT_AD_B_HAS == 1) {
            DATAS[UniqueKey][RAW_FIELD_SET.TTD_LABEL] = ROW[RAW_FIELD_SET.TTD_LABEL];
          }

        }
      }

      var RESULT_DATA: any; RESULT_DATA = [];
      for (var iKeyDatas in DATAS) {
        var RAW_DATAS = DATAS[iKeyDatas];
        var THIS_D_FIELD: any; THIS_D_FIELD = {};
        THIS_D_FIELD["D_ID"] = iKeyDatas;

        var FIELD_NUMBER = ["D_MDR_A", "D_DPP_A", "D_PPN_A", "D_PPH23_A", "D_TOTAL_A", "D_MDR_B", "D_DPP_B", "D_PPN_B", "D_PPH23_B", "D_TOTAL_B"];
        for (var iFIELD_D in RAW_DATAS) {
          if (FIELD_NUMBER.indexOf(iFIELD_D) != -1) {
            THIS_D_FIELD[iFIELD_D] = RAW_DATAS[iFIELD_D] == null ? 0 : RAW_DATAS[iFIELD_D];
          } else {
            THIS_D_FIELD[iFIELD_D] = RAW_DATAS[iFIELD_D];
          }
        }

        var D_MDR_A = Number(((Number(THIS_D_FIELD.D_SALES_AMOUNT_A) * Number(THIS_D_FIELD.D_COUNT_TRX_A)) * (TRANSAKSI_TYPE_STORE.TT_BOBOT_A / 100)).toFixed(2));
        var D_DPP_A = Number((D_MDR_A / TRANSAKSI_TYPE_STORE.TT_BOBOT_DPP_A).toFixed(2));
        var D_PPN_A = Number((D_DPP_A * (TRANSAKSI_TYPE_STORE.TT_BOBOT_PPN_A / 100)).toFixed(2));
        var D_PPH23_A = Number((D_DPP_A * (TRANSAKSI_TYPE_STORE.TT_BOBOT_PPH23_A / 100)).toFixed(2));
        var D_TOTAL_A = Number((D_DPP_A + D_PPN_A).toFixed(0));

        THIS_D_FIELD["D_MDR_A"] = D_MDR_A == null || isNaN(D_MDR_A) ? 0 : D_MDR_A;
        THIS_D_FIELD["D_DPP_A"] = D_DPP_A == null || isNaN(D_DPP_A) ? 0 : D_DPP_A;
        THIS_D_FIELD["D_PPN_A"] = D_PPN_A == null || isNaN(D_PPN_A) ? 0 : D_PPN_A;
        THIS_D_FIELD["D_PPH23_A"] = D_PPH23_A == null || isNaN(D_PPH23_A) ? 0 : D_PPH23_A;
        THIS_D_FIELD["D_TOTAL_A"] = D_TOTAL_A == null || isNaN(D_TOTAL_A) ? 0 : D_TOTAL_A;

        var D_MDR_B = Number(((Number(THIS_D_FIELD.D_SALES_AMOUNT_B) * Number(THIS_D_FIELD.D_COUNT_TRX_B)) * (TRANSAKSI_TYPE_STORE.TT_BOBOT_B / 100)).toFixed(2));
        var D_DPP_B = Number((D_MDR_B / TRANSAKSI_TYPE_STORE.TT_BOBOT_DPP_B).toFixed(2));
        var D_PPN_B = Number((D_DPP_B * (TRANSAKSI_TYPE_STORE.TT_BOBOT_PPN_B / 100)).toFixed(2));
        var D_PPH23_B = Number((D_DPP_B * (TRANSAKSI_TYPE_STORE.TT_BOBOT_PPH23_B / 100)).toFixed(2));
        var D_TOTAL_B = Number((D_DPP_B + D_PPN_B).toFixed(0));

        THIS_D_FIELD["D_MDR_B"] = D_MDR_B == null ? 0 : D_MDR_B;// Number(((Number(THIS_D_FIELD.D_SALES_AMOUNT_B) * Number(THIS_D_FIELD.D_COUNT_TRX_B)) * (TRANSAKSI_TYPE_STORE.TT_BOBOT_B / 100)).toFixed(2));
        THIS_D_FIELD["D_DPP_B"] = D_DPP_B == null || isNaN(D_DPP_B) ? 0 : D_DPP_B;//Number((THIS_D_FIELD["D_MDR_B"] / TRANSAKSI_TYPE_STORE.TT_BOBOT_DPP_B).toFixed(2));
        THIS_D_FIELD["D_PPN_B"] = D_PPN_B == null || isNaN(D_PPN_B) ? 0 : D_PPN_B;//Number((THIS_D_FIELD["D_DPP_B"] * (TRANSAKSI_TYPE_STORE.TT_BOBOT_PPN_B / 100)).toFixed(2));
        THIS_D_FIELD["D_PPH23_B"] = D_PPH23_B == null || isNaN(D_PPH23_B) ? 0 : D_PPH23_B;//Number((THIS_D_FIELD["D_DPP_B"] * (TRANSAKSI_TYPE_STORE.TT_BOBOT_PPH23_B / 100)).toFixed(2));
        THIS_D_FIELD["D_TOTAL_B"] = D_TOTAL_B == null || isNaN(D_TOTAL_B) ? 0 : D_TOTAL_B;//Number((THIS_D_FIELD["D_DPP_B"] + THIS_D_FIELD["D_PPN_B"]).toFixed(0));

        RESULT_DATA.push(THIS_D_FIELD);
      }

      if (STORE_DATA[userId] == undefined) {
        STORE_DATA[userId] = {}
        STORE_CALCULATION[userId] = {};
      }

      if (STORE_DATA[userId][periode] == undefined) {
        STORE_DATA[userId][periode] = {}
        STORE_CALCULATION[userId][periode] = {};
      }

      if (STORE_DATA[userId][periode][TRANSAKSI_TYPE_STORE.TT_ID] == undefined) {
        STORE_DATA[userId][periode][TRANSAKSI_TYPE_STORE.TT_ID] = {}
      }
      STORE_DATA[userId][periode][TRANSAKSI_TYPE_STORE.TT_ID] = {};

      var DATA_TYPE: any; DATA_TYPE = TRANSAKSI_TYPE_STORE;
      for (var iTT in DATA_TYPE) {
        STORE_DATA[userId][periode][TRANSAKSI_TYPE_STORE.TT_ID][iTT] = DATA_TYPE[iTT];
      }
      STORE_DATA[userId][periode][TRANSAKSI_TYPE_STORE.TT_ID]["TT_DATAS"] = RESULT_DATA;

      result.success = true;
      result.message = "Success !";
      result.data = RESULT_DATA;


    } else {
      result.success = false;
      result.message = "Error, transaksi type not found !";
    }
    // result = {success: true, message: "", data: DATA_RESULT};
    return result;
  }

  @get('/big-query-sycronize/summry/{periode}', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: BigQueryReqPeriode
          }
        },
      },
    },
  })
  async findBigQuerySummary(
    @param.path.string('periode') periode: string,
  ): Promise<Object> {
    var userId = this.currentUserProfile[securityId];
    var result: any; result = {success: false, message: "", data: {}};

    var AD_MASTER: any; AD_MASTER = {};
    var RESULT_AD = await this.adMasterRepository.find();
    for (var i in RESULT_AD) {
      var RAW_AD = RESULT_AD[i];
      AD_MASTER[RAW_AD.ad_code] = RAW_AD;
    }

    if (STORE_DATA[userId] != undefined) {
      if (STORE_DATA[userId][periode] != undefined) {


        var RESULT_DATA: any; RESULT_DATA = {"TRANSACTION_A": {}, "TRANSACTION_B": {}};
        for (var iSTORE in STORE_DATA[userId][periode]) {
          var RAW_STORE_DATA = STORE_DATA[userId][periode][iSTORE];

          STORE_CALCULATION[userId][periode][iSTORE] = {
            "TRANSACTION_A": {
              TT_LABEL: RAW_STORE_DATA.TT_LABEL,
              TT_BOBOT: RAW_STORE_DATA.TT_BOBOT_A,
              TT_AMOUNT: 0,
              TT_AD: [],
              TT_COUNT_AD: 0,
              TT_FLAG: 0
            },
            "TRANSACTION_B": {
              TT_LABEL: RAW_STORE_DATA.TT_LABEL,
              TT_BOBOT: RAW_STORE_DATA.TT_BOBOT_B,
              TT_AMOUNT: 0,
              TT_AD: [],
              TT_COUNT_AD: 0,
              TT_FLAG: 0
            }
          };




          for (var iTRX in RAW_STORE_DATA["TT_DATAS"]) {
            var RAW_TRX = RAW_STORE_DATA["TT_DATAS"][iTRX];
            if (RAW_STORE_DATA.TT_AD_A_HAS == 1) {

              if (RESULT_DATA["TRANSACTION_A"][RAW_TRX.D_CLUSTER_CODE_AD] == undefined) {
                RESULT_DATA["TRANSACTION_A"][RAW_TRX.D_CLUSTER_CODE_AD] = {
                  D_PERIODE: RAW_TRX.D_PERIODE,
                  D_CLUSTER_CODE_AD: RAW_TRX.D_CLUSTER_CODE_AD,
                  D_CLUSTER_NAME_AD: AD_MASTER[RAW_TRX.D_CLUSTER_CODE_AD] == undefined ? RAW_TRX.D_CLUSTER_NAME_AD : AD_MASTER[RAW_TRX.D_CLUSTER_CODE_AD].global_name,
                  D_TRANSACTION: {}
                };
              }

              if (RESULT_DATA["TRANSACTION_A"][RAW_TRX.D_CLUSTER_CODE_AD]["D_TRANSACTION"][iSTORE] == undefined) {
                RESULT_DATA["TRANSACTION_A"][RAW_TRX.D_CLUSTER_CODE_AD]["D_TRANSACTION"][iSTORE] = {
                  TT_LABEL: RAW_STORE_DATA.TT_LABEL,
                  TT_BOBOT_A: RAW_STORE_DATA.TT_BOBOT_A,
                  D_SALES_AMOUNT_A: RAW_TRX.D_SALES_AMOUNT_A,
                  D_COUNT_TRX_A: 0,
                  D_MDR_A: 0,
                  D_DPP_A: 0,
                  D_PPN_A: 0,
                  D_PPH23_A: 0,
                  D_TOTAL_A: 0
                }
              }
              RESULT_DATA["TRANSACTION_A"][RAW_TRX.D_CLUSTER_CODE_AD]["D_TRANSACTION"][iSTORE]["D_COUNT_TRX_A"] += RAW_TRX.D_COUNT_TRX_A;
              RESULT_DATA["TRANSACTION_A"][RAW_TRX.D_CLUSTER_CODE_AD]["D_TRANSACTION"][iSTORE]["D_MDR_A"] += RAW_TRX.D_MDR_A;
              RESULT_DATA["TRANSACTION_A"][RAW_TRX.D_CLUSTER_CODE_AD]["D_TRANSACTION"][iSTORE]["D_DPP_A"] += RAW_TRX.D_DPP_A;
              RESULT_DATA["TRANSACTION_A"][RAW_TRX.D_CLUSTER_CODE_AD]["D_TRANSACTION"][iSTORE]["D_PPN_A"] += RAW_TRX.D_PPN_A;
              RESULT_DATA["TRANSACTION_A"][RAW_TRX.D_CLUSTER_CODE_AD]["D_TRANSACTION"][iSTORE]["D_PPH23_A"] += RAW_TRX.D_PPH23_A;
              RESULT_DATA["TRANSACTION_A"][RAW_TRX.D_CLUSTER_CODE_AD]["D_TRANSACTION"][iSTORE]["D_TOTAL_A"] += RAW_TRX.D_TOTAL_A;

              STORE_CALCULATION[userId][periode][iSTORE]["TRANSACTION_A"]["TT_AMOUNT"] += RAW_TRX.D_MDR_A;
              STORE_CALCULATION[userId][periode][iSTORE]["TRANSACTION_A"]["TT_FLAG"] = 1;

              if (STORE_CALCULATION[userId][periode][iSTORE]["TRANSACTION_A"]["TT_AD"].indexOf(RAW_TRX.D_CLUSTER_CODE_AD) == -1) {
                STORE_CALCULATION[userId][periode][iSTORE]["TRANSACTION_A"]["TT_AD"].push(RAW_TRX.D_CLUSTER_CODE_AD);
                STORE_CALCULATION[userId][periode][iSTORE]["TRANSACTION_A"]["TT_COUNT_AD"] += 1;
              }

            }

            if (RAW_STORE_DATA.TT_AD_B_HAS == 1) {
              if (RESULT_DATA["TRANSACTION_B"][RAW_TRX.D_CLUSTER_CODE_AD] == undefined) {
                RESULT_DATA["TRANSACTION_B"][RAW_TRX.D_CLUSTER_CODE_AD] = {
                  D_PERIODE: RAW_TRX.D_PERIODE,
                  D_CLUSTER_CODE_AD: RAW_TRX.D_CLUSTER_CODE_AD,
                  D_CLUSTER_NAME_AD: AD_MASTER[RAW_TRX.D_CLUSTER_CODE_AD] == undefined ? RAW_TRX.D_CLUSTER_NAME_AD : AD_MASTER[RAW_TRX.D_CLUSTER_CODE_AD].global_name,
                  D_TRANSACTION: {}
                };
              }

              if (RESULT_DATA["TRANSACTION_B"][RAW_TRX.D_CLUSTER_CODE_AD]["D_TRANSACTION"][iSTORE] == undefined) {
                RESULT_DATA["TRANSACTION_B"][RAW_TRX.D_CLUSTER_CODE_AD]["D_TRANSACTION"][iSTORE] = {
                  TT_LABEL: RAW_STORE_DATA.TT_LABEL,
                  TT_BOBOT_B: RAW_STORE_DATA.TT_BOBOT_B,
                  D_SALES_AMOUNT_B: RAW_TRX.D_SALES_AMOUNT_B,
                  D_COUNT_TRX_B: 0,
                  D_MDR_B: 0,
                  D_DPP_B: 0,
                  D_PPN_B: 0,
                  D_PPH23_B: 0,
                  D_TOTAL_B: 0
                }
              }
              RESULT_DATA["TRANSACTION_B"][RAW_TRX.D_CLUSTER_CODE_AD]["D_TRANSACTION"][iSTORE]["D_COUNT_TRX_B"] += RAW_TRX.D_COUNT_TRX_B;
              RESULT_DATA["TRANSACTION_B"][RAW_TRX.D_CLUSTER_CODE_AD]["D_TRANSACTION"][iSTORE]["D_MDR_B"] += RAW_TRX.D_MDR_B;
              RESULT_DATA["TRANSACTION_B"][RAW_TRX.D_CLUSTER_CODE_AD]["D_TRANSACTION"][iSTORE]["D_DPP_B"] += RAW_TRX.D_DPP_B;
              RESULT_DATA["TRANSACTION_B"][RAW_TRX.D_CLUSTER_CODE_AD]["D_TRANSACTION"][iSTORE]["D_PPN_B"] += RAW_TRX.D_PPN_B;
              RESULT_DATA["TRANSACTION_B"][RAW_TRX.D_CLUSTER_CODE_AD]["D_TRANSACTION"][iSTORE]["D_PPH23_B"] += RAW_TRX.D_PPH23_B;
              RESULT_DATA["TRANSACTION_B"][RAW_TRX.D_CLUSTER_CODE_AD]["D_TRANSACTION"][iSTORE]["D_TOTAL_B"] += RAW_TRX.D_TOTAL_B;

              STORE_CALCULATION[userId][periode][iSTORE]["TRANSACTION_B"]["TT_AMOUNT"] += RAW_TRX.D_MDR_B;
              STORE_CALCULATION[userId][periode][iSTORE]["TRANSACTION_B"]["TT_FLAG"] = 1;
              if (STORE_CALCULATION[userId][periode][iSTORE]["TRANSACTION_B"]["TT_AD"].indexOf(RAW_TRX.D_CLUSTER_CODE_AD) == -1) {
                STORE_CALCULATION[userId][periode][iSTORE]["TRANSACTION_B"]["TT_AD"].push(RAW_TRX.D_CLUSTER_CODE_AD);
                STORE_CALCULATION[userId][periode][iSTORE]["TRANSACTION_B"]["TT_COUNT_AD"] += 1;
              }

            }
          }
        }
        result.success = true;
        result.data = RESULT_DATA;
      } else {
        result.success = false;
        result.message = "Error, periode not avaliable  please running raw periode " + periode + " !";
      }
    } else {
      result.success = false;
      result.message = "Error, please running raw before summary !";
    }
    return result;
  }


  @get('/big-query-sycronize/calculation/{periode}', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: BigQueryReqPeriode
          }
        },
      },
    },
  })
  async findBigQueryCalculation(
    @param.path.string('periode') periode: string,
  ): Promise<Object> {
    var result: any; result = {success: false, message: "", data: {}};
    var userId = this.currentUserProfile[securityId];

    var DATA_TYPE = await this.transaksiTypeRepository.find({where: {and: [{TT_FLAG: 1}]}});
    var STORE_TYPE: any; STORE_TYPE = {};
    for (var IDT in DATA_TYPE) {
      var INFO_TYPE = DATA_TYPE[IDT];
      var ID = INFO_TYPE.TT_ID == undefined ? "-" : INFO_TYPE.TT_ID;
      STORE_TYPE[ID] = INFO_TYPE;
    }

    if (STORE_CALCULATION[userId] != undefined) {
      if (STORE_CALCULATION[userId][periode] != undefined) {
        var RESULT_DATA: any; RESULT_DATA = {};
        RESULT_DATA["TRANSACTION_A"] = {"TOTAL_AMOUNT": 0, "RAW": []};
        RESULT_DATA["TRANSACTION_B"] = {"TOTAL_AMOUNT": 0, "RAW": []};

        var CREATE_CALCULATION: any; CREATE_CALCULATION = [];
        for (var i in STORE_CALCULATION[userId][periode]) {
          var RAW_CALCULATE = STORE_CALCULATION[userId][periode][i];

          for (var iGROUP in RAW_CALCULATE) {
            var RAW_GROUP = RAW_CALCULATE[iGROUP];
            if (RAW_GROUP.TT_BOBOT > 0 || RAW_GROUP.TT_BOBOT < 0) {
              RESULT_DATA[iGROUP]["RAW"].push({LABEL: RAW_GROUP.TT_LABEL, BOBOT: RAW_GROUP.TT_BOBOT, COUNT_AD: RAW_GROUP.TT_COUNT_AD, AMAOUT: RAW_GROUP.TT_AMOUNT});
              RESULT_DATA[iGROUP]["TOTAL_AMOUNT"] += Number(RAW_GROUP.TT_AMOUNT);
            }
          }

          var data: any; data = {};
          data["PERIODE"] = periode;
          data["TYPE_TRX"] = i;

          data["BOBOT_A"] = STORE_TYPE[i].TT_BOBOT_A;
          data["BOBOT_DPP_A"] = STORE_TYPE[i].TT_BOBOT_DPP_A;
          data["BOBOT_PPN_A"] = STORE_TYPE[i].TT_BOBOT_PPN_A;
          data["BOBOT_PPH23_A"] = STORE_TYPE[i].TT_BOBOT_PPH23_A;
          data["COUNT_AD_A"] = RAW_CALCULATE["TRANSACTION_A"]["TT_COUNT_AD"];
          data["AMOUNT_AD_A"] = RAW_CALCULATE["TRANSACTION_A"]["TT_AMOUNT"];


          data["BOBOT_B"] = STORE_TYPE[i].TT_BOBOT_B;
          data["BOBOT_DPP_B"] = STORE_TYPE[i].TT_BOBOT_DPP_B;
          data["BOBOT_PPN_B"] = STORE_TYPE[i].TT_BOBOT_PPN_B;
          data["BOBOT_PPH23_B"] = STORE_TYPE[i].TT_BOBOT_PPH23_B;
          data["COUNT_AD_B"] = RAW_CALCULATE["TRANSACTION_B"]["TT_COUNT_AD"];
          data["AMOUNT_AD_B"] = RAW_CALCULATE["TRANSACTION_B"]["TT_AMOUNT"];


          data["AT_CREATE"] = ControllerConfig.onCurrentTime().toString();
          data["AT_UPDATE"] = undefined;
          data["AT_USER"] = userId;

          CREATE_CALCULATION.push(data);
        }

        for (var iCR in CREATE_CALCULATION) {
          var INFO_CREATE = CREATE_CALCULATION[iCR];
          INFO_CREATE["AT_UPDATE"] = ControllerConfig.onCurrentTime().toString();
          var TRX_CALCULATION = await this.transaksiCalculationRepository.findOne({where: {and: [{PERIODE: INFO_CREATE.PERIODE}, {TYPE_TRX: INFO_CREATE.TYPE_TRX}]}});
          if (TRX_CALCULATION == null) {
            await this.transaksiCalculationRepository.create(INFO_CREATE);
          } else {
            await this.transaksiCalculationRepository.updateById(TRX_CALCULATION.ID, INFO_CREATE);
          }
        }

        result.success = true;
        result.message = "success !";
        result.data = RESULT_DATA;
      } else {
        result.success = false;
        result.message = "error, data this periode is empty";
        result.data = [];
      }
    } else {
      result.success = false;
      result.message = "error, data is empty";
      result.data = [];
    }
    return result;// STORE_CALCULATION[userId][periode];
  }

  @post('/big-query-sycronize/import', {
    responses: {
      '200': {
        description: 'BigQuery Create Cluster TMP model instance',
        content: {
          'application/json': {
            schema: BigQueryReqPeriode
          }
        },
      },
    },
  })
  async ImportBigQuery(
    @requestBody(BigQueryReqPeriode) reqCreateClusterTmp: {
      periode: string
    },
  ): Promise<Object> {
    var userId = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: "success", data: []};

    var AvaliablePeriode = await this.dataDigiposRepository.find({where: {and: [{trx_date_group: reqCreateClusterTmp.periode}]}});
    var AvaliableReject = await this.dataDigiposRepository.find({where: {and: [{trx_date_group: reqCreateClusterTmp.periode}, {ar_stat: {inq: [-1, -2]}}]}});


    if (AvaliablePeriode.length == 0 || AvaliableReject.length > 0) {

      var TYPE_TRX = await this.transaksiTypeRepository.find({where: {and: [{TT_FLAG: 1}]}});
      var TYPE_STORE_AVA: any; TYPE_STORE_AVA = {};


      var AD_MASTER: any; AD_MASTER = {};
      var RESULT_AD = await this.adMasterRepository.find();
      for (var i in RESULT_AD) {
        var RAW_AD = RESULT_AD[i];
        AD_MASTER[RAW_AD.ad_code] = RAW_AD;
      }

      if (STORE_DATA[userId] != undefined) {
        if (STORE_DATA[userId][reqCreateClusterTmp.periode] != undefined) {

          var DATA_OLAH = STORE_DATA[userId][reqCreateClusterTmp.periode];//[TRANSAKSI_TYPE_STORE.TT_ID]["TT_DATAS"] = RESULT_DATA;
          var NEXT_STEP = 1;
          for (var iTYPE in TYPE_TRX) {
            var RAW_TYPE_TRX = TYPE_TRX[iTYPE];
            var ID_TYPE = RAW_TYPE_TRX.TT_ID == undefined ? "-" : RAW_TYPE_TRX.TT_ID;
            if (DATA_OLAH[ID_TYPE] == undefined) {
              NEXT_STEP = 0;
            }
          }

          if (NEXT_STEP == 1) {

            if (AvaliablePeriode.length > 0) {
              await this.dataDigiposRepository.deleteAll({and: [{trx_date_group: reqCreateClusterTmp.periode}]});
              await this.dataDigiposDetailRepository.deleteAll({and: [{trx_date_group: reqCreateClusterTmp.periode}]});
              await this.dataSumarryRepository.deleteAll({and: [{periode: reqCreateClusterTmp.periode}]});
            }


            var CREATE_SUMMARY: any; CREATE_SUMMARY = [];
            var CREATE_DIGIPOS: any; CREATE_DIGIPOS = {};
            var CREATE_DIGIPOS_DETAIL: any; CREATE_DIGIPOS_DETAIL = {};

            for (var iOLAH in DATA_OLAH) {
              var RAW_OLAH = DATA_OLAH[iOLAH];
              for (var iTRX in RAW_OLAH["TT_DATAS"]) {
                var RAW_TRX = RAW_OLAH["TT_DATAS"][iTRX];
                var DATA_TRX_CREATE = {
                  id: undefined,
                  dealer_code: RAW_TRX.D_CLUSTER_ID,
                  location: RAW_TRX.D_CLUSTER_NAME,
                  name: AD_MASTER[RAW_TRX.D_CLUSTER_CODE_AD] == undefined ? RAW_TRX.D_CLUSTER_NAME_AD : AD_MASTER[RAW_TRX.D_CLUSTER_CODE_AD].global_name,
                  type_trx: iOLAH,
                  paid_in_amount: RAW_TRX.D_SALES_AMOUNT_A,
                  withdraw_amount: 0,
                  reason_type_id: RAW_TRX.REASON == undefined ? "-" : RAW_TRX.REASON,
                  reason_type_name: RAW_TRX.REASON_NAME == undefined ? "-" : RAW_TRX.REASON_NAME,
                  trx_a: RAW_TRX.D_COUNT_TRX_A == undefined ? 0 : RAW_TRX.D_COUNT_TRX_A,
                  tot_mdr_a: RAW_TRX.D_MDR_A == undefined ? 0 : RAW_TRX.D_MDR_A,
                  trx_b: RAW_TRX.D_COUNT_TRX_B == undefined ? 0 : RAW_TRX.D_COUNT_TRX_B,
                  tot_mdr_b: RAW_TRX.D_MDR_B == undefined ? 0 : RAW_TRX.D_MDR_B,
                  status: 1,
                  periode: reqCreateClusterTmp.periode,
                  create_at: ControllerConfig.onCurrentTime().toString(),
                  code_ad: RAW_TRX.D_CLUSTER_CODE_AD
                }
                CREATE_SUMMARY.push(DATA_TRX_CREATE);

                var CODE_AD = RAW_TRX.D_CLUSTER_CODE_AD == undefined ? "-" : RAW_TRX.D_CLUSTER_CODE_AD;
                if (CREATE_DIGIPOS[CODE_AD] == undefined) {
                  CREATE_DIGIPOS[CODE_AD] = {
                    digipost_id: undefined,
                    trx_date_group: reqCreateClusterTmp.periode,
                    ad_code: RAW_TRX.D_CLUSTER_CODE_AD,
                    ad_name: AD_MASTER[RAW_TRX.D_CLUSTER_CODE_AD] == undefined ? RAW_TRX.D_CLUSTER_NAME_AD : AD_MASTER[RAW_TRX.D_CLUSTER_CODE_AD].global_name,
                    inv_number: undefined,
                    inv_attacment: undefined,
                    ba_number_a: undefined,
                    ba_attachment_a: undefined,
                    ba_number_b: undefined,
                    ba_attachment_b: undefined,
                    vat_number: undefined,
                    vat_attachment: undefined,
                    vat_distribute: 0,
                    at_vat_distibute: null,
                    inv_desc: undefined,
                    sales_fee: RAW_TRX.D_SALES_AMOUNT_A == undefined ? 0 : RAW_TRX.D_SALES_AMOUNT_A,
                    sales_paid: RAW_TRX.D_SALES_AMOUNT_B == undefined ? 0 : RAW_TRX.D_SALES_AMOUNT_B,
                    trx_cluster_out: 0,
                    mdr_fee_amount: 0,
                    dpp_fee_amount: 0,
                    vat_fee_amount: 0,
                    total_fee_amount: 0,
                    pph_fee_amount: 0,
                    trx_paid_invoice: 0,
                    paid_inv_amount: 0,
                    inv_adj_amount: 0,
                    dpp_paid_amount: 0,
                    vat_paid_amount: 0,
                    total_paid_amount: 0,
                    pph_paid_amount: 0,
                    remark: "-",
                    ar_stat: 0,
                    ar_valid_a: 0,
                    ar_valid_b: 0,
                    response_stat: 0,
                    generate_file_a: 0,
                    generate_file_b: 0,
                    google_drive_a: 0,
                    google_drive_b: 0,
                    sign_off_a: 0,
                    sign_off_b: 0,
                    acc_file_ba_a: 0,
                    acc_file_ba_b: 0,
                    acc_file_invoice: 0,
                    generate_file_invoice: 0,
                    distribute_file_invoice: 0,
                    attach_invoice_ttd: undefined,
                    distribute_invoice_ttd: 0,
                    at_distribute_invoice_ttd: undefined,
                    acc_recv: 0,
                    at_generate_csv: undefined,
                    flag_generate_csv: 0,
                    oracle_id: undefined,
                    interface_status: "NEW",
                    error_message: undefined
                  }
                  CREATE_DIGIPOS_DETAIL[CODE_AD] = {};
                }
                if (CREATE_DIGIPOS_DETAIL[CODE_AD][iOLAH] == undefined) {
                  CREATE_DIGIPOS_DETAIL[CODE_AD][iOLAH] = {
                    digipos_detail_id: undefined,
                    trx_date_group: reqCreateClusterTmp.periode,
                    ad_code: RAW_TRX.D_CLUSTER_CODE_AD,
                    ad_name: AD_MASTER[RAW_TRX.D_CLUSTER_CODE_AD] == undefined ? RAW_TRX.D_CLUSTER_NAME_AD : AD_MASTER[RAW_TRX.D_CLUSTER_CODE_AD].global_name,
                    type_trx: iOLAH,
                    sales_fee: RAW_TRX.D_SALES_AMOUNT_A,
                    sales_paid: RAW_TRX.D_SALES_AMOUNT_B,
                    trx_cluster_out: 0,
                    mdr_fee_amount: 0,
                    dpp_fee_amount: 0,
                    vat_fee_amount: 0,
                    total_fee_amount: 0,
                    pph_fee_amount: 0,
                    trx_paid_invoice: 0,
                    paid_inv_amount: 0,
                    inv_adj_amount: 0,
                    dpp_paid_amount: 0,
                    vat_paid_amount: 0,
                    pph_paid_amount: 0,
                    total_paid_amount: 0,
                    digipost_id: 0
                  }
                }


                //DIGIPOS
                CREATE_DIGIPOS[CODE_AD]["trx_cluster_out"] += RAW_TRX.D_COUNT_TRX_A == undefined ? 0 : RAW_TRX.D_COUNT_TRX_A;
                CREATE_DIGIPOS[CODE_AD]["mdr_fee_amount"] += RAW_TRX.D_MDR_A == undefined ? 0 : RAW_TRX.D_MDR_A;
                CREATE_DIGIPOS[CODE_AD]["dpp_fee_amount"] += RAW_TRX.D_DPP_A == undefined ? 0 : RAW_TRX.D_DPP_A;
                CREATE_DIGIPOS[CODE_AD]["vat_fee_amount"] += RAW_TRX.D_PPN_A == undefined ? 0 : RAW_TRX.D_PPN_A;
                CREATE_DIGIPOS[CODE_AD]["pph_fee_amount"] += RAW_TRX.D_PPH23_A == undefined ? 0 : RAW_TRX.D_PPH23_A;
                CREATE_DIGIPOS[CODE_AD]["total_fee_amount"] += RAW_TRX.D_TOTAL_A == undefined ? 0 : RAW_TRX.D_TOTAL_A;


                CREATE_DIGIPOS[CODE_AD]["trx_paid_invoice"] += RAW_TRX.D_COUNT_TRX_B == undefined ? 0 : RAW_TRX.D_COUNT_TRX_B;
                CREATE_DIGIPOS[CODE_AD]["paid_inv_amount"] += RAW_TRX.D_MDR_B == undefined ? 0 : RAW_TRX.D_MDR_B;
                CREATE_DIGIPOS[CODE_AD]["dpp_paid_amount"] += RAW_TRX.D_DPP_B == undefined ? 0 : RAW_TRX.D_DPP_B;
                CREATE_DIGIPOS[CODE_AD]["vat_paid_amount"] += RAW_TRX.D_PPN_B == undefined ? 0 : RAW_TRX.D_PPN_B;
                CREATE_DIGIPOS[CODE_AD]["pph_paid_amount"] += RAW_TRX.D_PPH23_B == undefined ? 0 : RAW_TRX.D_PPH23_B;
                CREATE_DIGIPOS[CODE_AD]["total_paid_amount"] += RAW_TRX.D_TOTAL_B == undefined ? 0 : RAW_TRX.D_TOTAL_B;

                //DIGIPOS DETAIL
                CREATE_DIGIPOS_DETAIL[CODE_AD][iOLAH]["trx_cluster_out"] += RAW_TRX.D_COUNT_TRX_A == undefined ? 0 : RAW_TRX.D_COUNT_TRX_A;
                CREATE_DIGIPOS_DETAIL[CODE_AD][iOLAH]["mdr_fee_amount"] += RAW_TRX.D_MDR_A == undefined ? 0 : RAW_TRX.D_MDR_A;
                CREATE_DIGIPOS_DETAIL[CODE_AD][iOLAH]["dpp_fee_amount"] += RAW_TRX.D_DPP_A == undefined ? 0 : RAW_TRX.D_DPP_A;
                CREATE_DIGIPOS_DETAIL[CODE_AD][iOLAH]["vat_fee_amount"] += RAW_TRX.D_PPN_A == undefined ? 0 : RAW_TRX.D_PPN_A;
                CREATE_DIGIPOS_DETAIL[CODE_AD][iOLAH]["pph_fee_amount"] += RAW_TRX.D_PPH23_A == undefined ? 0 : RAW_TRX.D_PPH23_A;
                CREATE_DIGIPOS_DETAIL[CODE_AD][iOLAH]["total_fee_amount"] += RAW_TRX.D_TOTAL_A == undefined ? 0 : RAW_TRX.D_TOTAL_A;


                CREATE_DIGIPOS_DETAIL[CODE_AD][iOLAH]["trx_paid_invoice"] += RAW_TRX.D_COUNT_TRX_B == undefined ? 0 : RAW_TRX.D_COUNT_TRX_B;
                CREATE_DIGIPOS_DETAIL[CODE_AD][iOLAH]["paid_inv_amount"] += RAW_TRX.D_MDR_B == undefined ? 0 : RAW_TRX.D_MDR_B;
                CREATE_DIGIPOS_DETAIL[CODE_AD][iOLAH]["dpp_paid_amount"] += RAW_TRX.D_DPP_B == undefined ? 0 : RAW_TRX.D_DPP_B;
                CREATE_DIGIPOS_DETAIL[CODE_AD][iOLAH]["vat_paid_amount"] += RAW_TRX.D_PPN_B == undefined ? 0 : RAW_TRX.D_PPN_B;
                CREATE_DIGIPOS_DETAIL[CODE_AD][iOLAH]["pph_paid_amount"] += RAW_TRX.D_PPH23_B == undefined ? 0 : RAW_TRX.D_PPH23_B;
                CREATE_DIGIPOS_DETAIL[CODE_AD][iOLAH]["total_paid_amount"] += RAW_TRX.D_TOTAL_B == undefined ? 0 : RAW_TRX.D_TOTAL_B;

              }
            }

            var have_error = 0;
            var message: any; message = "";
            for (var iSUMMARY in CREATE_SUMMARY) {
              try {
                var RAW_SUMMARY = CREATE_SUMMARY[iSUMMARY];
                await this.dataSumarryRepository.create(RAW_SUMMARY);
              } catch (error) {
                have_error = 1;
                message = error;
                console.log(error);
              }
            }


            for (var iD in CREATE_DIGIPOS) {

              try {
                var RAW_DIGIPOS = CREATE_DIGIPOS[iD];
                await this.dataDigiposRepository.create(RAW_DIGIPOS).then(async InsDigipos => {
                  if (CREATE_DIGIPOS_DETAIL[InsDigipos.ad_code] != undefined) {
                    for (var iDET in CREATE_DIGIPOS_DETAIL[InsDigipos.ad_code]) {
                      var RAW_DET = CREATE_DIGIPOS_DETAIL[InsDigipos.ad_code][iDET];
                      RAW_DET["digipost_id"] = InsDigipos.digipost_id;
                      await this.dataDigiposDetailRepository.create(RAW_DET);
                    }
                  }
                });
              } catch (error) {
                have_error = 1;
                message = error;
                console.log(error);
              }
            }

            if (have_error == 0) {
              result.success = true;
              result.message = "success !";
              result.data = [];
            } else {
              try {
                await this.dataDigiposRepository.deleteAll({and: [{trx_date_group: reqCreateClusterTmp.periode}]});
                await this.dataDigiposDetailRepository.deleteAll({and: [{trx_date_group: reqCreateClusterTmp.periode}]});
                await this.dataSumarryRepository.deleteAll({and: [{periode: reqCreateClusterTmp.periode}]});
                result.success = false;
                result.message = message
                result.data = [];
              } catch (error) {
                result.success = false;
                result.message = error
                result.data = [];
              }
            }
          } else {
            result.success = false;
            result.message = "error, please running all type !";
            result.data = [];
          }
        } else {
          result.success = false;
          result.message = "error, data this periode is empty";
          result.data = [];
        }

      } else {
        result.success = false;
        result.message = "error, data is empty";
        result.data = [];
      }
    } else {
      if (AvaliableReject.length == 0) {
        result.success = false;
        result.message = "error, no data rejected";
        result.data = [];
      }
    }
    return result;
  }


  //============================================================================MAPPING NEW CLUSTER TMP
  @get('/big-query-sycronize/cluster-tmp/view', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: BigQueryReqPeriode
          }
        },
      },
    },
  })
  async findClusterTmp(): Promise<Object> {
    var returnMs: any; returnMs = {success: true, message: "success", data: []};
    var data: any; data = [];
    for (var i in CLUSTER_DECLARE) {
      var RAW_DECLARE = CLUSTER_DECLARE[i];
      var THIS_DATA = {
        BEFORE_CLUSTER: i,
        AFTER_CLUSTER: RAW_DECLARE.ad_cluster_id,
        D_CLUSTER_NAME: RAW_DECLARE.ad_cluster_name,
        D_CLUSTER_NAME_AD: RAW_DECLARE.ad_name,
        D_CLUSTER_CODE_AD: RAW_DECLARE.ad_code,
      }
      data.push(THIS_DATA);
    }
    returnMs.data = data;
    return returnMs;
  }

  @post('/big-query-sycronize/cluster-tmp/create-multi', {
    responses: {
      '200': {
        description: 'BigQuery Create Cluster TMP model instance',
        content: {
          'application/json': {
            schema: BigQueryReqCreateMultiClusterTmp
          }
        },
      },
    },
  })
  async CreateClusterTmpMulti(
    @requestBody(BigQueryReqCreateMultiClusterTmp) reqCreateClusterTmp: {
      cluster: any
    },
  ): Promise<Object> {
    var returnMs: any; returnMs = {success: true, success_create: [], error_create: [], data: {}};

    var errorCluster: any; errorCluster = [];
    var successCluster: any; successCluster = [];
    for (var i in reqCreateClusterTmp.cluster) {
      var textCreate = reqCreateClusterTmp.cluster[i].split("#");
      if (CLUSTER_DECLARE[textCreate[0]] == undefined) {
        var cluster = await this.adClusterRepository.findById(textCreate[1]);
        if (cluster.ad_cluster_id != undefined) {
          CLUSTER_DECLARE[textCreate[0]] = cluster;
          if (successCluster.indexOf(textCreate[1]) == -1) {
            successCluster.push(textCreate[1]);
          }
        } else {
          if (errorCluster.indexOf(textCreate[1]) == -1) {
            errorCluster.push(textCreate[1]);
          }
        }
      } else {
        continue;
      }
    }
    returnMs.success_create = successCluster;
    returnMs.error_create = errorCluster;
    return returnMs;
  }

  @post('/big-query-sycronize/cluster-tmp/create', {
    responses: {
      '200': {
        description: 'BigQuery Create Cluster TMP model instance',
        content: {
          'application/json': {
            schema: BigQueryReqCreateClusterTmp
          }
        },
      },
    },
  })
  async CreateClusterTmp(
    @requestBody(BigQueryReqCreateClusterTmp) reqCreateClusterTmp: {
      before_cluster: string, after_cluster: string
    },
  ): Promise<Object> {
    var returnMs: any; returnMs = {success: true, message: "", data: {}};

    if (CLUSTER_DECLARE[reqCreateClusterTmp.before_cluster] == undefined) {
      var cluster = await this.adClusterRepository.findById(reqCreateClusterTmp.after_cluster);
      if (cluster.ad_cluster_id != undefined) {
        CLUSTER_DECLARE[reqCreateClusterTmp.before_cluster] = cluster;
        returnMs.success = true;
        returnMs.message = "success !";
      } else {
        returnMs.success = false;
        returnMs.message = "Error, cluster id not found !";
      }
    } else {
      returnMs.success = false;
      returnMs.message = "Error, cluster name already create !";
    }
    return returnMs;
  }

  @post('/big-query-sycronize/cluster-tmp/delete', {
    responses: {
      '200': {
        description: 'BigQuery Create Cluster TMP model instance',
        content: {
          'application/json': {
            schema: BigQueryReqDeleteClusterTmp
          }
        },
      },
    },
  })
  async DeleteClusterTmp(
    @requestBody(BigQueryReqDeleteClusterTmp) reqCreateClusterTmp: {
      before_cluster: string,
    },
  ): Promise<Object> {
    var returnMs: any; returnMs = {success: true, message: "", data: {}};
    if (CLUSTER_DECLARE[reqCreateClusterTmp.before_cluster] != undefined) {
      delete CLUSTER_DECLARE[reqCreateClusterTmp.before_cluster];
      returnMs = {success: true, message: "success !", data: {}};
    } else {
      returnMs = {success: false, message: "error, before cluster not found!", data: {}};
    }
    return returnMs;
  }

  @post('/big-query-sycronize/cluster-tmp/clear', {
    responses: {
      '200': {
        description: 'BigQuery Create Cluster TMP model instance',
        content: {
          'application/json': {
            schema: BigQueryReqCreateClusterTmp
          }
        },
      },
    },
  })
  async ClearClusterTmp(): Promise<Object> {
    var returnMs: any; returnMs = {success: true, message: "success !!", data: {}};
    CLUSTER_DECLARE = {};
    return returnMs;
  }

  //============================================================================ CLUSTER SKIP
  @get('/big-query-sycronize/cluster-skip/view', {
    responses: {
      '200': {
        description: 'BigQuery Cluster SKIP model instance',
        content: {
          'application/json': {
            schema: BigQueryReqClusterSkip
          }
        },
      },
    },
  })
  async findClusterSkip(): Promise<Object> {
    var returnMs: any; returnMs = {success: true, message: "success", data: []};
    returnMs.data = CLUSTER_SKIP;
    return returnMs;
  }

  @post('/big-query-sycronize/cluster-skip/create', {
    responses: {
      '200': {
        description: 'BigQuery Create Cluster SKIP model instance',
        content: {
          'application/json': {
            schema: BigQueryReqClusterSkip
          }
        },
      },
    },
  })
  async CreateClusterSkip(
    @requestBody(BigQueryReqClusterSkip) reqCluster: {
      cluster: string,
    },
  ): Promise<Object> {
    var returnMs: any; returnMs = {success: true, message: "", data: {}};
    if (CLUSTER_SKIP.indexOf(reqCluster.cluster) == -1) {
      CLUSTER_SKIP.push(reqCluster.cluster);
    } else {
      returnMs = {success: false, message: "error, cluster skip already exities create !", data: {}};
    }
    return returnMs;
  }

  @post('/big-query-sycronize/cluster-skip/delete', {
    responses: {
      '200': {
        description: 'BigQuery Create Cluster TMP model instance',
        content: {
          'application/json': {
            schema: BigQueryReqClusterSkip
          }
        },
      },
    },
  })
  async DeleteClusterSkip(
    @requestBody(BigQueryReqClusterSkip) reqClusterSkip: {
      cluster: string,
    },
  ): Promise<Object> {
    var returnMs: any; returnMs = {success: true, message: "", data: {}};
    if (CLUSTER_SKIP.indexOf(reqClusterSkip.cluster) != -1) {
      var key = CLUSTER_SKIP.indexOf(reqClusterSkip.cluster);
      CLUSTER_SKIP.splice(key, 1);
      returnMs = {success: true, message: "success !", data: {}};
    } else {
      returnMs = {success: false, message: "error,  cluster skip not found !", data: {}};
    }
    return returnMs;
  }

  @post('/big-query-sycronize/cluster-skip/clear', {
    responses: {
      '200': {
        description: 'BigQuery Create Cluster TMP model instance',
        content: {
          'application/json': {
            schema: BigQueryReqClusterSkip
          }
        },
      },
    },
  })
  async ClearClusterSkip(): Promise<Object> {
    var returnMs: any; returnMs = {success: true, message: "success !!", data: {}};
    CLUSTER_SKIP = [];
    return returnMs;
  }




}
