import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where
} from '@loopback/repository';
import {
  del, get,
  getModelSchemaRef, param,


  patch, post,




  put,

  requestBody
} from '@loopback/rest';
import {AdCluster} from '../models';
import {AdClusterRepository, AdMasterRepository} from '../repositories';


export const InserByCSVRequestBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['ad_code', 'data'],
        properties: {
          ad_code: {
            type: 'string',
          },
          data: {
            type: 'array',
            items: {type: 'object'},
          }
        }
      }
    },
  },
};

export class AdClusterActionController {
  constructor(
    @repository(AdClusterRepository)
    public adClusterRepository: AdClusterRepository,
    @repository(AdMasterRepository)
    public adMasterRepository: AdMasterRepository,
  ) { }

  @post('/AdClusterAction/insert-multiple', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: InserByCSVRequestBody
          }
        },
      },
    },
  })
  async insertMultiple(
    @requestBody(InserByCSVRequestBody) genDoc: {
      ad_code: string, data: []
    },
  ): Promise<Object> {
    var insert = true;
    var messages: string; messages = "";
    var cluster_duplicat = [];
    var countAD = await this.adMasterRepository.count({ad_code: genDoc.ad_code});
    if (countAD.count == 0) {
      insert = false;
      messages = "Ad Code yang dipilih tidak ditemukan !";
    } else {
      for (var i in genDoc.data) {
        var rawDoc: any; rawDoc = genDoc.data[i];
        if (rawDoc.ad_cluster_id == undefined || rawDoc.ad_cluster_name == undefined) {
          insert = false;
          messages = "Field ad_cluster_id ar  ad_cluster_name not found !";
          continue;
        }
        var countCls = await this.adClusterRepository.count({ad_cluster_id: rawDoc.ad_cluster_id});
        if (countCls.count > 0) {
          insert = false;
          cluster_duplicat.push(rawDoc.ad_cluster_id);
          continue;
        }
      }
    }
    if (insert == false) {
      if (cluster_duplicat.length > 0) {
        messages = " duplicate cluster id " + JSON.stringify(cluster_duplicat) + ", cluster id can't not duplicate values !";
        return {success: false, message: messages};
      } else {
        return {success: false, message: messages};
      }
    } else {
      var infoAd = await this.adMasterRepository.findOne({where: {ad_code: genDoc.ad_code}});
      try {
        for (var i in genDoc.data) {
          var rawDoc: any; rawDoc = genDoc.data[i];
          await this.adClusterRepository.create({
            ad_cluster_id: rawDoc.ad_cluster_id,
            ad_cluster_name: rawDoc.ad_cluster_name,
            ad_code: infoAd?.ad_code,
            ad_name: infoAd?.ad_name
          });
        }
        return {success: true, message: "Success"};
      } catch (error) {
        return {success: false, message: error};
      }
    }
  }


  @post('/AdClusterAction', {
    responses: {
      '200': {
        description: 'AdCluster model instance',
        content: {'application/json': {schema: getModelSchemaRef(AdCluster)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(AdCluster, {
            title: 'NewAdCluster',
            // exclude: ['ad_cluster_id'],
          }),
        },
      },
    })
    adCluster: Omit<AdCluster, 'ad_cluster_id'>,
  ): Promise<AdCluster> {
    return this.adClusterRepository.create(adCluster);
  }

  @get('/AdClusterAction/count', {
    responses: {
      '200': {
        description: 'AdCluster model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(AdCluster) where?: Where<AdCluster>,
  ): Promise<Count> {
    return this.adClusterRepository.count(where);
  }

  @get('/AdClusterAction', {
    responses: {
      '200': {
        description: 'Array of AdCluster model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(AdCluster, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(AdCluster) filter?: Filter<AdCluster>,
  ): Promise<AdCluster[]> {
    return this.adClusterRepository.find(filter);
  }

  @patch('/AdClusterAction', {
    responses: {
      '200': {
        description: 'AdCluster PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(AdCluster, {partial: true}),
        },
      },
    })
    adCluster: AdCluster,
    @param.where(AdCluster) where?: Where<AdCluster>,
  ): Promise<Count> {
    return this.adClusterRepository.updateAll(adCluster, where);
  }

  @get('/AdClusterAction/{id}', {
    responses: {
      '200': {
        description: 'AdCluster model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(AdCluster, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(AdCluster, {exclude: 'where'}) filter?: FilterExcludingWhere<AdCluster>
  ): Promise<AdCluster> {
    return this.adClusterRepository.findById(id, filter);
  }

  @patch('/AdClusterAction/{id}', {
    responses: {
      '204': {
        description: 'AdCluster PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(AdCluster, {partial: true}),
        },
      },
    })
    adCluster: AdCluster,
  ): Promise<void> {
    await this.adClusterRepository.updateById(id, adCluster);
  }

  @put('/AdClusterAction/{id}', {
    responses: {
      '204': {
        description: 'AdCluster PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() adCluster: AdCluster,
  ): Promise<void> {
    await this.adClusterRepository.replaceById(id, adCluster);
  }

  @del('/AdClusterAction/{id}', {
    responses: {
      '204': {
        description: 'AdCluster DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.adClusterRepository.deleteById(id);
  }
}
