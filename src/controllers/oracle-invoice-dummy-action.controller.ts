// Uncomment these imports to begin using these cool features!

import {authenticate} from '@loopback/authentication';
import {UserRepository} from '@loopback/authentication-jwt';
import {Filter, repository} from '@loopback/repository';
import {get, getModelSchemaRef, param, post, requestBody} from '@loopback/rest';
import {NoFakturPajakDummy} from '../models';
import {AdMasterRepository, DataDigiposRepository, DataFpjpRepository, DataUserRepository, NoFakturPajakDummyRepository, NoFakturPajakRepository, RolemappingRepository} from '../repositories';
import {ControllerConfig} from './controller.config';
// import {inject} from '@loopback/core';
const mergePDF = require('pdf-merge');

export const UpdateMasterRequestBodyDummy = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['ad_code', 'interface_status', "organization_id", "error_message"],
        properties: {
          interface_status: {
            type: 'string',
          },
          organization_id: {
            type: 'string',
          },
          ad_code: {
            type: 'string',
          },
          error_message: {
            type: 'string',
          }
        }
      }
    },
  },
};



export const UpdateInvoiceRequestBodyDummy = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['id', 'oracle_id', "interface_status", "error_message"],
        properties: {
          id: {
            type: 'number',
          },
          oracle_id: {
            type: 'string',
          },
          interface_status: {
            type: 'string',
          },
          error_message: {
            type: 'string',
          }
        }
      }
    },
  },
};



export const UpdateFPJPRequestBodyDummy = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['id', 'invoice_id', "interface_status", "error_message", "payment"],
        properties: {
          id: {
            type: 'number',
          },
          invoice_id: {
            type: 'string',
          },
          interface_status: {
            type: 'string',
          },
          error_message: {
            type: 'string',
          },
          payment: {
            type: 'string',
          },
        }
      }
    },
  },
};


export const RegisterFPNonDigiposRequestBodyDummy = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['id', 'invoice_number', "fp_number"],
        properties: {
          id: {
            type: 'string',
          },
          invoice_number: {
            type: 'string',
          },
          fp_number: {
            type: 'string',
          },
        }
      }
    },
  },
};



export const RequestFPNonDigiposRequestBodyDummy = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['count_req'],
        properties: {
          count_req: {
            type: 'number',
          },
          tahun: {
            type: 'string',
          },
        }
      }
    },
  },
};


export const CacelFPNonDigiposRequestBodyDummy = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['fp_number', 'invoice_number'],
        properties: {
          fp_number: {
            type: 'string',
          },
          invoice_number: {
            type: 'string',
          },
        }
      }
    },
  },
};

export const ChangeFPNonDigiposRequestBodyDummy = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['id', "fp_number"],
        properties: {
          id: {
            type: 'string',
          },
          fp_number: {
            type: 'string',
          },
        }
      }
    },
  },
};


export const UpdateFPNonDigiposRequestBodyDummy = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['id', "fp_number", "no_invoice", "tahun"],
        properties: {
          id: {
            type: 'string',
          },
          fp_number: {
            type: 'string',
          },
          no_invoice: {
            type: 'string',
          },
        }
      }
    },
  },
};

@authenticate('jwt')
export class OracleInvoiceDummyActionController {
  constructor(
    @repository(DataDigiposRepository)
    public dataDigiposRepository: DataDigiposRepository,
    @repository(DataFpjpRepository)
    public dataFpjpRepository: DataFpjpRepository,
    @repository(DataUserRepository)
    public dataUserRepository: DataUserRepository,
    @repository(UserRepository)
    public userRepository: UserRepository,
    @repository(RolemappingRepository)
    public rolemappingRepository: RolemappingRepository,
    @repository(AdMasterRepository)
    public adMasterRepository: AdMasterRepository,
    @repository(NoFakturPajakRepository)
    public noFakturPajakRepository: NoFakturPajakRepository,
    @repository(NoFakturPajakDummyRepository)
    public noFakturPajakDummyRepository: NoFakturPajakDummyRepository,
  ) { }

  @get('/oracle/dummy/no_faktur/nondigipos', {
    responses: {
      '200': {
        description: 'Array of Cistomer  model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(NoFakturPajakDummy, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async findNoFakturDummy(
    @param.filter(NoFakturPajakDummy) filter?: Filter<NoFakturPajakDummy>,
  ): Promise<NoFakturPajakDummy[]> {

    var filterSet: any; filterSet = {}
    var filters: any; filters = filter;
    for (var i in filters) {
      filterSet[i] = filters[i];
    }


    if (filterSet.where != undefined) {
      var thisWhere = filterSet.where;
      filterSet.where = {and: [{type_faktur: 2}, {or: [{no_invoice_link: null}, {no_invoice_link: ""}]}, thisWhere]};
    } else {
      filterSet.where = {and: [{type_faktur: 2}, {or: [{no_invoice_link: null}, {no_invoice_link: ""}]}]}
    }


    var datas: any; datas = [];

    var dataNoFaktur = await this.noFakturPajakDummyRepository.find(filterSet);
    for (var iNo in dataNoFaktur) {
      var rawFaktur = dataNoFaktur[iNo];
      var LabelSet = rawFaktur.description == undefined ? "".split("-") : rawFaktur.description.split("-");
      if (LabelSet[0].trim() != "NON_DIGIPOS") {
        continue;
      }
      datas.push({
        ID: rawFaktur.no_faktur_pajak,
        SEQUENTIAL_FP: rawFaktur.number_fp,
        NO_FAKTUR_PAJAK_DEFAULT: rawFaktur.no_faktur_link,
        DESCRIPTION: rawFaktur.description,
        PERIODE: rawFaktur.periode
      });
    }

    return datas;
  }


  @post('/oracle/dummy/no_faktur/nondigipos/request', {
    responses: {
      '200': {
        description: 'Invoice model instance',
        content: {
          'application/json': {
            schema: RequestFPNonDigiposRequestBodyDummy
          }
        },
      },
    },
  })
  async requestNoFaktur(
    @requestBody(RequestFPNonDigiposRequestBodyDummy) requestData: {count_req: number, tahun: string},
  ): Promise<Object> {
    var returnMs: any; returnMs = {success: true, message: ""};
    try {
      if (requestData.count_req > 0 && requestData.count_req != null && requestData.count_req != undefined) {
        var readyNoFP = await this.noFakturPajakDummyRepository.find({where: {and: [{fp_state: 0}]}, limit: requestData.count_req, order: ["id ASC"]});
        if (requestData.tahun != "" || requestData.tahun != undefined) {
          readyNoFP = await this.noFakturPajakDummyRepository.find({where: {and: [{fp_state: 0}, {periode: requestData.tahun}]}, limit: requestData.count_req, order: ["id ASC"]});
        }

        if (readyNoFP.length > 0) {
          for (var iNo in readyNoFP) {
            var infoNoFP = readyNoFP[iNo];
            await this.noFakturPajakDummyRepository.updateById(infoNoFP.id, {
              description: "NON_DIGIPOS-Register Faktur From Oracle",
              type_faktur: 2,
              fp_state: 1,
              no_invoice_link: undefined,
              no_faktur_link: undefined,
              at_used: ControllerConfig.onCurrentTime().toString()
            });
          }
        } else {
          returnMs.success = true;
          returnMs.message = "not avaliable no faktur pajak, please confirm to tax team !";
        }
      } else {
        returnMs.success = true;
        returnMs.message = "not data requuest !";
      }

    } catch (error) {
      returnMs.success = false;
      returnMs.message = error;
    }
    return returnMs;
  }


  @post('/oracle/dummy/no_faktur/nondigipos/used', {
    responses: {
      '200': {
        description: 'Invoice model instance',
        content: {
          'application/json': {
            schema: RegisterFPNonDigiposRequestBodyDummy
          }
        },
      },
    },
  })
  async registerNoFaktur(
    @requestBody(RegisterFPNonDigiposRequestBodyDummy) registerNo: {id: string, invoice_number: string, fp_number: string},
  ): Promise<Object> {
    var returnMs: any; returnMs = {success: true, message: ""};
    try {

      var readyNoFP = await this.noFakturPajakDummyRepository.findOne({where: {and: [{no_faktur_pajak: registerNo.id}]}});
      if (readyNoFP != null) {
        var LabelSet = readyNoFP.description == undefined ? "".split("-") : readyNoFP.description.split("-");
        if (LabelSet[0].trim() == "NON_DIGIPOS") {
          if (readyNoFP.no_invoice_link == null || readyNoFP.no_invoice_link == "") {

            var hasNoInv = await this.noFakturPajakDummyRepository.findOne({where: {and: [{no_invoice_link: registerNo.invoice_number}]}});
            if (hasNoInv == null) {
              await this.noFakturPajakDummyRepository.updateById(readyNoFP.id, {
                no_invoice_link: registerNo.invoice_number,
                no_faktur_link: registerNo.fp_number,
                at_used: ControllerConfig.onCurrentTime().toString()
              });
              returnMs.success = true;
              returnMs.message = "SUCCESS !";
            } else {
              returnMs.success = true;
              returnMs.message = "No invoice sudah memiliki no faktur !";
            }
          } else {
            returnMs.success = true;
            returnMs.message = "No Faktur Pajak tidak diperuntukan untuk non digipos !";
          }
        } else {
          returnMs.success = true;
          returnMs.message = "No Faktur Pajak Sudah Digunakan !";
        }
      } else {
        returnMs.success = true;
        returnMs.message = "document fpjp not found !";
      }

    } catch (error) {
      returnMs.success = false;
      returnMs.message = error;
    }
    return returnMs;
  }

  @post('/oracle/dummy/no_faktur/nondigipos/cancel', {
    responses: {
      '200': {
        description: 'Invoice model instance',
        content: {
          'application/json': {
            schema: CacelFPNonDigiposRequestBodyDummy
          }
        },
      },
    },
  })
  async cancelNoFaktur(
    @requestBody(CacelFPNonDigiposRequestBodyDummy) registerNo: {invoice_number: string, fp_number: string},
  ): Promise<Object> {
    var returnMs: any; returnMs = {success: true, message: ""};
    try {

      var readyNoFP = await this.noFakturPajakDummyRepository.findOne({where: {and: [{no_faktur_link: registerNo.fp_number}, {no_invoice_link: registerNo.invoice_number}]}});
      if (readyNoFP != null) {
        var LabelSet = readyNoFP.description == undefined ? "".split("-") : readyNoFP.description.split("-");
        if (LabelSet[0].trim() == "NON_DIGIPOS") {
          try {
            await this.noFakturPajakDummyRepository.updateById(readyNoFP.id, {
              no_invoice_link: undefined,
              type_faktur: 1,
              no_faktur_link: undefined,
              fp_state: 0,
              description: undefined,
              at_used: undefined
            });
            returnMs.success = true;
            returnMs.message = "Success No Faktur Pajak Cancel !";
          } catch (error) {
            returnMs.success = false;
            returnMs.message = error;
          }
        } else {
          returnMs.success = true;
          returnMs.message = "No Faktur Pajak not found !";
        }
      } else {
        returnMs.success = true;
        returnMs.message = "No Faktur Pajak not found !";
      }
    } catch (error) {
      returnMs.success = false;
      returnMs.message = error;
    }
    return returnMs;
  }



  @post('/oracle/dummy/no_faktur/nondigipos/update-info', {
    responses: {
      '200': {
        description: 'Invoice model instance',
        content: {
          'application/json': {
            schema: UpdateFPNonDigiposRequestBodyDummy
          }
        },
      },
    },
  })
  async updateInfoNoFaktur(
    @requestBody(UpdateFPNonDigiposRequestBodyDummy) registerNo: {id: string, fp_number: string, no_invoice: string},
  ): Promise<Object> {
    var returnMs: any; returnMs = {success: true, message: ""};
    try {
      var readyNoFP = await this.noFakturPajakDummyRepository.findOne({where: {and: [{no_faktur_pajak: registerNo.id}]}});
      if (readyNoFP != null) {
        var LabelSet = readyNoFP.description == undefined ? "".split("-") : readyNoFP.description.split("-");
        if (LabelSet[0].trim() == "NON_DIGIPOS") {
          var infoInv = await this.noFakturPajakDummyRepository.findOne({where: {and: [{no_invoice_link: registerNo.no_invoice}, {no_faktur_pajak: {neq: registerNo.id}}]}});

          if (infoInv == null) {
            try {
              await this.noFakturPajakDummyRepository.updateById(readyNoFP.id, {
                no_faktur_link: registerNo.fp_number,
                no_invoice_link: registerNo.fp_number,
                at_used: ControllerConfig.onCurrentTime().toString()
              });
              returnMs.success = true;
              returnMs.message = "Success No Faktur Pajak Update !";
            } catch (error) {
              returnMs.success = false;
              returnMs.message = error;
            }
          } else {
            returnMs.success = true;
            returnMs.message = "Number Invoice already have no faktur !";
          }

        } else {
          returnMs.success = true;
          returnMs.message = "No Faktur Pajak not found !";
        }
      } else {
        returnMs.success = true;
        returnMs.message = "No Faktur Pajak not found !";
      }
    } catch (error) {
      returnMs.success = false;
      returnMs.message = error;
    }
    return returnMs;
  }

  @post('/oracle/dummy/no_faktur/nondigipos/change', {
    responses: {
      '200': {
        description: 'Invoice model instance',
        content: {
          'application/json': {
            schema: ChangeFPNonDigiposRequestBodyDummy
          }
        },
      },
    },
  })
  async changeNoFaktur(
    @requestBody(ChangeFPNonDigiposRequestBodyDummy) registerNo: {id: string, fp_number: string},
  ): Promise<Object> {
    var returnMs: any; returnMs = {success: true, message: ""};
    try {
      var readyNoFP = await this.noFakturPajakDummyRepository.findOne({where: {and: [{no_faktur_pajak: registerNo.id}]}});
      if (readyNoFP != null) {
        var LabelSet = readyNoFP.description == undefined ? "".split("-") : readyNoFP.description.split("-");
        if (LabelSet[0].trim() == "NON_DIGIPOS") {
          var NoFakturChange = registerNo.fp_number.substring(4);
          if (NoFakturChange == registerNo.id) {
            try {
              await this.noFakturPajakDummyRepository.updateById(readyNoFP.id, {
                no_faktur_link: registerNo.fp_number,
                at_used: ControllerConfig.onCurrentTime().toString()
              });
              returnMs.success = true;
              returnMs.message = "Success No Faktur Pajak Cancel !";
            } catch (error) {
              returnMs.success = false;
              returnMs.message = error;
            }
          } else {
            returnMs.success = true;
            returnMs.message = "Format No Faktur Pajak Change Vailed !";
          }

        } else {
          returnMs.success = true;
          returnMs.message = "No Faktur Pajak not found !";
        }
      } else {
        returnMs.success = true;
        returnMs.message = "No Faktur Pajak not found !";
      }
    } catch (error) {
      returnMs.success = false;
      returnMs.message = error;
    }
    return returnMs;
  }

}
