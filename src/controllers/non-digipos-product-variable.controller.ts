// // Uncomment these imports to begin using these cool features!

// import {Filter, repository} from '@loopback/repository';
// import {del, get, getModelSchemaRef, param, post, requestBody} from '@loopback/rest';
// import {MerchantProductVariable} from '../models';
// import {MerchantProductRepository, MerchantProductVariableCalRepository, MerchantProductVariableGroupCalRepository, MerchantProductVariableRepository, MerchantVariableRepository} from '../repositories';
// import {ControllerConfig} from './controller.config';

// // import {inject} from '@loopback/core';


// export class NonDigiposProductVariableController {

//   constructor(
//     @repository(MerchantProductVariableRepository)
//     public merchantProductVariableRepository: MerchantProductVariableRepository,
//     @repository(MerchantProductRepository)
//     public merchantProductRepository: MerchantProductRepository,
//     @repository(MerchantVariableRepository)
//     public merchantVariableRepository: MerchantVariableRepository,
//     @repository(MerchantProductVariableCalRepository)
//     public merchantProductVariableCalRepository: MerchantProductVariableCalRepository,
//     @repository(MerchantProductVariableGroupCalRepository)
//     public merchantProductVariableGroupCalRepository: MerchantProductVariableGroupCalRepository,
//   ) { }

//   @get('/non-digipos-product-variable', {
//     responses: {
//       '200': {
//         description: 'Array of non digipos product variable model instances',
//         content: {
//           'application/json': {
//             schema: {
//               type: 'array',
//               items: getModelSchemaRef(MerchantProductVariable, {includeRelations: true}),
//             },
//           },
//         },
//       },
//     },
//   })
//   async find(
//     @param.filter(MerchantProductVariable) filter?: Filter<MerchantProductVariable>,
//   ): Promise<[]> {

//     var result: any; result = [];
//     var infoProductVariable = await this.merchantProductVariableRepository.find(filter);
//     var productAva: any; productAva = {};
//     for (var iPV in infoProductVariable) {
//       var rawPV: any; rawPV = infoProductVariable[iPV];
//       if (productAva[rawPV.mp_id] == undefined) {
//         productAva[rawPV.mp_id] = 0;
//       }
//       productAva[rawPV.mp_id] = productAva[rawPV.mp_id] + 1;
//     }

//     var infoProduct = await this.merchantProductRepository.find();
//     for (var i in infoProduct) {
//       var infoPV: any; infoPV = infoProduct[i];
//       if (productAva[infoPV.mp_id] == undefined) {
//         continue;
//       }
//       var data: any; data = {};
//       for (var j in infoPV) {
//         data[j] = infoPV[j];
//       }
//       data["count_field"] = productAva[infoPV.mp_id];
//       result.push(data);
//     }

//     return result;
//   }

//   @get('/non-digipos-product-variable/by-id/{mpv_id}', {
//     responses: {
//       '200': {
//         description: 'non digipos product variable model instance',
//         content: {
//           'application/json': {
//             schema: getModelSchemaRef(MerchantProductVariable, {includeRelations: true}),
//           },
//         },
//       },
//     },
//   })
//   async findById(
//     @param.path.number('mpv_id') id: number,
//   ): Promise<Object> {
//     var result = {};
//     var infoProduct = await this.merchantProductVariableRepository.findById(id);
//     if (infoProduct.mpv_id != undefined) {
//       var infoVariable: any; infoVariable = await this.merchantVariableRepository.findById(infoProduct.mv_id);
//       var data: any; data = {};
//       for (var j in infoVariable) {
//         data[j] = infoVariable[j];
//       }
//       var hasFormula = await this.merchantProductVariableCalRepository.count({mpv_id: infoProduct.mpv_id});
//       data["hasFormula"] = hasFormula.count > 0 ? 1 : 0;
//       data["mpv_id"] = infoProduct.mpv_id;
//       data["mp_id"] = infoProduct.mp_id;
//       result = data;
//     } else {
//       result = {};
//     }
//     return result;
//   }

//   @get('/non-digipos-product-variable/by-product/{mp_id}', {
//     responses: {
//       '200': {
//         description: 'non digipos product variable type model instance',
//         content: {
//           'application/json': {
//             schema: getModelSchemaRef(MerchantProductVariable, {includeRelations: true}),
//           },
//         },
//       },
//     },
//   })
//   async findVariableByProduct(
//     @param.path.number('mp_id') id: number,
//   ): Promise<[]> {
//     var infoProduct = await this.merchantProductRepository.findById(id);
//     var datas: any; datas = {product: infoProduct, selected_field: []};

//     var infoProductVariable = await this.merchantProductVariableRepository.find({where: {mp_id: id}});
//     for (var i in infoProductVariable) {
//       var rawProductVar = infoProductVariable[i];
//       var infoVariable: any; infoVariable = await this.merchantVariableRepository.findById(rawProductVar.mv_id);
//       var data: any; data = {};
//       for (var j in infoVariable) {
//         data[j] = infoVariable[j];
//       }
//       var hasFormula = await this.merchantProductVariableCalRepository.count({mpv_id: rawProductVar.mpv_id});
//       data["hasFormula"] = hasFormula.count > 0 ? 1 : 0;
//       data["mpv_id"] = rawProductVar.mpv_id
//       data["mp_id"] = rawProductVar.mp_id
//       datas["selected_field"].push(data);
//     }
//     return datas;
//   }

//   @post('/non-digipos-product-variable/create', {
//     responses: {
//       '200': {
//         description: 'Menu model instance',
//         content: {'application/json': {schema: getModelSchemaRef(MerchantProductVariable)}},
//       },
//     },
//   })
//   async create(
//     @requestBody({
//       content: {
//         'application/json': {
//           schema: getModelSchemaRef(MerchantProductVariable, {
//             title: 'NewNonDigipostProductVariable',
//             exclude: ['mpv_id', 'mpv_at_create', 'mpv_at_update'],
//           }),
//         },
//       },
//     })
//     variable: Omit<MerchantProductVariable, 'mpv_id'>,
//   ): Promise<Object> {
//     var result: any; result = {success: false, message: "", data: {}};
//     try {
//       var infoCreate = await this.merchantProductVariableRepository.create(variable);
//       var infoUpdate = await this.merchantProductVariableRepository.updateById(infoCreate.mpv_id, {
//         mpv_at_create: ControllerConfig.onCurrentTime().toString()
//       });
//       result.success = true;
//       result.message = "Success, create merchant variable.";
//       result.data = infoUpdate;

//     } catch (error) {
//       result.success = false;
//       result.message = error;
//     }
//     return result;
//   }

//   @del('/non-digipos-product-variable/delete/{id}', {
//     responses: {
//       '200': {
//         description: 'Merchan Product Variable DELETE success',
//       },
//     },
//   })
//   async deleteById(@param.path.number('id') id: number): Promise<Object> {
//     var result: any; result = {success: false, message: ""};
//     try {
//       await this.merchantProductVariableRepository.deleteById(id);
//       await this.merchantProductVariableCalRepository.deleteAll({and: [{mpv_id: id}]});
//       await this.merchantProductVariableGroupCalRepository.deleteAll({and: [{mpv_id: id}]});
//       result.success = true;
//       result.message = "Success, delete product variable.";
//     } catch (error) {
//       result.success = false;
//       result.message = error;
//     }
//     return result;
//   }



// }
