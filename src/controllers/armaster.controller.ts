import {
  Count,
  CountSchema,







  Filter,
  FilterExcludingWhere,


  repository,
  Where
} from '@loopback/repository';
import {
  del, get,
  getModelSchemaRef, param,


  patch, post,




  put,

  requestBody
} from '@loopback/rest';
import {Armaster} from '../models';
import {ArmasterRepository} from '../repositories';



export type EditArStatus = {
  edit_store: any;
  status: boolean;
};

const EditArStatusSchema = {
  type: 'object',
  required: ['edit_store'],
  properties: {
    edit_store: {
      type: 'object',
    }
  },
};

export const EditArStatusRequestBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {schema: EditArStatusSchema},
  },
};

export class ArmasterController {
  constructor(
    @repository(ArmasterRepository)
    public armasterRepository: ArmasterRepository,
  ) {}

  @post('/armasters', {
    responses: {
      '200': {
        description: 'Armaster model instance',
        content: {'application/json': {schema: getModelSchemaRef(Armaster)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Armaster, {
            title: 'NewArmaster',
            exclude: ['id'],
          }),
        },
      },
    })
    armaster: Omit<Armaster, 'id'>,
  ): Promise<Armaster> {
    return this.armasterRepository.create(armaster);
  }

  @get('/armasters/count', {
    responses: {
      '200': {
        description: 'Armaster model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(Armaster) where?: Where<Armaster>,
  ): Promise<Count> {
    return this.armasterRepository.count(where);
  }

  @get('/armasters', {
    responses: {
      '200': {
        description: 'Array of Armaster model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Armaster, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(Armaster) filter?: Filter<Armaster>,
  ): Promise<Armaster[]> {
    return this.armasterRepository.find(filter);
  }

  @post('/areditstatus', {
    responses: {
      '200': {
        description: 'Array of Armaster model instances',
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                edit_store: {
                  type: 'object'
                },
              },
            },
          }
        },
      },
    },
  })
  async areditstatus(
    @requestBody(EditArStatusRequestBody) credentials: EditArStatus,
  ): Promise<Object> {
    var datas: any;
    datas = {};
    for (var i in credentials.edit_store) {
      var idar = i;
      var status = Boolean(credentials.edit_store[idar]);
      var update = await this.armasterRepository.updateById(parseInt(idar), {ar_stat: status});
    }
    datas = {"success": true, "message": "success"}
    // if (credentials.edit_store.arcode == undefined) {
    //   datas = {"error": {"message": "Can't not key arcode from edit_store, please set arcode in edit_store parameter !"}};
    // } else {
    //   // this.armasterRepository.find(filter);
    //   for (var i in credentials.edit_store.arcode) {
    //     var idar = credentials.edit_store.arcode[i];
    //     // var status =
    //     console.log();
    //   }
    // }
    return datas;
  }


  @patch('/armasters', {
    responses: {
      '200': {
        description: 'Armaster PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Armaster, {partial: true}),
        },
      },
    })
    armaster: Armaster,
    @param.where(Armaster) where?: Where<Armaster>,
  ): Promise<Count> {
    return this.armasterRepository.updateAll(armaster, where);
  }

  @get('/armasters/{id}', {
    responses: {
      '200': {
        description: 'Armaster model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Armaster, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(Armaster, {exclude: 'where'}) filter?: FilterExcludingWhere<Armaster>
  ): Promise<Armaster> {
    return this.armasterRepository.findById(id, filter);
  }

  @patch('/armasters/{id}', {
    responses: {
      '204': {
        description: 'Armaster PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Armaster, {partial: true}),
        },
      },
    })
    armaster: Armaster,
  ): Promise<void> {
    await this.armasterRepository.updateById(id, armaster);
  }

  @put('/armasters/{id}', {
    responses: {
      '204': {
        description: 'Armaster PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() armaster: Armaster,
  ): Promise<void> {
    await this.armasterRepository.replaceById(id, armaster);
  }

  @del('/armasters/{id}', {
    responses: {
      '204': {
        description: 'Armaster DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.armasterRepository.deleteById(id);
  }
}
