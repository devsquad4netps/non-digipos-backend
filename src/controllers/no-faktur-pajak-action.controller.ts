import {authenticate} from '@loopback/authentication';
import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where
} from '@loopback/repository';
import {
  del, get,
  getModelSchemaRef, param,


  patch, post,




  put,

  requestBody
} from '@loopback/rest';
import {NoFakturPajak} from '../models';
import {AdMasterRepository, DataDigiposRepository, NoFakturPajakRepository} from '../repositories';
import {ControllerConfig} from './controller.config';

export const HistoryRequestBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ["periode"],
        properties: {
          periode: {
            type: 'string'
          }
        }
      }
    },
  },
};


export const BatalRegFPRequestBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ["times", "count_delete"],
        properties: {
          times: {
            type: 'string'
          },
          count_delete: {
            type: 'number'
          }
        }
      }
    },
  },
};

export const CancelMapingFakturRequestBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ["fid"],
        properties: {
          fid: {
            type: 'number'
          }
        }
      }
    },
  },
};

export const RegisterFakturNonDigiposRequestBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ["periode", "code_perfix", "request_count", "description"],
        properties: {
          periode: {
            type: 'string'
          },
          code_perfix: {
            type: 'string'
          },
          request_count: {
            type: 'number'
          },
          description: {
            type: 'string'
          },
        }
      }
    },
  },
};

export const EditFakturNonDigiposRequestBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ["id", "code_perfix", "description"],
        properties: {
          id: {
            type: 'number'
          },
          invoice: {
            type: 'string'
          },
          code_perfix: {
            type: 'string'
          },
          description: {
            type: 'string'
          },
        }
      }
    },
  },
};

export const RegisterNoInvoiceRequestBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ["no_invoice", "periode", "code_perfix"],
        properties: {
          no_invoice: {
            type: 'string'
          },
          code_perfix: {
            type: 'string'
          },
          periode: {
            type: 'string'
          }
        }
      }
    },
  },
};


export const RegisterNoFakturRequestBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['quote_allocation', 'start_number', 'end_number', 'remarks', 'periode', 'fp_buffer'],
        properties: {
          quote_allocation: {
            type: 'string'
          },
          start_number: {
            type: 'number'
          },
          end_number: {
            type: 'number'
          },
          remarks: {
            type: 'string'
          },
          periode: {
            type: 'string'
          },
          fp_buffer: {
            type: "boolean"
          }
        }
      }
    },
  },
};


export const AsignNoFakturRequestBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['periode', 'invoice_start', 'invoice_end', 'use_buffer'],
        properties: {
          periode: {
            type: 'string'
          },
          invoice_start: {
            type: 'string'
          },
          invoice_end: {
            type: 'string'
          },
          use_buffer: {
            type: 'boolean'
          }
        }
      }
    },
  },
};

var NoFakturProcess: any; NoFakturProcess = [];

@authenticate('jwt')
export class NoFakturPajakActionController {
  constructor(
    @repository(NoFakturPajakRepository)
    public noFakturPajakRepository: NoFakturPajakRepository,
    @repository(DataDigiposRepository)
    public dataDigiposRepository: DataDigiposRepository,
    @repository(AdMasterRepository)
    public adMasterRepository: AdMasterRepository,
  ) { }

  @post('/no-faktur-pajaks', {
    responses: {
      '200': {
        description: 'NoFakturPajak model instance',
        content: {'application/json': {schema: getModelSchemaRef(NoFakturPajak)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(NoFakturPajak, {
            title: 'NewNoFakturPajak',
            exclude: ['id'],
          }),
        },
      },
    })
    noFakturPajak: Omit<NoFakturPajak, 'id'>,
  ): Promise<NoFakturPajak> {
    return this.noFakturPajakRepository.create(noFakturPajak);
  }

  @get('/no-faktur-pajaks/count', {
    responses: {
      '200': {
        description: 'NoFakturPajak model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(NoFakturPajak) where?: Where<NoFakturPajak>,
  ): Promise<Count> {
    return this.noFakturPajakRepository.count(where);
  }

  @get('/no-faktur-pajaks', {
    responses: {
      '200': {
        description: 'Array of NoFakturPajak model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(NoFakturPajak, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(NoFakturPajak) filter?: Filter<NoFakturPajak>,
  ): Promise<NoFakturPajak[]> {
    return this.noFakturPajakRepository.find(filter);
  }

  @patch('/no-faktur-pajaks', {
    responses: {
      '200': {
        description: 'NoFakturPajak PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(NoFakturPajak, {partial: true}),
        },
      },
    })
    noFakturPajak: NoFakturPajak,
    @param.where(NoFakturPajak) where?: Where<NoFakturPajak>,
  ): Promise<Count> {
    return this.noFakturPajakRepository.updateAll(noFakturPajak, where);
  }

  @get('/no-faktur-pajaks/{id}', {
    responses: {
      '200': {
        description: 'NoFakturPajak model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(NoFakturPajak, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(NoFakturPajak, {exclude: 'where'}) filter?: FilterExcludingWhere<NoFakturPajak>
  ): Promise<NoFakturPajak> {
    return this.noFakturPajakRepository.findById(id, filter);
  }

  @patch('/no-faktur-pajaks/{id}', {
    responses: {
      '204': {
        description: 'NoFakturPajak PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(NoFakturPajak, {partial: true}),
        },
      },
    })
    noFakturPajak: NoFakturPajak,
  ): Promise<void> {
    await this.noFakturPajakRepository.updateById(id, noFakturPajak);
  }

  @put('/no-faktur-pajaks/{id}', {
    responses: {
      '204': {
        description: 'NoFakturPajak PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() noFakturPajak: NoFakturPajak,
  ): Promise<void> {
    await this.noFakturPajakRepository.replaceById(id, noFakturPajak);
  }

  @del('/no-faktur-pajaks/{id}', {
    responses: {
      '204': {
        description: 'NoFakturPajak DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.noFakturPajakRepository.deleteById(id);
  }

  @get('/no-faktur-pajaks/history-register', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: {}
          }
        },
      },
    },
  })
  async historyRegisterNoFaktur(

  ): Promise<Object> {
    var result: any; result = {}
    var infoHistory = await this.noFakturPajakRepository.execute("SELECT periode , COUNT(id) as jml_request, at_create,  SUM(if( fp_buffer= '1', 1, 0)) AS buffer_req, SUM(if( fp_buffer= '0', 1, 0)) AS not_buffer_reqq   FROM NoFakturPajak GROUP BY periode, at_create");
    result = infoHistory;
    return result;
  }

  @post('/no-faktur-pajaks/register', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: RegisterNoFakturRequestBody
          }
        },
      },
    },
  })
  async registerNoFaktur(
    @requestBody(RegisterNoFakturRequestBody) registerNoFaktur: {quote_allocation: string, start_number: number, end_number: number, remarks: string, periode: string, fp_buffer: boolean},
  ): Promise<Object> {
    // var infoFaktur = await this.noFakturPajakRepository.find({where: {and: [{periode: registerNoFaktur.periode}]}});
    // 010.XXX-YY.12345678
    // var no = registerNoFaktur.start_number;
    // for (var i = start_number in infoFaktur) {
    //   var dataFaktur = infoFaktur[i];
    //   var Label = registerNoFaktur.quote_allocation + "-" + registerNoFaktur.periode;
    //   no++;
    // }
    var returnMs: any; returnMs = {success: true, message: ""};
    var CreateAllow = true;
    var DuplicateNo = "";
    var DateCreate = ControllerConfig.onCurrentTime();
    for (var i = registerNoFaktur.start_number; i <= registerNoFaktur.end_number; i++) {
      var Label = registerNoFaktur.quote_allocation + "-" + registerNoFaktur.periode + "." + i;
      var infoFaktur = await this.noFakturPajakRepository.count({and: [{periode: registerNoFaktur.periode}, {no_faktur_pajak: Label}]});
      if (infoFaktur.count > 0) {
        CreateAllow = false;
        DuplicateNo = Label;
        break;
      }
    }

    var maxNumber = await this.noFakturPajakRepository.findOne({order: ['number_fp DESC'], where: {and: [{periode: registerNoFaktur.periode}, {fp_quota_alocate: registerNoFaktur.quote_allocation}]}})


    if (CreateAllow) {
      var no = 0;
      for (var i = registerNoFaktur.start_number; i <= registerNoFaktur.end_number; i++) {
        var Label = registerNoFaktur.quote_allocation + "-" + registerNoFaktur.periode + "." + i;
        await this.noFakturPajakRepository.create({
          no_faktur_pajak: Label,
          number_fp: i,
          periode: registerNoFaktur.periode,
          at_create: DateCreate.toString(),
          remarks: registerNoFaktur.remarks,
          fp_state: 0,
          fp_buffer: registerNoFaktur.fp_buffer,
          fp_quota_alocate: registerNoFaktur.quote_allocation
        });
        no++;
        returnMs.message = "success register no faktur pajak, count register is [" + no + "]";
      }
    } else {
      returnMs.success = false;
      returnMs.message = "error register no faktur pajak, because no faktur pajak [" + DuplicateNo + "] is duplicate, Last no faktur pajak is : [" + maxNumber?.number_fp + "]";
    }
    return returnMs;
  }


  @post('/no-faktur-pajaks/assign', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: AsignNoFakturRequestBody
          }
        },
      },
    },
  })
  async asignNoFaktur(
    @requestBody(AsignNoFakturRequestBody) assignNoFaktur: {periode: string, invoice_start: string, invoice_end: string, use_buffer: boolean},
  ): Promise<Object> {
    var returnMs: any; returnMs = {success: false, message: ""};

    var PeriodeSpace = assignNoFaktur.periode.split("-");
    var MM = PeriodeSpace[1];
    var YY = PeriodeSpace[0];
    var digipos = await this.dataDigiposRepository.find({order: ['inv_number ASC'], where: {and: [{trx_date_group: assignNoFaktur.periode}, {acc_file_invoice: 1}]}})
    var noFaktur = await this.noFakturPajakRepository.find({order: ['fp_buffer ASC', 'number_fp ASC'], where: {and: [{periode: YY}, {fp_state: 0}]}});
    // var noFaktur = await this.noFakturPajakRepository.find({order: ['fp_buffer ASC', 'number_fp ASC'], where: {and: [{fp_state: 0}]}});
    var faktur: any; faktur = [];
    for (var iFo in noFaktur) {
      var infoFaktur = noFaktur[iFo];
      if (infoFaktur.fp_buffer == true) {
        if (assignNoFaktur.use_buffer == false) {
          continue;
        }
      }
      faktur.push(infoFaktur);
    }

    if (faktur.length > 0) {
      // 010.XXX - YY.12345678
      // var Invoice = await this.
      var no = 0;
      var foundDigiposStart = false;
      for (var i in digipos) {
        var infoDigipos = digipos[i];
        if (infoDigipos.inv_number == "" || infoDigipos.vat_number != "") {
          continue;
        }

        if (foundDigiposStart == false) {
          if (infoDigipos.inv_number == assignNoFaktur.invoice_start) {
            foundDigiposStart = true;
          }
        }

        if (foundDigiposStart == false) {
          continue;
        }

        if (faktur[no].no_faktur_pajak != undefined) {

          var NotUsed = 0;
          var NoFakturSet = "";
          while (NotUsed < 1) {
            console.log(faktur[no].no_faktur_pajak);
            var fakturNotUsed = await this.noFakturPajakRepository.find({where: {and: [{no_faktur_pajak: faktur[no].no_faktur_pajak}, {fp_state: 1}]}});
            if (fakturNotUsed.length == 0) {
              NotUsed = 1;
              NoFakturSet = faktur[no].no_faktur_pajak;
            } else {
              no++;
            }
          }

          var dataMaster = await this.adMasterRepository.findOne({where: {ad_code: infoDigipos.ad_code}});
          var NoPajak = dataMaster?.prefix + "." + NoFakturSet;//faktur[no].no_faktur_pajak;
          try {
            await this.dataDigiposRepository.updateById(infoDigipos.digipost_id, {
              vat_number: NoPajak
            });
            try {
              await this.noFakturPajakRepository.updateById(faktur[no].id, {
                fp_state: 1,
                no_invoice_link: infoDigipos.inv_number,
                type_faktur: 1,
                no_faktur_link: NoPajak,
                at_used: ControllerConfig.onCurrentTime().toString()
              });
              returnMs.success = true;
              returnMs.message = "Success assign no faktur in periode : " + assignNoFaktur.periode + " !"
            } catch (error) {
              returnMs.success = false;
              returnMs.message = error
            }
          } catch (error) {
            returnMs.success = false;
            returnMs.message = error
          };
        } else {
          returnMs.success = false;
          returnMs.message = "no faktur not found in periode : " + assignNoFaktur.periode + " !"
        }
        if (infoDigipos.inv_number == assignNoFaktur.invoice_end) {
          break;
        }
        no++;
      }
    } else {
      returnMs.success = false;
      returnMs.message = "no faktur not found in periode : " + assignNoFaktur.periode + " !"
    }

    return returnMs;
  }


  @get('/no-faktur-pajaks/data-reg-non-digipos', {
    responses: {
      '200': {
        description: 'Array of DataDigiposAttention model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(NoFakturPajak, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async findDataRegNon(
    @param.filter(NoFakturPajak) filter?: Filter<NoFakturPajak>,
  ): Promise<NoFakturPajak[]> {
    return this.noFakturPajakRepository.find(filter);
  }



  @post('/no-faktur-pajaks/register-non-digipos', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: RegisterFakturNonDigiposRequestBody
          }
        },
      },
    },
  })
  async registerFpNonDigipos(
    @requestBody(RegisterFakturNonDigiposRequestBody) regNoFaktur: {periode: string, code_perfix: string, request_count: number, description: string},
  ): Promise<Object> {

    var returnMs: any; returnMs = {success: true, message: ""};
    var dataFaktur = await this.noFakturPajakRepository.find({where: {and: [{periode: regNoFaktur.periode}, {fp_state: 0}]}});
    if (dataFaktur.length > 0) {
      var no = 1;
      for (var i in dataFaktur) {
        var rawFaktur = dataFaktur[i];
        if (NoFakturProcess.indexOf(rawFaktur.no_faktur_pajak) == -1) {
          try {
            await this.noFakturPajakRepository.updateById(rawFaktur.id, {
              fp_state: 1,
              no_invoice_link: "",
              type_faktur: 2,
              no_faktur_link: (regNoFaktur.code_perfix + "." + rawFaktur.no_faktur_pajak),
              description: regNoFaktur.description,
              at_used: ControllerConfig.onCurrentTime().toString()
            });
            returnMs.success = true;
            returnMs.message = "Success register no faktur pajak non digipos " + regNoFaktur.request_count + " in periode " + regNoFaktur.periode;
          } catch (error) {
            returnMs.success = false;
            returnMs.message = error;
          }

          if (no == regNoFaktur.request_count) {
            break;
          }
          no++;

        }
      }
    } else {
      returnMs.success = false;
      returnMs.message = "Error, Register no faktur non digipos ! ";
    }
    return returnMs;
  }


  @post('/no-faktur-pajaks/edit-reg-non-digipos', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: EditFakturNonDigiposRequestBody
          }
        },
      },
    },
  })
  async EditNoFakturNoDigipos(
    @requestBody(EditFakturNonDigiposRequestBody) regNoFaktur: {id: number, invoice: string, code_perfix: string, description: string},
  ): Promise<Object> {
    var returnMs: any; returnMs = {success: true, message: ""};
    var dataFaktur = await this.noFakturPajakRepository.find({where: {id: regNoFaktur.id}});
    if (dataFaktur.length > 0) {
      for (var i in dataFaktur) {
        var rawFaktur = dataFaktur[i];
        if (NoFakturProcess.indexOf(rawFaktur.no_faktur_pajak) == -1) {
          try {
            await this.noFakturPajakRepository.updateById(rawFaktur.id, {
              fp_state: 1,
              no_invoice_link: regNoFaktur.invoice,
              type_faktur: 2,
              no_faktur_link: (regNoFaktur.code_perfix + "." + rawFaktur.no_faktur_pajak),
              description: regNoFaktur.description,
              at_used: ControllerConfig.onCurrentTime().toString()
            });
            returnMs.success = true;
            returnMs.message = "Success update register no faktur pajak !";
          } catch (error) {
            returnMs.success = false;
            returnMs.message = error;
          }
          break;
        }
      }
    } else {
      returnMs.success = true;
      returnMs.message = "Error update register no faktur pajak, data not found !";
    }
    return returnMs;
  }


  @post('/no-faktur-pajaks/cancel-reg-non-digipos', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: CancelMapingFakturRequestBody
          }
        },
      },
    },
  })
  async cancelFpRegNon(
    @requestBody(CancelMapingFakturRequestBody) regNoFaktur: {fid: number},
  ): Promise<Object> {
    var returnMs: any; returnMs = {success: true, message: ""};
    var dataFaktur = await this.noFakturPajakRepository.find({where: {and: [{id: regNoFaktur.fid}, {fp_state: 1}, {type_faktur: 2}]}});
    if (dataFaktur.length > 0) {
      for (var i in dataFaktur) {
        var rawFaktur = dataFaktur[i];
        try {
          await this.noFakturPajakRepository.updateById(rawFaktur.id, {
            no_invoice_link: "",
            type_faktur: 0,
            no_faktur_link: "",
            fp_state: 0,
            description: "",
            at_used: ""
          });
          returnMs.success = true;
          returnMs.message = "success cancel register faktur";
        } catch (error) {
          returnMs.success = false;
          returnMs.message = error;
        }
      }
    } else {
      returnMs.success = false;
      returnMs.message = "No data faktur in registration for non digipos";
    }
    return returnMs;
  }




  @post('/no-faktur-pajaks/batal-register-fp', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: BatalRegFPRequestBody
          }
        },
      },
    },
  })
  async cancelFpRegisterCount(
    @requestBody(BatalRegFPRequestBody) regNoFaktur: {times: string, count_delete: number},
  ): Promise<Object> {
    var returnMs: any; returnMs = {success: true, message: ""};

    var dataFakturUsed = await this.noFakturPajakRepository.find({limit: regNoFaktur.count_delete, order: ["id DESC"], where: {and: [{at_create: regNoFaktur.times}, {fp_state: 1}, {type_faktur: 1}]}});
    var dataFaktur = await this.noFakturPajakRepository.find({limit: regNoFaktur.count_delete, order: ["id DESC"], where: {and: [{at_create: regNoFaktur.times}, {fp_state: 0}, {type_faktur: 1}]}});
    if (dataFakturUsed.length > 0) {
      returnMs.success = false;
      returnMs.message = "Error, register no faktur pajak sudah digunakan.";
    } else {

      if (dataFaktur.length != regNoFaktur.count_delete) {
        if (dataFaktur.length < regNoFaktur.count_delete) {
          returnMs.success = false;
          returnMs.message = "Error, quota no faktur pajak not use not enough.";
        }
      } else {
        if (dataFaktur.length > 0) {
          for (var i in dataFaktur) {
            var rawFaktur = dataFaktur[i];
            await this.noFakturPajakRepository.deleteById(rawFaktur.id);
          }
        } else {
          returnMs.success = false;
          returnMs.message = "No data no faktur deleted.";
        }
      }
    }
    return returnMs;
  }


}
