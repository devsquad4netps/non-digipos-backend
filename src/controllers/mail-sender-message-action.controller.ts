// Uncomment these imports to begin using these cool features!

import {MysqlAccessPortalDataSource, MysqlDataSource} from '../datasources';
import {AdMasterRepository, DataFpjpRepository, MailAccountNotifRepository, MailSenderMessageRepository} from '../repositories';
import {ControllerConfig} from './controller.config';
import {MailConfig} from './mail.config';

var TMP_PROSESS: any; TMP_PROSESS = {};
var Running: boolean; Running = false;


const DATABASES = new MysqlDataSource();
const DATABASE_AD = new MysqlAccessPortalDataSource();

export class MailSenderMessageActionController {
  constructor() { }

  static async onRunLog(db: MysqlDataSource) {
    var NO_ANTRY = 0;
    for (var i in TMP_PROSESS) {NO_ANTRY++;}

    var mailSenderLog = new MailSenderMessageRepository(db);
    var DataMailSender = await mailSenderLog.find({where: {and: [{FLAG_SEND: 0}]}});
    for (var i in DataMailSender) {
      var rawMailLog = DataMailSender[i];
      if (rawMailLog.MESSAGE == "" || rawMailLog.MESSAGE == null || rawMailLog.MESSAGE == undefined) {
        continue;
      } else {
        if (rawMailLog.MSM_ID != undefined) {
          if (TMP_PROSESS[rawMailLog.MSM_ID] == undefined) {
            TMP_PROSESS[rawMailLog.MSM_ID] = rawMailLog;
          }
        }
      }
    }

    if (NO_ANTRY == 0) {
      this.onRunLogProses();
    }
  }

  static async onRunLogProses() {
    var NO = 0;
    var mailAcountDB = new MailAccountNotifRepository(DATABASES);
    var mailSenderLog = new MailSenderMessageRepository(DATABASES);
    var dataFPJP = new DataFpjpRepository(DATABASES);
    var mailAD = new AdMasterRepository(DATABASES);

    for (var i in TMP_PROSESS) {
      console.log("START SEND MAIL.... (" + i + ")");
      const keyProses = i;
      var rawProses = TMP_PROSESS[i];
      var FLAG_SENDER = 0;
      //REPORT MAIL TO AP
      if (rawProses.SOURCE_DATA == "DataFpjp") {

        var MessageSplit = rawProses.MESSAGE.split("#");
        var MessageTitle: any; MessageTitle = {
          REVIEW_AP: MessageSplit[4] + " HAVE NEW FPJP NEED REVIEW AP",
          RETURN_AP: MessageSplit[4] + " RETURN FPJP FOR AP",
          RETURN_AD: MessageSplit[4] + " DOC FPJP FAILED FROM AP !",
          ON_FPJP_FINISH: "DOCUMENT FPJP FINISH FOR " + MessageSplit[4],
          ON_TREASURY: "NEW FPJP FOR ON PROGRES BY TREASURY " + MessageSplit[4]
        };

        var TYPE_NOTA = "";
        var rawFPJP = await dataFPJP.findOne({where: {transaction_code: MessageSplit[1]}});
        if (rawFPJP != null) {
          TYPE_NOTA = rawFPJP.type_trx == 2 ? "REIMBURSE PPH 23" : "SALES FEE";
        }
        var mailAccount = await mailAcountDB.find({where: {and: [{MAN_FLAG: 1}, {MAN_GROUP: rawProses.MESSAGE_FOR}]}});
        var account: any; account = [];
        var txtAccount: string; txtAccount = "";
        for (var iM in mailAccount) {
          var rawMail = mailAccount[iM];
          account.push(rawMail.MAN_ACCOUNT);
          var countMail = rawMail.MAN_ACCOUNT == undefined ? 0 : rawMail.MAN_ACCOUNT.length;
          if (txtAccount.length + countMail <= 500) {
            txtAccount += "," + rawMail.MAN_ACCOUNT;
          }
        }
        mailSenderLog.updateById(parseInt(keyProses), {SENDER_TO: txtAccount.substring(1)});

        if (account.length > 0) {

          //FOR REVIEW AP
          if (rawProses.MESSAGE_FOR == "REVIEW_AP") {
            MailConfig.onSendMailDefault(keyProses, MessageTitle[rawProses.MESSAGE_FOR], account, {
              TITLE_MESSAGE: MessageTitle[rawProses.MESSAGE_FOR],
              TEXT_MESSSAGE: "Untuk TIM AP, segera lakukan review document FPJP yang telah di submit untuk No FPJP : " + MessageSplit[1] + ", Periode : " + MessageSplit[2] + ", NO : " + MessageSplit[3] + ", Suplier : " + MessageSplit[4] + ", Type Transaksi : " + TYPE_NOTA,
              LINK_MESSSAGE: "https://digipos.linkaja.com/",
              LINK_MESSAGE_LABEL: "BA-TOOLS"
            }, async (r: any) => {
              if (r.success == true) {
                FLAG_SENDER = 1;
                await mailSenderLog.updateById(rawProses.MSM_ID, {
                  AT_SEND: ControllerConfig.onCurrentTime().toString(),
                  FLAG_SEND: 1
                });
              } else {
                await mailSenderLog.updateById(rawProses.MSM_ID, {
                  AT_SEND: undefined,
                  AT_UPDATE: ControllerConfig.onCurrentTime().toString(),
                  FLAG_SEND: 2
                });
              }
              delete TMP_PROSESS[r.id];
              console.log("END SEND MAIL.... (" + r.id + ")");
              this.onRunLogProses();
            });
          }


          if (rawProses.MESSAGE_FOR == "RETURN_AP") {
            MailConfig.onSendMailDefault(keyProses, MessageTitle[rawProses.MESSAGE_FOR], account, {
              TITLE_MESSAGE: MessageTitle[rawProses.MESSAGE_FOR],
              TEXT_MESSSAGE: "Untuk TIM AP, segera lakukan review document FPJP yang telah diperbaiki oleh Suplier/Authorization Dealer untuk No FPJP : " + MessageSplit[1] + ", Periode : " + MessageSplit[2] + ", NO : " + MessageSplit[3] + ", Suplier : " + MessageSplit[4] + ", Type Transaksi : " + TYPE_NOTA + ", Dengan Note : " + MessageSplit[5],
              LINK_MESSSAGE: "https://digipos.linkaja.com/",
              LINK_MESSAGE_LABEL: "BA-TOOLS"
            }, async (r: any) => {
              if (r.success == true) {
                FLAG_SENDER = 1;
                await mailSenderLog.updateById(rawProses.MSM_ID, {
                  AT_SEND: ControllerConfig.onCurrentTime().toString(),
                  FLAG_SEND: 1
                });
              } else {
                await mailSenderLog.updateById(rawProses.MSM_ID, {
                  AT_SEND: undefined,
                  AT_UPDATE: ControllerConfig.onCurrentTime().toString(),
                  FLAG_SEND: 2
                });
              }
              delete TMP_PROSESS[r.id];
              console.log("END SEND MAIL.... (" + r.id + ")");
              this.onRunLogProses();
            });
          }

          if (rawProses.MESSAGE_FOR == "ON_FPJP_FINISH") {
            MailConfig.onSendMailDefault(keyProses, MessageTitle[rawProses.MESSAGE_FOR], account, {
              TITLE_MESSAGE: MessageTitle[rawProses.MESSAGE_FOR],
              TEXT_MESSSAGE: "Untuk TIM AP, document FPJP Suplier/Authorization Dealer untuk No FPJP : " + MessageSplit[1] + ", Periode : " + MessageSplit[2] + ", NO : " + MessageSplit[3] + ", Suplier : " + MessageSplit[4] + ", Type Transaksi : " + TYPE_NOTA + ", telah selesai dibayarkan.",
              LINK_MESSSAGE: "https://digipos.linkaja.com/",
              LINK_MESSAGE_LABEL: "BA-TOOLS"
            }, async (r: any) => {
              if (r.success == true) {
                FLAG_SENDER = 1;
                await mailSenderLog.updateById(rawProses.MSM_ID, {
                  AT_SEND: ControllerConfig.onCurrentTime().toString(),
                  FLAG_SEND: 1
                });
              } else {
                await mailSenderLog.updateById(rawProses.MSM_ID, {
                  AT_SEND: undefined,
                  AT_UPDATE: ControllerConfig.onCurrentTime().toString(),
                  FLAG_SEND: 2
                });
              }
              delete TMP_PROSESS[r.id];
              console.log("END SEND MAIL.... (" + r.id + ")");
              this.onRunLogProses();
            });
          }

          if (rawProses.MESSAGE_FOR == "ON_TREASURY") {
            MailConfig.onSendMailDefault(keyProses, MessageTitle[rawProses.MESSAGE_FOR], account, {
              TITLE_MESSAGE: MessageTitle[rawProses.MESSAGE_FOR],
              TEXT_MESSSAGE: "Untuk TIM AP, document FPJP Suplier/Authorization Dealer untuk No FPJP : " + MessageSplit[1] + ", Periode : " + MessageSplit[2] + ", NO : " + MessageSplit[3] + ", Suplier : " + MessageSplit[4] + ", Type Transaksi : " + TYPE_NOTA + ", telah memasuki status Progres By Treasury.",
              LINK_MESSSAGE: "https://digipos.linkaja.com/",
              LINK_MESSAGE_LABEL: "BA-TOOLS"
            }, async (r: any) => {
              if (r.success == true) {
                FLAG_SENDER = 1;
                await mailSenderLog.updateById(rawProses.MSM_ID, {
                  AT_SEND: ControllerConfig.onCurrentTime().toString(),
                  FLAG_SEND: 1
                });
              } else {
                await mailSenderLog.updateById(rawProses.MSM_ID, {
                  AT_SEND: undefined,
                  AT_UPDATE: ControllerConfig.onCurrentTime().toString(),
                  FLAG_SEND: 2
                });
              }
              delete TMP_PROSESS[r.id];
              console.log("END SEND MAIL.... (" + r.id + ")");
              this.onRunLogProses();
            });
          }

        }

        if (rawProses.MESSAGE_FOR == "RETURN_AD") {
          var rawAD = await mailAD.findById(MessageSplit[3]);
          if (rawAD.email != undefined) {
            var emailSenderAD = rawAD.email != null || rawAD.email != "" ? rawAD.email : "";
            if (emailSenderAD == "") {
              await mailSenderLog.updateById(rawProses.MSM_ID, {
                AT_SEND: ControllerConfig.onCurrentTime().toString(),
                FLAG_SEND: 2
              });
            } else {
              MailConfig.onSendMailDefault(keyProses, MessageTitle[rawProses.MESSAGE_FOR], emailSenderAD, {
                TITLE_MESSAGE: MessageTitle[rawProses.MESSAGE_FOR],
                TEXT_MESSSAGE: "Untuk " + MessageSplit[4] + ", segera lakukan perbaikan document FPJP karena document tersebut memiliki kesalahan sebagai berikut :  No FPJP : " + MessageSplit[1] + ", Periode : " + MessageSplit[2] + ", Type Transaksi : " + TYPE_NOTA + ", Dengan Note : " + MessageSplit[5],
                LINK_MESSSAGE: "https://digipos.linkaja.com/",
                LINK_MESSAGE_LABEL: "BA-TOOLS"
              }, async (r: any) => {
                if (r.success == true) {
                  FLAG_SENDER = 1;
                  await mailSenderLog.updateById(rawProses.MSM_ID, {
                    SENDER_TO: emailSenderAD,
                    AT_SEND: ControllerConfig.onCurrentTime().toString(),
                    FLAG_SEND: 1
                  });
                } else {
                  await mailSenderLog.updateById(rawProses.MSM_ID, {
                    SENDER_TO: emailSenderAD,
                    AT_SEND: undefined,
                    AT_UPDATE: ControllerConfig.onCurrentTime().toString(),
                    FLAG_SEND: 2
                  });
                }
                delete TMP_PROSESS[r.id];
                console.log("END SEND MAIL.... (" + r.id + ")");
                this.onRunLogProses();
              });
            }
          } else {
            await mailSenderLog.updateById(rawProses.MSM_ID, {
              AT_SEND: undefined,
              AT_UPDATE: ControllerConfig.onCurrentTime().toString(),
              FLAG_SEND: 2
            });
          }
        }

        //START RUN FPJP APPROVAL
        if (rawProses.MESSAGE_FOR == "APPROVAL_FPJP") {
          if (MessageSplit.length > 0) {
            var FPJP_ID = MessageSplit[0];
            var NO_FPJP = MessageSplit[1];
            var PERIODE = MessageSplit[2];
            var FPJP_TYPE = MessageSplit[3];
            var APPROVAL_NAME = MessageSplit[4];
            var SUBMIT_NAME = MessageSplit[5];
            var TOKEN_EMAIL = MessageSplit[6];
          }

          var FPJP_STORE = await dataFPJP.findById(parseInt(FPJP_ID));
          if (FPJP_STORE != null) {
            var CONTENT_FPJP: any; CONTENT_FPJP = {
              name_approval: APPROVAL_NAME,
              fpjp_number: NO_FPJP,
              fpjp_submit: SUBMIT_NAME,
              link_approval: process.env.API_URL_FPJP,
              fpjp_type: FPJP_TYPE,
              fpjp_name: "FPJP For " + FPJP_STORE.ad_name,
              currency: "IDR ( Rupiah )",
              amount: "Rp." + ControllerConfig.onGeneratePuluhan(FPJP_STORE.total_amount == undefined ? 0 : FPJP_STORE.total_amount),
              link_list_approval: process.env.API_URL_FPJP,
              remarks: FPJP_STORE.remarks,
              token_set: TOKEN_EMAIL
            }
            if (rawProses.SENDER_TO != "" && rawProses.SENDER_TO != null) {
              MailConfig.onSendMailFPJP(rawProses.SENDER_TO, CONTENT_FPJP, async (r: any) => {
                if (r.success == true) {
                  FLAG_SENDER = 1;
                  await mailSenderLog.updateById(rawProses.MSM_ID, {
                    SENDER_TO: rawProses.SENDER_TO,
                    AT_SEND: ControllerConfig.onCurrentTime().toString(),
                    FLAG_SEND: 1
                  });
                } else {
                  await mailSenderLog.updateById(rawProses.MSM_ID, {
                    SENDER_TO: rawProses.SENDER_TO,
                    AT_SEND: undefined,
                    AT_UPDATE: ControllerConfig.onCurrentTime().toString(),
                    FLAG_SEND: 2
                  });
                }
                delete TMP_PROSESS[keyProses];
                console.log("END SEND MAIL.... (" + keyProses + ")");
                this.onRunLogProses();
              });
            } else {
              await mailSenderLog.updateById(rawProses.MSM_ID, {
                AT_SEND: undefined,
                AT_UPDATE: ControllerConfig.onCurrentTime().toString(),
                FLAG_SEND: -2
              });
            }
          } else {
            await mailSenderLog.updateById(rawProses.MSM_ID, {
              AT_SEND: undefined,
              AT_UPDATE: ControllerConfig.onCurrentTime().toString(),
              FLAG_SEND: -1
            });
          }
        }
      }
      //END DATA FPJP SOURCE
      break;
    }

  }


}
