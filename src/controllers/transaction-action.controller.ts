// Uncomment these imports to begin using these cool features!
import {authenticate} from '@loopback/authentication';
import {inject} from '@loopback/core';
import {Filter, repository} from '@loopback/repository';
import {get, getModelSchemaRef, param, ResponseObject} from '@loopback/rest';
import {SecurityBindings, securityId, UserProfile} from '@loopback/security';
import {DataDigipos} from '../models';
import {AdMasterRepository, DataDigiposDetailRepository, DataDigiposRejectRepository, DataDigiposRepository, TransaksiTypeDetailRepository, TransaksiTypeRepository} from '../repositories';
// import {inject} from '@loopback/core';


export const RequestGenerateDocBAR: ResponseObject = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['DIGIPOS_ID'],
        properties: {
          DIGIPOS_ID: {
            type: 'array',
            items: {type: 'number'},
          }
        }
      }
    },
  },
};


var PROCESS_GENERATE_BAR: any; PROCESS_GENERATE_BAR = [];

@authenticate('jwt')
export class TransactionActionController {
  constructor(
    @repository(DataDigiposRepository)
    public dataDigiposRepository: DataDigiposRepository,
    @repository(DataDigiposDetailRepository)
    public dataDigiposDetailRepository: DataDigiposDetailRepository,
    @repository(DataDigiposRejectRepository)
    public dataDigiposRejectRepository: DataDigiposRejectRepository,
    @repository(AdMasterRepository)
    public adMasterRepository: AdMasterRepository,
    @repository(TransaksiTypeRepository)
    public transaksiTypeRepository: TransaksiTypeRepository,
    @repository(TransaksiTypeDetailRepository)
    public transaksiTypeDetailRepository: TransaksiTypeDetailRepository,
    @inject(SecurityBindings.USER)
    public currentUserProfile: UserProfile,
  ) { }


  @get('/transaksi/digipos/views', {
    responses: {
      '200': {
        description: 'Array of transaksi type model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(DataDigipos, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async findTransaksiType(
    // @param.path.string('periode') periode: string,
    @param.filter(DataDigipos) filter?: Filter<DataDigipos>,
  ): Promise<Object> {
    var userId = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: "", data: []};

    // var filterSet: any; filterSet = {}
    // var filters: any; filters = filter;
    // for (var i in filters) {
    //   filterSet[i] = filters[i];
    // }

    // if (filterSet.where != undefined) {
    //   var thisWhere = filterSet.where;
    //   filterSet.where = {and: [{trx_date_group: periode}, thisWhere]};
    // } else {
    //   filterSet.where = {and: [{trx_date_group: periode}]}
    // }

    var DATA_TRANSAKSI_TYPE = await this.transaksiTypeRepository.find({where: {and: [{TT_FLAG: 1}]}});
    var TRANSAKSI_TYPE: any; TRANSAKSI_TYPE = {};
    for (var ITYPE in DATA_TRANSAKSI_TYPE) {
      var RAW_TRANSAKSI_TYPE = DATA_TRANSAKSI_TYPE[ITYPE];
      var ID = RAW_TRANSAKSI_TYPE.TT_ID == undefined ? "-" : RAW_TRANSAKSI_TYPE.TT_ID;
      if (TRANSAKSI_TYPE[ID] == undefined) {
        TRANSAKSI_TYPE[ID] = {};
      }
      TRANSAKSI_TYPE[ID] = RAW_TRANSAKSI_TYPE;
    }

    var DATA_DIGIPOS = await this.dataDigiposRepository.find(filter);
    var RESULT_DATA: any; RESULT_DATA = {};

    var TRANSAKSI_A: any; TRANSAKSI_A = {};
    var TRANSAKSI_B: any; TRANSAKSI_B = {};
    var TRANSAKSI_TOTAL: any; TRANSAKSI_TOTAL = {
      TRANSAKSI_A: {},
      TRANSAKSI_B: {}
    };

    for (var IDIGIPOS in DATA_DIGIPOS) {
      var RAW_DIGIPOS: any; RAW_DIGIPOS = DATA_DIGIPOS[IDIGIPOS];

      var REASON_TOLAK_BA = await this.dataDigiposRejectRepository.find({order: ["reject_id DESC"], where: {and: [{digipos_id: RAW_DIGIPOS.digipost_id}, {reject_code: {like: '0002-%'}}]}}); //await this.dataDigiposRejectRepository.find({order: ["reject_id DESC"], limit: 1, where: {and: [{digipos_id: data.digipost_id}, {reject_code: {like: '0002-%'}}]}});
      var REASON_TOLAK_BU = await this.dataDigiposRejectRepository.find({order: ["reject_id DESC"], where: {and: [{digipos_id: RAW_DIGIPOS.digipost_id}, {reject_code: {like: '0003-%'}}]}}); //infoDigipos.digipost_id; //await this.dataDigiposRejectRepository.find({order: ["reject_id DESC"], limit: 1, where: {and: [{digipos_id: data.digipos_id}, {reject_code: {like: '0003-%'}}]}});
      var REASON_TOLAK_SIGN = await this.dataDigiposRejectRepository.find({order: ["reject_id DESC"], where: {and: [{digipos_id: RAW_DIGIPOS.digipost_id}, {reject_code: {like: '0006-%'}}]}}); //infoDigipos.digipost_id; //await this.dataDigiposRejectRepository.find({order: ["reject_id DESC"], limit: 1, where: {and: [{digipos_id: data.digipos_id}, {reject_code: {like: '0006-%'}}]}});
      var REASON_TOLAK_DISPUTE = await this.dataDigiposRejectRepository.find({order: ["reject_id DESC"], where: {and: [{digipos_id: RAW_DIGIPOS.digipost_id}, {reject_code: {like: '0007-%'}}]}}); //infoDigipos.digipost_id; //await this.dataDigiposRejectRepository.find({order: ["reject_id DESC"], limit: 1, where: {and: [{digipos_id: data.digipos_id}, {reject_code: {like: '0007-%'}}]}});
      var REASON_TOLAK_INVOICE = await this.dataDigiposRejectRepository.find({order: ["reject_id DESC"], where: {and: [{digipos_id: RAW_DIGIPOS.digipost_id}, {reject_code: {like: '0012-%'}}]}}); //infoDigipos.digipost_id; //await this.dataDigiposRejectRepository.find({order: ["reject_id DESC"], limit: 1, where: {and: [{digipos_id: data.digipos_id}, {reject_code: {like: '0007-%'}}]}});
      var REASON_DISTRIBUSI_INVOICE: any; REASON_DISTRIBUSI_INVOICE = [];
      var REASON_DISTRIBUSI_BAR: any; REASON_DISTRIBUSI_BAR = [];
      var CONTRACT_NUMBER = "";
      var DEALER_INFORMATION: any; DEALER_INFORMATION = {};

      var INFO_DEALER = await this.adMasterRepository.findById(RAW_DIGIPOS.ad_code);
      if (INFO_DEALER.contract_number != "" && INFO_DEALER.contract_number != null && INFO_DEALER.contract_number != undefined) {
        CONTRACT_NUMBER = INFO_DEALER.contract_number;
      }

      if (INFO_DEALER != null) {
        DEALER_INFORMATION["CODE_AD"] = INFO_DEALER.ad_code;
        DEALER_INFORMATION["GLOBAL_NAME"] = INFO_DEALER.global_name;
        DEALER_INFORMATION["LEGAL_NAME"] = INFO_DEALER.legal_name;
        DEALER_INFORMATION["ALIAS_NAME"] = INFO_DEALER.alias_name;
        DEALER_INFORMATION["CITY"] = INFO_DEALER.city;
        DEALER_INFORMATION["ALAMAT"] = INFO_DEALER.address;
        DEALER_INFORMATION["ATTENTION"] = INFO_DEALER.attention;
        DEALER_INFORMATION["ATTENTION_POSITION"] = INFO_DEALER.attention_position;
        DEALER_INFORMATION["EMAIL"] = INFO_DEALER.email;
        DEALER_INFORMATION["NPWP"] = INFO_DEALER.npwp;
        DEALER_INFORMATION["NPWP_ALAMAT"] = INFO_DEALER.npwp_address;
      }



      var DETAIL_DIGIPOS = await this.dataDigiposDetailRepository.find({where: {and: [{digipost_id: RAW_DIGIPOS.digipost_id}]}});
      for (var IDETDIGIPOS in DETAIL_DIGIPOS) {
        var RAW_DETDIGIPOS = DETAIL_DIGIPOS[IDETDIGIPOS];
        if (TRANSAKSI_TYPE[RAW_DETDIGIPOS.type_trx] == undefined) {
          continue;
        }
        var TYPE = TRANSAKSI_TYPE[RAW_DETDIGIPOS.type_trx];
        var DIGIPOS_ID = RAW_DETDIGIPOS.digipost_id == undefined ? "-" : RAW_DETDIGIPOS.digipost_id;


        if (TYPE.TT_AD_A_HAS == 1) {
          if (TRANSAKSI_TOTAL["TRANSAKSI_A"][0] == undefined) {
            TRANSAKSI_TOTAL["TRANSAKSI_A"][0] = {
              "TYPE_ID": 0,
              "TYPE": "TOTAL",
              "TOTAL_TRX_MDR": 0,
              "TOTAL_AMOUNT_MDR": 0,
              "TOTAL_DPP_MDR": 0,
              "TOTAL_PPN_MDR": 0,
              "TOTAL_PPH23_MDR": 0,
              "TOTAL_TOTAL_MDR": 0,
              "BOBOT": 100
            }
          }

          if (TRANSAKSI_TOTAL["TRANSAKSI_A"][TYPE.TT_ID] == undefined) {
            TRANSAKSI_TOTAL["TRANSAKSI_A"][TYPE.TT_ID] = {
              "TYPE_ID": TYPE.TT_ID,
              "TYPE": TYPE.TT_LABEL,
              "TOTAL_TRX_MDR": 0,
              "TOTAL_AMOUNT_MDR": 0,
              "TOTAL_DPP_MDR": 0,
              "TOTAL_PPN_MDR": 0,
              "TOTAL_PPH23_MDR": 0,
              "TOTAL_TOTAL_MDR": 0,
              "BOBOT": TYPE.TT_BOBOT_A
            }
          }

          if (TRANSAKSI_A[DIGIPOS_ID] == undefined) {
            TRANSAKSI_A[DIGIPOS_ID] = {};
            var EXCLUDE_FIELD_CODE: any; EXCLUDE_FIELD_CODE = ["sales_fee", "trx_cluster_out", "mdr_fee_amount", "dpp_fee_amount", "vat_fee_amount", "total_fee_amount", "pph_fee_amount", "trx_paid_invoice", "paid_inv_amount", "inv_adj_amount", "sales_paid", "dpp_paid_amount", "vat_paid_amount", "total_paid_amount", "pph_paid_amount", "oracle_id", "interface_status", "error_message"];
            for (var KEY_DIGIPOS in RAW_DIGIPOS) {
              if (EXCLUDE_FIELD_CODE.indexOf(KEY_DIGIPOS) == -1) {
                TRANSAKSI_A[DIGIPOS_ID][KEY_DIGIPOS] = RAW_DIGIPOS[KEY_DIGIPOS];
              }
            }
            TRANSAKSI_A[DIGIPOS_ID]["REASON_TOLAK_BA"] = REASON_TOLAK_BA;
            TRANSAKSI_A[DIGIPOS_ID]["REASON_TOLAK_BU"] = REASON_TOLAK_BU;
            TRANSAKSI_A[DIGIPOS_ID]["REASON_TOLAK_SIGN"] = REASON_TOLAK_SIGN;
            TRANSAKSI_A[DIGIPOS_ID]["REASON_TOLAK_DISPUTE"] = REASON_TOLAK_DISPUTE;
            TRANSAKSI_A[DIGIPOS_ID]["REASON_TOLAK_INVOICE"] = REASON_TOLAK_INVOICE;
            TRANSAKSI_A[DIGIPOS_ID]["REASON_DISTRIBUSI_INVOICE"] = REASON_DISTRIBUSI_INVOICE;
            TRANSAKSI_A[DIGIPOS_ID]["CONTRACT_NUMBER"] = CONTRACT_NUMBER;
            TRANSAKSI_A[DIGIPOS_ID]["DEALER_INFORMATION"] = DEALER_INFORMATION;


            TRANSAKSI_A[DIGIPOS_ID]["DETAIL_TRANSAKSI"] = [];
            TRANSAKSI_A[DIGIPOS_ID]["DETAIL_TRANSAKSI"].push({
              "TYPE_ID": 0,
              "TYPE": "TOTAL",
              "SALES_MDR": RAW_DIGIPOS.sales_fee,
              "TRX_MDR": RAW_DIGIPOS.trx_cluster_out,
              "AMOUNT_MDR": RAW_DIGIPOS.mdr_fee_amount,
              "DPP_MDR": RAW_DIGIPOS.dpp_fee_amount,
              "PPN_MDR": RAW_DIGIPOS.vat_fee_amount,
              "PPH23_MDR": RAW_DIGIPOS.pph_fee_amount,
              "TOTAL_MDR": RAW_DIGIPOS.total_fee_amount
            });

            TRANSAKSI_TOTAL["TRANSAKSI_A"][0]["TOTAL_TRX_MDR"] += Number(RAW_DIGIPOS.trx_cluster_out);
            TRANSAKSI_TOTAL["TRANSAKSI_A"][0]["TOTAL_AMOUNT_MDR"] += Number(RAW_DIGIPOS.mdr_fee_amount);
            TRANSAKSI_TOTAL["TRANSAKSI_A"][0]["TOTAL_DPP_MDR"] += Number(RAW_DIGIPOS.dpp_fee_amount);
            TRANSAKSI_TOTAL["TRANSAKSI_A"][0]["TOTAL_PPN_MDR"] += Number(RAW_DIGIPOS.vat_fee_amount);
            TRANSAKSI_TOTAL["TRANSAKSI_A"][0]["TOTAL_PPH23_MDR"] += Number(RAW_DIGIPOS.pph_fee_amount);
            TRANSAKSI_TOTAL["TRANSAKSI_A"][0]["TOTAL_TOTAL_MDR"] += Number(RAW_DIGIPOS.total_fee_amount);

          }
          TRANSAKSI_A[DIGIPOS_ID]["DETAIL_TRANSAKSI"].push({
            "TYPE_ID": TYPE.TT_ID,
            "TYPE": TYPE.TT_LABEL,
            "SALES_MDR": RAW_DETDIGIPOS.sales_fee,
            "TRX_MDR": RAW_DETDIGIPOS.trx_cluster_out,
            "AMOUNT_MDR": RAW_DETDIGIPOS.mdr_fee_amount,
            "DPP_MDR": RAW_DETDIGIPOS.dpp_fee_amount,
            "PPN_MDR": RAW_DETDIGIPOS.vat_fee_amount,
            "PPH23_MDR": RAW_DETDIGIPOS.pph_fee_amount,
            "TOTAL_MDR": RAW_DETDIGIPOS.total_fee_amount
          });

          TRANSAKSI_TOTAL["TRANSAKSI_A"][TYPE.TT_ID]["TOTAL_TRX_MDR"] += Number(RAW_DETDIGIPOS.trx_cluster_out);
          TRANSAKSI_TOTAL["TRANSAKSI_A"][TYPE.TT_ID]["TOTAL_AMOUNT_MDR"] += Number(RAW_DETDIGIPOS.mdr_fee_amount);
          TRANSAKSI_TOTAL["TRANSAKSI_A"][TYPE.TT_ID]["TOTAL_DPP_MDR"] += Number(RAW_DETDIGIPOS.dpp_fee_amount);
          TRANSAKSI_TOTAL["TRANSAKSI_A"][TYPE.TT_ID]["TOTAL_PPN_MDR"] += Number(RAW_DETDIGIPOS.vat_fee_amount);
          TRANSAKSI_TOTAL["TRANSAKSI_A"][TYPE.TT_ID]["TOTAL_PPH23_MDR"] += Number(RAW_DETDIGIPOS.pph_fee_amount);
          TRANSAKSI_TOTAL["TRANSAKSI_A"][TYPE.TT_ID]["TOTAL_TOTAL_MDR"] += Number(RAW_DETDIGIPOS.total_fee_amount);


        }



        if (TYPE.TT_AD_B_HAS == 1) {
          if (TRANSAKSI_TOTAL["TRANSAKSI_B"][0] == undefined) {
            TRANSAKSI_TOTAL["TRANSAKSI_B"][0] = {
              "TYPE_ID": 0,
              "TYPE": "TOTAL",
              "TOTAL_TRX_MDR": 0,
              "TOTAL_AMOUNT_MDR": 0,
              "TOTAL_DPP_MDR": 0,
              "TOTAL_PPN_MDR": 0,
              "TOTAL_PPH23_MDR": 0,
              "TOTAL_TOTAL_MDR": 0,
              "BOBOT": 100
            }
          }
          if (TRANSAKSI_TOTAL["TRANSAKSI_B"][TYPE.TT_ID] == undefined) {
            TRANSAKSI_TOTAL["TRANSAKSI_B"][TYPE.TT_ID] = {
              "TYPE_ID": TYPE.TT_ID,
              "TYPE": TYPE.TT_LABEL,
              "TOTAL_TRX_MDR": 0,
              "TOTAL_AMOUNT_MDR": 0,
              "TOTAL_DPP_MDR": 0,
              "TOTAL_PPN_MDR": 0,
              "TOTAL_PPH23_MDR": 0,
              "TOTAL_TOTAL_MDR": 0,
              "BOBOT": TYPE.TT_BOBOT_B
            }
          }

          if (TRANSAKSI_B[DIGIPOS_ID] == undefined) {
            TRANSAKSI_B[DIGIPOS_ID] = {};
            var EXCLUDE_FIELD_CODE: any; EXCLUDE_FIELD_CODE = ["sales_fee", "trx_cluster_out", "mdr_fee_amount", "dpp_fee_amount", "vat_fee_amount", "total_fee_amount", "pph_fee_amount", "trx_paid_invoice", "paid_inv_amount", "inv_adj_amount", "sales_paid", "dpp_paid_amount", "vat_paid_amount", "total_paid_amount", "pph_paid_amount", "oracle_id", "interface_status", "error_message"];
            for (var KEY_DIGIPOS in RAW_DIGIPOS) {
              if (EXCLUDE_FIELD_CODE.indexOf(KEY_DIGIPOS) == -1) {
                TRANSAKSI_B[DIGIPOS_ID][KEY_DIGIPOS] = RAW_DIGIPOS[KEY_DIGIPOS];
              }
            }
            TRANSAKSI_B[DIGIPOS_ID]["REASON_TOLAK_BA"] = REASON_TOLAK_BA;
            TRANSAKSI_B[DIGIPOS_ID]["REASON_TOLAK_BU"] = REASON_TOLAK_BU;
            TRANSAKSI_B[DIGIPOS_ID]["REASON_TOLAK_SIGN"] = REASON_TOLAK_SIGN;
            TRANSAKSI_B[DIGIPOS_ID]["REASON_TOLAK_DISPUTE"] = REASON_TOLAK_DISPUTE;
            TRANSAKSI_B[DIGIPOS_ID]["REASON_TOLAK_INVOICE"] = REASON_TOLAK_INVOICE;
            TRANSAKSI_B[DIGIPOS_ID]["REASON_DISTRIBUSI_INVOICE"] = REASON_DISTRIBUSI_INVOICE;
            TRANSAKSI_B[DIGIPOS_ID]["CONTRACT_NUMBER"] = CONTRACT_NUMBER;
            TRANSAKSI_B[DIGIPOS_ID]["DEALER_INFORMATION"] = DEALER_INFORMATION;

            TRANSAKSI_B[DIGIPOS_ID]["DETAIL_TRANSAKSI"] = [];
            TRANSAKSI_B[DIGIPOS_ID]["DETAIL_TRANSAKSI"].push({
              "TYPE_ID": 0,
              "TYPE": "TOTAL",
              "SALES_MDR": RAW_DIGIPOS.sales_paid,
              "TRX_MDR": RAW_DIGIPOS.trx_paid_invoice,
              "AMOUNT_MDR": RAW_DIGIPOS.paid_inv_amount,
              "DPP_MDR": RAW_DIGIPOS.dpp_paid_amount,
              "PPN_MDR": RAW_DIGIPOS.vat_paid_amount,
              "PPH23_MDR": RAW_DIGIPOS.pph_paid_amount,
              "TOTAL_MDR": RAW_DIGIPOS.total_paid_amount
            });

            TRANSAKSI_TOTAL["TRANSAKSI_B"][0]["TOTAL_TRX_MDR"] += Number(RAW_DIGIPOS.trx_paid_invoice);
            TRANSAKSI_TOTAL["TRANSAKSI_B"][0]["TOTAL_AMOUNT_MDR"] += Number(RAW_DIGIPOS.paid_inv_amount);
            TRANSAKSI_TOTAL["TRANSAKSI_B"][0]["TOTAL_DPP_MDR"] += Number(RAW_DIGIPOS.dpp_paid_amount);
            TRANSAKSI_TOTAL["TRANSAKSI_B"][0]["TOTAL_PPN_MDR"] += Number(RAW_DIGIPOS.vat_paid_amount);
            TRANSAKSI_TOTAL["TRANSAKSI_B"][0]["TOTAL_PPH23_MDR"] += Number(RAW_DIGIPOS.pph_paid_amount);
            TRANSAKSI_TOTAL["TRANSAKSI_B"][0]["TOTAL_TOTAL_MDR"] += Number(RAW_DIGIPOS.total_paid_amount);
          }

          TRANSAKSI_B[DIGIPOS_ID]["DETAIL_TRANSAKSI"].push({
            "TYPE_ID": TYPE.TT_ID,
            "TYPE": TYPE.TT_LABEL,
            "SALES_MDR": RAW_DETDIGIPOS.sales_paid,
            "TRX_MDR": RAW_DETDIGIPOS.trx_paid_invoice,
            "AMOUNT_MDR": RAW_DETDIGIPOS.paid_inv_amount,
            "DPP_MDR": RAW_DETDIGIPOS.dpp_paid_amount,
            "PPN_MDR": RAW_DETDIGIPOS.vat_paid_amount,
            "PPH23_MDR": RAW_DETDIGIPOS.pph_paid_amount,
            "TOTAL_MDR": RAW_DETDIGIPOS.total_paid_amount
          });

          TRANSAKSI_TOTAL["TRANSAKSI_B"][TYPE.TT_ID]["TOTAL_TRX_MDR"] += Number(RAW_DETDIGIPOS.trx_paid_invoice);
          TRANSAKSI_TOTAL["TRANSAKSI_B"][TYPE.TT_ID]["TOTAL_AMOUNT_MDR"] += Number(RAW_DETDIGIPOS.paid_inv_amount);
          TRANSAKSI_TOTAL["TRANSAKSI_B"][TYPE.TT_ID]["TOTAL_DPP_MDR"] += Number(RAW_DETDIGIPOS.dpp_paid_amount);
          TRANSAKSI_TOTAL["TRANSAKSI_B"][TYPE.TT_ID]["TOTAL_PPN_MDR"] += Number(RAW_DETDIGIPOS.vat_paid_amount);
          TRANSAKSI_TOTAL["TRANSAKSI_B"][TYPE.TT_ID]["TOTAL_PPH23_MDR"] += Number(RAW_DETDIGIPOS.pph_paid_amount);
          TRANSAKSI_TOTAL["TRANSAKSI_B"][TYPE.TT_ID]["TOTAL_TOTAL_MDR"] += Number(RAW_DETDIGIPOS.total_paid_amount);

        }
      }
    }


    RESULT_DATA["TRANSAKSI_A"] = [];
    for (var ITRXA in TRANSAKSI_A) {
      RESULT_DATA["TRANSAKSI_A"].push(TRANSAKSI_A[ITRXA]);
    }
    RESULT_DATA["TRANSAKSI_B"] = [];
    for (var ITRXB in TRANSAKSI_B) {
      RESULT_DATA["TRANSAKSI_B"].push(TRANSAKSI_B[ITRXB]);
    }
    RESULT_DATA["TRANSAKSI_TOTAL"] = TRANSAKSI_TOTAL;

    result.success = true;
    result.message = "success !";
    result.data = RESULT_DATA;

    return result;
  }


  // @post('/transaksi/digipos/generate-document/bar/transaksi-a', {
  //   responses: {
  //     '200': {
  //       description: 'DataDigipos model instance',
  //       content: {
  //         'application/json': {
  //           schema: RequestGenerateDocBAR
  //         }
  //       },
  //     },
  //   },
  // })
  // async GenerateDocumentBAR(
  //   @requestBody(RequestGenerateDocBAR) requestGenerateDocBAR: {DIGIPOS_ID: []},
  // ): Promise<Object> {
  //   var userId = this.currentUserProfile[securityId];
  //   var result: any; result = {success: true, message: "", data: []};

  //   var DIGIPOS = await this.dataDigiposRepository.find({where: {and: [{ba_number_a: {nin: [undefined, ""]}}, {digipost_id: {inq: requestGenerateDocBAR.DIGIPOS_ID}}, {ar_stat: 2}]}});

  //   if (DIGIPOS.length > 0) {
  //     for (var i in DIGIPOS) {
  //       var RAW_DIGIPOS = DIGIPOS[i];

  //       var NEW_PROCESS = 0;
  //       if (PROCESS_GENERATE_BAR.indexOf(RAW_DIGIPOS.digipost_id) == -1) {
  //         PROCESS_GENERATE_BAR.push(RAW_DIGIPOS.digipost_id);
  //         NEW_PROCESS = 1;
  //       }
  //       if (NEW_PROCESS == 1) {

  //       } else {
  //         continue;
  //       }
  //     }
  //   } else {
  //     result.success = false;
  //     result.message = "Error, not found transaksi avaliable for generate !";
  //   }

  //   return result;
  // }



}
