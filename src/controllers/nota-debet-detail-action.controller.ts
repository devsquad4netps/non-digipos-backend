import {authenticate} from '@loopback/authentication';
import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where
} from '@loopback/repository';
import {
  del, get,
  getModelSchemaRef, param,


  patch, post,




  put,

  requestBody
} from '@loopback/rest';
import {NotaDebetDetail} from '../models';
import {NotaDebetDetailRepository} from '../repositories';


@authenticate('jwt')
export class NotaDebetDetailActionController {
  constructor(
    @repository(NotaDebetDetailRepository)
    public notaDebetDetailRepository: NotaDebetDetailRepository,
  ) { }

  @post('/nota-debet-details', {
    responses: {
      '200': {
        description: 'NotaDebetDetail model instance',
        content: {'application/json': {schema: getModelSchemaRef(NotaDebetDetail)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(NotaDebetDetail, {
            title: 'NewNotaDebetDetail',
            exclude: ['nota_debet_detail_id'],
          }),
        },
      },
    })
    notaDebetDetail: Omit<NotaDebetDetail, 'nota_debet_detail_id'>,
  ): Promise<NotaDebetDetail> {
    return this.notaDebetDetailRepository.create(notaDebetDetail);
  }

  @get('/nota-debet-details/count', {
    responses: {
      '200': {
        description: 'NotaDebetDetail model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(NotaDebetDetail) where?: Where<NotaDebetDetail>,
  ): Promise<Count> {
    return this.notaDebetDetailRepository.count(where);
  }

  @get('/nota-debet-details', {
    responses: {
      '200': {
        description: 'Array of NotaDebetDetail model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(NotaDebetDetail, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(NotaDebetDetail) filter?: Filter<NotaDebetDetail>,
  ): Promise<NotaDebetDetail[]> {
    return this.notaDebetDetailRepository.find(filter);
  }

  @patch('/nota-debet-details', {
    responses: {
      '200': {
        description: 'NotaDebetDetail PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(NotaDebetDetail, {partial: true}),
        },
      },
    })
    notaDebetDetail: NotaDebetDetail,
    @param.where(NotaDebetDetail) where?: Where<NotaDebetDetail>,
  ): Promise<Count> {
    return this.notaDebetDetailRepository.updateAll(notaDebetDetail, where);
  }

  @get('/nota-debet-details/{id}', {
    responses: {
      '200': {
        description: 'NotaDebetDetail model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(NotaDebetDetail, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(NotaDebetDetail, {exclude: 'where'}) filter?: FilterExcludingWhere<NotaDebetDetail>
  ): Promise<NotaDebetDetail> {
    return this.notaDebetDetailRepository.findById(id, filter);
  }

  @patch('/nota-debet-details/{id}', {
    responses: {
      '204': {
        description: 'NotaDebetDetail PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(NotaDebetDetail, {partial: true}),
        },
      },
    })
    notaDebetDetail: NotaDebetDetail,
  ): Promise<void> {
    await this.notaDebetDetailRepository.updateById(id, notaDebetDetail);
  }

  @put('/nota-debet-details/{id}', {
    responses: {
      '204': {
        description: 'NotaDebetDetail PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() notaDebetDetail: NotaDebetDetail,
  ): Promise<void> {
    await this.notaDebetDetailRepository.replaceById(id, notaDebetDetail);
  }

  @del('/nota-debet-details/{id}', {
    responses: {
      '204': {
        description: 'NotaDebetDetail DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.notaDebetDetailRepository.deleteById(id);
  }
}
