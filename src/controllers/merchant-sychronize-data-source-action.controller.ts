import {BigQuery, BigQueryDate} from '@google-cloud/bigquery';
import {authenticate} from '@loopback/authentication';
import {inject} from '@loopback/core';
import {Filter, repository} from '@loopback/repository';
import {get, getModelSchemaRef, param, post, requestBody, ResponseObject} from '@loopback/rest';
import {SecurityBindings, securityId, UserProfile} from '@loopback/security';
import {MerchantProductDatastore, SycronizeProcessLog} from '../models';
import {MerchantProductConditionRepository, MerchantProductDatastoreRepository, MerchantProductGroupRepository, MerchantProductMasterRepository, MerchantProductQueryRepository, MerchantProductSummaryRepository, MerchantProductVariableRepository, MpvTypeKeyRepository, MpvTypeRepository, SycronizeProcessLogRepository} from '../repositories';
import {MerchantProductRepository} from '../repositories/merchant-product.repository';
import {MerchantRepository} from '../repositories/merchant.repository';
import {UserRepository} from '../repositories/user.repository';
import {ControllerConfig} from './controller.config';

var fs = require('fs')
var createHTML = require('create-html');


var TRANSACTION_TMP: any; TRANSACTION_TMP = {};
var PROCESS_SYNC: any; PROCESS_SYNC = {};
var PROCESS_SYNC_DATA: any; PROCESS_SYNC_DATA = {};

export const CreateSyncGroupRequestBody: ResponseObject = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ["MPG_ID", "PERIODE"],
        properties: {
          MPG_ID: {
            type: 'array',
            items: {type: 'number'}
          },
          PERIODE: {
            type: 'string',
          },
        }
      }
    },
  },
};

export const CreateSyncRunningLogBody: ResponseObject = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ["SPL_ID"],
        properties: {
          SPL_ID: {
            type: 'array',
            items: {type: 'number'}
          },
        }
      }
    },
  },
};



@authenticate('jwt')
export class MerchantSychronizeDataSourceActionController {
  constructor(
    @repository(MerchantRepository)
    public merchantRepository: MerchantRepository,
    @repository(MerchantProductRepository)
    public merchantProductRepository: MerchantProductRepository,
    @repository(MerchantProductGroupRepository)
    public merchantProductGroupRepository: MerchantProductGroupRepository,
    @repository(MerchantProductVariableRepository)
    public merchantProductVariableRepository: MerchantProductVariableRepository,
    @repository(MerchantProductDatastoreRepository)
    public merchantProductDatastoreRepository: MerchantProductDatastoreRepository,
    @repository(MerchantProductSummaryRepository)
    public merchantProductSummaryRepository: MerchantProductSummaryRepository,
    @repository(MpvTypeRepository)
    public mpvTypeRepository: MpvTypeRepository,
    @repository(MerchantProductMasterRepository)
    public merchantProductMasterRepository: MerchantProductMasterRepository,
    @repository(MpvTypeKeyRepository)
    public mpvTypeKeyRepository: MpvTypeKeyRepository,
    @repository(SycronizeProcessLogRepository)
    public sycronizeProcessLogRepository: SycronizeProcessLogRepository,
    @repository(MerchantProductQueryRepository)
    public merchantProductQueryRepository: MerchantProductQueryRepository,
    @repository(MerchantProductConditionRepository)
    public merchantProductConditionRepository: MerchantProductConditionRepository,
    @inject(SecurityBindings.USER)
    public currentUserProfile: UserProfile,
    @repository(UserRepository)
    public userRepository: UserRepository,
  ) { }



  ON_PROCESS_CREATE = async (PROCESS_KEY: any) => {
    var USER_ID = this.currentUserProfile[securityId];
    for (var I_KEY_CREATE in PROCESS_SYNC_DATA[PROCESS_KEY]) {
      var RAW_DATA = PROCESS_SYNC_DATA[PROCESS_KEY][I_KEY_CREATE];

      var MP_ID = RAW_DATA.MP_ID;
      var M_ID = RAW_DATA.M_ID;
      var MPG_ID = RAW_DATA.MPG_ID;
      var SPL_ID = RAW_DATA.SPL_ID;
      var PERIODE = RAW_DATA.SPL_PERIODE;

      if (MPG_ID == null) {
        await this.sycronizeProcessLogRepository.updateById(SPL_ID, {
          SPL_MDR: 0,
          SPL_COUNT_TRX: 0,
          SPL_TRX_AMOUNT: 0,
          SPL_PROCESS_COUNT: 100,
          SPL_PROCESS_NOW: 100,
          AT_FLAG: 3,
          SPL_MESSAGE: "Error, please select merchant product !"
        });

        delete PROCESS_SYNC_DATA[PROCESS_KEY][I_KEY_CREATE];
        this.ON_PROCESS_CREATE(PROCESS_KEY);
        break;
      };

      if (PERIODE == null) {
        await this.sycronizeProcessLogRepository.updateById(SPL_ID, {
          SPL_MDR: 0,
          SPL_COUNT_TRX: 0,
          SPL_TRX_AMOUNT: 0,
          SPL_PROCESS_COUNT: 100,
          SPL_PROCESS_NOW: 100,
          AT_FLAG: 3,
          SPL_MESSAGE: "Error, please select periode !"
        });

        delete PROCESS_SYNC_DATA[PROCESS_KEY][I_KEY_CREATE];
        this.ON_PROCESS_CREATE(PROCESS_KEY); break;
      };

      var MASTER_AVA = await this.merchantProductMasterRepository.find({where: {and: [{PERIODE: PERIODE}, {M_ID: M_ID}, {MP_ID: MP_ID}, {AT_FLAG: {nin: [0, 1]}}, {AT_POSISI: {nin: [0, 1]}}]}});
      if (MASTER_AVA.length > 0) {

        await this.sycronizeProcessLogRepository.updateById(SPL_ID, {
          SPL_MDR: 0,
          SPL_COUNT_TRX: 0,
          SPL_TRX_AMOUNT: 0,
          SPL_PROCESS_COUNT: 100,
          SPL_PROCESS_NOW: 100,
          AT_FLAG: 3,
          SPL_MESSAGE: "Error, please reject transaksi before update transaksi !"
        });

        delete PROCESS_SYNC_DATA[PROCESS_KEY][I_KEY_CREATE];
        this.ON_PROCESS_CREATE(PROCESS_KEY); break;
        break;
      }


      var MERCHANT_GROUP_COUNT = await this.merchantProductGroupRepository.count({MPG_ID: MPG_ID});
      if (MERCHANT_GROUP_COUNT.count > 0) {

        var MERCHANT_GROUP = await this.merchantProductGroupRepository.findById(MPG_ID);
        var MDR_TYPE = MERCHANT_GROUP.MPG_MDR_TYPE;//1 : PERCENT - 2 : AMOUNT
        var MDR_AMOUNT = MERCHANT_GROUP.MPG_MDR_AMOUNT;
        var MDR_DPP = MERCHANT_GROUP.MPG_DPP;
        var MDR_PPN = MERCHANT_GROUP.MPG_PPN;
        var MDR_PPH = MERCHANT_GROUP.MPG_PPH;

        var VARIABLE_COUNT = await this.merchantProductVariableRepository.count({MP_ID: MP_ID});
        var MERCHANT_QUERY = await this.merchantProductQueryRepository.findOne({where: {and: [{MP_ID: MP_ID}]}});
        var MERCHANT_CONDITION = await this.merchantProductConditionRepository.find({where: {and: [{AT_FLAG: 1}, {MPG_ID: MPG_ID}]}});

        var LOGIC_CON: any; LOGIC_CON = {};
        for (var MC_I in MERCHANT_CONDITION) {
          var RAW_MC = MERCHANT_CONDITION[MC_I];
          if (LOGIC_CON[RAW_MC.MPC_GROUP] == undefined) {
            LOGIC_CON[RAW_MC.MPC_GROUP] = [];
          }
          LOGIC_CON[RAW_MC.MPC_GROUP].push(RAW_MC);
        }

        if (MERCHANT_QUERY != null && MERCHANT_QUERY.AT_FLAG == 1 && MERCHANT_QUERY.MPQ_QUERY != undefined) {

          var FIELD_REQ: any; FIELD_REQ = {};
          var PERIODE_FILTER = "";

          var REQUIRED_FIELDS: any; REQUIRED_FIELDS = {1: 0, 2: 0, 3: 0, 4: 0, 6: 0};


          var FIELD_DATASTORE: any; FIELD_DATASTORE = {}
          if (VARIABLE_COUNT.count > 0) {
            var VARIABLE = await this.merchantProductVariableRepository.find({where: {and: [{MP_ID: MP_ID}]}});
            for (var I_VAR in VARIABLE) {
              var INFO_VARIABLE = VARIABLE[I_VAR];
              var VARIABLE_ID = INFO_VARIABLE.MPV_ID == undefined ? -1 : INFO_VARIABLE.MPV_ID;
              if (FIELD_REQ[VARIABLE_ID] == undefined) {
                FIELD_REQ[VARIABLE_ID] = {};
                FIELD_DATASTORE[INFO_VARIABLE.MPV_LABEL] = "";
              }
              FIELD_REQ[VARIABLE_ID] = {KEY_ID: INFO_VARIABLE.MPV_TYPE_KEY_ID, TYPE: INFO_VARIABLE.MPV_TYPE_ID, KEY: INFO_VARIABLE.MPV_KEY, LABEL: INFO_VARIABLE.MPV_LABEL};

              if (REQUIRED_FIELDS[INFO_VARIABLE.MPV_TYPE_KEY_ID] != undefined) {
                REQUIRED_FIELDS[INFO_VARIABLE.MPV_TYPE_KEY_ID] = 1;
              }
              //UNTUK PERIODE
              if (INFO_VARIABLE.MPV_TYPE_KEY_ID == 1) {
                if (PERIODE_FILTER == "") {
                  PERIODE_FILTER = " " + INFO_VARIABLE.MPV_KEY + " BETWEEN  @START_DATE AND @END_DATE ";
                }
              }
            }
          }

          var NEXT_ENTRY = 1;
          for (var I_CEK in REQUIRED_FIELDS) {
            if (REQUIRED_FIELDS[I_CEK] == 0) {
              NEXT_ENTRY = 0;
            }
          }

          if (NEXT_ENTRY == 1) {

            await this.merchantProductDatastoreRepository.deleteAll({and: [{SPL_ID: SPL_ID}]});

            var PERIODE_DATA = PERIODE;
            var PERIODE_SPLIT = PERIODE_DATA.split("-");
            var TRANSAKSI_DATE = new Date(Number(PERIODE_SPLIT[0]), Number(PERIODE_SPLIT[1]), 0);
            var TRANSAKSI_MONTH = TRANSAKSI_DATE.getMonth() + 1;
            var END_DATE_TRANSAKSI = TRANSAKSI_DATE.getFullYear() + "-" + "00".substring(TRANSAKSI_MONTH.toString().length) + TRANSAKSI_MONTH + "-" + TRANSAKSI_DATE.getDate();
            var START_DATE_TRANSAKSI = TRANSAKSI_DATE.getFullYear() + "-" + "00".substring(TRANSAKSI_MONTH.toString().length) + TRANSAKSI_MONTH + "-" + "01";


            const BIG_QUERY = new BigQuery();
            var SQL_QUERY = MERCHANT_QUERY?.MPQ_QUERY.split("$FILTER_PERIODE").join(PERIODE_FILTER).split("$AND").join(" AND ").split("$OR").join(" OR ").split("$WHERE").join(" WHERE ");
            const OPTION_BIG_QUERY = {
              query: SQL_QUERY,
              params: {START_DATE: new BigQueryDate(START_DATE_TRANSAKSI), END_DATE: new BigQueryDate(END_DATE_TRANSAKSI)},
            }
            const [RESULT_BIG_QUERY] = await BIG_QUERY.query(OPTION_BIG_QUERY);

            await this.sycronizeProcessLogRepository.updateById(SPL_ID, {
              SPL_MDR: 0,
              SPL_COUNT_TRX: 0,
              SPL_TRX_AMOUNT: 0,
              SPL_PROCESS_COUNT: RESULT_BIG_QUERY.length,
              SPL_PROCESS_NOW: 0,
              AT_FLAG: 1,
              SPL_MESSAGE: "PROCESS.."
            });


            var START_TIME_RUN = SPL_ID;
            var DATASOURCE: any; DATASOURCE = {};
            var NO = 0;
            var PROCCESS_LOAD_DATA = 0;
            var POSISI = 1;
            var TRANSACTION: any; TRANSACTION = [];

            var CREATE_DATA: any; CREATE_DATA = {};
            //START LOOPING DATA BIG QUERY
            for (var I_RESULT in RESULT_BIG_QUERY) {

              await this.sycronizeProcessLogRepository.updateById(SPL_ID, {
                SPL_MDR: 0,
                SPL_COUNT_TRX: 0,
                SPL_TRX_AMOUNT: 0,
                SPL_PROCESS_COUNT: RESULT_BIG_QUERY.length,
                SPL_PROCESS_NOW: PROCCESS_LOAD_DATA,
                AT_FLAG: 1,
                SPL_MESSAGE: "PROCESS.."
              });
              PROCCESS_LOAD_DATA++;

              var RAW_BIGQUERY = RESULT_BIG_QUERY[I_RESULT];
              for (var ICLEAR_FIELD in FIELD_DATASTORE) {
                FIELD_DATASTORE[ICLEAR_FIELD] = "";
              }

              var DATA_VALUES: any; DATA_VALUES = {};
              var DATA_ID = "";
              // var MDR_VAR = "";
              // var AMOUNT_VAR = "";
              // var COUNT_VAR = "";

              for (var VAR_ID in FIELD_REQ) {
                var RAW_VARIABLES = FIELD_REQ[VAR_ID];
                if (RAW_VARIABLES.KEY_ID == 6) {
                  DATA_ID = RAW_VARIABLES.TYPE == 4 ? RAW_BIGQUERY[RAW_VARIABLES.KEY]["value"] : RAW_BIGQUERY[RAW_VARIABLES.KEY];
                }
              }

              for (var VAR_ID in FIELD_REQ) {
                var RAW_VARIABLES = FIELD_REQ[VAR_ID];

                if (RAW_VARIABLES.KEY_ID == 1) {
                  if (FIELD_DATASTORE[RAW_VARIABLES.LABEL] != undefined) {
                    FIELD_DATASTORE[RAW_VARIABLES.LABEL] = PERIODE;
                  }
                  continue;
                }

                if (FIELD_DATASTORE[RAW_VARIABLES.LABEL] != undefined) {
                  if (RAW_BIGQUERY[RAW_VARIABLES.KEY] != undefined) {
                    var TEXT_DATA: any; TEXT_DATA = "";
                    switch (RAW_VARIABLES.TYPE) {
                      case 1:
                        TEXT_DATA = Number(RAW_BIGQUERY[RAW_VARIABLES.KEY])
                        break;
                      case 2:
                        TEXT_DATA = RAW_BIGQUERY[RAW_VARIABLES.KEY];
                        break;
                      case 3:
                        TEXT_DATA = RAW_BIGQUERY[RAW_VARIABLES.KEY];
                        break;
                      case 4:
                        TEXT_DATA = RAW_BIGQUERY[RAW_VARIABLES.KEY]["value"]
                        break;
                      case 5:
                        TEXT_DATA = RAW_BIGQUERY[RAW_VARIABLES.KEY]["value"]
                        break;
                      case 6:
                        TEXT_DATA = RAW_BIGQUERY[RAW_VARIABLES.KEY]["value"]
                        break;
                      default:
                        TEXT_DATA = JSON.stringify(RAW_BIGQUERY[RAW_VARIABLES.KEY]);
                        break;
                    }

                    FIELD_DATASTORE[RAW_VARIABLES.LABEL] = TEXT_DATA;// RAW_VARIABLES.TYPE == 4 ? RAW_BIGQUERY[RAW_VARIABLES.KEY]["value"] : RAW_BIGQUERY[RAW_VARIABLES.KEY];
                    if (DATA_VALUES[VAR_ID] == undefined) {
                      DATA_VALUES[VAR_ID] = FIELD_DATASTORE[RAW_VARIABLES.LABEL];
                    }
                  } else {
                    FIELD_DATASTORE[RAW_VARIABLES.LABEL] = "";
                    if (DATA_VALUES[VAR_ID] == undefined) {
                      DATA_VALUES[VAR_ID] = null;
                    }
                  }
                }

                if (CREATE_DATA[Number(VAR_ID)] == undefined) {
                  CREATE_DATA[Number(VAR_ID)] = RAW_VARIABLES;
                }
              }

              var AND_STATE: any; AND_STATE = [];
              for (var LOGIC_I in LOGIC_CON) {
                var RAW_LOGIC_AND = LOGIC_CON[LOGIC_I];
                var OR_STATE: any; OR_STATE = [];
                for (var OR_I in RAW_LOGIC_AND) {
                  var RAW_LOGIC_OR = RAW_LOGIC_AND[OR_I];
                  if (DATA_VALUES[RAW_LOGIC_OR.MPV_ID] == undefined) {
                    OR_STATE.push(0);
                    continue;
                  }
                  switch (RAW_LOGIC_OR.MPC_OPERATOR) {
                    case "=":
                      if (DATA_VALUES[RAW_LOGIC_OR.MPV_ID] == RAW_LOGIC_OR.MPC_VALUE) {
                        OR_STATE.push(1);
                      } else {
                        OR_STATE.push(0);
                      }
                      break;
                    case "<=":
                      if (Number(DATA_VALUES[RAW_LOGIC_OR.MPV_ID]) <= Number(RAW_LOGIC_OR.MPC_VALUE)) {
                        OR_STATE.push(1);
                      } else {
                        OR_STATE.push(0);
                      }
                      break;
                    case ">=":
                      if (Number(DATA_VALUES[RAW_LOGIC_OR.MPV_ID]) >= Number(RAW_LOGIC_OR.MPC_VALUE)) {
                        OR_STATE.push(1);
                      } else {
                        OR_STATE.push(0);
                      }
                      break;
                    case "<>":
                      if (DATA_VALUES[RAW_LOGIC_OR.MPV_ID] != RAW_LOGIC_OR.MPC_VALUE) {
                        OR_STATE.push(1);
                      } else {
                        OR_STATE.push(0);
                      }
                      break;
                    case "<":
                      if (Number(DATA_VALUES[RAW_LOGIC_OR.MPV_ID]) < Number(RAW_LOGIC_OR.MPC_VALUE)) {
                        OR_STATE.push(1);
                      } else {
                        OR_STATE.push(0);
                      }
                      break;
                    case ">":
                      if (Number(DATA_VALUES[RAW_LOGIC_OR.MPV_ID]) > Number(RAW_LOGIC_OR.MPC_VALUE)) {
                        OR_STATE.push(1);
                      } else {
                        OR_STATE.push(0);
                      }
                      break;
                    case "LIKE":
                      if (RAW_LOGIC_OR.MPC_VALUE == "" || RAW_LOGIC_OR.MPC_VALUE == null) {
                        if (DATA_VALUES[RAW_LOGIC_OR.MPV_ID] == null || DATA_VALUES[RAW_LOGIC_OR.MPV_ID] == "") {
                          OR_STATE.push(1);
                        } else {
                          OR_STATE.push(0);
                        }
                      } else {
                        if (DATA_VALUES[RAW_LOGIC_OR.MPV_ID].search(RAW_LOGIC_OR.MPC_VALUE) == -1) {
                          OR_STATE.push(0);
                        } else {
                          OR_STATE.push(1);
                        }
                      }
                      break;
                    case "NOT IN":
                      var TEXT_SEARCH = JSON.parse(RAW_LOGIC_OR.MPC_VALUE);
                      if (TEXT_SEARCH.indexOf(DATA_VALUES[RAW_LOGIC_OR.MPV_ID]) == -1) {
                        OR_STATE.push(1);
                      } else {
                        OR_STATE.push(0);
                      }
                      break;
                    case "IN":
                      var TEXT_SEARCH = JSON.parse(RAW_LOGIC_OR.MPC_VALUE);
                      if (TEXT_SEARCH.indexOf(DATA_VALUES[RAW_LOGIC_OR.MPV_ID]) == -1) {
                        OR_STATE.push(0);
                      } else {
                        OR_STATE.push(1);
                      }
                      break;
                    default:
                      OR_STATE.push(0);
                      break;
                  }
                }
                if (OR_STATE.indexOf(1) != -1) {
                  AND_STATE.push(1);
                } else {
                  AND_STATE.push(0);
                }
              }

              if (AND_STATE.indexOf(0) != -1) {
                continue;
              }

              for (var VAR_ID in FIELD_REQ) {
                var RAW_VARIABLES = FIELD_REQ[VAR_ID];

                if (RAW_VARIABLES.KEY_ID == 1) {
                  if (FIELD_DATASTORE[RAW_VARIABLES.LABEL] != undefined) {
                    FIELD_DATASTORE[RAW_VARIABLES.LABEL] = PERIODE;
                  }
                  continue;
                }


                var DATA_RAW: any; DATA_RAW = {};
                DATA_RAW["PERIODE"] = PERIODE;
                DATA_RAW["DATA_ID"] = DATA_ID;
                DATA_RAW["MPD_VALUE"] = DATA_VALUES[VAR_ID];// RAW_VARIABLES.TYPE == 4 ? RAW_BIGQUERY[RAW_VARIABLES.KEY]["value"] : RAW_BIGQUERY[RAW_VARIABLES.KEY];
                DATA_RAW["MPD_TEXT"] = DATA_VALUES[VAR_ID];//RAW_VARIABLES.TYPE == 4 ? RAW_BIGQUERY[RAW_VARIABLES.KEY]["value"] : RAW_BIGQUERY[RAW_VARIABLES.KEY];
                DATA_RAW["START_TIME_RUN"] = START_TIME_RUN;
                DATA_RAW["M_ID"] = MERCHANT_GROUP.M_ID;
                DATA_RAW["MP_ID"] = MERCHANT_GROUP.MP_ID;
                DATA_RAW["MPG_ID"] = MERCHANT_GROUP.MPG_ID;
                DATA_RAW["MPV_ID"] = Number(VAR_ID);
                DATA_RAW["AT_CREATE"] = ControllerConfig.onCurrentTime().toString();
                DATA_RAW["AT_FLAG"] = 1;
                DATA_RAW["TYPE_DATA"] = 1;
                DATA_RAW["AT_WHO"] = USER_ID;
                DATA_RAW["SPL_ID"] = SPL_ID;

                NO++;
                if (NO % 100 == 0) {
                  POSISI++;
                }
                if (DATASOURCE[POSISI] == undefined) {
                  DATASOURCE[POSISI] = [];
                }
                DATASOURCE[POSISI].push(DATA_RAW);
              }
            }
            //END LOOPING DATA BIG QUERY

            var ERROR_CREATE = false;
            var ERROR_MSG: any; ERROR_MSG = "";
            for (var I_DATA in DATASOURCE) {
              try {
                await this.merchantProductDatastoreRepository.createAll(DATASOURCE[I_DATA]);
              } catch (errorCreateDatasource) {
                ERROR_CREATE = true;
                ERROR_MSG = errorCreateDatasource;
              }
            }

            if (ERROR_CREATE == false) {
              // CREATE DATA SOURCE SUCCESS

              var SQL = "SELECT PERIODE, M_ID, MP_ID, MPG_ID,SPL_ID,";
              for (var i in CREATE_DATA) {
                var VARID = CREATE_DATA[i];
                if (RAW_VARIABLES = FIELD_REQ[i].TYPE == 1) {
                  SQL += "MAX";
                } else {
                  SQL += "GROUP_CONCAT";
                }
                SQL += "(case when MERCHANT_PRODUCT_DATASTORE.MPV_ID = " + i + " then MERCHANT_PRODUCT_DATASTORE.MPD_TEXT end) AS VAR_" + i + ",";
              }

              SQL += "DATA_ID FROM MERCHANT_PRODUCT_DATASTORE ";
              SQL += "WHERE PERIODE = '" + PERIODE + "' AND M_ID = " + M_ID + " AND MP_ID = " + MP_ID + " AND MPG_ID = " + MPG_ID + " AND SPL_ID = " + SPL_ID + " ";
              SQL += "GROUP BY PERIODE,M_ID,MP_ID,MPG_ID,DATA_ID"
              var DATA_SUMMARY = await this.merchantProductDatastoreRepository.execute(SQL);

              var LABEL_FIELD: any; LABEL_FIELD = {}
              for (var RS_I in CREATE_DATA) {
                var FIELD = CREATE_DATA[RS_I];
                if (LABEL_FIELD[FIELD.LABEL] == undefined) {
                  LABEL_FIELD[FIELD.LABEL] = RS_I;
                }
              }

              var SUMMARY_TRANSAKSI: any; SUMMARY_TRANSAKSI = {};
              for (var SUMMARY_I in DATA_SUMMARY) {
                var RAW_SUMMARY = DATA_SUMMARY[SUMMARY_I];


                var KEY_GROUP = MERCHANT_QUERY.MPQ_GROUP_SUMMARY == "" || MERCHANT_QUERY.MPQ_GROUP_SUMMARY == undefined ? [] : JSON.parse(MERCHANT_QUERY.MPQ_GROUP_SUMMARY.split("'").join('"'));
                var KEY_SUMMARY_GROUP: any; KEY_SUMMARY_GROUP = RAW_SUMMARY.PERIODE;
                if (KEY_GROUP.length > 0) {
                  KEY_SUMMARY_GROUP = "";
                  for (var IG in KEY_GROUP) {
                    var RAW_IG = KEY_GROUP[IG];
                    if (LABEL_FIELD[RAW_IG] != undefined) {
                      KEY_SUMMARY_GROUP += "_" + RAW_SUMMARY["VAR_" + LABEL_FIELD[RAW_IG]];
                    }
                  }
                }
                if (KEY_SUMMARY_GROUP == "") {
                  KEY_SUMMARY_GROUP = RAW_SUMMARY.PERIODE;
                } else {
                  KEY_SUMMARY_GROUP = KEY_SUMMARY_GROUP.substring(1);
                }

                if (SUMMARY_TRANSAKSI[KEY_SUMMARY_GROUP] == undefined) {
                  SUMMARY_TRANSAKSI[KEY_SUMMARY_GROUP] = {
                    TRANSAKSI_TOTAL_TRX: 0,
                    TRANSAKSI_TOTAL_AMOUNT: 0,
                    TRANSAKSI_TOTAL_MDR: 0
                  };
                }

                for (var RS_I in CREATE_DATA) {
                  var FIELD_RS = CREATE_DATA[RS_I];
                  if (FIELD_RS.KEY_ID == 3) {
                    SUMMARY_TRANSAKSI[KEY_SUMMARY_GROUP].TRANSAKSI_TOTAL_TRX += Number(RAW_SUMMARY["VAR_" + RS_I]);
                  }

                  if (FIELD_RS.KEY_ID == 2) {
                    SUMMARY_TRANSAKSI[KEY_SUMMARY_GROUP].TRANSAKSI_TOTAL_AMOUNT += Number(RAW_SUMMARY["VAR_" + RS_I]);
                  }

                  if (FIELD_RS.KEY_ID == 4) {
                    if (MDR_TYPE == 1) {
                      SUMMARY_TRANSAKSI[KEY_SUMMARY_GROUP].TRANSAKSI_TOTAL_MDR += Number(RAW_SUMMARY["VAR_" + RS_I]);
                    }
                    if (MDR_TYPE == 2) {
                      SUMMARY_TRANSAKSI[KEY_SUMMARY_GROUP].TRANSAKSI_TOTAL_MDR += Number(RAW_SUMMARY["VAR_" + RS_I]);
                    }
                  }
                }
              }

              var DATASOURCE_SUMMARY: any; DATASOURCE_SUMMARY = [];
              for (var TRX in SUMMARY_TRANSAKSI) {
                var RAW_SM = SUMMARY_TRANSAKSI[TRX];


                var TRANSAKSI_TOTAL_AMOUNT = RAW_SM.TRANSAKSI_TOTAL_AMOUNT;
                var TRANSAKSI_TOTAL_TRX = RAW_SM.TRANSAKSI_TOTAL_TRX;
                var TRANSAKSI_TOTAL_MDR = RAW_SM.TRANSAKSI_TOTAL_MDR;

                if (MDR_TYPE == 1) {
                  TRANSAKSI_TOTAL_MDR = (TRANSAKSI_TOTAL_AMOUNT * (MDR_AMOUNT / 100));
                }

                var TRANSAKSI_DPP = TRANSAKSI_TOTAL_MDR / MDR_DPP;
                var TRANSAKSI_PPN = (TRANSAKSI_DPP * (MDR_PPN / 100));
                var TRANSAKSI_PPH = (TRANSAKSI_DPP * (MDR_PPH / 100));
                var TRANSAKSI_TOTAL = TRANSAKSI_DPP + TRANSAKSI_PPN;


                var SYSTEM_TOTAL_AMOUNT = TRANSAKSI_TOTAL_AMOUNT;
                var SYSTEM_TOTAL_TRX = TRANSAKSI_TOTAL_TRX;
                var SYSTEM_TOTAL_MDR = TRANSAKSI_TOTAL_MDR;

                var SELISIH = 0;
                if (MDR_TYPE == 2) {
                  var TRANSAKSI_TOTAL_MDR_SYSTEM = MDR_AMOUNT * TRANSAKSI_TOTAL_TRX;
                  SYSTEM_TOTAL_MDR = TRANSAKSI_TOTAL_MDR_SYSTEM;
                  if (TRANSAKSI_TOTAL_AMOUNT != TRANSAKSI_TOTAL_MDR_SYSTEM) {
                    if (TRANSAKSI_TOTAL_MDR_SYSTEM > TRANSAKSI_TOTAL_AMOUNT) {
                      SELISIH = 1;
                    }
                    if (TRANSAKSI_TOTAL_MDR_SYSTEM < TRANSAKSI_TOTAL_AMOUNT) {
                      SELISIH = 2;
                    }
                  }
                }

                var DPP_STYSTEM = SYSTEM_TOTAL_MDR / MDR_DPP;
                var PPN_STYSTEM = (DPP_STYSTEM * (MDR_PPN / 100));
                var PPH_SYSTEM = (DPP_STYSTEM * (MDR_PPH / 100));
                var TOTAL_SYSTEM = DPP_STYSTEM + PPN_STYSTEM;

                if (TOTAL_SYSTEM != TRANSAKSI_TOTAL) {
                  if (TOTAL_SYSTEM > TRANSAKSI_TOTAL) {
                    SELISIH = 1;
                  }
                  if (TOTAL_SYSTEM < TRANSAKSI_TOTAL) {
                    SELISIH = 2;
                  }
                } else {
                  SELISIH = 0;
                }

                DATASOURCE_SUMMARY.push({
                  PERIODE: PERIODE,
                  LINKAJA_FEE: MDR_AMOUNT,
                  LINKAJA_COUNT_TRX: Math.round(Number(TRANSAKSI_TOTAL_TRX.toFixed(2))),
                  LINKAJA_AMOUNT: Math.round(Number(TRANSAKSI_TOTAL_AMOUNT.toFixed(2))),
                  LINKAJA_MDR: Math.round(Number(TRANSAKSI_TOTAL_MDR.toFixed(2))),
                  LINKAJA_DPP: Math.round(Number(TRANSAKSI_DPP.toFixed(2))),
                  LINKAJA_PPN: Math.round(Number(TRANSAKSI_PPN.toFixed(2))),
                  LINKAJA_PPH: Math.round(Number(TRANSAKSI_PPH.toFixed(2))),
                  LINKAJA_TOTAL: Math.round(Number(TRANSAKSI_TOTAL.toFixed(2))),
                  APP_FEE: Math.round(Number(MDR_AMOUNT.toFixed(2))),
                  APP_COUNT_TRX: Math.round(Number(SYSTEM_TOTAL_TRX.toFixed(2))),
                  APP_AMOUNT: Math.round(Number(SYSTEM_TOTAL_AMOUNT.toFixed(2))),
                  APP_MDR: Math.round(Number(SYSTEM_TOTAL_MDR.toFixed(2))),
                  APP_DPP: Math.round(Number(DPP_STYSTEM.toFixed(2))),
                  APP_PPN: Math.round(Number(PPN_STYSTEM.toFixed(2))),
                  APP_PPH: Math.round(Number(PPH_SYSTEM.toFixed(2))),
                  APP_TOTAL: Math.round(Number(TOTAL_SYSTEM.toFixed(2))),
                  HAS_SELISIH: SELISIH,
                  START_TIME_RUN: "BIGQUERY_" + SPL_ID.toString(),
                  M_ID: M_ID,
                  MP_ID: MP_ID,
                  MPG_ID: MPG_ID,
                  MPM_ID: undefined,
                  MPM_SELISIH_ID: undefined,
                  TYPE_DATA: 1,
                  AT_CREATE: ControllerConfig.onCurrentTime().toString(),
                  AT_FLAG: 0,
                  AT_WHO: USER_ID,
                  MPS_LABEL: TRX.toString()
                });
              }

              var DELETE_SUMMARY = await this.merchantProductSummaryRepository.deleteAll({and: [{PERIODE: PERIODE}, {M_ID: M_ID}, {MP_ID: MP_ID}, {MPG_ID: MPG_ID}]});
              for (var i in DATASOURCE_SUMMARY) {
                await this.merchantProductSummaryRepository.create(DATASOURCE_SUMMARY[i]);
              }

              var SUMMARY_DATA = await this.merchantProductSummaryRepository.find({where: {and: [{PERIODE: PERIODE}, {M_ID: M_ID}, {MP_ID: MP_ID}]}});

              var LINKAJA_COUNT_TRX = 0;
              var LINKAJA_AMOUNT = 0;
              var LINKAJA_MDR = 0;
              var LINKAJA_DPP = 0;
              var LINKAJA_PPN = 0;
              var LINKAJA_PPH = 0;
              var LINKAJA_TOTAL = 0;


              var SYSTEM_COUNT_TRX = 0;
              var SYSTEM_AMOUNT = 0;
              var SYSTEM_MDR = 0;
              var SYSTEM_DPP = 0;
              var SYSTEM_PPN = 0;
              var SYSTEM_PPH = 0;
              var SYSTEM_TOTAL = 0;

              for (var I_SMD in SUMMARY_DATA) {
                var RAW_SUMMARY: any; RAW_SUMMARY = SUMMARY_DATA[I_SMD];

                //MIGRASI
                LINKAJA_COUNT_TRX += Number(RAW_SUMMARY.LINKAJA_COUNT_TRX);
                LINKAJA_AMOUNT += Number(RAW_SUMMARY.LINKAJA_AMOUNT);
                LINKAJA_MDR += Number(RAW_SUMMARY.LINKAJA_MDR);
                LINKAJA_DPP += Number(RAW_SUMMARY.LINKAJA_DPP);
                LINKAJA_PPN += Number(RAW_SUMMARY.LINKAJA_PPN);
                LINKAJA_PPH += Number(RAW_SUMMARY.LINKAJA_PPH);
                LINKAJA_TOTAL += Number(RAW_SUMMARY.LINKAJA_TOTAL);

                //DATASTORE
                SYSTEM_COUNT_TRX += Number(RAW_SUMMARY.APP_COUNT_TRX);
                SYSTEM_AMOUNT += Number(RAW_SUMMARY.APP_AMOUNT);
                SYSTEM_MDR += Number(RAW_SUMMARY.APP_MDR);
                SYSTEM_DPP += Number(RAW_SUMMARY.APP_DPP);
                SYSTEM_PPN += Number(RAW_SUMMARY.APP_PPN);
                SYSTEM_PPH += Number(RAW_SUMMARY.APP_PPH);
                SYSTEM_TOTAL += Number(RAW_SUMMARY.APP_TOTAL);

              }

              var IS_SELISIH = 0;
              var STATUS_CALCULATE = 0;

              var SELISIH_COUNT_TRX = 0;
              var SELISIH_AMOUNT = 0;
              var SELISIH_MDR = 0;
              var SELISIH_DPP = 0;
              var SELISIH_PPN = 0;
              var SELISIH_PPH = 0;
              var SELISIH_TOTAL = 0;

              if (LINKAJA_TOTAL != SYSTEM_TOTAL) {
                IS_SELISIH = 1;
                if (LINKAJA_TOTAL < SYSTEM_TOTAL) {
                  //KURANG BAYAR
                  STATUS_CALCULATE = 1;
                  SELISIH_COUNT_TRX = SYSTEM_COUNT_TRX - LINKAJA_COUNT_TRX;
                  SELISIH_AMOUNT = SYSTEM_AMOUNT - LINKAJA_AMOUNT;
                  SELISIH_MDR = SYSTEM_MDR - LINKAJA_MDR;
                  SELISIH_DPP = SYSTEM_DPP - LINKAJA_DPP;
                  SELISIH_PPN = SYSTEM_PPN - LINKAJA_PPN;
                  SELISIH_PPH = SYSTEM_PPH - LINKAJA_PPH;
                  SELISIH_TOTAL = SYSTEM_TOTAL - LINKAJA_TOTAL;
                }
                if (LINKAJA_TOTAL > SYSTEM_TOTAL) {
                  //LEBIH BAYAR
                  STATUS_CALCULATE = 2;
                  SELISIH_COUNT_TRX = LINKAJA_COUNT_TRX - SYSTEM_COUNT_TRX;
                  SELISIH_AMOUNT = LINKAJA_AMOUNT - SYSTEM_AMOUNT;
                  SELISIH_MDR = LINKAJA_MDR - SYSTEM_MDR;
                  SELISIH_DPP = LINKAJA_DPP - SYSTEM_DPP;
                  SELISIH_PPN = LINKAJA_PPN - SYSTEM_PPN;
                  SELISIH_PPH = LINKAJA_PPH - SYSTEM_PPH;
                  SELISIH_TOTAL = LINKAJA_TOTAL - SYSTEM_TOTAL;
                }
              }

              var MASTER_ID = undefined;
              var MASTER_SELISIH_ID = undefined;

              var MASTER_NOT_SELISIH = await this.merchantProductMasterRepository.findOne({where: {and: [{PERIODE: PERIODE}, {M_ID: M_ID}, {MP_ID: MP_ID}, {IS_SELISIH: 0}]}});
              if (MASTER_NOT_SELISIH == null) {
                var MASTER_DATA = await this.merchantProductMasterRepository.create({
                  PERIODE: PERIODE,
                  MPM_COUNT_TRX: LINKAJA_COUNT_TRX,
                  MPM_AMOUNT: LINKAJA_AMOUNT,
                  MPM_MDR: LINKAJA_MDR,
                  MPM_DPP: LINKAJA_DPP,
                  MPM_PPN: LINKAJA_PPN,
                  MPM_PPH: LINKAJA_PPH,
                  MPM_TOTAL: LINKAJA_TOTAL,
                  M_ID: M_ID,
                  MP_ID: MP_ID,
                  IS_SELISIH: 0,
                  START_TIME_RUN: "BIGQUERY_" + SPL_ID.toString(),
                  AT_CREATE: ControllerConfig.onCurrentTime().toString(),
                  AT_UPDATE: undefined,
                  AT_WHO: USER_ID,
                  AT_POSISI: 1,
                  AT_FLAG: 0
                });
                MASTER_ID = MASTER_DATA.MPM_ID;
              } else {
                await this.merchantProductMasterRepository.updateById(MASTER_NOT_SELISIH.MPM_ID, {
                  PERIODE: PERIODE,
                  MPM_COUNT_TRX: LINKAJA_COUNT_TRX,
                  MPM_AMOUNT: LINKAJA_AMOUNT,
                  MPM_MDR: LINKAJA_MDR,
                  MPM_DPP: LINKAJA_DPP,
                  MPM_PPN: LINKAJA_PPN,
                  MPM_PPH: LINKAJA_PPH,
                  MPM_TOTAL: LINKAJA_TOTAL,
                  M_ID: M_ID,
                  MP_ID: MP_ID,
                  IS_SELISIH: 0,
                  START_TIME_RUN: "BIGQUERY_" + SPL_ID.toString(),
                  AT_CREATE: ControllerConfig.onCurrentTime().toString(),
                  AT_UPDATE: undefined,
                  AT_WHO: USER_ID,
                  AT_POSISI: 1,
                  AT_FLAG: 0
                });
                MASTER_ID = MASTER_NOT_SELISIH.MPM_ID;
              }



              if (IS_SELISIH != 0) {
                var MASTER_YES_SELISIH = await this.merchantProductMasterRepository.findOne({where: {and: [{PERIODE: PERIODE}, {M_ID: M_ID}, {MP_ID: MP_ID}, {IS_SELISIH: {inq: [1, 2]}}]}});
                if (MASTER_YES_SELISIH == null) {
                  var MASTER_DATA_SELISIH = await this.merchantProductMasterRepository.create({
                    PERIODE: PERIODE,
                    MPM_COUNT_TRX: 1,
                    MPM_AMOUNT: 1,
                    MPM_MDR: SELISIH_MDR,
                    MPM_DPP: SELISIH_DPP,
                    MPM_PPN: SELISIH_PPN,
                    MPM_PPH: SELISIH_PPH,
                    MPM_TOTAL: SELISIH_TOTAL,
                    M_ID: M_ID,
                    MP_ID: MP_ID,
                    IS_SELISIH: STATUS_CALCULATE,
                    START_TIME_RUN: "BIGQUERY_" + SPL_ID.toString(),
                    AT_CREATE: ControllerConfig.onCurrentTime().toString(),
                    AT_UPDATE: undefined,
                    AT_WHO: USER_ID,
                    AT_POSISI: 1,
                    AT_FLAG: 0
                  });
                  MASTER_SELISIH_ID = MASTER_DATA_SELISIH.MPM_ID;
                } else {
                  await this.merchantProductMasterRepository.updateById(MASTER_YES_SELISIH.MPM_ID, {
                    PERIODE: PERIODE,
                    MPM_COUNT_TRX: 1,
                    MPM_AMOUNT: 1,
                    MPM_MDR: SELISIH_MDR,
                    MPM_DPP: SELISIH_DPP,
                    MPM_PPN: SELISIH_PPN,
                    MPM_PPH: SELISIH_PPH,
                    MPM_TOTAL: SELISIH_TOTAL,
                    M_ID: M_ID,
                    MP_ID: MP_ID,
                    IS_SELISIH: STATUS_CALCULATE,
                    START_TIME_RUN: "BIGQUERY_" + SPL_ID.toString(),
                    AT_CREATE: ControllerConfig.onCurrentTime().toString(),
                    AT_UPDATE: undefined,
                    AT_WHO: USER_ID,
                    AT_POSISI: 1,
                    AT_FLAG: 0
                  });
                  MASTER_SELISIH_ID = MASTER_YES_SELISIH.MPM_ID;
                }
              }

              for (var i in SUMMARY_DATA) {
                var RAW_SM: any; RAW_SM = SUMMARY_DATA[i];
                await this.merchantProductSummaryRepository.updateById(RAW_SM.SUMMARY_ID, {
                  MPM_ID: MASTER_ID,
                  MPM_SELISIH_ID: MASTER_SELISIH_ID
                });
              }

              await this.sycronizeProcessLogRepository.updateById(SPL_ID, {
                SPL_PROCESS_NOW: PROCCESS_LOAD_DATA,
                AT_FLAG: 2,
                SPL_MESSAGE: "SUCCESS !"
              });

              delete PROCESS_SYNC_DATA[PROCESS_KEY][I_KEY_CREATE];
              this.ON_PROCESS_CREATE(PROCESS_KEY);

            } else {
              await this.sycronizeProcessLogRepository.updateById(SPL_ID, {
                SPL_MDR: 0,
                SPL_COUNT_TRX: 0,
                SPL_TRX_AMOUNT: 0,
                SPL_PROCESS_COUNT: 100,
                SPL_PROCESS_NOW: 100,
                AT_FLAG: 3,
                SPL_MESSAGE: JSON.stringify(ERROR_MSG)
              });

              await this.merchantProductDatastoreRepository.deleteAll({and: [{SPL_ID: SPL_ID}]});
              delete PROCESS_SYNC_DATA[PROCESS_KEY][I_KEY_CREATE];
              this.ON_PROCESS_CREATE(PROCESS_KEY);
            }

          } else {
            await this.sycronizeProcessLogRepository.updateById(SPL_ID, {
              SPL_MDR: 0,
              SPL_COUNT_TRX: 0,
              SPL_TRX_AMOUNT: 0,
              SPL_PROCESS_COUNT: 100,
              SPL_PROCESS_NOW: 100,
              AT_FLAG: 3,
              SPL_MESSAGE: "Error, please config all requiredment avaliable !"
            });
            delete PROCESS_SYNC_DATA[PROCESS_KEY][I_KEY_CREATE];
            this.ON_PROCESS_CREATE(PROCESS_KEY);
          }



          //       var INSERT_DATA: boolean; INSERT_DATA = true;
          //       for (var CAHECK_I in CREATE_DATA) {
          //         if (CREATE_DATA[CAHECK_I] == "") {
          //           INSERT_DATA = false;
          //           break;
          //         }
          //       }

          //       for (var VAR_ID in FIELD_REQ) {
          //         var RAW_VARIABLES = FIELD_REQ[VAR_ID];
          //         if (RAW_VARIABLES.KEY_ID == 1) {
          //           if (FIELD_DATASTORE[RAW_VARIABLES.LABEL] != undefined) {
          //             FIELD_DATASTORE[RAW_VARIABLES.LABEL] = PERIODE;
          //           }
          //           continue;
          //         }

          //         if (FIELD_DATASTORE[RAW_VARIABLES.LABEL] != undefined) {
          //           FIELD_DATASTORE[RAW_VARIABLES.LABEL] = RAW_VARIABLES.TYPE == 4 ? RAW_BIGQUERY[RAW_VARIABLES.KEY]["value"] : RAW_BIGQUERY[RAW_VARIABLES.KEY];
          //         }

          //         var DATA_RAW: any; DATA_RAW = {};
          //         DATA_RAW["PERIODE"] = PERIODE;
          //         DATA_RAW["DATA_ID"] = DATA_ID;
          //         DATA_RAW["MPD_VALUE"] = RAW_VARIABLES.TYPE == 4 ? RAW_BIGQUERY[RAW_VARIABLES.KEY]["value"] : RAW_BIGQUERY[RAW_VARIABLES.KEY];
          //         DATA_RAW["MPD_TEXT"] = RAW_VARIABLES.TYPE == 4 ? RAW_BIGQUERY[RAW_VARIABLES.KEY]["value"] : RAW_BIGQUERY[RAW_VARIABLES.KEY];
          //         DATA_RAW["START_TIME_RUN"] = START_TIME_RUN;
          //         DATA_RAW["M_ID"] = MERCHANT_GROUP.M_ID;
          //         DATA_RAW["MP_ID"] = MERCHANT_GROUP.MP_ID;
          //         DATA_RAW["MPG_ID"] = MERCHANT_GROUP.MPG_ID;
          //         DATA_RAW["MPV_ID"] = Number(VAR_ID);
          //         DATA_RAW["AT_CREATE"] = ControllerConfig.onCurrentTime().toString();
          //         DATA_RAW["AT_FLAG"] = 1;
          //         DATA_RAW["TYPE_DATA"] = 1;
          //         DATA_RAW["AT_WHO"] = USER_ID;
          //         DATASOURCE.push(DATA_RAW);
          //         if (INSERT_DATA == true) {
          //           await this.merchantProductDatastoreRepository.create(DATA_RAW);
          //         }
          //       }
          //       TRANSACTION.push(FIELD_DATASTORE);
          //     }

          //     var CREATE_DATA_CECK: any; CREATE_DATA = {2: "", 4: ""};
          //     var DATA_ID = "";
          //     for (var VAR_ID in FIELD_REQ) {
          //       var RAW_VARIABLES = FIELD_REQ[VAR_ID];
          //       if (RAW_VARIABLES.KEY_ID == 6) {
          //         DATA_ID = RAW_VARIABLES.TYPE == 4 ? RAW_BIGQUERY[RAW_VARIABLES.KEY]["value"] : RAW_BIGQUERY[RAW_VARIABLES.KEY];
          //       }

          //     }

          //     var SQL = "SELECT PERIODE, M_ID, MP_ID, MPG_ID,";
          //     for (var i in CREATE_DATA) {
          //       var VARID = CREATE_DATA[i];
          //       if (RAW_VARIABLES = FIELD_REQ[VARID].TYPE == 1) {
          //         SQL += "MAX";
          //       } else {
          //         SQL += "GROUP_CONCAT";
          //       }
          //       SQL += "(case when MERCHANT_PRODUCT_DATASTORE.MPV_ID = " + VARID + " then MERCHANT_PRODUCT_DATASTORE.MPD_TEXT end) AS VAR_" + i + ",";
          //     }

          //     SQL += "DATA_ID FROM MERCHANT_PRODUCT_DATASTORE ";
          //     SQL += "WHERE PERIODE = '" + PERIODE + "' AND M_ID = " + M_ID + " AND MP_ID = " + MP_ID + " AND MPG_ID = " + MPG_ID + " ";
          //     SQL += "GROUP BY PERIODE,M_ID,MP_ID,MPG_ID,DATA_ID"

          //     // console.log(SQL);
          //     var DELETE_SUMMARY = await this.merchantProductSummaryRepository.deleteAll({and: [{PERIODE: PERIODE}, {M_ID: M_ID}, {MP_ID: MP_ID}, {MPG_ID: MPG_ID}]});
          //     var DATA_SUMMARY = await this.merchantProductDatastoreRepository.execute(SQL);
          //     // console.log(DATA_SUMMARY);

          //     //var MDR_TYPE = MERCHANT_GROUP.MPG_MDR_TYPE;//1 : PERCENT - 2 : AMOUNT
          //     //var MDR_AMOUNT = MERCHANT_GROUP.MPG_MDR_AMOUNT;
          //     //var MDR_DPP = MERCHANT_GROUP.MPG_DPP;
          //     //var MDR_PPN = MERCHANT_GROUP.MPG_PPN;
          //     //var MDR_PPH = MERCHANT_GROUP.MPG_PPH;
          //     var TRANSAKSI_TOTAL_AMOUNT = 0;
          //     var TRANSAKSI_TOTAL_TRX = 0;
          //     var TRANSAKSI_TOTAL_MDR = 0;
          //     for (var RAW_INDEX in DATA_SUMMARY) {
          //       var DATA_SM = DATA_SUMMARY[RAW_INDEX];
          //       if (MDR_TYPE == 1) {
          //         if (DATA_SM.VAR_4 == 0 || DATA_SM.VAR_4 == null) {
          //           continue;
          //         }
          //         TRANSAKSI_TOTAL_TRX++;
          //         TRANSAKSI_TOTAL_AMOUNT += Number(DATA_SM.VAR_4);
          //       }
          //       if (MDR_TYPE == 2) {
          //         TRANSAKSI_TOTAL_TRX++;
          //         TRANSAKSI_TOTAL_AMOUNT += Number(DATA_SM.VAR_2);
          //         TRANSAKSI_TOTAL_MDR += Number(DATA_SM.VAR_4);
          //       }
          //     }

          //     if (MDR_TYPE == 1) {
          //       TRANSAKSI_TOTAL_MDR = (TRANSAKSI_TOTAL_AMOUNT * (MDR_AMOUNT / 100));
          //     }

          //     var TRANSAKSI_DPP = TRANSAKSI_TOTAL_MDR / MDR_DPP;
          //     var TRANSAKSI_PPN = (TRANSAKSI_DPP * (MDR_PPN / 100));
          //     var TRANSAKSI_PPH = (TRANSAKSI_DPP * (MDR_PPH / 100));
          //     var TRANSAKSI_TOTAL = TRANSAKSI_DPP + TRANSAKSI_PPN;


          //     var SYSTEM_TOTAL_AMOUNT = TRANSAKSI_TOTAL_AMOUNT;
          //     var SYSTEM_TOTAL_TRX = TRANSAKSI_TOTAL_TRX;
          //     var SYSTEM_TOTAL_MDR = TRANSAKSI_TOTAL_MDR;

          //     var SELISIH = 0;
          //     if (MDR_TYPE == 2) {
          //       var TRANSAKSI_TOTAL_MDR_SYSTEM = MDR_AMOUNT * TRANSAKSI_TOTAL_TRX;
          //       SYSTEM_TOTAL_MDR = TRANSAKSI_TOTAL_MDR_SYSTEM;
          //       if (TRANSAKSI_TOTAL_AMOUNT != TRANSAKSI_TOTAL_MDR_SYSTEM) {
          //         if (TRANSAKSI_TOTAL_MDR_SYSTEM > TRANSAKSI_TOTAL_AMOUNT) {
          //           SELISIH = 1;
          //         }
          //         if (TRANSAKSI_TOTAL_MDR_SYSTEM < TRANSAKSI_TOTAL_AMOUNT) {
          //           SELISIH = 2;
          //         }
          //       }
          //     }

          //     var DPP_STYSTEM = SYSTEM_TOTAL_MDR / MDR_DPP;
          //     var PPN_STYSTEM = (DPP_STYSTEM * (MDR_PPN / 100));
          //     var PPH_SYSTEM = (DPP_STYSTEM * (MDR_PPH / 100));
          //     var TOTAL_SYSTEM = DPP_STYSTEM + PPN_STYSTEM;

          //     if (TOTAL_SYSTEM != TRANSAKSI_TOTAL) {
          //       if (TOTAL_SYSTEM > TRANSAKSI_TOTAL) {
          //         SELISIH = 1;
          //       }
          //       if (TOTAL_SYSTEM < TRANSAKSI_TOTAL) {
          //         SELISIH = 2;
          //       }
          //     } else {
          //       SELISIH = 0;
          //     }

          //     var START_TIME = SPL_ID + "_" + PERIODE;
          //     await this.merchantProductSummaryRepository.create({
          //       PERIODE: PERIODE,
          //       LINKAJA_FEE: MDR_AMOUNT,
          //       LINKAJA_COUNT_TRX: TRANSAKSI_TOTAL_TRX,
          //       LINKAJA_AMOUNT: TRANSAKSI_TOTAL_AMOUNT,
          //       LINKAJA_MDR: TRANSAKSI_TOTAL_MDR,
          //       LINKAJA_DPP: TRANSAKSI_DPP,
          //       LINKAJA_PPN: TRANSAKSI_PPN,
          //       LINKAJA_PPH: TRANSAKSI_PPH,
          //       LINKAJA_TOTAL: TRANSAKSI_TOTAL,
          //       APP_FEE: MDR_AMOUNT,
          //       APP_COUNT_TRX: SYSTEM_TOTAL_TRX,
          //       APP_AMOUNT: SYSTEM_TOTAL_AMOUNT,
          //       APP_MDR: SYSTEM_TOTAL_MDR,
          //       APP_DPP: DPP_STYSTEM,
          //       APP_PPN: PPN_STYSTEM,
          //       APP_PPH: PPH_SYSTEM,
          //       APP_TOTAL: TOTAL_SYSTEM,
          //       HAS_SELISIH: SELISIH,
          //       START_TIME_RUN: START_TIME,
          //       M_ID: M_ID,
          //       MP_ID: MP_ID,
          //       MPG_ID: MPG_ID,
          //       MPM_ID: undefined,
          //       MPM_SELISIH_ID: undefined,
          //       TYPE_DATA: 1,
          //       AT_CREATE: ControllerConfig.onCurrentTime().toString(),
          //       AT_FLAG: 0,
          //       AT_WHO: USER_ID
          //     });

          //     var SUMMARY_DATA = await this.merchantProductSummaryRepository.find({where: {and: [{PERIODE: PERIODE}, {M_ID: M_ID}, {MP_ID: MP_ID}]}});
          //     var MASTER = await this.merchantProductMasterRepository.deleteAll({and: [{PERIODE: PERIODE}, {M_ID: M_ID}, {MP_ID: MP_ID}]});

          //     var LINKAJA_COUNT_TRX = 0;
          //     var LINKAJA_AMOUNT = 0;
          //     var LINKAJA_MDR = 0;
          //     var LINKAJA_DPP = 0;
          //     var LINKAJA_PPN = 0;
          //     var LINKAJA_PPH = 0;
          //     var LINKAJA_TOTAL = 0;


          //     var SYSTEM_COUNT_TRX = 0;
          //     var SYSTEM_AMOUNT = 0;
          //     var SYSTEM_MDR = 0;
          //     var SYSTEM_DPP = 0;
          //     var SYSTEM_PPN = 0;
          //     var SYSTEM_PPH = 0;
          //     var SYSTEM_TOTAL = 0;

          //     var SUMMARY_AVA: any; SUMMARY_AVA = [];
          //     for (var I_SMD in SUMMARY_DATA) {
          //       var RAW_SUMMARY = SUMMARY_DATA[I_SMD];
          //       if (SUMMARY_AVA.indexOf(RAW_SUMMARY.SUMMARY_ID) == -1) {
          //         SUMMARY_AVA.push(RAW_SUMMARY.SUMMARY_ID);
          //       }
          //       //MIGRASI
          //       LINKAJA_COUNT_TRX += Number(RAW_SUMMARY.LINKAJA_COUNT_TRX);
          //       LINKAJA_AMOUNT += Number(RAW_SUMMARY.LINKAJA_AMOUNT);
          //       LINKAJA_MDR += Number(RAW_SUMMARY.LINKAJA_MDR);
          //       LINKAJA_DPP += Number(RAW_SUMMARY.LINKAJA_DPP);
          //       LINKAJA_PPN += Number(RAW_SUMMARY.LINKAJA_PPN);
          //       LINKAJA_PPH += Number(RAW_SUMMARY.LINKAJA_PPH);
          //       LINKAJA_TOTAL += Number(RAW_SUMMARY.LINKAJA_TOTAL);

          //       //DATASTORE
          //       SYSTEM_COUNT_TRX += Number(RAW_SUMMARY.APP_COUNT_TRX);
          //       SYSTEM_AMOUNT += Number(RAW_SUMMARY.APP_AMOUNT);
          //       SYSTEM_MDR += Number(RAW_SUMMARY.APP_MDR);
          //       SYSTEM_DPP += Number(RAW_SUMMARY.APP_DPP);
          //       SYSTEM_PPN += Number(RAW_SUMMARY.APP_PPN);
          //       SYSTEM_PPH += Number(RAW_SUMMARY.APP_PPH);
          //       SYSTEM_TOTAL += Number(RAW_SUMMARY.APP_TOTAL);

          //     }

          //     var IS_SELISIH = 0;
          //     var STATUS_CALCULATE = 0;

          //     var SELISIH_COUNT_TRX = 0;
          //     var SELISIH_AMOUNT = 0;
          //     var SELISIH_MDR = 0;
          //     var SELISIH_DPP = 0;
          //     var SELISIH_PPN = 0;
          //     var SELISIH_PPH = 0;
          //     var SELISIH_TOTAL = 0;

          //     if (LINKAJA_TOTAL != SYSTEM_TOTAL) {
          //       IS_SELISIH = 1;
          //       if (LINKAJA_TOTAL < SYSTEM_TOTAL) {
          //         //KURANG BAYAR
          //         STATUS_CALCULATE = 1;
          //         SELISIH_COUNT_TRX = SYSTEM_COUNT_TRX - LINKAJA_COUNT_TRX;
          //         SELISIH_AMOUNT = SYSTEM_AMOUNT - LINKAJA_AMOUNT;
          //         SELISIH_MDR = SYSTEM_MDR - LINKAJA_MDR;
          //         SELISIH_DPP = SYSTEM_DPP - LINKAJA_DPP;
          //         SELISIH_PPN = SYSTEM_PPN - LINKAJA_PPN;
          //         SELISIH_PPH = SYSTEM_PPH - LINKAJA_PPH;
          //         SELISIH_TOTAL = SYSTEM_TOTAL - LINKAJA_TOTAL;
          //       }
          //       if (LINKAJA_TOTAL > SYSTEM_TOTAL) {
          //         //LEBIH BAYAR
          //         STATUS_CALCULATE = 2;
          //         SELISIH_COUNT_TRX = LINKAJA_COUNT_TRX - SYSTEM_COUNT_TRX;
          //         SELISIH_AMOUNT = LINKAJA_AMOUNT - SYSTEM_AMOUNT;
          //         SELISIH_MDR = LINKAJA_MDR - SYSTEM_MDR;
          //         SELISIH_DPP = LINKAJA_DPP - SYSTEM_DPP;
          //         SELISIH_PPN = LINKAJA_PPN - SYSTEM_PPN;
          //         SELISIH_PPH = LINKAJA_PPH - SYSTEM_PPH;
          //         SELISIH_TOTAL = LINKAJA_TOTAL - SYSTEM_TOTAL;
          //       }
          //     }


          //     await this.merchantProductMasterRepository.create({
          //       PERIODE: PERIODE,
          //       MPM_COUNT_TRX: LINKAJA_COUNT_TRX,
          //       MPM_AMOUNT: LINKAJA_AMOUNT,
          //       MPM_MDR: LINKAJA_MDR,
          //       MPM_DPP: LINKAJA_DPP,
          //       MPM_PPN: LINKAJA_PPN,
          //       MPM_PPH: LINKAJA_PPH,
          //       MPM_TOTAL: LINKAJA_TOTAL,
          //       M_ID: M_ID,
          //       MP_ID: MP_ID,
          //       IS_SELISIH: 0,
          //       START_TIME_RUN: JSON.stringify(SUMMARY_AVA),
          //       AT_CREATE: ControllerConfig.onCurrentTime().toString(),
          //       AT_UPDATE: undefined,
          //       AT_WHO: USER_ID,
          //       AT_POSISI: 1,
          //       AT_FLAG: 0
          //     });

          //     if (IS_SELISIH != 0) {
          //       await this.merchantProductMasterRepository.create({
          //         PERIODE: PERIODE,
          //         MPM_COUNT_TRX: 1,
          //         MPM_AMOUNT: 1,
          //         MPM_MDR: SELISIH_MDR,
          //         MPM_DPP: SELISIH_DPP,
          //         MPM_PPN: SELISIH_PPN,
          //         MPM_PPH: SELISIH_PPH,
          //         MPM_TOTAL: SELISIH_TOTAL,
          //         M_ID: M_ID,
          //         MP_ID: MP_ID,
          //         IS_SELISIH: STATUS_CALCULATE,
          //         START_TIME_RUN: JSON.stringify(SUMMARY_AVA),
          //         AT_CREATE: ControllerConfig.onCurrentTime().toString(),
          //         AT_UPDATE: undefined,
          //         AT_WHO: USER_ID,
          //         AT_POSISI: 1,
          //         AT_FLAG: 0
          //       });
          //     }


          //     try {
          //       await this.sycronizeProcessLogRepository.updateById(SPL_ID, {
          //         SPL_MDR: MDR_AMOUNT,
          //         SPL_COUNT_TRX: TRANSAKSI_TOTAL_TRX,
          //         SPL_TRX_AMOUNT: TRANSAKSI_TOTAL_AMOUNT,
          //         AT_FLAG: 1
          //       })
          //     } catch (error) {

          //     }

          //     delete PROCESS_SYNC_DATA[PROCESS_KEY][I_KEY_CREATE];
          //     this.ON_PROCESS_CREATE(PROCESS_KEY);
        } else {
          await this.sycronizeProcessLogRepository.updateById(SPL_ID, {
            SPL_MDR: 0,
            SPL_COUNT_TRX: 0,
            SPL_TRX_AMOUNT: 0,
            SPL_PROCESS_COUNT: 100,
            SPL_PROCESS_NOW: 100,
            AT_FLAG: 3,
            SPL_MESSAGE: "Error, Please config big query before syc with big query !"
          });
          delete PROCESS_SYNC_DATA[PROCESS_KEY][I_KEY_CREATE];
          this.ON_PROCESS_CREATE(PROCESS_KEY);
        }
      } else {
        try {
          await this.sycronizeProcessLogRepository.updateById(SPL_ID, {
            SPL_MDR: 0,
            SPL_COUNT_TRX: 0,
            SPL_TRX_AMOUNT: 0,
            AT_FLAG: 3,
            SPL_MESSAGE: "PRODUCT GROUP NOT FOUND FOR SYCRONIZE"
          });
        } catch (error) {
          await this.sycronizeProcessLogRepository.updateById(SPL_ID, {
            SPL_MDR: 0,
            SPL_COUNT_TRX: 0,
            SPL_TRX_AMOUNT: 0,
            SPL_PROCESS_COUNT: 100,
            SPL_PROCESS_NOW: 100,
            AT_FLAG: 3,
            SPL_MESSAGE: JSON.stringify(error)
          });
        }
        delete PROCESS_SYNC_DATA[PROCESS_KEY][I_KEY_CREATE];
        this.ON_PROCESS_CREATE(PROCESS_KEY);
      }
      break;
    }
  }

  ON_PROCESS_SYNC = async (PROCESS_KEY: any) => {
    setTimeout(() => {
      console.log("checking....");
      var NO = 0;
      for (var i in PROCESS_SYNC_DATA[PROCESS_KEY]) {NO++;}
      if (NO == 0) {
        delete PROCESS_SYNC_DATA[PROCESS_KEY];
        delete PROCESS_SYNC[PROCESS_KEY];
      } else {
        this.ON_PROCESS_SYNC(PROCESS_KEY);
      }
    }, 5000);
  }

  ON_RUNNING_SYNC = async (SYNC_ID: any) => {
    var Process: any; Process = [];
    if (SYNC_ID == null) {
      Process = await this.sycronizeProcessLogRepository.find({where: {and: [{AT_FLAG: 0}]}});
    } else {
      Process = await this.sycronizeProcessLogRepository.find({where: {and: [{AT_FLAG: 0}, {SPL_ID: {inq: SYNC_ID}}]}});
    }

    for (var I_PROCESS in Process) {
      var LOG_PROCESS = Process[I_PROCESS];
      var KEY_PROCESS = LOG_PROCESS.SPL_PERIODE + "_" + LOG_PROCESS.M_ID + "_" + LOG_PROCESS.MP_ID;
      if (PROCESS_SYNC_DATA[KEY_PROCESS] == undefined) {
        PROCESS_SYNC_DATA[KEY_PROCESS] = {};
      }
      var KEY_PROCESS_DATA = LOG_PROCESS.SPL_PERIODE + "_" + LOG_PROCESS.M_ID + "_" + LOG_PROCESS.MP_ID + "_" + LOG_PROCESS.MPG_ID;
      if (PROCESS_SYNC_DATA[KEY_PROCESS][KEY_PROCESS_DATA] == undefined) {
        PROCESS_SYNC_DATA[KEY_PROCESS][KEY_PROCESS_DATA] = LOG_PROCESS;
      }

      if (PROCESS_SYNC[KEY_PROCESS] == undefined) {
        this.ON_PROCESS_CREATE(KEY_PROCESS);
        PROCESS_SYNC[KEY_PROCESS] = new Promise(async (resolve, reject) => {
          var ID = KEY_PROCESS;
          var RESULT_PROCESS = this.ON_PROCESS_SYNC(ID);
          resolve({success: true, message: "Done !"});
        });
      }
    }
  }

  @get('/merchant-sychronize-datasource/big-query/new-avaliable', {
    responses: {
      '200': {
        description: 'Array of Merchant Product model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(SycronizeProcessLog, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async sychBigQueryNewAva(
    @param.filter(SycronizeProcessLog) filter?: Filter<SycronizeProcessLog>,
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};

    var DATA_PRODUCT_GROUP = await this.merchantProductGroupRepository.find();

    var DATAS: any; DATAS = {};
    for (var IPG in DATA_PRODUCT_GROUP) {
      var RAW_IGP = DATA_PRODUCT_GROUP[IPG];
      if (DATAS[RAW_IGP.M_ID] == undefined) {
        var MERCHANT = await this.merchantRepository.findById(RAW_IGP.M_ID);
        DATAS[RAW_IGP.M_ID] = {ID: MERCHANT.M_ID, LABEL: MERCHANT.M_LEGAL_NAME, DATA: {}}
      }

      if (DATAS[RAW_IGP.M_ID]["DATA"][RAW_IGP.MP_ID] == undefined) {
        var MERCHANT_PRODUCT = await this.merchantProductRepository.findById(RAW_IGP.MP_ID);
        DATAS[RAW_IGP.M_ID]["DATA"][RAW_IGP.MP_ID] = {ID: RAW_IGP.MP_ID, LABEL: MERCHANT_PRODUCT.MP_BRAND, DATA: {}, TARGET: []}
      }

      var ID_IGP = RAW_IGP.MPG_ID == undefined ? -1 : RAW_IGP.MPG_ID;
      if (DATAS[RAW_IGP.M_ID]["DATA"][RAW_IGP.MP_ID]["DATA"][ID_IGP] == undefined) {
        DATAS[RAW_IGP.M_ID]["DATA"][RAW_IGP.MP_ID]["DATA"][ID_IGP] = {ID: RAW_IGP.MPG_ID, LABEL: RAW_IGP.MPG_LABEL};
        DATAS[RAW_IGP.M_ID]["DATA"][RAW_IGP.MP_ID]["TARGET"].push(RAW_IGP.MPG_ID);
      }


    }
    result = {success: true, message: 'Succes !', datas: DATAS};

    return result;
  }

  @get('/merchant-sychronize-datasource/big-query/avaliable', {
    responses: {
      '200': {
        description: 'Array of Merchant Product model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(SycronizeProcessLog, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async sychBigQueryAvaliable(
    @param.filter(SycronizeProcessLog) filter?: Filter<SycronizeProcessLog>,
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};

    var SYCRONIZE = await this.sycronizeProcessLogRepository.find();
    var MERCHANT_AVA: any; MERCHANT_AVA = [];
    var MERCHANT_PRODUCT_AVA: any; MERCHANT_PRODUCT_AVA = [];
    var MERCHANT_PRODUCT_GROUP_AVA: any; MERCHANT_PRODUCT_GROUP_AVA = [];

    for (var i in SYCRONIZE) {
      var RAW_SYC = SYCRONIZE[i];
      if (MERCHANT_PRODUCT_GROUP_AVA.indexOf(RAW_SYC.MPG_ID) == -1) {
        MERCHANT_PRODUCT_GROUP_AVA.push(RAW_SYC.MPG_ID);
      }
    }

    var DATA_PRODUCT_GROUP = await this.merchantProductGroupRepository.find({where: {and: [{MPG_ID: {inq: MERCHANT_PRODUCT_GROUP_AVA}}]}});

    var DATAS: any; DATAS = {};
    for (var IPG in DATA_PRODUCT_GROUP) {
      var RAW_IGP = DATA_PRODUCT_GROUP[IPG];
      if (DATAS[RAW_IGP.M_ID] == undefined) {
        var MERCHANT = await this.merchantRepository.findById(RAW_IGP.M_ID);
        DATAS[RAW_IGP.M_ID] = {ID: MERCHANT.M_ID, LABEL: MERCHANT.M_LEGAL_NAME, DATA: {}}
      }

      if (DATAS[RAW_IGP.M_ID]["DATA"][RAW_IGP.MP_ID] == undefined) {
        var MERCHANT_PRODUCT = await this.merchantProductRepository.findById(RAW_IGP.MP_ID);
        DATAS[RAW_IGP.M_ID]["DATA"][RAW_IGP.MP_ID] = {ID: RAW_IGP.MP_ID, LABEL: MERCHANT_PRODUCT.MP_BRAND, DATA: {}, TARGET: []}
      }

      var ID_IGP = RAW_IGP.MPG_ID == undefined ? -1 : RAW_IGP.MPG_ID;
      if (DATAS[RAW_IGP.M_ID]["DATA"][RAW_IGP.MP_ID]["DATA"][ID_IGP] == undefined) {
        DATAS[RAW_IGP.M_ID]["DATA"][RAW_IGP.MP_ID]["DATA"][ID_IGP] = {ID: RAW_IGP.MPG_ID, LABEL: RAW_IGP.MPG_LABEL};
        DATAS[RAW_IGP.M_ID]["DATA"][RAW_IGP.MP_ID]["TARGET"].push(RAW_IGP.MPG_ID);
      }


    }
    result = {success: true, message: 'Succes !', datas: DATAS};
    return result;
  }


  //MENGAMBIL DATA PER GROUP
  @get('/merchant-sychronize-datasource/big-query/merhant-group/views', {
    responses: {
      '200': {
        description: 'Array of Merchant Product model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(MerchantProductDatastore, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async merchantSychBigQueryGroupView(
    @param.query.number("MPG_ID") MPG_ID: number,
    @param.query.string('PERIODE') PERIODE: string,
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};

    // if (MPG_ID == null) {result.success = false; result.message = "MPEG ID can't be null !"; return result;};
    // if (PERIODE == null) {result.success = false; result.message = "PERIODE can't be null !"; return result;};

    // if (TRANSACTION_TMP[USER_ID] == undefined) {
    //   TRANSACTION_TMP[USER_ID] = {};
    // }

    // if (TRANSACTION_TMP[USER_ID][PERIODE] == undefined) {
    //   TRANSACTION_TMP[USER_ID][PERIODE] = {};
    // }

    // if (TRANSACTION_TMP[USER_ID][PERIODE][MPG_ID] == undefined) {
    //   TRANSACTION_TMP[USER_ID][PERIODE][MPG_ID] = [];
    // }



    // var MERCHANT_GROUP_COUNT = await this.merchantProductGroupRepository.count({MPG_ID: MPG_ID});
    // if (MERCHANT_GROUP_COUNT.count > 0) {



    //   var MERCHANT_GROUP = await this.merchantProductGroupRepository.findById(MPG_ID);
    //   var MDR_AMOUNT = MERCHANT_GROUP.MPG_MDR_AMOUNT;
    //   var MDR_DPP = MERCHANT_GROUP.MPG_DPP;
    //   var MDR_PPN = MERCHANT_GROUP.MPG_PPN;
    //   var MDR_PPH = MERCHANT_GROUP.MPG_PPH;

    //   var VARIABLE_COUNT = await this.merchantProductVariableRepository.count({MPG_ID: MPG_ID});

    //   var FIELD_REQ: any; FIELD_REQ = {};

    //   var PERIODE_FILTER = "";

    //   var FIELD_DATASTORE: any; FIELD_DATASTORE = {}
    //   if (VARIABLE_COUNT.count > 0) {
    //     var VARIABLE = await this.merchantProductVariableRepository.find({where: {and: [{MPG_ID: MPG_ID}]}});
    //     for (var I_VAR in VARIABLE) {
    //       var INFO_VARIABLE = VARIABLE[I_VAR];

    //       var VARIABLE_ID = INFO_VARIABLE.MPV_ID == undefined ? -1 : INFO_VARIABLE.MPV_ID;
    //       if (FIELD_REQ[VARIABLE_ID] == undefined) {
    //         FIELD_REQ[VARIABLE_ID] = {};
    //         FIELD_DATASTORE[INFO_VARIABLE.MPV_LABEL] = "";
    //       }
    //       FIELD_REQ[VARIABLE_ID] = {KEY_ID: INFO_VARIABLE.MPV_TYPE_KEY_ID, TYPE: INFO_VARIABLE.MPV_TYPE_ID, KEY: INFO_VARIABLE.MPV_KEY, LABEL: INFO_VARIABLE.MPV_LABEL};


    //       //UNTUK PERIODE
    //       if (INFO_VARIABLE.MPV_TYPE_KEY_ID == 1) {
    //         if (PERIODE_FILTER == "") {
    //           PERIODE_FILTER = " " + INFO_VARIABLE.MPV_KEY + " BETWEEN  @START_DATE AND @END_DATE ";
    //         }
    //       }

    //     }
    //   }

    //   var PERIODE_DATA = PERIODE;
    //   var PERIODE_SPLIT = PERIODE_DATA.split("-");
    //   var TRANSAKSI_DATE = new Date(Number(PERIODE_SPLIT[0]), Number(PERIODE_SPLIT[1]), 0);
    //   var TRANSAKSI_MONTH = TRANSAKSI_DATE.getMonth() + 1;
    //   var END_DATE_TRANSAKSI = TRANSAKSI_DATE.getFullYear() + "-" + "00".substring(TRANSAKSI_MONTH.toString().length) + TRANSAKSI_MONTH + "-" + TRANSAKSI_DATE.getDate();
    //   var START_DATE_TRANSAKSI = TRANSAKSI_DATE.getFullYear() + "-" + "00".substring(TRANSAKSI_MONTH.toString().length) + TRANSAKSI_MONTH + "-" + "01";


    //   const BIG_QUERY = new BigQuery();
    //   var SQL_QUERY = MERCHANT_GROUP.MPG_QUERY.split("$FILTER_PERIODE").join(PERIODE_FILTER).split("$AND").join(" AND ").split("$OR").join(" OR ").split("$WHERE").join(" WHERE ");
    //   const OPTION_BIG_QUERY = {
    //     query: SQL_QUERY,
    //     params: {START_DATE: new BigQueryDate(START_DATE_TRANSAKSI), END_DATE: new BigQueryDate(END_DATE_TRANSAKSI)},
    //   }
    //   const [RESULT_BIG_QUERY] = await BIG_QUERY.query(OPTION_BIG_QUERY);

    //   var START_TIME_RUN = ControllerConfig.onCurrentTime().toString();
    //   var DATASOURCE: any; DATASOURCE = {"FIELD": {}, "RAW": []};

    //   if (RESULT_BIG_QUERY.length > 0) {
    //     //await this.merchantProductDatastoreRepository.deleteAll({and: [{TYPE_DATA: 1}, {PERIODE: PERIODE}, {MPG_ID: MPG_ID}]});
    //   }

    //   var TRANSACTION: any; TRANSACTION = [];
    //   for (var I_RESULT in RESULT_BIG_QUERY) {
    //     var RAW_BIGQUERY = RESULT_BIG_QUERY[I_RESULT];
    //     for (var ICLEAR_FIELD in FIELD_DATASTORE) {
    //       FIELD_DATASTORE[ICLEAR_FIELD] = "";
    //     }
    //     var DATA_ID = undefined;
    //     for (var VAR_ID in FIELD_REQ) {
    //       var RAW_VARIABLES = FIELD_REQ[VAR_ID];
    //       if (RAW_VARIABLES.KEY_ID == 6) {
    //         DATA_ID = RAW_BIGQUERY[RAW_VARIABLES.KEY];
    //       }
    //     }

    //     var RAW_BUILD: any; RAW_BUILD = {};
    //     for (var VAR_ID in FIELD_REQ) {
    //       var RAW_VARIABLES = FIELD_REQ[VAR_ID];
    //       if (RAW_VARIABLES.KEY_ID == 1) {
    //         if (FIELD_DATASTORE[RAW_VARIABLES.LABEL] != undefined) {
    //           FIELD_DATASTORE[RAW_VARIABLES.LABEL] = PERIODE;
    //         }
    //         continue;
    //       }

    //       if (FIELD_DATASTORE[RAW_VARIABLES.LABEL] != undefined) {
    //         FIELD_DATASTORE[RAW_VARIABLES.LABEL] = RAW_VARIABLES.TYPE == 4 ? RAW_BIGQUERY[RAW_VARIABLES.KEY]["value"] : RAW_BIGQUERY[RAW_VARIABLES.KEY];
    //       }

    //       var DATA_RAW: any; DATA_RAW = {};
    //       DATA_RAW["DATA_ID"] = DATA_ID;
    //       DATA_RAW["PERIODE"] = PERIODE;
    //       DATA_RAW["MPD_VALUE"] = RAW_VARIABLES.TYPE == 4 ? RAW_BIGQUERY[RAW_VARIABLES.KEY]["value"] : RAW_BIGQUERY[RAW_VARIABLES.KEY];
    //       DATA_RAW["MPD_TEXT"] = RAW_VARIABLES.TYPE == 4 ? RAW_BIGQUERY[RAW_VARIABLES.KEY]["value"] : RAW_BIGQUERY[RAW_VARIABLES.KEY];
    //       DATA_RAW["START_TIME_RUN"] = START_TIME_RUN;
    //       DATA_RAW["M_ID"] = MERCHANT_GROUP.M_ID;
    //       DATA_RAW["MP_ID"] = MERCHANT_GROUP.MP_ID;
    //       DATA_RAW["MPG_ID"] = MERCHANT_GROUP.MPG_ID;
    //       DATA_RAW["MPV_ID"] = Number(VAR_ID);
    //       DATA_RAW["AT_CREATE"] = ControllerConfig.onCurrentTime().toString();
    //       DATA_RAW["AT_FLAG"] = 1;
    //       DATA_RAW["AT_WHO"] = USER_ID;
    //       DATA_RAW["TYPE_DATA"] = 1;

    //       RAW_BUILD[RAW_VARIABLES.KEY] = RAW_VARIABLES.TYPE == 4 ? RAW_BIGQUERY[RAW_VARIABLES.KEY]["value"] : RAW_BIGQUERY[RAW_VARIABLES.KEY];
    //       DATASOURCE["FIELD"][RAW_VARIABLES.KEY] = {LABEL: RAW_VARIABLES.LABEL, TYPE: RAW_VARIABLES.TYPE};

    //       // await this.merchantProductDatastoreRepository.create(DATA_RAW);
    //     }
    //     TRANSACTION.push(RAW_BUILD);
    //   }

    //   DATASOURCE["RAW"] = TRANSACTION;

    //   result.success = true;
    //   result.message = "Success !";
    //   result.datas = DATASOURCE;

    // } else {
    //   result.success = false;
    //   result.message = "Error, merchant group not found !";
    // }

    return result;
  }

  @post('/merchant-sychronize-datasource/big-query/merhant-group/create', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: CreateSyncGroupRequestBody
          }
        },
      },
    },
  })
  async merchantSychBigQueryGroupCreate(
    @requestBody(CreateSyncGroupRequestBody) CreateSyncGroupRequestBody: {MPG_ID: [], PERIODE: string},
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};


    var GROUP = await this.merchantProductGroupRepository.find({where: {and: [{MPG_ID: {inq: CreateSyncGroupRequestBody.MPG_ID}}]}});
    if (GROUP.length == CreateSyncGroupRequestBody.MPG_ID.length && GROUP.length != 0) {
      var COUNT_PROCESS = await this.sycronizeProcessLogRepository.count({and: [{MPG_ID: {inq: CreateSyncGroupRequestBody.MPG_ID}}, {AT_FLAG: {inq: [0, 1]}}, {SPL_PERIODE: CreateSyncGroupRequestBody.PERIODE}]});
      if (COUNT_PROCESS.count == 0) {
        try {
          for (var i in GROUP) {
            var INFO_GROUP = GROUP[i];
            var countMaster = await this.merchantProductMasterRepository.count({and: [{AT_FLAG: {nin: [0]}}, {PERIODE: CreateSyncGroupRequestBody.PERIODE}, {M_ID: INFO_GROUP.M_ID}, {MP_ID: INFO_GROUP.MP_ID}]});
            if (countMaster.count == 0) {

              await this.merchantProductDatastoreRepository.deleteAll({and: [{PERIODE: CreateSyncGroupRequestBody.PERIODE}, {MP_ID: INFO_GROUP.MP_ID}, {M_ID: INFO_GROUP.M_ID}, {MPG_ID: INFO_GROUP.MPG_ID}, {TYPE_DATA: 1}]});
              await this.merchantProductSummaryRepository.deleteAll({and: [{PERIODE: CreateSyncGroupRequestBody.PERIODE}, {MP_ID: INFO_GROUP.MP_ID}, {M_ID: INFO_GROUP.M_ID}, {MPG_ID: INFO_GROUP.MPG_ID}]});
              await this.merchantProductMasterRepository.deleteAll({and: [{AT_FLAG: {nin: [0]}}, {PERIODE: CreateSyncGroupRequestBody.PERIODE}, {M_ID: INFO_GROUP.M_ID}, {MP_ID: INFO_GROUP.MP_ID}]});

              await this.sycronizeProcessLogRepository.create({
                SPL_PERIODE: CreateSyncGroupRequestBody.PERIODE,
                MP_ID: INFO_GROUP.MP_ID,
                M_ID: INFO_GROUP.M_ID,
                MPG_ID: INFO_GROUP.MPG_ID,
                SPL_COUNT_TRX: 0,
                SPL_TRX_AMOUNT: 0,
                SPL_MDR: 0,
                AT_CREATE: ControllerConfig.onCurrentTime().toString(),
                AT_UPDATE: undefined,
                AT_WHO: USER_ID,
                AT_FLAG: 0
              });
            }
          }
          this.ON_RUNNING_SYNC(null);
          result.success = true;
          result.message = "Success Create Process Sycronize  !";
        } catch (error) {
          result.success = false;
          result.message = error;
        }
      } else {
        result.success = false;
        result.message = "Error, merchant product group on process in periode " + CreateSyncGroupRequestBody.PERIODE + " !";
      }
    } else {
      result.success = false;
      result.message = "Error, merchant product group id not found !";
    }
    return result;
  }

  //============================================================================SYCRONIZE PROGRES
  @get('/merchant-sychronize-datasource/big-query/views', {
    responses: {
      '200': {
        description: 'Array of Sycronize BigQury model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(SycronizeProcessLog, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async findBigQuryViews(
    @param.filter(SycronizeProcessLog) filter?: Filter<SycronizeProcessLog>,
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};
    var FILTER_SET: any; FILTER_SET = {};
    var FIXFILTER: any; FIXFILTER = filter;
    for (var IFilter in FIXFILTER) {
      FILTER_SET[IFilter] = FIXFILTER[IFilter];
    }

    if (FILTER_SET.order == undefined) {
      FILTER_SET.order = "AT_CREATE ASC";
    }
    var SycronizeLog = await this.sycronizeProcessLogRepository.find(FILTER_SET);
    var DataProgresLog: any; DataProgresLog = {};
    for (var i in SycronizeLog) {
      var DATA_LOG = SycronizeLog[i];
      var GROUP_ID = DATA_LOG.SPL_PERIODE + "_" + DATA_LOG.M_ID + "_" + DATA_LOG.MP_ID;
      if (DataProgresLog[GROUP_ID] == undefined) {
        DataProgresLog[GROUP_ID] = {
          PERIODE: DATA_LOG.SPL_PERIODE,
          MERCHANT_ID: DATA_LOG.M_ID,
          PRODUCT_ID: DATA_LOG.MP_ID,
          MERCHANT: await this.merchantRepository.findById(DATA_LOG.M_ID, {
            fields: {
              M_ID: true,
              M_CODE: true,
              M_NAME: true,
              M_LEGAL_NAME: true,
            }
          }),

          MERCHANT_PRODUCT: await this.merchantProductRepository.findById(DATA_LOG.MP_ID, {
            fields: {
              MP_ID: true,
              MP_CODE: true,
              MP_BRAND: true,
            }
          }),
          AT_FLAG_HISTORY: [],
          PROCESS_LOG: [],
          ON_PROGRES: 0,
          SPL_COUNT_TRX: 0,
          SPL_TRX_AMOUNT: 0,
          SPL_MDR: 0,
          PROGRES: 0,
          AT_CREATE: "",
          AT_UPDATE: "",
          AT_FLAG: 0
        };
      }


      var data: any; data = {};
      data["SPL_ID"] = DATA_LOG.SPL_ID;
      data["SPL_PERIODE"] = DATA_LOG.SPL_PERIODE;
      data["SPL_COUNT_TRX"] = DATA_LOG.SPL_COUNT_TRX;
      data["SPL_TRX_AMOUNT"] = DATA_LOG.SPL_TRX_AMOUNT;
      data["SPL_MDR"] = DATA_LOG.SPL_MDR;
      data["PROGRES"] = 0;
      data["AT_CREATE"] = DATA_LOG.AT_CREATE;
      data["AT_UPDATE"] = DATA_LOG.AT_UPDATE;
      data["AT_FLAG"] = DATA_LOG.AT_FLAG;

      data["MERCHANT_GROUP"] = await this.merchantProductGroupRepository.findById(DATA_LOG.MPG_ID, {
        fields: {
          MPG_ID: true,
          MPG_LABEL: true
        }
      });

      if (DATA_LOG.AT_FLAG != 2) {
        DataProgresLog[GROUP_ID]["ON_PROGRES"] = 1;
        DataProgresLog[GROUP_ID]["SPL_COUNT_TRX"] = DATA_LOG.SPL_COUNT_TRX;
        DataProgresLog[GROUP_ID]["SPL_TRX_AMOUNT"] = DATA_LOG.SPL_TRX_AMOUNT;
        DataProgresLog[GROUP_ID]["SPL_MDR"] = DATA_LOG.SPL_MDR;
        DataProgresLog[GROUP_ID]["AT_CREATE"] = DATA_LOG.AT_CREATE;
        DataProgresLog[GROUP_ID]["AT_UPDATE"] = DATA_LOG.AT_UPDATE;
        DataProgresLog[GROUP_ID]["AT_FLAG"] = DATA_LOG.AT_FLAG;
      }

      if (DataProgresLog[GROUP_ID]["ON_PROGRES"] == 0) {
        DataProgresLog[GROUP_ID]["SPL_COUNT_TRX"] = DATA_LOG.SPL_COUNT_TRX;
        DataProgresLog[GROUP_ID]["SPL_TRX_AMOUNT"] = DATA_LOG.SPL_TRX_AMOUNT;
        DataProgresLog[GROUP_ID]["SPL_MDR"] = DATA_LOG.SPL_MDR;
        DataProgresLog[GROUP_ID]["AT_CREATE"] = DATA_LOG.AT_CREATE;
        DataProgresLog[GROUP_ID]["AT_UPDATE"] = DATA_LOG.AT_UPDATE;
        DataProgresLog[GROUP_ID]["AT_FLAG"] = DATA_LOG.AT_FLAG;
      }


      DataProgresLog[GROUP_ID]["AT_FLAG_HISTORY"].push(DATA_LOG.AT_FLAG);
      DataProgresLog[GROUP_ID]["PROCESS_LOG"].push(data);

    }

    var DATAS_RESULT: any; DATAS_RESULT = [];
    for (var i in DataProgresLog) {
      DATAS_RESULT.push(DataProgresLog[i]);
    }

    result = {success: true, message: 'Success !', datas: DATAS_RESULT};
    return result;

  }


  @post('/merchant-sychronize-datasource/big-query/running-log', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: CreateSyncRunningLogBody
          }
        },
      },
    },
  })
  async sychBigQueryRunLog(
    @requestBody(CreateSyncRunningLogBody) CreateSyncGroupRequestBody: {SPL_ID: []},
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};
    if (CreateSyncGroupRequestBody.SPL_ID.length == 0) {
      this.ON_RUNNING_SYNC(null);
    } else {
      this.ON_RUNNING_SYNC(CreateSyncGroupRequestBody.SPL_ID);
    }
    return result;
  }


  // @post('/merchant-sychronize-datasource/big-query/merhant-group/create-datastore', {
  //   responses: {
  //     '200': {
  //       description: 'DataDigipos model instance',
  //       content: {
  //         'application/json': {
  //           schema: CreateSyncGroupRequestBody
  //         }
  //       },
  //     },
  //   },
  // })
  // async merchantSychBigQueryGroupCreateDatastore(
  //   @requestBody(CreateSyncGroupRequestBody) CreateSyncGroupRequestBody: {MPG_ID: [], PERIODE: string},
  // ): Promise<Object> {
  //   var USER_ID = this.currentUserProfile[securityId];
  //   var result: any; result = {success: true, message: '', datas: []};

  //   var html = createHTML({
  //     title: 'PDF BAR',
  //     // script: 'example.js',
  //     scriptAsync: true,
  //     // css: 'example.css',
  //     lang: 'en',
  //     dir: 'rtl',
  //     head: '<meta name="description" content="example">',
  //     body: '<div id="pageHeader">Default header</div><p>example</p><div id="pageFooter">Default footer</div>',
  //     // favicon: 'favicon.png'
  //   });
  //   fs.writeFile(path.join(__dirname, '../../.digipost/TEMPLATE_BAR.html'), html, function (err: any) {
  //     if (err) console.log(err)
  //   })

  //   return result;
  // }
  // @post('/merchant-sychronize-datasource/big-query/merhant-group/create-datastore', {
  //   responses: {
  //     '200': {
  //       description: 'DataDigipos model instance',
  //       content: {
  //         'application/json': {
  //           schema: CreateSyncGroupRequestBody
  //         }
  //       },
  //     },
  //   },
  // })
  // async merchantSychBigQueryGroupCreateDatastore(
  //   @requestBody(CreateSyncGroupRequestBody) CreateSyncGroupRequestBody: {MPG_ID: [], PERIODE: string},
  // ): Promise<Object> {
  //   var USER_ID = this.currentUserProfile[securityId];
  //   var result: any; result = {success: true, message: '', datas: []};
  //   try {
  //     var MPG_STORE = CreateSyncGroupRequestBody.MPG_ID;
  //     var PERIODE = CreateSyncGroupRequestBody.PERIODE;

  //     if (MPG_STORE.length > 0) {
  //       for (var i in MPG_STORE) {
  //         await this.merchantProductSummaryRepository.deleteAll({and: [{PERIODE: PERIODE}, {MPG_ID: MPG_STORE[i]}]});
  //         await this.merchantProductMasterRepository.deleteAll({and: [{PERIODE: PERIODE}, {MPG_ID: MPG_STORE[i]}]});
  //         await this.merchantProductDatastoreRepository.deleteAll({and: [{PERIODE: PERIODE}, {MPG_ID: MPG_STORE[i]}]});
  //       }
  //     }

  //     var START_TIME = ControllerConfig.onCurrentTime().toString();
  //     if (MPG_STORE.length > 0) {
  //       for (var i in MPG_STORE) {
  //         if (TRANSACTION_TMP[USER_ID] != undefined) {
  //           if (TRANSACTION_TMP[USER_ID][PERIODE] != undefined) {
  //             if (TRANSACTION_TMP[USER_ID][PERIODE][MPG_STORE[i]] != undefined) {
  //               var SUMMARY_GROUP: any; SUMMARY_GROUP = {};
  //               var MPG_INFO = await this.merchantProductGroupRepository.findById(MPG_STORE[i]);
  //               if (MPG_INFO.MPG_ID != undefined) {
  //                 if (TRANSACTION_TMP[USER_ID][PERIODE][MPG_STORE[i]].length > 0) {
  //                   for (var I_SUM in TRANSACTION_TMP[USER_ID][PERIODE][MPG_STORE[i]]) {
  //                     var COUNT_SUMMARY = await this.merchantProductMasterRepository.count({and: [{PERIODE: PERIODE}, {MPG_ID: MPG_STORE[i]}, {AT_FLAG: {nin: [0]}}]});
  //                     if (COUNT_SUMMARY.count == 0) {
  //                       var RAW_DATA = TRANSACTION_TMP[USER_ID][PERIODE][MPG_STORE[i]][I_SUM];

  //                       console.log(RAW_DATA);
  //                       try {
  //                         await this.merchantProductDatastoreRepository.create(RAW_DATA);
  //                         result.success = true;
  //                         result.message = "Success !";
  //                       } catch (error) {
  //                         await this.merchantProductDatastoreRepository.deleteAll({and: [{START_TIME_RUN: RAW_DATA.START_TIME_RUN}]});
  //                         result.success = false;
  //                         result.message = error;
  //                         break;
  //                       }
  //                     } else {
  //                       result.success = false;
  //                       result.message = "Error, data merchant group have summary !";
  //                       return result;
  //                     }
  //                   }
  //                 } else {
  //                   result.success = false;
  //                   result.message = "Error, data merchant group not found !";
  //                   return result;
  //                 }

  //               } else {
  //                 result.success = false;
  //                 result.message = "Error, data merchant group summary is empty !";
  //                 return result;
  //               }
  //             } else {
  //               result.success = false;
  //               result.message = "Error, data merchant group not found !";
  //               return result;
  //             }
  //           } else {
  //             result.success = false;
  //             result.message = "Error, data merchant group periode " + PERIODE + " is empty !";
  //             return result;
  //           }
  //         } else {
  //           result.success = false;
  //           result.message = "Error, data merchant group  is empty !";
  //           return result;
  //         }
  //       }
  //     } else {
  //       result.success = false;
  //       result.message = "Error, MPG_ID is empty !";
  //     }
  //   } catch (error) {
  //     result.success = false;
  //     result.message = "MPG_ID must be array [1,2,...]";
  //   }

  //   return result;
  // }



}
