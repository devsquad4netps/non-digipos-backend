import {authenticate} from '@loopback/authentication';
import {inject} from '@loopback/core';
import {Filter, FilterExcludingWhere, repository} from '@loopback/repository';
import {get, getModelSchemaRef, param, post, requestBody} from '@loopback/rest';
import {SecurityBindings, securityId, UserProfile} from '@loopback/security';
import {MerchantProductVariable} from '../models';
import {MerchantProductDatastoreRepository, MerchantProductGroupRepository, MerchantProductVariableRepository, MpvTypeKeyRepository, MpvTypeRepository} from '../repositories';
import {MerchantProductRepository} from '../repositories/merchant-product.repository';
import {MerchantRepository} from '../repositories/merchant.repository';
import {UserRepository} from '../repositories/user.repository';
import {ControllerConfig} from './controller.config';



@authenticate('jwt')
export class MerchantVariableActionController {
  constructor(
    @repository(MerchantRepository)
    public merchantRepository: MerchantRepository,
    @repository(MerchantProductRepository)
    public merchantProductRepository: MerchantProductRepository,
    @repository(MerchantProductGroupRepository)
    public merchantProductGroupRepository: MerchantProductGroupRepository,
    @repository(MerchantProductVariableRepository)
    public merchantProductVariableRepository: MerchantProductVariableRepository,
    @repository(MerchantProductDatastoreRepository)
    public merchantProductDatastoreRepository: MerchantProductDatastoreRepository,
    @repository(MpvTypeRepository)
    public mpvTypeRepository: MpvTypeRepository,
    @repository(MpvTypeKeyRepository)
    public mpvTypeKeyRepository: MpvTypeKeyRepository,
    @inject(SecurityBindings.USER)
    public currentUserProfile: UserProfile,
    @repository(UserRepository)
    public userRepository: UserRepository,
  ) { }


  //GET ALL MERCHANT PRODUCT DATA
  @get('/merchant-product-variable-action/{MP_ID}/views', {
    responses: {
      '200': {
        description: 'Array of Merchant Product variable model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(MerchantProductVariable, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async findMerchantProductVariable(
    @param.path.number('MP_ID') id: number,
    @param.filter(MerchantProductVariable) filter?: Filter<MerchantProductVariable>,
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};

    var MERCHANT_GROUP_COUNT = await this.merchantProductRepository.count({MP_ID: id});
    if (MERCHANT_GROUP_COUNT.count > 0) {
      var MERCHANT_PRODUCT = await this.merchantProductRepository.findById(id);
      var MERCHANT_VARIABLE: any; MERCHANT_VARIABLE = await this.merchantProductVariableRepository.find({
        where: {and: [{MP_ID: id}]}, fields: {
          MPV_ID: true,
          MPV_KEY: true,
          MPV_LABEL: true,
          MPV_TYPE_ID: true,
          MPV_TYPE_KEY_ID: true,
          M_ID: true,
          MP_ID: true,
          AT_FLAG: true,
        }
      });

      var VARIABLE_STORE: any; VARIABLE_STORE = {
        MP_CODE: MERCHANT_PRODUCT.MP_CODE,
        MP_BRAND: MERCHANT_PRODUCT.MP_BRAND,
        MP_LABEL: MERCHANT_PRODUCT.MP_LABEL,
        MP_DESCRIPTION: MERCHANT_PRODUCT.MP_DESCRIPTION,
        MERCHANT_PRODUCT_VARIABLE: []
      };

      for (var i in MERCHANT_VARIABLE) {
        var RAW: any; RAW = {};
        var INFO_VARIABLE: any; INFO_VARIABLE = MERCHANT_VARIABLE[i];
        for (var j in INFO_VARIABLE) {
          RAW[j] = INFO_VARIABLE[j];
        }

        RAW["MERCHANT"] = await this.merchantRepository.findById(RAW.M_ID, {
          fields: {
            M_ID: true,
            M_CODE: true,
            M_NAME: true,
            M_LEGAL_NAME: true,
          }
        });

        RAW["MERCHANT_PRODUCT"] = await this.merchantProductRepository.findById(RAW.MP_ID, {
          fields: {
            MP_ID: true,
            MP_CODE: true,
            MP_BRAND: true,
          }
        });

        // RAW["MERCHANT_PRODUCT_GROUP"] = await this.merchantProductGroupRepository.findById(RAW.MPG_ID, {
        //   fields: {
        //     MPG_ID: true,
        //     MPG_LABEL: true
        //   }
        // });

        RAW["MPV_TYPE"] = await this.mpvTypeRepository.findById(RAW.MPV_TYPE_ID, {
          fields: {
            MPV_TYPE_ID: true,
            MPV_TYPE_LABEL: true
          }
        });

        RAW["MPV_TYPE_KEY"] = await this.mpvTypeKeyRepository.findById(RAW.MPV_TYPE_KEY_ID, {
          fields: {
            MPV_TYPE_KEY_ID: true,
            MPV_TYPE_KEY_LABEL: true
          }
        });



        VARIABLE_STORE.MERCHANT_PRODUCT_VARIABLE.push(RAW);
      }
      result.success = true;
      result.message = "Success !";
      result.datas = VARIABLE_STORE;
    } else {
      result.success = false;
      result.message = "Error, merchant product " + id + " not found !";
    }

    return result;
  }

  @get('/merchant-product-variable-action/view/{id}', {
    responses: {
      '200': {
        description: 'Array of Merchant Product variable model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(MerchantProductVariable, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async findMerchantProductVariableById(
    @param.path.number('id') id: number,
    @param.filter(MerchantProductVariable, {exclude: 'where'}) filter?: FilterExcludingWhere<MerchantProductVariable>
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};

    var MPG_COUNT = await this.merchantProductVariableRepository.count({MPV_ID: id});

    if (MPG_COUNT.count > 0) {
      var infoMerchantProductVariable: any; infoMerchantProductVariable = await this.merchantProductVariableRepository.findById(id, {
        fields: {
          MPV_ID: true,
          MPV_KEY: true,
          MPV_LABEL: true,
          MPV_TYPE_ID: true,
          MPV_TYPE_KEY_ID: true,
          M_ID: true,
          MP_ID: true,
          AT_FLAG: true,
        }
      });
      var RAW: any; RAW = {};
      for (var key in infoMerchantProductVariable) {
        RAW[key] = infoMerchantProductVariable[key];
      }

      RAW["MERCHANT"] = await this.merchantRepository.findById(infoMerchantProductVariable.M_ID, {
        fields: {
          M_ID: true,
          M_CODE: true,
          M_NAME: true,
          M_LEGAL_NAME: true,
        }
      });

      RAW["MERCHANT_PRODUCT"] = await this.merchantProductRepository.findById(infoMerchantProductVariable.MP_ID, {
        fields: {
          MP_ID: true,
          MP_CODE: true,
          MP_BRAND: true,
        }
      });

      // RAW["MERCHANT_PRODUCT_GROUP"] = await this.merchantProductGroupRepository.findById(infoMerchantProductVariable.MPG_ID, {
      //   fields: {
      //     MPG_ID: true,
      //     MPG_LABEL: true
      //   }
      // });

      RAW["MPV_TYPE"] = await this.mpvTypeRepository.findById(infoMerchantProductVariable.MPV_TYPE_ID, {
        fields: {
          MPV_TYPE_ID: true,
          MPV_TYPE_LABEL: true
        }
      });

      RAW["MPV_TYPE_KEY"] = await this.mpvTypeKeyRepository.findById(infoMerchantProductVariable.MPV_TYPE_KEY_ID, {
        fields: {
          MPV_TYPE_KEY_ID: true,
          MPV_TYPE_KEY_LABEL: true
        }
      });

      result.success = true;
      result.datas = RAW;
      result.message = "Success !";
    } else {
      result.success = false;
      result.message = "Error, Merchant product variable not found !";
      result.datas = [];
    }
    return result;
  }


  @post('/merchant-product-variable-action/create', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {'application/json': {schema: getModelSchemaRef(MerchantProductVariable)}},
      },
    },
  })
  async createMerchantProductVariable(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(MerchantProductVariable, {
            title: 'VARIABLE_CREATE',
            exclude: ['MPV_ID', 'AT_CREATE', 'AT_UPDATE', 'AT_WHO'],
          }),
        },
      },
    })
    dataMerchantProductVariable: any,
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};

    var REQUIRED_FIELDS = ['MPV_KEY', 'MPV_LABEL', 'MPV_TYPE_ID', 'MPV_TYPE_KEY_ID', 'M_ID', 'MP_ID', 'AT_FLAG'];
    var BODY_PARAM: any; BODY_PARAM = {};
    for (var i in dataMerchantProductVariable) {
      if (REQUIRED_FIELDS.indexOf(i) != -1) {
        REQUIRED_FIELDS.splice(REQUIRED_FIELDS.indexOf(i), 1);
      }
      BODY_PARAM[i] = dataMerchantProductVariable[i];
    }

    if (REQUIRED_FIELDS.length == 0) {
      var MERCHANT = await this.merchantRepository.count({M_ID: BODY_PARAM["M_ID"]});
      var MERCHANT_PRODUCT = await this.merchantProductRepository.count({and: [{MP_ID: BODY_PARAM["MP_ID"]}, {M_ID: BODY_PARAM["M_ID"]}]});
      // var MERCHANT_PRODUCT_GROUP = await this.merchantProductGroupRepository.count({and: [{MPG_ID: BODY_PARAM["MPG_ID"]}, {MP_ID: BODY_PARAM["MP_ID"]}, {M_ID: BODY_PARAM["M_ID"]}]});
      var MVP_TYPE = await this.mpvTypeRepository.count({MPV_TYPE_ID: BODY_PARAM["MPV_TYPE_ID"]});
      var MVP_TYPE_KEY = await this.mpvTypeKeyRepository.count({MPV_TYPE_KEY_ID: BODY_PARAM["MPV_TYPE_KEY_ID"]});

      if (MERCHANT.count > 0) {
        if (MERCHANT_PRODUCT.count > 0) {
          // if (MERCHANT_PRODUCT_GROUP.count > 0) {
          if (MVP_TYPE.count > 0) {
            if (MVP_TYPE_KEY.count > 0) {
              BODY_PARAM["AT_CREATE"] = ControllerConfig.onCurrentTime().toString();
              BODY_PARAM["AT_UPDATE"] = undefined;
              BODY_PARAM["AT_WHO"] = USER_ID;

              try {
                var PRODUCT_VARIABLE = await this.merchantProductVariableRepository.create(BODY_PARAM);
                result.success = true;
                result.message = "Success create merchant product variable !";
                result.datas = PRODUCT_VARIABLE;
              } catch (error) {
                result.success = false;
                result.message = error;
              }

            } else {
              result.success = false;
              result.message = "Error, merchant variable type key " + BODY_PARAM["MPV_TYPE_KEY_ID"] + " not found !";
            }
          } else {
            result.success = false;
            result.message = "Error, merchant variable type " + BODY_PARAM["MPV_TYPE_ID"] + " not found !";
          }
          // } else {
          //   result.success = false;
          //   result.message = "Error, merchant product group " + BODY_PARAM["MP_ID"] + " not found !";
          // }
        } else {
          result.success = false;
          result.message = "Error, merchant product " + BODY_PARAM["MP_ID"] + " not found !";
        }
      } else {
        result.success = false;
        result.message = "Error, merchant " + BODY_PARAM["M_ID"] + " not found !";
      }
    } else {
      result.success = false;
      result.message = "Error, Field " + JSON.stringify(REQUIRED_FIELDS) + " must entry !";
    }
    return result;
  }

  @post('/merchant-product-variable-action/update/{id}', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {'application/json': {schema: getModelSchemaRef(MerchantProductVariable)}},
      },
    },
  })
  async updateMerchantProductVariable(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(MerchantProductVariable, {
            title: 'VARIABLE_UPDATE',
            exclude: ['MPV_ID', 'AT_CREATE', 'AT_UPDATE', 'AT_WHO'],
          }),
        },
      },
    })
    dataMerchantProductVariable: any,
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};

    var REQUIRED_FIELDS = ['MPV_KEY', 'MPV_LABEL', 'MPV_TYPE_ID', 'MPV_TYPE_KEY_ID', 'M_ID', 'MP_ID', 'AT_FLAG'];
    var BODY_PARAM: any; BODY_PARAM = {};
    for (var i in dataMerchantProductVariable) {
      if (REQUIRED_FIELDS.indexOf(i) != -1) {
        REQUIRED_FIELDS.splice(REQUIRED_FIELDS.indexOf(i), 1);
      }
      BODY_PARAM[i] = dataMerchantProductVariable[i];
    }

    if (REQUIRED_FIELDS.length == 0) {
      var MERCHANT = await this.merchantRepository.count({M_ID: BODY_PARAM["M_ID"]});
      var MERCHANT_PRODUCT = await this.merchantProductRepository.count({and: [{MP_ID: BODY_PARAM["MP_ID"]}, {M_ID: BODY_PARAM["M_ID"]}]});
      var MERCHANT_PRODUCT_GROUP = await this.merchantProductGroupRepository.count({and: [{MPG_ID: BODY_PARAM["MPG_ID"]}, {MP_ID: BODY_PARAM["MP_ID"]}, {M_ID: BODY_PARAM["M_ID"]}]});
      var MVP_TYPE = await this.mpvTypeRepository.count({MPV_TYPE_ID: BODY_PARAM["MPV_TYPE_ID"]});
      var MVP_TYPE_KEY = await this.mpvTypeKeyRepository.count({MPV_TYPE_KEY_ID: BODY_PARAM["MPV_TYPE_KEY_ID"]});

      if (MERCHANT.count > 0) {
        if (MERCHANT_PRODUCT.count > 0) {
          // if (MERCHANT_PRODUCT_GROUP.count > 0) {
          if (MVP_TYPE.count > 0) {
            if (MVP_TYPE_KEY.count > 0) {

              BODY_PARAM["AT_UPDATE"] = ControllerConfig.onCurrentTime().toString();
              BODY_PARAM["AT_WHO"] = USER_ID;

              try {
                var PRODUCT_VARIABLE = await this.merchantProductVariableRepository.updateById(id, BODY_PARAM);
                result.success = true;
                result.message = "Success update merchant product variable !";
                result.datas = PRODUCT_VARIABLE;
              } catch (error) {
                result.success = false;
                result.message = error;
              }

            } else {
              result.success = false;
              result.message = "Error, merchant variable type key " + BODY_PARAM["MPV_TYPE_KEY_ID"] + " not found !";
            }
          } else {
            result.success = false;
            result.message = "Error, merchant variable type " + BODY_PARAM["MPV_TYPE_ID"] + " not found !";
          }
          // } else {
          //   result.success = false;
          //   result.message = "Error, merchant product group " + BODY_PARAM["MP_ID"] + " not found !";
          // }
        } else {
          result.success = false;
          result.message = "Error, merchant product " + BODY_PARAM["MP_ID"] + " not found !";
        }
      } else {
        result.success = false;
        result.message = "Error, merchant " + BODY_PARAM["M_ID"] + " not found !";
      }
    } else {
      result.success = false;
      result.message = "Error, Field " + JSON.stringify(REQUIRED_FIELDS) + " must entry !";
    }
    return result;
  }

  @post('/merchant-product-variable-action/delete/{id}', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {'application/json': {schema: getModelSchemaRef(MerchantProductVariable)}},
      },
    },
  })
  async deleteMerchantProductVariable(
    @param.path.number('id') id: number
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};

    var PRODUCT_DATASTORE = await this.merchantProductDatastoreRepository.count({MPV_ID: id});
    if (PRODUCT_DATASTORE.count == 0) {
      try {
        await this.merchantProductVariableRepository.deleteById(id);
        result.success = true;
        result.message = "Success delete merchant product variable !";
      } catch (error) {
        result.success = false;
        result.message = error;
      }
    } else {
      result.success = false;
      result.message = "Error, merchant product variable has used !";
    }

    return result;
  }



}
