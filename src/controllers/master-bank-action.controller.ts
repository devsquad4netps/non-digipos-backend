// Uncomment these imports to begin using these cool features!

// import {inject} from '@loopback/core';
import {authenticate} from '@loopback/authentication';
import {inject} from '@loopback/core';
import {Filter, FilterExcludingWhere, repository} from '@loopback/repository';
import {get, getModelSchemaRef, param, post, requestBody} from '@loopback/rest';
import {SecurityBindings, securityId, UserProfile} from '@loopback/security';
import {MasterBank, MerchantRekening} from '../models';
import {MasterBankRepository} from '../repositories';
import {UserRepository} from '../repositories/user.repository';
import {ControllerConfig} from './controller.config';


@authenticate('jwt')
export class MasterBankActionController {
  constructor(
    @repository(MasterBankRepository)
    public masterBankRepository: MasterBankRepository,
    @inject(SecurityBindings.USER)
    public currentUserProfile: UserProfile,
    @repository(UserRepository)
    public userRepository: UserRepository,
  ) { }

  @post('/master-bank/create', {
    responses: {
      '200': {
        description: 'Master Bank Create model instance',
        content: {'application/json': {schema: getModelSchemaRef(MasterBank)}},
      },
    },
  })
  async createMasterBank(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(MasterBank, {
            title: 'NewBankMaster',
            exclude: ['ID', 'AT_FLAG', 'AT_CREATE', 'AT_UPDATE', 'AT_WHO'],
          }),
        },
      },
    })
    MASTER_BANK_BODY: Omit<MasterBank, 'ID'>,
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};

    MASTER_BANK_BODY["AT_FLAG"] = 1;
    MASTER_BANK_BODY["AT_CREATE"] = ControllerConfig.onCurrentTime().toString();
    MASTER_BANK_BODY["AT_UPDATE"] = undefined;
    MASTER_BANK_BODY["AT_WHO"] = USER_ID;

    try {
      var CREATE_REKENING = await this.masterBankRepository.create(MASTER_BANK_BODY);
      result.success = true;
      result.message = "Success !";
      result.datas = CREATE_REKENING;
    } catch (error) {
      result.success = false;
      result.message = error;
    }

    return result;
  }

  @post('/master-bank/update/{id}', {
    responses: {
      '200': {
        description: 'Master Bank update Class Update model instance',
        content: {'application/json': {schema: getModelSchemaRef(MasterBank)}},
      },
    },
  })
  async updateMasterBank(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(MasterBank, {
            title: 'UpdateBankMaster',
            exclude: ['ID', 'AT_FLAG', 'AT_CREATE', 'AT_UPDATE', 'AT_WHO'],
          }),
        },
      },
    })
    MASTER_BANK_BODY: MasterBank,
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};


    var BANK_COUNT = await this.masterBankRepository.count({and: [{ID: id}]});
    if (BANK_COUNT.count == 1) {
      MASTER_BANK_BODY["AT_UPDATE"] = ControllerConfig.onCurrentTime().toString();
      MASTER_BANK_BODY["AT_WHO"] = USER_ID;

      try {
        await this.masterBankRepository.updateById(id, MASTER_BANK_BODY);
        result.success = true;
        result.message = "Success update BANK class !";
      } catch (error) {
        result.success = false;
        result.message = error;
      }
    } else {
      result.success = false;
      result.message = "Error, BANK not found !";
    }
    return result;
  }

  @get('/master-bank/views', {
    responses: {
      '200': {
        description: 'Master Bank Views type model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(MasterBank, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findMasterBank(
    @param.filter(MasterBank) filter?: Filter<MasterBank>,
  ): Promise<Object> {
    var result: any; result = {success: true, message: '', datas: []};
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};


    var MASTER_BANK = await this.masterBankRepository.find(filter);
    var DATAS: any; DATAS = [];
    for (var i in MASTER_BANK) {
      var RAW_BANK = MASTER_BANK[i];


      var DATA: any; DATA = {
        ID: RAW_BANK.ID,
        NAME_BANK: RAW_BANK.NAME_BANK,
        CODE_BANK: RAW_BANK.CODE_BANK,
        AT_FLAG: RAW_BANK.AT_FLAG,
      };
      DATAS.push(DATA);
    }

    result.success = true;
    result.datas = DATAS;
    result.message = "Success !";

    return result
  }

  @get('/master-bank/view/{id}', {
    responses: {
      '200': {
        description: 'Master Bank View type model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(MasterBank, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findMasterBankById(
    @param.path.number('id') id: number,
    @param.filter(MasterBank, {exclude: 'where'}) filter?: FilterExcludingWhere<MerchantRekening>
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: [], count: 0};

    var BANK_COUNT = await this.masterBankRepository.count({and: [{ID: id}]});
    if (BANK_COUNT.count == 1) {
      var RAW_BANK = await this.masterBankRepository.findById(id, filter);
      result.success = true;
      result.datas = RAW_BANK;
      result.count = 1;
      result.message = "Success !";
    } else {
      result.success = false;
      result.message = "Error, merchant not found !";
    }
    return result
  }

  @post('/master-bank/delete/{id}', {
    responses: {
      '200': {
        description: 'Delete Master Bank Class  model instance',
        content: {'application/json': {schema: getModelSchemaRef(MasterBank)}},
      },
    },
  })
  async deleteMerchantRekening(
    @param.path.number('id') id: number,
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};


    var BANK_COUNT = await this.masterBankRepository.count({and: [{ID: id}]});
    if (BANK_COUNT.count == 1) {
      try {
        await this.masterBankRepository.deleteById(id);
        result.success = true;
        result.message = "Success delete Bank !";
      } catch (error) {
        result.success = false;
        result.message = error;
      }
    } else {
      result.success = false;
      result.message = "Error, Bank not found !";
    }
    return result;
  }


}
