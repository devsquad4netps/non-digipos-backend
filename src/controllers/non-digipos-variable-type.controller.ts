// // Uncomment these imports to begin using these cool features!

// import {Filter, FilterExcludingWhere, repository} from '@loopback/repository';
// import {get, getModelSchemaRef, param} from '@loopback/rest';
// import {MerchantVariableType} from '../models';
// import {MerchantVariableTypeRepository} from '../repositories';

// // import {inject} from '@loopback/core';


// export class NonDigiposVariableTypeController {
//   constructor(
//     @repository(MerchantVariableTypeRepository)
//     public merchantVariableTypeRepository: MerchantVariableTypeRepository,
//   ) { }

//   @get('/non-digipos-variable-type', {
//     responses: {
//       '200': {
//         description: 'Array of non digipos variable type model instances',
//         content: {
//           'application/json': {
//             schema: {
//               type: 'array',
//               items: getModelSchemaRef(MerchantVariableType, {includeRelations: true}),
//             },
//           },
//         },
//       },
//     },
//   })
//   async find(
//     @param.filter(MerchantVariableType) filter?: Filter<MerchantVariableType>,
//   ): Promise<MerchantVariableType[]> {
//     return this.merchantVariableTypeRepository.find(filter);
//   }

//   @get('/non-digipos-variable-type/{id}', {
//     responses: {
//       '200': {
//         description: 'non digipos variable type model instance',
//         content: {
//           'application/json': {
//             schema: getModelSchemaRef(MerchantVariableType, {includeRelations: true}),
//           },
//         },
//       },
//     },
//   })
//   async findById(
//     @param.path.number('id') id: number,
//     @param.filter(MerchantVariableType, {exclude: 'where'}) filter?: FilterExcludingWhere<MerchantVariableType>
//   ): Promise<MerchantVariableType> {
//     return this.merchantVariableTypeRepository.findById(id, filter);
//   }



// }
