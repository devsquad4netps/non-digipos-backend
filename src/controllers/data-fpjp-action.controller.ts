import {authenticate} from '@loopback/authentication';
import {inject} from '@loopback/core';
import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where
} from '@loopback/repository';
import {
  del, get,
  getModelSchemaRef, param,


  patch, post,




  put,

  requestBody
} from '@loopback/rest';
import {SecurityBindings, securityId, UserProfile} from '@loopback/security';
import ejs from "ejs";
import fs from 'fs';
import pdf from "html-pdf";
import path from 'path';
import randomstring from "randomstring";
import {DataFpjp} from '../models';
import {AdMasterRepository, ApprovalMatrixMemberRepository, ApprovalMatrixRepository, ApprovalNotaRepository, DataconfigRepository, DataDigiposRejectRepository, DataFpjpRepository, DataNotificationRepository, HistoryFpjpNoteRepository, HistoryFpjpRejectRepository, MailSenderMessageRepository, UserRepository} from '../repositories';
import {ControllerConfig} from './controller.config';
require('dotenv').config();


function pad2(n: Number) {  // always returns a string
  return (n < 10 ? '0' : '') + n;
}

function dateFormat(values: Date) {
  return values.getFullYear() + "-" +
    pad2(values.getMonth() + 1) + "-" +
    pad2(values.getDate()) + " " +
    pad2(values.getHours()) + ":" +
    pad2(values.getMinutes()) + ":" +
    pad2(values.getSeconds());
}


export const FpjpUserRejectRequestBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['reason_id', 'id', 'description'],
        properties: {
          reason_id: {
            type: 'number',
          },
          id: {
            type: 'number',
          },
          description: {
            type: 'string',
          }
        }
      }
    },
  },
};


export const FpjpDataRejectRequestBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['reason_id', 'fpjp_id', 'description'],
        properties: {
          reason_id: {
            type: 'number',
          },
          fpjp_id: {
            type: 'array',
            items: {type: 'number'},
          },
          description: {
            type: 'string',
          }
        }
      }
    },
  },
};

export const FpjpNoAllRequestBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['periode', 'type_trx'],
        properties: {
          periode: {
            type: 'string',
          },
          type_trx: {
            type: 'number',
          },
        }
      }
    },
  },
};


export const FpjpValidateRequestBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['fpjp'],
        properties: {
          fpjp: {
            type: 'array',
            items: {type: 'number'},
          }
        }
      }
    },
  },
};



export const FpjpUnHoldRequestBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['fpjp_id'],
        properties: {
          fpjp_id: {
            type: 'number'
          }
        }
      }
    },
  },
};


export const FpjpSendMessageRequestBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['fpjp_id', 'reason_id', 'description'],
        properties: {
          fpjp_id: {
            type: 'number'
          },
          reason_id: {
            type: 'number'
          },
          description: {
            type: 'string'
          },
        }
      }
    },
  },
};

export const FPJPApprovalBodyReq = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['appraval_id'],
        properties: {
          appraval_id: {
            type: 'array',
            items: {type: 'number'},
          }
        }
      }
    },
  },
};

export const FPJPWaitingPayBodyReq = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['fpjp_id'],
        properties: {
          fpjp_id: {
            type: 'array',
            items: {type: 'number'},
          }
        }
      }
    },
  },
};

// export const FPJPApprovalByEmailBodyReq = {
//   description: 'The input of login function',
//   required: true,
//   content: {
//     'application/json': {
//       schema: {
//         type: 'object',
//         required: ['token'],
//         properties: {
//           token: {
//             type: 'string'
//           }
//         }
//       }
//     },
//   },
// };

// export const FpjpUserRejectByEmailRequestBody = {
//   description: 'The input of login function',
//   required: true,
//   content: {
//     'application/json': {
//       schema: {
//         type: 'object',
//         required: ['token_set', 'reason_id', 'description'],
//         properties: {
//           reason_id: {
//             type: 'number',
//           },
//           description: {
//             type: 'string',
//           },
//           token_set: {
//             type: 'string',
//           }
//         }
//       }
//     },
//   },
// };

var FpjpGenerateStore: any; FpjpGenerateStore = {};
var runFpjpGenerate = false;

@authenticate('jwt')
export class DataFpjpActionController {
  constructor(
    @repository(DataFpjpRepository)
    public dataFpjpRepository: DataFpjpRepository,
    @inject(SecurityBindings.USER)
    public currentUserProfile: UserProfile,
    @repository(ApprovalMatrixRepository)
    public approvalMatrixRepository: ApprovalMatrixRepository,
    @repository(ApprovalMatrixMemberRepository)
    public approvalMatrixMemberRepository: ApprovalMatrixMemberRepository,
    @repository(ApprovalNotaRepository)
    public approvalNotaRepository: ApprovalNotaRepository,
    @repository(DataconfigRepository)
    public dataconfigRepository: DataconfigRepository,
    @repository(UserRepository)
    public userRepository: UserRepository,
    @repository(AdMasterRepository)
    public adMasterRepository: AdMasterRepository,
    @repository(DataDigiposRejectRepository)
    public dataDigiposRejectRepository: DataDigiposRejectRepository,
    @repository(HistoryFpjpRejectRepository)
    public historyFpjpRejectRepository: HistoryFpjpRejectRepository,
    @repository(HistoryFpjpNoteRepository)
    public historyFpjpNoteRepository: HistoryFpjpNoteRepository,
    @repository(DataNotificationRepository)
    public dataNotificationRepository: DataNotificationRepository,
    @repository(MailSenderMessageRepository)
    public mailSenderMessageRepository: MailSenderMessageRepository,
  ) { }


  onNowDate = () => {
    var d = new Date();
    return d.getFullYear() + "-" + d.getMonth() + "-" + d.getDay();
  }

  onCheckLogin = (a: number, op: string, b: number) => {
    var status: boolean; status = false;
    switch (op) {
      case ">":
        status = a > b;
        break;
      case ">=":
        status = a >= b;
        break;
      case "<":
        status = a < b;
        break;
      case "<=":
        status = a <= b;
        break;
      case "==":
        status = a == b;
        break;
      case "!=":
        status = a != b;
        break;
      default:
        status = a == b;
        break;
    }
    return status;
  }

  onGenerateFileFpjp = async () => {

    var UserID = this.currentUserProfile[securityId];
    for (var i in FpjpGenerateStore) {
      console.log(i, "RUNNING....");
      var indexFile = i;

      var FpjpInfo = FpjpGenerateStore[i];
      var DataFPJP: any; DataFPJP = {
        FPJPNumber: "",
        FPJPType: "",
        Tanngal: "",
        NamaPembuat: "",
        NIKPembuat: "",
        Jabatan: "",
        Divisi: "",
        DataPengeluaran: [],
        TotalAmount: 0,
        JmlTerbilang: "",
        PTName: "",
        NoRek: "",
        NamaBank: "",
        Diketahui: []
      };

      if (FpjpInfo.transaction_code != null && FpjpInfo.transaction_code != undefined) {
        var typeFPJP = ["", "Invoice", "Note Debit"];
        DataFPJP.FPJPNumber = FpjpInfo.transaction_code;
        DataFPJP.Tanngal = ControllerConfig.onBANowDate()//this.onBANowDate();
        var fpjpPeriode = FpjpInfo.periode.split("-");
        var Periode = ControllerConfig.onGetMonth((parseInt(fpjpPeriode[1]) - 1)) + " " + fpjpPeriode[0];
        var InfoUser = await this.userRepository.findById(FpjpInfo.by_submit_fpjp);
        var InfoAD = await this.adMasterRepository.findById(FpjpInfo.ad_code);
        DataFPJP.PTName = InfoAD.ad_name;
        DataFPJP.NoRek = InfoAD.no_rek;
        DataFPJP.NamaBank = InfoAD.name_bank;
        DataFPJP.NameRekening = InfoAD.name_rekening;

        DataFPJP.NamaPembuat = InfoUser.fullname;
        DataFPJP.NIKPembuat = InfoUser.nik;
        DataFPJP.Jabatan = InfoUser.jabatan;
        DataFPJP.Divisi = InfoUser.divisi;

        DataFPJP.Diketahui.push({Jabatan: InfoUser.jabatan, Name: InfoUser.fullname});

        var DataConfig = await this.dataconfigRepository.find({where: {config_group_id: 4}});
        var InfoPengeluaran: any; InfoPengeluaran = {};
        var PPNFpjp = 0;
        var TotalFpjp = 0;
        for (var i in DataConfig) {
          var infoConfig = DataConfig[i];
          if (FpjpInfo.type_trx == 1) {
            if (infoConfig.config_code == "0004-01") {
              TotalFpjp = FpjpInfo.price;
              InfoPengeluaran[infoConfig.config_code] = {No: 1, Keterangan: infoConfig.config_desc + " " + Periode, KodeAccount: infoConfig.label_code, Jumlah: FpjpInfo.price};
            }
            if (infoConfig.config_code == "0004-02") {
              InfoPengeluaran[infoConfig.config_code] = {No: 2, Keterangan: infoConfig.config_desc, KodeAccount: infoConfig.label_code, Jumlah: 0};
            }
            if (infoConfig.config_code == "0004-03") {
              PPNFpjp = infoConfig.config_value;
            }
            if (infoConfig.config_code == "0004-04") {
              DataFPJP.FPJPType = infoConfig.config_desc;
            }
          }

          if (FpjpInfo.type_trx == 2) {
            if (infoConfig.config_code == "0004-06") {
              DataFPJP.FPJPType = infoConfig.config_desc;
            }

            if (infoConfig.config_code == "0004-05") {
              TotalFpjp = FpjpInfo.price;
              InfoPengeluaran[infoConfig.config_code] = {No: 1, Keterangan: infoConfig.config_desc + " " + Periode, KodeAccount: infoConfig.label_code, Jumlah: FpjpInfo.price};
            }
          }
        }

        if (FpjpInfo.type_trx == 1) {
          var JmlPPN: number; JmlPPN = InfoPengeluaran["0004-01"].Jumlah * PPNFpjp;
          InfoPengeluaran["0004-02"].Keterangan = InfoPengeluaran["0004-02"].Keterangan + " " + (PPNFpjp * 100).toString() + "%"; //= {No: 1, Keterangan: infoConfig.config_value, Jumlah: 0};
          InfoPengeluaran["0004-02"].Jumlah = ControllerConfig.onGeneratePuluhan(parseFloat(JmlPPN.toFixed(0)));
          InfoPengeluaran["0004-01"].Jumlah = ControllerConfig.onGeneratePuluhan(parseFloat(InfoPengeluaran["0004-01"].Jumlah.toFixed(0)));

          DataFPJP.DataPengeluaran.push(InfoPengeluaran["0004-01"]);
          DataFPJP.DataPengeluaran.push(InfoPengeluaran["0004-02"]);
        }


        if (FpjpInfo.type_trx == 2) {
          InfoPengeluaran["0004-05"].Jumlah = ControllerConfig.onGeneratePuluhan(Number(InfoPengeluaran["0004-05"].Jumlah.toFixed(0)));
          DataFPJP.DataPengeluaran.push(InfoPengeluaran["0004-05"]);
        }


        var TotalAmount = FpjpInfo.type_trx == 1 ? TotalFpjp + (TotalFpjp * PPNFpjp) : TotalFpjp;

        DataFPJP.TotalAmount = ControllerConfig.onGeneratePuluhan(parseFloat(TotalAmount.toFixed(0)));
        DataFPJP.JmlTerbilang = ControllerConfig.onAngkaTerbilang(Number(TotalAmount.toFixed(0))) + " rupiah";

        var InfoApproval = await this.approvalNotaRepository.find({where: {nota_debet_id: FpjpInfo.id}});
        var UserApprove: any; UserApprove = [];
        for (var iUser in InfoApproval) {
          var users = InfoApproval[iUser].user_id;
          UserApprove.push(users);
          var ListApproval = await this.userRepository.findById(users);
          DataFPJP.Diketahui.push({Jabatan: ListApproval.jabatan, Name: ListApproval.fullname});
        }


        // var ListApproval = await this.userRepository.find({where: {id: {inq: UserApprove}}});
        // for (var iList in ListApproval) {
        //   var DataList = ListApproval[iList];
        //   DataFPJP.Diketahui.push({Jabatan: DataList.jabatan, Name: DataList.fullname});
        // }


        const headerImagePath = path.join(__dirname, '../../.digipost/template_ba/inv_header.png');
        const headerImageContents = fs.readFileSync(headerImagePath, {encoding: 'base64'});
        DataFPJP.headerImg = headerImageContents;

        var refToken = randomstring.generate() + ControllerConfig.onCurrentTime().toString();
        await this.dataFpjpRepository.updateById(FpjpInfo.id, {
          doc_ref: refToken
        });
        DataFPJP.refToken = refToken;

        var NameFileFPJP = FpjpInfo.transaction_code + ".pdf";
        var pathFile = path.join(__dirname, '../../.digipost/fpjp/') + NameFileFPJP;
        //MAKE FILE PDF BA
        var compiled = ejs.compile(fs.readFileSync(path.join(__dirname, '../../.digipost/template_ba/') + 'pdf_fpjp.html', 'utf8'));
        var html = compiled(DataFPJP);


        var config: any; config = {
          orientation: "portrait",
          "header": {
            "height": "80px",
            "contents": ""
          },
          "footer": {
            "height": "70px",
            "contents": ''
          }
        }
        pdf.create(html, config).toFile(pathFile, async (err: any, res: any) => {
          if (err) {
            console.log(err);
            await this.dataFpjpRepository.updateById(FpjpInfo.id, {
              fpjp_generate: 3,
              fpjp_generate_err: JSON.stringify(err),
            });
          } else {
            await this.dataFpjpRepository.updateById(FpjpInfo.id, {
              fpjp_generate: 2,
              fpjp_attach: NameFileFPJP,
              fpjp_generate_err: "",
              at_fpjp_generate: ControllerConfig.onCurrentTime().toString()
            })
          }
        });
      }

      delete FpjpGenerateStore[indexFile];
      console.log(FpjpGenerateStore);
      this.onGenerateFileFpjp();
      break;
    }
    var no = 0;
    for (var i in FpjpGenerateStore) {
      no++;
    }
    if (no == 0) {
      runFpjpGenerate = false;
    }
  }


  @post('/data-fpjp/un-hold-fpjp', {
    responses: {
      '200': {
        description: 'Array of DataFpjp model instances',
        content: {
          'application/json': {
            schema: FpjpUnHoldRequestBody,
          },
        },
      },
    },
  })
  async un_hole_fpjp(
    @requestBody(FpjpUnHoldRequestBody) fpjpUnHold: {
      fpjp_id: number
    },
  ): Promise<Object> {
    var UserID = this.currentUserProfile[securityId];
    var result: any; result = {};
    var dataFPJP = await this.dataFpjpRepository.find({where: {and: [{state: 3}, {id: fpjpUnHold.fpjp_id}]}});
    if (dataFPJP.length > 0) {
      for (var i in dataFPJP) {
        var rawFpjp = dataFPJP[i];
        try {
          await this.dataFpjpRepository.updateById(rawFpjp.id, {
            fpjp_hold_state: 0,
            at_verify: ControllerConfig.onCurrentTime().toString()
          });

          var userId = this.currentUserProfile[securityId];
          var roleNotifSetADB = ControllerConfig.onGetRoleNotifGroup("1002010201");
          var infoUser = await this.userRepository.findById(userId)
          await this.dataNotificationRepository.create({
            types: "NOTIFICATION",
            target: roleNotifSetADB.role,
            to: "",
            from_who: infoUser.username,
            subject: "Document FPJP On Proses  by Treasury",
            body: "Document FPJP untuk No: " + rawFpjp.transaction_code + " on progres by Proses  by Treasury !",
            path_url: "",
            app: "DIGIPOS",
            code: "1002010201",
            code_label: "",
            at_create: ControllerConfig.onCurrentTime().toString(),
            at_read: "",
            periode: rawFpjp.periode,
            at_flag: 0
          });

          ControllerConfig.onSendNotif({
            types: "NOTIFICATION",
            target: roleNotifSetADB.role,
            to: "",
            from_who: infoUser.username,
            subject: "Document FPJP On Proses  by Treasury",
            body: "Document FPJP untuk No: " + rawFpjp.transaction_code + " on progres by Proses  by Treasury !",
            path_url: "",
            app: "DIGIPOS",
            code: "1002010201",
            code_label: "",
            at_create: ControllerConfig.onCurrentTime().toString(),
            at_read: "",
            periode: rawFpjp.periode,
            at_flag: 0
          });


        } catch (error) {
          result["success"] = false;
          result["message"] = error;
          return result;
        }
      }
      result["success"] = true;
      result["message"] = "Data  Validate is [" + dataFPJP.length + "] Fpjp";
    } else {
      result["success"] = false;
      result["message"] = "No Data Fpjp Validate !";
    }
    return result;
  }

  @post('/data-fpjp/send-note-fpjp', {
    responses: {
      '200': {
        description: 'Array of DataFpjp model instances',
        content: {
          'application/json': {
            schema: FpjpSendMessageRequestBody,
          },
        },
      },
    },
  })
  async send_note_fpjp(
    @requestBody(FpjpSendMessageRequestBody) fpjpSendMessage: {
      fpjp_id: number,
      reason_id: number,
      description: string
    },
  ): Promise<Object> {
    var UserID = this.currentUserProfile[securityId];
    var result: any; result = {};
    var dataFPJP = await this.dataFpjpRepository.find({where: {and: [{state: 3}, {id: fpjpSendMessage.fpjp_id}]}});
    if (dataFPJP.length > 0) {
      for (var i in dataFPJP) {
        var rawFpjp = dataFPJP[i];
        try {
          await this.historyFpjpNoteRepository.create({
            fpjp_id: rawFpjp.id,
            reason_id: fpjpSendMessage.reason_id,
            note: fpjpSendMessage.description,
            at_create: ControllerConfig.onCurrentTime().toString(),
            by_create: UserID,
            state: 0
          });

          await this.dataFpjpRepository.updateById(rawFpjp.id, {
            fpjp_hold_state: 1,
            fpjp_note_state: 1,
            fpjp_note: fpjpSendMessage.description
          });

          var userId = this.currentUserProfile[securityId];
          var roleNotifSetADB = ControllerConfig.onGetRoleNotifGroup("12010101");
          var infoUser = await this.userRepository.findById(userId)
          await this.dataNotificationRepository.create({
            types: "NOTIFICATION",
            target: roleNotifSetADB.role,
            to: rawFpjp.ad_code,
            from_who: infoUser.username,
            subject: roleNotifSetADB.label,
            body: "New Note Document FPJP for periode  " + rawFpjp.periode + " : " + fpjpSendMessage.description,
            path_url: "",
            app: "DIGIPOS",
            code: "12010101",
            code_label: "",
            at_create: ControllerConfig.onCurrentTime().toString(),
            at_read: "",
            periode: rawFpjp.periode,
            at_flag: 0
          });

          ControllerConfig.onSendNotif({
            types: "NOTIFICATION",
            target: roleNotifSetADB.role,
            to: rawFpjp.ad_code,
            from_who: infoUser.username,
            subject: roleNotifSetADB.label,
            body: "New Note Document FPJP for periode  " + rawFpjp.periode + " : " + fpjpSendMessage.description,
            path_url: "",
            app: "DIGIPOS",
            code: "12010101",
            code_label: "",
            at_create: ControllerConfig.onCurrentTime().toString(),
            at_read: "",
            periode: rawFpjp.periode,
            at_flag: 0
          });


          // var messageFb = fpjpSendMessage.description;
          // var dataMsg = {
          //   title: "New Note FPJP For " + rawFpjp.ad_code,
          //   message: messageFb,
          //   type: "FPJP_SEND_NOTE",
          //   to_role: "",
          //   to_user: rawFpjp.ad_code,
          //   at_create: ControllerConfig.onCurrentTime().toString(),
          //   status: "0",
          //   id_target: rawFpjp.id == undefined ? "0" : rawFpjp.id.toString(),
          //   for_app: "PORTAL_AD",
          //   periode: rawFpjp.periode
          // };
          // await this.dataNotificationRepository.create(dataMsg);
          // ControllerConfig.onSendMessageToSubscribe(dataMsg, (r: any) => {
          //   console.log(r);
          // });

        } catch (error) {
          result["success"] = false;
          result["message"] = error;
          return result;
        }

      }
      result["success"] = true;
      result["message"] = "Data  Validate is [" + dataFPJP.length + "] Fpjp";
    } else {
      result["success"] = false;
      result["message"] = "No Data Fpjp Validate !";
    }
    return result;
  }




  @post('/data-fpjp/a-validate-bu', {
    responses: {
      '200': {
        description: 'Array of DataFpjp model instances',
        content: {
          'application/json': {
            schema: FpjpValidateRequestBody,
          },
        },
      },
    },
  })
  async validate_a_bu(
    @requestBody(FpjpValidateRequestBody) fpjpValidate: {
      fpjp: []
    },
  ): Promise<Object> {
    var UserID = this.currentUserProfile[securityId];
    var result: any; result = {};
    var dataFPJP = await this.dataFpjpRepository.find({where: {and: [{acc_bu_state: 0}, {state: 1}, {type_trx: 2}, {id: {inq: fpjpValidate.fpjp}}]}});
    if (dataFPJP.length > 0) {
      for (var i in dataFPJP) {
        var rawFpjp = dataFPJP[i];
        try {
          await this.dataFpjpRepository.updateById(rawFpjp.id, {
            acc_bu_at: ControllerConfig.onCurrentTime().toString(),
            acc_bu_by: UserID,
            acc_bu_state: 1,
            acc_ba_at: ControllerConfig.onCurrentTime().toString(),
            acc_ba_by: UserID,
            acc_ba_state: 1,
            state: 1
          });

          var userId = this.currentUserProfile[securityId];
          var roleNotifSetADB = ControllerConfig.onGetRoleNotifGroup("0702010201");
          var infoUser = await this.userRepository.findById(userId)
          await this.dataNotificationRepository.create({
            types: "NOTIFICATION",
            target: roleNotifSetADB.role,
            to: rawFpjp.ad_code,
            from_who: infoUser.username,
            subject: roleNotifSetADB.label,
            body: "Data FPJP  Dengan Nomor  : " + rawFpjp.inv_number + " untuk " + rawFpjp.ad_code + " telah di validasi.",
            path_url: "",
            app: "DIGIPOS",
            code: "0702010201",
            code_label: "",
            at_create: ControllerConfig.onCurrentTime().toString(),
            at_read: "",
            periode: rawFpjp.periode,
            at_flag: 0
          });

          ControllerConfig.onSendNotif({
            types: "NOTIFICATION",
            target: roleNotifSetADB.role,
            to: rawFpjp.ad_code,
            from_who: infoUser.username,
            subject: roleNotifSetADB.label,
            body: "Data FPJP  Dengan Nomor  : " + rawFpjp.inv_number + " untuk " + rawFpjp.ad_code + " telah di validasi.",
            path_url: "",
            app: "DIGIPOS",
            code: "0702010201",
            code_label: "",
            at_create: ControllerConfig.onCurrentTime().toString(),
            at_read: "",
            periode: rawFpjp.periode,
            at_flag: 0
          });

        } catch (error) {
          result["success"] = false;
          result["message"] = error;
          return result;
        }
      }
      result["success"] = true;
      result["message"] = "Data  Validate is [" + dataFPJP.length + "] Fpjp";
    } else {
      result["success"] = false;
      result["message"] = "No Data Fpjp Validate !";
    }

    return result;
  }

  @post('/data-fpjp/b-validate-ba', {
    responses: {
      '200': {
        description: 'Array of DataFpjp model instances',
        content: {
          'application/json': {
            schema: FpjpValidateRequestBody,
          },
        },
      },
    },
  })
  async validate_b_ba(
    @requestBody(FpjpValidateRequestBody) fpjpValidate: {
      fpjp: []
    },
  ): Promise<Object> {
    var UserID = this.currentUserProfile[securityId];
    var result: any; result = {};
    var dataFPJP = await this.dataFpjpRepository.find({where: {and: [{acc_bu_state: 0}, {acc_ba_state: 0}, {state: 1}, {type_trx: 1}, {id: {inq: fpjpValidate.fpjp}}]}});
    if (dataFPJP.length > 0) {
      for (var i in dataFPJP) {
        var rawFpjp = dataFPJP[i];
        try {
          await this.dataFpjpRepository.updateById(rawFpjp.id, {
            acc_ba_at: ControllerConfig.onCurrentTime().toString(),
            acc_ba_by: UserID,
            acc_ba_state: 1
          });

          var userId = this.currentUserProfile[securityId];
          var roleNotifSetADB = ControllerConfig.onGetRoleNotifGroup("0701010101");
          var infoUser = await this.userRepository.findById(userId)
          await this.dataNotificationRepository.create({
            types: "NOTIFICATION",
            target: roleNotifSetADB.role,
            to: rawFpjp.ad_code,
            from_who: infoUser.username,
            subject: roleNotifSetADB.label,
            body: "Data FPJP  Dengan Nomor  : " + rawFpjp.inv_number + " untuk " + rawFpjp.ad_code + " telah di validasi.",
            path_url: "",
            app: "DIGIPOS",
            code: "0701010101",
            code_label: "",
            at_create: ControllerConfig.onCurrentTime().toString(),
            at_read: "",
            periode: rawFpjp.periode,
            at_flag: 0
          });

          ControllerConfig.onSendNotif({
            types: "NOTIFICATION",
            target: roleNotifSetADB.role,
            to: rawFpjp.ad_code,
            from_who: infoUser.username,
            subject: roleNotifSetADB.label,
            body: "Data FPJP  Dengan Nomor  : " + rawFpjp.inv_number + " untuk " + rawFpjp.ad_code + " telah di validasi.",
            path_url: "",
            app: "DIGIPOS",
            code: "0701010101",
            code_label: "",
            at_create: ControllerConfig.onCurrentTime().toString(),
            at_read: "",
            periode: rawFpjp.periode,
            at_flag: 0
          });

        } catch (error) {
          result["success"] = false;
          result["message"] = error;
          return result;
        }
      }


      result["success"] = true;
      result["message"] = "Data  Validate is [" + dataFPJP.length + "] Fpjp";
    } else {
      result["success"] = false;
      result["message"] = "No Data Fpjp Validate !";
    }

    return result;
  }

  @post('/data-fpjp/b-validate-bu', {
    responses: {
      '200': {
        description: 'Array of DataFpjp model instances',
        content: {
          'application/json': {
            schema: FpjpValidateRequestBody,
          },
        },
      },
    },
  })
  async validate_b_bu(
    @requestBody(FpjpValidateRequestBody) fpjpValidate: {
      fpjp: []
    },
  ): Promise<Object> {
    var UserID = this.currentUserProfile[securityId];
    var result: any; result = {};
    var dataFPJP = await this.dataFpjpRepository.find({where: {and: [{acc_bu_state: 0}, {state: 1}, {id: {inq: fpjpValidate.fpjp}}]}});
    if (dataFPJP.length > 0) {
      for (var i in dataFPJP) {
        var rawFpjp = dataFPJP[i];
        try {
          await this.dataFpjpRepository.updateById(rawFpjp.id, {
            acc_bu_at: ControllerConfig.onCurrentTime().toString(),
            acc_bu_by: UserID,
            acc_bu_state: 1,
            acc_ba_at: ControllerConfig.onCurrentTime().toString(),
            acc_ba_by: UserID,
            acc_ba_state: 1,
            state: 1
          });

          var userId = this.currentUserProfile[securityId];
          var roleNotifSetADB = ControllerConfig.onGetRoleNotifGroup("0701010201");
          var infoUser = await this.userRepository.findById(userId)
          await this.dataNotificationRepository.create({
            types: "NOTIFICATION",
            target: roleNotifSetADB.role,
            to: rawFpjp.ad_code,
            from_who: infoUser.username,
            subject: roleNotifSetADB.label,
            body: "Data FPJP  Dengan Nomor  : " + rawFpjp.inv_number + " untuk " + rawFpjp.ad_code + " telah di validasi.",
            path_url: "",
            app: "DIGIPOS",
            code: "0701010101",
            code_label: "",
            at_create: ControllerConfig.onCurrentTime().toString(),
            at_read: "",
            periode: rawFpjp.periode,
            at_flag: 0
          });

          ControllerConfig.onSendNotif({
            types: "NOTIFICATION",
            target: roleNotifSetADB.role,
            to: rawFpjp.ad_code,
            from_who: infoUser.username,
            subject: roleNotifSetADB.label,
            body: "Data FPJP  Dengan Nomor  : " + rawFpjp.inv_number + " untuk " + rawFpjp.ad_code + " telah di validasi.",
            path_url: "",
            app: "DIGIPOS",
            code: "0701010201",
            code_label: "",
            at_create: ControllerConfig.onCurrentTime().toString(),
            at_read: "",
            periode: rawFpjp.periode,
            at_flag: 0
          });

        } catch (error) {
          result["success"] = false;
          result["message"] = error;
          return result;
        }
      }
      result["success"] = true;
      result["message"] = "Data  Validate is [" + dataFPJP.length + "] Fpjp";
    } else {
      result["success"] = false;
      result["message"] = "No Data Fpjp Validate !";
    }

    return result;
  }

  @post('/data-fpjp/generate_no_fpjp', {
    responses: {
      '200': {
        description: 'Array of DataFpjp model instances',
        content: {
          'application/json': {
            schema: FpjpValidateRequestBody,
          },
        },
      },
    },
  })
  async generate_no_fpjp(
    @requestBody(FpjpValidateRequestBody) fpjpValidate: {
      fpjp: []
    },
  ): Promise<Object> {
    var UserID = this.currentUserProfile[securityId];
    var result: any; result = {};
    var today = new Date();
    var MM = (today.getMonth() + 1);
    var YY = today.getFullYear();
    var DD = today.getDate();
    var DateZero = "00";
    var NumberFp = 'BAT-' + YY + '' + DateZero.substring(MM.toString().length, 2) + (today.getMonth() + 1) + '' + today.getDate();
    var dataFPJP = await this.dataFpjpRepository.find({where: {and: [{acc_ba_state: 1}, {acc_bu_state: 1}, {state: 1}, {id: {inq: fpjpValidate.fpjp}}, {transaction_code: ""}]}});
    if (dataFPJP.length > 0) {
      for (var i in dataFPJP) {
        var info = dataFPJP[i];
        var Zero = "0000";
        var id = info.id == undefined ? "0" : info.id;
        var NumberFPJP = NumberFp + "-" + Zero.substring(id.toString().length, 4) + id.toString();
        try {
          if (info.inv_number == "" || info.inv_number == null || info.inv_number == "-") {
            await this.dataFpjpRepository.updateById(info.id, {
              transaction_code: NumberFPJP,
              inv_number: NumberFPJP
            });
          } else {
            await this.dataFpjpRepository.updateById(info.id, {
              transaction_code: NumberFPJP
            });
          }
        } catch (error) {
          result["success"] = false;
          result["message"] = error;
          return result;
        }
      }
      result["success"] = true;
      result["message"] = "Data  Number Fpjp Generate is [" + dataFPJP.length + "] Number";
    } else {
      result["success"] = false;
      result["message"] = "No Data Number Fpjp Generated !";
    }
    return result;
  }

  @post('/data-fpjp/generate_no_fpjp_all', {
    responses: {
      '200': {
        description: 'Array of DataFpjp model instances',
        content: {
          'application/json': {
            schema: FpjpNoAllRequestBody,
          },
        },
      },
    },
  })
  async generate_no_fpjp_all(
    @requestBody(FpjpNoAllRequestBody) fpjpValidate: {
      periode: string, type_trx: number
    },
  ): Promise<Object> {
    var UserID = this.currentUserProfile[securityId];
    var result: any; result = {};
    var today = new Date();
    var MM = (today.getMonth() + 1);
    var YY = today.getFullYear();
    var DD = today.getDate();
    var DateZero = "00";
    var NumberFp = 'BAT-' + YY + '' + DateZero.substring(MM.toString().length, 2) + (today.getMonth() + 1) + '' + today.getDate();
    var dataFPJP = await this.dataFpjpRepository.find({where: {and: [{acc_ba_state: 1}, {acc_bu_state: 1}, {state: 1}, {type_trx: fpjpValidate.type_trx}, {periode: fpjpValidate.periode}, {transaction_code: ""}]}});
    if (dataFPJP.length > 0) {
      for (var i in dataFPJP) {
        var info = dataFPJP[i];
        var Zero = "0000";
        var id = info.id == undefined ? "0" : info.id;
        var NumberFPJP = NumberFp + "-" + Zero.substring(id.toString().length, 4) + id.toString();
        try {
          if (info.inv_number == "" || info.inv_number == null || info.inv_number == "-") {
            await this.dataFpjpRepository.updateById(info.id, {
              transaction_code: NumberFPJP,
              inv_number: NumberFPJP
            });
          } else {
            await this.dataFpjpRepository.updateById(info.id, {
              transaction_code: NumberFPJP
            });
          }
        } catch (error) {
          result["success"] = false;
          result["message"] = error;
          return result;
        }
      }
      result["success"] = true;
      result["message"] = "Data  Number Fpjp Generate is [" + dataFPJP.length + "] Number";
    } else {
      result["success"] = false;
      result["message"] = "No Data Number Fpjp Generated !";
    }
    return result;
  }

  @post('/data-fpjp/set-process-approve', {
    responses: {
      '200': {
        description: 'Array of DataFpjp model instances',
        content: {
          'application/json': {
            schema: FpjpValidateRequestBody,
          },
        },
      },
    },
  })
  async set_process_approve_fpjp(
    @requestBody(FpjpValidateRequestBody) fpjpValidate: {
      fpjp: []
    },
  ): Promise<Object> {
    var LoginUserID = this.currentUserProfile[securityId];
    var result: any; result = {};
    var dataFPJP = await this.dataFpjpRepository.find({where: {and: [{acc_ba_state: 1}, {acc_bu_state: 1}, {state: 1}, {id: {inq: fpjpValidate.fpjp}}, {transaction_code: {neq: ""}}]}});
    if (dataFPJP.length > 0) {
      for (var i in dataFPJP) {
        var info = dataFPJP[i];
        try {

          var TotalTransaksi = info.total_amount == undefined ? 0 : info.total_amount;
          var LogicValidation = await this.approvalMatrixRepository.find({order: ["amd_index ASC"]});
          var DataMatrix: any;
          for (var i in LogicValidation) {
            var infoValidation = LogicValidation[i];
            if (infoValidation.amd_condition == "AND") {
              if (
                this.onCheckLogin(TotalTransaksi, infoValidation.amd_operator, infoValidation.amd_value) == true &&
                this.onCheckLogin(TotalTransaksi, infoValidation.amd_operator_2, infoValidation.amd_value_2) == true) {
                DataMatrix = infoValidation;
              }
            }
            if (infoValidation.amd_condition == "OR") {
              if (
                this.onCheckLogin(TotalTransaksi, infoValidation.amd_operator, infoValidation.amd_value) == true ||
                this.onCheckLogin(TotalTransaksi, infoValidation.amd_operator_2, infoValidation.amd_value_2) == true) {
                DataMatrix = infoValidation;
              }
            }
          }

          var GroupRoot = DataMatrix.amg_id_root;
          var Group1 = DataMatrix.amg_id_level1;
          var Group2 = DataMatrix.amg_id_level2;

          var MemberApproval = await this.approvalMatrixMemberRepository.find({where: {and: [{amg_id: {inq: [GroupRoot, Group1, Group2]}}, {amm_state: true}]}});
          var UserFirst = "";
          var UserToken = "";
          for (var iUser in MemberApproval) {
            var InfoMember = MemberApproval[iUser];
            var UserID = InfoMember.amm_userid;
            var ap_level = 1;


            if (InfoMember.amg_id != GroupRoot) {
              ap_level = 2;
            }

            if (UserFirst == "") {
              UserFirst = UserID;
            }

            UserToken = randomstring.generate(6) + "-" + ControllerConfig.onCurrentTime().toString();
            await this.approvalNotaRepository.create({
              user_id: UserID,
              nota_debet_id: info.id,
              state: 0,
              ap_level: ap_level,
              at_create: new Date().toLocaleString(),
              code_rand: UserToken
            });


            var userId = this.currentUserProfile[securityId];
            var roleNotifSetADB = ControllerConfig.onGetRoleNotifGroup("0801010101");
            var infoUser = await this.userRepository.findById(userId)
            await this.dataNotificationRepository.create({
              types: "NOTIFICATION",
              target: roleNotifSetADB.role,
              to: UserID,
              from_who: infoUser.username,
              subject: roleNotifSetADB.label,
              body: "Document FPJP dengan No : " + info.transaction_code + " telah disubmit.",
              path_url: "",
              app: "DIGIPOS",
              code: "0801010101",
              code_label: "",
              at_create: ControllerConfig.onCurrentTime().toString(),
              at_read: "",
              periode: info.periode,
              at_flag: 0
            });

            ControllerConfig.onSendNotif({
              types: "NOTIFICATION",
              target: roleNotifSetADB.role,
              to: UserID,
              from_who: infoUser.username,
              subject: roleNotifSetADB.label,
              body: "Document FPJP dengan No : " + info.transaction_code + " telah disubmit.",
              path_url: "",
              app: "DIGIPOS",
              code: "0801010101",
              code_label: "",
              at_create: ControllerConfig.onCurrentTime().toString(),
              at_read: "",
              periode: info.periode,
              at_flag: 0
            });

          }

          var FpjpInfo = await this.dataFpjpRepository.findById(info.id);
          var UserInfo = await this.userRepository.findById(UserFirst);
          var UserInfoSubmit = await this.userRepository.findById(LoginUserID);
          var FpjpType = ["", "Transaksi Nota Debit", "Transaksi Invoice"]
          var ContentFPJP: any; ContentFPJP = {
            name_approval: UserInfo.fullname,
            fpjp_number: FpjpInfo.transaction_code,
            fpjp_submit: UserInfoSubmit.fullname,
            link_approval: process.env.API_URL_FPJP,
            fpjp_type: FpjpType[FpjpInfo.type_trx],
            fpjp_name: "FPJP For " + FpjpInfo.ad_name,
            currency: "IDR ( Rupiah )",
            amount: "Rp." + ControllerConfig.onGeneratePuluhan(FpjpInfo.total_amount == undefined ? 0 : FpjpInfo.total_amount),
            link_list_approval: process.env.API_URL_FPJP,
            remarks: FpjpInfo.remarks,
            token_set: UserToken
          }

          // console.log(UserInfo.email, UserInfo.fullname, FpjpInfo.transaction_code, FpjpType[FpjpInfo.type_trx]);
          if (UserInfo.email != "" && UserInfo.email != null) {
            //mailSenderMessageRepository
            var dateNow = new Date();
            await this.mailSenderMessageRepository.create({
              AT_CREATE: dateFormat(dateNow),
              AT_UPDATE: dateFormat(dateNow),
              AT_SEND: undefined,
              FLAG_SEND: 0,
              SENDER_TO: UserInfo.email,
              MESSAGE: FpjpInfo.id + "#" + FpjpInfo.transaction_code + "#" + FpjpInfo.periode + "#" + FpjpType[FpjpInfo.type_trx] + "#" + UserInfo.fullname + "#" + UserInfoSubmit.fullname + "#" + UserToken,
              MESSAGE_FOR: "APPROVAL_FPJP",
              SOURCE_DATA: "DataFpjp"
            });

            // MailConfig.onSendMailFPJP(UserInfo.email, ContentFPJP, (r: any) => {

            // });
          }

          await this.dataFpjpRepository.updateById(info.id, {
            state: 2,
            date_submit_fpjp: ControllerConfig.onCurrentTime().toString(),
            by_submit_fpjp: LoginUserID,
            fpjp_note: undefined
          });

        } catch (error) {
          result["success"] = false;
          result["message"] = error;
          return result;
        }
      }
      result["success"] = true;
      result["message"] = "Data Fpjp Set Process Approve is [" + dataFPJP.length + "] Fpjp";
    } else {
      result["success"] = false;
      result["message"] = "No Data Set Process Approve FPJP !";
    }
    return result;
  }

  @get('/data-fpj/by_user/{user}', {
    responses: {
      '200': {
        description: 'Array of NotaDebet model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(DataFpjp, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find_fpjp_by_user(
    @param.path.string('user') user: string,
    // @param.filter(DataFpjp, {exclude: 'where'}) filter?: FilterExcludingWhere<DataFpjp>
    @param.filter(DataFpjp) filter?: Filter<DataFpjp>,
  ): Promise<Object> {
    var UserID = this.currentUserProfile[securityId];
    var result: any; result = {};
    result["success"] = true;
    result["message"] = "success";
    result["data"] = [];
    var notaApproval = await this.approvalNotaRepository.find({where: {user_id: user}});
    var fpjp_store: any; fpjp_store = [];
    for (var i in notaApproval) {
      fpjp_store.push(notaApproval[i].nota_debet_id);
    }

    var filterThis: any; filterThis = {};
    if (filter != null || filter != undefined) {
      filterThis = filter;
    }

    if (filterThis.where != undefined) {
      filterThis.where = {and: [filterThis.where, {id: {inq: fpjp_store}}]};
    } else {
      filterThis.where = {id: {inq: fpjp_store}};
    }
    var infoFpjp = await this.dataFpjpRepository.find(filterThis);
    for (var i in infoFpjp) {
      var rawFpjp: any; rawFpjp = infoFpjp[i];
      var data: any; data = {};
      for (var j in rawFpjp) {
        data[j] = rawFpjp[j]
      }
      data["user_submit"] = {};
      if (rawFpjp.by_submit_fpjp != null || rawFpjp.by_submit_fpjp != "") {
        var infoUser = await this.userRepository.findById(rawFpjp.by_submit_fpjp);
        if (infoUser != null) {
          data["user_submit"] = {
            id: infoUser.id,
            nik: infoUser.nik,
            jabatan: infoUser.jabatan,
            divisi: infoUser.divisi,
            fullname: infoUser.fullname,
            gender: infoUser.gender,
            address: infoUser.address,
            phone: infoUser.phone,
            email: infoUser.email
          }
        }
      }
      var infoApprov = await this.approvalNotaRepository.find({where: {nota_debet_id: rawFpjp.id}});
      data["usr_approval"] = [];
      for (var k in infoApprov) {
        var rawApprove = infoApprov[k];
        var infoUser = await this.userRepository.findById(rawApprove.user_id);
        data["usr_approval"].push({
          id: infoUser.id,
          nik: infoUser.nik,
          jabatan: infoUser.jabatan,
          divisi: infoUser.divisi,
          fullname: infoUser.fullname,
          gender: infoUser.gender,
          address: infoUser.address,
          phone: infoUser.phone,
          email: infoUser.email,
          approval_id: rawApprove.id,
          appraval_state: rawApprove.state,
          date_need_approve: rawApprove.at_create,
          date_set_approve: rawApprove.at_update
        })
      }

      var infoNote = await this.historyFpjpRejectRepository.find({where: {fpjp_id: rawFpjp.id}});
      data["fpjp_note_reject"] = [];
      for (var l in infoNote) {
        data["fpjp_note_reject"].push(infoNote[l]);
      }

      var infoNoteHold = await this.historyFpjpNoteRepository.find({where: {fpjp_id: rawFpjp.id}});
      data["fpjp_note_hold"] = [];
      for (var k in infoNoteHold) {
        data["fpjp_note_hold"].push(infoNoteHold[k]);
      }

      result["data"].push(data);
    }
    return result;
  }

  @post('/data-fpjp/complite', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: FpjpValidateRequestBody
          }
        },
      },
    },
  })
  async FinishFjp(
    @requestBody(FpjpValidateRequestBody) FPJPreq: {
      fpjp: []
    },
  ): Promise<Object> {
    var returnMs: any; returnMs = {success: true, message: ""};
    try {
      var dataFPJP = await this.dataFpjpRepository.find({where: {and: [{state: 3}, {id: {inq: FPJPreq.fpjp}}, {fpjp_hold_state: 0}]}});
      if (dataFPJP.length > 0) {
        for (var i in dataFPJP) {
          var info = dataFPJP[i];
          this.dataFpjpRepository.updateById(info.id, {state: 4});
        }
      } else {
        returnMs.success = false;
        returnMs.message = "No data fpjp complited !";
      }
    } catch (error) {
      returnMs.success = false;
      returnMs.message = error;
    }
    return returnMs;
  }

  @post('/data-fpjp/uncomplite', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: FpjpValidateRequestBody
          }
        },
      },
    },
  })
  async UnCompliteFjp(
    @requestBody(FpjpValidateRequestBody) FPJPreq: {
      fpjp: []
    },
  ): Promise<Object> {
    var returnMs: any; returnMs = {success: true, message: ""};
    try {
      var dataFPJP = await this.dataFpjpRepository.find({where: {and: [{state: 4}, {id: {inq: FPJPreq.fpjp}}]}});
      for (var i in dataFPJP) {
        var info = dataFPJP[i];
        this.dataFpjpRepository.updateById(info.id, {state: 3});
      }
    } catch (error) {
      returnMs.success = false;
      returnMs.message = error;
    }
    return returnMs;
  }



  @post('/data-fpjp/data-reject', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: FpjpDataRejectRequestBody
          }
        },
      },
    },
  })
  async dataRejectFjp(
    @requestBody(FpjpDataRejectRequestBody) FPJPReject: {
      reason_id: number,
      fpjp_id: [],
      description: string
    },
  ): Promise<Object> {
    var returnMs: any; returnMs = {success: true, message: ""};
    var UserID = this.currentUserProfile[securityId];
    var userInfo = await this.userRepository.findById(UserID);
    var dataFpjp = await this.dataFpjpRepository.find({where: {and: [{id: {inq: FPJPReject.fpjp_id}}, {state: 1}]}});
    if (dataFpjp.length > 0) {
      for (var i in dataFpjp) {
        var rawFpjp = dataFpjp[i];
        await this.historyFpjpRejectRepository.create({
          fpjp_id: rawFpjp.id,
          reason_id: FPJPReject.reason_id,
          message: FPJPReject.description,
          at_reject: ControllerConfig.onCurrentTime().toString(),
          by_id_reject: UserID,
          by_reject: userInfo.fullname
        });

        await this.dataFpjpRepository.updateById(rawFpjp.id, {
          state: 5,
          fpjp_note: FPJPReject.description,
          fpjp_note_state: 1,
          fpjp_hold_state: 1
        });

        var userId = this.currentUserProfile[securityId];
        var roleNotifSetADB: any; roleNotifSetADB = {role: "", label: ""};
        var code: string; code = "";
        if (rawFpjp.type_trx == 1) {
          code = "0701010202";
          roleNotifSetADB = ControllerConfig.onGetRoleNotifGroup("0701010202");
        } else {
          code = "0702010202";
          roleNotifSetADB = ControllerConfig.onGetRoleNotifGroup("0702010202");
        }

        var infoUser = await this.userRepository.findById(userId)
        await this.dataNotificationRepository.create({
          types: "NOTIFICATION",
          target: roleNotifSetADB.role,
          to: rawFpjp.ad_code,
          from_who: infoUser.username,
          subject: roleNotifSetADB.label,
          body: "Data FPJP  Dengan Nomor  : " + rawFpjp.inv_number + " untuk " + rawFpjp.ad_code + " telah di tolak.",
          path_url: "",
          app: "DIGIPOS",
          code: code,
          code_label: "",
          at_create: ControllerConfig.onCurrentTime().toString(),
          at_read: "",
          periode: rawFpjp.periode,
          at_flag: 0
        });

        ControllerConfig.onSendNotif({
          types: "NOTIFICATION",
          target: roleNotifSetADB.role,
          to: rawFpjp.ad_code,
          from_who: infoUser.username,
          subject: roleNotifSetADB.label,
          body: "Data FPJP  Dengan Nomor  : " + rawFpjp.inv_number + " untuk " + rawFpjp.ad_code + " telah di tolak.",
          path_url: "",
          app: "DIGIPOS",
          code: code,
          code_label: "",
          at_create: ControllerConfig.onCurrentTime().toString(),
          at_read: "",
          periode: rawFpjp.periode,
          at_flag: 0
        });

      }
      returnMs.success = true;
      returnMs.message = "Success Fpjp reject !";
    } else {
      returnMs.success = false;
      returnMs.message = "Error Fpjp reject, not data reject exities !";
    }
    return returnMs;
  }

  @post('/data-fpjp/user-reject', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: FpjpUserRejectRequestBody
          }
        },
      },
    },
  })
  async UserRejectFjp(
    @requestBody(FpjpUserRejectRequestBody) FPJPApproval: {
      reason_id: number,
      id: number,
      description: string
    },
  ): Promise<Object> {
    var returnMs: any; returnMs = {success: true, message: ""};
    var UserID = this.currentUserProfile[securityId];
    var userInfo = await this.userRepository.findById(UserID);
    var dataApprove = await this.approvalNotaRepository.findOne({where: {and: [{id: FPJPApproval.id}, {state: 0}]}});
    var FpjpId = 0;
    if (dataApprove != null) {
      FpjpId = dataApprove.nota_debet_id;
    }

    if (FpjpId != 0) {
      var DataApproveFPJP = await this.approvalNotaRepository.find({where: {nota_debet_id: FpjpId}});
      var ApprovalIndex: any; ApprovalIndex = [];

      for (var i in DataApproveFPJP) {
        var rawApproval = DataApproveFPJP[i];
        ApprovalIndex.push(rawApproval.id);
      }

      await this.approvalNotaRepository.deleteAll({nota_debet_id: FpjpId});
      var dataFPJP = await this.dataFpjpRepository.findById(FpjpId);

      if (dataFPJP.type_trx == 1) {
        await this.dataFpjpRepository.updateById(FpjpId, {
          acc_bu_state: 0,
          acc_bu_at: "",
          acc_bu_by: "",
          state: 1,
          fpjp_note_state: 1,
          fpjp_hold_state: 1,
          fpjp_note: FPJPApproval.description
        });
      }

      if (dataFPJP.type_trx == 2) {
        await this.dataFpjpRepository.updateById(FpjpId, {
          acc_ba_state: 0,
          acc_ba_at: "",
          acc_ba_by: "",
          acc_bu_state: 0,
          acc_bu_at: "",
          acc_bu_by: "",
          state: 1,
          fpjp_note_state: 1,
          fpjp_hold_state: 1,
          fpjp_note: FPJPApproval.description
        });
      }

      await this.historyFpjpRejectRepository.create({
        fpjp_id: FpjpId,
        reason_id: FPJPApproval.reason_id,
        message: FPJPApproval.description,
        at_reject: ControllerConfig.onCurrentTime().toString(),
        by_id_reject: UserID,
        by_reject: userInfo.fullname
      });

      var dataFPJP = await this.dataFpjpRepository.findById(FpjpId);
      var userId = this.currentUserProfile[securityId];
      var roleNotifSetADB = ControllerConfig.onGetRoleNotifGroup("0801010105");
      var infoUser = await this.userRepository.findById(userId)

      await this.dataNotificationRepository.create({
        types: "NOTIFICATION",
        target: roleNotifSetADB.role,
        to: "",
        from_who: infoUser.username,
        subject: roleNotifSetADB.label,
        body: "Document FPJP Nomor : " + dataFPJP.transaction_code + " sudah di tolak oleh" + infoUser.username,
        path_url: "",
        app: "DIGIPOS",
        code: "0801010105",
        code_label: "",
        at_create: ControllerConfig.onCurrentTime().toString(),
        at_read: "",
        periode: dataFPJP.periode,
        at_flag: 0
      });

      ControllerConfig.onSendNotif({
        types: "NOTIFICATION",
        target: roleNotifSetADB.role,
        to: "",
        from_who: infoUser.username,
        subject: roleNotifSetADB.label,
        body: "Document FPJP Nomor : " + dataFPJP.transaction_code + " sudah di tolak oleh" + infoUser.username,
        path_url: "",
        app: "DIGIPOS",
        code: "0801010105",
        code_label: "",
        at_create: ControllerConfig.onCurrentTime().toString(),
        at_read: "",
        periode: dataFPJP.periode,
        at_flag: 0
      });

      returnMs.success = true;
      returnMs.message = "succes reject fpjp";



      // if (ApprovalIndex.indexOf(FPJPApproval.id) != -1) {
      //   var indexApprovalSet = ApprovalIndex.indexOf(FPJPApproval.id);
      //   if (indexApprovalSet == 0) {
      //     await this.dataFpjpRepository.updateById(FpjpId, {
      //       acc_ba_state: 0,
      //       acc_ba_at: "",
      //       acc_ba_by: "",
      //       acc_bu_state: 0,
      //       acc_bu_at: "",
      //       acc_bu_by: "",
      //       state: 1
      //     });
      //     await this.approvalNotaRepository.deleteAll({nota_debet_id: FpjpId});
      //   } else {
      //     var beforeID = ApprovalIndex[(indexApprovalSet - 1)];
      //     await this.approvalNotaRepository.updateById(beforeID, {
      //       state: 0,
      //       at_update: ""
      //     });
      //     await this.approvalNotaRepository.updateById(ApprovalIndex[indexApprovalSet], {
      //       state: 0,
      //       at_update: ""
      //     });
      //   }



      //   await this.historyFpjpRejectRepository.create({
      //     fpjp_id: FpjpId,
      //     reason_id: FPJPApproval.reason_id,
      //     message: FPJPApproval.description,
      //     at_reject: ControllerConfig.onCurrentTime().toString(),
      //     by_id_reject: UserID,
      //     by_reject: userInfo.fullname
      //   });


      // } else {
      //   returnMs.success = false;
      //   returnMs.message = "Error Fpjp reject, not data reject exities !";
      // }
    } else {
      returnMs.success = false;
      returnMs.message = "Error Fpjp reject, not data reject exities !";
    }

    // if (dataApprove.length > 0) {
    //   for (var i in dataApprove) {
    //     var infoApprove = dataApprove[i];
    //     // try {
    //     var today = new Date();
    //     var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    //     var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    //     var dateTime = date + ' ' + time;
    //     await this.approvalNotaRepository.updateById(infoApprove.id, {
    //       state: 2,
    //       at_update: dateTime
    //     });
    //     var dataNota = await this.approvalNotaRepository.findById(infoApprove.id);
    //     var dataFpjp = await this.dataFpjpRepository.findOne({where: {id: dataNota.nota_debet_id}});

    //     /*this.dataFpjpRepository.updateById(dataNota.nota_debet_id, {

    //     });*/

    //     await this.dataDigiposRejectRepository.create({
    //       ad_code: dataFpjp?.ad_code,
    //       ad_name: dataFpjp?.ad_name,
    //       config_id: FPJPApproval.reason_id,
    //       reject_code: "-",
    //       reject_label: FPJPApproval.description,
    //       reject_description: FPJPApproval.description,
    //       create_at: dateTime,
    //       user_reject: UserID,
    //       periode: dataFpjp?.periode
    //     });
    //   }
    //   returnMs.success = true;
    //   returnMs.message = "success update status reject !";
    // } else {
    //   returnMs.success = false;
    //   returnMs.message = "Error Approve, not data reject exities !";
    // }
    return returnMs;
  }


  @post('/data-fpjp/approve', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: FPJPApprovalBodyReq
          }
        },
      },
    },
  })
  async ApproveFjp(
    @requestBody(FPJPApprovalBodyReq) FPJPApproval: {
      appraval_id: []
    },
  ): Promise<Object> {
    var returnMs: any; returnMs = {success: true, message: ""};

    var dataApprove = await this.approvalNotaRepository.find({where: {and: [{id: {inq: FPJPApproval.appraval_id}}, {state: 0}]}})
    if (dataApprove.length > 0) {
      for (var i in dataApprove) {
        var infoApprove = dataApprove[i];
        // try {

        var today = new Date();
        var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        var dateTime = date + ' ' + time;

        await this.approvalNotaRepository.updateById(infoApprove.id, {
          state: 1,
          at_update: dateTime
        });

        var dataFPJP = await this.dataFpjpRepository.findById(infoApprove.nota_debet_id);
        var userId = this.currentUserProfile[securityId];
        var roleNotifSetADB = ControllerConfig.onGetRoleNotifGroup("0801010103");
        var infoUser = await this.userRepository.findById(userId)
        await this.dataNotificationRepository.create({
          types: "NOTIFICATION",
          target: roleNotifSetADB.role,
          to: "",
          from_who: infoUser.username,
          subject: roleNotifSetADB.label,
          body: "Document FPJP Nomor : " + dataFPJP.transaction_code + " sudah di approve oleh " + infoUser.username,
          path_url: "",
          app: "DIGIPOS",
          code: "0801010103",
          code_label: "",
          at_create: ControllerConfig.onCurrentTime().toString(),
          at_read: "",
          periode: dataFPJP.periode,
          at_flag: 0
        });

        ControllerConfig.onSendNotif({
          types: "NOTIFICATION",
          target: roleNotifSetADB.role,
          to: "",
          from_who: infoUser.username,
          subject: roleNotifSetADB.label,
          body: "Document FPJP Nomor : " + dataFPJP.transaction_code + " sudah di approve oleh " + infoUser.username,
          path_url: "",
          app: "DIGIPOS",
          code: "0801010103",
          code_label: "",
          at_create: ControllerConfig.onCurrentTime().toString(),
          at_read: "",
          periode: dataFPJP.periode,
          at_flag: 0
        });

        var AccFPJP = false;
        var FalidRoot = true;
        var AllValidRoot = await this.approvalNotaRepository.find({where: {and: [{nota_debet_id: infoApprove.nota_debet_id}, {ap_level: 1}]}});
        for (var jRoot in AllValidRoot) {
          if (AllValidRoot[jRoot].state == 0 || AllValidRoot[jRoot].state == 2) {
            FalidRoot = false;
          }
        }

        var AllValidLevel = await this.approvalNotaRepository.find({where: {and: [{nota_debet_id: infoApprove.nota_debet_id}, {ap_level: 2}]}});;
        if (FalidRoot == false || AllValidRoot.length == 0) {
          var ValidLevel = true;
          for (var jRoot in AllValidLevel) {
            if (AllValidLevel[jRoot].state == 0 || AllValidLevel[jRoot].state == 2) {
              FalidRoot = false;
              ValidLevel = false;
            }
          }
          if (ValidLevel == true) {
            AccFPJP = true;
          }
        } else {
          //Valid
          AccFPJP = true;
        }

        if (AccFPJP == true) {
          await this.dataFpjpRepository.updateById(infoApprove.nota_debet_id, {
            state: 3,
            fpjp_generate: 1
          });
          var infoFpjp = await this.dataFpjpRepository.findById(infoApprove.nota_debet_id);
          FpjpGenerateStore[infoApprove.nota_debet_id] = infoFpjp;
        } else {
          var dataAppv = await this.approvalNotaRepository.findById(infoApprove.id);
          var dataAppvList = await this.approvalNotaRepository.find({where: {and: [{nota_debet_id: dataAppv.nota_debet_id}, {state: 0}]}});
          var UserFirstSet: string; UserFirstSet = "";
          var UserFirstToken: string; UserFirstToken = "";
          for (var iAppv in dataAppvList) {
            UserFirstSet = dataAppvList[iAppv].user_id;
            UserFirstToken = dataAppvList[iAppv].code_rand;
            break;
          }

          if (dataAppv != null) {
            var FpjpInfo = await this.dataFpjpRepository.findById(dataAppv.nota_debet_id);
            var UserInfo = await this.userRepository.findById(UserFirstSet);
            var UserInfoSubmit = await this.userRepository.findById(dataAppv.user_id);
            var FpjpType = ["", "Transaksi Nota Debit", "Transaksi Invoice"]
            var ContentFPJP: any; ContentFPJP = {
              name_approval: UserInfo.fullname,
              fpjp_number: FpjpInfo.transaction_code,
              fpjp_submit: UserInfoSubmit.fullname,
              link_approval: process.env.API_URL_FPJP,
              fpjp_type: FpjpType[FpjpInfo.type_trx],
              fpjp_name: "FPJP For " + FpjpInfo.ad_name,
              currency: "IDR ( Rupiah )",
              amount: "Rp." + ControllerConfig.onGeneratePuluhan(FpjpInfo.total_amount == undefined ? 0 : FpjpInfo.total_amount),
              link_list_approval: process.env.API_URL_FPJP,
              remarks: FpjpInfo.remarks,
              token_set: UserFirstToken
            }

            console.log(UserInfo.email, UserInfo.fullname, FpjpInfo.transaction_code, FpjpType[FpjpInfo.type_trx]);
            if (UserInfo.email != "" && UserInfo.email != null) {

              var dateNow = new Date();
              await this.mailSenderMessageRepository.create({
                AT_CREATE: dateFormat(dateNow),
                AT_UPDATE: dateFormat(dateNow),
                AT_SEND: undefined,
                FLAG_SEND: 0,
                SENDER_TO: UserInfo.email,
                MESSAGE: FpjpInfo.id + "#" + FpjpInfo.transaction_code + "#" + FpjpInfo.periode + "#" + FpjpType[FpjpInfo.type_trx] + "#" + UserInfo.fullname + "#" + UserInfoSubmit.fullname + "#" + UserFirstToken,
                MESSAGE_FOR: "APPROVAL_FPJP",
                SOURCE_DATA: "DataFpjp"
              });
              // MailConfig.onSendMailFPJP(UserInfo.email, ContentFPJP, (r: any) => {

              // });
            }
          }
        }


        returnMs.success = true;
        returnMs.message = "success update status approve !";
      }

      console.log(runFpjpGenerate);
      // if (runFpjpGenerate == false) {
      console.log("RIINING START...");
      this.onGenerateFileFpjp();
      runFpjpGenerate = true;
      // }
    } else {
      returnMs.success = false;
      returnMs.message = "Error Approve, not data approve exities !";
    }
    return returnMs;
  }

  @post('/data-fpjp/waiting-payment', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: FPJPWaitingPayBodyReq
          }
        },
      },
    },
  })
  async WaitingFjp(
    @requestBody(FPJPWaitingPayBodyReq) FPJPWaitingPay: {
      fpjp_id: []
    },
  ): Promise<Object> {
    var returnMs: any; returnMs = {success: true, message: ""};
    var fpjpDoc = await this.dataFpjpRepository.find({where: {and: [{id: {inq: FPJPWaitingPay.fpjp_id}}]}});
    if (fpjpDoc.length > 0) {
      for (var i in fpjpDoc) {
        var infoFpjp = fpjpDoc[i];
        if (infoFpjp.state == 3) {
          try {
            await this.dataFpjpRepository.updateById(infoFpjp.id, {
              state: 6
            });

            var userId = this.currentUserProfile[securityId];
            var infoUser = await this.userRepository.findById(userId)
            var roleNotifSetADB = ControllerConfig.onGetRoleNotifGroup("1002010201");
            await this.dataNotificationRepository.create({
              types: "NOTIFICATION",
              target: roleNotifSetADB.role,
              to: "",
              from_who: infoUser.username,
              subject: "Document FPJP Siap Di Bayar",
              body: "Document FPJP untuk No: " + infoFpjp.transaction_code + " sudah siap untuk di dibayar  !",
              path_url: "",
              app: "DIGIPOS",
              code: "1002010201",
              code_label: "",
              at_create: ControllerConfig.onCurrentTime().toString(),
              at_read: "",
              periode: infoFpjp.periode,
              at_flag: 0
            });

            ControllerConfig.onSendNotif({
              types: "NOTIFICATION",
              target: roleNotifSetADB.role,
              to: "",
              from_who: infoUser.username,
              subject: "Document FPJP Siap Di Bayar",
              body: "Document FPJP untuk No: " + infoFpjp.transaction_code + " sudah siap untuk di dibayar !",
              path_url: "",
              app: "DIGIPOS",
              code: "1002010201",
              code_label: "",
              at_create: ControllerConfig.onCurrentTime().toString(),
              at_read: "",
              periode: infoFpjp.periode,
              at_flag: 0
            });

          } catch (error) {
            returnMs.success = false;
            returnMs.message = "Error set data to waiting payment, state data not on progress !";
          }
        }
      }
    } else {
      returnMs.success = false;
      returnMs.message = "Error regenerate Document FPJP, because document FPJP not exities  !";
    }
    return returnMs;
  }




  @post('/data-fpjps', {
    responses: {
      '200': {
        description: 'DataFpjp model instance',
        content: {'application/json': {schema: getModelSchemaRef(DataFpjp)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(DataFpjp, {
            title: 'NewDataFpjp',
            exclude: ['id'],
          }),
        },
      },
    })
    dataFpjp: Omit<DataFpjp, 'id'>,
  ): Promise<DataFpjp> {
    return this.dataFpjpRepository.create(dataFpjp);
  }

  @get('/data-fpjps/count', {
    responses: {
      '200': {
        description: 'DataFpjp model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(DataFpjp) where?: Where<DataFpjp>,
  ): Promise<Count> {
    return this.dataFpjpRepository.count(where);
  }

  @get('/data-fpjps', {
    responses: {
      '200': {
        description: 'Array of DataFpjp model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(DataFpjp, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(DataFpjp) filter?: Filter<DataFpjp>,
  ): Promise<Object> {
    var result: any; result = {};
    result["success"] = true;
    result["message"] = "success";
    result["data"] = [];


    var DataConfig = await this.dataconfigRepository.find({where: {config_group_id: 4}});
    var ConfigData: any; ConfigData = {};
    for (var i in DataConfig) {
      var infoConfig = DataConfig[i];
      var codes = infoConfig.config_code == undefined ? "-1" : infoConfig.config_code;
      ConfigData[codes] = infoConfig;
    }

    var infoFpjp = await this.dataFpjpRepository.find(filter);
    for (var i in infoFpjp) {
      var rawFpjp: any; rawFpjp = infoFpjp[i];
      var data: any; data = {};
      for (var j in rawFpjp) {
        data[j] = rawFpjp[j]
      }

      var dataAppNota = await this.approvalNotaRepository.findOne({order: ["id DESC"], where: {nota_debet_id: data["id"]}});

      var DateInv = new Date(Number(data["at_ad_submit_fpjp"]));
      var TglInv = new Date(Number(data["at_verify"]));


      if (data["at_fpjp_generate"] == null || data["at_fpjp_generate"] == "") {
        var dataAppNota = await this.approvalNotaRepository.findOne({order: ["id DESC"], where: {nota_debet_id: data["id"]}});
        if (dataAppNota != null) {
          TglInv = new Date(dataAppNota.at_update);
        }
      } else {
        TglInv = new Date(Number(data["at_fpjp_generate"]));
      }


      var invMounth = DateInv.getMonth() + 1;
      var dateInvSet = DateInv.getFullYear() + "-" + "00".substring(invMounth.toString().length) + invMounth + "-" + "00".substring(DateInv.getDate().toString().length) + DateInv.getDate();

      var tglInvMounth = TglInv.getMonth() + 1;
      var dateInvTglSet = TglInv.getFullYear() + "-" + "00".substring(tglInvMounth.toString().length) + tglInvMounth + "-" + "00".substring(TglInv.getDate().toString().length) + TglInv.getDate();

      var fpjpPeriode = data["periode"].split("-");
      var PeriodeThisFpjp = ControllerConfig.onGetMonth((parseInt(fpjpPeriode[1]) - 1)) + " " + fpjpPeriode[0];


      if (data["type_trx"] == 1) {
        data["description"] = ConfigData["0004-01"]["config_desc"] + " " + PeriodeThisFpjp;
      }

      if (data["type_trx"] == 2) {
        data["description"] = ConfigData["0004-05"]["config_desc"] + " " + PeriodeThisFpjp;
      }

      data["at_create"] = dateInvTglSet;//dateInvTglSet;
      data["at_verify"] = data["inv_date"] == null || data["inv_date"] == undefined ? dateInvSet : data["inv_date"];



      data["user_submit"] = {};
      if (rawFpjp.by_submit_fpjp != null && rawFpjp.by_submit_fpjp != "") {
        var infoUser = await this.userRepository.findById(rawFpjp.by_submit_fpjp);
        if (infoUser != null) {
          data["user_submit"] = {
            id: infoUser.id,
            nik: infoUser.nik,
            jabatan: infoUser.jabatan,
            divisi: infoUser.divisi,
            fullname: infoUser.fullname,
            gender: infoUser.gender,
            address: infoUser.address,
            phone: infoUser.phone,
            email: infoUser.email
          }
        }
      }

      data["ad_info"] = {};
      if (rawFpjp.ad_code != null && rawFpjp.ad_code != "") {
        data["ad_info"] = await this.adMasterRepository.findById(rawFpjp.ad_code);
      }

      var infoApprov = await this.approvalNotaRepository.find({order: ["id ASC"], where: {nota_debet_id: rawFpjp.id}});
      data["usr_approval"] = [];
      for (var k in infoApprov) {
        var rawApprove = infoApprov[k];
        if (rawApprove.user_id != "" && rawApprove.user_id != null && rawApprove.user_id != undefined) {
          var infoUser = await this.userRepository.findById(rawApprove.user_id);
          data["usr_approval"].push({
            id: infoUser.id,
            nik: infoUser.nik,
            jabatan: infoUser.jabatan,
            divisi: infoUser.divisi,
            fullname: infoUser.fullname,
            gender: infoUser.gender,
            address: infoUser.address,
            phone: infoUser.phone,
            email: infoUser.email,
            approval_id: rawApprove.id,
            appraval_state: rawApprove.state,
            date_need_approve: rawApprove.at_create,
            date_set_approve: rawApprove.at_update
          });
        }
      }

      var infoNoteReject = await this.historyFpjpRejectRepository.find({where: {fpjp_id: rawFpjp.id}});
      data["fpjp_note_reject"] = [];
      for (var l in infoNoteReject) {
        data["fpjp_note_reject"].push(infoNoteReject[l]);
      }


      var infoNoteHold = await this.historyFpjpNoteRepository.find({where: {fpjp_id: rawFpjp.id}});
      data["fpjp_note_hold"] = [];
      for (var k in infoNoteHold) {
        data["fpjp_note_hold"].push(infoNoteHold[k]);
      }


      result["data"].push(data);
    }

    return result;
  }

  @patch('/data-fpjps', {
    responses: {
      '200': {
        description: 'DataFpjp PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(DataFpjp, {partial: true}),
        },
      },
    })
    dataFpjp: DataFpjp,
    @param.where(DataFpjp) where?: Where<DataFpjp>,
  ): Promise<Count> {
    return this.dataFpjpRepository.updateAll(dataFpjp, where);
  }

  @get('/data-fpjps/{id}', {
    responses: {
      '200': {
        description: 'DataFpjp model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(DataFpjp, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(DataFpjp, {exclude: 'where'}) filter?: FilterExcludingWhere<DataFpjp>
  ): Promise<DataFpjp> {
    return this.dataFpjpRepository.findById(id, filter);
  }

  @patch('/data-fpjps/{id}', {
    responses: {
      '204': {
        description: 'DataFpjp PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(DataFpjp, {partial: true}),
        },
      },
    })
    dataFpjp: DataFpjp,
  ): Promise<void> {
    await this.dataFpjpRepository.updateById(id, dataFpjp);
  }

  @put('/data-fpjps/{id}', {
    responses: {
      '204': {
        description: 'DataFpjp PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() dataFpjp: DataFpjp,
  ): Promise<void> {
    await this.dataFpjpRepository.replaceById(id, dataFpjp);
  }

  @del('/data-fpjps/{id}', {
    responses: {
      '204': {
        description: 'DataFpjp DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.dataFpjpRepository.deleteById(id);
  }
}
