import {authenticate} from '@loopback/authentication';
import {UserRepository} from '@loopback/authentication-jwt';
import {inject} from '@loopback/core';
import {
  Count,
  CountSchema,


  Filter,


  FilterExcludingWhere,


  repository,
  Where
} from '@loopback/repository';
import {
  del, get,
  getModelSchemaRef, param,


  patch, post,




  put,

  requestBody
} from '@loopback/rest';
import {SecurityBindings, securityId, UserProfile} from '@loopback/security';
import {DataDispute} from '../models';
import {DataconfigRepository, DataDigiposRejectRepository, DataDisputeDetailRepository, DataDisputeRepository, DataNotificationRepository} from '../repositories';
import {ControllerConfig} from './controller.config';

export const ValidateDataDisputeBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['dispute_id'],
        properties: {
          dispute_id: {
            type: 'array',
            items: {type: 'number'},
          }
        }
      }
    },
  },
};


export const RejectDataDisputeRequestBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['reason_id', 'dispute', 'description'],
        properties: {
          reason_id: {
            type: 'number',
          },
          dispute_id: {
            type: 'array',
            items: {type: 'number'},
          },
          description: {
            type: 'string',
          }
        }
      }
    },
  },
};


@authenticate('jwt')
export class DataDisputeActionController {
  constructor(
    @repository(DataDisputeRepository)
    public dataDisputeRepository: DataDisputeRepository,
    @repository(UserRepository)
    public userRepository: UserRepository,
    @inject(SecurityBindings.USER)
    public currentUserProfile: UserProfile,
    @repository(DataconfigRepository)
    public dataconfigRepository: DataconfigRepository,
    @repository(DataDigiposRejectRepository)
    public dataDigiposRejectRepository: DataDigiposRejectRepository,
    @repository(DataDisputeDetailRepository)
    public dataDisputeDetailRepository: DataDisputeDetailRepository,
    @repository(DataNotificationRepository)
    public dataNotificationRepository: DataNotificationRepository,
  ) { }



  @post('/disputes/validate-dispute', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: ValidateDataDisputeBody
          }
        },
      },
    },
  })
  async ValidateDispute(
    @requestBody(ValidateDataDisputeBody) disputeAccBA: {
      dispute_id: []
    },
  ): Promise<Object> {
    var returnMs: any; returnMs = {success: true, message: ""};
    var UserID = this.currentUserProfile[securityId];
    var disputeData = await this.dataDisputeRepository.find({where: {and: [{dispute_id: {inq: disputeAccBA.dispute_id}}, {dispute_status: {neq: 2}}]}});
    for (var i in disputeData) {
      var infoDispute = disputeData[i];
      try {
        await this.dataDisputeRepository.updateById(infoDispute.dispute_id, {
          dispute_status: 2,
          at_acc: ControllerConfig.onCurrentTime().toString(),
          by_acc: UserID
        });

        var userId = this.currentUserProfile[securityId];
        var roleNotifSetADB = ControllerConfig.onGetRoleNotifGroup("0601010101");
        var infoUser = await this.userRepository.findById(userId)
        await this.dataNotificationRepository.create({
          types: "NOTIFICATION",
          target: roleNotifSetADB.role,
          to: infoDispute.ad_code,
          from_who: infoUser.username,
          subject: roleNotifSetADB.label,
          body: "Data Dispute Telah di Validasi Untuk  " + infoDispute.ad_code + " periode " + infoDispute.periode + " dan akan diproses dibulan berikutnya.",
          path_url: "",
          app: "DIGIPOS",
          code: "0601010101",
          code_label: "",
          at_create: ControllerConfig.onCurrentTime().toString(),
          at_read: "",
          periode: infoDispute.periode,
          at_flag: 0
        });

        ControllerConfig.onSendNotif({
          types: "NOTIFICATION",
          target: roleNotifSetADB.role,
          to: infoDispute.ad_code,
          from_who: infoUser.username,
          subject: roleNotifSetADB.label,
          body: "Data Dispute Telah di Validasi Untuk  " + infoDispute.ad_code + " periode " + infoDispute.periode + " dan akan diproses dibulan berikutnya.",
          path_url: "",
          app: "DIGIPOS",
          code: "0601010101",
          code_label: "",
          at_create: ControllerConfig.onCurrentTime().toString(),
          at_read: "",
          periode: infoDispute.periode,
          at_flag: 0
        });

      } catch (error) {
        returnMs.success = false;
        returnMs.message = error;
      }
    }
    if (disputeData.length == 0) {
      returnMs.success = false;
      returnMs.message = "No data dispute validate !";
    } else {
      if (returnMs.success == true) {
        returnMs.message = "success validate  [" + disputeData.length.toString() + "] data dispute !";
      }
    }
    return returnMs;
  }

  @post('/disputes/reject-dispute', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: RejectDataDisputeRequestBody
          }
        },
      },
    },
  })
  async RejectDispute(
    @requestBody(RejectDataDisputeRequestBody) disputeRejectBA: {
      reason_id: number, dispute_id: [], description: string
    },
  ): Promise<Object> {
    var returnMs: any; returnMs = {success: true, message: ""};

    var today = new Date();
    var digiposData = await this.dataDisputeRepository.find({where: {and: [{dispute_id: {inq: disputeRejectBA.dispute_id}}]}});

    for (var i in digiposData) {
      var infoDigipost = digiposData[i];
      var config = await this.dataconfigRepository.findOne({where: {and: [{id: disputeRejectBA.reason_id}, {config_group_id: 12}]}});
      if (config != null) {
        await this.dataDigiposRejectRepository.create({
          periode: infoDigipost.periode,
          ad_code: infoDigipost.ad_code,
          ad_name: infoDigipost.ad_name,
          config_id: disputeRejectBA.reason_id,
          reject_code: config?.config_code,
          reject_label: config?.config_desc,
          reject_description: "REJECT DISPUTE|BU|" + disputeRejectBA.description,
          create_at: today.toDateString(),
          user_reject: this.currentUserProfile[securityId],
          digipos_id: infoDigipost.dispute_id
        });

        await this.dataDisputeRepository.updateById(infoDigipost.dispute_id, {
          dispute_status: 3,
          note: disputeRejectBA.description
        });

        var userId = this.currentUserProfile[securityId];
        var roleNotifSetADB = ControllerConfig.onGetRoleNotifGroup("0601010101");
        var infoUser = await this.userRepository.findById(userId)
        await this.dataNotificationRepository.create({
          types: "NOTIFICATION",
          target: roleNotifSetADB.role,
          to: infoDigipost.ad_code,
          from_who: infoUser.username,
          subject: "Data Dispute Telah di tolak Untuk  " + infoDigipost.ad_code + " periode " + infoDigipost.periode + ", dengan reason : " + disputeRejectBA.description,
          body: disputeRejectBA.description,
          path_url: "",
          app: "DIGIPOS",
          code: "0601010101",
          code_label: "",
          at_create: ControllerConfig.onCurrentTime().toString(),
          at_read: "",
          periode: infoDigipost.periode,
          at_flag: 0
        });

        ControllerConfig.onSendNotif({
          types: "NOTIFICATION",
          target: roleNotifSetADB.role,
          to: infoDigipost.ad_code,
          from_who: infoUser.username,
          subject: "Data Dispute Telah di tolak Untuk  " + infoDigipost.ad_code + " periode " + infoDigipost.periode + ", dengan reason : " + disputeRejectBA.description,
          body: disputeRejectBA.description,
          path_url: "",
          app: "DIGIPOS",
          code: "0601010101",
          code_label: "",
          at_create: ControllerConfig.onCurrentTime().toString(),
          at_read: "",
          periode: infoDigipost.periode,
          at_flag: 0
        });

        // var messageFb = "Data  Dispute periode " + infoDigipost.periode + " telah di reject, tolong cek dispute anda !";
        // var dataMsg = {
        //   title: "Data  Dispute Rejected",
        //   message: messageFb,
        //   type: "DATA_DISPUTE_REJECT",
        //   to_role: "",
        //   to_user: infoDigipost.ad_code,
        //   at_create: ControllerConfig.onCurrentTime().toString(),
        //   status: "0",
        //   id_target: infoDigipost.dispute_id == undefined ? "0" : infoDigipost.dispute_id.toString(),
        //   for_app: "PORTAL_AD",
        //   periode: infoDigipost.periode
        // };
        // await this.dataNotificationRepository.create(dataMsg);
        // ControllerConfig.onSendMessageToSubscribe(dataMsg, (r: any) => {
        //   console.log(r);
        // });

      } else {
        returnMs.success = false;
        returnMs.message = "Reason not found, please entry other reason !";
      }
    }
    if (digiposData.length == 0) {
      returnMs.success = false;
      returnMs.message = "No data digipos rejected !";
    } else {
      if (returnMs.success == true) {
        returnMs.message = "success update [" + digiposData.length.toString() + "] data digipos !";
      }
    }
    return returnMs;
  }

  @post('/data-disputes', {
    responses: {
      '200': {
        description: 'DataDispute model instance',
        content: {'application/json': {schema: getModelSchemaRef(DataDispute)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(DataDispute, {
            title: 'NewDataDispute',
            exclude: ['dispute_id'],
          }),
        },
      },
    })
    dataDispute: Omit<DataDispute, 'dispute_id'>,
  ): Promise<DataDispute> {
    return this.dataDisputeRepository.create(dataDispute);
  }

  @get('/data-disputes/count', {
    responses: {
      '200': {
        description: 'DataDispute model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(DataDispute) where?: Where<DataDispute>,
  ): Promise<Count> {
    return this.dataDisputeRepository.count(where);
  }

  @get('/data-disputes', {
    responses: {
      '200': {
        description: 'Array of DataDispute model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(DataDispute, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(DataDispute) filter?: Filter<DataDispute>,
  ): Promise<[]> {
    var result: any; result = [];
    var disputeData = await this.dataDisputeRepository.find(filter);
    for (var i in disputeData) {
      var rawDispute: any; rawDispute = disputeData[i];
      var data: any; data = {}
      for (var j in rawDispute) {
        data[j] = rawDispute[j];
      }
      var diputeDet = await this.dataDisputeDetailRepository.find({where: {dispute_id: rawDispute.dispute_id}});
      data["dispute_detail"] = diputeDet;
      data["tolak_dispute"] = await this.dataDigiposRejectRepository.find({order: ["reject_id DESC"], where: {and: [{digipos_id: rawDispute.dispute_id}, {reject_code: {like: '0007-%'}}]}})

      result.push(data);
    }

    return result
  }

  @patch('/data-disputes', {
    responses: {
      '200': {
        description: 'DataDispute PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(DataDispute, {partial: true}),
        },
      },
    })
    dataDispute: DataDispute,
    @param.where(DataDispute) where?: Where<DataDispute>,
  ): Promise<Count> {
    return this.dataDisputeRepository.updateAll(dataDispute, where);
  }

  @get('/data-disputes/{id}', {
    responses: {
      '200': {
        description: 'DataDispute model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(DataDispute, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(DataDispute, {exclude: 'where'}) filter?: FilterExcludingWhere<DataDispute>
  ): Promise<DataDispute> {
    return this.dataDisputeRepository.findById(id, filter);
  }

  @patch('/data-disputes/{id}', {
    responses: {
      '204': {
        description: 'DataDispute PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(DataDispute, {partial: true}),
        },
      },
    })
    dataDispute: DataDispute,
  ): Promise<void> {
    await this.dataDisputeRepository.updateById(id, dataDispute);
  }

  @put('/data-disputes/{id}', {
    responses: {
      '204': {
        description: 'DataDispute PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() dataDispute: DataDispute,
  ): Promise<void> {
    await this.dataDisputeRepository.replaceById(id, dataDispute);
  }

  @del('/data-disputes/{id}', {
    responses: {
      '204': {
        description: 'DataDispute DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.dataDisputeRepository.deleteById(id);
  }
}
