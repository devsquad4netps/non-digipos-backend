import {authenticate} from '@loopback/authentication';
import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where
} from '@loopback/repository';
import {
  del, get,
  getModelSchemaRef, param,


  patch, post,




  put,

  requestBody
} from '@loopback/rest';
import {ScheduleData} from '../models';
import {ScheduleDataRepository} from '../repositories';


@authenticate('jwt')
export class ScheduleDataActionController {
  constructor(
    @repository(ScheduleDataRepository)
    public scheduleDataRepository: ScheduleDataRepository,
  ) { }

  @post('/schedule-data', {
    responses: {
      '200': {
        description: 'ScheduleData model instance',
        content: {'application/json': {schema: getModelSchemaRef(ScheduleData)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ScheduleData, {
            title: 'NewScheduleData',
            exclude: ['id'],
          }),
        },
      },
    })
    scheduleData: Omit<ScheduleData, 'id'>,
  ): Promise<ScheduleData> {
    return this.scheduleDataRepository.create(scheduleData);
  }

  @get('/schedule-data/count', {
    responses: {
      '200': {
        description: 'ScheduleData model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(ScheduleData) where?: Where<ScheduleData>,
  ): Promise<Count> {
    return this.scheduleDataRepository.count(where);
  }

  @get('/schedule-data', {
    responses: {
      '200': {
        description: 'Array of ScheduleData model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(ScheduleData, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(ScheduleData) filter?: Filter<ScheduleData>,
  ): Promise<ScheduleData[]> {
    return this.scheduleDataRepository.find(filter);
  }

  @patch('/schedule-data', {
    responses: {
      '200': {
        description: 'ScheduleData PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ScheduleData, {partial: true}),
        },
      },
    })
    scheduleData: ScheduleData,
    @param.where(ScheduleData) where?: Where<ScheduleData>,
  ): Promise<Count> {
    return this.scheduleDataRepository.updateAll(scheduleData, where);
  }

  @get('/schedule-data/{id}', {
    responses: {
      '200': {
        description: 'ScheduleData model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(ScheduleData, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(ScheduleData, {exclude: 'where'}) filter?: FilterExcludingWhere<ScheduleData>
  ): Promise<ScheduleData> {
    return this.scheduleDataRepository.findById(id, filter);
  }

  @patch('/schedule-data/{id}', {
    responses: {
      '204': {
        description: 'ScheduleData PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ScheduleData, {partial: true}),
        },
      },
    })
    scheduleData: ScheduleData,
  ): Promise<void> {
    await this.scheduleDataRepository.updateById(id, scheduleData);
  }

  @put('/schedule-data/{id}', {
    responses: {
      '204': {
        description: 'ScheduleData PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() scheduleData: ScheduleData,
  ): Promise<void> {
    await this.scheduleDataRepository.replaceById(id, scheduleData);
  }

  @del('/schedule-data/{id}', {
    responses: {
      '204': {
        description: 'ScheduleData DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.scheduleDataRepository.deleteById(id);
  }
}
