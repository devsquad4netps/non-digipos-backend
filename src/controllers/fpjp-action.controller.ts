// Uncomment these imports to begin using these cool features!

import {UserRepository} from '@loopback/authentication-jwt';
import {Filter, repository} from '@loopback/repository';
import {get, getModelSchemaRef, param, post, requestBody} from '@loopback/rest';
import {NotaDebet} from '../models';
import {AdMasterRepository, ApprovalMatrixMemberRepository, ApprovalMatrixRepository, ApprovalNotaRepository, DataconfigRepository, DataDigiposAttentionRepository, DataDigiposRepository, NotaDebetDetailRepository, NotaDebetRepository} from '../repositories';

// import {inject} from '@loopback/core';

export const FPJPNumberById = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['type_ad', 'nota_id'],
        properties: {
          type_ad: {
            type: 'string',
          },
          nota_id: {
            type: 'array',
            items: {type: 'number'},
          }
        }
      }
    },
  },
};

export const FPJPApproval = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['appraval_id'],
        properties: {
          appraval_id: {
            type: 'array',
            items: {type: 'number'},
          }
        }
      }
    },
  },
};

export class FpjpActionController {
  constructor(
    @repository(NotaDebetRepository)
    public notaDebetRepository: NotaDebetRepository,
    @repository(NotaDebetDetailRepository)
    public notaDebetDetailRepository: NotaDebetDetailRepository,
    @repository(AdMasterRepository)
    public adMasterRepository: AdMasterRepository,
    @repository(DataDigiposAttentionRepository)
    public dataDigiposAttentionRepository: DataDigiposAttentionRepository,
    @repository(DataDigiposRepository)
    public dataDigiposRepository: DataDigiposRepository,
    @repository(DataconfigRepository)
    public dataconfigRepository: DataconfigRepository,
    @repository(ApprovalMatrixRepository)
    public approvalMatrixRepository: ApprovalMatrixRepository,
    @repository(ApprovalMatrixMemberRepository)
    public approvalMatrixMemberRepository: ApprovalMatrixMemberRepository,
    @repository(ApprovalNotaRepository)
    public approvalNotaRepository: ApprovalNotaRepository,
    @repository(UserRepository)
    public userRepository: UserRepository,
  ) {
  }

  onNowDate = () => {
    var d = new Date();
    return d.getFullYear() + "-" + d.getMonth() + "-" + d.getDay();
  }

  onCheckLogin = (a: number, op: string, b: number) => {
    var status: boolean; status = false;
    switch (op) {
      case ">":
        status = a > b;
        break;
      case ">=":
        status = a >= b;
        break;
      case "<":
        status = a < b;
        break;
      case "<=":
        status = a <= b;
        break;
      case "==":
        status = a == b;
        break;
      case "!=":
        status = a != b;
        break;
      default:
        status = a == b;
        break;
    }
    return status;
  }

  @post('/fpjp/approve-fpjp', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: FPJPApproval
          }
        },
      },
    },
  })
  async ApproveFjp(
    @requestBody(FPJPApproval) FPJPApproval: {
      appraval_id: []
    },
  ): Promise<Object> {
    var returnMs: any; returnMs = {success: true, message: ""};

    var dataApprove = await this.approvalNotaRepository.find({where: {and: [{id: {inq: FPJPApproval.appraval_id}}, {state: 0}]}})
    if (dataApprove.length > 0) {
      for (var i in dataApprove) {
        var infoApprove = dataApprove[i];
        try {
          await this.approvalNotaRepository.updateById(infoApprove.id, {
            state: 1
          });
          var AccFPJP = false;
          var FalidRoot = true;
          var AllValidRoot = await this.approvalNotaRepository.find({where: {and: [{nota_debet_id: infoApprove.nota_debet_id}, {ap_level: 1}]}});
          for (var jRoot in AllValidRoot) {
            if (AllValidRoot[jRoot].state == 0) {
              FalidRoot = false;
            }
          }
          var AllValidLevel = await this.approvalNotaRepository.find({where: {and: [{nota_debet_id: infoApprove.nota_debet_id}, {ap_level: 2}]}});;
          if (FalidRoot == false) {
            var ValidLevel = true;
            for (var jRoot in AllValidRoot) {
              if (AllValidRoot[jRoot].state == 0) {
                FalidRoot = false;
              }
            }
            if (ValidLevel == true) {
              AccFPJP = true;
            }
          } else {
            //Valid
            AccFPJP = true;
          }
          if (AccFPJP == true) {
            await this.notaDebetRepository.updateById(infoApprove.nota_debet_id, {
              nota_state: 1
            });
          }
          returnMs.success = true;
          returnMs.message = "success update status approve !";
        } catch (error) {
          returnMs.success = false;
          returnMs.message = "Error Approve, falied update status approved !";
        }
      }
    } else {
      returnMs.success = false;
      returnMs.message = "Error Approve, not data approve exities !";
    }
    return returnMs;
  }



  @post('/fpjp/generate-number-id', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: FPJPNumberById
          }
        },
      },
    },
  })
  async GenerateNumberId(
    @requestBody(FPJPNumberById) FpjpRequest: {
      type_ad: string,
      nota_id: []
    },
  ): Promise<Object> {

    var returnMs: any; returnMs = {success: true, message: ""};
    var today = new Date();
    var MM = (today.getMonth() + 1);
    var YY = today.getFullYear();
    var DD = today.getDate();
    var DateZero = "00";
    var NumberFp = YY + '' + DateZero.substring(MM.toString().length, 2) + (today.getMonth() + 1) + '' + today.getDate();



    var condition: any; condition = [{nota_debet_id: {inq: FpjpRequest.nota_id}}, {acc_bu: 1}, {type_ad: FpjpRequest.type_ad}, {fpjp_number: ""}];
    if (FpjpRequest.type_ad == "AD_B") {
      condition.push({acc_ba: 1});
    }
    var DataNota = await this.notaDebetRepository.find({where: {and: condition}});
    if (DataNota.length > 0) {
      for (var i in DataNota) {
        var info = DataNota[i];
        var Zero = "0000";
        var id = info.nota_debet_id == undefined ? "0" : info.nota_debet_id;
        var NumberFPJP = NumberFp + "-" + Zero.substring(id.toString().length, 4) + id.toString();
        try {

          var TotalTransaksi = info.total_price;
          var LogicValidation = await this.approvalMatrixRepository.find({order: ["amd_index ASC"]});
          var DataMatrix: any;
          for (var i in LogicValidation) {
            var infoValidation = LogicValidation[i];
            if (infoValidation.amd_condition == "AND") {
              if (
                this.onCheckLogin(TotalTransaksi, infoValidation.amd_operator, infoValidation.amd_value) == true &&
                this.onCheckLogin(TotalTransaksi, infoValidation.amd_operator_2, infoValidation.amd_value_2) == true) {
                DataMatrix = infoValidation;
              }
            }
            if (infoValidation.amd_condition == "OR") {
              if (
                this.onCheckLogin(TotalTransaksi, infoValidation.amd_operator, infoValidation.amd_value) == true ||
                this.onCheckLogin(TotalTransaksi, infoValidation.amd_operator_2, infoValidation.amd_value_2) == true) {
                DataMatrix = infoValidation;
              }
            }
          }
          var GroupRoot = DataMatrix.amg_id_root;
          var Group1 = DataMatrix.amg_id_level1;
          var Group2 = DataMatrix.amg_id_level2;

          var MemberApproval = await this.approvalMatrixMemberRepository.find({where: {and: [{amg_id: {inq: [GroupRoot, Group1, Group2]}}, {amm_state: true}]}});
          for (var iUser in MemberApproval) {
            var InfoMember = MemberApproval[iUser];
            var UserID = InfoMember.amm_userid;
            var ap_level = 1;
            if (InfoMember.amg_id != GroupRoot) {
              ap_level = 2;
            }


            await this.approvalNotaRepository.create({
              user_id: UserID,
              nota_debet_id: info.nota_debet_id,
              state: 0,
              ap_level: ap_level,
              at_create: new Date().toLocaleString()
            });
          }
          await this.notaDebetRepository.updateById(info.nota_debet_id, {
            fpjp_number: NumberFPJP,
            req_acc_fpjp: 1,
            generate_fpjp: 0
          });
          returnMs.success = true;
          returnMs.message = "success generate number fpjp !";
        } catch (error) {
          returnMs.success = false;
          returnMs.message = error;
        }
      }
    } else {
      returnMs.success = false;
      returnMs.message = "Error generate number fpjp, not found nota avaliable !";
    }
    return returnMs;
  }



  @post('/fpjp/generate-number-all', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: FPJPNumberById
          }
        },
      },
    },
  })
  async GenerateNumberAll(
    @requestBody(FPJPNumberById) FpjpRequest: {
      type_ad: string
    },
  ): Promise<Object> {
    var returnMs: any; returnMs = {success: true, message: ""};
    var today = new Date();
    var MM = (today.getMonth() + 1);
    var YY = today.getFullYear();
    var DD = today.getDate();
    var DateZero = "00";
    var NumberFp = YY + '' + DateZero.substring(MM.toString().length, 2) + (today.getMonth() + 1) + '' + today.getDate();

    var condition: any; condition = [{acc_bu: 1}, {type_ad: FpjpRequest.type_ad}, {fpjp_number: ""}];
    if (FpjpRequest.type_ad == "AD_B") {
      condition.push({acc_ba: 1});
    }
    var DataNota = await this.notaDebetRepository.find({where: {and: condition}});
    if (DataNota.length > 0) {
      for (var i in DataNota) {
        var info = DataNota[i];
        var Zero = "0000";
        var id = info.nota_debet_id == undefined ? "0" : info.nota_debet_id;
        var NumberFPJP = NumberFp + "-" + Zero.substring(id.toString().length, 4) + id.toString();
        try {

          var TotalTransaksi = info.total_price;
          var LogicValidation = await this.approvalMatrixRepository.find({order: ["amd_index ASC"]});
          var DataMatrix: any;
          for (var i in LogicValidation) {
            var infoValidation = LogicValidation[i];
            if (infoValidation.amd_condition == "AND") {
              if (
                this.onCheckLogin(TotalTransaksi, infoValidation.amd_operator, infoValidation.amd_value) == true &&
                this.onCheckLogin(TotalTransaksi, infoValidation.amd_operator_2, infoValidation.amd_value_2) == true) {
                DataMatrix = infoValidation;
              }
            }
            if (infoValidation.amd_condition == "OR") {
              if (
                this.onCheckLogin(TotalTransaksi, infoValidation.amd_operator, infoValidation.amd_value) == true ||
                this.onCheckLogin(TotalTransaksi, infoValidation.amd_operator_2, infoValidation.amd_value_2) == true) {
                DataMatrix = infoValidation;
              }
            }
          }
          var GroupRoot = DataMatrix.amg_id_root;
          var Group1 = DataMatrix.amg_id_level1;
          var Group2 = DataMatrix.amg_id_level2;

          var MemberApproval = await this.approvalMatrixMemberRepository.find({where: {and: [{amg_id: {inq: [GroupRoot, Group1, Group2]}}, {amm_state: true}]}});
          for (var iUser in MemberApproval) {
            var InfoMember = MemberApproval[iUser];
            var UserID = InfoMember.amm_userid;
            var ap_level = 1;
            if (InfoMember.amg_id != GroupRoot) {
              ap_level = 2;
            }
            await this.approvalNotaRepository.create({
              user_id: UserID,
              nota_debet_id: info.nota_debet_id,
              state: 0,
              ap_level: ap_level,
              at_create: this.onNowDate()
            });
          }
          await this.notaDebetRepository.updateById(info.nota_debet_id, {
            fpjp_number: NumberFPJP,
            req_acc_fpjp: 1,
            generate_fpjp: 0
          });
          returnMs.success = true;
          returnMs.message = "success generate number fpjp !";
        } catch (error) {
          returnMs.success = false;
          returnMs.message = error;
        }
      }
    } else {
      returnMs.success = false;
      returnMs.message = "Error generate number fpjp, not found nota avaliable !";
    }
    return returnMs;
  }


  @get('/fpjp', {
    responses: {
      '200': {
        description: 'Array of NotaDebet model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(NotaDebet, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(NotaDebet) filter?: Filter<NotaDebet>,
  ): Promise<Object> {
    var info: any; info = {"datas": new Array(), "size": 0};
    var filterOrder = {};
    if (filter?.where !== undefined) {
      filterOrder = {"where": filter?.where};
    }

    var infoFileSyc = await this.notaDebetRepository.find(filter);
    var sizeFileSyc = await this.notaDebetRepository.find(filterOrder);
    var NotaList: any; NotaList = [];
    for (var i in infoFileSyc) {
      var data: any; data = {};
      var infoNota = infoFileSyc[i];
      // data = infoNota;
      for (var key in infoNota) {
        var inFo: any; inFo = infoNota
        data[key] = inFo[key];
      }
      var infoDigipost: any; infoDigipost = {};
      var digipos: any; digipos = {};
      if (infoNota.type_ad == "AD_A") {
        infoDigipost = await this.dataDigiposRepository.findOne({where: {ba_number_a: infoNota.bast_number}});
        if (infoDigipost != undefined && infoDigipost != null) {
          digipos = {
            trx: infoDigipost.trx_cluster_out,
            dpp: infoDigipost.dpp_fee_amount,
            ppn: infoDigipost.vat_fee_amount,
            pph: infoDigipost.pph_fee_amount,
            total: infoDigipost.total_fee_amount
          };
        }
      }
      if (infoNota.type_ad == "AD_B") {
        infoDigipost = await this.dataDigiposRepository.findOne({where: {ba_number_b: infoNota.bast_number}});
        if (infoDigipost.digipost_id != undefined || infoDigipost.digipost_id != null) {
          digipos = {
            trx: infoDigipost.trx_paid_invoice,
            dpp: infoDigipost.dpp_paid_amount,
            ppn: infoDigipost.vat_paid_amount,
            pph: infoDigipost.pph_paid_amount,
            total: infoDigipost.total_paid_amount
          };
        }
      }

      var config: any; config = [];
      var settingConfig = await this.dataconfigRepository.find({where: {and: [{config_group_id: 5}, {state: 1}]}});
      for (var iC in settingConfig) {
        var infoCon = settingConfig[iC];
        if (infoCon.config_code == "0005-01") {
          config.push({description: infoCon.config_desc, code: infoCon.label_code, persentase: infoCon.config_value})
        }
        if (infoCon.config_code == "0005-02") {
          config.push({description: infoCon.config_desc, code: infoCon.label_code, persentase: infoCon.config_value})
        }
        if (infoCon.config_code == "0005-03") {
          config.push({description: infoCon.config_desc, code: infoCon.label_code, persentase: infoCon.config_value})
        }
      }
      data["config"] = config;

      data["digipos_ad"] = digipos;
      var infoAD = await this.adMasterRepository.findById(infoNota.ad_code);
      data["master_ad"] = {
        ad_code: infoAD.ad_code,
        ad_name: infoAD.ad_name,
        address: infoAD.address,
        city: infoAD.city,
        post_code: infoAD.post_code,
        npwp: infoAD.npwp,
        npwp_attachment: infoAD.npwp_attachment,
        pks_attachment: infoAD.pks_attachment,
        attention: infoAD.attention,
        attention_position: infoAD.attention_position,
        phone_number: infoAD.phone_number,
        email: infoAD.email,
        contract_number: infoAD.contract_number,
        join_date: infoAD.join_date,
        ad_status: infoAD.ad_status,
        prefix: infoAD.prefix,
        no_rek: infoAD.no_rek,
        name_rekening: infoAD.name_rekening,
        name_bank: infoAD.name_bank
      }
      NotaList.push(data);
    }

    info["datas"] = NotaList;
    info["size"] = sizeFileSyc.length;

    return info;
    // return this.notaDebetRepository.find(filter);
  }

  @get('/fpjp/need-approval/{user}', {
    responses: {
      '200': {
        description: 'Array of NotaDebet model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(NotaDebet, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async findApproval(
    @param.path.string('user') user: string
  ): Promise<Object> {
    var appNota = await this.approvalNotaRepository.find({where: {and: [{user_id: user}, {state: 0}]}});
    var data: any; data = [];
    for (var i in appNota) {
      var infoAppNota = appNota[i];
      var notaInfo = await this.notaDebetRepository.findById(infoAppNota.nota_debet_id);
      data.push({approval_id: infoAppNota.id, info: notaInfo});
    }
    // console.log(user);
    return data;
    // return this.notaDebetRepository.find(filter);
  }


  @get('/fpjp/status-approval/{nota}', {
    responses: {
      '200': {
        description: 'Array of NotaDebet model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(NotaDebet, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async findApprovalStatus(
    @param.path.number('nota') nota: number
  ): Promise<Object> {
    var appNota = await this.approvalNotaRepository.find({where: {and: [{nota_debet_id: nota}]}});
    var data: any; data = [];
    for (var i in appNota) {
      var infoAppNota = appNota[i];
      var notaInfo = await this.notaDebetRepository.findById(infoAppNota.nota_debet_id);
      var infoUser = await this.userRepository.findById(infoAppNota.user_id);
      data.push({approval_id: infoAppNota.id, state: infoAppNota.state, user_info: infoUser, info: notaInfo});
    }
    // console.log(user);
    return data;
    // return this.notaDebetRepository.find(filter);
  }


}
