import {MysqlDataSource} from '../datasources';
import {AdMasterRepository, AlertPortalAdRepository, DataconfigRepository, DataDigiposRepository, DataDistributeBukpotRepository, DataFpjpRepository} from '../repositories';
import {ControllerConfig} from './controller.config';

export class AlertPortalAdControler {

  constructor() { }

  static async onCallProgressPortal(db: MysqlDataSource) {

    var periodData = ControllerConfig.onPeriodeDigipos();
    var adMaster = new AdMasterRepository(db);
    var alertPortalAd = new AlertPortalAdRepository(db);
    var digipos = new DataDigiposRepository(db);
    var bukpot = new DataDistributeBukpotRepository(db);
    var configProgres = new DataconfigRepository(db);
    var dataFPJP = new DataFpjpRepository(db);

    var timeLine = {"UPLOAD_INVOICE": 25, "UPLOAD_NOTA": 25, "UPLOAD_DISPUTE": 25, "REMANDER": 5};

    // data config
    var dataConfig = await configProgres.find({where: {config_group_id: 18}});
    for (var iConfig in dataConfig) {
      var rawConfig = dataConfig[iConfig];
      //
      if (rawConfig.config_code == "0013-01") {
        timeLine["UPLOAD_INVOICE"] = rawConfig.config_value;
      }
      //
      if (rawConfig.config_code == "0013-02") {
        timeLine["UPLOAD_NOTA"] = rawConfig.config_value;
      }
      //
      if (rawConfig.config_code == "0013-03") {
        timeLine["UPLOAD_DISPUTE"] = rawConfig.config_value;
      }
      //
      if (rawConfig.config_code == "0013-04") {
        timeLine["REMANDER"] = rawConfig.config_value;
      }

    }

    var dateNow = new Date();
    var dateTimeLine = new Date(dateNow.getFullYear() + "-" + dateNow.getMonth() + "-" + dateNow.getDate());

    var lastInvoiceUpload = new Date(periodData + "-" + timeLine["UPLOAD_INVOICE"]);
    var lastNota = new Date(periodData + "-" + timeLine["UPLOAD_NOTA"]);
    var lastDispute = new Date(periodData + "-" + timeLine["UPLOAD_DISPUTE"]);

    var rangeDateInvoiceTime = lastInvoiceUpload.getTime() - dateTimeLine.getTime();
    var rangeDateInvoiceDay = rangeDateInvoiceTime / (1000 * 3600 * 24);

    var rangeDateNotaTime = lastNota.getTime() - dateTimeLine.getTime();
    var rangeDateNotaDay = rangeDateNotaTime / (1000 * 3600 * 24);

    var rangeDateDisputeTime = lastDispute.getTime() - dateTimeLine.getTime();
    var rangeDateDisputeDay = rangeDateDisputeTime / (1000 * 3600 * 24);


    if (isNaN(rangeDateInvoiceDay) == true || isNaN(rangeDateNotaDay) == true || isNaN(rangeDateDisputeDay) == true) {
      console.log("Config Invoice", rangeDateInvoiceDay);
      console.log("Config Nota", rangeDateNotaDay);
      console.log("Config Dispute", rangeDateDisputeDay);
    } else {
      //data digipos


      // console.log("Config Invoice", rangeDateInvoiceDay.toFixed(0));
      // console.log("Config Nota", rangeDateNotaDay.toFixed(0));
      // console.log("Config Dispute", rangeDateDisputeDay.toFixed(0));

      var rawDataProgres: any; rawDataProgres = {};
      var infoDigipos = await digipos.find({where: {trx_date_group: periodData}});
      for (var iDigipos in infoDigipos) {
        var rawDigipos = infoDigipos[iDigipos];
        if (rawDataProgres[rawDigipos.ad_code] == undefined) {
          rawDataProgres[rawDigipos.ad_code] = {};
        }

        rawDataProgres[rawDigipos.ad_code]["share_bar_a"] = rawDigipos.google_drive_a;
        rawDataProgres[rawDigipos.ad_code]["at_share_bar_a"] = ControllerConfig.onCurrentTime().toString();
        rawDataProgres[rawDigipos.ad_code]["share_bar_b"] = rawDigipos.google_drive_b;
        rawDataProgres[rawDigipos.ad_code]["at_share_bar_b"] = ControllerConfig.onCurrentTime().toString();
        rawDataProgres[rawDigipos.ad_code]["share_inv"] = rawDigipos.distribute_file_invoice;
        rawDataProgres[rawDigipos.ad_code]["at_share_inv"] = ControllerConfig.onCurrentTime().toString();
        rawDataProgres[rawDigipos.ad_code]["share_inv_sign"] = rawDigipos.distribute_invoice_ttd == 2 ? 1 : 0;
        rawDataProgres[rawDigipos.ad_code]["at_share_inv_sign"] = ControllerConfig.onCurrentTime().toString();
        rawDataProgres[rawDigipos.ad_code]["share_fp"] = rawDigipos.vat_distribute == 2 ? 1 : 0;
        rawDataProgres[rawDigipos.ad_code]["at_share_fp"] = ControllerConfig.onCurrentTime().toString();
        rawDataProgres[rawDigipos.ad_code]["share_bukpot"] = 0;
        rawDataProgres[rawDigipos.ad_code]["at_share_bukpot"] = "";

        var infoBukpot = await bukpot.findOne({where: {and: [{periode: periodData}, {ad_code: rawDigipos.ad_code}]}});
        if (infoBukpot != null && infoBukpot.ad_code != undefined && infoBukpot != undefined) {
          rawDataProgres[rawDigipos.ad_code]["share_bukpot"] = infoBukpot.bukpot_status == 2 ? 1 : 0;
          rawDataProgres[rawDigipos.ad_code]["at_share_bukpot"] = ControllerConfig.onCurrentTime().toString();
        }

        var countInvoice = await dataFPJP.count({and: [{periode: periodData}, {ad_code: rawDigipos.ad_code}, {type_trx: 1}, {state: {neq: 0}}]});
        var countNota = await dataFPJP.count({and: [{periode: periodData}, {ad_code: rawDigipos.ad_code}, {type_trx: 1}, {state: {neq: 0}}]});


        rawDataProgres[rawDigipos.ad_code]["submit_inv"] = countInvoice.count > 0 ? 1 : 0;
        rawDataProgres[rawDigipos.ad_code]["warning_inv"] = rangeDateInvoiceDay > 3 ? 0 : countInvoice.count > 0 ? 0 : 1;
        rawDataProgres[rawDigipos.ad_code]["level_inv"] = rangeDateInvoiceDay.toFixed(0);


        rawDataProgres[rawDigipos.ad_code]["submit_nota"] = countNota.count > 0 ? 1 : 0;
        rawDataProgres[rawDigipos.ad_code]["warning_nota"] = rangeDateNotaDay > 3 ? 0 : countNota.count > 0 ? 0 : 1;
        rawDataProgres[rawDigipos.ad_code]["level_nota"] = rangeDateNotaDay.toFixed(0);


      }

      var rawMasterAd = await adMaster.find();
      for (var i in rawMasterAd) {
        var raw = rawMasterAd[i];
        if (rawDataProgres[raw.ad_code] != undefined) {
          var rawAlert = await alertPortalAd.find({where: {and: [{periode: periodData}, {ad_code: raw.ad_code}]}});
          if (rawAlert.length > 0) {
            for (var iAlert in rawAlert) {
              var rawAlertPortal = rawAlert[iAlert];

              if (rawAlertPortal.share_bar_a == 0 && rawDataProgres[raw.ad_code]["share_bar_a"] == 1) { } else {
                delete rawDataProgres[raw.ad_code]["at_share_bar_a"];
              }

              if (rawAlertPortal.share_bar_b == 0 && rawDataProgres[raw.ad_code]["share_bar_b"] == 1) { } else {
                delete rawDataProgres[raw.ad_code]["at_share_bar_b"];
              }

              if (rawAlertPortal.share_inv == 0 && rawDataProgres[raw.ad_code]["share_inv"] == 1) { } else {
                delete rawDataProgres[raw.ad_code]["at_share_inv"];
              }

              if (rawAlertPortal.share_inv == 0 && rawDataProgres[raw.ad_code]["share_inv"] == 1) { } else {
                delete rawDataProgres[raw.ad_code]["at_share_inv"];
              }

              if (rawAlertPortal.share_inv_sign == 0 && rawDataProgres[raw.ad_code]["share_inv_sign"] == 1) { } else {
                delete rawDataProgres[raw.ad_code]["at_share_inv_sign"];
              }

              if (rawAlertPortal.share_fp == 0 && rawDataProgres[raw.ad_code]["share_fp"] == 1) { } else {
                delete rawDataProgres[raw.ad_code]["at_share_fp"];
              }

              if (rawAlertPortal.share_bukpot == 0 && rawDataProgres[raw.ad_code]["share_bukpot"] == 1) { } else {
                delete rawDataProgres[raw.ad_code]["at_share_bukpot"];
              }

              rawDataProgres[raw.ad_code]["ad_code"] = raw.ad_code;
              rawDataProgres[raw.ad_code]["periode"] = periodData;
              rawDataProgres[raw.ad_code]["at_update"] = ControllerConfig.onCurrentTime().toString();
              alertPortalAd.updateById(rawAlertPortal.id, rawDataProgres[raw.ad_code]);

            }
          } else {
            rawDataProgres[raw.ad_code]["ad_code"] = raw.ad_code;
            rawDataProgres[raw.ad_code]["periode"] = periodData;
            rawDataProgres[raw.ad_code]["at_update"] = ControllerConfig.onCurrentTime().toString();
            alertPortalAd.create(rawDataProgres[raw.ad_code]);
            // console.log(rawDataProgres);
          }
        }
      }
    }
    // console.log("MASTER : ", await adMaster.count());

  }

}
