import {authenticate} from '@loopback/authentication';
import {inject} from '@loopback/core';
import {repository} from '@loopback/repository';
import {post, requestBody, ResponseObject} from '@loopback/rest';
import {SecurityBindings, securityId, UserProfile} from '@loopback/security';
import ejs from "ejs";
import fs from 'fs';
import pdf from "html-pdf";
import path from 'path';
import PDFMerger from 'pdf-merger-js';
import {AdClusterRepository, AdMasterRepository, DataDigiposAttentionRepository, DataDigiposDetailRepository, DataDigiposRejectRepository, DataDigiposRepository, DataSumarryRepository, TransaksiTypeDetailRepository, TransaksiTypeRepository} from '../repositories';
import {ControllerConfig} from './controller.config';



export const RequestGenerateDocBARNumber: ResponseObject = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['DIGIPOS_ID', 'LAST_NUMBER', 'DOC_LABEL'],
        properties: {
          DIGIPOS_ID: {
            type: 'array',
            items: {type: 'number'},
          },
          LAST_NUMBER: {
            type: 'number'
          },
          DOC_LABEL: {
            type: 'string'
          }
        }
      }
    },
  },
};

export const RequestReGenerateDocBAR: ResponseObject = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['DIGIPOS_ID'],
        properties: {
          DIGIPOS_ID: {
            type: 'array',
            items: {type: 'number'},
          }
        }
      }
    },
  },
};


var thisContent: any;
var GENERATE_DOCUMENT_BAR: any; GENERATE_DOCUMENT_BAR = {};


@authenticate('jwt')
export class TransaksiGenerateDocController {
  constructor(
    @repository(DataDigiposRepository)
    public dataDigiposRepository: DataDigiposRepository,
    @repository(DataDigiposDetailRepository)
    public dataDigiposDetailRepository: DataDigiposDetailRepository,
    @repository(DataDigiposRejectRepository)
    public dataDigiposRejectRepository: DataDigiposRejectRepository,
    @repository(AdMasterRepository)
    public adMasterRepository: AdMasterRepository,
    @repository(TransaksiTypeRepository)
    public transaksiTypeRepository: TransaksiTypeRepository,
    @repository(DataDigiposAttentionRepository)
    public dataDigiposAttentionRepository: DataDigiposAttentionRepository,
    @repository(TransaksiTypeDetailRepository)
    public transaksiTypeDetailRepository: TransaksiTypeDetailRepository,
    @repository(AdClusterRepository)
    public adClusterRepository: AdClusterRepository,
    @repository(DataSumarryRepository)
    public dataSumarryRepository: DataSumarryRepository,

    @inject(SecurityBindings.USER)
    public currentUserProfile: UserProfile,
  ) {
    thisContent = this;
  }


  ON_GENERATE_DOCUMENT_BAR_PROSESS = async () => {
    console.log("generate bar start...")
    for (var INDEX_DOCUMENT in GENERATE_DOCUMENT_BAR) {

      var TYPE_TRX = GENERATE_DOCUMENT_BAR[INDEX_DOCUMENT]["TYPE"];
      var DATA_DOCUMENT = GENERATE_DOCUMENT_BAR[INDEX_DOCUMENT]["DATA"];

      var infoPihakLinkAja = await this.dataDigiposAttentionRepository.findOne({where: {and: [{set_attention: true}, {group: 1}]}});
      var infoPihakAD = await this.adMasterRepository.findOne({where: {ad_code: DATA_DOCUMENT.ad_code}});

      var BAR_NUMBER = TYPE_TRX == "AD_A" ? DATA_DOCUMENT.ba_number_a : DATA_DOCUMENT.ba_number_b;

      console.log(TYPE_TRX, BAR_NUMBER);

      var NameFileBA = BAR_NUMBER.split("/").join("_") + "_" + DATA_DOCUMENT.trx_date_group + "_" + DATA_DOCUMENT.ad_code + ".pdf";
      var pathFile = "";
      var pathFile1 = "";
      var pathFile2 = "";
      const fileBlankPath = path.join(__dirname, '../../.digipost/template_ba/blank_signature.PNG');
      const contentsBlank = fs.readFileSync(fileBlankPath, {encoding: 'base64'});
      var SignImageA = contentsBlank;
      var SignImageB = contentsBlank;



      const filePath = path.join(__dirname, '../../.digipost/sign_atten/' + infoPihakLinkAja?.sign_attention);
      const contents = fs.readFileSync(filePath, {encoding: 'base64'});

      if (TYPE_TRX == "AD_A") {
        pathFile = path.join(__dirname, '../../.digipost/berita_acara/ad_a/') + NameFileBA;
        pathFile1 = path.join(__dirname, '../../.digipost/berita_acara/ad_a/1') + NameFileBA;
        pathFile2 = path.join(__dirname, '../../.digipost/berita_acara/ad_a/2') + NameFileBA;
        if (DATA_DOCUMENT.sign_off_a == 1) {
          SignImageA = contents;
        }
      }

      if (TYPE_TRX == "AD_B") {
        pathFile = path.join(__dirname, '../../.digipost/berita_acara/ad_b/') + NameFileBA;
        pathFile1 = path.join(__dirname, '../../.digipost/berita_acara/ad_b/1') + NameFileBA;
        pathFile2 = path.join(__dirname, '../../.digipost/berita_acara/ad_b/2') + NameFileBA;
        if (DATA_DOCUMENT.sign_off_b == 1) {
          SignImageB = contents;
        }
      }


      //INFORMATION PDF FILE UNTUK BERITA ACARA
      var DataBA_PDF: any;
      DataBA_PDF = {
        TypeAD: TYPE_TRX == "AD_A" ? "invoice" : "paid  invoice",
        NoBA: "",
        DateBACreate: "",
        //PIHAK 1
        PihakICompany: "",
        PihakIName: "",
        PihakIPosition: "",
        PihakIAddress: "",
        PihakISign: SignImageA,

        //PIHAK 2
        PihakIICompany: "",
        PihakIIName: "",
        PihakIIPosition: "",
        PihakIIAddress: "",
        PihakIISign: SignImageB,


        //DESCRIPTION
        NoContract: "",
        NominalString: "",

        //TABLE
        TotalTrxCount: 0,
        TotalTrxAmount: 0,
        DataTransaction: {},
        ketStore: [],
        TitleShare: ""
      }

      DataBA_PDF.TitleShare = TYPE_TRX == "AD_A" ? "Mitra SBP" : "LinkAja";
      DataBA_PDF.NoBA = BAR_NUMBER;

      DataBA_PDF.PihakIName = TYPE_TRX == "AD_A" ? infoPihakLinkAja?.attention_name : infoPihakAD?.attention;
      DataBA_PDF.PihakIPosition = TYPE_TRX == "AD_A" ? infoPihakLinkAja?.attention_position : infoPihakAD?.attention_position;
      DataBA_PDF.PihakIAddress = TYPE_TRX == "AD_A" ? infoPihakLinkAja?.address : infoPihakAD?.npwp_address;
      DataBA_PDF.PihakICompany = TYPE_TRX == "AD_A" ? infoPihakLinkAja?.company : infoPihakAD?.ad_name;

      DataBA_PDF.PihakIIName = TYPE_TRX == "AD_A" ? infoPihakAD?.attention : infoPihakLinkAja?.attention_name;
      DataBA_PDF.PihakIIPosition = TYPE_TRX == "AD_A" ? infoPihakAD?.attention_position : infoPihakLinkAja?.attention_position;
      DataBA_PDF.PihakIIAddress = TYPE_TRX == "AD_A" ? infoPihakAD?.npwp_address : infoPihakLinkAja?.address;
      DataBA_PDF.PihakIICompany = TYPE_TRX == "AD_A" ? infoPihakAD?.ad_name : infoPihakLinkAja?.company;

      DataBA_PDF.NoContract = infoPihakAD?.contract_number;

      //TRANSAKSI TYPE
      var TYPE_TRANSAKSI = await this.transaksiTypeRepository.find();
      var STORE_TT: any; STORE_TT = {};
      for (var INDEX_TT in TYPE_TRANSAKSI) {
        var RAW_TT = TYPE_TRANSAKSI[INDEX_TT];
        var ID_TT = RAW_TT.TT_ID == undefined ? 0 : RAW_TT.TT_ID;
        STORE_TT[ID_TT] = RAW_TT;
      }

      //TRANSAKSI DETAIL
      var TRANSAKSI_DETAIL = await this.dataDigiposDetailRepository.find({where: {digipost_id: DATA_DOCUMENT.digipost_id}});

      var transaction: any; transaction = {FEE: {data: {}, TotalTrx: 0, TotalAmout: 0}, PAID: {data: {}, TotalTrx: 0, TotalAmout: 0}};
      var TotalTrxFee = 0;
      var TotalAmoutFee = 0;
      var TotalTrxPaid = 0;
      var TotalAmoutPaid = 0;


      var FeeKey: any; FeeKey = [];
      var PaidKey: any; PaidKey = [];
      for (var i in TRANSAKSI_DETAIL) {
        var TransactionBA = TRANSAKSI_DETAIL[i];
        DataBA_PDF.TitleDataRaw = TransactionBA.trx_date_group;
        DataBA_PDF.PeriodeData = TransactionBA.trx_date_group;

        var share_for_fee: string; share_for_fee = Math.round(STORE_TT[TransactionBA.type_trx]["TT_BOBOT_A"]).toString() + "%";
        var share_for_paid: string; share_for_paid = Math.round(STORE_TT[TransactionBA.type_trx]["TT_BOBOT_B"]).toString() + "%";

        var thisDateFee = new Date(TransactionBA.trx_date_group + "-01");
        var Mount = ControllerConfig.onGetMonth(thisDateFee.getMonth());
        var DescData = "Transaksi " + Mount + " " + thisDateFee.getFullYear();
        var thisPeriode = TransactionBA.trx_date_group;

        if (transaction.FEE.data[thisPeriode] == undefined) {
          transaction.FEE.data[thisPeriode] = {raw: [], rowspan: 0, desc: DescData};
          FeeKey.push(thisPeriode);
        }

        if (transaction.PAID.data[thisPeriode] == undefined) {
          transaction.PAID.data[thisPeriode] = {raw: [], rowspan: 0, desc: DescData};
          PaidKey.push(thisPeriode)
        }


        //NOMINAL FEE
        if (TransactionBA.trx_cluster_out != 0.00) {
          transaction.FEE.data[thisPeriode].raw.push({
            desc: DescData,
            type: STORE_TT[TransactionBA.type_trx]["TT_LABEL"],
            sales: ControllerConfig.onGeneratePuluhan(TransactionBA.sales_fee),
            count_trx: ControllerConfig.onGeneratePuluhan(TransactionBA.trx_cluster_out),
            total_amout: ControllerConfig.onGeneratePuluhan(TransactionBA.total_fee_amount),
            share_for: share_for_fee
          });
          transaction.FEE.data[thisPeriode].rowspan = transaction.FEE.data[thisPeriode].rowspan + 1;
        }

        //NOMINAL PAID
        if (TransactionBA.trx_paid_invoice != 0.00) {

          transaction.PAID.data[thisPeriode].raw.push({
            desc: DescData,
            type: STORE_TT[TransactionBA.type_trx]["TT_LABEL"],
            sales: ControllerConfig.onGeneratePuluhan(TransactionBA.sales_paid),
            count_trx: ControllerConfig.onGeneratePuluhan(TransactionBA.trx_paid_invoice),
            total_amout: ControllerConfig.onGeneratePuluhan(TransactionBA.total_paid_amount),
            share_for: share_for_paid
          });
          transaction.PAID.data[thisPeriode].rowspan = transaction.PAID.data[thisPeriode].rowspan + 1;
        }


        TotalTrxFee = TotalTrxFee + TransactionBA.trx_cluster_out;
        TotalAmoutFee = TotalAmoutFee + TransactionBA.total_fee_amount;
        TotalTrxPaid = TotalTrxPaid + TransactionBA.trx_paid_invoice;
        TotalAmoutPaid = TotalAmoutPaid + TransactionBA.total_paid_amount;
      }

      transaction.FEE.TotalTrx = TotalTrxFee;
      transaction.FEE.TotalAmout = TotalAmoutFee;
      transaction.PAID.TotalTrx = TotalTrxPaid;
      transaction.PAID.TotalAmout = TotalAmoutPaid;

      DataBA_PDF.TotalTrxCount = TYPE_TRX == "AD_A" ? ControllerConfig.onGeneratePuluhan(transaction.FEE.TotalTrx) : ControllerConfig.onGeneratePuluhan(transaction.PAID.TotalTrx);
      DataBA_PDF.TotalTrxAmount = TYPE_TRX == "AD_A" ? ControllerConfig.onGeneratePuluhan(transaction.FEE.TotalAmout) : ControllerConfig.onGeneratePuluhan(transaction.PAID.TotalAmout);
      DataBA_PDF.DataTransaction = TYPE_TRX == "AD_A" ? transaction.FEE.data : transaction.PAID.data;
      DataBA_PDF.ketStore = TYPE_TRX == "AD_A" ? FeeKey : PaidKey;

      var TrxAmountStringFee = "Rp" + ControllerConfig.onGeneratePuluhan(transaction.FEE.TotalAmout) + " (" + ControllerConfig.onAngkaTerbilang(transaction.FEE.TotalAmout) + " Rupiah)";
      var TrxAmountStringPaid = "Rp" + ControllerConfig.onGeneratePuluhan(transaction.PAID.TotalAmout) + " (" + ControllerConfig.onAngkaTerbilang(transaction.PAID.TotalAmout) + " Rupiah)";

      var PeriodeTrx = DataBA_PDF.PeriodeData.split("-");
      var dateLast = new Date(PeriodeTrx[0], PeriodeTrx[1], 0);
      var dateAD_B = ControllerConfig.onGetDayString(dateLast.getDay()) + ", " + dateLast.getDate() + " " + ControllerConfig.onGetMonth(dateLast.getMonth()) + " " + dateLast.getFullYear();



      DataBA_PDF.NominalString = TYPE_TRX == "AD_A" ? TrxAmountStringFee : TrxAmountStringPaid;
      DataBA_PDF.DateBACreate = TYPE_TRX == "AD_A" ? ControllerConfig.onBANowDate() : dateAD_B;

      //console.log(digipos?.trx_date_group, " | ", PeriodeDispute, "|", InfoBA.number_ba, pathFile1, pathFile2, pathFile);

      //MAKE FILE PDF BA
      var merger = new PDFMerger();
      var compiled = ejs.compile(fs.readFileSync(path.join(__dirname, '../../.digipost/template_ba/') + 'pdf_ba.html', 'utf8'));
      var html = compiled(DataBA_PDF);

      var config: any; config = {
        format: "A4",
        orientation: "portrait",
        "footer": {
          "height": "70px",
          "contents": ''
        }
      }

      pdf.create(html, config).toFile(pathFile1, async (err1: any, res1: any) => {
        if (err1) {
          delete GENERATE_DOCUMENT_BAR[INDEX_DOCUMENT];
          this.ON_GENERATE_DOCUMENT_BAR_PROSESS();
          console.log(err1);
        } else {
          pdf.create(html).toFile(pathFile2, async (err2: any, res2: any) => {
            if (err2) {
              delete GENERATE_DOCUMENT_BAR[INDEX_DOCUMENT];
              this.ON_GENERATE_DOCUMENT_BAR_PROSESS();
              console.log(err2);
            } else {

              merger.add(pathFile1);
              merger.add(pathFile2);
              await merger.save(pathFile);
              fs.unlinkSync(pathFile1);
              fs.unlinkSync(pathFile2);
              if (fs.existsSync(pathFile)) {
                this.ON_GENERATE_SUMMARY(DATA_DOCUMENT, TYPE_TRX, async () => {

                  if (TYPE_TRX == "AD_A") {
                    await thisContent.dataDigiposRepository.updateById(DATA_DOCUMENT.digipost_id, {
                      ba_attachment_a: NameFileBA,
                      generate_file_a: 1
                    });
                  }

                  if (TYPE_TRX == "AD_B") {
                    await thisContent.dataDigiposRepository.updateById(DATA_DOCUMENT.digipost_id, {
                      ba_attachment_b: NameFileBA,
                      generate_file_b: 1
                    });
                  }

                  delete GENERATE_DOCUMENT_BAR[INDEX_DOCUMENT];
                  this.ON_GENERATE_DOCUMENT_BAR_PROSESS();

                });
              } else {
                delete GENERATE_DOCUMENT_BAR[INDEX_DOCUMENT];
                this.ON_GENERATE_DOCUMENT_BAR_PROSESS();
                console.log(err2);
              }
            }
          });
        }
      });
      break;
    }
    console.log("generate bar finish...");
  }

  ON_GENERATE_SUMMARY = async (digipos: any, TYPE_TRX: string, callback: any) => {

    var digiposCluster = await this.adClusterRepository.find({where: {ad_code: digipos.ad_code}});
    var IdCluster = "";
    for (var j in digiposCluster) {
      var cluster = digiposCluster[j];
      IdCluster += ",'" + cluster.ad_cluster_id + "'";
    }
    var datePeriode = digipos.trx_date_group.split("-");
    var Periode = digipos.trx_date_group;
    var Tahun = datePeriode[0];
    var Bulan = datePeriode[1];
    var sql = "SELECT " +
      "DISTINCT(DataSumarry.id), " +
      "AdCluster.ad_cluster_id, " +
      "AdCluster.ad_code, " +
      "DataSumarry.dealer_code, " +
      "DataSumarry.location, " +
      "DataSumarry.type_trx, " +
      "DataSumarry.name, " +
      "IF(DataSumarry.paid_in_amount = 0, DataSumarry.withdraw_amount , DataSumarry.paid_in_amount) as group_amount, " +
      "sum(DataSumarry.trx_a) as trx_a, " +
      "sum(DataSumarry.tot_mdr_a) as tot_mdr_a, " +
      "sum(DataSumarry.trx_b) as trx_b, " +
      "sum(DataSumarry.tot_mdr_b) as  tot_mdr_b, " +
      "DataSumarry.status, " +
      "DataSumarry.periode, " +
      "DataSumarry.create_at " +
      "FROM " +
      "DataSumarry " +
      "INNER JOIN " +
      "AdCluster " +
      "on " +
      "DataSumarry.dealer_code = AdCluster.ad_cluster_id " +
      "WHERE " +
      "DataSumarry.periode = '" + Periode + "'  AND DataSumarry.dealer_code in (" + IdCluster.substring(1) + ") GROUP BY AdCluster.ad_cluster_id,group_amount,type_trx"


    var DataBA_PDF: any;

    DataBA_PDF = {
      Periode: ControllerConfig.onGetMonth(parseInt(Bulan)) + " " + Tahun,
      DataTransaction: [],
      total_trx: 0,
      total_total_mdr: 0,
      total_dpp: 0,
      total_ppn: 0,
      total_pph: 0
    }

    if (TYPE_TRX == "AD_A") {
      DataBA_PDF.Periode = "SUMMARY AD-A PERIODE " + ControllerConfig.onGetMonth((parseInt(Bulan) - 1)) + " " + Tahun
    }

    if (TYPE_TRX == "AD_B") {
      DataBA_PDF.Periode = "SUMMARY AD-B PERIODE " + ControllerConfig.onGetMonth((parseInt(Bulan) - 1)) + " " + Tahun
    }

    var DATA_TYPE_TRX = await this.transaksiTypeRepository.find();
    var DATASOURCE_TYPE_TRX: any; DATASOURCE_TYPE_TRX = {};
    for (var I_TYPE_TRX in DATA_TYPE_TRX) {
      var RAW_TYPE_TRX = DATA_TYPE_TRX[I_TYPE_TRX];
      var ID_TYPE_TRX = RAW_TYPE_TRX.TT_ID == undefined ? 0 : RAW_TYPE_TRX.TT_ID;
      if (DATASOURCE_TYPE_TRX[ID_TYPE_TRX] == undefined) {
        DATASOURCE_TYPE_TRX[ID_TYPE_TRX] = {};
      }
      DATASOURCE_TYPE_TRX[ID_TYPE_TRX] = RAW_TYPE_TRX;
    }

    var dataSummary = await this.dataSumarryRepository.execute(sql);
    for (var iSM in dataSummary) {
      var RAW_SUMMARY = dataSummary[iSM];

      var dateSplit = RAW_SUMMARY.periode.split("-");
      var TAHUN_SUMMARY = dateSplit[0];
      var BULAN_SUMMARY = dateSplit[1];
      var DataSM: any; DataSM = {
        dealer_code: RAW_SUMMARY.dealer_code,
        dealer_name: RAW_SUMMARY.name,
        bulan: BULAN_SUMMARY,
        tahun: TAHUN_SUMMARY,
        fee: ControllerConfig.onGeneratePuluhan(RAW_SUMMARY.group_amount),
        type: DATASOURCE_TYPE_TRX[RAW_SUMMARY.type_trx]["TT_LABEL"],
        trx: 0,
        total_mdr: 0,
        dpp: 0,
        ppn: 0,
        pph: 0
      }

      var BOBOT_TYPE = DATASOURCE_TYPE_TRX[RAW_SUMMARY.type_trx];

      if (TYPE_TRX == "AD_A") {
        if (RAW_SUMMARY.trx_a == 0) {
          continue;
        }

        var FEE_AMOUNT = ((RAW_SUMMARY.trx_a * RAW_SUMMARY.group_amount) * (Number(BOBOT_TYPE["TT_BOBOT_A"]) / 100));
        var FEE_DPP = (FEE_AMOUNT / BOBOT_TYPE["TT_BOBOT_DPP_A"]);
        var FEE_PPN = (FEE_DPP * (Number(BOBOT_TYPE["TT_BOBOT_PPN_A"]) / 100));
        var FEE_TOTAL = FEE_DPP + FEE_PPN;
        var FEE_PPH = (FEE_DPP * (Number(BOBOT_TYPE["TT_BOBOT_PPH23_A"]) / 100));


        DataSM.trx = ControllerConfig.onGeneratePuluhan(RAW_SUMMARY.trx_a);
        DataSM.total_mdr = ControllerConfig.onGeneratePuluhan(Number(FEE_TOTAL.toFixed(0)));
        DataSM.dpp = ControllerConfig.onGeneratePuluhan(parseFloat(FEE_DPP.toFixed(2)));
        DataSM.ppn = ControllerConfig.onGeneratePuluhan(parseFloat(FEE_PPN.toFixed(2)));
        DataSM.pph = ControllerConfig.onGeneratePuluhan(parseFloat(FEE_PPH.toFixed(2)));
        DataSM.type = DATASOURCE_TYPE_TRX[RAW_SUMMARY.type_trx]["TT_LABEL"];

        DataBA_PDF.total_trx = DataBA_PDF.total_trx + RAW_SUMMARY.trx_a;
        DataBA_PDF.total_total_mdr = DataBA_PDF.total_total_mdr + FEE_TOTAL;
        DataBA_PDF.total_dpp = DataBA_PDF.total_dpp + FEE_DPP;
        DataBA_PDF.total_ppn = DataBA_PDF.total_ppn + FEE_PPN;
        DataBA_PDF.total_pph = DataBA_PDF.total_pph + FEE_PPH;

      }

      if (TYPE_TRX == "AD_B") {
        if (RAW_SUMMARY.trx_b == 0) {
          continue;
        }

        var PAID_AMOUNT = ((RAW_SUMMARY.trx_b * RAW_SUMMARY.group_amount) * (Number(BOBOT_TYPE["TT_BOBOT_B"]) / 100));
        var PAID_DPP = (PAID_AMOUNT / BOBOT_TYPE["TT_BOBOT_DPP_B"]);
        var PAID_PPN = (PAID_DPP * (Number(BOBOT_TYPE["TT_BOBOT_PPN_B"]) / 100));
        var PAID_TOTAL = PAID_DPP + PAID_PPN;
        var PAID_PPH = (PAID_DPP * (Number(BOBOT_TYPE["TT_BOBOT_PPH23_B"]) / 100));

        DataSM.trx = ControllerConfig.onGeneratePuluhan(RAW_SUMMARY.trx_b);
        DataSM.total_mdr = ControllerConfig.onGeneratePuluhan(Number(PAID_TOTAL.toFixed(0)));
        DataSM.dpp = ControllerConfig.onGeneratePuluhan(parseFloat(PAID_DPP.toFixed(2)));
        DataSM.ppn = ControllerConfig.onGeneratePuluhan(parseFloat(PAID_PPN.toFixed(2)));
        DataSM.pph = ControllerConfig.onGeneratePuluhan(parseFloat(PAID_PPH.toFixed(2)));
        DataSM.type = DATASOURCE_TYPE_TRX[RAW_SUMMARY.type_trx]["TT_LABEL"];

        DataBA_PDF.total_trx = DataBA_PDF.total_trx + RAW_SUMMARY.trx_b;
        DataBA_PDF.total_total_mdr = DataBA_PDF.total_total_mdr + PAID_TOTAL;
        DataBA_PDF.total_dpp = DataBA_PDF.total_dpp + PAID_DPP;
        DataBA_PDF.total_ppn = DataBA_PDF.total_ppn + PAID_PPN;
        DataBA_PDF.total_pph = DataBA_PDF.total_pph + PAID_PPH;
      }
      DataBA_PDF.DataTransaction.push(DataSM);
    }


    DataBA_PDF.total_trx = ControllerConfig.onGeneratePuluhan(parseFloat(DataBA_PDF.total_trx.toFixed(2)));
    DataBA_PDF.total_total_mdr = ControllerConfig.onGeneratePuluhan(parseFloat(DataBA_PDF.total_total_mdr.toFixed(2)));
    DataBA_PDF.total_dpp = ControllerConfig.onGeneratePuluhan(parseFloat(DataBA_PDF.total_dpp.toFixed(2)));
    DataBA_PDF.total_ppn = ControllerConfig.onGeneratePuluhan(parseFloat(DataBA_PDF.total_ppn.toFixed(2)));
    DataBA_PDF.total_pph = ControllerConfig.onGeneratePuluhan(parseFloat(DataBA_PDF.total_pph.toFixed(2)));


    var NameFileBA = "NONE";
    var pathFile = "";
    if (TYPE_TRX == "AD_A") {
      NameFileBA = digipos.ba_number_a.split("/").join("_") + "_" + digipos.trx_date_group + "_" + digipos.ad_code + ".pdf";
      pathFile = path.join(__dirname, '../../.digipost/berita_acara/ad_a/summary_') + NameFileBA;
    }
    if (TYPE_TRX == "AD_B") {
      NameFileBA = digipos.ba_number_b.split("/").join("_") + "_" + digipos.trx_date_group + "_" + digipos.ad_code + ".pdf";
      pathFile = path.join(__dirname, '../../.digipost/berita_acara/ad_b/summary_') + NameFileBA;
    }

    //MAKE FILE PDF BA
    var compiled = ejs.compile(fs.readFileSync(path.join(__dirname, '../../.digipost/template_ba/') + 'pdf_ba_summary.html', 'utf8'));
    var html = compiled(DataBA_PDF);

    var config: any; config = {
      format: "A4",
      orientation: "landscape",
      "header": {
        "height": "80px",
        "contents": ""
      },
      "footer": {
        "height": "80px",
        "contents": ''
      }
    }
    pdf.create(html, config).toFile(pathFile, async (err: any, res: any) => {
      if (err) {
        return console.log(err);
        callback(err);
      } else {
        callback({success: true, message: ""});
      }
    });

  }

  @post('/transaksi/digipos/generate-document/bar-number/transaksi-a', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: RequestGenerateDocBARNumber
          }
        },
      },
    },
  })
  async GenerateDocumentBARA(
    @requestBody(RequestGenerateDocBARNumber) requestGenerateDocBARNumber: {DIGIPOS_ID: [], LAST_NUMBER: number, DOC_LABEL: string},
  ): Promise<Object> {
    var userId = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: "", data: []};

    var DATA_DIGIPOS = await this.dataDigiposRepository.find({where: {and: [{digipost_id: {inq: requestGenerateDocBARNumber.DIGIPOS_ID}}, {ar_stat: 2}]}});
    if (DATA_DIGIPOS.length > 0) {

      var START_NUMBER_DOC_COUNT = requestGenerateDocBARNumber.LAST_NUMBER + 1;
      var START_NUMBER_DOC = START_NUMBER_DOC_COUNT.toString() + "/" + requestGenerateDocBARNumber.DOC_LABEL;

      var AVA_EXITIES = await this.dataDigiposRepository.find({where: {or: [{ba_number_a: START_NUMBER_DOC}, {ba_number_b: START_NUMBER_DOC}]}});
      if (AVA_EXITIES.length == 0) {

        var GENERATE_NUM = 0;
        for (var INDEX_DIGIPOS in DATA_DIGIPOS) {
          var RAW_DIGIPOS = DATA_DIGIPOS[INDEX_DIGIPOS];
          if (RAW_DIGIPOS.ba_number_a != null && RAW_DIGIPOS.ba_number_a != "") {
            continue;
          }

          var NUMBER_AVA = 0;
          var DOCUMENT_NUMBER = "";
          do {
            DOCUMENT_NUMBER = START_NUMBER_DOC_COUNT.toString() + "/" + requestGenerateDocBARNumber.DOC_LABEL;
            var DOC_NUMBER_AVA = await this.dataDigiposRepository.find({where: {or: [{ba_number_a: DOCUMENT_NUMBER}, {ba_number_b: DOCUMENT_NUMBER}]}});
            if (DOC_NUMBER_AVA.length > 0) {
              START_NUMBER_DOC_COUNT++;
            } else {
              NUMBER_AVA = 1;
            }
          } while (NUMBER_AVA == 0);

          var DIGIPOS_ID = RAW_DIGIPOS.digipost_id == undefined ? -1 + "_AD_A" : RAW_DIGIPOS.digipost_id + "_AD_A";
          await this.dataDigiposRepository.updateById(RAW_DIGIPOS.digipost_id, {ba_number_a: DOCUMENT_NUMBER, generate_file_a: 2});
          RAW_DIGIPOS.ba_number_a = DOCUMENT_NUMBER;
          if (GENERATE_DOCUMENT_BAR[DIGIPOS_ID] == undefined) {
            GENERATE_DOCUMENT_BAR[DIGIPOS_ID] = {TYPE: "AD_A", DATA: RAW_DIGIPOS};
          }
          GENERATE_NUM++;
          START_NUMBER_DOC_COUNT++;
        }

        if (GENERATE_NUM != 0) {
          this.ON_GENERATE_DOCUMENT_BAR_PROSESS();
        } else {
          result.success = false;
          result.message = "Error,  not found transaksi avaliable for generate document number  !";
        }

      } else {
        result.success = false;
        result.message = "Error, start number document already exities !";
      }

    } else {
      result.success = false;
      result.message = "Error, not found transaksi avaliable for generate document number !";
    }
    return result;
  }

  @post('/transaksi/digipos/re-generate-document/bar-number/transaksi-a', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: RequestReGenerateDocBAR
          }
        },
      },
    },
  })
  async ReGenerateDocumentBARA(
    @requestBody(RequestReGenerateDocBAR) requestGenerateDocBARNumber: {DIGIPOS_ID: []},
  ): Promise<Object> {
    var userId = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: "", data: []};

    var DATA_DIGIPOS = await this.dataDigiposRepository.find({where: {and: [{digipost_id: {inq: requestGenerateDocBARNumber.DIGIPOS_ID}}, {ar_stat: 2}]}});

    if (DATA_DIGIPOS.length > 0) {

      var GENERATE_NUM = 0;
      for (var INDEX_DIGIPOS in DATA_DIGIPOS) {
        var RAW_DIGIPOS = DATA_DIGIPOS[INDEX_DIGIPOS];
        if (RAW_DIGIPOS.ba_number_a == null || RAW_DIGIPOS.ba_number_a == "") {
          continue;
        }

        await this.dataDigiposRepository.updateById(RAW_DIGIPOS.digipost_id, {generate_file_a: 2});
        var DIGIPOS_ID = RAW_DIGIPOS.digipost_id == undefined ? -1 + "_AD_A" : RAW_DIGIPOS.digipost_id + "_AD_A";
        if (GENERATE_DOCUMENT_BAR[DIGIPOS_ID] == undefined) {
          GENERATE_DOCUMENT_BAR[DIGIPOS_ID] = {TYPE: "AD_A", DATA: RAW_DIGIPOS};
        }
        GENERATE_NUM++;
      }

      if (GENERATE_NUM != 0) {
        this.ON_GENERATE_DOCUMENT_BAR_PROSESS();
      } else {
        result.success = false;
        result.message = "Error,  not found transaksi avaliable for generate document number  !";
      }


    } else {
      result.success = false;
      result.message = "Error, not found transaksi avaliable for generate document number !";
    }
    return result;
  }



  @post('/transaksi/digipos/generate-document/bar-number/transaksi-b', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: RequestGenerateDocBARNumber
          }
        },
      },
    },
  })
  async GenerateDocumentBARB(
    @requestBody(RequestGenerateDocBARNumber) requestGenerateDocBARNumber: {DIGIPOS_ID: [], LAST_NUMBER: number, DOC_LABEL: string},
  ): Promise<Object> {
    var userId = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: "", data: []};

    var DATA_DIGIPOS = await this.dataDigiposRepository.find({where: {and: [{digipost_id: {inq: requestGenerateDocBARNumber.DIGIPOS_ID}}, {ar_stat: 2}]}});
    if (DATA_DIGIPOS.length > 0) {

      var START_NUMBER_DOC_COUNT = requestGenerateDocBARNumber.LAST_NUMBER + 1;
      var START_NUMBER_DOC = START_NUMBER_DOC_COUNT.toString() + "/" + requestGenerateDocBARNumber.DOC_LABEL;

      var AVA_EXITIES = await this.dataDigiposRepository.find({where: {or: [{ba_number_a: START_NUMBER_DOC}, {ba_number_b: START_NUMBER_DOC}]}});
      if (AVA_EXITIES.length == 0) {

        var GENERATE_NUM = 0;
        for (var INDEX_DIGIPOS in DATA_DIGIPOS) {
          var RAW_DIGIPOS = DATA_DIGIPOS[INDEX_DIGIPOS];
          if (RAW_DIGIPOS.ba_number_b != null && RAW_DIGIPOS.ba_number_b != "") {
            continue;
          }

          var NUMBER_AVA = 0;
          var DOCUMENT_NUMBER = "";
          do {
            DOCUMENT_NUMBER = START_NUMBER_DOC_COUNT.toString() + "/" + requestGenerateDocBARNumber.DOC_LABEL;
            var DOC_NUMBER_AVA = await this.dataDigiposRepository.find({where: {or: [{ba_number_a: DOCUMENT_NUMBER}, {ba_number_b: DOCUMENT_NUMBER}]}});
            if (DOC_NUMBER_AVA.length > 0) {
              START_NUMBER_DOC_COUNT++;
            } else {
              NUMBER_AVA = 1;
            }
          } while (NUMBER_AVA == 0);

          var DIGIPOS_ID = RAW_DIGIPOS.digipost_id == undefined ? -1 + "_AD_B" : RAW_DIGIPOS.digipost_id + "_AD_B";
          await this.dataDigiposRepository.updateById(RAW_DIGIPOS.digipost_id, {ba_number_b: DOCUMENT_NUMBER, generate_file_b: 2});
          RAW_DIGIPOS.ba_number_b = DOCUMENT_NUMBER;
          if (GENERATE_DOCUMENT_BAR[DIGIPOS_ID] == undefined) {
            GENERATE_DOCUMENT_BAR[DIGIPOS_ID] = {TYPE: "AD_B", DATA: RAW_DIGIPOS};
          }
          GENERATE_NUM++;
          START_NUMBER_DOC_COUNT++;
        }

        if (GENERATE_NUM != 0) {
          this.ON_GENERATE_DOCUMENT_BAR_PROSESS();
        } else {
          result.success = false;
          result.message = "Error,  not found transaksi avaliable for generate document number  !";
        }

      } else {
        result.success = false;
        result.message = "Error, start number document already exities !";
      }

    } else {
      result.success = false;
      result.message = "Error, not found transaksi avaliable for generate document number !";
    }
    return result;
  }

  @post('/transaksi/digipos/re-generate-document/bar-number/transaksi-b', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: RequestReGenerateDocBAR
          }
        },
      },
    },
  })
  async ReGenerateDocumentBARB(
    @requestBody(RequestReGenerateDocBAR) requestGenerateDocBARNumber: {DIGIPOS_ID: []},
  ): Promise<Object> {
    var userId = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: "", data: []};

    var DATA_DIGIPOS = await this.dataDigiposRepository.find({where: {and: [{digipost_id: {inq: requestGenerateDocBARNumber.DIGIPOS_ID}}, {ar_stat: 2}]}});
    if (DATA_DIGIPOS.length > 0) {

      var GENERATE_NUM = 0;
      for (var INDEX_DIGIPOS in DATA_DIGIPOS) {
        var RAW_DIGIPOS = DATA_DIGIPOS[INDEX_DIGIPOS];
        if (RAW_DIGIPOS.ba_number_b == null || RAW_DIGIPOS.ba_number_b == "") {
          continue;
        }

        var DIGIPOS_ID = RAW_DIGIPOS.digipost_id == undefined ? -1 + "_AD_B" : RAW_DIGIPOS.digipost_id + "_AD_B";
        await this.dataDigiposRepository.updateById(RAW_DIGIPOS.digipost_id, {generate_file_b: 2});

        if (GENERATE_DOCUMENT_BAR[DIGIPOS_ID] == undefined) {
          GENERATE_DOCUMENT_BAR[DIGIPOS_ID] = {TYPE: "AD_B", DATA: RAW_DIGIPOS};
        }
        GENERATE_NUM++;
      }

      if (GENERATE_NUM != 0) {
        this.ON_GENERATE_DOCUMENT_BAR_PROSESS();
      } else {
        result.success = false;
        result.message = "Error,  not found transaksi avaliable for generate document number  !";
      }

    } else {
      result.success = false;
      result.message = "Error, not found transaksi avaliable for generate document number !";
    }
    return result;
  }


}
