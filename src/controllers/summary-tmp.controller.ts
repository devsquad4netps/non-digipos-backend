import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where
} from '@loopback/repository';
import {
  del, get,
  getModelSchemaRef, param,


  patch, post,




  put,

  requestBody
} from '@loopback/rest';
import {SummaryTmp} from '../models';
import {SummaryTmpRepository} from '../repositories';

export class SummaryTmpController {
  constructor(
    @repository(SummaryTmpRepository)
    public summaryTmpRepository: SummaryTmpRepository,
  ) {}

  @post('/summary-tmps', {
    responses: {
      '200': {
        description: 'SummaryTmp model instance',
        content: {'application/json': {schema: getModelSchemaRef(SummaryTmp)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(SummaryTmp, {
            title: 'NewSummaryTmp',
            exclude: ['id'],
          }),
        },
      },
    })
    summaryTmp: Omit<SummaryTmp, 'id'>,
  ): Promise<SummaryTmp> {
    return this.summaryTmpRepository.create(summaryTmp);
  }

  @get('/summary-tmps/count', {
    responses: {
      '200': {
        description: 'SummaryTmp model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(SummaryTmp) where?: Where<SummaryTmp>,
  ): Promise<Count> {
    return this.summaryTmpRepository.count(where);
  }

  @get('/summary-tmps', {
    responses: {
      '200': {
        description: 'Array of SummaryTmp model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(SummaryTmp, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(SummaryTmp) filter?: Filter<SummaryTmp>,
  ): Promise<SummaryTmp[]> {
    return this.summaryTmpRepository.find(filter);
  }

  @patch('/summary-tmps', {
    responses: {
      '200': {
        description: 'SummaryTmp PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(SummaryTmp, {partial: true}),
        },
      },
    })
    summaryTmp: SummaryTmp,
    @param.where(SummaryTmp) where?: Where<SummaryTmp>,
  ): Promise<Count> {
    return this.summaryTmpRepository.updateAll(summaryTmp, where);
  }

  @get('/summary-tmps/{id}', {
    responses: {
      '200': {
        description: 'SummaryTmp model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(SummaryTmp, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(SummaryTmp, {exclude: 'where'}) filter?: FilterExcludingWhere<SummaryTmp>
  ): Promise<SummaryTmp> {
    return this.summaryTmpRepository.findById(id, filter);
  }

  @patch('/summary-tmps/{id}', {
    responses: {
      '204': {
        description: 'SummaryTmp PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(SummaryTmp, {partial: true}),
        },
      },
    })
    summaryTmp: SummaryTmp,
  ): Promise<void> {
    await this.summaryTmpRepository.updateById(id, summaryTmp);
  }

  @put('/summary-tmps/{id}', {
    responses: {
      '204': {
        description: 'SummaryTmp PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() summaryTmp: SummaryTmp,
  ): Promise<void> {
    await this.summaryTmpRepository.replaceById(id, summaryTmp);
  }

  @del('/summary-tmps/{id}', {
    responses: {
      '204': {
        description: 'SummaryTmp DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.summaryTmpRepository.deleteById(id);
  }
}
