// // Uncomment these imports to begin using these cool features!

// import {Filter, FilterExcludingWhere, repository} from '@loopback/repository';
// import {del, get, getModelSchemaRef, param, post, put, requestBody} from '@loopback/rest';
// import {MerchantProductCondition, MerchantProductVariableCal, MerchantProductVariableGroupCal} from '../models';
// import {MerchantProductConditionRepository, MerchantProductVariableCalRepository, MerchantProductVariableGroupCalRepository, MerchantProductVariableRepository} from '../repositories';
// import {ControllerConfig} from './controller.config';

// // import {inject} from '@loopback/core';


// export class NonDigiposFormulaController {
//   constructor(
//     @repository(MerchantProductVariableRepository)
//     public merchantProductVariableRepository: MerchantProductVariableRepository,
//     @repository(MerchantProductVariableGroupCalRepository)
//     public merchantProductVariableGroupCalRepository: MerchantProductVariableGroupCalRepository,
//     @repository(MerchantProductVariableCalRepository)
//     public merchantProductVariableCalRepository: MerchantProductVariableCalRepository,
//     @repository(MerchantProductConditionRepository)
//     public merchantProductConditionRepository: MerchantProductConditionRepository,
//   ) { }


//   @get('/non-digipos-formula/by-product_variable/{mpv_id}', {
//     responses: {
//       '200': {
//         description: 'Array of non digipos variable type model instances',
//         content: {
//           'application/json': {
//             schema: {
//               type: 'array',
//               items: getModelSchemaRef(MerchantProductVariableCal, {includeRelations: true}),
//             },
//           },
//         },
//       },
//     },
//   })
//   async findByProductVariable(
//     @param.path.number('mpv_id') id: number,
//   ): Promise<Object> {
//     var result: any; result = [];
//     var infoGroup = await this.merchantProductVariableGroupCalRepository.find({where: {and: [{mpv_id: id}]}});
//     for (var i in infoGroup) {
//       var data: any; data = {};
//       var rawGroup: any; rawGroup = infoGroup[i];
//       for (var j in rawGroup) {
//         data[j] = rawGroup[j];
//       }
//       //Condition Formula
//       data["formula_condition"] = [];
//       var infoFormulaCondition = await this.merchantProductConditionRepository.find({where: {and: [{mpvgc_id: rawGroup.mpvgc_id}]}});
//       data["formula_condition"] = infoFormulaCondition;

//       //Variable Formula
//       data["formula_variable"] = [];
//       var infoFormulaVariable = await this.merchantProductVariableCalRepository.find({where: {and: [{mpvgc_id: rawGroup.mpvgc_id}]}});
//       data["formula_variable"] = infoFormulaVariable;
//       result.push(data)
//     }
//     return result;
//   }


//   // START FORMULA GROUP
//   @get('/non-digipos-formula/group', {
//     responses: {
//       '200': {
//         description: 'Array of non digipos variable type model instances',
//         content: {
//           'application/json': {
//             schema: {
//               type: 'array',
//               items: getModelSchemaRef(MerchantProductVariableGroupCal, {includeRelations: true}),
//             },
//           },
//         },
//       },
//     },
//   })
//   async find(
//     @param.filter(MerchantProductVariableGroupCal) filter?: Filter<MerchantProductVariableGroupCal>,
//   ): Promise<MerchantProductVariableGroupCal[]> {
//     return this.merchantProductVariableGroupCalRepository.find(filter);
//   }

//   @get('/non-digipos-formula/group/{id}', {
//     responses: {
//       '200': {
//         description: 'non digipos variable type model instance',
//         content: {
//           'application/json': {
//             schema: getModelSchemaRef(MerchantProductVariableGroupCal, {includeRelations: true}),
//           },
//         },
//       },
//     },
//   })
//   async findById(
//     @param.path.number('id') id: number,
//     @param.filter(MerchantProductVariableGroupCal, {exclude: 'where'}) filter?: FilterExcludingWhere<MerchantProductVariableGroupCal>
//   ): Promise<MerchantProductVariableGroupCal> {
//     return this.merchantProductVariableGroupCalRepository.findById(id, filter);
//   }

//   @post('/non-digipos-formula/group/create', {
//     responses: {
//       '200': {
//         description: 'Menu model instance',
//         content: {'application/json': {schema: getModelSchemaRef(MerchantProductVariableGroupCal)}},
//       },
//     },
//   })
//   async create(
//     @requestBody({
//       content: {
//         'application/json': {
//           schema: getModelSchemaRef(MerchantProductVariableGroupCal, {
//             title: 'NewMerchanrProductVariableGroupCall',
//             exclude: ['mpvgc_id', 'mpvgc_at_create', 'mpvgc_at_update'],
//           }),
//         },
//       },
//     })
//     productFormulaGroup: Omit<MerchantProductVariableGroupCal, 'mpvgc_id'>,
//   ): Promise<Object> {
//     var result: any; result = {success: false, message: "", data: {}};
//     try {
//       var infoCreate = await this.merchantProductVariableGroupCalRepository.create(productFormulaGroup);
//       var infoUpdate = await this.merchantProductVariableGroupCalRepository.updateById(infoCreate.mpvgc_id, {
//         mpvgc_at_create: ControllerConfig.onCurrentTime().toString()
//       });
//       result.success = true;
//       result.message = "Success, create merchant product  variable formula group.";
//       result.data = infoUpdate;
//     } catch (error) {
//       result.success = false;
//       result.message = error;
//     }
//     return result;
//   }

//   @put('/non-digipos-formula/group/update/{id}', {
//     responses: {
//       '200': {
//         description: 'Merchan Variable Formula Group model instance',
//         content: {'application/json': {schema: getModelSchemaRef(MerchantProductVariableGroupCal)}},
//       },
//     },
//   })
//   async updateById(
//     @param.path.number('id') id: number,
//     @requestBody({
//       content: {
//         'application/json': {
//           schema: getModelSchemaRef(MerchantProductVariableGroupCal, {
//             title: 'UpdateMerchanrProductVariableGroupCall',
//             exclude: ['mpvgc_id', 'mpvgc_at_create', 'mpvgc_at_update'],
//           }),
//         },
//       },
//     }) productFormulaGroup: any,
//   ): Promise<Object> {
//     var result: any; result = {success: false, message: "", data: {}};
//     try {
//       var dataUpdate: any; dataUpdate = {};
//       for (var i in productFormulaGroup) {
//         dataUpdate[i] = productFormulaGroup[i];
//       }
//       dataUpdate["mpvgc_at_update"] = ControllerConfig.onCurrentTime().toString();
//       var updateProduct = await this.merchantProductVariableGroupCalRepository.updateById(id, dataUpdate);
//       result.success = true;
//       result.message = "Success, update merchant product  variable formula group.";
//       result.data = updateProduct;
//     } catch (error) {
//       result.success = false;
//       result.message = error;
//     }
//     return result;
//   }

//   @del('/non-digipos-formula/group/delete/{id}', {
//     responses: {
//       '200': {
//         description: 'Merchant Variable Formula Group DELETE success',
//       },
//     },
//   })
//   async deleteById(@param.path.number('id') id: number): Promise<Object> {
//     var result: any; result = {success: false, message: ""};
//     await this.merchantProductVariableGroupCalRepository.deleteById(id);
//     await this.merchantProductVariableCalRepository.deleteAll({and: [{mpvgc_id: id}]});
//     result.success = true;
//     result.message = "Success, delete  merchant variable.";
//     return result;
//   }

//   // END FORMULA GROUP

//   // START FORMULA VARIABLE
//   @get('/non-digipos-formula/variable', {
//     responses: {
//       '200': {
//         description: 'Array of non digipos variable formula variable type model instances',
//         content: {
//           'application/json': {
//             schema: {
//               type: 'array',
//               items: getModelSchemaRef(MerchantProductVariableCal, {includeRelations: true}),
//             },
//           },
//         },
//       },
//     },
//   })
//   async findFormulaVariable(
//     @param.filter(MerchantProductVariableCal) filter?: Filter<MerchantProductVariableCal>,
//   ): Promise<MerchantProductVariableCal[]> {
//     return this.merchantProductVariableCalRepository.find(filter);
//   }

//   @get('/non-digipos-formula/variable/{id}', {
//     responses: {
//       '200': {
//         description: 'non digipos variable formula variable type model instance',
//         content: {
//           'application/json': {
//             schema: getModelSchemaRef(MerchantProductVariableCal, {includeRelations: true}),
//           },
//         },
//       },
//     },
//   })
//   async findByIdVariable(
//     @param.path.number('id') id: number,
//     @param.filter(MerchantProductVariableCal, {exclude: 'where'}) filter?: FilterExcludingWhere<MerchantProductVariableCal>
//   ): Promise<MerchantProductVariableCal> {
//     return this.merchantProductVariableCalRepository.findById(id, filter);
//   }

//   @post('/non-digipos-formula/variable/create', {
//     responses: {
//       '200': {
//         description: 'Non digipos formula variable model instance',
//         content: {'application/json': {schema: getModelSchemaRef(MerchantProductVariableCal)}},
//       },
//     },
//   })
//   async createVariable(
//     @requestBody({
//       content: {
//         'application/json': {
//           schema: getModelSchemaRef(MerchantProductVariableCal, {
//             title: 'NewMerchanrProductVariableCall',
//             exclude: ['mpvc_id', 'mpvc_at_create', 'mpvc_at_update'],
//           }),
//         },
//       },
//     })
//     productFormulaVariable: Omit<MerchantProductVariableCal, 'mpvc_id'>,
//   ): Promise<Object> {
//     var result: any; result = {success: false, message: "", data: {}};
//     try {
//       var infoCreate = await this.merchantProductVariableCalRepository.create(productFormulaVariable);
//       var infoUpdate = await this.merchantProductVariableCalRepository.updateById(infoCreate.mpvc_id, {
//         mpvc_at_create: ControllerConfig.onCurrentTime().toString()
//       });
//       result.success = true;
//       result.message = "Success, create merchant product  variable formula variable.";
//       result.data = infoUpdate;
//     } catch (error) {
//       result.success = false;
//       result.message = error;
//     }
//     return result;
//   }

//   @put('/non-digipos-formula/variable/update/{id}', {
//     responses: {
//       '200': {
//         description: 'Merchan Variable Formula Group model instance',
//         content: {'application/json': {schema: getModelSchemaRef(MerchantProductVariableGroupCal)}},
//       },
//     },
//   })
//   async updateByIdVariable(
//     @param.path.number('id') id: number,
//     @requestBody({
//       content: {
//         'application/json': {
//           schema: getModelSchemaRef(MerchantProductVariableCal, {
//             title: 'UpdateMerchanrProductVariableCall',
//             exclude: ['mpvc_id', 'mpvc_at_create', 'mpvc_at_update'],
//           }),
//         },
//       },
//     }) productFormulaVariable: any,
//   ): Promise<Object> {
//     var result: any; result = {success: false, message: "", data: {}};
//     try {
//       var dataUpdate: any; dataUpdate = {};
//       for (var i in productFormulaVariable) {
//         dataUpdate[i] = productFormulaVariable[i];
//       }
//       dataUpdate["mpvc_at_update"] = ControllerConfig.onCurrentTime().toString();
//       var updateProduct = await this.merchantProductVariableCalRepository.updateById(id, dataUpdate);
//       result.success = true;
//       result.message = "Success, update merchant product  variable formula variable.";
//       result.data = updateProduct;
//     } catch (error) {
//       result.success = false;
//       result.message = error;
//     }
//     return result;
//   }

//   @del('/non-digipos-formula/variable/delete/{id}', {
//     responses: {
//       '200': {
//         description: 'Merchant Variable Formula Variable DELETE success',
//       },
//     },
//   })
//   async deleteByIdVariable(@param.path.number('id') id: number): Promise<Object> {
//     var result: any; result = {success: false, message: ""};
//     await this.merchantProductVariableCalRepository.deleteById(id);
//     result.success = true;
//     result.message = "Success, delete  merchant variable.";
//     return result;
//   }
//   // END FORMULA VARIABLE
//   // START FORMULA CONDITION

//   @get('/non-digipos-formula/condition', {
//     responses: {
//       '200': {
//         description: 'Array of non digipos  formula condition type model instances',
//         content: {
//           'application/json': {
//             schema: {
//               type: 'array',
//               items: getModelSchemaRef(MerchantProductCondition, {includeRelations: true}),
//             },
//           },
//         },
//       },
//     },
//   })
//   async findFormulaCondtion(
//     @param.filter(MerchantProductCondition) filter?: Filter<MerchantProductCondition>,
//   ): Promise<MerchantProductCondition[]> {
//     return this.merchantProductConditionRepository.find(filter);
//   }

//   @get('/non-digipos-formula/condition/{id}', {
//     responses: {
//       '200': {
//         description: 'non digipos  formula condition type model instance',
//         content: {
//           'application/json': {
//             schema: getModelSchemaRef(MerchantProductCondition, {includeRelations: true}),
//           },
//         },
//       },
//     },
//   })
//   async findByIdCondition(
//     @param.path.number('id') id: number,
//     @param.filter(MerchantProductCondition, {exclude: 'where'}) filter?: FilterExcludingWhere<MerchantProductCondition>
//   ): Promise<MerchantProductCondition> {
//     return this.merchantProductConditionRepository.findById(id, filter);
//   }

//   @post('/non-digipos-formula/condition/create', {
//     responses: {
//       '200': {
//         description: 'Non digipos formula variable model instance',
//         content: {'application/json': {schema: getModelSchemaRef(MerchantProductCondition)}},
//       },
//     },
//   })
//   async createCondition(
//     @requestBody({
//       content: {
//         'application/json': {
//           schema: getModelSchemaRef(MerchantProductCondition, {
//             title: 'NewMerchanrProductCondition',
//             exclude: ['mpc_id', 'mpc_at_create', 'mpc_at_update'],
//           }),
//         },
//       },
//     })
//     productFormulaCondition: Omit<MerchantProductCondition, 'mpc_id'>,
//   ): Promise<Object> {
//     var result: any; result = {success: false, message: "", data: {}};
//     try {
//       var infoCreate = await this.merchantProductConditionRepository.create(productFormulaCondition);
//       var infoUpdate = await this.merchantProductConditionRepository.updateById(infoCreate.mpc_id, {
//         mpc_at_create: ControllerConfig.onCurrentTime().toString()
//       });
//       result.success = true;
//       result.message = "Success, create merchant product  formula condition.";
//       result.data = infoUpdate;
//     } catch (error) {
//       result.success = false;
//       result.message = error;
//     }
//     return result;
//   }

//   @put('/non-digipos-formula/condition/update/{id}', {
//     responses: {
//       '200': {
//         description: 'Merchan Variable Formula Condition model instance',
//         content: {'application/json': {schema: getModelSchemaRef(MerchantProductCondition)}},
//       },
//     },
//   })
//   async updateByIdCondition(
//     @param.path.number('id') id: number,
//     @requestBody({
//       content: {
//         'application/json': {
//           schema: getModelSchemaRef(MerchantProductCondition, {
//             title: 'UpdateMerchanrProductVariableCall',
//             exclude: ['mpc_id', 'mpc_at_create', 'mpc_at_update'],
//           }),
//         },
//       },
//     }) productFormulaCondition: any,
//   ): Promise<Object> {
//     var result: any; result = {success: false, message: "", data: {}};
//     try {
//       var dataUpdate: any; dataUpdate = {};
//       for (var i in productFormulaCondition) {
//         dataUpdate[i] = productFormulaCondition[i];
//       }
//       dataUpdate["mpc_at_update"] = ControllerConfig.onCurrentTime().toString();
//       var updateProduct = await this.merchantProductConditionRepository.updateById(id, dataUpdate);
//       result.success = true;
//       result.message = "Success, update merchant product  formula condition.";
//       result.data = updateProduct;
//     } catch (error) {
//       result.success = false;
//       result.message = error;
//     }
//     return result;
//   }

//   @del('/non-digipos-formula/condition/delete/{id}', {
//     responses: {
//       '200': {
//         description: 'Merchant Variable Formula Condition DELETE success',
//       },
//     },
//   })
//   async deleteByIdCondition(@param.path.number('id') id: number): Promise<Object> {
//     var result: any; result = {success: false, message: ""};
//     await this.merchantProductConditionRepository.deleteById(id);
//     result.success = true;
//     result.message = "Success, delete  merchant formula condition.";
//     return result;
//   }

//   // END FORMULA CONDITION


// }
