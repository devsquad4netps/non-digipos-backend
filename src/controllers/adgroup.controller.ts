import {
  repository
} from '@loopback/repository';
import {AdgroupRepository} from '../repositories';

export class AdgroupController {
  constructor(
    @repository(AdgroupRepository)
    public adgroupRepository: AdgroupRepository,
  ) {}

  // @post('/adgroups', {
  //   responses: {
  //     '200': {
  //       description: 'Adgroup model instance',
  //       content: {'application/json': {schema: getModelSchemaRef(Adgroup)}},
  //     },
  //   },
  // })
  // async create(
  //   @requestBody({
  //     content: {
  //       'application/json': {
  //         schema: getModelSchemaRef(Adgroup, {
  //           title: 'NewAdgroup',
  //           exclude: ['id'],
  //         }),
  //       },
  //     },
  //   })
  //   adgroup: Omit<Adgroup, 'id'>,
  // ): Promise<Adgroup> {
  //   return this.adgroupRepository.create(adgroup);
  // }

  // @get('/adgroups/count', {
  //   responses: {
  //     '200': {
  //       description: 'Adgroup model count',
  //       content: {'application/json': {schema: CountSchema}},
  //     },
  //   },
  // })
  // async count(
  //   @param.where(Adgroup) where?: Where<Adgroup>,
  // ): Promise<Count> {
  //   return this.adgroupRepository.count(where);
  // }

  // @get('/adgroups', {
  //   responses: {
  //     '200': {
  //       description: 'Array of Adgroup model instances',
  //       content: {
  //         'application/json': {
  //           schema: {
  //             type: 'array',
  //             items: getModelSchemaRef(Adgroup, {includeRelations: true}),
  //           },
  //         },
  //       },
  //     },
  //   },
  // })
  // async find(
  //   @param.filter(Adgroup) filter?: Filter<Adgroup>,
  // ): Promise<Adgroup[]> {
  //   return this.adgroupRepository.find(filter);
  // }

  // @patch('/adgroups', {
  //   responses: {
  //     '200': {
  //       description: 'Adgroup PATCH success count',
  //       content: {'application/json': {schema: CountSchema}},
  //     },
  //   },
  // })
  // async updateAll(
  //   @requestBody({
  //     content: {
  //       'application/json': {
  //         schema: getModelSchemaRef(Adgroup, {partial: true}),
  //       },
  //     },
  //   })
  //   adgroup: Adgroup,
  //   @param.where(Adgroup) where?: Where<Adgroup>,
  // ): Promise<Count> {
  //   return this.adgroupRepository.updateAll(adgroup, where);
  // }

  // @get('/adgroups/{id}', {
  //   responses: {
  //     '200': {
  //       description: 'Adgroup model instance',
  //       content: {
  //         'application/json': {
  //           schema: getModelSchemaRef(Adgroup, {includeRelations: true}),
  //         },
  //       },
  //     },
  //   },
  // })
  // async findById(
  //   @param.path.number('id') id: number,
  //   @param.filter(Adgroup, {exclude: 'where'}) filter?: FilterExcludingWhere<Adgroup>
  // ): Promise<Adgroup> {
  //   return this.adgroupRepository.findById(id, filter);
  // }

  // @patch('/adgroups/{id}', {
  //   responses: {
  //     '204': {
  //       description: 'Adgroup PATCH success',
  //     },
  //   },
  // })
  // async updateById(
  //   @param.path.number('id') id: number,
  //   @requestBody({
  //     content: {
  //       'application/json': {
  //         schema: getModelSchemaRef(Adgroup, {partial: true}),
  //       },
  //     },
  //   })
  //   adgroup: Adgroup,
  // ): Promise<void> {
  //   await this.adgroupRepository.updateById(id, adgroup);
  // }

  // @put('/adgroups/{id}', {
  //   responses: {
  //     '204': {
  //       description: 'Adgroup PUT success',
  //     },
  //   },
  // })
  // async replaceById(
  //   @param.path.number('id') id: number,
  //   @requestBody() adgroup: Adgroup,
  // ): Promise<void> {
  //   await this.adgroupRepository.replaceById(id, adgroup);
  // }

  // @del('/adgroups/{id}', {
  //   responses: {
  //     '204': {
  //       description: 'Adgroup DELETE success',
  //     },
  //   },
  // })
  // async deleteById(@param.path.number('id') id: number): Promise<void> {
  //   await this.adgroupRepository.deleteById(id);
  // }
}
