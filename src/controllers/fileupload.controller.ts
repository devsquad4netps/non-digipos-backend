// Uncomment these imports to begin using these cool features!

// import {inject} from '@loopback/core';
import {inject} from '@loopback/core';
import {repository} from '@loopback/repository';
import {
  post,
  Request,
  requestBody,
  Response,
  RestBindings
} from '@loopback/rest';
import csv from 'csv-parser';
import fs from 'fs';
import multer from 'multer';
import path from 'path';
import {FILE_UPLOAD_SERVICE} from '../keys';
import {ArmasterRepository, DataconfigRepository, SummaryTmpRepository} from '../repositories';
import {FileUploadHandler} from '../types';


export class FileuploadController {

  constructor(
    @repository(SummaryTmpRepository)
    public summaryTmpRepository: SummaryTmpRepository,
    @repository(ArmasterRepository)
    public armasterRepository: ArmasterRepository,
    @repository(DataconfigRepository)
    public dataconfigRepository: DataconfigRepository,
    @inject(FILE_UPLOAD_SERVICE) private handler: FileUploadHandler,
  ) {
  }

  @post('/files', {
    responses: {
      200: {
        content: {
          'application/json': {
            schema: {
              type: 'object',
            },
          },
        },
        description: 'Files and fields',
      },
    },
  })
  async fileUpload(
    @requestBody.file()
    request: Request,
    @inject(RestBindings.Http.RESPONSE) response: Response,
  ): Promise<object> {


    let msg = [];
    const storage = multer.diskStorage({
      destination: path.join(__dirname, '../../.digipost'),
      // Use the original file name as is
      filename: (req, file, cb) => {
        cb(null, file.originalname);
      },
    });

    const upload = multer({
      storage: storage,
      fileFilter: (req, file, cb) => {
        if (file.originalname != "summary_digipost.csv") {
          cb(null, false);
          return cb(new Error('Name format file error !'));
        }
        cb(null, true);
      }
    });

    return new Promise<object>((resolve, reject) => {
      upload.single('files')(request, response, async (err: any) => {
        if (err) {
          reject({error: err});
        } else {
          let oldPath = path.join(__dirname, '../../.digipost/summary_digipost.csv');
          var d = new Date();
          var date = d.getDate();
          var month = d.getMonth();
          var year = d.getFullYear();
          var dateStr = date + "_" + month + "_" + year;

          let newPath = path.join(__dirname, '../../.digipos_process/summary_digipost_' + dateStr + '.csv');

          const deleteSumary = await this.summaryTmpRepository.deleteAll({runnig_group: month + "-" + year});

          fs.rename(oldPath, newPath, (err) => {
            if (err) throw err;
            var MapingData: any;
            MapingData = {
              "dealer_code": "dealer_code",
              "location": "location",
              "name": "name",
              "paid_in_amount": "paid_in_amount",
              "withdraw_amount": "withdraw_amount",
              "reason_type_id": "reason_type_id",
              "reason_type_name": "reason_type_name",
              "trx_a": "trx_a",
              "tot_mdr_a": "tot_mdr_a",
              "trx_b": "trx_b",
              "tot_mdr_b": "tot_mdr_b",
            }
            var error = 0;
            fs.createReadStream(path.join(__dirname, '../../.digipos_process/summary_digipost_' + dateStr + '.csv')).pipe(csv())
              .on('data', async (row: any) => {
                // console.log(row);
                let status = 1;
                // row.dealer_code
                var Datas: any;
                Datas = {
                  "dealer_code": "",
                  "location": "",
                  "name": "",
                  "paid_in_amount": "",
                  "withdraw_amount": "",
                  "reason_type_id": "",
                  "reason_type_name": "",
                  "trx_a": "",
                  "tot_mdr_a": "",
                  "trx_b": "",
                  "tot_mdr_b": "",
                  "status": 0
                };
                for (var i in row) {
                  if (MapingData[i.trim()] == undefined) {
                    status = 0
                    error = 1;
                  } else {
                    Datas[MapingData[i.trim()]] = row[i];
                  }
                }
                Datas["id"] = null;
                Datas["status"] = status;
                Datas["runnig_date"] = year + "-" + month + "-" + date;
                Datas["runnig_group"] = month + "-" + year;
                let addSummaryTmp = await this.summaryTmpRepository.create(Datas);
                // console.log(addSummaryTmp);
              })
              .on('end', async () => {
                if (error == 1) {
                  const dataSumary = await this.summaryTmpRepository.find({where: {status: 0}});
                  reject(dataSumary);
                } else {
                  // const dataSumary = await this.summaryTmpRepository.find({where: {status: 1}});
                  const dataSumary = await this.summaryTmpRepository.execute("SELECT " +
                    "summary.ad_code, " +
                    "summary.location, " +
                    "summary.name, " +
                    "summary.group_amount,  " +
                    "sum(summary.trx_a) as trx_a_sum, " +
                    "sum(summary.tot_mdr_a) as tot_mdr_a_sum, " +
                    "sum(summary.trx_b) as trx_b_sum, " +
                    "sum(summary.tot_mdr_b) as tot_mdr_b_sum " +
                    "FROM " +
                    "(SELECT " +
                    "DISTINCT(SummaryTmp.id), " +
                    "Adgroup.ad_code, " +
                    "SummaryTmp.location, " +
                    "SummaryTmp.name, " +
                    "IF(SummaryTmp.paid_in_amount = 0, SummaryTmp.withdraw_amount , SummaryTmp.paid_in_amount) as group_amount, " +
                    "SummaryTmp.trx_a, " +
                    "SummaryTmp.tot_mdr_a, " +
                    "SummaryTmp.trx_b, " +
                    "SummaryTmp.tot_mdr_b, " +
                    "SummaryTmp.status, " +
                    "SummaryTmp.runnig_group, " +
                    "SummaryTmp.runnig_date " +
                    "FROM " +
                    "SummaryTmp " +
                    "INNER JOIN " +
                    "Adgroup " +
                    "on " +
                    "SummaryTmp.dealer_code = Adgroup.dealer_code " +
                    "WHERE " +
                    "SummaryTmp.status = 1) as summary " +
                    "GROUP BY summary.ad_code ,summary.group_amount ");

                  const deleteArMaster = await this.armasterRepository.deleteAll({trx_date_group: month + "-" + year});
                  const configData = await this.dataconfigRepository.find({where: {config_group_id: 1}});

                  var configNum: any;
                  configNum = {};
                  for (var i in configData) {
                    var infoConfig = configData[i];
                    if (infoConfig.config_code == "0001-01") {
                      configNum["persen_fee"] = infoConfig.config_value;
                    }
                    if (infoConfig.config_code == "0001-02") {
                      configNum["persen_paid"] = infoConfig.config_value;
                    }

                    if (infoConfig.config_code == "0001-03") {
                      configNum["num_dpp_fee"] = infoConfig.config_value;
                    }
                    if (infoConfig.config_code == "0001-04") {
                      configNum["num_ppn_fee"] = infoConfig.config_value;
                    }
                    if (infoConfig.config_code == "0001-05") {
                      configNum["num_pph_fee"] = infoConfig.config_value;
                    }

                    if (infoConfig.config_code == "0001-11") {
                      configNum["num_dpp_paid"] = infoConfig.config_value;
                    }
                    if (infoConfig.config_code == "0001-12") {
                      configNum["num_ppn_paid"] = infoConfig.config_value;
                    }
                    if (infoConfig.config_code == "0001-13") {
                      configNum["num_pph_paid"] = infoConfig.config_value;
                    }


                    //   var infoConfig = configData[i];
                    //   configNum = {
                    //     persen_fee: (infoConfig.persen_fee / 100),
                    //     persen_paid: (infoConfig.persen_paid / 100),
                    //     num_dpp: infoConfig.num_dpp,
                    //     num_ppn: infoConfig.num_ppn,
                    //     num_pph: infoConfig.num_pph
                    //   }
                  }
                  // console.log(configNum);

                  for (var i in dataSumary) {
                    var infoAr = dataSumary[i];
                    var trxFee = ((infoAr.trx_a_sum * infoAr.group_amount) * (configNum.persen_fee));
                    var feeDPP = (trxFee / configNum.num_dpp_fee);
                    var feePPN = (feeDPP * configNum.num_ppn_fee);
                    var feeTotal = feeDPP + feePPN;
                    var feePPH = (feeDPP * configNum.num_pph_fee);

                    var trxPaid = ((infoAr.trx_b_sum * infoAr.group_amount) * (configNum.persen_paid));
                    var paidDPP = (trxPaid / configNum.num_dpp_paid);
                    var paidPPN = (paidDPP * configNum.num_ppn_paid);
                    var paidTotal = paidDPP + paidPPN;
                    var paidPPH = (paidDPP * configNum.num_pph_paid);

                    let dataArMaster: any;
                    dataArMaster = {
                      "trx_date_group": month + "-" + year,
                      "ad_code": infoAr.ad_code,
                      "ad_name": infoAr.name,
                      "inv_number": "",
                      "inv_attacment": "",
                      "ba_number_a": "",
                      "ba_attachment_a": "",
                      "ba_number_b": "",
                      "ba_attachment_b": "",
                      "vat_number": "",
                      "vat_attachment": "",
                      "inv_desc": "",
                      "sales_fee": infoAr.group_amount,
                      "trx_cluster_out": infoAr.trx_a_sum,
                      "mdr_fee_amount": trxFee,
                      "dpp_fee_amount": feeDPP,
                      "vat_fee_amount": feePPN,
                      "total_fee_amount": feeTotal,
                      "pph_fee_amount": feePPH,
                      "trx_paid_invoice": infoAr.trx_b_sum,
                      "paid_inv_amount": trxPaid,
                      "inv_adj_amount": 0,
                      "dpp_paid_amount": paidDPP,
                      "vat_paid_amount": paidPPN,
                      "total_paid_amount": paidTotal,
                      "pph_paid_amount": paidPPH,
                      "remark": "-",
                      "ar_stat": false,
                      "response_stat": true
                    }
                    const insertArMaster = await this.armasterRepository.create(dataArMaster);
                  }

                  const dataArMaster = await this.armasterRepository.execute(
                    "SELECT " +
                    "trx_date_group, " +
                    "COUNT(DISTINCT ad_code) as CountAD, " +
                    "SUM(trx_cluster_out) SumTrxFee,  " +
                    "SUM(mdr_fee_amount) SumFee, " +
                    "SUM(trx_paid_invoice) SumTrxPaid,  " +
                    "SUM(paid_inv_amount) SumPaid " +
                    "FROM " +
                    "Armaster WHERE trx_date_group = '" + month + "-" + year + "'"
                  );

                  resolve(dataArMaster);

                }
              });
          });

          // console.log(request.files);
          // fs.createReadStream(path.join(__dirname, '../../.digipost/summary_digipost.csv'))
          //   .pipe(csv())
          //   .on('data', (row: any) => {
          //     console.log(row);
          //   })
          //   .on('end', () => {
          //     console.log('CSV file successfully processed');
          //   });
          // resolve({
          //   files: request.files,
          //   fields: (request as any).fields
          // });
        }
      });
    });
  }

  /**
   * Get files and fields for the request
   * @param request - Http request
   */
  private static getFilesAndFields(request: Request) {
    const uploadedFiles = request.files;
    console.log(uploadedFiles);
    //   const mapper = (f: globalThis.Express.Multer.File) => ({
    //     fieldname: f.fieldname,
    //     originalname: f.originalname,
    //     encoding: f.encoding,
    //     mimetype: f.mimetype,
    //     size: f.size
    //   });
    //   let files: object[] = [];
    //   if (Array.isArray(uploadedFiles)) {
    //     files = uploadedFiles.map(mapper);
    //   } else {
    //     for (const filename in uploadedFiles) {
    //       files.push(...uploadedFiles[filename].map(mapper));
    //     }
    //   }
    // return {files, fields: request.body};4
    return {};
  }
  static to(arg0: string) {
    throw new Error('Method not implemented.');
  }

}
