import {authenticate, TokenService} from '@loopback/authentication';
import {Credentials, TokenServiceBindings, UserCredentialsRepository, UserServiceBindings} from '@loopback/authentication-jwt';
import {inject} from '@loopback/core';
import {repository} from '@loopback/repository';
import {post, requestBody} from '@loopback/rest';
import {SecurityBindings} from '@loopback/security';
import {User} from '../models';
import {DataconfigRepository, DataFpjpRepository, DataNotificationRepository, RolemappingRepository, UserRepository} from '../repositories';
import {MyUserService} from '../services/user.service';

const CredentialsOpenApiSchema = {
  type: 'object',
  required: ['email', 'password'],
  properties: {
    email: {
      type: 'string',
      format: 'email',
    },
    password: {
      type: 'string',
      minLength: 8,
    },
  },
};

export const CredentialsOpenApiRequestBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {schema: CredentialsOpenApiSchema},
  },
};




export const FpjpPaidRequestBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['no_fpjp'],
        properties: {
          no_fpjp: {
            type: 'array',
            items: {type: 'string'},
          }
        }
      }
    },
  },
};

export class DigiposApisActionController {
  constructor(
    @inject(TokenServiceBindings.TOKEN_SERVICE)
    public jwtService: TokenService,
    @inject(UserServiceBindings.USER_SERVICE)
    public userService: MyUserService,
    @inject(SecurityBindings.USER, {optional: true})
    public user: User,
    @repository(UserRepository) protected userRepository: UserRepository,
    @repository(RolemappingRepository)
    public roleMappingRepository: RolemappingRepository,
    @repository(DataconfigRepository)
    public dataconfigRepository: DataconfigRepository,
    @repository(UserCredentialsRepository)
    public userCredentialsRepository: UserCredentialsRepository,
    @repository(DataNotificationRepository)
    public dataNotificationRepository: DataNotificationRepository,
    @repository(DataFpjpRepository)
    public dataFpjpRepository: DataFpjpRepository,

  ) { }


  onCallTokenCode = () => {

  }

  @post('/digipos-apis-action/get-token', {
    responses: {
      '200': {
        description: 'Token',
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                token: {
                  type: 'string',
                },
              },
            },
          },
        },
      },
    },
  })
  async access_token(
    @requestBody(CredentialsOpenApiRequestBody) credentials: Credentials,
  ): Promise<{data: Object}> {
    // // ensure the user exists, and the password is correct
    const user = await this.userService.verifyCredentials(credentials);
    // // convert a User object into a UserProfile object (reduced set of properties)
    const userProfile = this.userService.convertToUserProfile(user);
    var data: any; data = {};

    if (user.realm === "DELETED") {
      data = {"status": false, "message": "username or password vailed !", token: ""};
    } else {
      const token = await this.jwtService.generateToken(userProfile);
      data['token'] = token;
      data['status'] = true;
      data['message'] = "success";
    }
    // create a JSON Web Token based on the user profile
    return data;
  }

  @authenticate('jwt')
  @post('/digipos-apis-action/paid-fpjp', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: FpjpPaidRequestBody
          }
        },
      },
    },
  })
  async FinishFjp(
    @requestBody(FpjpPaidRequestBody) FPJPreq: {
      no_fpjp: any
    },
  ): Promise<Object> {
    var returnMs: any; returnMs = {success: true, message: ""};
    try {
      var dataFPJP: any; dataFPJP = [];
      if (Array.isArray(FPJPreq.no_fpjp)) {
        //Array
        dataFPJP = await this.dataFpjpRepository.find({where: {and: [{transaction_code: {inq: FPJPreq.no_fpjp}}, {fpjp_hold_state: 0}]}});
      }
      if (typeof FPJPreq.no_fpjp === 'string' || FPJPreq.no_fpjp instanceof String) {
        //String
        dataFPJP = await this.dataFpjpRepository.find({where: {and: [{transaction_code: FPJPreq.no_fpjp.toString()}, {fpjp_hold_state: 0}]}});
      }

      if (dataFPJP.length > 0) {
        for (var i in dataFPJP) {
          var info = dataFPJP[i];
          if (info.state != 3) {
            returnMs.success = false;
            returnMs.message = "Error, Update paid fpjp must be state 'Waiting Payment' !";
          } else {
            this.dataFpjpRepository.updateById(info.id, {state: 4});
            returnMs.success = true;
            returnMs.message = "Success, Update paid data fpjp no : " + FPJPreq.no_fpjp;
          }
        }
      } else {
        returnMs.success = false;
        returnMs.message = "Error, No data fpjp already paid !";
      }
    } catch (error) {
      returnMs.success = false;
      returnMs.message = error;
    }
    return returnMs;
  }

  @authenticate('jwt')
  @post('/digipos-apis-action/unpaid-fpjp', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: FpjpPaidRequestBody
          }
        },
      },
    },
  })
  async UnCompliteFjp(
    @requestBody(FpjpPaidRequestBody) FPJPreq: {
      no_fpjp: any
    },
  ): Promise<Object> {
    var returnMs: any; returnMs = {success: true, message: ""};
    try {
      var dataFPJP: any; dataFPJP = [];
      if (Array.isArray(FPJPreq.no_fpjp)) {
        //Array
        dataFPJP = await this.dataFpjpRepository.find({where: {and: [{transaction_code: {inq: FPJPreq.no_fpjp}}, {fpjp_hold_state: 0}]}});
      }
      if (typeof FPJPreq.no_fpjp === 'string' || FPJPreq.no_fpjp instanceof String) {
        //String
        dataFPJP = await this.dataFpjpRepository.find({where: {and: [{transaction_code: FPJPreq.no_fpjp.toString()}, {fpjp_hold_state: 0}]}});
      }


      if (dataFPJP.length == 0) {
        returnMs.success = false;
        returnMs.message = "Error, No data fpjp already unpaid !";
      } else {
        var message = "";
        for (var i in dataFPJP) {
          var info = dataFPJP[i];
          if (info.state != 4) {
            returnMs.success = false;
            returnMs.message = "Error, Update unpaid fpjp must be state 'Payment' !";
          } else {
            this.dataFpjpRepository.updateById(info.id, {state: 3});
            returnMs.success = true;
            returnMs.message = "Success, Update unpaid data fpjp no : " + FPJPreq.no_fpjp;
          }
        }

      }
    } catch (error) {
      returnMs.success = false;
      returnMs.message = error;
    }
    return returnMs;
  }


}
