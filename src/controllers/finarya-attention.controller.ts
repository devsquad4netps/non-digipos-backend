import {authenticate} from '@loopback/authentication';
import {inject} from '@loopback/core';
import {Filter, FilterExcludingWhere, repository} from '@loopback/repository';
import {get, getModelSchemaRef, param, post, Request, requestBody, Response, RestBindings} from '@loopback/rest';
import {SecurityBindings, securityId, UserProfile} from '@loopback/security';
import fs from 'fs';
import multer from 'multer';
import path from 'path';
import {MerchantFinaryaPic} from '../models';
import {MerchantFinaryaPicRepository} from '../repositories';
import {UserRepository} from '../repositories/user.repository';
import {ControllerConfig} from './controller.config';

@authenticate('jwt')
export class FinaryaAttentionController {
  constructor(
    @inject(SecurityBindings.USER)
    public currentUserProfile: UserProfile,
    @repository(UserRepository)
    public userRepository: UserRepository,
    @repository(MerchantFinaryaPicRepository)
    public merchantFinaryaPicRepository: MerchantFinaryaPicRepository,
  ) { }


  @get('/finarya-attention/non-digipos/pic/views', {
    responses: {
      '200': {
        description: 'Array of Merchant Finarya PIC model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(MerchantFinaryaPic, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async findMerchantFinaryaPIC(
    @param.filter(MerchantFinaryaPic) filter?: Filter<MerchantFinaryaPic>,
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};
    var MerchantStore: any; MerchantStore = [];
    var MerchantFinaryaPICInfo = await this.merchantFinaryaPicRepository.find(filter);

    for (var i in MerchantFinaryaPICInfo) {
      var infoMerchantFinaryaPIC = MerchantFinaryaPICInfo[i];
      var RAW = {
        ID: infoMerchantFinaryaPIC.ID,
        PIC_NAME: infoMerchantFinaryaPIC.PIC_NAME,
        PIC_JABATAN: infoMerchantFinaryaPIC.PIC_JABATAN,
        PIC_ALAMAT: infoMerchantFinaryaPIC.PIC_ALAMAT,
        PIC_EMAIL: infoMerchantFinaryaPIC.PIC_EMAIL,
        PIC_SIGN: infoMerchantFinaryaPIC.PIC_SIGN,
        PIC_NOTELP: infoMerchantFinaryaPIC.PIC_NOTELP,
        PT_NAME: infoMerchantFinaryaPIC.PT_NAME,
        PT_ALAMAT: infoMerchantFinaryaPIC.PT_ALAMAT,
        PT_NOTELP: infoMerchantFinaryaPIC.PT_NOTELP,
        PT_NPWP: infoMerchantFinaryaPIC.PT_NPWP,
        PT_ACOUNT_NAME: infoMerchantFinaryaPIC.PT_ACOUNT_NAME,
        PT_ACOUNT_NUMBER: infoMerchantFinaryaPIC.PT_ACOUNT_NUMBER,
        PT_BANK_NAME: infoMerchantFinaryaPIC.PT_BANK_NAME,
        PT_BRANCH: infoMerchantFinaryaPIC.PT_BRANCH,
        PIC_TYPE: infoMerchantFinaryaPIC.PIC_TYPE,
        AT_FLAG: infoMerchantFinaryaPIC.AT_FLAG
      };
      MerchantStore.push(RAW);
    }
    result.datas = MerchantStore;
    return result;
  }

  //GET  MERCHANT DATA BY ID
  @get('/finarya-attention/non-digipos/pic/view/{id}', {
    responses: {
      '200': {
        description: 'Merchant Finarya PIC type model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(MerchantFinaryaPic, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findByIdMerchantFinaryaPIC(
    @param.path.number('id') id: number,
    @param.filter(MerchantFinaryaPic, {exclude: 'where'}) filter?: FilterExcludingWhere<MerchantFinaryaPic>
  ): Promise<Object> {
    var result: any; result = {success: true, message: '', datas: []};
    var infoMerchantFinaryaPIC = await this.merchantFinaryaPicRepository.findOne({where: {and: [{ID: id}]}}, filter);
    if (infoMerchantFinaryaPIC != null) {
      var RAW = {
        ID: infoMerchantFinaryaPIC.ID,
        PIC_NAME: infoMerchantFinaryaPIC.PIC_NAME,
        PIC_JABATAN: infoMerchantFinaryaPIC.PIC_JABATAN,
        PIC_ALAMAT: infoMerchantFinaryaPIC.PIC_ALAMAT,
        PIC_EMAIL: infoMerchantFinaryaPIC.PIC_EMAIL,
        PIC_SIGN: infoMerchantFinaryaPIC.PIC_SIGN,
        PIC_NOTELP: infoMerchantFinaryaPIC.PIC_NOTELP,
        PT_NAME: infoMerchantFinaryaPIC.PT_NAME,
        PT_ALAMAT: infoMerchantFinaryaPIC.PT_ALAMAT,
        PT_NOTELP: infoMerchantFinaryaPIC.PT_NOTELP,
        PT_NPWP: infoMerchantFinaryaPIC.PT_NPWP,
        PT_ACOUNT_NAME: infoMerchantFinaryaPIC.PT_ACOUNT_NAME,
        PT_ACOUNT_NUMBER: infoMerchantFinaryaPIC.PT_ACOUNT_NUMBER,
        PT_BANK_NAME: infoMerchantFinaryaPIC.PT_BANK_NAME,
        PT_BRANCH: infoMerchantFinaryaPIC.PT_BRANCH,
        PIC_TYPE: infoMerchantFinaryaPIC.PIC_TYPE,
        AT_FLAG: infoMerchantFinaryaPIC.AT_FLAG
      };
      result.datas = RAW;
    } else {
      result.success = false;
      result.message = "Merchant Finarya PIC not found  !";
    }
    return result;
  }


  //CREATE MERCHANT STORE
  @post('/finarya-attention/non-digipos/pic/create', {
    responses: {
      '200': {
        description: 'merchant finarya PIC create model instance',
        content: {'application/json': {schema: getModelSchemaRef(MerchantFinaryaPic)}},
      },
    },
  })
  async createMerchantFinaryaPIC(
    @requestBody.file()
    requestFile: Request,
    @inject(RestBindings.Http.RESPONSE) response: Response,
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    return new Promise<object>(async (resolve, reject) => {

      //SET STORAGE DAN FILE NAME UPLOAD
      const STORAGE_CONFIG = multer.diskStorage({
        destination: path.join(__dirname, './../../.non-digipos/attention/'),
        filename: (req, file, cb) => {
          cb(null, Date.now() + "-" + file.originalname);
        },
      });

      //CONFIG UPLOAD NAME FIELD
      const UPLOAD_ACTION = multer({
        storage: STORAGE_CONFIG,
        fileFilter: async function (req, file, callback) {
          var ext = path.extname(file.originalname).toLowerCase();
          if (ext !== '.png' && ext !== '.jpg' && ext !== '.jpeg') {
            return callback(new Error('Only png dan jpg are allowed file upload !'));
          }
          callback(null, true);
        }
      }).fields([{name: 'PIC_SIGN', maxCount: 1}]);

      UPLOAD_ACTION(requestFile, response, async (errUploadFile: any) => {
        if (errUploadFile) {
          resolve({success: false, message: errUploadFile, datas: []});
        } else {
          var result: any; result = {success: true, message: '', datas: []};

          //SET FIELD UPLOADED
          var BODY_PARAM: any; BODY_PARAM = {};
          for (var i in requestFile.body) {
            BODY_PARAM[i] = requestFile.body[i];
          }
          BODY_PARAM["PIC_SIGN"] = ControllerConfig.onCurrentTime().toString() + "_PIC_SIGN" + ".png";


          var FILE_UPLOAD: any; FILE_UPLOAD = requestFile.files;
          var PIC_SIGN: any; PIC_SIGN = "";
          for (var iFile in FILE_UPLOAD) {
            var InfiFile = FILE_UPLOAD[iFile];
            for (var iFilePath in InfiFile) {
              if (InfiFile[iFilePath]["fieldname"] == 'PIC_SIGN') {
                PIC_SIGN = InfiFile[iFilePath];
              }
            }
          }

          var IS_UPLOADED = true;
          if (PIC_SIGN == "") {
            IS_UPLOADED = false;
            resolve({success: false, message: "Create merchant error, please upload PIC sign !", datas: []});
          }


          if (PIC_SIGN == "") {
            if (PIC_SIGN != "") {
              try {
                fs.unlinkSync(PIC_SIGN.path);
              } catch (err) { }
            }
          } else {

            try {
              fs.rename(PIC_SIGN.path, PIC_SIGN.destination + "/" + BODY_PARAM["PIC_SIGN"], function (err) {
                if (err) {
                  IS_UPLOADED = false;
                  resolve({success: false, message: err, datas: []});
                }
              });
            } catch (error) {
              IS_UPLOADED = false;
              resolve({success: false, message: error, datas: []});
              try {
                fs.unlinkSync(PIC_SIGN.path);
              } catch (err) { }
            }
          }

          if (IS_UPLOADED) {
            try {

              BODY_PARAM["ID"] = undefined;
              BODY_PARAM["AT_CREATE"] = ControllerConfig.onCurrentTime().toString();
              BODY_PARAM["AT_UPDATE"] = undefined;
              BODY_PARAM["AT_FLAG"] = BODY_PARAM["AT_FLAG"] == null || BODY_PARAM["AT_FLAG"] == undefined ? 0 : BODY_PARAM["AT_FLAG"];
              BODY_PARAM["AT_WHO"] = USER_ID;

              var MERCHANT_FINARYA_PIC_INFO = await this.merchantFinaryaPicRepository.create(BODY_PARAM);
              resolve({success: true, message: 'success create !', datas: BODY_PARAM});

            } catch (error) {
              try {
                fs.unlinkSync(PIC_SIGN.destination + "/" + BODY_PARAM["PIC_SIGN"]);
              } catch (err) { }
              resolve({success: false, message: error, datas: []});
            }
          }
        }
      });
    });
  }

  //UPDATE MERCHANT STORE
  @post('/finarya-attention/non-digipos/pic/update/{id}', {
    responses: {
      '200': {
        description: 'merchant Finarya PIC Update model instance',
        content: {'application/json': {schema: getModelSchemaRef(MerchantFinaryaPic)}},
      },
    },
  })
  async updateMerchantFinaryaPIC(
    @param.path.number('id') id: number,
    @requestBody.file()
    requestFile: Request,
    @inject(RestBindings.Http.RESPONSE) response: Response,
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    return new Promise<object>(async (resolve, reject) => {


      var HAS_MERCHANT_FINARYA_PIC = await this.merchantFinaryaPicRepository.findOne({where: {and: [{ID: id}]}});
      if (HAS_MERCHANT_FINARYA_PIC != null) {

        //SET STORAGE DAN FILE NAME UPLOAD
        const STORAGE_CONFIG = multer.diskStorage({
          destination: path.join(__dirname, './../../.non-digipos/attention/'),
          filename: (req, file, cb) => {
            cb(null, Date.now() + "-" + file.originalname);
          },
        });

        //CONFIG UPLOAD NAME FIELD
        const UPLOAD_ACTION = multer({
          storage: STORAGE_CONFIG,
          fileFilter: async function (req, file, callback) {
            var ext = path.extname(file.originalname).toLowerCase();
            if (ext !== '.png' && ext !== '.jpg' && ext !== '.jpeg') {
              return callback(new Error('Only png dan jpg are allowed file upload !'));
            }
            callback(null, true);
          }
        }).fields([{name: 'PIC_SIGN', maxCount: 1}]);

        UPLOAD_ACTION(requestFile, response, async (errUploadFile: any) => {
          if (errUploadFile) {
            resolve({success: false, message: errUploadFile, datas: []});
          } else {
            var result: any; result = {success: true, message: '', datas: []};

            //SET FIELD UPLOADED
            var BODY_PARAM: any; BODY_PARAM = {};
            var DELETE_ARR = ["ID", "AT_CREATE"];
            for (var i in requestFile.body) {
              BODY_PARAM[i] = requestFile.body[i];
              if (DELETE_ARR.indexOf(i) != -1) {
                delete BODY_PARAM[i];
              }
            }


            var FILE_UPLOAD: any; FILE_UPLOAD = requestFile.files;
            var PIC_SIGN: any; PIC_SIGN = "";
            for (var iFile in FILE_UPLOAD) {
              var InfiFile = FILE_UPLOAD[iFile];
              for (var iFilePath in InfiFile) {
                if (InfiFile[iFilePath]["fieldname"] == 'PIC_SIGN') {
                  PIC_SIGN = InfiFile[iFilePath];
                }
              }
            }

            BODY_PARAM["PIC_SIGN"] = PIC_SIGN == "" ? HAS_MERCHANT_FINARYA_PIC?.PIC_SIGN : ControllerConfig.onCurrentTime().toString() + "_PIC_SIGN" + ".png";


            var IS_UPLOADED = true;
            if (PIC_SIGN == "") {
              if (PIC_SIGN != "") {
                try {
                  fs.unlinkSync(PIC_SIGN.path);
                } catch (err) { }
              }
            } else {
              try {
                fs.rename(PIC_SIGN.path, PIC_SIGN.destination + "/" + BODY_PARAM["PIC_SIGN"], function (err) {
                  if (err) {
                    IS_UPLOADED = false;
                    resolve({success: false, message: err, datas: []});
                  }
                });
              } catch (error) {
                IS_UPLOADED = false;
                resolve({success: false, message: error, datas: []});
                try {
                  fs.unlinkSync(PIC_SIGN.path);
                } catch (err) { }
              }
            }


            if (IS_UPLOADED) {
              try {
                BODY_PARAM["AT_UPDATE"] = ControllerConfig.onCurrentTime().toString();
                BODY_PARAM["AT_FLAG"] = BODY_PARAM["AT_FLAG"] == null || BODY_PARAM["AT_FLAG"] == undefined ? 0 : BODY_PARAM["AT_FLAG"];
                BODY_PARAM["AT_WHO"] = USER_ID;

                var MERCHANT_FINARYA_PIC_INFO = await this.merchantFinaryaPicRepository.updateById(id, BODY_PARAM);
                resolve({success: true, message: "success update merchant finarya PIC !", datas: []});

              } catch (error) {
                try {
                  fs.unlinkSync(PIC_SIGN.path);
                } catch (err) { }
                resolve({success: false, message: error, datas: []});
              }
            }
          }
        });
      } else {
        resolve({success: false, message: 'merchant finarya PIC no found !', datas: []});
      }

    });
  }


  //DELETE MERCHANT
  @post('/finarya-attention/non-digipos/pic/delete/{id}', {
    responses: {
      '200': {
        description: 'merchant finarya PIC delete model instance',
        content: {'application/json': {schema: getModelSchemaRef(MerchantFinaryaPic)}},
      },
    },
  })
  async deleteMerchantFinaryaPIC(
    @param.path.number('id') id: number,
  ): Promise<Object> {
    var result: any; result = {success: true, message: '', datas: []};

    var HAS_MERCHANT_FINARYA_PIC = await this.merchantFinaryaPicRepository.findOne({where: {and: [{ID: id}]}});
    if (HAS_MERCHANT_FINARYA_PIC != null) {
      try {
        var PIC_SIGN = path.join(__dirname, './../../.non-digipos/attention/' + HAS_MERCHANT_FINARYA_PIC.PIC_SIGN);

        try {
          fs.unlinkSync(PIC_SIGN);
        } catch (err) { }

        await this.merchantFinaryaPicRepository.deleteById(id);
        result.succes = true;
        result.message = "success delete !";
      } catch (error) {
        result.succes = false;
        result.datas = [];
        result.message = error;
      }
    } else {
      result.succes = false;
      result.datas = [];
      result.message = "Delete error merchant finarya PIC not found !";
    }
    return result;
  }



}
