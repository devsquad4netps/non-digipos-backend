import {authenticate} from '@loopback/authentication';
import {inject} from '@loopback/core';
import {repository} from '@loopback/repository';
import {post, requestBody} from '@loopback/rest';
import {SecurityBindings, securityId, UserProfile} from '@loopback/security';
import path from 'path';
import {DataDigiposRepository, DataDistributeBukpotRepository} from '../repositories';
import {ControllerConfig} from './controller.config';
import {MailConfig} from './mail.config';

export const OnMailDocumentShareByPeriodeBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ["periode", "group"],
        properties: {
          periode: {
            type: 'string',
          },
          group: {
            type: 'string',
          }
        }
      }
    },
  },
};

export const OnMailDocumentShareByTypePeriodeBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ["periode", "email", "type_doc"],
        properties: {
          periode: {
            type: 'string',
          },
          email: {
            type: 'array',
            items: {type: 'string'},
          },
          type_doc: {
            type: 'array',
            items: {type: 'string'},
          }
        }
      }
    },
  },
};

@authenticate('jwt')
export class MailDocumentShareController {
  constructor(
    @repository(DataDigiposRepository)
    public dataDigiposRepository: DataDigiposRepository,
    @repository(DataDistributeBukpotRepository)
    public dataDistributeBukpotRepository: DataDistributeBukpotRepository,
  ) { }

  @post('/mail-document-share/share-by-periode', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: OnMailDocumentShareByPeriodeBody
          }
        },
      },
    },
  })
  async MailDocumentShareByPeriode(
    @requestBody(OnMailDocumentShareByPeriodeBody) reqBody: {periode: string, group: string},
    @inject(SecurityBindings.USER)
    currentUserProfile: UserProfile,
  ): Promise<Object> {
    var result: any; result = {success: true, message: "", data: []};
    var UserID = currentUserProfile[securityId];
    var sqlRole = "SELECT " +
      "User.id, " +
      "User.email," +
      "role_info.RoleId ," +
      "role_info.name  " +
      "FROM " +
      "User " +
      "INNER JOIN (SELECT Rolemapping.UserId, Rolemapping.RoleId, Role.name FROM Rolemapping INNER JOIN  Role ON Rolemapping.RoleId = Role.id ) as role_info on role_info.UserId = User.id " +
      "HAVING role_info.name = '" + reqBody.group + "'";
    var infoRole = await this.dataDigiposRepository.execute(sqlRole);
    var emailAccount: any; emailAccount = [];
    for (var iRole in infoRole) {
      var rawRole = infoRole[iRole];
      if (emailAccount.indexOf(rawRole.email) == -1) {
        emailAccount.push(rawRole.email);
      }
    }

    var documentAD = await this.dataDigiposRepository.find({where: {and: [{ar_stat: 2}, {trx_date_group: reqBody.periode}]}});
    for (var iDoc in documentAD) {
      var rawDoc = documentAD[iDoc];
      var DataSplit = rawDoc.trx_date_group.split("-")
      var param = {
        type_doc: "Document Digipos",
        ad_name: rawDoc.ad_name,
        name_doc: "Document Digipos Periode " + ControllerConfig.onGetMonth(parseInt(DataSplit[1])) + " - " + DataSplit[0]
      }
      var DocumentAttach: any; DocumentAttach = [];
      if (rawDoc.google_drive_a == 1) {
        DocumentAttach.push({
          filename: rawDoc.ba_attachment_a,
          path: path.join(__dirname, '../../.digipost/berita_acara/ad_a/' + rawDoc.ba_attachment_a)
        });

        DocumentAttach.push({
          filename: 'summary_' + rawDoc.ba_attachment_a,
          path: path.join(__dirname, '../../.digipost/berita_acara/ad_a/summary_' + rawDoc.ba_attachment_a)
        });
      }

      if (rawDoc.google_drive_b == 1) {
        DocumentAttach.push({
          filename: rawDoc.ba_attachment_b,
          path: path.join(__dirname, '../../.digipost/berita_acara/ad_b/' + rawDoc.ba_attachment_b)
        });

        DocumentAttach.push({
          filename: 'summary_' + rawDoc.ba_attachment_b,
          path: path.join(__dirname, '../../.digipost/berita_acara/ad_b/summary_' + rawDoc.ba_attachment_b)
        });
      }

      //=========================DOCUMENT AD B
      //INVOICE BEFORE SIGN
      if (rawDoc.distribute_file_invoice == 1) {
        DocumentAttach.push({
          filename: rawDoc.inv_attacment,
          path: path.join(__dirname, '../../.digipost/invoice/' + rawDoc.inv_attacment)
        });
      }

      //INVOICE AFTER SIGN
      if (rawDoc.distribute_invoice_ttd == 1) {
        DocumentAttach.push({
          filename: rawDoc.attach_invoice_ttd,
          path: path.join(__dirname, '../../.digipost/doc-distribute/invoice-sign/' + rawDoc.attach_invoice_ttd)
        });
      }

      //FAKTUR PAJAK
      if (rawDoc.vat_distribute == 1) {
        DocumentAttach.push({
          filename: rawDoc.vat_attachment,
          path: path.join(__dirname, '../../.digipost/doc-distribute/faktur-pajak/' + rawDoc.vat_attachment)
        });
      }

      //DATA BUKPOT
      var dataBukpot = await this.dataDistributeBukpotRepository.findOne({where: {and: [{periode: rawDoc.trx_date_group}, {bukpot_status: 2}, {ad_code: rawDoc.ad_code}]}});
      if (dataBukpot != null) {
        DocumentAttach.push({
          filename: dataBukpot.bukpot_attachment,
          path: path.join(__dirname, '../../.digipost/doc-distribute/bukpot/' + dataBukpot.bukpot_attachment)
        });
      }

      if (DocumentAttach.length == 0) {

      } else {
        MailConfig.onSendMailDocumentAD("DIGIPOS DOCUMENT SHARE " + rawDoc.ad_name, emailAccount, param, DocumentAttach, (r: any) => {
          console.log(r);
        });
      }
    }
    return emailAccount;
  }

  @post('/mail-document-share/user-and-type', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: OnMailDocumentShareByTypePeriodeBody
          }
        },
      },
    },
  })
  async MailDocumentShareByUserType(
    @requestBody(OnMailDocumentShareByTypePeriodeBody) reqBody: {periode: string, email: [], type_doc: []},
    @inject(SecurityBindings.USER)
    currentUserProfile: UserProfile,
  ): Promise<Object> {
    var result: any; result = {success: true, message: "", data: []};
    var UserID = currentUserProfile[securityId];
    var TYPE_DOC: any; TYPE_DOC = reqBody.type_doc;

    var documentAD = await this.dataDigiposRepository.find({where: {and: [{ar_stat: 2}, {trx_date_group: reqBody.periode}]}});
    for (var iDoc in documentAD) {
      var rawDoc = documentAD[iDoc];
      var DataSplit = rawDoc.trx_date_group.split("-")
      var param = {
        type_doc: "Document Digipos",
        ad_name: rawDoc.ad_name,
        name_doc: "Document Digipos Periode " + ControllerConfig.onGetMonth(parseInt(DataSplit[1])) + " - " + DataSplit[0]
      }
      var DocumentAttach: any; DocumentAttach = [];
      if (rawDoc.google_drive_a == 1) {
        if (TYPE_DOC.indexOf("BAR_A") != -1) {
          DocumentAttach.push({
            filename: rawDoc.ba_attachment_a,
            path: path.join(__dirname, '../../.digipost/berita_acara/ad_a/' + rawDoc.ba_attachment_a)
          });

          DocumentAttach.push({
            filename: 'summary_' + rawDoc.ba_attachment_a,
            path: path.join(__dirname, '../../.digipost/berita_acara/ad_a/summary_' + rawDoc.ba_attachment_a)
          });
        }
      }

      if (rawDoc.google_drive_b == 1) {
        if (TYPE_DOC.indexOf("BAR_B") != -1) {
          DocumentAttach.push({
            filename: rawDoc.ba_attachment_b,
            path: path.join(__dirname, '../../.digipost/berita_acara/ad_b/' + rawDoc.ba_attachment_b)
          });

          DocumentAttach.push({
            filename: 'summary_' + rawDoc.ba_attachment_b,
            path: path.join(__dirname, '../../.digipost/berita_acara/ad_b/summary_' + rawDoc.ba_attachment_b)
          });
        }
      }

      //=========================DOCUMENT AD B
      //INVOICE BEFORE SIGN
      if (rawDoc.distribute_file_invoice == 1) {
        if (TYPE_DOC.indexOf("INVOICE") != -1) {
          DocumentAttach.push({
            filename: rawDoc.inv_attacment,
            path: path.join(__dirname, '../../.digipost/invoice/' + rawDoc.inv_attacment)
          });
        }
      }

      //INVOICE AFTER SIGN
      if (rawDoc.distribute_invoice_ttd == 2) {
        if (TYPE_DOC.indexOf("INVOICE_SIGN") != -1) {
          DocumentAttach.push({
            filename: rawDoc.attach_invoice_ttd,
            path: path.join(__dirname, '../../.digipost/doc-distribute/invoice-sign/' + rawDoc.attach_invoice_ttd)
          });
        }
      }

      //FAKTUR PAJAK
      if (rawDoc.vat_distribute == 2) {
        if (TYPE_DOC.indexOf("FAKTUR_PAJAK") != -1) {
          DocumentAttach.push({
            filename: rawDoc.vat_attachment,
            path: path.join(__dirname, '../../.digipost/doc-distribute/faktur-pajak/' + rawDoc.vat_attachment)
          });
        }
      }

      //DATA BUKPOT
      var dataBukpot = await this.dataDistributeBukpotRepository.findOne({where: {and: [{periode: rawDoc.trx_date_group}, {bukpot_status: 2}, {ad_code: rawDoc.ad_code}]}});
      if (dataBukpot != null) {
        if (TYPE_DOC.indexOf("BUKPOT") != -1) {
          DocumentAttach.push({
            filename: dataBukpot.bukpot_attachment,
            path: path.join(__dirname, '../../.digipost/doc-distribute/bukpot/' + dataBukpot.bukpot_attachment)
          });
        }
      }

      if (DocumentAttach.length == 0) {

      } else {
        MailConfig.onSendMailDocumentAD("DIGIPOS DOCUMENT SHARE " + rawDoc.ad_name, reqBody.email, param, DocumentAttach, (r: any) => {
          console.log(r);
        });
      }
    }
    return reqBody.email;
  }

}
