// Uncomment these imports to begin using these cool features!

import {authenticate} from '@loopback/authentication';
import {inject} from '@loopback/core';
import {Filter, FilterExcludingWhere, repository} from '@loopback/repository';
import {get, getModelSchemaRef, param, post, requestBody} from '@loopback/rest';
import {SecurityBindings, securityId, UserProfile} from '@loopback/security';
import {MerchantProduct, MerchantProductSkema, MerchantProductTribe} from '../models';
import {MerchantProductDatastoreRepository, MerchantProductGroupRepository, MerchantProductMasterRepository, MerchantProductSummaryRepository, MerchantProductTribeRepository, MerchantProductVariableRepository} from '../repositories';
import {MerchantProductSkemaRepository} from '../repositories/merchant-product-skema.repository';
import {MerchantProductRepository} from '../repositories/merchant-product.repository';
import {MerchantRepository} from '../repositories/merchant.repository';
import {UserRepository} from '../repositories/user.repository';
import {ControllerConfig} from './controller.config';


@authenticate('jwt')
export class MerchantProductActionController {
  constructor(
    @repository(MerchantRepository)
    public merchantRepository: MerchantRepository,
    @repository(MerchantProductSkemaRepository)
    public merchantProductSkemaRepository: MerchantProductSkemaRepository,
    @repository(MerchantProductRepository)
    public merchantProductRepository: MerchantProductRepository,
    @repository(MerchantProductDatastoreRepository)
    public merchantProductDatastoreRepository: MerchantProductDatastoreRepository,
    @repository(MerchantProductSummaryRepository)
    public merchantProductSummaryRepository: MerchantProductSummaryRepository,
    @repository(MerchantProductGroupRepository)
    public merchantProductGroupRepository: MerchantProductGroupRepository,
    @repository(MerchantProductVariableRepository)
    public merchantProductVariableRepository: MerchantProductVariableRepository,
    @repository(MerchantProductMasterRepository)
    public merchantProductMasterRepository: MerchantProductMasterRepository,
    @repository(MerchantProductTribeRepository)
    public merchantProductTribeRepository: MerchantProductTribeRepository,
    @inject(SecurityBindings.USER)
    public currentUserProfile: UserProfile,
    @repository(UserRepository)
    public userRepository: UserRepository,
  ) { }



  //GET ALL MERCHANT PRODUCT SKEMA DATA
  @get('/merchant-product-action/skema/views', {
    responses: {
      '200': {
        description: 'Array of Merchant Product Skema model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(MerchantProductSkema, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async findMerchantProductSkema(
    @param.filter(MerchantProductSkema) filter?: Filter<MerchantProductSkema>,
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};

    var MPS_STORE: any; MPS_STORE = [];
    var MPS_INFO = await this.merchantProductSkemaRepository.find(filter);
    for (var i in MPS_INFO) {
      var MPS_RAW: any; MPS_RAW = MPS_INFO[i];
      var RAW: any; RAW = {};
      var FIELDS = ["MPS_ID", "MPS_LABEL", "MPS_PATH_TEMPLATE", "MPS_PATH_TEMPLATE_PLUS", "MPS_PATH_TEMPLATE_MINUS", "AT_FLAG"];
      for (var key in MPS_RAW) {
        if (FIELDS.indexOf(key) == -1) {
          continue;
        }
        RAW[key] = MPS_RAW[key];
      }
      MPS_STORE.push(RAW);
    }
    result.datas = MPS_STORE;
    return result;
  }

  //GET  MERCHANT DATA BY ID
  @get('/merchant-product-action/skema/view/{id}', {
    responses: {
      '200': {
        description: 'Product merchant skema type model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(MerchantProductSkema, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findByIdMerchantProductSkema(
    @param.path.number('id') id: number,
    @param.filter(MerchantProductSkema, {exclude: 'where'}) filter?: FilterExcludingWhere<MerchantProductSkema>
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};
    var MPS_INFO = await this.merchantProductSkemaRepository.findOne({where: {and: [{MPS_ID: id}]}}, filter);
    if (MPS_INFO != null) {
      var RAW = {
        MPS_ID: MPS_INFO.MPS_ID,
        MPS_LABEL: MPS_INFO.MPS_LABEL,
        MPS_PATH_TEMPLATE: MPS_INFO.MPS_PATH_TEMPLATE,
        MPS_PATH_TEMPLATE_PLUS: MPS_INFO.MPS_PATH_TEMPLATE_PLUS,
        MPS_PATH_TEMPLATE_MINUS: MPS_INFO.MPS_PATH_TEMPLATE_MINUS,
        AT_FLAG: MPS_INFO.AT_FLAG
      }
      result.datas = RAW;
      result.success = true;
      result.message = "success !";
    } else {
      result.success = false;
      result.message = "Error, Merchant product skema not found !";
    }

    return result;
  }

  @post('/merchant-product-action/skema/create', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {'application/json': {schema: getModelSchemaRef(MerchantProductSkema)}},
      },
    },
  })
  async createMerchantProductSkema(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(MerchantProductSkema, {
            title: 'NewDataDigipos',
            exclude: ['MPS_ID', 'AT_CREATE', 'AT_UPDATE', 'AT_WHO'],
          }),
        },
      },
    })
    dataMerchantProductSkema: any,
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};


    var BODY_PARAM: any; BODY_PARAM = {};
    for (var i in dataMerchantProductSkema) {
      BODY_PARAM[i] = dataMerchantProductSkema[i];
    }

    BODY_PARAM["AT_CREATE"] = ControllerConfig.onCurrentTime().toString();
    BODY_PARAM["AT_UPDATE"] = undefined;
    BODY_PARAM["AT_WHO"] = USER_ID;
    BODY_PARAM["AT_FLAG"] = BODY_PARAM["AT_FLAG"] == undefined || BODY_PARAM["AT_FLAG"] == null ? 0 : BODY_PARAM["AT_FLAG"];

    try {
      var CREATE_SKEMA = await this.merchantProductSkemaRepository.create(BODY_PARAM);
      result.success = true;
      result.message = "Success create !";
      result.data = CREATE_SKEMA;
    } catch (error) {
      result.success = false;
      result.message = error;
    }
    return result;
  }

  @post('/merchant-product-action/skema/update/{id}', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {'application/json': {schema: getModelSchemaRef(MerchantProductSkema)}},
      },
    },
  })
  async updateMerchantProductSkema(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(MerchantProductSkema, {
            title: 'NewDataDigipos',
            exclude: ['MPS_ID', 'AT_CREATE', 'AT_UPDATE', 'AT_WHO'],
          }),
        },
      },
    })
    dataMerchantProduct: any,
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};

    var BODY_PARAM: any; BODY_PARAM = {};

    var DELETE_ARR = ["MPS_ID", "AT_CREATE", "AT_UPDATE", "AT_WHO"];
    for (var i in dataMerchantProduct) {
      if (DELETE_ARR.indexOf(i) != -1) {
        continue;
      }
      BODY_PARAM[i] = dataMerchantProduct[i];
    }

    BODY_PARAM["AT_UPDATE"] = ControllerConfig.onCurrentTime().toString();
    BODY_PARAM["AT_WHO"] = USER_ID;

    try {
      await this.merchantProductSkemaRepository.updateById(id, BODY_PARAM);
      result.success = true;
      result.message = "Success update produk skema !";
    } catch (error) {
      result.success = false;
      result.message = error;
    }

    return result;
  }


  @post('/merchant-product-action/skema/delete/{id}', {
    responses: {
      '200': {
        description: 'DELETE MERCHANT PRODUCT SKEMA model instance',
        content: {'application/json': {schema: getModelSchemaRef(MerchantProduct)}},
      },
    },
  })
  async deleteMerchantProductSkema(
    @param.path.number('id') id: number
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};
    var MERCHANT_SKEMA = await this.merchantProductSkemaRepository.count({MPS_ID: id});

    if (MERCHANT_SKEMA.count > 0) {
      var PRODUCT = await this.merchantProductRepository.count({MP_TYPE_SKEMA: id});
      if (PRODUCT.count == 0) {
        try {
          await this.merchantProductSkemaRepository.deleteById(id);
          result.success = true;
          result.message = "Success delete skema !";
        } catch (error) {
          result.succes = false;
          result.message = "Error delete, skema not found !";
        }
      } else {
        result.success = false;
        result.message = "Error delete, skema used by product !";
      }
    } else {
      result.success = false;
      result.message = "Error delete, skema not found !";
    }

    return result;
  }



  //GET ALL MERCHANT PRODUCT DATA
  @get('/merchant-product-action/views', {
    responses: {
      '200': {
        description: 'Array of Merchant Product model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(MerchantProduct, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async findMerchantProduct(
    @param.filter(MerchantProduct) filter?: Filter<MerchantProduct>,
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};
    var MerchantProductStore: any; MerchantProductStore = [];
    var MerchantProductInfo = await this.merchantProductRepository.find(filter);
    for (var i in MerchantProductInfo) {
      var infoMerchantProduct: any; infoMerchantProduct = MerchantProductInfo[i];
      var RAW: any; RAW = {};
      var FIELDS = ["MP_ID", "MP_CODE", "MP_BRAND", "MP_LABEL", "MP_DESCRIPTION", "MP_TYPE_INVOICE", "MP_TYPE_SKEMA", "MPT_ID", "M_ID", "AT_FLAG"];
      for (var key in infoMerchantProduct) {
        if (FIELDS.indexOf(key) == -1) {
          continue;
        }
        RAW[key] = infoMerchantProduct[key];
      }

      RAW["PRODUCT_TRIBE"] = await this.merchantProductTribeRepository.findById(infoMerchantProduct.MPT_ID, {
        fields: {
          MPT_ID: true,
          MPT_CODE: true,
          MPT_LABEL: true
        }
      });

      RAW["PRODUCT_SKEMA"] = await this.merchantProductSkemaRepository.findById(infoMerchantProduct.MP_TYPE_SKEMA, {
        fields: {
          MPS_ID: true,
          MPS_LABEL: true,
          MPS_PATH_TEMPLATE: true
        }
      });

      RAW["MERCHANT"] = await this.merchantRepository.findById(infoMerchantProduct.M_ID, {
        fields: {
          M_ID: true,
          M_CODE: true,
          M_NAME: true,
          M_LEGAL_NAME: true,
        }
      });

      MerchantProductStore.push(RAW);
    }
    result.datas = MerchantProductStore;
    return result;
  }

  //GET  MERCHANT DATA BY ID
  @get('/merchant-product-action/view/{id}', {
    responses: {
      '200': {
        description: 'Product merchant  type model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(MerchantProduct, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findByIdMerchantProduct(
    @param.path.number('id') id: number,
    @param.filter(MerchantProduct, {exclude: 'where'}) filter?: FilterExcludingWhere<MerchantProduct>
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};

    var MP_INFO: any; MP_INFO = await this.merchantProductRepository.findOne({where: {and: [{MP_ID: id}]}}, filter);
    if (MP_INFO != null) {
      var RAW: any; RAW = {}
      var FIELDS = ["MP_ID", "MP_CODE", "MP_BRAND", "MP_LABEL", "MP_DESCRIPTION", "MP_TYPE_INVOICE", "MP_TYPE_SKEMA", "MPT_ID", "M_ID", "AT_FLAG"];
      for (var key in MP_INFO) {
        if (FIELDS.indexOf(key) == -1) {
          continue;
        }
        RAW[key] = MP_INFO[key];
      }

      RAW["PRODUCT_TRIBE"] = await this.merchantProductTribeRepository.findById(RAW.MPT_ID, {
        fields: {
          MPT_ID: true,
          MPT_CODE: true,
          MPT_LABEL: true
        }
      });

      RAW["PRODUCT_SKEMA"] = await this.merchantProductSkemaRepository.findById(RAW.MP_TYPE_SKEMA, {
        fields: {
          MPS_ID: true,
          MPS_LABEL: true,
          MPS_PATH_TEMPLATE: true
        }
      });

      RAW["MERCHANT"] = await this.merchantRepository.findById(RAW.M_ID, {
        fields: {
          M_ID: true,
          M_CODE: true,
          M_NAME: true,
          M_LEGAL_NAME: true,
        }
      });

      result.datas = RAW;
      result.success = true;
      result.message = "success !";
    } else {
      result.success = false;
      result.message = "Error, Merchant product not found !";
    }

    return result;
  }


  @post('/merchant-product-action/create', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {'application/json': {schema: getModelSchemaRef(MerchantProduct)}},
      },
    },
  })
  async createMerchantProduct(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(MerchantProduct, {
            title: 'PRODUCT_CREATE',
            exclude: ['MP_ID', 'MP_CODE', 'AT_CREATE', 'AT_UPDATE', 'AT_WHO'],
          }),
        },
      },
    })
    dataMerchantProduct: any,
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};

    var BODY_PARAM: any; BODY_PARAM = {};
    for (var i in dataMerchantProduct) {
      BODY_PARAM[i] = dataMerchantProduct[i];
    }

    var AVALIABLE_CODE: boolean; AVALIABLE_CODE = false;
    var COUNT_MERCHANT_PRODUCT = await this.merchantProductRepository.count();
    var NO = COUNT_MERCHANT_PRODUCT.count + 1;
    var M_CODE = "";
    do {
      var CODE = "0000000000".substring(NO.toString().length) + NO;
      var VALID_CODE = await this.merchantRepository.findOne({where: {and: [{M_CODE: CODE}]}});
      if (VALID_CODE == null) {
        M_CODE = CODE;
        AVALIABLE_CODE = true;
      }
      NO++;
    }
    while (AVALIABLE_CODE == false);

    BODY_PARAM["MP_CODE"] = M_CODE;
    BODY_PARAM["AT_CREATE"] = ControllerConfig.onCurrentTime().toString();
    BODY_PARAM["AT_UPDATE"] = undefined;
    BODY_PARAM["AT_WHO"] = USER_ID;

    var MERCHANT_ID = BODY_PARAM["M_ID"] == undefined || BODY_PARAM["M_ID"] == null ? -1 : BODY_PARAM["M_ID"];
    var PRODUCT_SKEMA = BODY_PARAM["MP_TYPE_SKEMA"] == undefined || BODY_PARAM["MP_TYPE_SKEMA"] == null ? -1 : BODY_PARAM["MP_TYPE_SKEMA"];


    var HAS_MERCHANT = await this.merchantRepository.findOne({where: {and: [{M_ID: MERCHANT_ID}, {AT_FLAG: 1}]}});
    if (HAS_MERCHANT != null) {
      var HAS_SKEMA = await this.merchantProductSkemaRepository.findOne({where: {and: [{MPS_ID: PRODUCT_SKEMA}, {AT_FLAG: 1}]}});
      if (HAS_SKEMA != null) {
        try {
          var MERCHANT_PRODUCT_CREATE = await this.merchantProductRepository.create(BODY_PARAM);
          result.success = true;
          result.message = "success create merchant product !";
          result.datas = MERCHANT_PRODUCT_CREATE;
        } catch (error) {
          result.success = false;
          result.message = error;
        }
      } else {
        result.success = false;
        result.message = "Error, merchant product skema not found !";
      }
    } else {
      result.success = false;
      result.message = "Error, merchant not found !";
    }
    return result;
  }


  @post('/merchant-product-action/update/{id}', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {'application/json': {schema: getModelSchemaRef(MerchantProduct)}},
      },
    },
  })
  async updateMerchantProduct(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(MerchantProduct, {
            title: 'PRODUCT_UPDATE',
            exclude: ['MP_ID', 'MP_CODE', 'AT_CREATE', 'AT_UPDATE', 'AT_WHO'],
          }),
        },
      },
    })
    dataMerchantProduct: any,
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};

    var BODY_PARAM: any; BODY_PARAM = {};

    var DELETE_ARR = ["MP_ID", "MP_CODE", "AT_CREATE", "AT_UPDATE", "AT_WHO"];
    for (var i in dataMerchantProduct) {
      if (DELETE_ARR.indexOf(i) != -1) {
        continue;
      }
      BODY_PARAM[i] = dataMerchantProduct[i];
    }

    BODY_PARAM["AT_UPDATE"] = ControllerConfig.onCurrentTime().toString();
    BODY_PARAM["AT_WHO"] = USER_ID;

    var INFO_MERCHANT = await this.merchantProductRepository.findOne({where: {and: [{MP_ID: id}]}});

    if (INFO_MERCHANT != null) {

      var MERCHANT_ID = BODY_PARAM["M_ID"] == undefined || BODY_PARAM["M_ID"] == null ? -1 : BODY_PARAM["M_ID"];
      var PRODUCT_SKEMA = BODY_PARAM["MP_TYPE_SKEMA"] == undefined || BODY_PARAM["MP_TYPE_SKEMA"] == null ? -1 : BODY_PARAM["MP_TYPE_SKEMA"];

      var HAS_MERCHANT = await this.merchantRepository.findOne({where: {and: [{M_ID: MERCHANT_ID}, {AT_FLAG: 1}]}});
      if (HAS_MERCHANT != null) {
        var HAS_SKEMA = await this.merchantProductSkemaRepository.findOne({where: {and: [{MPS_ID: PRODUCT_SKEMA}, {AT_FLAG: 1}]}});
        if (HAS_SKEMA != null) {
          try {
            await this.merchantProductRepository.updateById(id, BODY_PARAM);
            result.success = true;
            result.message = "success update merchant product !"
          } catch (error) {
            result.success = false;
            result.message = error;
          }
        } else {
          result.success = false;
          result.message = "Error, merchant product skema not found !";
        }
      } else {
        result.success = false;
        result.message = "Error, merchant not found !";
      }
    } else {
      result.success = false;
      result.message = "Error, merchant product not found !";
    }

    return result;
  }

  @post('/merchant-product-action/delete/{id}', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {'application/json': {schema: getModelSchemaRef(MerchantProduct)}},
      },
    },
  })
  async deleteMerchantProduct(
    @param.path.number('id') id: number
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};

    var MERCHANT_PRODUCT = await this.merchantProductRepository.count({MP_ID: id});
    if (MERCHANT_PRODUCT.count > 0) {
      var MERCAHNT_DATASOURCE = await this.merchantProductDatastoreRepository.count({MP_ID: id});
      if (MERCAHNT_DATASOURCE.count == 0) {
        var MERCHANT_PRODUCT_GROUP = await this.merchantProductGroupRepository.count({MP_ID: id});
        if (MERCHANT_PRODUCT_GROUP.count == 0) {
          try {
            await this.merchantProductRepository.deleteById(id);
            result.success = true;
            result.message = "Success delete merchant product !";
          } catch (error) {
            result.success = false;
            result.message = error;
          }
        } else {
          result.success = false;
          result.message = "Error delete, merchant product have data  mdr amount !";
        }

      } else {
        result.success = false;
        result.message = "Error delete, merchant product have data transaction  !";
      }
    } else {
      result.success = false;
      result.message = "Error, merchant product not found !";
    }

    return result;
  }


  @post('/merchant-product-action/tribe/create', {
    responses: {
      '200': {
        description: 'Product tribe Create model instance',
        content: {'application/json': {schema: getModelSchemaRef(MerchantProductTribe)}},
      },
    },
  })
  async createMerchantProductTribe(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(MerchantProductTribe, {
            title: 'PRODUCT_TRIBE_CREATE',
            exclude: ['MPT_ID', 'AT_CREATE', 'AT_UPDATE', 'AT_WHO'],
          }),
        },
      },
    })
    dataMerchantProductTribe: any,
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};

    var BODY_PARAM: any; BODY_PARAM = {};
    for (var i in dataMerchantProductTribe) {
      BODY_PARAM[i] = dataMerchantProductTribe[i];
    }
    BODY_PARAM["AT_CREATE"] = ControllerConfig.onCurrentTime().toString();
    BODY_PARAM["AT_UPDATE"] = undefined;
    BODY_PARAM["AT_WHO"] = USER_ID;

    var HAS_CODE = await this.merchantProductTribeRepository.count({and: [{MPT_CODE: BODY_PARAM.MPT_CODE}]});
    if (HAS_CODE.count == 0) {
      var HAS_TRIBE = await this.merchantProductTribeRepository.count({and: [{MPT_LABEL: BODY_PARAM.MPT_LABEL}]});
      if (HAS_TRIBE.count == 0) {
        try {
          var CREATE_TRIBE = await this.merchantProductTribeRepository.create(BODY_PARAM);
          result.success = true;
          result.message = "Success !";
          result.datas = CREATE_TRIBE;
        } catch (error) {
          result.success = false;
          result.message = error;
        }
      } else {
        result.success = false;
        result.message = "Error, merchant product tribe label  exities, please entry others label !";
      }
    } else {
      result.success = false;
      result.message = "Error, merchant product tribe code exities, please entry others code !";
    }
    return result;
  }


  @post('/merchant-product-action/tribe/update/{id}', {
    responses: {
      '200': {
        description: 'Product Tribe update model instance',
        content: {'application/json': {schema: getModelSchemaRef(MerchantProductTribe)}},
      },
    },
  })
  async updateMerchantProductTribe(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(MerchantProductTribe, {
            title: 'PRODUCT_TRIBE_UPDATE',
            exclude: ['MPT_ID', 'AT_CREATE', 'AT_UPDATE', 'AT_WHO'],
          }),
        },
      },
    })
    dataMerchantProductTribe: any,
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};

    var BODY_PARAM: any; BODY_PARAM = {};
    var DELETE_ARR = ["MPT_ID", "AT_CREATE", "AT_UPDATE", "AT_WHO"];
    for (var i in dataMerchantProductTribe) {
      if (DELETE_ARR.indexOf(i) != -1) {
        continue;
      }
      BODY_PARAM[i] = dataMerchantProductTribe[i];
    }

    BODY_PARAM["AT_UPDATE"] = ControllerConfig.onCurrentTime().toString();
    BODY_PARAM["AT_WHO"] = USER_ID;

    var HAS_CODE = await this.merchantProductTribeRepository.count({and: [{MPT_CODE: BODY_PARAM.MPT_CODE}, {MPT_ID: {neq: id}}]});
    if (HAS_CODE.count == 0) {
      var HAS_TRIBE = await this.merchantProductTribeRepository.count({and: [{MPT_LABEL: BODY_PARAM.MPT_LABEL}, {MPT_ID: {neq: id}}]});
      if (HAS_TRIBE.count == 0) {
        try {
          var UPDATE_TRIBE = await this.merchantProductTribeRepository.updateById(id, BODY_PARAM);
          result.success = true;
          result.message = "Success !";
        } catch (error) {
          result.success = false;
          result.message = error;
        }
      } else {
        result.success = false;
        result.message = "Error, merchant product tribe label exities, please entry others label !";
      }
    } else {
      result.success = false;
      result.message = "Error, merchant product tribe code exities, please entry others code !";
    }
    return result;
  }

  @post('/merchant-product-action/tribe/delete/{id}', {
    responses: {
      '200': {
        description: 'Product Tribe delete model instance',
        content: {'application/json': {schema: getModelSchemaRef(MerchantProductTribe)}},
      },
    },
  })
  async deleteMerchantProductTribe(
    @param.path.number('id') id: number
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};

    var HAS_ID = await this.merchantProductTribeRepository.count({and: [{MPT_ID: id}]});
    if (HAS_ID.count > 0) {
      var HAS_USED = await this.merchantProductRepository.find({where: {and: [{MPT_ID: id}]}});
      if (HAS_USED.length == 0) {
        try {
          await this.merchantProductTribeRepository.deleteById(id);
        } catch (error) {
          result.success = false;
          result.message = error;
        }
      } else {
        result.success = false;
        result.message = "Error, merchant product tribe already used !";
      }
    } else {
      result.success = false;
      result.message = "Error, merchant product tribe not found !";
    }
    return result;
  }

  @get('/merchant-product-action/tribe/views', {
    responses: {
      '200': {
        description: 'Array of Merchant Product tribe model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(MerchantProductTribe, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async findMerchantProductTribe(
    @param.filter(MerchantProductTribe) filter?: Filter<MerchantProductTribe>,
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};

    var MPS_STORE: any; MPS_STORE = [];
    var MPS_INFO = await this.merchantProductTribeRepository.find(filter);
    for (var i in MPS_INFO) {
      var MPS_RAW: any; MPS_RAW = MPS_INFO[i];
      var RAW: any; RAW = {};
      var FIELDS = ["MPT_ID", "MPT_LABEL", "MPT_CODE", "AT_FLAG"];
      for (var key in MPS_RAW) {
        if (FIELDS.indexOf(key) == -1) {
          continue;
        }
        RAW[key] = MPS_RAW[key];
      }
      MPS_STORE.push(RAW);
    }
    result.datas = MPS_STORE;
    return result;
  }

  @get('/merchant-product-action/tribe/view/{id}', {
    responses: {
      '200': {
        description: 'Product merchant tribe view type model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(MerchantProductTribe, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findByIdMerchantProductTribe(
    @param.path.number('id') id: number,
    @param.filter(MerchantProductTribe, {exclude: 'where'}) filter?: FilterExcludingWhere<MerchantProductTribe>
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};
    var MP_INFO: any; MP_INFO = await this.merchantProductTribeRepository.findOne({where: {and: [{MPT_ID: id}]}}, filter);
    if (MP_INFO != null) {
      var RAW: any; RAW = {}
      var FIELDS = ["MPT_ID", "MPT_CODE", "MPT_LABEL", "AT_FLAG"];
      for (var key in MP_INFO) {
        if (FIELDS.indexOf(key) == -1) {
          continue;
        }
        RAW[key] = MP_INFO[key];
      }

      result.datas = RAW;
      result.success = true;
      result.message = "success !";
    } else {
      result.success = false;
      result.message = "Error, Merchant product tribe not found !";
    }

    return result;
  }


}
