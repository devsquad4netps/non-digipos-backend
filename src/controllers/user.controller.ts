import {authenticate, TokenService} from '@loopback/authentication';
import {TokenServiceBindings, UserCredentialsRepository, UserServiceBindings} from '@loopback/authentication-jwt';
import {inject} from '@loopback/core';
import {Filter, model, property, repository} from '@loopback/repository';
import {get, getModelSchemaRef, param, post, requestBody} from '@loopback/rest';
import {SecurityBindings, securityId, UserProfile} from '@loopback/security';
import {genSalt, hash} from 'bcryptjs';
import _ from 'lodash';
import randomstring from "randomstring";
import {DataNotification, User} from '../models';
import {DataconfigRepository, DataNotificationRepository, RolemappingRepository, UserRepository} from '../repositories';
import {Credentials, MyUserService} from '../services/user.service';
import {ControllerConfig} from './controller.config';
import {MailConfig} from './mail.config';
var jwt = require('jsonwebtoken');

@model()
export class NewUserRequest extends User {

  @property({
    type: 'number',
    required: true,
  })
  role: number;

  @property({
    type: 'string',
    required: true,
  })
  password: string;
}


export const OnReadNotifRequestBodyALL = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ["status"],
        properties: {
          status: {
            type: 'string',
          }
        }
      }
    },
  },
};


export const OnReadNotifRequestBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ["notif_id"],
        properties: {
          notif_id: {
            type: 'number',
          }
        }
      }
    },
  },
};

export const ForgotPassword = {
  description: 'The input of forgot function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['email'],
        properties: {
          email: {
            type: 'string',
            format: 'email'
          },
        }
      }
    },
  },
};

export const CheckForgotToken = {
  description: 'The input of forgot function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['token'],
        properties: {
          token: {
            type: 'string',
          },
        }
      }
    },
  },
};

export const ResetExecutePassword = {
  description: 'The input of forgot function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['old_pass', 'new_pass', 'confirm_pass'],
        properties: {
          old_pass: {
            type: 'string',
            minLength: 8,
          },
          new_pass: {
            type: 'string',
            minLength: 8,
          },
          confirm_pass: {
            type: 'string',
            minLength: 8,
          },
        }
      }
    },
  },
};

export const ForgotExecutePassword = {
  description: 'The input of forgot function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['new_pass', 'confirm_pass', 'token'],
        properties: {
          new_pass: {
            type: 'string',
            minLength: 8,
          },
          confirm_pass: {
            type: 'string',
            minLength: 8,
          },
          token: {
            type: 'string',
          },
        }
      }
    },
  },
};


const CredentialsSchema = {
  type: 'object',
  required: ['email', 'password'],
  properties: {
    email: {
      type: 'string',
      format: 'email',
    },
    password: {
      type: 'string',
      minLength: 8,
    },
  },
};

export const OnRegisterTokenFBRequestBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ["register_token"],
        properties: {
          register_token: {
            type: 'string',
          }
        }
      }
    },
  },
};



export const CredentialsRequestBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {schema: CredentialsSchema},
  },
};

export class UserController {
  constructor(
    @inject(TokenServiceBindings.TOKEN_SERVICE)
    public jwtService: TokenService,
    @inject(UserServiceBindings.USER_SERVICE)
    public userService: MyUserService,
    @inject(SecurityBindings.USER, {optional: true})
    public user: User,
    @repository(UserRepository) protected userRepository: UserRepository,
    @repository(RolemappingRepository)
    public roleMappingRepository: RolemappingRepository,
    @repository(DataconfigRepository)
    public dataconfigRepository: DataconfigRepository,
    @repository(UserCredentialsRepository)
    public userCredentialsRepository: UserCredentialsRepository,
    @repository(DataNotificationRepository)
    public dataNotificationRepository: DataNotificationRepository,
  ) { }

  finding = () => {
    return "WORK";
  }


  @post('/users/login', {
    responses: {
      '200': {
        description: 'Token',
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                token: {
                  type: 'string',
                },
              },
            },
          },
        },
      },
    },
  })
  async login(
    @requestBody(CredentialsRequestBody) credentials: Credentials,
  ): Promise<{data: Object}> {
    // // ensure the user exists, and the password is correct
    const user = await this.userService.verifyCredentials(credentials);
    // // convert a User object into a UserProfile object (reduced set of properties)
    const userProfile = this.userService.convertToUserProfile(user);

    var status = {};

    if (user.realm === "DELETED") {
      status = {"status": false, "message": "username or password vailed !"};
    } else {
      const token = await this.jwtService.generateToken(userProfile);
      const tokenNotification = jwt.sign(userProfile, '22665362778878766524534', {expiresIn: 60 * 60});
      user['token'] = token;
      user['token_notif'] = tokenNotification;
      user['status'] = true;
      user['message'] = "success";
      status = user;
    }
    // create a JSON Web Token based on the user profile

    return {
      data: {"user": status}
    };

  }

  @authenticate('jwt')
  @post('/signup', {
    responses: {
      '200': {
        description: 'User',
        content: {
          'application/json': {
            schema: {
              'x-ts-type': User,
            },
          },
        },
      },
    },
  })
  async signUp(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(NewUserRequest, {
            title: 'NewUser',
          }),
        },
      },
    })
    newUserRequest: NewUserRequest,
  ): Promise<User> {
    const password = await hash(newUserRequest.password, await genSalt());

    const savedUser = await this.userRepository.create(
      _.omit(newUserRequest, 'password', 'role'),
    );

    await this.roleMappingRepository.create({UserId: savedUser.id, RoleId: newUserRequest.role})
    await this.userRepository.userCredentials(savedUser.id).create({password});
    return savedUser;
  }

  @authenticate('jwt')
  @post('/reset-password', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: ResetExecutePassword
          }
        },
      },
    },
  })
  async resetExecutePassword(
    @requestBody(ResetExecutePassword) resetExecutePassword: {old_pass: string, new_pass: string, confirm_pass: string},
    @inject(SecurityBindings.USER)
    currentUserProfile: UserProfile,
  ): Promise<Object> {
    var returnMsg: any; returnMsg = {success: false, message: "error"};
    var UserId = currentUserProfile[securityId];
    var userInfo = await this.userRepository.findById(UserId);
    if (userInfo != null) {
      try {
        const user = await this.userService.verifyCredentials({email: userInfo.email, password: resetExecutePassword.old_pass});
        if (user != null) {
          if (resetExecutePassword.new_pass.trim() == resetExecutePassword.confirm_pass.trim()) {
            try {
              const passwordNew = await hash(resetExecutePassword.new_pass, await genSalt());
              await this.userCredentialsRepository.updateAll({password: passwordNew}, {userId: user.id});
              returnMsg.success = true;
              returnMsg.message = "success reset password !";
            } catch (error) {
              returnMsg.success = false;
              returnMsg.message = error;
            }
          } else {
            returnMsg.success = false;
            returnMsg.message = "confirm password and password are not the same !";
          }
        } else {
          returnMsg.success = false;
          returnMsg.message = "user can't found !";
        }
      } catch (error) {
        returnMsg.success = false;
        returnMsg.message = "old password not valid, please try again !";
      }
    } else {
      returnMsg.success = false;
      returnMsg.message = "user can't found !";
    }
    return returnMsg;
  }


  @post('/check-forgot-token', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: CheckForgotToken
          }
        },
      },
    },
  })
  async tokenForgotCheck(
    @requestBody(CheckForgotToken) checkForgotToken: {token: string},
  ): Promise<Object> {
    var returnMsg: any; returnMsg = {success: false, message: "error"};
    var hasUser = await this.userRepository.findOne({where: {verificationToken: checkForgotToken.token}});
    if (hasUser != null) {
      var TimeToken = checkForgotToken.token.substring(64);
      var dateNow = new Date();
      var Difference_In_Time = dateNow.getTime() - parseInt(TimeToken);
      var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
      if (Difference_In_Days >= 1) {
        returnMsg.success = false;
        returnMsg.message = "token is expired, please forgot password again !";
      } else {
        returnMsg.success = true;
        returnMsg.message = "token is avaliable !";
      }
    } else {
      returnMsg.success = false;
      returnMsg.message = "token not avaliable, please entry other token !";
    }
    return returnMsg;
  }

  @post('/change-forgot-password', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: ForgotExecutePassword
          }
        },
      },
    },
  })
  async forgotExecutePassword(
    @requestBody(ForgotExecutePassword) forgotExecutePassword: {new_pass: string, confirm_pass: string, token: string},
  ): Promise<Object> {
    var returnMsg: any; returnMsg = {success: false, message: "error"};
    var hasUser = await this.userRepository.findOne({where: {verificationToken: forgotExecutePassword.token}});
    if (hasUser != null) {
      var TimeToken = forgotExecutePassword.token.substring(64);
      var dateNow = new Date();
      var Difference_In_Time = dateNow.getTime() - parseInt(TimeToken);
      var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
      if (Difference_In_Days >= 1) {
        returnMsg.success = false;
        returnMsg.message = "token is expired, please forgot password again !";
      } else {
        if (forgotExecutePassword.new_pass.trim() == forgotExecutePassword.confirm_pass.trim()) {
          try {
            const passwordNew = await hash(forgotExecutePassword.new_pass, await genSalt());
            await this.userCredentialsRepository.updateAll({password: passwordNew}, {userId: hasUser.id});

            returnMsg.success = true;
            returnMsg.message = "success reset password !";
          } catch (error) {
            returnMsg.success = false;
            returnMsg.message = error;
          }
        } else {
          returnMsg.success = false;
          returnMsg.message = "confirm password and password are not the same !";
        }
      }
    } else {
      returnMsg.success = false;
      returnMsg.message = "token not avaliable, please entry other token !";
    }
    return returnMsg;
  }

  @post('/forgot', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: ForgotPassword
          }
        },
      },
    },
  })
  async forgotPass(
    @requestBody(ForgotPassword) forgotPass: {email: string},
  ): Promise<Object> {
    var returnMsg: any; returnMsg = {success: false, message: "error"};

    var CurrentTime = ControllerConfig.onCurrentTime();
    var verified = randomstring.generate() + randomstring.generate() + CurrentTime.toString();
    var dataConfig = await this.dataconfigRepository.findOne({where: {and: [{config_group_id: 14}, {state: 1}]}});
    var User = await this.userRepository.findOne({where: {and: [{email: forgotPass.email}, {state: 1}]}});
    if (User != null) {
      try {
        await this.userRepository.updateById(User.id, {
          verificationToken: verified
        });
        await new Promise<object>(async (resolve, reject) => {

          MailConfig.onSendMailForgotPassword(forgotPass.email, {
            username: "@" + User?.username,
            token: verified,
            link_forgot: verified
          }, (rMail: any) => {
            if (rMail.success == true) {
              returnMsg.success = true;
              returnMsg.message = "Please check your email for create new password !";
              resolve(returnMsg);
            } else {
              returnMsg.success = false;
              returnMsg.message = rMail.message;
              resolve(returnMsg);
            }
          });

        });
      } catch (error) {
        returnMsg.success = false;
        returnMsg.message = error;
      }
    } else {
      returnMsg.success = false;
      returnMsg.message = "Email not valid, please entry email exities in application !"
    }
    return returnMsg;
  }

  @authenticate('jwt')
  @post('/user/register-token-fb', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: OnRegisterTokenFBRequestBody
          }
        },
      },
    },
  })
  async registrasi_token_fb(
    @requestBody(OnRegisterTokenFBRequestBody) reqToken: {register_token: string},
    @inject(SecurityBindings.USER)
    currentUserProfile: UserProfile,
  ): Promise<Object> {
    var result: any; result = {success: true, message: "", data: []};
    var UserID = currentUserProfile[securityId];
    var userInfo = await this.userRepository.findById(UserID);
    if (userInfo.at_key == reqToken.register_token) {
      result.success = true;
      result.message = "success"
      return result;
    }

    return result;
  }


  @authenticate('jwt')
  @post('/user/on_read_notif_all', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: OnReadNotifRequestBodyALL
          }
        },
      },
    },
  })
  async read_notif_all(
    @requestBody(OnReadNotifRequestBodyALL) notif: {status: number},
    @inject(SecurityBindings.USER)
    currentUserProfile: UserProfile,
  ): Promise<Object> {
    var result: any; result = {success: true, message: "", data: {}};
    var UserID = currentUserProfile[securityId];
    var userInfo = await this.userRepository.findById(UserID);
    try {
      await this.dataNotificationRepository.updateAll({
        at_flag: 1,
        at_read: ControllerConfig.onCurrentTime().toString()
      }, {at_flag: 0});
      result.success = true;
      result.data = {}
      result.message = "success"
    } catch (error) {
      result.success = false;
      result.message = error
    }
    return result;
  }


  @authenticate('jwt')
  @post('/user/on_read_notif', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: OnReadNotifRequestBody
          }
        },
      },
    },
  })
  async read_notif(
    @requestBody(OnReadNotifRequestBody) notif: {notif_id: number},
    @inject(SecurityBindings.USER)
    currentUserProfile: UserProfile,
  ): Promise<Object> {
    var result: any; result = {success: true, message: "", data: {}};
    var UserID = currentUserProfile[securityId];
    var userInfo = await this.userRepository.findById(UserID);
    try {
      await this.dataNotificationRepository.updateById(notif.notif_id, {
        at_flag: 1,
        at_read: ControllerConfig.onCurrentTime().toString()
      });
      result.success = true;
      result.data = await this.dataNotificationRepository.findById(notif.notif_id);
      result.message = "success"
    } catch (error) {
      result.success = false;
      result.message = error
    }
    return result;
  }


  @authenticate('jwt')
  @get('/user/notification', {
    responses: {
      '200': {
        description: 'Array of AdMaster model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(DataNotification, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find_notification(
    @inject(SecurityBindings.USER)
    currentUserProfile: UserProfile,
    @param.filter(DataNotification) filter?: Filter<DataNotification>,
  ): Promise<Object> {
    var result: any; result = {success: true, message: "", data: []};
    var UserID = currentUserProfile[securityId];
    var userInfo = await this.userRepository.findById(UserID);
    var company = userInfo.company_code;
    var filtering: any; filtering = filter;
    filtering["order"] = ["at_create desc"];
    var dataNotif = await this.dataNotificationRepository.find(filtering);
    for (var i in dataNotif) {
      var rowNotif = dataNotif[i];
      if (rowNotif.app !== "DIGIPOS") {
        continue;
      }

      var allowNotif: any; allowNotif = [];
      if (rowNotif.to !== "" && rowNotif.to !== null && rowNotif.to !== undefined) {
        if (rowNotif.to != UserID) {
          if (allowNotif.indexOf(rowNotif.code) != -1) {
            continue;
          }
        }
      }

      if (rowNotif.target != "" && rowNotif.target != null) {
        var role = rowNotif.target?.split(",");
        var RoleMap = await this.roleMappingRepository.findOne({where: {UserId: UserID}});
        var RoleId = RoleMap?.RoleId == undefined ? "" : RoleMap?.RoleId.toString();
        if (role.indexOf(RoleId) == -1) {
          continue;
        }
      }
      result.data.push(rowNotif);
    }
    return result;
  }


}
