
import fs from 'fs';
import handlebars from 'handlebars';
import nodemailer from 'nodemailer';
import path from 'path';
import {MysqlAccessPortalDataSource, MysqlDataSource, NonDigiposDataSource} from '../datasources';
import {MerchantProductDistribusiRepository, MerchantProductDocumentRepository, MerchantProductGroupRepository, MerchantProductMasterRepository, MerchantProductRepository, MerchantProductSummaryRepository, MerchantRepository} from '../repositories';
import {ControllerConfig} from './controller.config';

const DATABASES = new MysqlDataSource();
const DATABASE_AD = new MysqlAccessPortalDataSource();
const DATABASE_NON_DIGIPOS = new NonDigiposDataSource();


let emailAcount = "nondigipos@4netps.co.id";
let transporter = nodemailer.createTransport({
  host: "mail.4netps.co.id",
  port: 465,
  secure: true, // true for 465, false for other ports
  auth: {
    user: emailAcount, // generated ethereal user
    pass: 'pass123nondigipos', // generated ethereal password
  },
});

var DISTRIBUSI_PROCESS_COUNT: number; DISTRIBUSI_PROCESS_COUNT = 0;
var DISTRIBUSI_PROCESS: any; DISTRIBUSI_PROCESS = {};

export class MerchantMailSenderFeedback {
  constructor() { }

  static onRunDistribusiDoc = async () => {
    for (var I in DISTRIBUSI_PROCESS) {DISTRIBUSI_PROCESS_COUNT++;};

    var MERCHANT = new MerchantRepository(DATABASE_NON_DIGIPOS);
    var MERCHANT_PRODUCT = new MerchantProductRepository(DATABASE_NON_DIGIPOS);
    var MERCHANT_PRODUCT_GROUP = new MerchantProductGroupRepository(DATABASE_NON_DIGIPOS);
    var MERCHANT_MASTER = new MerchantProductMasterRepository(DATABASE_NON_DIGIPOS);
    var MERCHANT_DISTRIBUSI = new MerchantProductDistribusiRepository(DATABASE_NON_DIGIPOS);
    var MERCHANT_DOCUMENT = new MerchantProductDocumentRepository(DATABASE_NON_DIGIPOS);
    var MERCHANT_SUMMARY = new MerchantProductSummaryRepository(DATABASE_NON_DIGIPOS);

    for (var I in DISTRIBUSI_PROCESS) {
      var KEY = I;
      var RAW_DISTRIBUSI = DISTRIBUSI_PROCESS[I];
      var ATTACHMENT_DOC: any; ATTACHMENT_DOC = [];

      var TRANSACTION_MASTER = await MERCHANT_MASTER.findOne({where: {and: [{MPM_ID: RAW_DISTRIBUSI.MPM_ID}]}});
      var DOC = await MERCHANT_DOCUMENT.find({where: {and: [{AT_FLAG: 1}, {MPM_ID: RAW_DISTRIBUSI.MPM_ID}]}});
      if (TRANSACTION_MASTER == null) {
        await MERCHANT_DISTRIBUSI.updateById(RAW_DISTRIBUSI.MPDIST_ID, {
          SEND_FLAG: 3,
          SEND_MESSAGE: "ERROR, TRANSACTION MERCHANT NOT FOUND !",
          AT_SEND: ControllerConfig.onCurrentTime().toString(),
        });

        DISTRIBUSI_PROCESS_COUNT--;
        delete DISTRIBUSI_PROCESS[KEY];
        MerchantMailSenderFeedback.onRunDistribusiDoc();
        break;
      }


      var DOC_AVA: any; DOC_AVA = [];
      var DOCUMENT_DISTRIBUSI_AVA: string; DOCUMENT_DISTRIBUSI_AVA = "";
      for (var i_doc_ava in DOC) {
        var RAW_DOC_AVA = DOC[i_doc_ava];
        //BAR
        if (RAW_DOC_AVA.MPDOC_TYPE == 1) {
          if (RAW_DISTRIBUSI.SEND_BAR == 1) {
            ATTACHMENT_DOC.push({
              filename: RAW_DOC_AVA.MPDOC_NAME,
              path: RAW_DOC_AVA.MPDOC_PATH
            });
            if (DOC_AVA.indexOf("DOCUMENT_BAR") == -1) {
              DOCUMENT_DISTRIBUSI_AVA += ", DOCUMENT BAR ( " + TRANSACTION_MASTER.MPM_NO_BAR + " )";
            }
          }
        }

        if (RAW_DOC_AVA.MPDOC_TYPE == 2) {
          if (RAW_DISTRIBUSI.SEND_BAR_SIGN == 1) {
            ATTACHMENT_DOC.push({
              filename: RAW_DOC_AVA.MPDOC_NAME,
              path: RAW_DOC_AVA.MPDOC_PATH
            });
            if (DOC_AVA.indexOf("DOCUMENT_BAR_SIGN") == -1) {
              DOCUMENT_DISTRIBUSI_AVA += ", DOCUMENT BAR SIGN ( " + TRANSACTION_MASTER.MPM_NO_BAR + " )";
            }
          }
        }

        if (RAW_DOC_AVA.MPDOC_TYPE == 3) {
          if (RAW_DISTRIBUSI.SEND_INVOICE == 1) {
            ATTACHMENT_DOC.push({
              filename: RAW_DOC_AVA.MPDOC_NAME,
              path: RAW_DOC_AVA.MPDOC_PATH
            });
            if (DOC_AVA.indexOf("DOCUMENT_INVOICE") == -1) {
              DOCUMENT_DISTRIBUSI_AVA += ", DOCUMENT INVOICE ( " + TRANSACTION_MASTER.MPM_NO_INVOICE + " )";
            }
          }
        }

        if (RAW_DOC_AVA.MPDOC_TYPE == 4) {
          if (RAW_DISTRIBUSI.SEND_INVOICE_SIGN == 1) {
            ATTACHMENT_DOC.push({
              filename: RAW_DOC_AVA.MPDOC_NAME,
              path: RAW_DOC_AVA.MPDOC_PATH
            });
            if (DOC_AVA.indexOf("DOCUMENT_INVOICE_SIGN") == -1) {
              DOCUMENT_DISTRIBUSI_AVA += ", DOCUMENT INVOICE SIGN ( " + TRANSACTION_MASTER.MPM_NO_INVOICE + " )";
            }
          }
        }

        if (RAW_DOC_AVA.MPDOC_TYPE == 5) {
          if (RAW_DISTRIBUSI.SEND_FAKTUR == 1) {
            ATTACHMENT_DOC.push({
              filename: RAW_DOC_AVA.MPDOC_NAME,
              path: RAW_DOC_AVA.MPDOC_PATH
            });
            if (DOC_AVA.indexOf("DOCUMENT_FAKTUR_PAJAK") == -1) {
              DOCUMENT_DISTRIBUSI_AVA += ", DOCUMENT FAKTUR PAJAK ( " + TRANSACTION_MASTER.MPM_NO_FAKTUR + " )";
            }
          }
        }

        if (RAW_DOC_AVA.MPDOC_TYPE == 6) {
          if (RAW_DISTRIBUSI.SEND_BUKPOT == 1) {
            ATTACHMENT_DOC.push({
              filename: RAW_DOC_AVA.MPDOC_NAME,
              path: RAW_DOC_AVA.MPDOC_PATH
            });
            if (DOC_AVA.indexOf("DOCUMENT_BUKTI_POTONG") == -1) {
              DOCUMENT_DISTRIBUSI_AVA += ", DOCUMENT BUKTI POTONG";
            }
          }
        }

        if (RAW_DOC_AVA.MPDOC_TYPE == 7) {
          if (RAW_DISTRIBUSI.SEND_OTHERS == 1) {
            ATTACHMENT_DOC.push({
              filename: RAW_DOC_AVA.MPDOC_NAME,
              path: RAW_DOC_AVA.MPDOC_PATH
            });
            if (DOC_AVA.indexOf("DOCUMENT_OTHERS") == -1) {
              DOCUMENT_DISTRIBUSI_AVA += ", DOCUMENT OTHERS";
            }
          }
        }

      }

      if (ATTACHMENT_DOC.length == 0) {
        await MERCHANT_DISTRIBUSI.updateById(RAW_DISTRIBUSI.MPDIST_ID, {
          SEND_FLAG: 3,
          SEND_MESSAGE: "ERROR, NOT FOUND DOCUMENT ATTAHMENT !",
          AT_SEND: ControllerConfig.onCurrentTime().toString(),
        });

        DISTRIBUSI_PROCESS_COUNT--;
        delete DISTRIBUSI_PROCESS[KEY];
        MerchantMailSenderFeedback.onRunDistribusiDoc();
        break;
      }

      var M_ID = TRANSACTION_MASTER.M_ID;
      var MP_MP = TRANSACTION_MASTER.MP_ID;

      var MERCHANT_INFO = await MERCHANT.findById(M_ID);
      var PRODUCT_INFO = await MERCHANT_PRODUCT.findById(MP_MP);
      var SUMMARY_INFO = await MERCHANT_SUMMARY.find({where: {and: [{MPM_ID: TRANSACTION_MASTER.MPM_ID}]}});

      var GROUP_ID: any; GROUP_ID = [];
      var GROUP_LABEL = "";
      for (var I_SM in SUMMARY_INFO) {
        var RAW_SUMMARY = SUMMARY_INFO[I_SM];
        if (GROUP_ID.indexOf(RAW_SUMMARY.MPG_ID) == -1) {
          GROUP_ID.push(RAW_SUMMARY.MPG_ID);
          var RAW_GROUP = await MERCHANT_PRODUCT_GROUP.findById(RAW_SUMMARY.MPG_ID);
          if (RAW_GROUP.MPG_LABEL != undefined) {
            GROUP_LABEL += ", " + RAW_GROUP.MPG_LABEL;
          }
        }
      }

      // var PRODUCT_GROUP_INFO = MERCHANT_PRODUCT_GROUP.find({where:{and:[{AT_FLAG : }]}})

      var PARAMS_EMAIL = {
        MESSAGE: RAW_DISTRIBUSI.MPDIST_MESSAGE,
        DOCUMENT: DOCUMENT_DISTRIBUSI_AVA.substring(1),
        PT: MERCHANT_INFO.M_LEGAL_NAME == undefined ? "" : MERCHANT_INFO.M_LEGAL_NAME,
        PRODUCT: (PRODUCT_INFO.MP_BRAND == undefined ? "" : PRODUCT_INFO.MP_BRAND),
        PRODUCT_GROUP: GROUP_LABEL.substring(1),
        PERIODE: TRANSACTION_MASTER.PERIODE
      };

      var SEND_EMAIL = /*RAW_DISTRIBUSI.SEND_TO + ";" +*/ RAW_DISTRIBUSI.SEND_CC;
      MerchantMailSenderFeedback.onSendMailDocAttach("SHARE DOC " + PARAMS_EMAIL.PT + " " + TRANSACTION_MASTER.PERIODE, SEND_EMAIL, PARAMS_EMAIL, ATTACHMENT_DOC, async (RESULT: any) => {
        if (RESULT.success == true) {

          if (RAW_DISTRIBUSI.AT_FEEDBACK_MERCHANT == 1) {
            await MERCHANT_MASTER.updateById(RAW_DISTRIBUSI.MPM_ID, {
              WAITING_SEND_MAIL: 0,
              AT_POSISI: 4,
              AT_FLAG: 0
            });
          } else {
            await MERCHANT_MASTER.updateById(RAW_DISTRIBUSI.MPM_ID, {
              WAITING_SEND_MAIL: 0,
              AT_POSISI: RAW_DISTRIBUSI.AT_POSISI_BEFORE,
              AT_FLAG: RAW_DISTRIBUSI.AT_FLAG_BEFORE
            });
          }

          await MERCHANT_DISTRIBUSI.updateById(RAW_DISTRIBUSI.MPDIST_ID, {
            SEND_FLAG: 2,
            SEND_MESSAGE: "SUCCESS DISTRIBUTE !",
            AT_SEND: ControllerConfig.onCurrentTime().toString(),
          });


          DISTRIBUSI_PROCESS_COUNT--;
          delete DISTRIBUSI_PROCESS[KEY];
          MerchantMailSenderFeedback.onRunDistribusiDoc();

        } else {
          await MERCHANT_DISTRIBUSI.updateById(RAW_DISTRIBUSI.MPDIST_ID, {
            SEND_FLAG: 3,
            SEND_MESSAGE: RESULT.message,
            AT_SEND: ControllerConfig.onCurrentTime().toString(),
          });

          await MERCHANT_MASTER.updateById(RAW_DISTRIBUSI.MPM_ID, {
            AT_POSISI: RAW_DISTRIBUSI.AT_POSISI_BEFORE,
            AT_FLAG: RAW_DISTRIBUSI.AT_POSISI_BEFORE,
            WAITING_SEND_MAIL: 0
          });

          DISTRIBUSI_PROCESS_COUNT--;
          delete DISTRIBUSI_PROCESS[KEY];
          MerchantMailSenderFeedback.onRunDistribusiDoc();
        }
      });
      break;
    }
  }

  static async onServiceMailDocDistribusi(db: MysqlDataSource) {
    // var NO_ANTRY = 0;
    // for (var i in TMP_PROSESS) {NO_ANTRY++;}



    var merchantProductDocDistribusi = new MerchantProductDistribusiRepository(db);
    var DataMailSenderDistribusi = await merchantProductDocDistribusi.find({where: {and: [{SEND_FLAG: 0}]}});
    for (var i in DataMailSenderDistribusi) {
      var RAW_MAIL_DISTRIBUTE = DataMailSenderDistribusi[i];
      if (RAW_MAIL_DISTRIBUTE.MPDIST_ID == undefined) {
        continue;
      }

      if (RAW_MAIL_DISTRIBUTE.MPDIST_MESSAGE == "" || RAW_MAIL_DISTRIBUTE.MPDIST_MESSAGE == null || RAW_MAIL_DISTRIBUTE.MPDIST_MESSAGE == undefined) {
        continue;
      }

      if (DISTRIBUSI_PROCESS[RAW_MAIL_DISTRIBUTE.MPDIST_ID] == undefined) {
        DISTRIBUSI_PROCESS[RAW_MAIL_DISTRIBUTE.MPDIST_ID] = {};
      }
      DISTRIBUSI_PROCESS[RAW_MAIL_DISTRIBUTE.MPDIST_ID] = RAW_MAIL_DISTRIBUTE;

    }

    if (DISTRIBUSI_PROCESS_COUNT == 0) {
      MerchantMailSenderFeedback.onRunDistribusiDoc();
    }
  }



  //SEND MAIL WITH ATTAHMENT
  static onSendMailDocAttach = async (title: string, to: any, params: any, attachments: any, callback: any) => {
    MerchantMailSenderFeedback.onReadHtmlFile(path.join(__dirname, '../../.non-digipos/template-mail/distribusi_doc.html'), async (err: any, html: any) => {
      if (err) {
        callback({success: false, message: err});
        return;
      }
      try {
        var template = handlebars.compile(html);
        var htmlToSend = template(params);
        // let testAccount = await nodemailer.createTestAccount();
        let info = await transporter.sendMail({
          from: '<' + emailAcount + '>', // sender address
          to: to, // list of receivers
          subject: title, // Subject line
          html: htmlToSend, // html body
          attachments: attachments
        });
        callback({success: true, message: info.messageId});
      } catch (error) {
        callback({success: false, message: error});
      }
    });
  }


  //READ TEMPLATE EMIAL
  static onReadHtmlFile = (path: string, callback: any) => {
    fs.readFile(path, {encoding: 'utf-8'}, function (err, html) {
      if (err) {
        throw err;
        callback(err);
      }
      else {
        callback(null, html);
      }
    });
  }


}
