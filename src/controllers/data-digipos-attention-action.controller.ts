import {authenticate} from '@loopback/authentication';
import {inject} from '@loopback/core';
import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where
} from '@loopback/repository';
import {
  del, get,
  getModelSchemaRef, param,


  patch, post,




  put,

  Request, requestBody, Response, RestBindings
} from '@loopback/rest';
import {SecurityBindings, securityId, UserProfile} from '@loopback/security';
import fs from 'fs';
import multer from 'multer';
import path from 'path';
import {DataDigiposAttention} from '../models';
import {DataDigiposAttentionRepository, DataDigiposRepository, DataNotificationRepository, UserRepository} from '../repositories';
import {ControllerConfig} from './controller.config';

@authenticate('jwt')
export class DataDigiposAttentionActionController {
  constructor(
    @repository(DataDigiposAttentionRepository)
    public dataDigiposAttentionRepository: DataDigiposAttentionRepository,
    @repository(DataDigiposRepository)
    public dataDigiposRepository: DataDigiposRepository,
    @repository(DataNotificationRepository)
    public dataNotificationRepository: DataNotificationRepository,
    @inject(SecurityBindings.USER)
    public currentUserProfile: UserProfile,
    @repository(UserRepository)
    public userRepository: UserRepository,
  ) { }

  @post('/data-digipos-attentions', {
    responses: {
      '200': {
        description: 'DataDigiposAttention model instance',
        content: {'application/json': {schema: getModelSchemaRef(DataDigiposAttention)}},
      },
    },
  })
  async create(
    // @requestBody({
    //   content: {
    //     'application/json': {
    //       schema: getModelSchemaRef(DataDigiposAttention, {
    //         title: 'NewDataDigiposAttention',
    //         exclude: ['attention_id'],
    //       }),
    //     },
    //   },
    // })
    // dataDigiposAttention: Omit<DataDigiposAttention, 'attention_id'>,
    @requestBody.file()
    requestFile: Request,
    @inject(RestBindings.Http.RESPONSE) response: Response,
  ): Promise<Object> {
    return new Promise<object>(async (resolve, reject) => {

      const storage = multer.diskStorage({
        destination: path.join(__dirname, './../../.digipost/sign_atten'),
        filename: (req, file, cb) => {
          cb(null, Date.now() + "-" + file.originalname);
        },
      });

      const upload = multer({
        storage: storage,
        fileFilter: async function (req, file, callback) {
          var ext = path.extname(file.originalname).toLowerCase();
          if (ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg') {
            return callback(new Error('Only images are allowed'));
          }
          callback(null, true);
        }
      }).array("sign_attention");

      await upload(requestFile, response, async (errUpload: any) => {
        // console.log(requestFile.body);
        if (errUpload) resolve({error: true, message: 'Only image are allowed'})
        else {
          var InfoBody: any; InfoBody = {};
          for (var i in requestFile.body) {
            InfoBody[i] = requestFile.body[i];
          }
          InfoBody["sign_attention"] = Date.now() + ".png";

          var files: any; files = requestFile.files;
          var file: any; file = "";
          for (var iFile in files) {
            var InfiFile = files[iFile];
            if (InfiFile.fieldname == 'sign_attention') {
              file = InfiFile;
            }
          }
          try {
            await this.dataDigiposAttentionRepository.create(InfoBody);
            fs.rename(file.path, file.destination + "/" + InfoBody["sign_attention"], function (err) {
              if (err) {console.log(err); return;}
              resolve({success: true, message: "success create"});
            });
          } catch (error) {
            resolve({error: true, message: error});
            try {
              fs.unlinkSync(file.path);
            } catch (err) {
            }
          }
        }
      });
    });
  }

  @get('/data-digipos-attentions/count', {
    responses: {
      '200': {
        description: 'DataDigiposAttention model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(DataDigiposAttention) where?: Where<DataDigiposAttention>,
  ): Promise<Count> {
    return this.dataDigiposAttentionRepository.count(where);
  }

  @get('/data-digipos-attentions', {
    responses: {
      '200': {
        description: 'Array of DataDigiposAttention model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(DataDigiposAttention, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(DataDigiposAttention) filter?: Filter<DataDigiposAttention>,
  ): Promise<DataDigiposAttention[]> {
    return this.dataDigiposAttentionRepository.find(filter);
  }

  @patch('/data-digipos-attentions', {
    responses: {
      '200': {
        description: 'DataDigiposAttention PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(DataDigiposAttention, {partial: true}),
        },
      },
    })
    dataDigiposAttention: DataDigiposAttention,
    @param.where(DataDigiposAttention) where?: Where<DataDigiposAttention>,
  ): Promise<Count> {
    return this.dataDigiposAttentionRepository.updateAll(dataDigiposAttention, where);
  }

  @get('/data-digipos-attentions/{id}', {
    responses: {
      '200': {
        description: 'DataDigiposAttention model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(DataDigiposAttention, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(DataDigiposAttention, {exclude: 'where'}) filter?: FilterExcludingWhere<DataDigiposAttention>
  ): Promise<DataDigiposAttention> {
    return this.dataDigiposAttentionRepository.findById(id, filter);
  }

  @patch('/data-digipos-attentions/{id}', {
    responses: {
      '204': {
        description: 'DataDigiposAttention PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(DataDigiposAttention, {partial: true}),
        },
      },
    })
    dataDigiposAttention: DataDigiposAttention,
  ): Promise<void> {
    await this.dataDigiposAttentionRepository.updateById(id, dataDigiposAttention);
  }

  @put('/data-digipos-attentions/{id}', {
    responses: {
      '204': {
        description: 'DataDigiposAttention PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    // @requestBody() dataDigiposAttention: DataDigiposAttention,
    @requestBody.file()
    requestFile: Request,
    @inject(RestBindings.Http.RESPONSE) response: Response,
  ): Promise<Object> {
    return new Promise<object>(async (resolve, reject) => {

      const storage = multer.diskStorage({
        destination: path.join(__dirname, './../../.digipost/sign_atten'),
        filename: (req, file, cb) => {
          cb(null, Date.now() + "-" + file.originalname);
        },
      });

      const upload = multer({
        storage: storage,
        fileFilter: async function (req, file, callback: any) {
          var ext = path.extname(file.originalname).toLowerCase();
          // console.log(ext);
          if (ext == '.png' || ext == '.jpg' || ext == '.gif' || ext == '.jpeg') {
            callback(null, true);
          } else {
            return callback("Only images upload allowed !", false);
          }
          callback(null, true);
        }
      }).fields([{name: 'sign_attention', maxCount: 1}]);//.array("sign_attention");

      await upload(requestFile, response, async (errUpload: any) => {
        // console.log(requestFile.body);
        if (errUpload) resolve({error: false, message: errUpload})
        else {
          var InfoBody: any; InfoBody = {};
          for (var i in requestFile.body) {
            InfoBody[i] = requestFile.body[i];
          }
          InfoBody["sign_attention"] = Date.now() + ".png";

          var files: any; files = requestFile.files;
          var file: any; file = "";
          for (var iFile in files) {
            var InfiFile = files[iFile];
            for (var iFilePath in InfiFile) {
              if (InfiFile[iFilePath]["fieldname"] == 'sign_attention') {
                file = InfiFile[iFilePath];
              }
            }
          }

          if (file == "") {
            delete InfoBody["sign_attention"];
          }

          console.log(InfoBody, InfiFile);

          try {
            var infoUpdate = await this.dataDigiposAttentionRepository.findById(id);
            if (infoUpdate != null) {
              await this.dataDigiposAttentionRepository.updateById(id, InfoBody);
            }

            if (infoUpdate.group == 2) {
              // await this.dataDigiposRepository.updateAll({
              //   acc_file_invoice: 0,
              //   generate_file_invoice: 0,
              //   distribute_file_invoice: 0
              // }, {and: [{trx_date_group: ControllerConfig.onPeriodeDigipos()}]});

              var userId = this.currentUserProfile[securityId];
              var roleNotifSetADB = ControllerConfig.onGetRoleNotifGroup("14010103");
              var infoUser = await this.userRepository.findById(userId)
              await this.dataNotificationRepository.create({
                types: "NOTIFICATION",
                target: roleNotifSetADB.role,
                to: "",
                from_who: infoUser.username,
                subject: roleNotifSetADB.label,
                body: "Update Data Attention untuk  Invoice  berhasil !",
                path_url: "",
                app: "DIGIPOS",
                code: "14010103",
                code_label: "",
                at_create: ControllerConfig.onCurrentTime().toString(),
                at_read: "",
                periode: ControllerConfig.onPeriodeDigipos(),
                at_flag: 0
              });

              ControllerConfig.onSendNotif({
                types: "NOTIFICATION",
                target: roleNotifSetADB.role,
                to: "",
                from_who: infoUser.username,
                subject: roleNotifSetADB.label,
                body: "Update Data Attention untuk Invoice berhasil !",
                path_url: "",
                app: "DIGIPOS",
                code: "14010103",
                code_label: "",
                at_create: ControllerConfig.onCurrentTime().toString(),
                at_read: "",
                periode: ControllerConfig.onPeriodeDigipos(),
                at_flag: 0
              });
            }

            if (infoUpdate.group == 1) {
              // await this.dataDigiposRepository.updateAll({
              //   generate_file_a: 0,
              //   generate_file_b: 0,
              //   google_drive_a: 0,
              //   google_drive_b: 0,
              //   sign_off_a: 0,
              //   sign_off_b: 0,
              //   acc_file_ba_a: 0,
              //   acc_file_ba_b: 0,
              // }, {and: [{trx_date_group: ControllerConfig.onPeriodeDigipos()}]});

              var userId = this.currentUserProfile[securityId];
              var roleNotifSetADB = ControllerConfig.onGetRoleNotifGroup("14010102");
              var infoUser = await this.userRepository.findById(userId)
              await this.dataNotificationRepository.create({
                types: "NOTIFICATION",
                target: roleNotifSetADB.role,
                to: "",
                from_who: infoUser.username,
                subject: roleNotifSetADB.label,
                body: "Update Data Attention untuk  BAR Recon  berhasil !",
                path_url: "",
                app: "DIGIPOS",
                code: "14010102",
                code_label: "",
                at_create: ControllerConfig.onCurrentTime().toString(),
                at_read: "",
                periode: ControllerConfig.onPeriodeDigipos(),
                at_flag: 0
              });

              ControllerConfig.onSendNotif({
                types: "NOTIFICATION",
                target: roleNotifSetADB.role,
                to: "",
                from_who: infoUser.username,
                subject: roleNotifSetADB.label,
                body: "Update Data Attention untuk  BAR Recon  berhasil !",
                path_url: "",
                app: "DIGIPOS",
                code: "14010102",
                code_label: "",
                at_create: ControllerConfig.onCurrentTime().toString(),
                at_read: "",
                periode: ControllerConfig.onPeriodeDigipos(),
                at_flag: 0
              });

            }


            fs.rename(file.path, file.destination + "/" + InfoBody["sign_attention"], function (err) {
              if (err) {console.log(err); return;}
              resolve({success: true, message: "success create"});
            });
          } catch (error) {
            resolve({error: true, message: error});
            try {
              fs.unlinkSync(file.path);
            } catch (err) {
            }
          }
        }
      });
    });

  }

  @del('/data-digipos-attentions/{id}', {
    responses: {
      '204': {
        description: 'DataDigiposAttention DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.dataDigiposAttentionRepository.deleteById(id);
  }
}
