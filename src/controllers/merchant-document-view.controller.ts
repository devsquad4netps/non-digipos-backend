// Uncomment these imports to begin using these cool features!

import {authenticate} from '@loopback/authentication';
import {inject} from '@loopback/core';
import {repository} from '@loopback/repository';
import {param, post, requestBody} from '@loopback/rest';

import {SecurityBindings, securityId, UserProfile} from '@loopback/security';
import fs from 'fs';
import path from 'path';
import {UserRepository} from '../repositories';


export const DocumentViewByNameBody = {
  description: 'The input paraam view function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['names'],
        properties: {
          names: {
            type: 'string',
          }
        }
      }
    },
  },
};


@authenticate('jwt')
export class MerchantDocumentViewController {
  constructor(
    @inject(SecurityBindings.USER)
    public currentUserProfile: UserProfile,
    @repository(UserRepository)
    public userRepository: UserRepository,
  ) { }

  @post('/document-view/file/pks', {
    responses: {
      '200': {
        description: 'Document View PKS model instance',
        content: {
          'application/json': {
            schema: DocumentViewByNameBody
          }
        },
      },
    },
  })
  async FileViewPKS(
    @requestBody(DocumentViewByNameBody) fileViewDoc: {names: string},
  ): Promise<Object> {
    return await new Promise<object>((resolve, reject) => {
      var USER_ID = this.currentUserProfile[securityId];
      var result: any; result = {success: true, message: '', data: []};

      var FILE_NAME = fileViewDoc.names.split("/").join("_");
      const FILE_PKS = path.join(__dirname, '../../.non-digipos/merchant/' + FILE_NAME);

      var FILE_FOUND = false;
      var MESSAGE: any; MESSAGE = "File not found !";

      try {
        if (fs.existsSync(FILE_PKS)) {
          FILE_FOUND = true;
          MESSAGE = FILE_PKS;
        }
      } catch (err) {
        FILE_FOUND = false;
        MESSAGE = err;
      }



      if (FILE_FOUND == true) {
        fs.readFile(FILE_PKS, function (err, data) {
          if (err) {
            result.success = false;
            result.message = err;
            result.data = null;
            resolve(result);
          } else {
            result.success = true;
            result.data = data.toString("base64");
            result.message = "success";
            resolve(result);
          }
        });
      } else {
        result.success = false;
        result.data = null;
        result.message = MESSAGE;
        resolve(result);
      }
    });
  }

  @post('/document-view/file/npwp', {
    responses: {
      '200': {
        description: 'Document View NPWP model instance',
        content: {
          'application/json': {
            schema: DocumentViewByNameBody
          }
        },
      },
    },
  })
  async FileViewNPWP(
    @requestBody(DocumentViewByNameBody) fileViewDoc: {names: string},
  ): Promise<Object> {
    return await new Promise<object>((resolve, reject) => {
      var USER_ID = this.currentUserProfile[securityId];
      var result: any; result = {success: true, message: '', data: []};

      var FILE_NAME = fileViewDoc.names.split("/").join("_");
      const FILE_PKS = path.join(__dirname, '../../.non-digipos/merchant/' + FILE_NAME);

      var FILE_FOUND = false;
      var MESSAGE: any; MESSAGE = "File not found !";

      try {
        if (fs.existsSync(FILE_PKS)) {
          FILE_FOUND = true;
          MESSAGE = FILE_PKS;
        }
      } catch (err) {
        FILE_FOUND = false;
        MESSAGE = err;
      }

      if (FILE_FOUND == true) {
        fs.readFile(FILE_PKS, function (err, data) {
          if (err) {
            result.success = false;
            result.message = err;
            result.data = null;
            resolve(result);
          } else {
            result.success = true;
            result.data = data.toString("base64");
            result.message = "success";
            resolve(result);
          }
        });
      } else {
        result.success = false;
        result.data = null;
        result.message = MESSAGE;
        resolve(result);
      }
    });
  }

  @post('/document-view/file/bar/{id}', {
    responses: {
      '200': {
        description: 'Document View BAR model instance',
        content: {
          'application/json': {
            schema: DocumentViewByNameBody
          }
        },
      },
    },
  })
  async FileViewBAR(
    @param.path.string('id') id: string,
    @requestBody(DocumentViewByNameBody) fileViewDoc: {names: string},
  ): Promise<Object> {
    return await new Promise<object>((resolve, reject) => {
      var USER_ID = this.currentUserProfile[securityId];
      var result: any; result = {success: true, message: '', data: []};

      var FILE_NAME = fileViewDoc.names.split("/").join("_");
      const FILE_BAR = path.join(__dirname, '../../.non-digipos/document/' + id + "/bar/" + FILE_NAME);

      var FILE_FOUND = false;
      var MESSAGE: any; MESSAGE = "File not found !";

      try {
        if (fs.existsSync(FILE_BAR)) {
          FILE_FOUND = true;
          MESSAGE = FILE_BAR;
        }
      } catch (err) {
        FILE_FOUND = false;
        MESSAGE = err;
      }

      if (FILE_FOUND == true) {
        fs.readFile(FILE_BAR, function (err, data) {
          if (err) {
            result.success = false;
            result.message = err;
            result.data = null;
            resolve(result);
          } else {
            result.success = true;
            result.data = data.toString("base64");
            result.message = "success";
            resolve(result);
          }
        });
      } else {
        result.success = false;
        result.data = null;
        result.message = MESSAGE;
        resolve(result);
      }
    });
  }

  @post('/document-view/file/bar/sign/{id}', {
    responses: {
      '200': {
        description: 'Document View BAR SIGN model instance',
        content: {
          'application/json': {
            schema: DocumentViewByNameBody
          }
        },
      },
    },
  })
  async FileViewBARSign(
    @param.path.string('id') id: string,
    @requestBody(DocumentViewByNameBody) fileViewDoc: {names: string},
  ): Promise<Object> {
    return await new Promise<object>((resolve, reject) => {
      var USER_ID = this.currentUserProfile[securityId];
      var result: any; result = {success: true, message: '', data: []};

      var FILE_NAME = fileViewDoc.names.split("/").join("_");
      const FILE_BAR = path.join(__dirname, '../../.non-digipos/document/' + id + "/bar-sign/" + FILE_NAME);

      var FILE_FOUND = false;
      var MESSAGE: any; MESSAGE = "File not found !";

      try {
        if (fs.existsSync(FILE_BAR)) {
          FILE_FOUND = true;
          MESSAGE = FILE_BAR;
        }
      } catch (err) {
        FILE_FOUND = false;
        MESSAGE = err;
      }

      if (FILE_FOUND == true) {
        fs.readFile(FILE_BAR, function (err, data) {
          if (err) {
            result.success = false;
            result.message = err;
            result.data = null;
            resolve(result);
          } else {
            result.success = true;
            result.data = data.toString("base64");
            result.message = "success";
            resolve(result);
          }
        });
      } else {
        result.success = false;
        result.data = null;
        result.message = MESSAGE;
        resolve(result);
      }
    });
  }

  @post('/document-view/file/invoice/{id}', {
    responses: {
      '200': {
        description: 'Document View invoice model instance',
        content: {
          'application/json': {
            schema: DocumentViewByNameBody
          }
        },
      },
    },
  })
  async FileViewInvoice(
    @param.path.string('id') id: string,
    @requestBody(DocumentViewByNameBody) fileViewDoc: {names: string},
  ): Promise<Object> {
    return await new Promise<object>((resolve, reject) => {
      var USER_ID = this.currentUserProfile[securityId];
      var result: any; result = {success: true, message: '', data: []};

      var FILE_NAME = fileViewDoc.names.split("/").join("_");
      const FILE_INVOICE = path.join(__dirname, '../../.non-digipos/document/' + id + "/invoice/" + FILE_NAME);

      var FILE_FOUND = false;
      var MESSAGE: any; MESSAGE = "File not found !";

      try {
        if (fs.existsSync(FILE_INVOICE)) {
          FILE_FOUND = true;
          MESSAGE = FILE_INVOICE;
        }
      } catch (err) {
        FILE_FOUND = false;
        MESSAGE = err;
      }

      if (FILE_FOUND == true) {
        fs.readFile(FILE_INVOICE, function (err, data) {
          if (err) {
            result.success = false;
            result.message = err;
            result.data = null;
            resolve(result);
          } else {
            result.success = true;
            result.data = data.toString("base64");
            result.message = "success";
            resolve(result);
          }
        });
      } else {
        result.success = false;
        result.data = null;
        result.message = MESSAGE;
        resolve(result);
      }
    });
  }

  @post('/document-view/file/invoice/sign/{id}', {
    responses: {
      '200': {
        description: 'Document View BAR SIGN model instance',
        content: {
          'application/json': {
            schema: DocumentViewByNameBody
          }
        },
      },
    },
  })
  async FileViewInvoiceSign(
    @param.path.string('id') id: string,
    @requestBody(DocumentViewByNameBody) fileViewDoc: {names: string},
  ): Promise<Object> {
    return await new Promise<object>((resolve, reject) => {
      var USER_ID = this.currentUserProfile[securityId];
      var result: any; result = {success: true, message: '', data: []};

      var FILE_NAME = fileViewDoc.names.split("/").join("_");
      const FILE_INVOICE_SIGN = path.join(__dirname, '../../.non-digipos/document/' + id + "/invoice-sign/" + FILE_NAME);

      var FILE_FOUND = false;
      var MESSAGE: any; MESSAGE = "File not found !";

      try {
        if (fs.existsSync(FILE_INVOICE_SIGN)) {
          FILE_FOUND = true;
          MESSAGE = FILE_INVOICE_SIGN;
        }
      } catch (err) {
        FILE_FOUND = false;
        MESSAGE = err;
      }

      if (FILE_FOUND == true) {
        fs.readFile(FILE_INVOICE_SIGN, function (err, data) {
          if (err) {
            result.success = false;
            result.message = err;
            result.data = null;
            resolve(result);
          } else {
            result.success = true;
            result.data = data.toString("base64");
            result.message = "success";
            resolve(result);
          }
        });
      } else {
        result.success = false;
        result.data = null;
        result.message = MESSAGE;
        resolve(result);
      }
    });
  }


  @post('/document-view/file/faktur-pajak/{id}', {
    responses: {
      '200': {
        description: 'Document View BAR SIGN model instance',
        content: {
          'application/json': {
            schema: DocumentViewByNameBody
          }
        },
      },
    },
  })
  async FileViewFakturPajak(
    @param.path.string('id') id: string,
    @requestBody(DocumentViewByNameBody) fileViewDoc: {names: string},
  ): Promise<Object> {
    return await new Promise<object>((resolve, reject) => {
      var USER_ID = this.currentUserProfile[securityId];
      var result: any; result = {success: true, message: '', data: []};

      var FILE_NAME = fileViewDoc.names.split("/").join("_");
      const FILE_INVOICE_SIGN = path.join(__dirname, '../../.non-digipos/document/' + id + "/faktur-pajak/" + FILE_NAME);

      var FILE_FOUND = false;
      var MESSAGE: any; MESSAGE = "File not found !";

      try {
        if (fs.existsSync(FILE_INVOICE_SIGN)) {
          FILE_FOUND = true;
          MESSAGE = FILE_INVOICE_SIGN;
        }
      } catch (err) {
        FILE_FOUND = false;
        MESSAGE = err;
      }

      if (FILE_FOUND == true) {
        fs.readFile(FILE_INVOICE_SIGN, function (err, data) {
          if (err) {
            result.success = false;
            result.message = err;
            result.data = null;
            resolve(result);
          } else {
            result.success = true;
            result.data = data.toString("base64");
            result.message = "success";
            resolve(result);
          }
        });
      } else {
        result.success = false;
        result.data = null;
        result.message = MESSAGE;
        resolve(result);
      }
    });
  }

  @post('/document-view/file/bukti-potong/{id}', {
    responses: {
      '200': {
        description: 'Document View Bukti Potong model instance',
        content: {
          'application/json': {
            schema: DocumentViewByNameBody
          }
        },
      },
    },
  })
  async FileViewBuktiPotong(
    @param.path.string('id') id: string,
    @requestBody(DocumentViewByNameBody) fileViewDoc: {names: string},
  ): Promise<Object> {
    return await new Promise<object>((resolve, reject) => {
      var USER_ID = this.currentUserProfile[securityId];
      var result: any; result = {success: true, message: '', data: []};

      var FILE_NAME = fileViewDoc.names.split("/").join("_");
      const FILE_INVOICE_SIGN = path.join(__dirname, '../../.non-digipos/document/' + id + "/bukti-potong/" + FILE_NAME);

      var FILE_FOUND = false;
      var MESSAGE: any; MESSAGE = "File not found !";

      try {
        if (fs.existsSync(FILE_INVOICE_SIGN)) {
          FILE_FOUND = true;
          MESSAGE = FILE_INVOICE_SIGN;
        }
      } catch (err) {
        FILE_FOUND = false;
        MESSAGE = err;
      }

      if (FILE_FOUND == true) {
        fs.readFile(FILE_INVOICE_SIGN, function (err, data) {
          if (err) {
            result.success = false;
            result.message = err;
            result.data = null;
            resolve(result);
          } else {
            result.success = true;
            result.data = data.toString("base64");
            result.message = "success";
            resolve(result);
          }
        });
      } else {
        result.success = false;
        result.data = null;
        result.message = MESSAGE;
        resolve(result);
      }
    });
  }

  @post('/document-view/file/others/{id}', {
    responses: {
      '200': {
        description: 'Document View others model instance',
        content: {
          'application/json': {
            schema: DocumentViewByNameBody
          }
        },
      },
    },
  })
  async FileViewOthers(
    @param.path.string('id') id: string,
    @requestBody(DocumentViewByNameBody) fileViewDoc: {names: string},
  ): Promise<Object> {
    return await new Promise<object>((resolve, reject) => {
      var USER_ID = this.currentUserProfile[securityId];
      var result: any; result = {success: true, message: '', data: []};

      var FILE_NAME = fileViewDoc.names.split("/").join("_");
      const FILE_INVOICE_SIGN = path.join(__dirname, '../../.non-digipos/document/' + id + "/others/" + FILE_NAME);

      var FILE_FOUND = false;
      var MESSAGE: any; MESSAGE = "File not found !";

      try {
        if (fs.existsSync(FILE_INVOICE_SIGN)) {
          FILE_FOUND = true;
          MESSAGE = FILE_INVOICE_SIGN;
        }
      } catch (err) {
        FILE_FOUND = false;
        MESSAGE = err;
      }

      if (FILE_FOUND == true) {
        fs.readFile(FILE_INVOICE_SIGN, function (err, data) {
          if (err) {
            result.success = false;
            result.message = err;
            result.data = null;
            resolve(result);
          } else {
            result.success = true;
            result.data = data.toString("base64");
            result.message = "success";
            resolve(result);
          }
        });
      } else {
        result.success = false;
        result.data = null;
        result.message = MESSAGE;
        resolve(result);
      }
    });
  }


  @post('/document-view/images/attention-sign', {
    responses: {
      '200': {
        description: 'VIEW SIGN model instance',
        content: {
          'application/json': {
            schema: DocumentViewByNameBody
          }
        },
      },
    },
  })
  async FileViewAttentionSign(
    @requestBody(DocumentViewByNameBody) fileViewDoc: {names: string},
  ): Promise<Object> {
    return new Promise<object>((resolve, reject) => {
      var result: any; result = {success: false, data: "", message: ""};
      var fileNames = fileViewDoc.names.split("/").join("_");
      const filePath = path.join(__dirname, '../../.non-digipos/attention/' + fileNames);
      fs.readFile(filePath, function (err, data) {
        if (err) {
          result.success = false;
          result.message = err;
          resolve(result);
        } else {
          result.success = true;
          result.data = data.toString("base64");
          result.message = "success";
          resolve(result);
        }
      });
    });
  }

}
