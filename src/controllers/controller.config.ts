
import {inject} from '@loopback/core';
import {repository} from '@loopback/repository';
import {SecurityBindings, UserProfile} from '@loopback/security';
import sgMail from '@sendgrid/mail';
import async from 'async';
import fs from 'fs';
import {google} from 'googleapis';
import fetch from 'node-fetch';
import path from 'path';
import readline from 'readline';
import {AdMasterRepository} from '../repositories';
var jwt = require('jsonwebtoken');

var units = ['', 'ribu', 'juta', 'milyar', 'triliun', 'quadriliun', 'quintiliun', 'sextiliun', 'septiliun', 'oktiliun', 'noniliun', 'desiliun', 'undesiliun', 'duodesiliun', 'tredesiliun', 'quattuordesiliun', 'quindesiliun', 'sexdesiliun', 'septendesiliun', 'oktodesiliun', 'novemdesiliun', 'vigintiliun'];
var numbers = ['satu', 'dua', 'tiga', 'empat', 'lima', 'enam', 'tujuh', 'delapan', 'sembilan'];

const SCOPES = ['https://www.googleapis.com/auth/drive.metadata.readonly', 'https://www.googleapis.com/auth/drive'];
const TOKEN_PATH = path.join(__dirname, '../../.config/token.json');
const CREDENTIALS = path.join(__dirname, '../../.config/credentials_desktop.json');
const GDCONFIG = path.join(__dirname, '../../.config/directory.json');
const PATH_FIREBASE = path.join(__dirname, '../../.config/firebase.app.json');
var serviceAccount = require(PATH_FIREBASE);

// const NotificationURL = "ws://localhost:3002/notification";
var NotificationSockect: any;


var SENDGRID_APY_KEY = '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';
var FromMail = "digipos@linkaja-ba.tools";
var TemplateMailStore = {
  DefaultMail: "d-9675ef6c07ff4aa9846e3fc5036ae155",
  DistributDocMail: "d-8cda1354481549a0991ca9b3a05b5fcc",
  FPJPMail: "d-a2c0c333f23f476bbe0fce240d5f7a4c",
  ForgetPass: "d-fe33d37c0e2e434e988e50a642326909"
}

export class ControllerConfig {


  // static onAngkaTerbilang(TotalAmout: any): any {
  //   throw new Error('Method not implemented.');
  // }

  constructor(
    @repository(AdMasterRepository)
    public adMasterRepository: AdMasterRepository,
    @inject(SecurityBindings.USER)
    public currentUserProfile: UserProfile,
    // @repository(DataDigiposRepository)
    // public dataDigiposRepository: DataDigiposRepository,
    // @repository(DataFpjpRepository)
    // public dataFpjpRepository: DataFpjpRepository,
  ) {

  }

  static onStyleBAR() {
    var HTML = "<style>";
    HTML += ".page-break{page-break-before: always;}";
    HTML += ".content-bar{font-size:8px;}";
    HTML += ".draft-icon {z-index:10;font-size: 80px;color: #ddd;position: fixed;width:100%;height:100%;text-align:center;    text-align: center;top: 0px;}";
    HTML += ".draft-icon p {display: inline-block;line-height: normal;vertical-align: middle;-webkit-transform: rotate(30deg);-moz-transform: rotate(30deg);-o-transform: rotate(30deg); writing-mode: lr-tb;margin-top: 300px;}";
    HTML += ".content {z-index:100;text-align: justify;padding:10px;font-size:8px;font-family: Calibri;margin-left:50px;margin-right:50px;}";
    HTML += ".header_doc{text-align:center;font-size:12px;font-family: Calibri;font-weight: bold;margin-bottom:20px;padding:20px; border-bottom:solid 3px #000;}";
    HTML += ".table-pic{width:300px;}";
    HTML += "table.table-trx > thead > tr > td {padding:3px;border-bottom:solid 1px #000;border-right:solid 1px #000;font-weight: bold;text-align:center;}";
    HTML += "table.table-trx > tbody > tr > td {padding:3px;border-bottom:solid 1px #000;border-right:solid 1px #000;}";
    HTML += "table.table-trx {border-left:solid 1px #000;border-top:solid 1px #000; border-spacing: 0px;}";
    HTML += ".text-number{text-align:right;}";
    HTML += ".text-string{text-align:center;}";
    HTML += "</style>";
    return HTML;
  }

  static onStyleINVOICE() {
    var HTML = "<style>";
    HTML += ".page-break{page-break-before: always;}";
    HTML += ".content-inv{font-size:10px;}";
    HTML += ".draft-icon {z-index:10;font-size: 80px;color: #ddd;position: fixed;width:100%;height:100%;text-align:center;    text-align: center;top: 0px;}";
    HTML += ".draft-icon p {display: inline-block;line-height: normal;vertical-align: middle;-webkit-transform: rotate(30deg);-moz-transform: rotate(30deg);-o-transform: rotate(30deg); writing-mode: lr-tb;margin-top: 300px;}";
    HTML += ".content {z-index:100;text-align: justify;padding:10px;font-size:10px;font-family: Calibri;margin-left:50px;margin-right:50px;}";
    HTML += ".header_doc{text-align:left;font-size:10px;font-family: Calibri;margin-bottom:20px;margin-top:20px;}";
    HTML += ".table-pic{width:300px;}";
    HTML += "table.table-trx > thead > tr > td {padding:3px;border-bottom:solid 1px #000;border-right:solid 1px #000;font-weight: bold;text-align:center;}";
    HTML += "table.table-trx > tbody > tr > td {padding:3px;border-bottom:solid 1px #000;border-right:solid 1px #000;}";
    HTML += "table.table-trx {border-left:solid 1px #000;border-top:solid 1px #000; border-spacing: 0px;}";
    HTML += ".text-number{text-align:right;}";
    HTML += ".text-string{text-align:center;}";
    HTML += ".header-invoice{margin-left:96px;margin-right:95.04px;}";
    HTML += ".header-invoice > img{height:80px;}";
    HTML += ".footer-invoice{font-size:8px !important; text-align:center;margin-left:96px;margin-right:95.04px;margin-bottom:80px;}";
    HTML += ".footer-invoice > img{width:800px;float:left;margin-top:-60px;height:130px;}";
    HTML += ".footer-invoice .footer-pt{width:100%; padding:3px;color:#FFFFFF;font-weight:bold;text-align:center;}";
    HTML += ".footer-invoice .footer-address{width:600px;padding:3px;color:#FFFFFF;text-align:center;margin-left:auto;margin-right:auto;line-height:15px;}";
    HTML += ".address-inv{vertical-align:top;}";
    HTML += "table > tr > td {font-size:10px !important;font-family: Calibri;}";
    HTML += ".tbl-trx-inv{font-size:10px;border-left:solid 1px #000;border-top:solid 1px #000; border-spacing: 0px;}";
    HTML += ".tbl-trx-inv tr td {padding:10px;border-bottom:solid 1px #000;border-right:solid 1px #000;}";
    HTML += ".tbl-trx-inv-header{background-color: #deeaf6;font-weight:bold;text-align:center;}";
    HTML += ".tbl-trx-inv-footer{background-color: #deeaf6;font-weight:bold;}";


    HTML += "</style>";
    return HTML;
  }

  static RANDOME_ID(length: number) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNPQRSTUVWXYZ123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() *
        charactersLength));
    }
    return result;
  }


  public async onRunningCron(type: string) {
    // console.log("Crone is running... !");
    // var periodeProcess = ControllerConfig.onPeriodeDigipos();
    // await this.
    // await this.adMasterRepository.
    //   console.log(masterAD);

    var ad = await this.adMasterRepository.find();
    console.log(ad);
  }

  static onSetNotificationSocket(Socket: any) {
    NotificationSockect = Socket;
  }

  static onHelloAll() {
    NotificationSockect.emit("notification_app", "Hello All");
  }

  static onSendNotif(Message: any) {
    NotificationSockect.emit("notification_app", Message);
  }

  static onGetRoleNotifGroup(Code: string) {
    var roleSet = {label: "", role: ""}
    switch (Code) {
      case "0101010101":
        roleSet.label = "Data Inquiry Review Summary AD Sudah Tervalidasi BA.";
        roleSet.role = "1,4,5";
        break;
      case "0101010102":
        roleSet.label = "Data Inquiry Review Summary AD Sudah Tervalidasi BU.";
        roleSet.role = "1,4,5";
        break;
      case "01010101":
        roleSet.label = "Data Inquiry Review Summary AD A Tervalidasi BA.";
        roleSet.role = "1,4,5";
        break;
      case "01010102":
        roleSet.label = "Data Inquiry Review Summary AD B Tervalidasi BA.";
        roleSet.role = "1,4,5";
        break;
      case "01010201":
        roleSet.label = "Data Inquiry Review Summary  AD A Di Tolak BA.";
        roleSet.role = "1,4,5,8";
        break;
      case "01010202":
        roleSet.label = "Data Inquiry Review Summary AD B Di Tolak BA.";
        roleSet.role = "1,4,5,8";
        break;
      case "02020101":
        roleSet.label = "Data Inquiry Review Summary AD A Tervalidasi BU.";
        roleSet.role = "1,4,5";
        break;
      case "02020102":
        roleSet.label = "Data Inquiry Review Summary AD B Tervalidasi BU.";
        roleSet.role = "1,4,5";
        break;
      case "02020201":
        roleSet.label = "Data Inquiry Review Summary  AD A Di Tolak BU.";
        roleSet.role = "1,4,5,8";
        break;
      case "02020202":
        roleSet.label = "Data Inquiry Review Summary AD B Di Tolak BU.";
        roleSet.role = "1,4,5,8";
        break;
      case "03010101":
        roleSet.label = "Document BAR Recon Not Sign Telah Di Buat.";
        roleSet.role = "1,4,5";
        break;
      case "03010102":
        roleSet.label = "Document BAR Recon Sign Telah Di Buat.";
        roleSet.role = "1,4,5";
        break;
      case "04010101":
        roleSet.label = "Document BAR Recon Telah Siap Di Sign.";
        roleSet.role = "1,4,5";
        break;
      case "04010103":
        roleSet.label = "Document BAR Recon Telah Di Tolak Untuk Di Sign.";
        roleSet.role = "1,4,5";
        break;
      case "0501010101":
        roleSet.label = "Document BAR Recon Telah Di Distribusi.";
        roleSet.role = "1,4,5";
        break;
      case "0601010101":
        roleSet.label = "Data Dispute Telah Di Validasi.";
        roleSet.role = "1,4,5";
        break;
      case "0601010102":
        roleSet.label = "Data Dispute Telah Di Tolak.";
        roleSet.role = "1,4,5";
        break;
      case "0701010101":
        roleSet.label = "Data FPJP Invoice Telah Di Validasi Oleh BA.";
        roleSet.role = "1,4,5";
        break;
      case "0701010102":
        roleSet.label = "Data FPJP Invoice Telah Di Tolak Oleh  BA.";
        roleSet.role = "1,4,5";
        break;
      case "0701010201":
        roleSet.label = "Data FPJP Invoice Telah Di Validasi Oleh  BU.";
        roleSet.role = "1,4,5";
        break;
      case "0701010202":
        roleSet.label = "Data FPJP Invoice Telah Di Tolak Oleh  BU.";
        roleSet.role = "1,4,5";
        break;
      case "0702010201":
        roleSet.label = "Data FPJP Nota Debit Telah Di Validasi Oleh  BU.";
        roleSet.role = "1,4,5";
        break;
      case "0702010202":
        roleSet.label = "Data FPJP Nota Debit Telah Di Tolak Oleh  BU.";
        roleSet.role = "1,4,5";
        break;
      case "0801010101":
        roleSet.label = "Data FPJP Invoice Telah Di Submit.";
        roleSet.role = "1,4,5";
        break;
      case "801010102":
        roleSet.label = "Data FPJP Nota Debit Telah Di Submit.";
        roleSet.role = "1,4,5";
        break;
      case "0801010103":
        roleSet.label = "Data FPJP Invoice Telah Di Setujui.";
        roleSet.role = "1,4,5";
        break;
      case "0801010104":
        roleSet.label = "Data FPJP Nota Debit Telah Di Setujui.";
        roleSet.role = "1,4,5";
        break;
      case "0801010105":
        roleSet.label = "Data FPJP Invoice Tidak Di Setujui.";
        roleSet.role = "1,4,5";
        break;
      case "0801010106":
        roleSet.label = "Data FPJP Nota Debit Tidak Di Setujui.";
        roleSet.role = "1,4,5";
        break;
      case "0902010202":
        roleSet.label = "Distribusi Invoice Setelah Sign Telah Berhasil.";
        roleSet.role = "1,4,5";
        break;
      case "0902010201":
        roleSet.label = "Distribusi Bukti Potong Telah Berhasil.";
        roleSet.role = "1,4,5";
        break;
      case "0902010203":
        roleSet.label = "Distribusi Faktur Pajak Telah Berhasil.";
        roleSet.role = "1,4,5";
        break;
      case "1002010201":
        roleSet.label = "Document FPJP Siap Dibayar.";
        roleSet.role = "1,4,5";
        break;
      case "11010101":
        roleSet.label = "Document Invoice di validasi.";
        roleSet.role = "1,4,5";
        break;
      case "11010102":
        roleSet.label = "Document Invoice  berhasil di generate.";
        roleSet.role = "1,4,5";
        break;
      case "11010103":
        roleSet.label = "Document Invoice  berhasil di distibusikan.";
        roleSet.role = "1,4,5";
        break;
      case "12010101":
        roleSet.label = "New Note Document FPJP.";
        roleSet.role = "1,4,5";
        break;
      case "13010101":
        roleSet.label = "Document Invoice berhasil di validasi.";
        roleSet.role = "1,4,5";
        break;
      case "13010102":
        roleSet.label = "Document Invoice telah di tolak.";
        roleSet.role = "1,4,5";
        break;
      case "14010101":
        roleSet.label = "Update Informasi Master AD.";
        roleSet.role = "1,2,4,5,6,7,8";
        break;
      case "14010102":
        roleSet.label = "Update Informasi Attention BAR Recon.";
        roleSet.role = "1,2,4,5,6,7,8";
        break;
      case "14010103":
        roleSet.label = "Update Informasi Attention Paid Invoice.";
        roleSet.role = "1,2,4,5,6,7,8";
        break;
      case "16010101":
        roleSet.label = "Distribusi Document BAR Recon Success.";
        roleSet.role = "1,2,4,5,6,7,8";
        break;
      case "16010102":
        roleSet.label = "Distribusi Document Invoice Success.";
        roleSet.role = "1,2,4,5,6,7,8";
        break;
      case "16010103":
        roleSet.label = "Generate Document Invoice Success.";
        roleSet.role = "1,2,4,5,6,7,8";
        break;
      case "16010104":
        roleSet.label = "Generate Document BAR Recon Success.";
        roleSet.role = "1,2,4,5,6,7,8";
        break;
      case "16010105":
        roleSet.label = "Distribute Document Faktur Pajak Success.";
        roleSet.role = "1,2,4,5,6,7,8";
        break;
      case "16010106":
        roleSet.label = "Distribute Document Bukti Potong Success.";
        roleSet.role = "1,2,4,5,6,7,8";
        break;
      case "16010107":
        roleSet.label = "Distribute Document Invoice After Sign Success.";
        roleSet.role = "1,2,4,5,6,7,8";
        break;
      case "16010108":
        roleSet.label = "Generate Sign Document BAR Recon  Success.";
        roleSet.role = "1,2,4,5,6,7,8";
        break;
      default:
        break;
    }
    return roleSet;
  }


  static onGetDayString = (posisi: number) => {
    var DayString = ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"];
    return DayString[posisi];
  }

  static onGetMonth = (posisi: number) => {
    var MonthString = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
    return MonthString[posisi];
  }

  static onGetNowDay = () => {
    var d = new Date();
    return d.getDay();
  }

  static onGetNowMonth = () => {
    var d = new Date();
    return d.getMonth();
  }

  static onGetNowYears = () => {
    var d = new Date();
    return d.getFullYear();
  }

  static onBANowDate = () => {
    var d = new Date();
    return ControllerConfig.onGetDayString(d.getDay()) + ", " + d.getDate() + " " + ControllerConfig.onGetMonth(d.getMonth()) + " " + d.getFullYear();
  }

  static onPeriodeDigipos = () => {
    var today = new Date();
    var todays = today.setMonth(today.getMonth() - 1);
    var DateNew = new Date(todays);
    var Periode = DateNew.getFullYear() + '-' + ('0' + (DateNew.getMonth() + 1)).slice(-2);
    return Periode;
  }

  static digitToUnit = (digit: any) => {
    const curIndex = Math.floor(digit / 3)
    const maxIndex = units.length - 1
    return curIndex > maxIndex ? units[maxIndex] : units[curIndex]
  }


  static numberToText = (index: any) => {
    return numbers[index - 1] || ''
  }

  static terbilang = (angka: any) => {
    let result = ''
    let printUnit = true
    let isBelasan = false

    for (let i = 0; i < angka.length; i++) {
      const length = angka.length - 1 - i
      if (length % 3 == 0) {
        const num = (angka[i] == 1 && (isBelasan || (ControllerConfig.digitToUnit(length) == 'ribu' && ((angka[i - 2] == undefined || angka[i - 2] == 0) && (angka[i - 1] == undefined || angka[i - 1] == 0))))) ? 'se' : `${ControllerConfig.numberToText(angka[i])} `
        result += ` ${num}`

        if ((angka[i - 2] && angka[i - 2] != 0) || (angka[i - 1] && angka[i - 1] != 0) || angka[i] != 0) {
          printUnit = true
        }
        if (printUnit) {
          printUnit = false
          result += ((isBelasan) ? 'belas ' : '') + ControllerConfig.digitToUnit(length)
          if (isBelasan) {
            isBelasan = false
          }
        }
      } else if (length % 3 == 2 && angka[i] != 0) {
        result += ` ${(angka[i] == 1) ? 'se' : ControllerConfig.numberToText(angka[i]) + ' '}ratus`
      } else if (length % 3 == 1 && angka[i] != 0) {
        if (angka[i] == 1) {
          if (angka[i + 1] == 0) {
            result += ' sepuluh'
          } else {
            isBelasan = true
          }
        } else {
          result += ` ${ControllerConfig.numberToText(angka[i])} puluh`
        }
      }
    }

    return result.trim().replace(/\s+/g, ' ')
  }

  static terbilangSatuSatu = (angka: string) => {
    return angka
      .split('')
      .map(angka => angka == "0" ? 'nol' : ControllerConfig.numberToText(angka))
      .join(' ')
  }

  static onAngkaTerbilang = (target: any, settings = {decimal: '.'}) => {

    if (typeof target !== "string") target = target.toString()
    target = target.split(settings.decimal)

    return target[1]
      ? `${ControllerConfig.terbilang(target[0])} koma ${ControllerConfig.terbilangSatuSatu(target[1])}`
      : ControllerConfig.terbilang(target[0])
  }

  static onGeneratePuluhan = (angka: number) => {
    var nominal = angka.toString().split("-");
    if (nominal.length > 2) {
      return "";
    } else {
      var number_string = "";
      var minus = "";
      if (nominal.length == 2) {
        number_string = nominal[1];
        minus = "-"
      } else {
        number_string = nominal[0];
      }

      var split = number_string.toString().split('.'),
        sisa = split[0].length % 3,
        rupiah = split[0].substr(0, sisa),
        ribuan = split[0].substr(sisa).match(/\d{3}/gi);

      if (ribuan) {
        var separator = sisa ? ',' : '';
        rupiah += separator + ribuan.join(',');
      }
      rupiah = split[1] != undefined ? minus + rupiah + '.' + split[1] : minus + rupiah;
      return rupiah
    }
  }

  static onSendMailFPJP = (to: string, content: {
    name_approval: string,
    fpjp_number: string,
    fpjp_submit: string,
    link_approval: string,
    fpjp_type: string,
    fpjp_name: string,
    currency: string,
    amount: string,
    link_list_approval: string,
    remarks: string,
    token_set: string
  }, callback: any) => {

    sgMail.setApiKey(SENDGRID_APY_KEY);
    const message = {
      from: FromMail,
      subject: 'New FPJP Document Need Approval',
      templateId: TemplateMailStore.FPJPMail,
      dynamic_template_data: content,
      personalizations: [
        {
          to: [{"email": to}]
        }
      ],
      substitutions: content,
    };
    sgMail.send(message)
      .then(() => {
        callback({success: true, message: "Mail Sent"});
      })
      .catch((error) => {
        callback({success: false, message: error});
      })
    callback({success: true, message: "Mail Sent"});
  }


  static onSendMailForgotPass = (to: string, content: {
    username: string,
    token: string,
    link_forgot: string
  }, callback: any) => {

    // sgMail.setApiKey(SENDGRID_APY_KEY);

    // const message = {
    //   from: FromMail,
    //   subject: 'Forgot Password ?',
    //   templateId: TemplateMailStore.ForgetPass,
    //   dynamic_template_data: content,
    //   personalizations: [
    //     {
    //       to: [{"email": to}]
    //     }
    //   ],
    //   substitutions: content,
    // };
    // sgMail.send(message)
    //   .then(() => {
    //     console.log({success: true, message: "Mail Sent Forgot Pass !"});
    //     callback({success: true, message: "Mail Sent"});
    //   })
    //   .catch((error) => {
    //     console.log({success: false, message: error, label: "Mail Sent Forgot Pass !"});
    //     callback({success: false, message: error});
    //   })
    callback({success: true, message: "Mail Sent"});
  }

  static onSendMailDistributeDoc = (to: string, content: {
    type_doc: string,
    ad_name: string,
    name_doc: string,
    token_folder: string
  }, callback: any) => {

    // sgMail.setApiKey(SENDGRID_APY_KEY);

    // const message = {
    //   from: FromMail,
    //   subject: 'New Distribute Document',
    //   templateId: TemplateMailStore.DistributDocMail,
    //   dynamic_template_data: content,
    //   personalizations: [
    //     {
    //       to: [{"email": to}]
    //     }
    //   ],
    //   substitutions: content,
    // };

    // sgMail.send(message)
    //   .then(() => {
    //     console.log({success: true, message: "Mail Sent Distribute !"});
    //     callback({success: true, message: "Mail Sent"});
    //   })
    //   .catch((error) => {
    //     console.log({success: false, message: error, label: "Mail Sent Distribute !"});
    //     callback({success: false, message: error});
    //   })
    callback({success: true, message: "Mail Sent"});
  }

  static onSendMailDefault = (to: string, content: any, callback: any) => {

    // sgMail.setApiKey(SENDGRID_APY_KEY);

    // const message = {
    //   from: FromMail,
    //   subject: 'Forgot Password ?',
    //   templateId: TemplateMailStore.DefaultMail,
    //   personalizations: [
    //     {
    //       to: [{"email": to}]
    //     }
    //   ],
    //   substitutions: content,
    // };
    // sgMail.send(message)
    //   .then(() => {
    //     callback({success: true, message: "Mail Sent"});
    //   })
    //   .catch((error) => {
    //     callback({success: false, message: error});
    //   })
    callback({success: true, message: "Mail Sent"});
  }




  static onAPISendMail = (to: any, subject: string, content: any, callback: any) => {
    var SENDGRID_API_KEY: string; SENDGRID_API_KEY = process.env.SENDGRID_API_KEY == undefined ? "0" : process.env.SENDGRID_API_KEY;
    var bodyData = {
      "personalizations": [
        {
          "to": to,
          // [
          //   {
          //     "email": "yudhistira.job@gmail.com"
          //   }
          // ],
          "subject": subject
        }
      ],
      "from": {
        "email": "gugumnana@gmail.com"
      },
      "content": content
      // [
      //   {
      //     "type": "text/plain",
      //     "value": "UJICOBA BRO"
      //   }
      // ]
    }
    fetch('https://api.sendgrid.com/v3/mail/send', {
      method: 'post',
      body: JSON.stringify(bodyData),
      headers: {'Content-Type': 'application/json', 'Authorization': 'Bearer ' + SENDGRID_API_KEY},
    })
      .then(res => {
        console.log(res);
        callback(res);
        // console.log(res.status);
        // console.log(res.statusText);
        // console.log(res.headers.raw());
        // console.log(res.headers.get('content-type'));
      });
  }

  static onAPIGoogleDrive = (callback: any) => {
    fs.readFile(CREDENTIALS, (err, content: any) => {
      var returnStatus: any; returnStatus = {success: true, message: ""};
      if (err) {
        returnStatus.success = false;
        returnStatus.message = err;
        callback(returnStatus);
      } else {
        ControllerConfig.authorize(JSON.parse(content), function (auth: any) {
          const drive = google.drive({version: 'v3', auth});
          returnStatus.success = true;
          returnStatus.drive = drive;
          returnStatus.auth = auth;
          callback(returnStatus);
        });
      }
    });

    // fs.readFile(CREDENTIALS, (err, content: any) => {
    //   if (err) return console.log('Error loading client secret file:', err);
    //   // Authorize a client with credentials, then call the Google Drive API.
    //   // authorize(JSON.parse(content), addPermission);
    //   if (Data.InfoAD == undefined) {
    //   } else {
    //     if (Data.InfoAD.email == undefined || Data.InfoAD.email == "" || Data.InfoAD.email == "-" || Data.InfoAD.email == null) {

    //     } else {
    //       var PermissionGD = [
    //         {
    //           'type': 'user',
    //           'role': 'writer',
    //           'emailAddress': Data.InfoAD.email
    //         }
    //       ]


    //       fs.readFile(GDCONFIG, async (errConfig, contentConfig: any) => {
    //         if (errConfig) return console.log('Error loading root folder:', errConfig);
    //         // console.log(content);
    //         var configGD = JSON.parse(contentConfig);
    //         var RootID = configGD.root_directory;
    //         var FolderAD = Data.InfoAD.ad_code + "-" + Data.InfoAD.ad_name.split(" ").join("_");


    //         ControllerConfig.authorize(JSON.parse(content), function (auth: any) {
    //           ControllerConfig.onCheckFolderFound(auth, FolderAD, function (rAD: any) {
    //             if (rAD.success == true) {

    //             } else {
    //               ControllerConfig.onCreateDirectoryGD(auth, FolderAD, RootID, function (rRootFolder: any) {
    //                 if (rRootFolder.success == true) {
    //                   var rRootFolder_ID = rRootFolder.message.data.id;
    //                   console.log(rRootFolder);
    //                   ControllerConfig.onChangePermissionGD(auth, rRootFolder_ID, PermissionGD, function (rAddPermission: any) {
    //                     console.log(rAddPermission);
    //                     if (rAddPermission.success == true) {
    //                       ControllerConfig.onCreateDirectoryGD(auth, "DOWNLOAD", RootID, function (rRootFolder: any) {

    //                       });
    //                     } else {
    //                       ControllerConfig.onDeleteDirectoryGD(auth, rRootFolder_ID, function (rDelete: any) {

    //                       });
    //                     }
    //                   });
    //                 } else {
    //                 }
    //               });
    //             }
    //           });
    //         });

    //       });
    //       // var PermissionGD = [
    //       //   {
    //       //     'type': 'user',
    //       //     'role': 'commenter',
    //       //     'emailAddress': Data.InfoAD.email
    //       //   }
    //       // ]
    //       // ControllerConfig.authorize(JSON.parse(content), function (auth: any) {
    //       //   var FolderAD = Data.InfoAD.ad_code + "-" + Data.InfoAD.ad_name.split(" ").join("_");

    //       // ControllerConfig.onCheckFolderFound(auth, "DIRECTORY_DIGIPOS", function (r: any) {
    //       //   if (r.success) {

    //       //   } else {
    //       //     ControllerConfig.onCreateDirectoryGD(auth, "DIRECTORY_DIGIPOS", "", function (rRootFolder: any) {
    //       //       var RootID = rRootFolder.message.data.id;
    //       //       if (rRootFolder.success) {
    //       //         ControllerConfig.onCreateDirectoryGD(auth, "DIRECTORY_DIGIPOS", "", function (rRootFolder: any) {

    //       //         });
    //       //       }
    //       //     });
    //       //   }
    //       // console.log(r);
    //       // if (r.success) {
    //       //   var RootID = r.message.id;
    //       //   ControllerConfig.onCheckFolderFound(auth, FolderAD, function (rAd: any) {
    //       //     if (rAd.success) {

    //       //     } else {
    //       //       ControllerConfig.onCreateDirectoryGD(auth, FolderAD, RootID, function (rAdCreate: any) {
    //       //         if (rAdCreate.success) {
    //       //           var FolderAD_ID = rAdCreate.message.data.id;
    //       //           ControllerConfig.onChangePermissionGD(auth, FolderAD_ID, PermissionGD, function (rPermission: any) {
    //       //             if (rPermission.success) {

    //       //               ControllerConfig.onCreateDirectoryGD(auth, "DOWNLOAD", FolderAD_ID, function (rDownload: any) {
    //       //                 if (rDownload.success) {
    //       //                   var FolderDownload_ID = rDownload.message.data.id;
    //       //                   ControllerConfig.onCreateDirectoryGD(auth, "BERITA_ACARA", FolderDownload_ID, function (rBA: any) {

    //       //                   });
    //       //                   ControllerConfig.onCreateDirectoryGD(auth, "SUMMARY_BA_RECON", FolderDownload_ID, function (rSBR: any) {

    //       //                   });
    //       //                   ControllerConfig.onCreateDirectoryGD(auth, "PAID_INVOICE", FolderDownload_ID, function (rPermission: any) {

    //       //                   });
    //       //                   ControllerConfig.onCreateDirectoryGD(auth, "FAKTUR_PAJAK (PPN)", FolderDownload_ID, function (rFP: any) {

    //       //                   });
    //       //                   ControllerConfig.onCreateDirectoryGD(auth, "BUKTI_POTONG (PPH)", FolderDownload_ID, function (rBP: any) {

    //       //                   });
    //       //                 }
    //       //               });

    //       //               ControllerConfig.onCreateDirectoryGD(auth, "UPLOAD", FolderAD, function (rUpload: any) {
    //       //                 if (rUpload.success) {
    //       //                   var FolderUpload_ID = rUpload.message.data.id;
    //       //                   ControllerConfig.onCreateDirectoryGD(auth, "DISPUTE", FolderUpload_ID, function (rD: any) {

    //       //                   });
    //       //                   ControllerConfig.onCreateDirectoryGD(auth, "BUKTI_POTONG", FolderUpload_ID, function (rBP: any) {

    //       //                   });
    //       //                   ControllerConfig.onCreateDirectoryGD(auth, "BUKTI_BAYAR_PPH", FolderUpload_ID, function (rBBP: any) {

    //       //                   });
    //       //                   ControllerConfig.onCreateDirectoryGD(auth, "NOTE_DEBET", FolderUpload_ID, function (rND: any) {

    //       //                   });
    //       //                 }
    //       //               });
    //       //             }
    //       //           });
    //       //         } else {

    //       //         }
    //       //       });
    //       //     }
    //       //   });
    //       // } else {

    //       // }
    //       // });
    //       // });
    //     }
    //   }
    // });
  }


  // static onExecuteChekDirectory = (auth: any, folder_ad: string) => {
  // const drive = google.drive({version: 'v3', auth});
  // drive.files.list({
  //   q: "name contains 'DIRECTORY_DIGIPOS'",
  // }, (err, res: any) => {
  //   if (err) return console.log('The API returned an error: ' + err);
  //   var files: any; files = res.data.files;
  //   if (files.length) {
  //     files.map((file: any) => {
  //       console.log(`${file.name} (${file.id})`);
  //       var RootID = file.id;
  //       // var fileMetadata = {
  //       //   'name': folder_ad,
  //       //   'mimeType': 'application/vnd.google-apps.folder',
  //       //   'parents': [RootID]
  //       // };
  //       // drive.files.create({
  //       //   requestBody: fileMetadata,
  //       //   fields: 'id'
  //       // }, function (err: any, file: any) {
  //       //   if (err) {
  //       //     console.error(err);
  //       //   } else {
  //       //     console.log('Folder Id: ', file);
  //       //   }
  //       // });
  //     });
  //   } else {
  //     // ControllerConfig.onCreateDirectoryRoot(auth);
  //     // console.log('No files found.');
  //   }
  // });
  // }

  // static onCheckFolderFound = (auth: any, folder: string, callback: any) => {
  //   var returnStatus: any; returnStatus = {success: true, message: ""};
  //   const drive = google.drive({version: 'v3', auth});
  //   drive.files.list({
  //     q: "name contains '" + folder + "'",
  //   }, (err, res: any) => {
  //     if (err) return console.log('The API returned an error: ' + err);
  //     var files: any; files = res.data.files;
  //     if (files.length) {
  //       var RootID = "0";
  //       files.map((file: any) => {
  //         RootID = file;
  //       });
  //       returnStatus.success = true;
  //       returnStatus.message = RootID;
  //       callback(returnStatus);
  //     } else {
  //       // ControllerConfig.onCreateDirectoryGD(auth, folder, function (r: any) {
  //       //   console.log(r);
  //       // });
  //       returnStatus.success = false;
  //       returnStatus.message = 'No files found.';
  //       callback(returnStatus);
  //     }
  //   });
  // }

  static onCreateDirectoryGD = (auth: any, folder: string, parents: string, callback: any) => {
    var returnStatus: any; returnStatus = {success: true, message: ""};
    const drive = google.drive({version: 'v3', auth});
    var fileMetadata: any; fileMetadata = {
      'name': folder,
      'mimeType': 'application/vnd.google-apps.folder'
    };
    if (parents != "") {
      fileMetadata["parents"] = [parents];
    }
    drive.files.create({
      requestBody: fileMetadata,
      fields: 'id'
    }, function (err: any, file: any) {
      if (err) {
        returnStatus.success = false;
        returnStatus.message = err;
        callback(returnStatus);
      } else {
        returnStatus.success = true;
        returnStatus.message = file;
        callback(returnStatus);
      }
    });
  }

  static onUploadPDFGD = (auth: any, folderId: string, path: string, name: string, callback: any) => {
    var returnStatus: any; returnStatus = {success: true, message: ""};
    var folderId = folderId;
    var fileMetadata = {
      'name': name,
      parents: [folderId]
    };
    var media = {
      mimeType: 'application/pdf',
      body: fs.createReadStream(path)
    };

    const drive = google.drive({version: 'v3', auth});
    drive.files.create({
      requestBody: fileMetadata,
      media: media,
      fields: 'id'
    }, function (err: any, file: any) {
      if (err) {
        // Handle error
        returnStatus.success = false;
        returnStatus.message = err;
        callback(returnStatus);
      } else {
        returnStatus.success = true;
        returnStatus.message = file;
        callback(returnStatus);
      }
    });
  }

  static onUploadPDFGDReplace = (auth: any, fieldid: string, folderId: string, path: string, name: string, callback: any) => {
    var returnStatus: any; returnStatus = {success: true, message: ""};
    var folderId = folderId;
    var fileMetadata = {
      'name': name,
      parents: [folderId]
    };
    var media = {
      mimeType: 'application/pdf',
      body: fs.createReadStream(path)
    };

    const drive = google.drive({version: 'v3', auth});
    drive.files.update({
      fileId: fieldid,
      // requestBody: fileMetadata,
      media: media,
      fields: 'id'
    }, function (err: any, file: any) {
      if (err) {
        // Handle error
        returnStatus.success = false;
        returnStatus.message = err;
        callback(returnStatus);
      } else {
        returnStatus.success = true;
        returnStatus.message = file;
        callback(returnStatus);
      }
    });
  }


  // static onDeleteDirectoryGD = (auth: any, folder_id: string, callback: any) => {
  //   var returnStatus: any; returnStatus = {success: true, message: ""};
  //   const drive = google.drive({version: 'v3', auth});
  //   drive.files.delete({
  //     'fileId': folder_id
  //   }, function (err, file) {
  //     if (err) {
  //       returnStatus.success = false;
  //       returnStatus.message = err;
  //       callback(returnStatus);
  //     } else {
  //       returnStatus.success = true;
  //       returnStatus.message = file;
  //       callback(returnStatus);
  //     }
  //   });
  // }

  // static onCheckADDirectory = (auth: any, folder_ad: string) => {

  // }

  // static onCreateDirectoryRoot = (auth: any) => {
  //   const drive = google.drive({version: 'v3', auth});
  //   var fileMetadata = {
  //     'name': 'DIRECTORY_DIGIPOS',
  //     'mimeType': 'application/vnd.google-apps.folder'
  //   };
  //   drive.files.create({
  //     requestBody: fileMetadata,
  //     fields: 'id'
  //   }, function (err: any, file: any) {
  //     if (err) {
  //       console.error(err);
  //     } else {
  //       console.log('Folder Id: ', file);
  //     }
  //   });


  // }

  static onGetFileOnFolder = (auth: any, FolderID: any, callback: any) => {
    const drive = google.drive({version: 'v3', auth});
    drive.files.list({
      pageSize: 10,
      fields: 'nextPageToken, files(id, name)',
      q: "'" + FolderID + "' in parents ",
    }, (err, res: any) => {
      if (err) return console.log('The API returned an error: ' + err);
      var files: any; files = res.data.files;
      callback(files);
      // if (files.length) {
      //   files.map((file: any) => {
      //     console.log(`${file.name} (${file.id})`);
      //   });
      // } else {
      //   console.log('No files found.');
      // }
    });
  }

  static onDownloadFileCSV = async (auth: any, FileID: any, path: string, callback: any) => {
    var dest = fs.createWriteStream(path);
    const drive = google.drive({version: 'v3', auth});
    drive.files.get(
      {
        fileId: FileID,
        alt: 'media',
      },
      {
        responseType: 'stream',
      },
      (err: any, res: any) => {
        res.data
          .on('end', () => {
            callback({success: true, message: "success"});
          })
          .on('error', (error: any) => {
            // console.log('Error during download', error);
            callback({success: true, message: error});
          })
          .pipe(dest);
      }
    );
  }

  static authorize = (credentials: any, callback: any) => {
    const {client_secret, client_id, redirect_uris} = credentials.installed;
    const oAuth2Client = new google.auth.OAuth2(
      client_id, client_secret, redirect_uris[0]);

    // Check if we have previously stored a token.
    fs.readFile(TOKEN_PATH, (err: any, token: any) => {
      if (err) return ControllerConfig.getAccessToken(oAuth2Client, callback);
      oAuth2Client.setCredentials(JSON.parse(token));
      callback(oAuth2Client);
    });
  }

  static getAccessToken = (oAuth2Client: any, callback: any) => {
    const authUrl = oAuth2Client.generateAuthUrl({
      access_type: 'offline',
      scope: SCOPES,
    });
    console.log('Authorize this app by visiting this url:', authUrl);
    const rl = readline.createInterface({
      input: process.stdin,
      output: process.stdout,
    });
    rl.question('Enter the code from that page here: ', (code) => {
      rl.close();
      oAuth2Client.getToken(code, (err: any, token: any) => {
        if (err) return console.error('Error retrieving access token', err);
        oAuth2Client.setCredentials(token);
        // Store the token to disk for later program executions
        fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
          if (err) return console.error(err);
          console.log('Token stored to', TOKEN_PATH);
        });
        callback(oAuth2Client);
      });
    });
  }





  static onDateNow = () => {
    var d = new Date();
    return d.getDay() + "-" + ControllerConfig.onGetMonth(d.getMonth()) + "-" + d.getFullYear();
  }

  static onCurrentTime = () => {
    var d = new Date();
    return d.getTime();
  }

  // ================================================= GOOGLE DRIVE FUNCTION

  static getRoleIdMessage(type: string) {
    switch (type) {
      case "DIGIPOS_VALIDASI_DATA_BA":
        return "1,5";
      case "DIGIPOS_VALIDASI_DATA_BU":
        return "1,4";
      case "DIGIPOS_REJECT_DATA_BA":
        return "1,2,3,4,5,6,7,8";
      case "DIGIPOS_REJECT_DATA_BU":
        return "1,2,3,4,5,6,7,8";
      case "DIGIPOS_VALIDASI_DATA_INVOICE":
        return "1,6,8";
      case "FPJP_NEED_PAYMENT":
        return "1,8";
      case "FPJP_SUBMIT_INVOICE_AD":
        return "1,4,5";
      case "FPJP_SUBMIT_NOTA_AD":
        return "1,4,5";
      case "DIPUTE_SUBMIT_AD":
        return "1,4,5";
      default:
        return "1";
    }
  }


  // ================================================= GOOGLE DRIVE FUNCTION
  // SEARCH FOLDER
  static onGD_SearchFolder(auth: any, query: string, callback: any) {
    const thisDrives = google.drive({version: 'v3', auth});
    thisDrives.files.list({
      // pageSize: 10,
      // fields: 'nextPageToken, files(id, name)',
      fields: 'files(id, name)',
      q: "mimeType = 'application/vnd.google-apps.folder' and " + query,
    }, (err: any, res: any) => {
      var result: any; result = {success: false, data: [], message: ""}
      if (err) {
        result.success = false;
        result.data = [];
        result.message = err;
        callback(result);
        // return console.log('The API returned an error: ' + err);
      } else {

        result.success = true;
        result.data = res;
        result.message = "success";
        callback(result);
      }
    });
  }

  static onGD_Search(auth: any, query: string, callback: any) {
    const thisDrives = google.drive({version: 'v3', auth});
    thisDrives.files.list({
      // pageSize: 10,
      // fields: 'nextPageToken, files(id, name)',
      fields: 'files(id, name)',
      q: query,
    }, (err: any, res: any) => {
      var result: any; result = {success: false, data: [], message: ""}
      if (err) {
        result.success = false;
        result.data = [];
        result.message = err;
        callback(result);
        // return console.log('The API returned an error: ' + err);
      } else {

        result.success = true;
        result.data = res;
        result.message = "success";
        callback(result);
      }
    });
  }

  // Create Folder
  static onGD_CreateFolder(auth: any, query: any, callback: any) {
    const thisDrives = google.drive({version: 'v3', auth});
    thisDrives.files.create({
      requestBody: query,
      fields: 'id'
    }, async (err: any, res: any) => {
      var result: any; result = {success: false, data: [], message: ""}
      if (err) {
        result.success = false;
        result.data = [];
        result.message = err;
        callback(result);
        // return console.log('The API returned an error: ' + err);
      } else {
        result.success = true;
        result.data = res;
        result.message = "success";
        callback(result);
      }
    });
  }

  static staticconCheckEmailNamePermission(auth: any, email: string, callback: any) {
    const thisDrives = google.drive({version: 'v2', auth});
    thisDrives.permissions.getIdForEmail({
      'email': email,
    }, (err, res) => {
      if (err) {
        callback({success: false, message: err});
      } else {
        callback({success: true, message: res});
      }
    });
  }

  static onCheckPermisionAlready(auth: any, fileId: string, callback: any) {
    const thisDrives = google.drive({version: 'v3', auth});
    thisDrives.permissions.list({
      'fileId': fileId
    }, (err, res) => {
      if (err) {
        callback({success: false, message: err});
      } else {
        callback({success: true, message: res});
      }
    });
  }

  // MOVE FIle
  static onGD_MoveFile(auth: any, fieldId: any, fromFolder: any, toFolder: any, callback: any) {
    const thisDrives = google.drive({version: 'v3', auth});
    thisDrives.files.update({
      fileId: fieldId,
      addParents: toFolder,
      removeParents: fromFolder,
      fields: 'id, parents'
    }, async (err: any, res: any) => {
      var result: any; result = {success: false, data: [], message: ""}
      if (err) {
        // Handle error
        result.success = false;
        result.data = [];
        result.message = err;
        callback(result);
      } else {
        // File moved.
        result.success = true;
        result.data = res;
        result.message = "success";
        callback(result);
      }
    });
  }


  //CREATE pERMISSION
  static onChangePermissionGD = (auth: any, folder_id: any, permissions: any, callback: any) => {
    var returnStatus: any; returnStatus = {success: true, message: ""};
    var fileId = folder_id;
    async.eachSeries(permissions, function (permission: any, permissionCallback: any) {
      const drive = google.drive({version: 'v3', auth});
      drive.permissions.create({
        requestBody: permission,
        fileId: fileId,
        fields: 'id',
      }, function (err: any, res: any) {
        if (err) {
          // Handle error...
          console.error(err);
          permissionCallback(err);
        } else {
          // console.log('Permission ID: ', res)
          permissionCallback();
        }
      });
    }, function (err) {
      if (err) {
        // Handle error
        console.error(err);
        returnStatus.success = false;
        returnStatus.message = err;
        callback(returnStatus);
      } else {
        // All permissions inserted
        returnStatus.success = true;
        returnStatus.message = "success";
        callback(returnStatus);
      }
    });
  }


}
