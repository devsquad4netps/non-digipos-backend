import {authenticate} from '@loopback/authentication';
import {inject} from '@loopback/core';
import {
  repository
} from '@loopback/repository';
import {SecurityBindings, UserProfile} from '@loopback/security';
import {AdClusterRepository, AdMasterRepository, DataconfigRepository, DataDigiposRejectRepository, DataDigiposRepository, DataSumarryRepository, DisputeDetailRepository, DisputeRepository} from '../repositories';


export const ValidateDisputeBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['dispute'],
        properties: {
          dispute: {
            type: 'array',
            items: {type: 'number'},
          }
        }
      }
    },
  },
};


export const RejectDisputeRequestBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['reason_id', 'dispute', 'description'],
        properties: {
          reason_id: {
            type: 'number',
          },
          dispute: {
            type: 'array',
            items: {type: 'number'},
          },
          description: {
            type: 'string',
          }
        }
      }
    },
  },
};

@authenticate('jwt')
export class DisputeActionController {
  constructor(
    @repository(DisputeRepository)
    public disputeRepository: DisputeRepository,
    @repository(DataDigiposRepository)
    public dataDigiposRepository: DataDigiposRepository,
    @repository(DisputeDetailRepository)
    public disputeDetailRepository: DisputeDetailRepository,
    @repository(DataSumarryRepository)
    public dataSumarryRepository: DataSumarryRepository,
    @repository(AdMasterRepository)
    public adMasterRepository: AdMasterRepository,
    @repository(AdClusterRepository)
    public adClusterRepository: AdClusterRepository,
    @repository(DataconfigRepository)
    public dataconfigRepository: DataconfigRepository,
    @repository(DataDigiposRejectRepository)
    public dataDigiposRejectRepository: DataDigiposRejectRepository,
    @inject(SecurityBindings.USER)
    public currentUserProfile: UserProfile,
  ) { }

  // @post('/disputes/reject-dispute', {
  //   responses: {
  //     '200': {
  //       description: 'DataDigipos model instance',
  //       content: {
  //         'application/json': {
  //           schema: RejectDisputeRequestBody
  //         }
  //       },
  //     },
  //   },
  // })
  // async RejectDispute(
  //   @requestBody(RejectDisputeRequestBody) disputeRejectBA: {
  //     reason_id: number, dispute: [], description: string
  //   },
  // ): Promise<Object> {
  //   var returnMs: any; returnMs = {success: true, message: ""};

  //   var today = new Date();
  //   var digiposData = await this.disputeRepository.find({where: {and: [{dispute_id: {inq: disputeRejectBA.dispute}}]}});

  //   for (var i in digiposData) {
  //     var infoDigipost = digiposData[i];
  //     var config = await this.dataconfigRepository.findOne({where: {and: [{id: disputeRejectBA.reason_id}, {config_group_id: 12}]}});
  //     if (config != null) {
  //       await this.dataDigiposRejectRepository.create({
  //         periode: infoDigipost.periode,
  //         ad_code: infoDigipost.ad_code,
  //         ad_name: infoDigipost.ad_name,
  //         config_id: disputeRejectBA.reason_id,
  //         reject_code: config?.config_code,
  //         reject_label: config?.config_desc,
  //         reject_description: "Reject Dispute : BU - " + disputeRejectBA.description,
  //         create_at: today.toDateString(),
  //         user_reject: this.currentUserProfile[securityId]
  //       });
  //       await this.disputeRepository.updateById(infoDigipost.dispute_id, {
  //         acc_ba: 2
  //       });
  //     } else {
  //       returnMs.success = false;
  //       returnMs.message = "Reason not found, please entry other reason !";
  //     }
  //   }
  //   if (digiposData.length == 0) {
  //     returnMs.success = false;
  //     returnMs.message = "No data digipos rejected !";
  //   } else {
  //     if (returnMs.success == true) {
  //       returnMs.message = "success update [" + digiposData.length.toString() + "] data digipos !";
  //     }
  //   }
  //   return returnMs;
  // }

  // @post('/disputes/validate-dispute', {
  //   responses: {
  //     '200': {
  //       description: 'DataDigipos model instance',
  //       content: {
  //         'application/json': {
  //           schema: ValidateDisputeBody
  //         }
  //       },
  //     },
  //   },
  // })
  // async ValidateDispute(
  //   @requestBody(ValidateDisputeBody) disputeAccBA: {
  //     dispute: []
  //   },
  // ): Promise<Object> {
  //   var returnMs: any; returnMs = {success: true, message: ""};

  //   var today = new Date();
  //   var disputeData = await this.disputeRepository.find({where: {and: [{dispute_id: {inq: disputeAccBA.dispute}}, {acc_ba: {neq: 1}}]}});


  //   for (var i in disputeData) {
  //     var infoDispute = disputeData[i];
  //     try {
  //       await this.disputeRepository.updateById(infoDispute.dispute_id, {
  //         acc_ba: 1,
  //         at_acc: ControllerConfig.onCurrentTime().toString()
  //       });
  //       // var DigiposDispute = await this.dataDigiposRepository.findOne({where: {and: [{ad_code: infoDispute.ad_code}, {trx_date_group: infoDispute.periode}]}});
  //       // if (DigiposDispute != undefined && DigiposDispute != null) {
  //       //   await this.dataDigiposRepository.updateById(DigiposDispute.digipost_id, {
  //       //     dispute_id_a: infoDispute.dispute_id,
  //       //     is_dispute_b: 1,
  //       //     dispute_id_b: infoDispute.dispute_id
  //       //   });
  //       // }

  //     } catch (error) {
  //       returnMs.success = false;
  //       returnMs.message = error;
  //     }

  //   }
  //   if (disputeData.length == 0) {
  //     returnMs.success = false;
  //     returnMs.message = "No data dispute validate !";
  //   } else {
  //     if (returnMs.success == true) {
  //       returnMs.message = "success validate  [" + disputeData.length.toString() + "] data dispute !";
  //     }
  //   }
  //   return returnMs;
  // }


  // @post('/disputes', {
  //   responses: {
  //     '200': {
  //       description: 'Dispute model instance',
  //       content: {'application/json': {schema: getModelSchemaRef(Dispute)}},
  //     },
  //   },
  // })
  // async create(
  //   @requestBody({
  //     content: {
  //       'application/json': {
  //         schema: getModelSchemaRef(Dispute, {
  //           title: 'NewDispute',
  //           exclude: ['dispute_id'],
  //         }),
  //       },
  //     },
  //   })
  //   dispute: Omit<Dispute, 'dispute_id'>,
  // ): Promise<Dispute> {
  //   return this.disputeRepository.create(dispute);
  // }

  // @get('/disputes/count', {
  //   responses: {
  //     '200': {
  //       description: 'Dispute model count',
  //       content: {'application/json': {schema: CountSchema}},
  //     },
  //   },
  // })
  // async count(
  //   @param.where(Dispute) where?: Where<Dispute>,
  // ): Promise<Count> {
  //   return this.disputeRepository.count(where);
  // }

  // @get('/disputes', {
  //   responses: {
  //     '200': {
  //       description: 'Array of Dispute model instances',
  //       content: {
  //         'application/json': {
  //           schema: {
  //             type: 'array',
  //             items: getModelSchemaRef(Dispute, {includeRelations: true}),
  //           },
  //         },
  //       },
  //     },
  //   },
  // })
  // async find(
  //   @param.filter(Dispute) filter?: Filter<Dispute>,
  // ): Promise<Object> {
  //   var Datas: any; Datas = [];
  //   var dataDispute = await this.disputeRepository.find(filter);
  //   for (var i in dataDispute) {
  //     var infoDispute = dataDispute[i];
  //     var data: any; data = {};
  //     data["dispute_id"] = infoDispute.dispute_id;
  //     data["ad_code"] = infoDispute.ad_code;
  //     data["remarks"] = infoDispute.remarks;
  //     data["acc_ba"] = infoDispute.acc_ba;
  //     data["periode"] = infoDispute.periode;
  //     data["at_create"] = infoDispute.at_create;
  //     data["at_acc"] = infoDispute.at_acc;
  //     data["ad_name"] = infoDispute.ad_name;
  //     data["sales_fee"] = infoDispute.sales_fee;
  //     data["trx_a"] = infoDispute.trx_a;
  //     data["tot_mdr_a"] = infoDispute.tot_mdr_a;
  //     data["trx_b"] = infoDispute.trx_b;
  //     data["tot_mdr_b"] = infoDispute.tot_mdr_b;


  //     var summaryDetail = await this.dataDigiposRepository.execute(
  //       "SELECT " +
  //       "summary.periode, " +
  //       "summary.ad_code, " +
  //       "summary.dealer_code, " +
  //       "summary.location, " +
  //       "summary.name, " +
  //       "summary.group_amount,  " +
  //       "sum(summary.trx_a) as trx_a_sum, " +
  //       "sum(summary.tot_mdr_a) as tot_mdr_a_sum, " +
  //       "sum(summary.trx_b) as trx_b_sum, " +
  //       "sum(summary.tot_mdr_b) as tot_mdr_b_sum " +
  //       "FROM " +
  //       "(SELECT " +
  //       "DISTINCT(DataSumarry.id), " +
  //       "AdCluster.ad_code, " +
  //       "DataSumarry.dealer_code," +
  //       "DataSumarry.location, " +
  //       "DataSumarry.name, " +
  //       "IF(DataSumarry.paid_in_amount = 0, DataSumarry.withdraw_amount , DataSumarry.paid_in_amount) as group_amount, " +
  //       "DataSumarry.trx_a, " +
  //       "DataSumarry.tot_mdr_a, " +
  //       "DataSumarry.trx_b, " +
  //       "DataSumarry.tot_mdr_b, " +
  //       "DataSumarry.status, " +
  //       "DataSumarry.periode, " +
  //       "DataSumarry.create_at " +
  //       "FROM " +
  //       "DataSumarry " +
  //       "INNER JOIN " +
  //       "AdCluster " +
  //       "on " +
  //       "DataSumarry.dealer_code = AdCluster.ad_cluster_id " +
  //       "WHERE " +
  //       "DataSumarry.periode = '" + infoDispute.periode + "' AND AdCluster.ad_code = '" + infoDispute.ad_code + "') as summary " +
  //       "GROUP BY summary.dealer_code ,summary.group_amount ");

  //     const configData = await this.dataconfigRepository.find({where: {config_group_id: 1}});
  //     var configNum: any;
  //     configNum = {};
  //     for (var iC in configData) {
  //       var infoConfig = configData[iC];
  //       if (infoConfig.config_code == "0001-01") {
  //         configNum["persen_fee"] = infoConfig.config_value;
  //       }
  //       if (infoConfig.config_code == "0001-02") {
  //         configNum["persen_paid"] = infoConfig.config_value;
  //       }
  //       if (infoConfig.config_code == "0001-03") {
  //         configNum["num_dpp"] = infoConfig.config_value;
  //       }
  //       if (infoConfig.config_code == "0001-04") {
  //         configNum["num_ppn"] = infoConfig.config_value;
  //       }
  //       if (infoConfig.config_code == "0001-05") {
  //         configNum["num_pph"] = infoConfig.config_value;
  //       }
  //     }

  //     var infoSummaryDet: any;
  //     infoSummaryDet = [];
  //     for (var iSum in summaryDetail) {
  //       var infoDetail = summaryDetail[iSum];
  //       var trxFee = ((infoDetail.trx_a_sum * infoDetail.group_amount) * (configNum.persen_fee));
  //       var feeDPP = (trxFee / configNum.num_dpp);
  //       var feePPN = (feeDPP * configNum.num_ppn);
  //       var feeTotal = feeDPP + feePPN;
  //       var feePPH = (feeDPP * configNum.num_pph);
  //       var trxPaid = ((infoDetail.trx_b_sum * infoDetail.group_amount) * (configNum.persen_paid));
  //       var paidDPP = (trxPaid / configNum.num_dpp);
  //       var paidPPN = (paidDPP * configNum.num_ppn);
  //       var paidTotal = paidDPP + paidPPN;
  //       var paidPPH = (paidDPP * configNum.num_pph);
  //       infoSummaryDet.push({
  //         dealer_code: infoDetail.dealer_code,
  //         name: infoDetail.name,
  //         periode: infoDetail.periode,
  //         sales_fee: infoDetail.group_amount,
  //         trx_a: infoDetail.trx_a_sum,
  //         tot_mdr_a: trxFee,
  //         trx_b: infoDetail.trx_b_sum,
  //         tot_mdr_b: trxPaid
  //       });
  //     }
  //     data["summary"] = infoSummaryDet;

  //     var disputeDetail = await this.disputeDetailRepository.find({where: {dispute_id: infoDispute.dispute_id}});

  //     var infoDisputeDet: any;
  //     infoDisputeDet = [];
  //     for (var iDis in disputeDetail) {
  //       var infDis = disputeDetail[iDis];
  //       var trxFee = ((infDis.trx_a * infDis.sales_fee) * (configNum.persen_fee));
  //       var trxPaid = ((infDis.trx_b * infDis.sales_fee) * (configNum.persen_paid));
  //       infoDisputeDet.push({
  //         dealer_code: infDis.ad_cluster_id.toString(),
  //         name: infDis.ad_cluster_name,
  //         periode: infDis.periode,
  //         sales_fee: infDis.sales_fee,
  //         trx_a: infDis.trx_a,
  //         tot_mdr_a: trxFee,
  //         trx_b: infDis.trx_b,
  //         tot_mdr_b: trxPaid
  //       });
  //     }
  //     data["dispute"] = infoDisputeDet;
  //     Datas.push(data);
  //   }
  //   return Datas;
  // }

  // @patch('/disputes', {
  //   responses: {
  //     '200': {
  //       description: 'Dispute PATCH success count',
  //       content: {'application/json': {schema: CountSchema}},
  //     },
  //   },
  // })
  // async updateAll(
  //   @requestBody({
  //     content: {
  //       'application/json': {
  //         schema: getModelSchemaRef(Dispute, {partial: true}),
  //       },
  //     },
  //   })
  //   dispute: Dispute,
  //   @param.where(Dispute) where?: Where<Dispute>,
  // ): Promise<Count> {
  //   return this.disputeRepository.updateAll(dispute, where);
  // }

  // @get('/disputes/{id}', {
  //   responses: {
  //     '200': {
  //       description: 'Dispute model instance',
  //       content: {
  //         'application/json': {
  //           schema: getModelSchemaRef(Dispute, {includeRelations: true}),
  //         },
  //       },
  //     },
  //   },
  // })
  // async findById(
  //   @param.path.number('id') id: number,
  //   @param.filter(Dispute, {exclude: 'where'}) filter?: FilterExcludingWhere<Dispute>
  // ): Promise<Dispute> {
  //   return this.disputeRepository.findById(id, filter);
  // }

  // @patch('/disputes/{id}', {
  //   responses: {
  //     '204': {
  //       description: 'Dispute PATCH success',
  //     },
  //   },
  // })
  // async updateById(
  //   @param.path.number('id') id: number,
  //   @requestBody({
  //     content: {
  //       'application/json': {
  //         schema: getModelSchemaRef(Dispute, {partial: true}),
  //       },
  //     },
  //   })
  //   dispute: Dispute,
  // ): Promise<void> {
  //   await this.disputeRepository.updateById(id, dispute);
  // }

  // @put('/disputes/{id}', {
  //   responses: {
  //     '204': {
  //       description: 'Dispute PUT success',
  //     },
  //   },
  // })
  // async replaceById(
  //   @param.path.number('id') id: number,
  //   @requestBody() dispute: Dispute,
  // ): Promise<void> {
  //   await this.disputeRepository.replaceById(id, dispute);
  // }

  // @del('/disputes/{id}', {
  //   responses: {
  //     '204': {
  //       description: 'Dispute DELETE success',
  //     },
  //   },
  // })
  // async deleteById(@param.path.number('id') id: number): Promise<void> {
  //   await this.disputeRepository.deleteById(id);
  // }
}
