// // Uncomment these imports to begin using these cool features!

// import {Filter, FilterExcludingWhere, repository} from '@loopback/repository';
// import {del, get, getModelSchemaRef, param, post, put, requestBody} from '@loopback/rest';
// import {MerchantProduct} from '../models';
// import {MerchantProductRepository} from '../repositories/merchant-product.repository';
// import {ControllerConfig} from './controller.config';

// // import {inject} from '@loopback/core';


// export class NonDigiposProductController {
//   constructor(
//     @repository(MerchantProductRepository)
//     public merchantProductRepository: MerchantProductRepository,
//   ) { }

//   @get('/non-digipos-product', {
//     responses: {
//       '200': {
//         description: 'Array of non digipos product model instances',
//         content: {
//           'application/json': {
//             schema: {
//               type: 'array',
//               items: getModelSchemaRef(MerchantProduct, {includeRelations: true}),
//             },
//           },
//         },
//       },
//     },
//   })
//   async find(
//     @param.filter(MerchantProduct) filter?: Filter<MerchantProduct>,
//   ): Promise<MerchantProduct[]> {
//     return this.merchantProductRepository.find(filter);
//   }

//   @get('/non-digipos-product/{id}', {
//     responses: {
//       '200': {
//         description: 'non digipos product model instance',
//         content: {
//           'application/json': {
//             schema: getModelSchemaRef(MerchantProduct, {includeRelations: true}),
//           },
//         },
//       },
//     },
//   })
//   async findById(
//     @param.path.number('id') id: number,
//     @param.filter(MerchantProduct, {exclude: 'where'}) filter?: FilterExcludingWhere<MerchantProduct>
//   ): Promise<MerchantProduct> {
//     return this.merchantProductRepository.findById(id, filter);
//   }



//   @post('/non-digipos-product/create', {
//     responses: {
//       '200': {
//         description: 'Menu model instance',
//         content: {'application/json': {schema: getModelSchemaRef(MerchantProduct)}},
//       },
//     },
//   })
//   async create(
//     @requestBody({
//       content: {
//         'application/json': {
//           schema: getModelSchemaRef(MerchantProduct, {
//             title: 'NewMerchanProducnt',
//             exclude: ['mp_id', 'mp_product_id', 'mp_at_create', 'mp_at_update'],
//           }),
//         },
//       },
//     })
//     product: Omit<MerchantProduct, 'mp_id'>,
//   ): Promise<Object> {
//     var result: any; result = {success: false, message: "", data: {}};
//     try {
//       var infoCreate = await this.merchantProductRepository.create(product);
//       var id = infoCreate.mp_id == undefined ? "0" : infoCreate.mp_id;
//       var Zero = "0000"
//       var ProductId = "PRD" + Zero.substring(id.toString().length, 4) + id.toString();
//       var infoUpdate = await this.merchantProductRepository.updateById(infoCreate.mp_id, {
//         mp_product_id: ProductId,
//         mp_at_create: ControllerConfig.onCurrentTime().toString()
//       });
//       result.success = true;
//       result.message = "Success, create product";
//       result.data = infoUpdate;
//     } catch (error) {
//       result.success = false;
//       result.message = error;
//     }

//     return result;
//   }


//   @put('/non-digipos-product/update/{id}', {
//     responses: {
//       '200': {
//         description: 'Menu model instance',
//         content: {'application/json': {schema: getModelSchemaRef(MerchantProduct)}},
//       },
//     },
//   })
//   async updateById(
//     @param.path.number('id') id: number,
//     @requestBody({
//       content: {
//         'application/json': {
//           schema: getModelSchemaRef(MerchantProduct, {
//             title: 'UpdateMerchanProducnt',
//             exclude: ['mp_id', 'mp_product_id', 'mp_at_create', 'mp_at_update'],
//           }),
//         },
//       },
//     }) product: MerchantProduct,
//   ): Promise<Object> {
//     var result: any; result = {success: false, message: "", data: {}};
//     try {
//       var updateProduct = await this.merchantProductRepository.updateById(id, {
//         mp_product_code: product.mp_product_code,
//         mp_flag: product.mp_flag,
//         mp_product_name: product.mp_product_name,
//         mp_inactive_date: product.mp_inactive_date,
//         mp_at_update: ControllerConfig.onCurrentTime().toString()
//       });
//       result.success = true;
//       result.message = "Success, update product";
//       result.data = updateProduct;
//     } catch (error) {
//       result.success = false;
//       result.message = error;
//     }
//     return result;
//   }

//   @del('/non-digipos-product/delete/{id}', {
//     responses: {
//       '200': {
//         description: 'Merchan Product DELETE success',
//       },
//     },
//   })
//   async deleteById(@param.path.number('id') id: number): Promise<Object> {
//     var result: any; result = {success: false, message: ""};
//     await this.merchantProductRepository.deleteById(id);
//     result.success = true;
//     result.message = "Success, delete product.";
//     return result;
//   }


// }
