// Uncomment these imports to begin using these cool features!

import {repository} from '@loopback/repository';
import {post, requestBody} from '@loopback/rest';
import ejs from "ejs";
import fs from 'fs';
import pdf from "html-pdf";
import path from 'path';
import randomstring from 'randomstring';
import {AdMasterRepository, ApprovalMatrixMemberRepository, ApprovalMatrixRepository, ApprovalNotaRepository, DataconfigRepository, DataDigiposRejectRepository, DataFpjpRepository, DataNotificationRepository, HistoryFpjpNoteRepository, HistoryFpjpRejectRepository, MailSenderMessageRepository, UserRepository} from '../repositories';
import {ControllerConfig} from './controller.config';
// import {inject} from '@loopback/core';
require('dotenv').config();

export const FPJPApprovalByEmailBodyReq = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['token'],
        properties: {
          token: {
            type: 'string'
          }
        }
      }
    },
  },
};

export const FpjpUserRejectByEmailRequestBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['token_set', 'reason_id', 'description'],
        properties: {
          reason_id: {
            type: 'number',
          },
          description: {
            type: 'string',
          },
          token_set: {
            type: 'string',
          }
        }
      }
    },
  },
};


var FpjpGenerateStore: any; FpjpGenerateStore = {};
var runFpjpGenerate = false;
export class DataFpjpEmailFpjpController {
  constructor(
    @repository(DataFpjpRepository)
    public dataFpjpRepository: DataFpjpRepository,
    @repository(ApprovalMatrixRepository)
    public approvalMatrixRepository: ApprovalMatrixRepository,
    @repository(ApprovalMatrixMemberRepository)
    public approvalMatrixMemberRepository: ApprovalMatrixMemberRepository,
    @repository(ApprovalNotaRepository)
    public approvalNotaRepository: ApprovalNotaRepository,
    @repository(DataconfigRepository)
    public dataconfigRepository: DataconfigRepository,
    @repository(UserRepository)
    public userRepository: UserRepository,
    @repository(AdMasterRepository)
    public adMasterRepository: AdMasterRepository,
    @repository(DataDigiposRejectRepository)
    public dataDigiposRejectRepository: DataDigiposRejectRepository,
    @repository(HistoryFpjpRejectRepository)
    public historyFpjpRejectRepository: HistoryFpjpRejectRepository,
    @repository(HistoryFpjpNoteRepository)
    public historyFpjpNoteRepository: HistoryFpjpNoteRepository,
    @repository(DataNotificationRepository)
    public dataNotificationRepository: DataNotificationRepository,
    @repository(MailSenderMessageRepository)
    public mailSenderMessageRepository: MailSenderMessageRepository,
  ) { }


  pad2 = (n: Number) => {  // always returns a string
    return (n < 10 ? '0' : '') + n;
  }

  dateFormat = (values: Date) => {
    return values.getFullYear() + "-" +
      this.pad2(values.getMonth() + 1) + "-" +
      this.pad2(values.getDate()) + " " +
      this.pad2(values.getHours()) + ":" +
      this.pad2(values.getMinutes()) + ":" +
      this.pad2(values.getSeconds());
  }

  onNowDate = () => {
    var d = new Date();
    return d.getFullYear() + "-" + d.getMonth() + "-" + d.getDay();
  }

  onCheckLogin = (a: number, op: string, b: number) => {
    var status: boolean; status = false;
    switch (op) {
      case ">":
        status = a > b;
        break;
      case ">=":
        status = a >= b;
        break;
      case "<":
        status = a < b;
        break;
      case "<=":
        status = a <= b;
        break;
      case "==":
        status = a == b;
        break;
      case "!=":
        status = a != b;
        break;
      default:
        status = a == b;
        break;
    }
    return status;
  }

  onGenerateFileFpjp = async () => {

    for (var i in FpjpGenerateStore) {
      console.log(i, "RUNNING....");
      var indexFile = i;

      var FpjpInfo = FpjpGenerateStore[i];
      var DataFPJP: any; DataFPJP = {
        FPJPNumber: "",
        FPJPType: "",
        Tanngal: "",
        NamaPembuat: "",
        NIKPembuat: "",
        Jabatan: "",
        Divisi: "",
        DataPengeluaran: [],
        TotalAmount: 0,
        JmlTerbilang: "",
        PTName: "",
        NameRekening: "",
        NoRek: "",
        NamaBank: "",
        Diketahui: []
      };

      if (FpjpInfo.transaction_code != null && FpjpInfo.transaction_code != undefined) {
        var typeFPJP = ["", "Invoice", "Note Debit"];
        DataFPJP.FPJPNumber = FpjpInfo.transaction_code;
        DataFPJP.Tanngal = ControllerConfig.onBANowDate()//this.onBANowDate();
        var fpjpPeriode = FpjpInfo.periode.split("-");
        var Periode = ControllerConfig.onGetMonth((parseInt(fpjpPeriode[1]) - 1)) + " " + fpjpPeriode[0];
        var InfoUser = await this.userRepository.findById(FpjpInfo.by_submit_fpjp);
        var InfoAD = await this.adMasterRepository.findById(FpjpInfo.ad_code);
        DataFPJP.PTName = InfoAD.ad_name;
        DataFPJP.NoRek = InfoAD.no_rek;
        DataFPJP.NamaBank = InfoAD.name_bank;
        DataFPJP.NameRekening = InfoAD.name_rekening;

        DataFPJP.NamaPembuat = InfoUser.fullname;
        DataFPJP.NIKPembuat = InfoUser.nik;
        DataFPJP.Jabatan = InfoUser.jabatan;
        DataFPJP.Divisi = InfoUser.divisi;

        DataFPJP.Diketahui.push({Jabatan: InfoUser.jabatan, Name: InfoUser.fullname});

        var DataConfig = await this.dataconfigRepository.find({where: {config_group_id: 4}});
        var InfoPengeluaran: any; InfoPengeluaran = {};
        var PPNFpjp = 0;
        var TotalFpjp = 0;
        for (var i in DataConfig) {
          var infoConfig = DataConfig[i];
          if (FpjpInfo.type_trx == 1) {
            if (infoConfig.config_code == "0004-01") {
              TotalFpjp = FpjpInfo.price;
              InfoPengeluaran[infoConfig.config_code] = {No: 1, Keterangan: infoConfig.config_desc + " " + Periode, KodeAccount: infoConfig.label_code, Jumlah: FpjpInfo.price};
            }
            if (infoConfig.config_code == "0004-02") {
              InfoPengeluaran[infoConfig.config_code] = {No: 2, Keterangan: infoConfig.config_desc, KodeAccount: infoConfig.label_code, Jumlah: 0};
            }
            if (infoConfig.config_code == "0004-03") {
              PPNFpjp = infoConfig.config_value;
            }
            if (infoConfig.config_code == "0004-04") {
              DataFPJP.FPJPType = infoConfig.config_desc;
            }
          }

          if (FpjpInfo.type_trx == 2) {
            if (infoConfig.config_code == "0004-06") {
              DataFPJP.FPJPType = infoConfig.config_desc;
            }

            if (infoConfig.config_code == "0004-05") {
              TotalFpjp = FpjpInfo.price;
              InfoPengeluaran[infoConfig.config_code] = {No: 1, Keterangan: infoConfig.config_desc + " " + Periode, KodeAccount: infoConfig.label_code, Jumlah: FpjpInfo.price};
            }
          }
        }

        if (FpjpInfo.type_trx == 1) {
          var JmlPPN: number; JmlPPN = InfoPengeluaran["0004-01"].Jumlah * PPNFpjp;
          InfoPengeluaran["0004-02"].Keterangan = InfoPengeluaran["0004-02"].Keterangan + " " + (PPNFpjp * 100).toString() + "%"; //= {No: 1, Keterangan: infoConfig.config_value, Jumlah: 0};
          InfoPengeluaran["0004-02"].Jumlah = ControllerConfig.onGeneratePuluhan(parseFloat(JmlPPN.toFixed(0)));
          InfoPengeluaran["0004-01"].Jumlah = ControllerConfig.onGeneratePuluhan(parseFloat(InfoPengeluaran["0004-01"].Jumlah.toFixed(0)));

          DataFPJP.DataPengeluaran.push(InfoPengeluaran["0004-01"]);
          DataFPJP.DataPengeluaran.push(InfoPengeluaran["0004-02"]);
        }


        if (FpjpInfo.type_trx == 2) {
          InfoPengeluaran["0004-05"].Jumlah = ControllerConfig.onGeneratePuluhan(Number(InfoPengeluaran["0004-05"].Jumlah.toFixed(0)));
          DataFPJP.DataPengeluaran.push(InfoPengeluaran["0004-05"]);
        }


        var TotalAmount = FpjpInfo.type_trx == 1 ? TotalFpjp + (TotalFpjp * PPNFpjp) : TotalFpjp;

        DataFPJP.TotalAmount = ControllerConfig.onGeneratePuluhan(parseFloat(TotalAmount.toFixed(0)));
        DataFPJP.JmlTerbilang = ControllerConfig.onAngkaTerbilang(Number(TotalAmount.toFixed(0))) + " rupiah";

        var InfoApproval = await this.approvalNotaRepository.find({where: {nota_debet_id: FpjpInfo.id}});
        var UserApprove: any; UserApprove = [];
        for (var iUser in InfoApproval) {
          var users = InfoApproval[iUser].user_id;
          UserApprove.push(users);
          var ListApproval = await this.userRepository.findById(users);
          DataFPJP.Diketahui.push({Jabatan: ListApproval.jabatan, Name: ListApproval.fullname});
        }


        const headerImagePath = path.join(__dirname, '../../.digipost/template_ba/inv_header.png');
        const headerImageContents = fs.readFileSync(headerImagePath, {encoding: 'base64'});
        DataFPJP.headerImg = headerImageContents;

        var refToken = randomstring.generate() + ControllerConfig.onCurrentTime().toString();
        await this.dataFpjpRepository.updateById(FpjpInfo.id, {
          doc_ref: refToken
        });
        DataFPJP.refToken = refToken;

        var NameFileFPJP = FpjpInfo.transaction_code + ".pdf";
        var pathFile = path.join(__dirname, '../../.digipost/fpjp/') + NameFileFPJP;
        //MAKE FILE PDF BA
        var compiled = ejs.compile(fs.readFileSync(path.join(__dirname, '../../.digipost/template_ba/') + 'pdf_fpjp.html', 'utf8'));
        var html = compiled(DataFPJP);


        var config: any; config = {
          orientation: "portrait",
          "header": {
            "height": "80px",
            "contents": ""
          },
          "footer": {
            "height": "70px",
            "contents": ''
          }
        }
        pdf.create(html, config).toFile(pathFile, async (err: any, res: any) => {
          if (err) {
            console.log(err);
            await this.dataFpjpRepository.updateById(FpjpInfo.id, {
              fpjp_generate: 3,
              fpjp_generate_err: JSON.stringify(err)
            });
          } else {
            await this.dataFpjpRepository.updateById(FpjpInfo.id, {
              fpjp_generate: 2,
              fpjp_attach: NameFileFPJP,
              fpjp_generate_err: "",
              at_fpjp_generate: ControllerConfig.onCurrentTime().toString()
            })
          }
        });
      }

      delete FpjpGenerateStore[indexFile];
      console.log(FpjpGenerateStore);
      this.onGenerateFileFpjp();
      break;
    }
    var no = 0;
    for (var i in FpjpGenerateStore) {
      no++;
    }
    if (no == 0) {
      runFpjpGenerate = false;
    }
  }


  @post('/data-fpjp/approve-by-email', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: FPJPApprovalByEmailBodyReq
          }
        },
      },
    },
  })
  async ApproveFjpByEmail(
    @requestBody(FPJPApprovalByEmailBodyReq) FPJPApproval: {
      token: string
    },
  ): Promise<Object> {
    var returnMs: any; returnMs = {success: true, message: ""};

    var dataApprove = await this.approvalNotaRepository.find({where: {and: [{code_rand: FPJPApproval.token}, {state: 0}]}});
    if (dataApprove.length > 0) {
      for (var i in dataApprove) {
        var infoApprove = dataApprove[i];
        // try {

        var today = new Date();
        var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        var dateTime = date + ' ' + time;

        await this.approvalNotaRepository.updateById(infoApprove.id, {
          state: 1,
          at_update: dateTime
        });

        var dataFPJP = await this.dataFpjpRepository.findById(infoApprove.nota_debet_id);
        var userId = infoApprove.user_id;
        var roleNotifSetADB = ControllerConfig.onGetRoleNotifGroup("0801010103");
        var infoUser = await this.userRepository.findById(userId)
        await this.dataNotificationRepository.create({
          types: "NOTIFICATION",
          target: roleNotifSetADB.role,
          to: "",
          from_who: infoUser.username,
          subject: roleNotifSetADB.label,
          body: "Document FPJP Nomor : " + dataFPJP.transaction_code + " sudah di approve oleh " + infoUser.username,
          path_url: "",
          app: "DIGIPOS",
          code: "0801010103",
          code_label: "",
          at_create: ControllerConfig.onCurrentTime().toString(),
          at_read: "",
          periode: dataFPJP.periode,
          at_flag: 0
        });

        ControllerConfig.onSendNotif({
          types: "NOTIFICATION",
          target: roleNotifSetADB.role,
          to: "",
          from_who: infoUser.username,
          subject: roleNotifSetADB.label,
          body: "Document FPJP Nomor : " + dataFPJP.transaction_code + " sudah di approve oleh " + infoUser.username,
          path_url: "",
          app: "DIGIPOS",
          code: "0801010103",
          code_label: "",
          at_create: ControllerConfig.onCurrentTime().toString(),
          at_read: "",
          periode: dataFPJP.periode,
          at_flag: 0
        });

        var AccFPJP = false;
        var FalidRoot = true;
        var AllValidRoot = await this.approvalNotaRepository.find({where: {and: [{nota_debet_id: infoApprove.nota_debet_id}, {ap_level: 1}]}});
        for (var jRoot in AllValidRoot) {
          if (AllValidRoot[jRoot].state == 0 || AllValidRoot[jRoot].state == 2) {
            FalidRoot = false;
          }
        }

        var AllValidLevel = await this.approvalNotaRepository.find({where: {and: [{nota_debet_id: infoApprove.nota_debet_id}, {ap_level: 2}]}});;
        if (FalidRoot == false || AllValidRoot.length == 0) {
          var ValidLevel = true;
          for (var jRoot in AllValidLevel) {
            if (AllValidLevel[jRoot].state == 0 || AllValidLevel[jRoot].state == 2) {
              FalidRoot = false;
              ValidLevel = false;
            }
          }
          if (ValidLevel == true) {
            AccFPJP = true;
          }
        } else {
          //Valid
          AccFPJP = true;
        }

        if (AccFPJP == true) {
          await this.dataFpjpRepository.updateById(infoApprove.nota_debet_id, {
            state: 3,
            fpjp_generate: 1
          });
          var infoFpjp = await this.dataFpjpRepository.findById(infoApprove.nota_debet_id);
          FpjpGenerateStore[infoApprove.nota_debet_id] = infoFpjp;
        } else {
          var dataAppv = await this.approvalNotaRepository.findById(infoApprove.id);
          var dataAppvList = await this.approvalNotaRepository.find({where: {and: [{nota_debet_id: dataAppv.nota_debet_id}, {state: 0}]}});
          var UserFirstSet: string; UserFirstSet = "";
          var UserFirstToken: string; UserFirstToken = "";
          for (var iAppv in dataAppvList) {
            UserFirstSet = dataAppvList[iAppv].user_id;
            UserFirstToken = dataAppvList[iAppv].code_rand;
            break;
          }

          if (dataAppv != null) {
            var FpjpInfo = await this.dataFpjpRepository.findById(dataAppv.nota_debet_id);
            var UserInfo = await this.userRepository.findById(UserFirstSet);
            var UserInfoSubmit = await this.userRepository.findById(dataAppv.user_id);
            var FpjpType = ["", "Transaksi Nota Debit", "Transaksi Invoice"]
            var ContentFPJP: any; ContentFPJP = {
              name_approval: UserInfo.fullname,
              fpjp_number: FpjpInfo.transaction_code,
              fpjp_submit: UserInfoSubmit.fullname,
              link_approval: process.env.API_URL_FPJP,
              fpjp_type: FpjpType[FpjpInfo.type_trx],
              fpjp_name: "FPJP For " + FpjpInfo.ad_name,
              currency: "IDR ( Rupiah )",
              amount: "Rp." + ControllerConfig.onGeneratePuluhan(FpjpInfo.total_amount == undefined ? 0 : FpjpInfo.total_amount),
              link_list_approval: process.env.API_URL_FPJP,
              remarks: FpjpInfo.remarks,
              token_set: UserFirstToken
            }

            console.log(UserInfo.email, UserInfo.fullname, FpjpInfo.transaction_code, FpjpType[FpjpInfo.type_trx]);
            if (UserInfo.email != "" && UserInfo.email != null) {
              var dateNow = new Date();
              await this.mailSenderMessageRepository.create({
                AT_CREATE: this.dateFormat(dateNow),
                AT_UPDATE: this.dateFormat(dateNow),
                AT_SEND: undefined,
                FLAG_SEND: 0,
                SENDER_TO: UserInfo.email,
                MESSAGE: FpjpInfo.id + "#" + FpjpInfo.transaction_code + "#" + FpjpInfo.periode + "#" + FpjpType[FpjpInfo.type_trx] + "#" + UserInfo.fullname + "#" + UserInfoSubmit.fullname + "#" + UserFirstToken,
                MESSAGE_FOR: "APPROVAL_FPJP",
                SOURCE_DATA: "DataFpjp"
              });

              // MailConfig.onSendMailFPJP(UserInfo.email, ContentFPJP, (r: any) => {

              // });
            }
          }
        }
        returnMs.success = true;
        returnMs.message = "success update status approve !";
      }

      console.log(runFpjpGenerate);
      // if (runFpjpGenerate == false) {
      console.log("RIINING START...");
      this.onGenerateFileFpjp();
      runFpjpGenerate = true;
      // }
    } else {
      returnMs.success = false;
      returnMs.message = "Error Approve, not data approve exities !";
    }
    return returnMs;
  }

  @post('/data-fpjp/user-reject-by-email', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: FpjpUserRejectByEmailRequestBody
          }
        },
      },
    },
  })
  async UserRejectFjpByEmail(
    @requestBody(FpjpUserRejectByEmailRequestBody) FPJPApproval: {
      reason_id: number,
      description: string,
      token_set: string
    },
  ): Promise<Object> {
    var returnMs: any; returnMs = {success: true, message: ""};
    var UserID: string; UserID = "0";
    var thisDataApprove = await this.approvalNotaRepository.findOne({where: {and: [{code_rand: FPJPApproval.token_set}, {state: 0}]}});

    if (thisDataApprove != null) {

      var dataApprove = await this.approvalNotaRepository.findOne({where: {and: [{code_rand: FPJPApproval.token_set}, {state: 0}]}});
      var FpjpId = 0;
      if (dataApprove != null) {
        FpjpId = dataApprove.nota_debet_id;
        UserID = dataApprove.user_id;
      }

      var userInfo = await this.userRepository.findById(UserID);

      if (FpjpId != 0) {
        var DataApproveFPJP = await this.approvalNotaRepository.find({where: {nota_debet_id: FpjpId}});
        var ApprovalIndex: any; ApprovalIndex = [];

        for (var i in DataApproveFPJP) {
          var rawApproval = DataApproveFPJP[i];
          ApprovalIndex.push(rawApproval.id);
        }

        await this.approvalNotaRepository.deleteAll({nota_debet_id: FpjpId});
        var dataFPJP = await this.dataFpjpRepository.findById(FpjpId);
        if (dataFPJP.type_trx == 1) {
          await this.dataFpjpRepository.updateById(FpjpId, {
            acc_bu_state: 0,
            acc_bu_at: "",
            acc_bu_by: "",
            state: 1,
            fpjp_note_state: 1,
            fpjp_hold_state: 1,
            fpjp_note: FPJPApproval.description
          });
        }

        if (dataFPJP.type_trx == 2) {
          await this.dataFpjpRepository.updateById(FpjpId, {
            acc_ba_state: 0,
            acc_ba_at: "",
            acc_ba_by: "",
            acc_bu_state: 0,
            acc_bu_at: "",
            acc_bu_by: "",
            state: 1,
            fpjp_note_state: 1,
            fpjp_hold_state: 1,
            fpjp_note: FPJPApproval.description
          });
        }


        await this.historyFpjpRejectRepository.create({
          fpjp_id: FpjpId,
          reason_id: FPJPApproval.reason_id,
          message: FPJPApproval.description,
          at_reject: ControllerConfig.onCurrentTime().toString(),
          by_id_reject: UserID,
          by_reject: userInfo.fullname
        });

        var dataFPJP = await this.dataFpjpRepository.findById(FpjpId);
        var userId = UserID;
        var roleNotifSetADB = ControllerConfig.onGetRoleNotifGroup("0801010105");
        var infoUser = await this.userRepository.findById(userId)

        await this.dataNotificationRepository.create({
          types: "NOTIFICATION",
          target: roleNotifSetADB.role,
          to: "",
          from_who: infoUser.username,
          subject: roleNotifSetADB.label,
          body: "Document FPJP Nomor : " + dataFPJP.transaction_code + " sudah di tolak oleh" + infoUser.username,
          path_url: "",
          app: "DIGIPOS",
          code: "0801010105",
          code_label: "",
          at_create: ControllerConfig.onCurrentTime().toString(),
          at_read: "",
          periode: dataFPJP.periode,
          at_flag: 0
        });

        ControllerConfig.onSendNotif({
          types: "NOTIFICATION",
          target: roleNotifSetADB.role,
          to: "",
          from_who: infoUser.username,
          subject: roleNotifSetADB.label,
          body: "Document FPJP Nomor : " + dataFPJP.transaction_code + " sudah di tolak oleh" + infoUser.username,
          path_url: "",
          app: "DIGIPOS",
          code: "0801010105",
          code_label: "",
          at_create: ControllerConfig.onCurrentTime().toString(),
          at_read: "",
          periode: dataFPJP.periode,
          at_flag: 0
        });

        returnMs.success = true;
        returnMs.message = "succes reject fpjp";
      } else {
        returnMs.success = false;
        returnMs.message = "Error Fpjp reject, not data reject exities !";
      }
    } else {
      returnMs.success = false;
      returnMs.message = "Error Fpjp reject, not data reject exities !";
    }
    return returnMs;
  }

}
