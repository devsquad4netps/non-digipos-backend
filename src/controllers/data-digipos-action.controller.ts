import {authenticate} from '@loopback/authentication';
import {UserRepository} from '@loopback/authentication-jwt';
import {inject} from '@loopback/core';
import {
  Count,
  CountSchema,


  Filter,


  FilterExcludingWhere,


  repository,
  Where
} from '@loopback/repository';
import {
  del, get,
  getModelSchemaRef, param,


  patch, post,




  put,

  requestBody
} from '@loopback/rest';
import {SecurityBindings, securityId, UserProfile} from '@loopback/security';
import fs from 'fs';
import path from 'path';
import {DataDigipos, DataSumarry} from '../models';
import {AdClusterRepository, AdMasterRepository, DataconfigRepository, DataDigiposAttentionRepository, DataDigiposDetailRepository, DataDigiposRejectRepository, DataDigiposRepository, DataNotificationRepository, DisputeRepository} from '../repositories';
import {ControllerConfig} from './controller.config';
import {MailConfig} from './mail.config';

// import fs from 'fs';
// import readline from 'readline';
// import {google}  from 'googleapis';
// import async from 'async';

export const UpdateValidateRequestBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['digipos'],
        properties: {
          digipos: {
            type: 'array',
            items: {type: 'string'},
          }
        }
      }
    },
  },
};

export const UpdateGenerateCSVRequestBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['digipos'],
        properties: {
          digipos: {
            type: 'array',
            items: {type: 'string'},
          }
        }
      }
    },
  },
};


export const RejectValidateRequestBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['reason_id', 'digipos', 'description'],
        properties: {
          reason_id: {
            type: 'number',
          },
          digipos: {
            type: 'array',
            items: {type: 'string'},
          },
          description: {
            type: 'string',
          }
        }
      }
    },
  },
};

export const ShareDobBARequestBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['type_ad', 'periode'],
        properties: {
          type_ad: {
            type: 'string',
          },
          periode: {
            type: 'string',
          }
        }
      }
    },
  },
};

export const ShareDocInvoiceRequestBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['periode'],
        properties: {
          periode: {
            type: 'string',
          }
        }
      }
    },
  },
};


export const RejectValidateSignRequestBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['type_ad', 'reason_id', 'digipos', 'description'],
        properties: {
          type_ad: {
            type: 'string',
          },
          reason_id: {
            type: 'number',
          },
          digipos: {
            type: 'array',
            items: {type: 'string'},
          },
          description: {
            type: 'string',
          }
        }
      }
    },
  },
};

export const RejectValidatePaidRecvRequestBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['reason_id', 'digipos', 'description'],
        properties: {
          reason_id: {
            type: 'number',
          },
          digipos: {
            type: 'array',
            items: {type: 'string'},
          },
          description: {
            type: 'string',
          }
        }
      }
    },
  },
};

export const AccValidatePaidRecvRequestBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['digipos'],
        properties: {
          digipos: {
            type: 'array',
            items: {type: 'string'},
          },
        }
      }
    },
  },
};

export const ValidateSignRequestBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['type_ad', 'digipos'],
        properties: {
          type_ad: {
            type: 'string',
          },
          digipos: {
            type: 'array',
            items: {type: 'string'},
          }
        }
      }
    },
  },
};

export const UpdateNoBA = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['type_ad', 'digipos', 'no_ba'],
        properties: {
          type_ad: {
            type: 'string',
          },
          digipos: {
            type: 'number',
          },
          no_ba: {
            type: 'string',
          }
        }
      }
    },
  },
}

export const UpdateNoInvoice = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['digipos', 'no_ba'],
        properties: {
          digipos: {
            type: 'number',
          },
          no_ba: {
            type: 'string',
          }
        }
      }
    },
  },
}


const GDCONFIG = path.join(__dirname, '../../.config/directory.json');
var GoogleDriveShareBA: any; GoogleDriveShareBA = [];
var ProcessShareBA: any; ProcessShareBA = {};
var ProcessShareInvoice: any; ProcessShareInvoice = {};
var AdProcess: any; AdProcess = {};
var BarShareProcess: boolean; BarShareProcess = false;

var NewProcessShareBA: any; NewProcessShareBA = {};
var NewProcessShareBAState: boolean; NewProcessShareBAState = false;
var NewProcessShareBAAD: any; NewProcessShareBAAD = [];
var NewProcessShareBAPeriode: any; NewProcessShareBAPeriode = [];

@authenticate('jwt')
export class DataDigiposActionController {
  constructor(
    @repository(DataDigiposRepository)
    public dataDigiposRepository: DataDigiposRepository,
    @repository(DataDigiposDetailRepository)
    public dataDigiposDetailRepository: DataDigiposDetailRepository,
    @repository(DataconfigRepository)
    public dataconfigRepository: DataconfigRepository,
    @repository(DataDigiposRejectRepository)
    public dataDigiposRejectRepository: DataDigiposRejectRepository,
    @repository(AdMasterRepository)
    public adMasterRepository: AdMasterRepository,
    @repository(AdClusterRepository)
    public adClusterRepository: AdClusterRepository,
    @repository(DataDigiposAttentionRepository)
    public dataDigiposAttentionRepository: DataDigiposAttentionRepository,
    @repository(DisputeRepository)
    public disputeRepository: DisputeRepository,
    @inject(SecurityBindings.USER)
    public currentUserProfile: UserProfile,
    @repository(DataNotificationRepository)
    public dataNotificationRepository: DataNotificationRepository,
    @repository(UserRepository)
    public userRepository: UserRepository,
  ) { }

  onExecuteDistributeGoogleDriveInvoice = () => {
    for (var i in ProcessShareInvoice) {
      var infoDrive = ProcessShareInvoice[i];
      var IndexProcess = i;
      fs.readFile(GDCONFIG, async (errConfig, contentConfig: any) => {
        if (errConfig) return console.log('Error loading root folder:', errConfig);
        var configGD = JSON.parse(contentConfig);
        var RootID = configGD.root_directory;
        ControllerConfig.onAPIGoogleDrive(async (rConnect: any) => {
          if (rConnect.success == true) {
            var drive = rConnect.drive;
            var auth = rConnect.auth;
            var infoAD = await this.adMasterRepository.findById(infoDrive.ad_code);
            if (infoAD.gd_invoice != "" && infoDrive.inv_attacment != "" && infoAD.gd_invoice != null) {
              var pathFile = path.join(__dirname, '../../.digipost/invoice/') + infoDrive.inv_attacment;
              var FilePath = infoAD.gd_invoice;
              ControllerConfig.onGD_Search(auth, "'" + FilePath + "' in parents and	name = '" + infoDrive.inv_attacment + "'", async (rSearchInvoice: any) => {
                var filesinv = rSearchInvoice.data.data.files;
                if (filesinv.length == 0) {
                  ControllerConfig.onUploadPDFGD(auth, FilePath, pathFile, infoDrive.inv_attacment, async (rUp: any) => {
                    if (rUp.success == true) {
                      await this.dataDigiposRepository.updateById(infoDrive.digipost_id, {
                        distribute_file_invoice: 1
                      });

                      var infoDigipos = await this.dataDigiposRepository.findById(infoDrive.digipost_id);

                      if (infoAD.email != "" && infoAD.email != undefined || infoAD.email != null) {
                        MailConfig.onSendMailDistributeDoc(infoAD.email, {
                          type_doc: "Invoice",
                          ad_name: infoAD.ad_name,
                          name_doc: infoDrive.inv_attacment,
                          token_folder: FilePath
                        }, (r: any) => { });
                      }


                      var userId = this.currentUserProfile[securityId];
                      var roleNotifSetADB = ControllerConfig.onGetRoleNotifGroup("11010103");
                      var infoUser = await this.userRepository.findById(userId);
                      await this.dataNotificationRepository.create({
                        types: "NOTIFICATION",
                        target: roleNotifSetADB.role,
                        to: infoAD.ad_code,
                        from_who: infoUser.username,
                        subject: roleNotifSetADB.label,
                        body: "Document Invoice untuk No: " + infoDigipos.inv_number + " sudah berhasil di distribusikan !",
                        path_url: "",
                        app: "DIGIPOS",
                        code: "11010103",
                        code_label: "",
                        at_create: ControllerConfig.onCurrentTime().toString(),
                        at_read: "",
                        periode: infoDigipos.trx_date_group,
                        at_flag: 0
                      });

                      ControllerConfig.onSendNotif({
                        types: "NOTIFICATION",
                        target: roleNotifSetADB.role,
                        to: infoAD.ad_code,
                        from_who: infoUser.username,
                        subject: roleNotifSetADB.label,
                        body: "Document Invoice untuk No: " + infoDigipos.inv_number + " sudah berhasil di distribusikan !",
                        path_url: "",
                        app: "DIGIPOS",
                        code: "11010103",
                        code_label: "",
                        at_create: ControllerConfig.onCurrentTime().toString(),
                        at_read: "",
                        periode: infoDigipos.trx_date_group,
                        at_flag: 0
                      });


                    }
                  });

                  var noPr = 0;
                  for (var iPr in ProcessShareInvoice) {noPr++;}
                  if (noPr == 1) {

                    var infoDigipos = await this.dataDigiposRepository.findById(infoDrive.digipost_id);
                    var distribusiInvoice = await this.dataDigiposRepository.count({and: [{distribute_file_invoice: 1}, {trx_date_group: infoDigipos.trx_date_group}]});
                    var notDistribusiInvoice = await this.dataDigiposRepository.count({and: [{distribute_file_invoice: 0}, {trx_date_group: infoDigipos.trx_date_group}]});

                    var userId = this.currentUserProfile[securityId];
                    var roleNotifSetADB = ControllerConfig.onGetRoleNotifGroup("16010102");
                    var infoUser = await this.userRepository.findById(userId)
                    await this.dataNotificationRepository.create({
                      types: "NOTIFICATION",
                      target: roleNotifSetADB.role,
                      to: "",
                      from_who: infoUser.username,
                      subject: roleNotifSetADB.label,
                      body: "Document Invoice berhasil didistribusi, Jumlah sudah terdistribusi : " + distribusiInvoice.count.toString() + ", Jumlah belum terdistribusi : " + notDistribusiInvoice.count.toString(),
                      path_url: "",
                      app: "DIGIPOS",
                      code: "16010102",
                      code_label: "",
                      at_create: ControllerConfig.onCurrentTime().toString(),
                      at_read: "",
                      periode: infoDigipos.trx_date_group,
                      at_flag: 0
                    });

                    ControllerConfig.onSendNotif({
                      types: "NOTIFICATION",
                      target: roleNotifSetADB.role,
                      to: "",
                      from_who: infoUser.username,
                      subject: roleNotifSetADB.label,
                      body: "Document Invoice berhasil didistribusi, Jumlah sudah terdistribusi : " + distribusiInvoice.count.toString() + ", Jumlah belum terdistribusi : " + notDistribusiInvoice.count.toString(),
                      path_url: "",
                      app: "DIGIPOS",
                      code: "16010102",
                      code_label: "",
                      at_create: ControllerConfig.onCurrentTime().toString(),
                      at_read: "",
                      periode: infoDigipos.trx_date_group,
                      at_flag: 0
                    });
                  }

                  delete ProcessShareInvoice[IndexProcess];
                  this.onExecuteDistributeGoogleDriveInvoice();
                  return;
                } else {
                  var field: string; field = "";
                  for (var j in filesinv) {
                    var rFileInv = filesinv[j];
                    field = rFileInv.id
                  }
                  ControllerConfig.onUploadPDFGDReplace(auth, field, FilePath, pathFile, infoDrive.inv_attacment, async (rUp: any) => {
                    if (rUp.success == true) {
                      await this.dataDigiposRepository.updateById(infoDrive.digipost_id, {
                        distribute_file_invoice: 1
                      });

                      if (infoAD.email != "" && infoAD.email != undefined || infoAD.email != null) {
                        MailConfig.onSendMailDistributeDoc(infoAD.email, {
                          type_doc: "Invoice",
                          ad_name: infoAD.ad_name,
                          name_doc: infoDrive.inv_attacment,
                          token_folder: FilePath
                        }, (r: any) => { });
                      }

                      var infoDigipos = await this.dataDigiposRepository.findById(infoDrive.digipost_id);
                      var userId = this.currentUserProfile[securityId];
                      var roleNotifSetADB = ControllerConfig.onGetRoleNotifGroup("11010103");
                      var infoUser = await this.userRepository.findById(userId)
                      await this.dataNotificationRepository.create({
                        types: "NOTIFICATION",
                        target: roleNotifSetADB.role,
                        to: infoDigipos.ad_code,
                        from_who: infoUser.username,
                        subject: roleNotifSetADB.label,
                        body: "Document Invoice untuk No: " + infoDigipos.inv_number + " sudah berhasil di distribusikan !",
                        path_url: "",
                        app: "DIGIPOS",
                        code: "11010103",
                        code_label: "",
                        at_create: ControllerConfig.onCurrentTime().toString(),
                        at_read: "",
                        periode: infoDigipos.trx_date_group,
                        at_flag: 0
                      });

                      ControllerConfig.onSendNotif({
                        types: "NOTIFICATION",
                        target: roleNotifSetADB.role,
                        to: infoDigipos.ad_code,
                        from_who: infoUser.username,
                        subject: roleNotifSetADB.label,
                        body: "Document Invoice untuk No: " + infoDigipos.inv_number + " sudah berhasil di distribusikan !",
                        path_url: "",
                        app: "DIGIPOS",
                        code: "11010103",
                        code_label: "",
                        at_create: ControllerConfig.onCurrentTime().toString(),
                        at_read: "",
                        periode: infoDigipos.trx_date_group,
                        at_flag: 0
                      });
                    }
                  });

                  var noPr = 0;
                  for (var iPr in ProcessShareInvoice) {noPr++;}
                  if (noPr == 1) {

                    var infoDigipos = await this.dataDigiposRepository.findById(infoDrive.digipost_id);
                    var distribusiInvoice = await this.dataDigiposRepository.count({and: [{distribute_file_invoice: 1}, {trx_date_group: infoDigipos.trx_date_group}]});
                    var notDistribusiInvoice = await this.dataDigiposRepository.count({and: [{distribute_file_invoice: 0}, {trx_date_group: infoDigipos.trx_date_group}]});

                    var userId = this.currentUserProfile[securityId];
                    var roleNotifSetADB = ControllerConfig.onGetRoleNotifGroup("16010102");
                    var infoUser = await this.userRepository.findById(userId)
                    await this.dataNotificationRepository.create({
                      types: "NOTIFICATION",
                      target: roleNotifSetADB.role,
                      to: "",
                      from_who: infoUser.username,
                      subject: roleNotifSetADB.label,
                      body: "Document Invoice berhasil didistribusi, Jumlah sudah terdistribusi : " + distribusiInvoice.count.toString() + ", Jumlah belum terdistribusi : " + notDistribusiInvoice.count.toString(),
                      path_url: "",
                      app: "DIGIPOS",
                      code: "16010102",
                      code_label: "",
                      at_create: ControllerConfig.onCurrentTime().toString(),
                      at_read: "",
                      periode: infoDigipos.trx_date_group,
                      at_flag: 0
                    });

                    ControllerConfig.onSendNotif({
                      types: "NOTIFICATION",
                      target: roleNotifSetADB.role,
                      to: "",
                      from_who: infoUser.username,
                      subject: roleNotifSetADB.label,
                      body: "Document Invoice berhasil didistribusi, Jumlah sudah terdistribusi : " + distribusiInvoice.count.toString() + ", Jumlah belum terdistribusi : " + notDistribusiInvoice.count.toString(),
                      path_url: "",
                      app: "DIGIPOS",
                      code: "16010102",
                      code_label: "",
                      at_create: ControllerConfig.onCurrentTime().toString(),
                      at_read: "",
                      periode: infoDigipos.trx_date_group,
                      at_flag: 0
                    });
                  }

                  delete ProcessShareInvoice[IndexProcess];
                  this.onExecuteDistributeGoogleDriveInvoice();
                  return;
                }
              });
            } else {

              var noPr = 0;
              for (var iPr in ProcessShareInvoice) {noPr++;}
              if (noPr == 1) {

                var infoDigipos = await this.dataDigiposRepository.findById(infoDrive.digipost_id);
                var distribusiInvoice = await this.dataDigiposRepository.count({and: [{distribute_file_invoice: 1}, {trx_date_group: infoDigipos.trx_date_group}]});
                var notDistribusiInvoice = await this.dataDigiposRepository.count({and: [{distribute_file_invoice: 0}, {trx_date_group: infoDigipos.trx_date_group}]});

                var userId = this.currentUserProfile[securityId];
                var roleNotifSetADB = ControllerConfig.onGetRoleNotifGroup("16010102");
                var infoUser = await this.userRepository.findById(userId)
                await this.dataNotificationRepository.create({
                  types: "NOTIFICATION",
                  target: roleNotifSetADB.role,
                  to: "",
                  from_who: infoUser.username,
                  subject: roleNotifSetADB.label,
                  body: "Document Invoice berhasil didistribusi, Jumlah sudah terdistribusi : " + distribusiInvoice.count.toString() + ", Jumlah belum terdistribusi : " + notDistribusiInvoice.count.toString(),
                  path_url: "",
                  app: "DIGIPOS",
                  code: "16010102",
                  code_label: "",
                  at_create: ControllerConfig.onCurrentTime().toString(),
                  at_read: "",
                  periode: infoDigipos.trx_date_group,
                  at_flag: 0
                });

                ControllerConfig.onSendNotif({
                  types: "NOTIFICATION",
                  target: roleNotifSetADB.role,
                  to: "",
                  from_who: infoUser.username,
                  subject: roleNotifSetADB.label,
                  body: "Document Invoice berhasil didistribusi, Jumlah sudah terdistribusi : " + distribusiInvoice.count.toString() + ", Jumlah belum terdistribusi : " + notDistribusiInvoice.count.toString(),
                  path_url: "",
                  app: "DIGIPOS",
                  code: "16010102",
                  code_label: "",
                  at_create: ControllerConfig.onCurrentTime().toString(),
                  at_read: "",
                  periode: infoDigipos.trx_date_group,
                  at_flag: 0
                });
              }

              delete ProcessShareInvoice[IndexProcess];
              this.onExecuteDistributeGoogleDriveInvoice();
              return;
            }
          }
        });
      });

      break;
    }
  }


  onExecuteDistributeGoogleDriveBA = async () => {
    var dataMasterAd = await this.adMasterRepository.find({where: {ad_code: {inq: NewProcessShareBAAD}}});
    for (var i in dataMasterAd) {
      var infoAdMaster = dataMasterAd[i];
      if (AdProcess[infoAdMaster.ad_code] != undefined) {
        continue;
      }
      console.log("RUNNING", infoAdMaster.ad_code);
      var ADGoogleDrive: string; ADGoogleDrive = "";
      var ADGooglrAuth: any; ADGooglrAuth = "";

      AdProcess[infoAdMaster.ad_code] = [];
      fs.readFile(GDCONFIG, async (errConfig, contentConfig: any) => {
        var GD_PATH = JSON.parse(contentConfig);
        var Folder_PortalAD = GD_PATH.PORTAL_AD;
        ControllerConfig.onAPIGoogleDrive(async (rConnect: any) => {
          if (rConnect.success == true) {
            var drive = rConnect.drive;
            var auth = rConnect.auth;
            ADGooglrAuth = auth;

            var folderAD = infoAdMaster.ad_name.split(" ").join("_") + "(" + infoAdMaster.ad_code + ")";
            ControllerConfig.onGD_SearchFolder(auth, "'" + Folder_PortalAD + "' in parents and	name = '" + folderAD + "'", async (r: any) => {
              if (r.success == true) {
                var files = r.data.data.files;
                if (files.length == 0) {
                  ControllerConfig.onGD_CreateFolder(auth, {'name': folderAD, 'mimeType': 'application/vnd.google-apps.folder', 'parents': [Folder_PortalAD]}, async (rPortal: any) => {
                    AdProcess[infoAdMaster.ad_code].push(folderAD);
                    if (rPortal.success == true) {
                      var DirAD = rPortal.data.data.id;
                      ADGoogleDrive = DirAD;

                      ControllerConfig.onGD_CreateFolder(auth, {'name': "DOWNLOAD", 'mimeType': 'application/vnd.google-apps.folder', 'parents': [DirAD]}, async (rDownload: any) => {
                        AdProcess[infoAdMaster.ad_code].push("DOWNLOAD");
                        if (rDownload.success == true) {
                          var DirADDownload = rDownload.data.data.id;
                          ControllerConfig.onGD_CreateFolder(auth, {'name': "AD A", 'mimeType': 'application/vnd.google-apps.folder', 'parents': [DirADDownload]}, async (rAD_A: any) => {
                            AdProcess[infoAdMaster.ad_code].push("AD A");
                            if (rAD_A.success == true) {
                              var DirAD_A = rAD_A.data.data.id;

                              ControllerConfig.onGD_CreateFolder(auth, {'name': "BAR", 'mimeType': 'application/vnd.google-apps.folder', 'parents': [DirAD_A]}, async (rBar_A: any) => {
                                AdProcess[infoAdMaster.ad_code].push("BAR A");
                                if (rBar_A.success == true) {
                                  var DirBarRecA = rBar_A.data.data.id;
                                  await this.adMasterRepository.updateById(infoAdMaster.ad_code, {gd_bar_a: DirBarRecA});
                                }
                              });

                              ControllerConfig.onGD_CreateFolder(auth, {'name': "BUKPOT", 'mimeType': 'application/vnd.google-apps.folder', 'parents': [DirAD_A]}, async (rBukpot: any) => {
                                AdProcess[infoAdMaster.ad_code].push("BUKPOT");
                                if (rBukpot.success == true) {
                                  var DirBukpot = rBukpot.data.data.id;
                                  await this.adMasterRepository.updateById(infoAdMaster.ad_code, {gd_bukpot: DirBukpot});
                                }
                              });

                              ControllerConfig.onGD_CreateFolder(auth, {'name': "SUMMARY", 'mimeType': 'application/vnd.google-apps.folder', 'parents': [DirAD_A]}, async (rSummary_A: any) => {
                                AdProcess[infoAdMaster.ad_code].push("SUMMARY A");
                                if (rSummary_A.success == true) {
                                  var DirSummaryA = rSummary_A.data.data.id;
                                  await this.adMasterRepository.updateById(infoAdMaster.ad_code, {gd_summary_a: DirSummaryA});
                                }
                              });
                            }
                          });

                          ControllerConfig.onGD_CreateFolder(auth, {'name': "AD B", 'mimeType': 'application/vnd.google-apps.folder', 'parents': [DirADDownload]}, async (rAD_B: any) => {
                            AdProcess[infoAdMaster.ad_code].push("AD B");
                            if (rAD_B.success == true) {
                              var DirAD_B = rAD_B.data.data.id;
                              ControllerConfig.onGD_CreateFolder(auth, {'name': "BAR", 'mimeType': 'application/vnd.google-apps.folder', 'parents': [DirAD_B]}, async (rBar_B: any) => {
                                AdProcess[infoAdMaster.ad_code].push("BAR B");
                                if (rBar_B.success == true) {
                                  var DirBarRecB = rBar_B.data.data.id;
                                  await this.adMasterRepository.updateById(infoAdMaster.ad_code, {gd_bar_b: DirBarRecB});
                                }
                              });

                              ControllerConfig.onGD_CreateFolder(auth, {'name': "INVOICE", 'mimeType': 'application/vnd.google-apps.folder', 'parents': [DirAD_B]}, async (rInvoice: any) => {
                                AdProcess[infoAdMaster.ad_code].push("INVOICE");
                                if (rInvoice.success == true) {
                                  var DirInvoice = rInvoice.data.data.id;
                                  await this.adMasterRepository.updateById(infoAdMaster.ad_code, {gd_invoice: DirInvoice});
                                }
                              });

                              ControllerConfig.onGD_CreateFolder(auth, {'name': "FAKTUR PAJAK", 'mimeType': 'application/vnd.google-apps.folder', 'parents': [DirAD_B]}, async (rFp: any) => {
                                AdProcess[infoAdMaster.ad_code].push("FAKTUR PAJAK");
                                if (rFp.success == true) {
                                  var DirFp = rFp.data.data.id;
                                  await this.adMasterRepository.updateById(infoAdMaster.ad_code, {gd_faktur_pajak: DirFp});
                                }
                              });

                              ControllerConfig.onGD_CreateFolder(auth, {'name': "SUMMARY", 'mimeType': 'application/vnd.google-apps.folder', 'parents': [DirAD_B]}, async (rSummary: any) => {
                                AdProcess[infoAdMaster.ad_code].push("SUMMARY B");
                                if (rSummary.success == true) {
                                  var DirSummary = rSummary.data.data.id;
                                  await this.adMasterRepository.updateById(infoAdMaster.ad_code, {gd_summary_b: DirSummary});
                                }
                              });
                            }
                          });
                        }
                      });

                      ControllerConfig.onGD_CreateFolder(auth, {'name': "UPLOAD", 'mimeType': 'application/vnd.google-apps.folder', 'parents': [DirAD]}, async (rUpload: any) => {
                        AdProcess[infoAdMaster.ad_code].push("UPLOAD");
                        if (rUpload.success == true) {
                        }
                      });
                    }
                  });
                } else {
                  var field: string; field = "";
                  for (var j in files) {
                    var rFileBar = files[j];
                    field = rFileBar.id
                  }
                  ADGoogleDrive = field;
                  AdProcess[infoAdMaster.ad_code] = [
                    infoAdMaster.ad_name + "(" + infoAdMaster.ad_code + ")",
                    'UPLOAD',
                    'DOWNLOAD',
                    'AD A',
                    'AD B',
                    'BAR A',
                    'SUMMARY B',
                    'FAKTUR PAJAK',
                    'SUMMARY A',
                    'INVOICE',
                    'BAR B',
                    'BUKPOT'
                  ];
                }
              } else {
                //ERROR SEARCH FILE
              }
            });
          }
        });
      });
      var intervalSet: any;
      intervalSet = setInterval(async () => {
        if (AdProcess[infoAdMaster.ad_code].length >= 12) {
          if (infoAdMaster.email != null && infoAdMaster.email != "" && infoAdMaster.email != undefined) {

            // var PermissionList: any; PermissionList = [];

            // await new Promise((resolve, reject) => {
            //   ControllerConfig.onCheckPermisionAlready(ADGooglrAuth, ADGoogleDrive, (r: any) => {
            //     resolve(r);
            //     if (r.success == true) {
            //       // console.log(r.message.items);
            //       // console.log(r.message);
            //       for (var i in r.message.data.permissions) {
            //         var RawPermission = r.message.data.permissions[i];
            //         var idPermission = RawPermission.id;
            //         PermissionList.push(idPermission);
            //         console.log("Permission ID", idPermission);
            //       }
            //     } else {
            //       console.log(r);
            //     }
            //   });
            // });

            // ControllerConfig.staticconCheckEmailNamePermission(ADGooglrAuth, infoAdMaster.email, (rPermission: any) => {
            //   // console.log("Permission Email ID", rPermission);
            //   if (PermissionList.indexOf(rPermission.message.data.id) == -1) {
            //     var PermissionGD = [
            //       {
            //         'type': 'user',
            //         'role': 'writer',
            //         'emailAddress': infoAdMaster.email
            //       }
            //     ];
            //     ControllerConfig.onChangePermissionGD(ADGooglrAuth, ADGoogleDrive, PermissionGD, function (rPermission: any) {
            //       console.log(rPermission);
            //     });
            //     console.log("New Create Permission", "Success");
            //   }
            // });
          } else {
            console.log("Error Permison", infoAdMaster.email);
          }
          clearInterval(intervalSet);
          var noProcess = 0;
          for (var iProcess in AdProcess) {noProcess++;}
          if (noProcess == dataMasterAd.length) {
            console.log("FINISH");
            this.onExecuteDistributeDocBar();
          } else {

            this.onExecuteDistributeGoogleDriveBA();
          }
        }
      }, 1000);
      break;
    }
  }


  onExecuteDistributeDocBar = async () => {
    // NewProcessShareBA

    var noProcess = 0;
    for (var i in NewProcessShareBA) {
      console.log(i);

      var keyProcess = i;
      var Raw = NewProcessShareBA[i];

      // Data Digipos
      var Type = Raw.Type;
      var DataRaw = Raw.DataRaw;
      var DocBar = Raw.DOC_BAR;
      var DocSummary = Raw.DOC_SUMMARY;


      fs.readFile(GDCONFIG, async (errConfig, contentConfig: any) => {
        var GD_PATH = JSON.parse(contentConfig);
        var Folder_PortalAD = GD_PATH.PORTAL_AD;
        ControllerConfig.onAPIGoogleDrive(async (rConnect: any) => {
          if (rConnect.success == true) {
            var drive = rConnect.drive;
            var auth = rConnect.auth;

            //INFO MASTER
            var infoAdMaster = await this.adMasterRepository.findById(DataRaw.ad_code);
            if (infoAdMaster.ad_code != undefined) {

              // DOCUMENT BAR A
              if (Type == "AD_A") {

                await new Promise((resolve, reject) => {
                  // DOCUMENT BAR RECON SET
                  if (infoAdMaster.gd_bar_a != null && infoAdMaster.gd_bar_a != "") {
                    var FilePath = infoAdMaster.gd_bar_a;
                    ControllerConfig.onGD_Search(auth, "'" + FilePath + "' in parents and	name = '" + DocBar.FileName + "'", async (rSearchBar: any) => {
                      var filesBar = rSearchBar.data.data.files;
                      if (filesBar.length == 0) {
                        // NEW DOCUMENT GOOGLE DRIVE
                        ControllerConfig.onUploadPDFGD(auth, FilePath, DocBar.FilePath, DocBar.FileName, async (rCreate: any) => {
                          if (rCreate.success == true) {

                            //UPDATE STATE RAW DATA DISTRIBUTE BAR A
                            await this.dataDigiposRepository.updateById(DataRaw.digipost_id, {
                              google_drive_a: 1
                            });

                            // SEND MAIL NOTIF
                            var rawInfo = await this.dataDigiposRepository.findById(DataRaw.digipost_id);
                            if (infoAdMaster.email != "" && infoAdMaster.email != null) {
                              MailConfig.onSendMailDistributeDoc(infoAdMaster.email, {
                                type_doc: "BAR Recon AD A",
                                ad_name: infoAdMaster.ad_name,
                                name_doc: rawInfo.ba_attachment_a,
                                token_folder: FilePath
                              }, (r: any) => { });
                            }

                            // SEND NOTIFICATION
                            var userId = this.currentUserProfile[securityId];
                            var roleNotifSetADB = ControllerConfig.onGetRoleNotifGroup("0501010101");
                            var infoUser = await this.userRepository.findById(userId)
                            var dataMsg = {
                              types: "NOTIFICATION",
                              target: roleNotifSetADB.role,
                              to: DataRaw.ad_code,
                              from_who: infoUser.username,
                              subject: roleNotifSetADB.label,
                              body: "Distribute Document BAR Recon untuk AD  Nomor : " + DataRaw.ba_number_a + " sudah berhasil.",
                              path_url: "",
                              app: "DIGIPOS",
                              code: "0501010101",
                              code_label: "",
                              at_create: ControllerConfig.onCurrentTime().toString(),
                              at_read: "",
                              periode: DataRaw.trx_date_group,
                              at_flag: 0
                            }
                            await this.dataNotificationRepository.create(dataMsg);
                            ControllerConfig.onSendNotif(dataMsg);

                            console.log("SUCCESS, SHARE A ", DataRaw.ba_number_a);
                            resolve({success: true, message: "Success distribute BAR Recon A."});
                          } else {
                            // ERROR UPLOAD NEW DOCUMENT TO GOOGLE DRIVE
                            console.log("Error NEW Upload Google Drive BAR", rCreate);
                            resolve({success: false, message: rCreate});
                          }

                        });
                      } else {
                        // UPDATE DOCUMENT GOOGLE DRIVE
                        var field: string; field = "";
                        for (var j in filesBar) {
                          var rFileBar = filesBar[j];
                          field = rFileBar.id
                        }
                        ControllerConfig.onUploadPDFGDReplace(auth, field, FilePath, DocBar.FilePath, DocBar.FileName, async (rUp: any) => {
                          if (rUp.success == true) {
                            //UPDATE STATE RAW DATA DISTRIBUTE BAR A
                            await this.dataDigiposRepository.updateById(DataRaw.digipost_id, {
                              google_drive_a: 1
                            });

                            // SEND MAIL NOTIF
                            var rawInfo = await this.dataDigiposRepository.findById(DataRaw.digipost_id);
                            if (infoAdMaster.email != "" && infoAdMaster.email != null) {
                              MailConfig.onSendMailDistributeDoc(infoAdMaster.email, {
                                type_doc: "BAR Recon AD A",
                                ad_name: infoAdMaster.ad_name,
                                name_doc: rawInfo.ba_attachment_a,
                                token_folder: FilePath
                              }, (r: any) => { });
                            }

                            // SEND NOTIFICATION
                            var userId = this.currentUserProfile[securityId];
                            var roleNotifSetADB = ControllerConfig.onGetRoleNotifGroup("0501010101");
                            var infoUser = await this.userRepository.findById(userId)
                            var dataMsg = {
                              types: "NOTIFICATION",
                              target: roleNotifSetADB.role,
                              to: DataRaw.ad_code,
                              from_who: infoUser.username,
                              subject: roleNotifSetADB.label,
                              body: "Distribute Document BAR Recon untuk AD  Nomor : " + DataRaw.ba_number_a + " sudah berhasil.",
                              path_url: "",
                              app: "DIGIPOS",
                              code: "0501010101",
                              code_label: "",
                              at_create: ControllerConfig.onCurrentTime().toString(),
                              at_read: "",
                              periode: DataRaw.trx_date_group,
                              at_flag: 0
                            }
                            await this.dataNotificationRepository.create(dataMsg);
                            ControllerConfig.onSendNotif(dataMsg);

                            console.log("SUCCESS, SHARE A ", DataRaw.ba_number_a);
                            resolve({success: true, message: "Success distribute BAR Recon A."});
                          } else {
                            // ERROR UPDATE UPLOAD NEW DOCUMENT TO GOOGLE DRIVE
                            console.log("Error Update Upload Google Drive BAR", rUp);
                            resolve({success: false, message: rUp});
                          }
                        });
                      }
                    });
                  } else {
                    //DIRECTORY  BAR RECON UNTUK AD DIGOOGLE DRIVE BLUM ADA
                    console.log("Error Distribute AD A", "Directory AD A belum ada di google drive");
                    resolve({success: false, message: "Directory AD A belum ada di google drive"});
                  }
                });


                await new Promise((resolve, reject) => {
                  // DOCUMENT BAR RECON SUMMARY SET
                  if (infoAdMaster.gd_summary_a != null && infoAdMaster.gd_summary_a != "") {
                    var FilePath = infoAdMaster.gd_summary_a;
                    ControllerConfig.onGD_Search(auth, "'" + FilePath + "' in parents and	name = '" + DocSummary.FileName + "'", async (rSearchBar: any) => {
                      var filesBar = rSearchBar.data.data.files;
                      if (filesBar.length == 0) {
                        // NEW DOCUMENT GOOGLE DRIVE
                        ControllerConfig.onUploadPDFGD(auth, FilePath, DocSummary.FilePath, DocSummary.FileName, async (rCreate: any) => {
                          if (rCreate.success == true) {
                            resolve({success: true, message: "suucess"});
                          } else {
                            // ERROR UPDATE UPLOAD NEW DOCUMENT TO GOOGLE DRIVE
                            console.log("Error New Upload Google Drive BAR summary ", rCreate);
                            resolve({success: false, message: rCreate});
                          }
                        });
                      } else {
                        // UPDATE DOCUMENT GOOGLE DRIVE
                        var field: string; field = "";
                        for (var j in filesBar) {
                          var rFileBar = filesBar[j];
                          field = rFileBar.id
                        }
                        //UPDATE STATE RAW DATA DISTRIBUTE BAR A SUMMARY
                        ControllerConfig.onUploadPDFGDReplace(auth, field, FilePath, DocSummary.FilePath, DocSummary.FileName, async (rUp: any) => {
                          if (rUp.success == true) {
                            resolve({success: true, message: "success"});
                          } else {
                            // ERROR UPDATE UPLOAD NEW DOCUMENT TO GOOGLE DRIVE
                            console.log("Error Update Upload Google Drive BAR summary ", rUp);
                            resolve({success: false, message: rUp});
                          }
                        });
                      }
                    });
                  } else {
                    //DIRECTORY  BAR RECON SUMMARY UNTUK AD DIGOOGLE DRIVE BLUM ADA
                    console.log("Error Distribute AD A", "Directory AD A summary belum ada di google drive");
                    resolve({success: false, message: "Directory AD A summary belum ada di google drive"});
                  }
                });

                delete NewProcessShareBA[keyProcess];
                this.onExecuteDistributeDocBar();
              }

              // DOCUMENT BAR B
              if (Type == "AD_B") {

                await new Promise((resolve, reject) => {
                  // DOCUMENT BAR RECON SET
                  if (infoAdMaster.gd_bar_b != null && infoAdMaster.gd_bar_b != "") {
                    var FilePath = infoAdMaster.gd_bar_b;
                    ControllerConfig.onGD_Search(auth, "'" + FilePath + "' in parents and	name = '" + DocBar.FileName + "'", async (rSearchBar: any) => {
                      var filesBar = rSearchBar.data.data.files;
                      if (filesBar.length == 0) {
                        // NEW DOCUMENT GOOGLE DRIVE
                        ControllerConfig.onUploadPDFGD(auth, FilePath, DocBar.FilePath, DocBar.FileName, async (rCreate: any) => {
                          if (rCreate.success == true) {

                            //UPDATE STATE RAW DATA DISTRIBUTE BAR A
                            await this.dataDigiposRepository.updateById(DataRaw.digipost_id, {
                              google_drive_b: 1
                            });

                            // SEND MAIL NOTIF
                            var rawInfo = await this.dataDigiposRepository.findById(DataRaw.digipost_id);
                            if (infoAdMaster.email != "" && infoAdMaster.email != null) {
                              MailConfig.onSendMailDistributeDoc(infoAdMaster.email, {
                                type_doc: "BAR Recon AD B",
                                ad_name: infoAdMaster.ad_name,
                                name_doc: rawInfo.ba_attachment_b,
                                token_folder: FilePath
                              }, (r: any) => { });
                            }

                            // SEND NOTIFICATION
                            var userId = this.currentUserProfile[securityId];
                            var roleNotifSetADB = ControllerConfig.onGetRoleNotifGroup("0501010101");
                            var infoUser = await this.userRepository.findById(userId)
                            var dataMsg = {
                              types: "NOTIFICATION",
                              target: roleNotifSetADB.role,
                              to: DataRaw.ad_code,
                              from_who: infoUser.username,
                              subject: roleNotifSetADB.label,
                              body: "Distribute Document BAR Recon untuk AD  Nomor : " + DataRaw.ba_number_b + " sudah berhasil.",
                              path_url: "",
                              app: "DIGIPOS",
                              code: "0501010101",
                              code_label: "",
                              at_create: ControllerConfig.onCurrentTime().toString(),
                              at_read: "",
                              periode: DataRaw.trx_date_group,
                              at_flag: 0
                            }
                            await this.dataNotificationRepository.create(dataMsg);
                            ControllerConfig.onSendNotif(dataMsg);

                            console.log("SUCCESS, SHARE B ", DataRaw.ba_number_b);
                            resolve({success: true, message: "Success distribute BAR Recon B."});
                          } else {
                            // ERROR UPLOAD NEW DOCUMENT TO GOOGLE DRIVE
                            console.log("Error NEW Upload Google Drive BAR", rCreate);
                            resolve({success: false, message: rCreate});
                          }
                        });
                      } else {
                        // UPDATE DOCUMENT GOOGLE DRIVE
                        var field: string; field = "";
                        for (var j in filesBar) {
                          var rFileBar = filesBar[j];
                          field = rFileBar.id
                        }
                        ControllerConfig.onUploadPDFGDReplace(auth, field, FilePath, DocBar.FilePath, DocBar.FileName, async (rUp: any) => {
                          if (rUp.success == true) {
                            //UPDATE STATE RAW DATA DISTRIBUTE BAR A
                            await this.dataDigiposRepository.updateById(DataRaw.digipost_id, {
                              google_drive_b: 1
                            });

                            // SEND MAIL NOTIF

                            var rawInfo = await this.dataDigiposRepository.findById(DataRaw.digipost_id);
                            if (infoAdMaster.email != "" && infoAdMaster.email != null) {
                              MailConfig.onSendMailDistributeDoc(infoAdMaster.email, {
                                type_doc: "BAR Recon AD B",
                                ad_name: infoAdMaster.ad_name,
                                name_doc: rawInfo.ba_attachment_b,
                                token_folder: FilePath
                              }, (r: any) => { });
                            }

                            // SEND NOTIFICATION
                            var userId = this.currentUserProfile[securityId];
                            var roleNotifSetADB = ControllerConfig.onGetRoleNotifGroup("0501010101");
                            var infoUser = await this.userRepository.findById(userId)
                            var dataMsg = {
                              types: "NOTIFICATION",
                              target: roleNotifSetADB.role,
                              to: DataRaw.ad_code,
                              from_who: infoUser.username,
                              subject: roleNotifSetADB.label,
                              body: "Distribute Document BAR Recon untuk AD  Nomor : " + DataRaw.ba_number_b + " sudah berhasil.",
                              path_url: "",
                              app: "DIGIPOS",
                              code: "0501010101",
                              code_label: "",
                              at_create: ControllerConfig.onCurrentTime().toString(),
                              at_read: "",
                              periode: DataRaw.trx_date_group,
                              at_flag: 0
                            }
                            await this.dataNotificationRepository.create(dataMsg);
                            ControllerConfig.onSendNotif(dataMsg);

                            console.log("SUCCESS, SHARE B ", DataRaw.ba_number_b);
                            resolve({success: true, message: "Success distribute BAR Recon B."});
                          } else {
                            // ERROR UPDATE UPLOAD NEW DOCUMENT TO GOOGLE DRIVE
                            console.log("Error Update Upload Google Drive BAR", rUp);
                            resolve({success: false, message: rUp});
                          }
                        });
                      }
                    });
                  } else {
                    //DIRECTORY  BAR RECON UNTUK AD DIGOOGLE DRIVE BLUM ADA
                    console.log("Error Distribute AD A", "Directory AD A belum ada di google drive");
                    resolve({success: false, message: "Directory AD A belum ada di google drive"});
                  }
                });


                await new Promise((resolve, reject) => {
                  // DOCUMENT BAR RECON SUMMARY SET
                  if (infoAdMaster.gd_summary_b != null && infoAdMaster.gd_summary_b != "") {
                    var FilePath = infoAdMaster.gd_summary_b;
                    ControllerConfig.onGD_Search(auth, "'" + FilePath + "' in parents and	name = '" + DocSummary.FileName + "'", async (rSearchBar: any) => {
                      var filesBar = rSearchBar.data.data.files;
                      if (filesBar.length == 0) {
                        // NEW DOCUMENT GOOGLE DRIVE
                        ControllerConfig.onUploadPDFGD(auth, FilePath, DocSummary.FilePath, DocSummary.FileName, async (rCreate: any) => {
                          if (rCreate.success == true) {
                            resolve({success: true, message: "success"});
                          } else {
                            // ERROR UPDATE UPLOAD NEW DOCUMENT TO GOOGLE DRIVE
                            console.log("Error New Upload Google Drive BAR summary ", rCreate);
                            resolve({success: false, message: rCreate});
                          }
                        });
                      } else {
                        // UPDATE DOCUMENT GOOGLE DRIVE
                        var field: string; field = "";
                        for (var j in filesBar) {
                          var rFileBar = filesBar[j];
                          field = rFileBar.id
                        }
                        //UPDATE STATE RAW DATA DISTRIBUTE BAR A SUMMARY
                        ControllerConfig.onUploadPDFGDReplace(auth, field, FilePath, DocSummary.FilePath, DocSummary.FileName, async (rUp: any) => {
                          if (rUp.success == true) {
                            resolve({success: true, message: "success"});
                          } else {
                            // ERROR UPDATE UPLOAD NEW DOCUMENT TO GOOGLE DRIVE
                            console.log("Error Update Upload Google Drive BAR summary ", rUp);
                            resolve({success: false, message: rUp});
                          }
                        });
                      }
                    });
                  } else {
                    //DIRECTORY  BAR RECON SUMMARY UNTUK AD DIGOOGLE DRIVE BLUM ADA
                    console.log("Error Distribute AD B", "Directory AD B summary belum ada di google drive");
                    resolve({success: false, message: "Directory AD B summary belum ada di google drive"});
                  }
                });

                delete NewProcessShareBA[keyProcess];
                this.onExecuteDistributeDocBar();
              }

            } else {
              // JIKA AD CODE TIDAK DITEMUKAN
              console.log("Error Distribute BAR", "AD Code tidak ditemukan.");
              delete NewProcessShareBA[keyProcess];
              this.onExecuteDistributeDocBar();
            }

          } else {
            //JIKA TIDAK KONEK KE GOOGLE DRIVE
            console.log("Error Distribute BAR", "google drive not conected !");
            delete NewProcessShareBA[keyProcess];
            this.onExecuteDistributeDocBar();
          }
        });
      });

      noProcess++;
      break;
    }

    // JIKA TIDAK ADA YANG DIPERCESS
    if (noProcess == 0) {
      if (NewProcessShareBAPeriode.length > 0) {
        for (var iPeriode in NewProcessShareBAPeriode) {

          var rawPeriode = NewProcessShareBAPeriode[iPeriode];

          var countBarDistributA = await this.dataDigiposRepository.count({and: [{trx_date_group: rawPeriode}, {google_drive_a: 1}, {generate_file_a: 1}]});
          var countBarNotDistributA = await this.dataDigiposRepository.count({and: [{trx_date_group: rawPeriode}, {google_drive_a: 0}, {generate_file_a: 1}]});

          var countBarDistributB = await this.dataDigiposRepository.count({and: [{trx_date_group: rawPeriode}, {google_drive_b: 1}, {generate_file_b: 1}]});
          var countBarNotDistributB = await this.dataDigiposRepository.count({and: [{trx_date_group: rawPeriode}, {google_drive_b: 0}, {generate_file_b: 1}]});


          var userId = this.currentUserProfile[securityId];
          var roleNotifSetADB = ControllerConfig.onGetRoleNotifGroup("16010101");
          var infoUser = await this.userRepository.findById(userId)
          var dataMsg = {
            types: "NOTIFICATION",
            target: roleNotifSetADB.role,
            to: "",
            from_who: infoUser.username,
            subject: roleNotifSetADB.label,
            body: "Distribute Document BAR Recon untuk periode   : " + rawPeriode +
              " sudah berhasil. Jumlah yang sudah terdistribusi untuk BAR RECON A : " + countBarDistributA.count.toString() + " dan untuk BAR RECON B : " + countBarDistributB.count.toString() +
              " sedangkan Jumlah yang belum terdistribusi untuk BAR RECON A : " + countBarNotDistributA.count.toString() + " dan untuk BAR RECON B : " + countBarNotDistributB.count.toString(),
            path_url: "",
            app: "DIGIPOS",
            code: "16010101",
            code_label: "",
            at_create: ControllerConfig.onCurrentTime().toString(),
            at_read: "",
            periode: rawPeriode,
            at_flag: 0
          }
          await this.dataNotificationRepository.create(dataMsg);
          ControllerConfig.onSendNotif(dataMsg);

        }
      }

      NewProcessShareBAState = false;
      NewProcessShareBAAD = [];
      NewProcessShareBAPeriode = [];
      AdProcess = {};
      console.log("Prosess Distibute Selesai !");
    }

  }


  @get('/data-digipos/status-data-validasi-migrasi/{periode}', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(DataDigipos, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findStatusData(
    @param.path.string('periode') periode: string,
  ): Promise<Object> {
    var dataCountPeriode = await this.dataDigiposRepository.count({and: [{trx_date_group: periode}]});
    var DataNew = await this.dataDigiposRepository.count({and: [{trx_date_group: periode}, {ar_stat: 0}]});
    var DataValidBA = await this.dataDigiposRepository.count({and: [{trx_date_group: periode}, {or: [{ar_stat: 1}, {ar_stat: 2}]}]});
    var DataRejectBA = await this.dataDigiposRepository.count({and: [{trx_date_group: periode}, {ar_stat: -1}]});
    var DataValidBU = await this.dataDigiposRepository.count({and: [{trx_date_group: periode}, {ar_stat: 2}]});
    var DataRejectBU = await this.dataDigiposRepository.count({and: [{trx_date_group: periode}, {ar_stat: -2}]});
    var ValidasiBA = dataCountPeriode.count == DataValidBA.count ? 1 : 0;
    var ValidasiBU = dataCountPeriode.count == DataValidBU.count ? 1 : 0;

    var AdMater = await this.adMasterRepository.count({ad_status: false});

    var returnMs: any; returnMs = {
      NotValidAD: AdMater.count,
      DataNew: DataNew.count,
      DataValidBA: DataValidBA.count,
      DataRejectBA: DataRejectBA.count,
      DataValidBU: DataValidBU.count,
      DataRejectBU: DataRejectBU.count,
      ValidasiBA: DataValidBA.count != 0 ? ValidasiBA : 0,
      ValidasiBU: DataValidBU.count != 0 ? ValidasiBU : 0
    };
    return returnMs;
  }

  @post('/data-digipos', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {'application/json': {schema: getModelSchemaRef(DataDigipos)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(DataDigipos, {
            title: 'NewDataDigipos',
            exclude: ['digipost_id'],
          }),
        },
      },
    })
    dataDigipos: Omit<DataDigipos, 'digipost_id'>,
  ): Promise<DataDigipos> {
    return this.dataDigiposRepository.create(dataDigipos);
  }

  @get('/data-digipos/count', {
    responses: {
      '200': {
        description: 'DataDigipos model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(DataDigipos) where?: Where<DataDigipos>,
  ): Promise<Count> {
    return this.dataDigiposRepository.count(where);
  }

  @get('/data-digipos/process-share-ba', {
    responses: {
      '200': {
        description: 'DataDigipos model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async getProcessShareBA(
    @param.where(DataDigipos) where?: Where<DataDigipos>,
  ): Promise<Object> {
    var result: any; result = {type: 0, title: "", data: {}, count: 0};
    var adMaster = await this.adMasterRepository.count({ad_code: {inq: NewProcessShareBAAD}});
    // if (BarShareProcess == true) {
    if (NewProcessShareBAState == true) {
      var noProcessCheck = 0;
      for (var i in AdProcess) {noProcessCheck++;}
      // if (noProcessCheck != 0) {
      if (adMaster.count == noProcessCheck) {
        result.type = 2;
        result.title = "Process Share  BAR Recon To Goolge Drive..";
        var periodeProcess: any; periodeProcess = {};// {"A": [], "B": []};

        for (var iGD in NewProcessShareBA) {
          var rawGD = NewProcessShareBA[iGD];
          var rawData = await this.dataDigiposRepository.findById(rawGD.DataRaw.digipost_id);

          if (periodeProcess[rawData.trx_date_group] == undefined) {
            periodeProcess[rawData.trx_date_group] = {"A": [], "B": []};
          }

          if (rawGD.Type == "AD_A") {
            periodeProcess[rawData.trx_date_group]["A"].push(rawData.digipost_id);
          }

          if (rawGD.Type == "AD_B") {
            periodeProcess[rawData.trx_date_group]["B"].push(rawData.digipost_id);
          }
        }
        for (var pPeriode in periodeProcess) {
          var infoSharePeriode = periodeProcess[pPeriode];
          result.data[pPeriode] = {share_a: infoSharePeriode["A"].length, share_b: infoSharePeriode["B"].length};
        }
      } else {
        result.type = 1;
        result.title = "Checking Folder avaliable..";
        result.data = {total_process: adMaster.count, current_process: noProcessCheck};
      }
    }
    return result;
  }

  @get('/data-digipos/process-share-invoice', {
    responses: {
      '200': {
        description: 'DataDigipos model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async getProcessShareInvoice(
    @param.where(DataDigipos) where?: Where<DataDigipos>,
  ): Promise<Object> {
    var result: any; result = {type: 0, title: "", data: {}, count: 0};
    var noProcessInvoice = 0;
    for (var i in ProcessShareInvoice) {noProcessInvoice++;};
    if (noProcessInvoice != 0) {
      result.type = 1;
      result.title = "Process Share  Invoice To Goolge Drive..";
      for (var digipost_id in ProcessShareInvoice) {
        var digipos = await this.dataDigiposRepository.findById(parseInt(digipost_id));
        if (digipos != null) {
          if (result.data[digipos.trx_date_group] == undefined) {
            result.data[digipos.trx_date_group] = 0;
          }
          result.data[digipos.trx_date_group] = result.data[digipos.trx_date_group] + 1;
        }
      }
    }
    return result;
  }

  @post('/data-digipos/share-google-drive-ba-all', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: ShareDobBARequestBody
          }
        },
      },
    },
  })
  async shareGoogleDrive(
    @requestBody(ShareDobBARequestBody) shareDoc: {type_ad: string, periode: string},
  ): Promise<Object> {
    var result: any; result = {success: true, message: "success"};
    var AdSet: any; AdSet = [];



    // Untuk BAR RECON A
    if (shareDoc.type_ad == "AD_A") {
      var rawDataDoc = await this.dataDigiposRepository.find({where: {and: [{ar_stat: 2}, {generate_file_a: 1}, {sign_off_a: 1}, {google_drive_a: 0}, {trx_date_group: shareDoc.periode}]}});
      if (rawDataDoc.length > 0) {
        for (var i in rawDataDoc) {

          var infoRaw = rawDataDoc[i];
          var keyProcess = "AD_A_" + infoRaw.digipost_id;
          if (NewProcessShareBA[keyProcess] == undefined) {
            NewProcessShareBA[keyProcess] = {};
          } else {
            continue;
          }

          if (NewProcessShareBAAD.indexOf(infoRaw.ad_code) == -1) {
            NewProcessShareBAAD.push(infoRaw.ad_code);
          }

          if (NewProcessShareBAPeriode.indexOf(infoRaw.trx_date_group) == -1) {
            NewProcessShareBAPeriode.push(infoRaw.trx_date_group);
          }



          NewProcessShareBA[keyProcess] = {
            Type: shareDoc.type_ad,
            DataRaw: infoRaw,
            DOC_BAR: {
              FilePath: path.join(__dirname, '../../.digipost/berita_acara/ad_a/' + infoRaw.ba_attachment_a),
              FileName: infoRaw.ba_attachment_a,
            },
            DOC_SUMMARY: {
              FilePath: path.join(__dirname, '../../.digipost/berita_acara/ad_a/summary_' + infoRaw.ba_attachment_a),
              FileName: 'summary_' + infoRaw.ba_attachment_a,
            }
          }

        }
      } else {
        result.success = false;
        result.message = "No Data Document BAR Recon A can distribute !";
      }
    }

    // Untuk BAR RECON B
    if (shareDoc.type_ad == "AD_B") {
      var rawDataDoc = await this.dataDigiposRepository.find({where: {and: [{ar_stat: 2}, {generate_file_b: 1}, {sign_off_b: 1}, {google_drive_b: 0}, {trx_date_group: shareDoc.periode}]}});

      if (rawDataDoc.length > 0) {
        for (var i in rawDataDoc) {
          var infoRaw = rawDataDoc[i];
          var keyProcess = "AD_B_" + infoRaw.digipost_id;
          if (NewProcessShareBA[keyProcess] == undefined) {
            NewProcessShareBA[keyProcess] = {};
          } else {
            continue;
          }

          if (NewProcessShareBAAD.indexOf(infoRaw.ad_code) == -1) {
            NewProcessShareBAAD.push(infoRaw.ad_code);
          }

          if (NewProcessShareBAPeriode.indexOf(infoRaw.trx_date_group) == -1) {
            NewProcessShareBAPeriode.push(infoRaw.trx_date_group);
          }

          NewProcessShareBA[keyProcess] = {
            Type: shareDoc.type_ad,
            DataRaw: infoRaw,
            DOC_BAR: {
              FilePath: path.join(__dirname, '../../.digipost/berita_acara/ad_b/' + infoRaw.ba_attachment_b),
              FileName: infoRaw.ba_attachment_b
            },
            DOC_SUMMARY: {
              FilePath: path.join(__dirname, '../../.digipost/berita_acara/ad_b/summary_' + infoRaw.ba_attachment_b),
              FileName: 'summary_' + infoRaw.ba_attachment_b
            }
          }

        }
      } else {
        result.success = false;
        result.message = "No Data Document BAR Recon A can distribute !";
      }
    }

    if (NewProcessShareBAState === false) {
      if (NewProcessShareBAAD.length > 0) {
        NewProcessShareBAState = true;
        AdProcess = {};
        this.onExecuteDistributeGoogleDriveBA();
      } else {
        result.success = false;
        result.message = "No Data Document BAR Recon can distribute !";

        NewProcessShareBA = {};
        NewProcessShareBAState = false;
        NewProcessShareBAAD = [];
        NewProcessShareBAPeriode = [];
        AdProcess = {};
        console.log("No AdSet Distribute !");
      }
    }
    return result;
  }

  @post('/data-digipos/share-google-drive-invoice-all', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: ShareDocInvoiceRequestBody
          }
        },
      },
    },
  })
  async shareGoogleDriveInvoice(
    @requestBody(ShareDocInvoiceRequestBody) shareDoc: {periode: string},
  ): Promise<Object> {
    var infoInvoice = await this.dataDigiposRepository.find({where: {and: [{trx_date_group: shareDoc.periode}, {acc_file_invoice: 1}, {distribute_file_invoice: 0}]}});
    for (var i in infoInvoice) {
      var invoice = infoInvoice[i];
      var id = invoice.digipost_id == null ? "-1" : invoice.digipost_id;
      ProcessShareInvoice[id] = invoice;
    }
    this.onExecuteDistributeGoogleDriveInvoice();
    return {success: true, message: "success"};
  }

  @get('/data-digipos', {
    responses: {
      '200': {
        description: 'Array of DataDigipos model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(DataDigipos, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(DataDigipos) filter?: Filter<DataDigipos>,
  ): Promise<Object> {
    var info: any; info = {"datas": new Array(), "size": 0};
    var filterOrder = {};
    if (filter?.where !== undefined) {
      filterOrder = {"where": filter?.where};
    }

    var infoFileSyc = await this.dataDigiposRepository.find(filter);
    var sizeFileSyc = await this.dataDigiposRepository.find(filterOrder);

    var total_trx_cluster_out = 0;
    var total_mdr_fee_amount = 0;
    var total_dpp_fee_amount = 0;
    var total_vat_fee_amount = 0;
    var total_total_fee_amount = 0;
    var total_pph_fee_amount = 0;
    var total_trx_paid_invoice = 0;
    var total_paid_inv_amount = 0;
    var total_inv_adj_amount = 0;
    var total_dpp_paid_amount = 0;
    var total_vat_paid_amount = 0;
    var total_total_paid_amount = 0;
    var total_pph_paid_amount = 0;


    for (var i in sizeFileSyc) {
      var infoData = sizeFileSyc[i];
      total_trx_cluster_out = total_trx_cluster_out + infoData.trx_cluster_out;
      total_mdr_fee_amount = total_mdr_fee_amount + infoData.mdr_fee_amount;
      total_dpp_fee_amount = total_dpp_fee_amount + infoData.dpp_fee_amount;
      total_vat_fee_amount = total_vat_fee_amount + infoData.vat_fee_amount;
      total_total_fee_amount = total_total_fee_amount + infoData.total_fee_amount;
      total_pph_fee_amount = total_pph_fee_amount + infoData.pph_fee_amount;
      total_trx_paid_invoice = total_trx_paid_invoice + infoData.trx_paid_invoice;
      total_paid_inv_amount = total_paid_inv_amount + infoData.paid_inv_amount;
      total_inv_adj_amount = total_inv_adj_amount + infoData.inv_adj_amount;
      total_dpp_paid_amount = total_dpp_paid_amount + infoData.dpp_paid_amount;
      total_vat_paid_amount = total_vat_paid_amount + infoData.vat_paid_amount;
      total_total_paid_amount = total_total_paid_amount + infoData.total_paid_amount;
      total_pph_paid_amount = total_pph_paid_amount + infoData.pph_paid_amount;
    }

    var datas: any; datas = [];
    for (var j in infoFileSyc) {
      var data: any; data = {};
      var infoDigipos: any; infoDigipos = infoFileSyc[j];
      for (var m in infoDigipos) {
        data[m] = infoDigipos[m];
      }
      data["reason_tolak_ba"] = await this.dataDigiposRejectRepository.find({order: ["reject_id DESC"], where: {and: [{digipos_id: infoDigipos.digipost_id}, {reject_code: {like: '0002-%'}}]}}); //await this.dataDigiposRejectRepository.find({order: ["reject_id DESC"], limit: 1, where: {and: [{digipos_id: data.digipost_id}, {reject_code: {like: '0002-%'}}]}});
      data["reason_tolak_bu"] = await this.dataDigiposRejectRepository.find({order: ["reject_id DESC"], where: {and: [{digipos_id: infoDigipos.digipost_id}, {reject_code: {like: '0003-%'}}]}}); //infoDigipos.digipost_id; //await this.dataDigiposRejectRepository.find({order: ["reject_id DESC"], limit: 1, where: {and: [{digipos_id: data.digipos_id}, {reject_code: {like: '0003-%'}}]}});
      data["reason_tolak_sign"] = await this.dataDigiposRejectRepository.find({order: ["reject_id DESC"], where: {and: [{digipos_id: infoDigipos.digipost_id}, {reject_code: {like: '0006-%'}}]}}); //infoDigipos.digipost_id; //await this.dataDigiposRejectRepository.find({order: ["reject_id DESC"], limit: 1, where: {and: [{digipos_id: data.digipos_id}, {reject_code: {like: '0006-%'}}]}});
      data["reason_tolak_dispute"] = await this.dataDigiposRejectRepository.find({order: ["reject_id DESC"], where: {and: [{digipos_id: infoDigipos.digipost_id}, {reject_code: {like: '0007-%'}}]}}); //infoDigipos.digipost_id; //await this.dataDigiposRejectRepository.find({order: ["reject_id DESC"], limit: 1, where: {and: [{digipos_id: data.digipos_id}, {reject_code: {like: '0007-%'}}]}});
      data["reason_tolak_invoice"] = await this.dataDigiposRejectRepository.find({order: ["reject_id DESC"], where: {and: [{digipos_id: infoDigipos.digipost_id}, {reject_code: {like: '0012-%'}}]}}); //infoDigipos.digipost_id; //await this.dataDigiposRejectRepository.find({order: ["reject_id DESC"], limit: 1, where: {and: [{digipos_id: data.digipos_id}, {reject_code: {like: '0007-%'}}]}});
      data["reason_distribusi_invoice"] = [];
      data["reason_distribusi_BAR"] = []
      data["contract_number"] = "";

      var PortalAd = await this.adMasterRepository.findById(infoDigipos.ad_code);
      if (PortalAd.contract_number != "" && PortalAd.contract_number != null && PortalAd.contract_number != undefined) {
        data["contract_number"] = PortalAd.contract_number;
      }

      data["ad_info"] = {};
      if (PortalAd != null) {
        data["ad_info"] = PortalAd;
      }


      var digiposDetail = await this.dataDigiposDetailRepository.execute(
        "SELECT " +
        " type_trx," +
        " SUM(trx_cluster_out) AS trx_cluster_out," +
        " SUM(mdr_fee_amount) AS mdr_fee_amount," +
        " SUM(dpp_fee_amount) AS dpp_fee_amount," +
        " SUM(vat_fee_amount) AS vat_fee_amount," +
        " SUM(total_fee_amount) AS total_fee_amount," +
        " SUM(pph_fee_amount) AS pph_fee_amount," +
        " SUM(trx_paid_invoice) AS trx_paid_invoice," +
        " SUM(paid_inv_amount) AS paid_inv_amount," +
        " SUM(dpp_paid_amount) AS dpp_paid_amount," +
        " SUM(vat_paid_amount) AS vat_paid_amount," +
        " SUM(total_paid_amount) AS total_paid_amount," +
        " SUM(pph_paid_amount) AS pph_paid_amount," +
        " SUM(inv_adj_amount) AS inv_adj_amount " +
        "FROM DataDigiposDetail " +
        "WHERE digipost_id = '" + infoDigipos.digipost_id + "' " +
        "GROUP BY type_trx"
      );

      var info_trx: any; info_trx = [];
      for (var k in digiposDetail) {
        var infoDetailDigipos = digiposDetail[k];

        info_trx.push({
          type_trx: infoDetailDigipos.type_trx,
          trx_cluster_out: infoDetailDigipos.trx_cluster_out,
          mdr_fee_amount: infoDetailDigipos.mdr_fee_amount,
          dpp_fee_amount: infoDetailDigipos.dpp_fee_amount,
          vat_fee_amount: infoDetailDigipos.vat_fee_amount,
          total_fee_amount: infoDetailDigipos.total_fee_amount,
          pph_fee_amount: infoDetailDigipos.pph_fee_amount,
          trx_paid_invoice: infoDetailDigipos.trx_paid_invoice,
          paid_inv_amount: infoDetailDigipos.paid_inv_amount,
          dpp_paid_amount: infoDetailDigipos.dpp_paid_amount,
          vat_paid_amount: infoDetailDigipos.vat_paid_amount,
          total_paid_amount: infoDetailDigipos.total_paid_amount,
          pph_paid_amount: infoDetailDigipos.pph_paid_amount,
          inv_adj_amount: infoDetailDigipos.inv_adj_amount
        });
      }
      data["info_trx"] = info_trx;



      datas.push(data);
    }

    info["datas"] = datas;//infoFileSyc;
    info["size"] = sizeFileSyc.length;
    info["total_trx_cluster_out"] = total_trx_cluster_out;
    info["total_mdr_fee_amount"] = total_mdr_fee_amount;
    info["total_dpp_fee_amount"] = total_dpp_fee_amount;
    info["total_vat_fee_amount"] = total_vat_fee_amount;
    info["total_total_fee_amount"] = total_total_fee_amount;
    info["total_pph_fee_amount"] = total_pph_fee_amount;
    info["total_trx_paid_invoice"] = total_trx_paid_invoice;
    info["total_paid_inv_amount"] = total_paid_inv_amount;
    info["total_inv_adj_amount"] = total_inv_adj_amount;
    info["total_dpp_paid_amount"] = total_dpp_paid_amount;
    info["total_vat_paid_amount"] = total_vat_paid_amount;
    info["total_total_paid_amount"] = total_total_paid_amount;
    info["total_pph_paid_amount"] = total_pph_paid_amount;
    return info;
  }

  @patch('/data-digipos', {
    responses: {
      '200': {
        description: 'DataDigipos PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(DataDigipos, {partial: true}),
        },
      },
    })
    dataDigipos: DataDigipos,
    @param.where(DataDigipos) where?: Where<DataDigipos>,
  ): Promise<Count> {
    return this.dataDigiposRepository.updateAll(dataDigipos, where);
  }

  @get('/data-digipos/{id}', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(DataDigipos, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(DataDigipos, {exclude: 'where'}) filter?: FilterExcludingWhere<DataDigipos>
  ): Promise<DataDigipos> {
    return this.dataDigiposRepository.findById(id, filter);
  }

  @post('/data-digipos/update-validate-ba-ad-a', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: UpdateValidateRequestBody
          }
        },
      },
    },
  })
  async updateValidateBAADA(
    @requestBody(UpdateValidateRequestBody) updateValidate: {digipos: []},
  ): Promise<Object> {
    var result: any; result = {};
    try {
      var thisPeriode = "";
      var digipos = await this.dataDigiposRepository.updateAll({
        ar_valid_a: 1
      }, {and: [{digipost_id: {inq: updateValidate.digipos}}, {ar_stat: 0}]});
      var infoDigipos = await this.dataDigiposRepository.find({where: {and: [{digipost_id: {inq: updateValidate.digipos}}, {ar_stat: 0}]}});
      for (var i in infoDigipos) {
        var rawDigipos = infoDigipos[i];
        thisPeriode = rawDigipos.trx_date_group;
        if ((rawDigipos.ar_valid_a == 1 || rawDigipos.trx_cluster_out == 0) && (rawDigipos.ar_valid_b == 1 || rawDigipos.trx_paid_invoice == 0)) {
          await this.dataDigiposRepository.updateById(rawDigipos.digipost_id, {
            ar_stat: 1
          })
        }
      }

      var countValidata = await this.dataDigiposRepository.count({and: [{trx_date_group: thisPeriode}, {ar_valid_a: 1}]});
      // if (countNotValidata.count == 0) {
      var roleNotifSet = ControllerConfig.onGetRoleNotifGroup("01010101");
      var dataNotif = await this.dataNotificationRepository.findOne({where: {and: [{periode: thisPeriode}, {code: "01010101"}, {app: "DIGIPOS"}]}});
      if (dataNotif != null) {
        await this.dataNotificationRepository.updateById(dataNotif.id, {
          types: "NOTIFICATION",
          target: roleNotifSet.role,
          subject: roleNotifSet.label,
          body: "Jumlah Data Inquiry Review Summary AD A Tervalidasi BA Adalah : " + countValidata.count.toString() + " Data.",
          at_create: ControllerConfig.onCurrentTime().toString(),
          at_read: "",
          at_flag: 0
        });
      } else {
        await this.dataNotificationRepository.create({
          types: "NOTIFICATION",
          target: roleNotifSet.role,
          to: "",
          from_who: "SYSTTEM",
          subject: roleNotifSet.label,
          body: "Jumlah Data Inquiry Review Summary AD A Tervalidasi BA Adalah : " + countValidata.count.toString() + " Data.",
          path_url: "",
          app: "DIGIPOS",
          code: "01010101",
          code_label: "",
          at_create: ControllerConfig.onCurrentTime().toString(),
          at_read: "",
          periode: thisPeriode,
          at_flag: 0
        });
      }

      ControllerConfig.onSendNotif({
        types: "NOTIFICATION",
        target: roleNotifSet.role,
        to: "",
        from_who: "SYSTTEM",
        subject: roleNotifSet.label,
        body: "Jumlah Data Inquiry Review Summary AD A Tervalidasi BA Adalah : " + countValidata.count.toString() + " Data.",
        path_url: "",
        app: "DIGIPOS",
        code: "01010101",
        code_label: "",
        at_create: ControllerConfig.onCurrentTime().toString(),
        at_read: "",
        periode: thisPeriode,
        at_flag: 0
      });

      var countNotValidate = await this.dataDigiposRepository.count({and: [{trx_date_group: thisPeriode}, {ar_stat: {neq: 1}}]});
      if (countNotValidate.count == 0) {
        var roleNotifSetADA = ControllerConfig.onGetRoleNotifGroup("0101010101");
        var dataNotifADA = await this.dataNotificationRepository.findOne({where: {and: [{periode: thisPeriode}, {code: "0101010101"}, {app: "DIGIPOS"}]}});
        if (dataNotifADA != null) {
          await this.dataNotificationRepository.updateById(dataNotifADA.id, {
            types: "NOTIFICATION",
            target: roleNotifSetADA.role,
            subject: roleNotifSetADA.label,
            body: "Semua data inquiry review summary AD sudah di validasi BA.",
            at_create: ControllerConfig.onCurrentTime().toString(),
            at_read: "",
            at_flag: 0
          });
        } else {
          await this.dataNotificationRepository.create({
            types: "NOTIFICATION",
            target: roleNotifSetADA.role,
            to: "",
            from_who: "SYSTTEM",
            subject: roleNotifSetADA.label,
            body: "Semua data inquiry review summary AD sudah di validasi BA.",
            path_url: "",
            app: "DIGIPOS",
            code: "0101010101",
            code_label: "",
            at_create: ControllerConfig.onCurrentTime().toString(),
            at_read: "",
            periode: thisPeriode,
            at_flag: 0
          });
        }

        ControllerConfig.onSendNotif({
          types: "NOTIFICATION",
          target: roleNotifSetADA.role,
          to: "",
          from_who: "SYSTTEM",
          subject: roleNotifSetADA.label,
          body: "Semua data inquiry review summary AD sudah di validasi BA.",
          path_url: "",
          app: "DIGIPOS",
          code: "0101010101",
          code_label: "",
          at_create: ControllerConfig.onCurrentTime().toString(),
          at_read: "",
          periode: thisPeriode,
          at_flag: 0
        });
        // ControllerConfig.onSendMessageToSubscribe(dataMsg, (r: any) => {
        //   console.log(r);
        // });
        // }
      }
      result.success = true;
      result.message = "Success validasi AD A";
    } catch (error) {
      result.success = false;
      result.message = error;
    }
    return result;
  }

  @post('/data-digipos/update-validate-ba-ad-b', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: UpdateValidateRequestBody
          }
        },
      },
    },
  })
  async updateValidateBAADB(
    @requestBody(UpdateValidateRequestBody) updateValidate: {digipos: []},
  ): Promise<Object> {
    var result: any; result = {};
    try {
      var thisPeriode = "";
      await this.dataDigiposRepository.updateAll({
        ar_valid_b: 1
      }, {and: [{digipost_id: {inq: updateValidate.digipos}}, {ar_stat: 0}]});
      var infoDigipos = await this.dataDigiposRepository.find({where: {and: [{digipost_id: {inq: updateValidate.digipos}}, {ar_stat: 0}]}});
      for (var i in infoDigipos) {
        var rawDigipos = infoDigipos[i];
        thisPeriode = rawDigipos.trx_date_group;
        if ((rawDigipos.ar_valid_a == 1 || rawDigipos.trx_cluster_out == 0) && (rawDigipos.ar_valid_b == 1 || rawDigipos.trx_paid_invoice == 0)) {
          await this.dataDigiposRepository.updateById(rawDigipos.digipost_id, {
            ar_stat: 1
          })
        }
      }

      var countValidata = await this.dataDigiposRepository.count({and: [{trx_date_group: thisPeriode}, {ar_valid_b: 1}]});
      // if (countNotValidata.count == 0) {
      var roleNotifSet = ControllerConfig.onGetRoleNotifGroup("01010102");
      var dataNotif = await this.dataNotificationRepository.findOne({where: {and: [{periode: thisPeriode}, {code: "01010102"}, {app: "DIGIPOS"}]}});
      if (dataNotif != null) {
        await this.dataNotificationRepository.updateById(dataNotif.id, {
          types: "NOTIFICATION",
          target: roleNotifSet.role,
          subject: roleNotifSet.label,
          body: "Jumlah Data Inquiry Review Summary AD B Tervalidasi BA Adalah : " + countValidata.count.toString() + " Data.",
          at_create: ControllerConfig.onCurrentTime().toString(),
          at_read: "",
          at_flag: 0
        });
      } else {
        await this.dataNotificationRepository.create({
          types: "NOTIFICATION",
          target: roleNotifSet.role,
          to: "",
          from_who: "SYSTTEM",
          subject: roleNotifSet.label,
          body: "Jumlah Data Inquiry Review Summary AD B Tervalidasi BA Adalah : " + countValidata.count.toString() + " Data.",
          path_url: "",
          app: "DIGIPOS",
          code: "01010102",
          code_label: "",
          at_create: ControllerConfig.onCurrentTime().toString(),
          at_read: "",
          periode: thisPeriode,
          at_flag: 0
        });
      }

      ControllerConfig.onSendNotif({
        types: "NOTIFICATION",
        target: roleNotifSet.role,
        to: "",
        from_who: "SYSTTEM",
        subject: roleNotifSet.label,
        body: "Jumlah Data Inquiry Review Summary AD B Tervalidasi BA Adalah : " + countValidata.count.toString() + " Data.",
        path_url: "",
        app: "DIGIPOS",
        code: "01010102",
        code_label: "",
        at_create: ControllerConfig.onCurrentTime().toString(),
        at_read: "",
        periode: thisPeriode,
        at_flag: 0
      });

      var countNotValidata = await this.dataDigiposRepository.count({and: [{trx_date_group: thisPeriode}, {ar_stat: {neq: 1}}]});
      if (countNotValidata.count == 0) {

        var roleNotifSetADA = ControllerConfig.onGetRoleNotifGroup("0101010101");
        var dataNotifADA = await this.dataNotificationRepository.findOne({where: {and: [{periode: thisPeriode}, {code: "0101010101"}, {app: "DIGIPOS"}]}});
        if (dataNotifADA != null) {
          await this.dataNotificationRepository.updateById(dataNotifADA.id, {
            types: "NOTIFICATION",
            target: roleNotifSetADA.role,
            subject: roleNotifSetADA.label,
            body: "Semua data inquiry review summary AD sudah di validasi BA.",
            at_create: ControllerConfig.onCurrentTime().toString(),
            at_read: "",
            at_flag: 0
          });
        } else {
          await this.dataNotificationRepository.create({
            types: "NOTIFICATION",
            target: roleNotifSetADA.role,
            to: "",
            from_who: "SYSTTEM",
            subject: roleNotifSetADA.label,
            body: "Semua data inquiry review summary AD sudah di validasi BA.",
            path_url: "",
            app: "DIGIPOS",
            code: "0101010101",
            code_label: "",
            at_create: ControllerConfig.onCurrentTime().toString(),
            at_read: "",
            periode: thisPeriode,
            at_flag: 0
          });
        }

        ControllerConfig.onSendNotif({
          types: "NOTIFICATION",
          target: roleNotifSetADA.role,
          to: "",
          from_who: "SYSTTEM",
          subject: roleNotifSetADA.label,
          body: "Semua data inquiry review summary AD sudah di validasi BA.",
          path_url: "",
          app: "DIGIPOS",
          code: "0101010101",
          code_label: "",
          at_create: ControllerConfig.onCurrentTime().toString(),
          at_read: "",
          periode: thisPeriode,
          at_flag: 0
        });
        // ControllerConfig.onSendMessageToSubscribe(dataMsg, (r: any) => {
        //   console.log(r);
        // });
      }

      result.success = true;
      result.message = "Success validasi AD B";
    } catch (error) {
      result.success = false;
      result.message = error;
    }
    return result;
  }


  @post('/data-digipos/update-validate-bu-ad-a', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: UpdateValidateRequestBody
          }
        },
      },
    },
  })
  async updateValidateBUADA(
    @requestBody(UpdateValidateRequestBody) updateValidate: {digipos: []},
  ): Promise<Object> {
    var result: any; result = {};
    try {
      var thisPeriode = "";
      var digipos = await this.dataDigiposRepository.updateAll({
        ar_valid_a: 2
      }, {and: [{digipost_id: {inq: updateValidate.digipos}}, {ar_stat: 1}]});
      var infoDigipos = await this.dataDigiposRepository.find({where: {and: [{digipost_id: {inq: updateValidate.digipos}}, {ar_stat: 1}]}});
      for (var i in infoDigipos) {
        var rawDigipos = infoDigipos[i];
        thisPeriode = rawDigipos.trx_date_group;
        if ((rawDigipos.ar_valid_a == 2 || rawDigipos.trx_cluster_out == 0) && (rawDigipos.ar_valid_b == 2 || rawDigipos.trx_paid_invoice == 0)) {
          await this.dataDigiposRepository.updateById(rawDigipos.digipost_id, {
            ar_stat: 2,
          })
        }
      }


      var countValidata = await this.dataDigiposRepository.count({and: [{trx_date_group: thisPeriode}, {ar_valid_a: 2}]});
      // if (countNotValidata.count == 0) {
      var roleNotifSet = ControllerConfig.onGetRoleNotifGroup("02020101");
      var dataNotif = await this.dataNotificationRepository.findOne({where: {and: [{periode: thisPeriode}, {code: "02020101"}, {app: "DIGIPOS"}]}});
      if (dataNotif != null) {
        await this.dataNotificationRepository.updateById(dataNotif.id, {
          types: "NOTIFICATION",
          target: roleNotifSet.role,
          subject: roleNotifSet.label,
          body: "Jumlah Data Inquiry Review Summary AD A Tervalidasi BU Adalah : " + countValidata.count.toString() + " Data.",
          at_create: ControllerConfig.onCurrentTime().toString(),
          at_read: "",
          at_flag: 0
        });
      } else {
        await this.dataNotificationRepository.create({
          types: "NOTIFICATION",
          target: roleNotifSet.role,
          to: "",
          from_who: "SYSTTEM",
          subject: roleNotifSet.label,
          body: "Jumlah Data Inquiry Review Summary AD A Tervalidasi BU Adalah : " + countValidata.count.toString() + " Data.",
          path_url: "",
          app: "DIGIPOS",
          code: "02020101",
          code_label: "",
          at_create: ControllerConfig.onCurrentTime().toString(),
          at_read: "",
          periode: thisPeriode,
          at_flag: 0
        });
      }

      ControllerConfig.onSendNotif({
        types: "NOTIFICATION",
        target: roleNotifSet.role,
        to: "",
        from_who: "SYSTTEM",
        subject: roleNotifSet.label,
        body: "Jumlah Data Inquiry Review Summary AD A Tervalidasi BU Adalah : " + countValidata.count.toString() + " Data.",
        path_url: "",
        app: "DIGIPOS",
        code: "02020101",
        code_label: "",
        at_create: ControllerConfig.onCurrentTime().toString(),
        at_read: "",
        periode: thisPeriode,
        at_flag: 0
      });

      var countNotValidata = await this.dataDigiposRepository.count({and: [{trx_date_group: thisPeriode}, {ar_stat: {neq: 2}}]});
      if (countNotValidata.count == 0) {
        var roleNotifSetADB = ControllerConfig.onGetRoleNotifGroup("0101010102");
        var dataNotifADB = await this.dataNotificationRepository.findOne({where: {and: [{periode: thisPeriode}, {code: "0101010102"}, {app: "DIGIPOS"}]}});
        if (dataNotifADB != null) {
          await this.dataNotificationRepository.updateById(dataNotifADB.id, {
            types: "NOTIFICATION",
            target: roleNotifSetADB.role,
            subject: roleNotifSetADB.label,
            body: "Semua data inquiry review summary AD sudah di validasi BU.",
            at_create: ControllerConfig.onCurrentTime().toString(),
            at_read: "",
            at_flag: 0
          });
        } else {
          await this.dataNotificationRepository.create({
            types: "NOTIFICATION",
            target: roleNotifSetADB.role,
            to: "",
            from_who: "SYSTTEM",
            subject: roleNotifSetADB.label,
            body: "Semua data inquiry review summary AD sudah di validasi BU.",
            path_url: "",
            app: "DIGIPOS",
            code: "0101010102",
            code_label: "",
            at_create: ControllerConfig.onCurrentTime().toString(),
            at_read: "",
            periode: thisPeriode,
            at_flag: 0
          });
        }

        ControllerConfig.onSendNotif({
          types: "NOTIFICATION",
          target: roleNotifSetADB.role,
          to: "",
          from_who: "SYSTTEM",
          subject: roleNotifSetADB.label,
          body: "Semua data inquiry review summary AD sudah di validasi BU.",
          path_url: "",
          app: "DIGIPOS",
          code: "0101010102",
          code_label: "",
          at_create: ControllerConfig.onCurrentTime().toString(),
          at_read: "",
          periode: thisPeriode,
          at_flag: 0
        });
        // ControllerConfig.onSendMessageToSubscribe(dataMsg, (r: any) => {
        //   console.log(r);
        // });
      }

      result.success = true;
      result.message = "Success validasi AD A";
    } catch (error) {
      result.success = false;
      result.message = error;
    }
    return result;
  }

  @post('/data-digipos/update-validate-bu-ad-b', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: UpdateValidateRequestBody
          }
        },
      },
    },
  })
  async updateValidateBUADB(
    @requestBody(UpdateValidateRequestBody) updateValidate: {digipos: []},
  ): Promise<Object> {
    var result: any; result = {};
    try {
      var thisPeriode = "";
      await this.dataDigiposRepository.updateAll({
        ar_valid_b: 2
      }, {and: [{digipost_id: {inq: updateValidate.digipos}}, {ar_stat: 1}]});
      var infoDigipos = await this.dataDigiposRepository.find({where: {and: [{digipost_id: {inq: updateValidate.digipos}}, {ar_stat: 1}]}});
      for (var i in infoDigipos) {
        var rawDigipos = infoDigipos[i];
        thisPeriode = rawDigipos.trx_date_group;
        if ((rawDigipos.ar_valid_a == 2 || rawDigipos.trx_cluster_out == 0) && (rawDigipos.ar_valid_b == 2 || rawDigipos.trx_paid_invoice == 0)) {
          await this.dataDigiposRepository.updateById(rawDigipos.digipost_id, {
            ar_stat: 2
          })
        }
      }

      var countValidata = await this.dataDigiposRepository.count({and: [{trx_date_group: thisPeriode}, {ar_valid_b: 2}]});
      // if (countNotValidata.count == 0) {
      var roleNotifSet = ControllerConfig.onGetRoleNotifGroup("02020102");
      var dataNotif = await this.dataNotificationRepository.findOne({where: {and: [{periode: thisPeriode}, {code: "02020102"}, {app: "DIGIPOS"}]}});
      if (dataNotif != null) {
        await this.dataNotificationRepository.updateById(dataNotif.id, {
          types: "NOTIFICATION",
          target: roleNotifSet.role,
          subject: roleNotifSet.label,
          body: "Jumlah Data Inquiry Review Summary AD B Tervalidasi BU Adalah : " + countValidata.count.toString() + " Data.",
          at_create: ControllerConfig.onCurrentTime().toString(),
          at_read: "",
          at_flag: 0
        });
      } else {
        await this.dataNotificationRepository.create({
          types: "NOTIFICATION",
          target: roleNotifSet.role,
          to: "",
          from_who: "SYSTTEM",
          subject: roleNotifSet.label,
          body: "Jumlah Data Inquiry Review Summary AD A Tervalidasi BU Adalah : " + countValidata.count.toString() + " Data.",
          path_url: "",
          app: "DIGIPOS",
          code: "02020102",
          code_label: "",
          at_create: ControllerConfig.onCurrentTime().toString(),
          at_read: "",
          periode: thisPeriode,
          at_flag: 0
        });
      }


      ControllerConfig.onSendNotif({
        types: "NOTIFICATION",
        target: roleNotifSet.role,
        to: "",
        from_who: "SYSTTEM",
        subject: roleNotifSet.label,
        body: "Jumlah Data Inquiry Review Summary AD A Tervalidasi BU Adalah : " + countValidata.count.toString() + " Data.",
        path_url: "",
        app: "DIGIPOS",
        code: "02020102",
        code_label: "",
        at_create: ControllerConfig.onCurrentTime().toString(),
        at_read: "",
        periode: thisPeriode,
        at_flag: 0
      });

      var countNotValidata = await this.dataDigiposRepository.count({and: [{trx_date_group: thisPeriode}, {ar_stat: {neq: 2}}]});
      if (countNotValidata.count == 0) {
        var roleNotifSetADB = ControllerConfig.onGetRoleNotifGroup("0101010102");
        var dataNotifADB = await this.dataNotificationRepository.findOne({where: {and: [{periode: thisPeriode}, {code: "0101010102"}, {app: "DIGIPOS"}]}});
        if (dataNotifADB != null) {
          await this.dataNotificationRepository.updateById(dataNotifADB.id, {
            types: "NOTIFICATION",
            target: roleNotifSetADB.role,
            subject: roleNotifSetADB.label,
            body: "Semua data inquiry review summary AD sudah di validasi BU.",
            at_create: ControllerConfig.onCurrentTime().toString(),
            at_read: "",
            at_flag: 0
          });
        } else {
          await this.dataNotificationRepository.create({
            types: "NOTIFICATION",
            target: roleNotifSetADB.role,
            to: "",
            from_who: "SYSTTEM",
            subject: roleNotifSetADB.label,
            body: "Semua data inquiry review summary AD sudah di validasi BU.",
            path_url: "",
            app: "DIGIPOS",
            code: "0101010102",
            code_label: "",
            at_create: ControllerConfig.onCurrentTime().toString(),
            at_read: "",
            periode: thisPeriode,
            at_flag: 0
          });
        }

        ControllerConfig.onSendNotif({
          types: "NOTIFICATION",
          target: roleNotifSetADB.role,
          to: "",
          from_who: "SYSTTEM",
          subject: roleNotifSetADB.label,
          body: "Semua data inquiry review summary AD sudah di validasi BU.",
          path_url: "",
          app: "DIGIPOS",
          code: "0101010102",
          code_label: "",
          at_create: ControllerConfig.onCurrentTime().toString(),
          at_read: "",
          periode: thisPeriode,
          at_flag: 0
        });
        // ControllerConfig.onSendMessageToSubscribe(dataMsg, (r: any) => {
        //   console.log(r);
        // });
      }

      result.success = true;
      result.message = "Success validasi AD B";
    } catch (error) {
      result.success = false;
      result.message = error;
    }
    return result;
  }


  @post('/data-digipos/update-validate-ba', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: UpdateValidateRequestBody
          }
        },
      },
    },
  })
  async updateValidateBa(
    @requestBody(UpdateValidateRequestBody) updateValidate: {digipos: []},
  ): Promise<Object> {
    await this.dataDigiposRepository.updateAll({
      ar_stat: 1
    }, {and: [{digipost_id: {inq: updateValidate.digipos}}, {ar_stat: 0}]});
    var dataupdate = await this.dataDigiposRepository.count({and: [{digipost_id: {inq: updateValidate.digipos}}, {ar_stat: 1}]});
    var dataDigipos = await this.dataDigiposRepository.find({where: {and: [{digipost_id: {inq: updateValidate.digipos}}, {ar_stat: 1}]}});
    if (dataDigipos.length > 0) {
      var periode: string; periode = "";
      for (var i in dataDigipos) {
        periode = dataDigipos[i].trx_date_group;
        break;
      }
      var countNotValidata = await this.dataDigiposRepository.count({and: [{trx_date_group: periode}, {ar_stat: {neq: 1}}]});
      if (countNotValidata.count == 0) {
        var messageFb = "Data  Digipos periode " + periode + " telah divalidasi oleh bisnis anssurance, tolong bisnis user segera lakukan pengecekan, terima kasih !";
        var dataMsg = {
          title: "Data Digipos Validasi Bisnis Anssurance",
          message: messageFb,
          type: "DIGIPOS_VALIDASI_DATA_BA",
          to_role: ControllerConfig.getRoleIdMessage("DIGIPOS_VALIDASI_DATA_BA"),
          to_user: "",
          at_create: ControllerConfig.onCurrentTime().toString(),
          status: "0",
          id_target: "",
          for_app: "DIGIPOS",
          periode: periode
        };
        await this.dataNotificationRepository.create(dataMsg);
        ControllerConfig.onSendNotif(dataMsg);
        // ControllerConfig.onSendMessageToSubscribe(dataMsg, (r: any) => {
        //   console.log(r);
        // });
      }
    }


    return {success: true, updateCount: dataupdate};
  }

  // @post('/data-digipos/update-validate-invoice', {
  //   responses: {
  //     '200': {
  //       description: 'DataDigipos model instance',
  //       content: {
  //         'application/json': {
  //           schema: ValidateSignRequestBody
  //         }
  //       },
  //     },
  //   },
  // })
  // async updateValidateInvoice(
  //   @requestBody(UpdateValidateRequestBody) updateValidateInvoice: {type_ad: string, digipos: []},
  // ): Promise<Object> {
  //   await this.dataDigiposRepository.updateAll({
  //     acc_file_invoice: 1
  //   }, {and: [{digipost_id: {inq: updateValidateInvoice.digipos}}, {ar_stat: 2}, {generate_file_invoice: 1}]});
  //   var dataupdate = await this.dataDigiposRepository.count({and: [{digipost_id: {inq: updateValidateInvoice.digipos}}, {ar_stat: 2}, {acc_file_invoice: 1}]});
  //   if (dataupdate.count == 0) {
  //     // ControllerConfig.onSendNotif("/update-validate-invoice");
  //     // ControllerConfig.
  //   }
  //   return {success: true, updateCount: dataupdate};
  // }


  @post('/data-digipos/update-validate-bu', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: UpdateValidateRequestBody
          }
        },
      },
    },
  })
  async updateValidateBu(
    @requestBody(UpdateValidateRequestBody) updateValidate: {digipos: []},
  ): Promise<Object> {
    await this.dataDigiposRepository.updateAll({
      ar_stat: 2
    }, {and: [{digipost_id: {inq: updateValidate.digipos}}, {ar_stat: 1}]});
    var dataupdate = await this.dataDigiposRepository.count({and: [{digipost_id: {inq: updateValidate.digipos}}, {ar_stat: 2}]});
    var dataDigipos = await this.dataDigiposRepository.find({where: {and: [{digipost_id: {inq: updateValidate.digipos}}, {ar_stat: 2}]}});
    if (dataDigipos.length > 0) {
      var periode: string; periode = "";
      for (var i in dataDigipos) {
        periode = dataDigipos[i].trx_date_group;
        break;
      }

      var countNotValidata = await this.dataDigiposRepository.count({and: [{trx_date_group: periode}, {ar_stat: {neq: 2}}]});
      if (countNotValidata.count == 0) {
        // var messageFb = "Data  Digipos periode " + periode + " telah divalidasi oleh bisnis user, tolong bisnis anssurance segera lakukan pembuatan BAR Recon, terima kasih !";
        // var dataMsg = {
        //   title: "Data Digipos Validasi Bisnis User",
        //   message: messageFb,
        //   type: "DIGIPOS_VALIDASI_DATA_BU",
        //   to_role: ControllerConfig.getRoleIdMessage("DIGIPOS_VALIDASI_DATA_BU"),
        //   to_user: "",
        //   at_create: ControllerConfig.onCurrentTime().toString(),
        //   status: "0",
        //   id_target: "",
        //   for_app: "DIGIPOS",
        //   periode: periode
        // };
        // await this.dataNotificationRepository.create(dataMsg);
        // ControllerConfig.onSendNotif(dataMsg);
        // ControllerConfig.onSendMessageToSubscribe(dataMsg, (r: any) => {
        //   console.log(r);
        // });
      }
    }

    return {success: true, updateCount: dataupdate};
  }

  @post('/data-digipos/update-reject-ba-ad-a', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: RejectValidateRequestBody
          }
        },
      },
    },
  })
  async RejetBaADA(
    @requestBody(RejectValidateRequestBody) rejectBa: {
      reason_id: number,
      digipos: [],
      description: string
    },
  ): Promise<Object> {
    var returnMs: any; returnMs = {success: true, message: ""};

    var today = new Date();
    var digiposData = await this.dataDigiposRepository.find({where: {and: [{digipost_id: {inq: rejectBa.digipos}}, {ar_stat: 0}]}});

    for (var i in digiposData) {
      var infoDigipost = digiposData[i];
      var config = await this.dataconfigRepository.findOne({where: {and: [{id: rejectBa.reason_id}, {config_group_id: 2}]}});
      if (config != null) {
        await this.dataDigiposRejectRepository.create({
          periode: infoDigipost.trx_date_group,
          ad_code: infoDigipost.ad_code,
          ad_name: infoDigipost.ad_name,
          config_id: rejectBa.reason_id,
          reject_code: config?.config_code,
          reject_label: "REJECT BA AD A|" + config?.config_desc,
          reject_description: "REJECT BY|BA|AD_A|" + rejectBa.description,
          create_at: today.toDateString(),
          user_reject: this.currentUserProfile[securityId],
          digipos_id: infoDigipost.digipost_id
        });
        await this.dataDigiposRepository.updateById(infoDigipost.digipost_id, {
          ar_valid_a: -1,
          ar_stat: -1
        });

        // var thisDataDigipos = await this.dataDigiposRepository.findById(infoDigipost.digipost_id);
        // if (thisDataDigipos.digipost_id != undefined) {
        //   if (thisDataDigipos.ar_valid_a == -1 && thisDataDigipos.ar_valid_b == -1) {
        //     await this.dataDigiposRepository.updateById(thisDataDigipos.digipost_id, {
        //       ar_stat: -1
        //     });
        //   }
        // }

      } else {
        returnMs.success = false;
        returnMs.message = "Reason not found, please entry other reason !";
      }
    }

    if (digiposData.length == 0) {
      returnMs.success = false;
      returnMs.message = "No data digipos rejected !";
    } else {
      if (returnMs.success == true) {

        var dataDigipos = await this.dataDigiposRepository.findOne({where: {and: [{digipost_id: {inq: rejectBa.digipos}}]}});
        var userId = this.currentUserProfile[securityId];
        var roleNotifSetADB = ControllerConfig.onGetRoleNotifGroup("01010201");
        var infoUser = await this.userRepository.findById(userId)
        await this.dataNotificationRepository.create({
          types: "NOTIFICATION",
          target: roleNotifSetADB.role,
          to: "",
          from_who: infoUser.username,
          subject: roleNotifSetADB.label,
          body: "Data inquiry review summary AD A sudah di tolak oleh BA berjumlah : " + rejectBa.digipos.length,
          path_url: "",
          app: "DIGIPOS",
          code: "01010201",
          code_label: "",
          at_create: ControllerConfig.onCurrentTime().toString(),
          at_read: "",
          periode: dataDigipos?.trx_date_group,
          at_flag: 0
        });

        ControllerConfig.onSendNotif({
          types: "NOTIFICATION",
          target: roleNotifSetADB.role,
          to: "",
          from_who: infoUser.username,
          subject: roleNotifSetADB.label,
          body: "Data inquiry review summary AD A sudah di tolak oleh BA berjumlah : " + rejectBa.digipos.length,
          path_url: "",
          app: "DIGIPOS",
          code: "01010201",
          code_label: "",
          at_create: ControllerConfig.onCurrentTime().toString(),
          at_read: "",
          periode: dataDigipos?.trx_date_group,
          at_flag: 0
        });

        returnMs.message = "success update [" + digiposData.length.toString() + "] data digipos !";
      }
    }
    return returnMs;
  }

  @post('/data-digipos/update-reject-ba-ad-b', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: RejectValidateRequestBody
          }
        },
      },
    },
  })
  async RejetBaADB(
    @requestBody(RejectValidateRequestBody) rejectBa: {
      reason_id: number,
      digipos: [],
      description: string
    },
  ): Promise<Object> {
    var returnMs: any; returnMs = {success: true, message: ""};

    var today = new Date();
    var digiposData = await this.dataDigiposRepository.find({where: {and: [{digipost_id: {inq: rejectBa.digipos}}, {ar_stat: 0}]}});

    for (var i in digiposData) {
      var infoDigipost = digiposData[i];
      var config = await this.dataconfigRepository.findOne({where: {and: [{id: rejectBa.reason_id}, {config_group_id: 2}]}});
      if (config != null) {
        await this.dataDigiposRejectRepository.create({
          periode: infoDigipost.trx_date_group,
          ad_code: infoDigipost.ad_code,
          ad_name: infoDigipost.ad_name,
          config_id: rejectBa.reason_id,
          reject_code: config?.config_code,
          reject_label: "REJECT BA AD A|" + config?.config_desc,
          reject_description: "REJECT BY|BA|AD_B|" + rejectBa.description,
          create_at: today.toDateString(),
          user_reject: this.currentUserProfile[securityId],
          digipos_id: infoDigipost.digipost_id
        });
        await this.dataDigiposRepository.updateById(infoDigipost.digipost_id, {
          ar_valid_b: -1,
          ar_stat: -1
        });

        // var thisDataDigipos = await this.dataDigiposRepository.findById(infoDigipost.digipost_id);
        // if (thisDataDigipos.digipost_id != undefined) {
        //   if (thisDataDigipos.ar_valid_a == -1 && thisDataDigipos.ar_valid_b == -1) {
        //     await this.dataDigiposRepository.updateById(thisDataDigipos.digipost_id, {
        //       ar_stat: -1
        //     });
        //   }
        // }

      } else {
        returnMs.success = false;
        returnMs.message = "Reason not found, please entry other reason !";
      }
    }

    if (digiposData.length == 0) {
      returnMs.success = false;
      returnMs.message = "No data digipos rejected !";
    } else {
      if (returnMs.success == true) {
        var dataDigipos = await this.dataDigiposRepository.findOne({where: {and: [{digipost_id: {inq: rejectBa.digipos}}]}});
        var userId = this.currentUserProfile[securityId];
        var roleNotifSetADB = ControllerConfig.onGetRoleNotifGroup("01010202");
        var infoUser = await this.userRepository.findById(userId)
        await this.dataNotificationRepository.create({
          types: "NOTIFICATION",
          target: roleNotifSetADB.role,
          to: "",
          from_who: infoUser.username,
          subject: roleNotifSetADB.label,
          body: "Data inquiry review summary AD B sudah di tolak oleh BA berjumlah : " + rejectBa.digipos.length,
          path_url: "",
          app: "DIGIPOS",
          code: "01010202",
          code_label: "",
          at_create: ControllerConfig.onCurrentTime().toString(),
          at_read: "",
          periode: dataDigipos?.trx_date_group,
          at_flag: 0
        });

        ControllerConfig.onSendNotif({
          types: "NOTIFICATION",
          target: roleNotifSetADB.role,
          to: "",
          from_who: infoUser.username,
          subject: roleNotifSetADB.label,
          body: "Data inquiry review summary AD B sudah di tolak oleh BA berjumlah : " + rejectBa.digipos.length,
          path_url: "",
          app: "DIGIPOS",
          code: "01010202",
          code_label: "",
          at_create: ControllerConfig.onCurrentTime().toString(),
          at_read: "",
          periode: dataDigipos?.trx_date_group,
          at_flag: 0
        });

        returnMs.message = "success update [" + digiposData.length.toString() + "] data digipos !";
      }
    }
    return returnMs;
  }

  @post('/data-digipos/update-reject-ba', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: RejectValidateRequestBody
          }
        },
      },
    },
  })
  async RejetBa(
    @requestBody(RejectValidateRequestBody) rejectBa: {
      reason_id: number,
      digipos: [],
      description: string
    },
  ): Promise<Object> {
    var returnMs: any; returnMs = {success: true, message: ""};

    var today = new Date();
    var digiposData = await this.dataDigiposRepository.find({where: {and: [{digipost_id: {inq: rejectBa.digipos}}, {ar_stat: 0}]}});

    for (var i in digiposData) {
      var infoDigipost = digiposData[i];
      var config = await this.dataconfigRepository.findOne({where: {and: [{id: rejectBa.reason_id}, {config_group_id: 2}]}});
      if (config != null) {
        await this.dataDigiposRejectRepository.create({
          periode: infoDigipost.trx_date_group,
          ad_code: infoDigipost.ad_code,
          ad_name: infoDigipost.ad_name,
          config_id: rejectBa.reason_id,
          reject_code: config?.config_code,
          reject_label: config?.config_desc,
          reject_description: "REJECT BY|BA|" + rejectBa.description,
          create_at: today.toDateString(),
          user_reject: this.currentUserProfile[securityId],
          digipos_id: infoDigipost.digipost_id
        });
        await this.dataDigiposRepository.updateById(infoDigipost.digipost_id, {
          ar_stat: -1
        });
      } else {
        returnMs.success = false;
        returnMs.message = "Reason not found, please entry other reason !";
      }
    }
    if (digiposData.length == 0) {
      returnMs.success = false;
      returnMs.message = "No data digipos rejected !";
    } else {
      if (returnMs.success == true) {
        var dataDigipos = await this.dataDigiposRepository.findOne({where: {and: [{digipost_id: {inq: rejectBa.digipos}}]}});
        // var messageFb = "Data  Digipos periode " + dataDigipos?.trx_date_group + " telah di tolak oleh bisnis anssurance, tolong segera lakukan upload data ulang !";
        // var dataMsg = {
        //   title: "Data Digipos Reject Bisnis Anssurance",
        //   message: messageFb,
        //   type: "DIGIPOS_REJECT_DATA_BA",
        //   to_role: ControllerConfig.getRoleIdMessage("DIGIPOS_REJECT_DATA_BA"),
        //   to_user: "",
        //   at_create: ControllerConfig.onCurrentTime().toString(),
        //   status: "0",
        //   id_target: "",
        //   for_app: "DIGIPOS",
        //   periode: dataDigipos?.trx_date_group
        // };
        // await this.dataNotificationRepository.create(dataMsg);
        // ControllerConfig.onSendMessageToSubscribe(dataMsg, (r: any) => {
        //   console.log(r);
        // });
        returnMs.message = "success update [" + digiposData.length.toString() + "] data digipos !";
      }
    }
    return returnMs;
  }


  @post('/data-digipos/update-reject-bu-ad-a', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: RejectValidateRequestBody
          }
        },
      },
    },
  })
  async RejetBuADA(
    @requestBody(RejectValidateRequestBody) rejectBa: {
      reason_id: number,
      digipos: [],
      description: string
    },
  ): Promise<Object> {
    var returnMs: any; returnMs = {success: true, message: ""};

    var today = new Date();
    var digiposData = await this.dataDigiposRepository.find({where: {and: [{digipost_id: {inq: rejectBa.digipos}}, {ar_stat: 1}]}});

    for (var i in digiposData) {
      var infoDigipost = digiposData[i];
      var config = await this.dataconfigRepository.findOne({where: {and: [{id: rejectBa.reason_id}, {config_group_id: 3}]}});
      if (config != null) {
        await this.dataDigiposRejectRepository.create({
          periode: infoDigipost.trx_date_group,
          ad_code: infoDigipost.ad_code,
          ad_name: infoDigipost.ad_name,
          config_id: rejectBa.reason_id,
          reject_code: config?.config_code,
          reject_label: config?.config_desc,
          reject_description: "REJECT BY|BU|AD_A|" + rejectBa.description,
          create_at: today.toDateString(),
          user_reject: this.currentUserProfile[securityId],
          digipos_id: infoDigipost.digipost_id
        });
        await this.dataDigiposRepository.updateById(infoDigipost.digipost_id, {
          ar_valid_a: -2,
          ar_stat: -2
        });
        // var thisDataDigipos = await this.dataDigiposRepository.findById(infoDigipost.digipost_id);
        // if (thisDataDigipos.digipost_id != undefined) {
        //   if (thisDataDigipos.ar_valid_a == -2 && thisDataDigipos.ar_valid_b == -2) {
        //     await this.dataDigiposRepository.updateById(thisDataDigipos.digipost_id, {
        //       ar_stat: -2
        //     });
        //   }
        // }

      } else {
        returnMs.success = false;
        returnMs.message = "Reason not found, please entry other reason !";
      }
    }
    if (digiposData.length == 0) {
      returnMs.success = false;
      returnMs.message = "No data digipos rejected !";
    } else {
      if (returnMs.success == true) {
        returnMs.message = "success update [" + digiposData.length.toString() + "] data digipos !";
        var dataDigipos = await this.dataDigiposRepository.findOne({where: {and: [{digipost_id: {inq: rejectBa.digipos}}]}});
        var userId = this.currentUserProfile[securityId];
        var roleNotifSetADB = ControllerConfig.onGetRoleNotifGroup("02020201");
        var infoUser = await this.userRepository.findById(userId)
        await this.dataNotificationRepository.create({
          types: "NOTIFICATION",
          target: roleNotifSetADB.role,
          to: "",
          from_who: infoUser.username,
          subject: roleNotifSetADB.label,
          body: "Data inquiry review summary AD A sudah di tolak oleh BU berjumlah : " + rejectBa.digipos.length,
          path_url: "",
          app: "DIGIPOS",
          code: "02020201",
          code_label: "",
          at_create: ControllerConfig.onCurrentTime().toString(),
          at_read: "",
          periode: dataDigipos?.trx_date_group,
          at_flag: 0
        });

        ControllerConfig.onSendNotif({
          types: "NOTIFICATION",
          target: roleNotifSetADB.role,
          to: "",
          from_who: infoUser.username,
          subject: roleNotifSetADB.label,
          body: "Data inquiry review summary AD A sudah di tolak oleh BU berjumlah : " + rejectBa.digipos.length,
          path_url: "",
          app: "DIGIPOS",
          code: "02020201",
          code_label: "",
          at_create: ControllerConfig.onCurrentTime().toString(),
          at_read: "",
          periode: dataDigipos?.trx_date_group,
          at_flag: 0
        });

        // ControllerConfig.onSendMessageToSubscribe(dataMsg, (r: any) => {
        //   console.log(r);
        // });
      }
    }
    return returnMs;
  }


  @post('/data-digipos/update-reject-bu-ad-b', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: RejectValidateRequestBody
          }
        },
      },
    },
  })
  async RejetBuADB(
    @requestBody(RejectValidateRequestBody) rejectBa: {
      reason_id: number,
      digipos: [],
      description: string
    },
  ): Promise<Object> {
    var returnMs: any; returnMs = {success: true, message: ""};

    var today = new Date();
    var digiposData = await this.dataDigiposRepository.find({where: {and: [{digipost_id: {inq: rejectBa.digipos}}, {ar_stat: 1}]}});

    for (var i in digiposData) {
      var infoDigipost = digiposData[i];
      var config = await this.dataconfigRepository.findOne({where: {and: [{id: rejectBa.reason_id}, {config_group_id: 3}]}});
      if (config != null) {
        await this.dataDigiposRejectRepository.create({
          periode: infoDigipost.trx_date_group,
          ad_code: infoDigipost.ad_code,
          ad_name: infoDigipost.ad_name,
          config_id: rejectBa.reason_id,
          reject_code: config?.config_code,
          reject_label: config?.config_desc,
          reject_description: "REJECT BY|BU|AD_B|" + rejectBa.description,
          create_at: today.toDateString(),
          user_reject: this.currentUserProfile[securityId],
          digipos_id: infoDigipost.digipost_id
        });
        await this.dataDigiposRepository.updateById(infoDigipost.digipost_id, {
          ar_valid_b: -2,
          ar_stat: -2
        });

        // var thisDataDigipos = await this.dataDigiposRepository.findById(infoDigipost.digipost_id);
        // if (thisDataDigipos.digipost_id != undefined) {
        //   if (thisDataDigipos.ar_valid_a == -2 && thisDataDigipos.ar_valid_b == -2) {
        //     await this.dataDigiposRepository.updateById(thisDataDigipos.digipost_id, {
        //       ar_stat: -2
        //     });
        //   }
        // }

      } else {
        returnMs.success = false;
        returnMs.message = "Reason not found, please entry other reason !";
      }
    }
    if (digiposData.length == 0) {
      returnMs.success = false;
      returnMs.message = "No data digipos rejected !";
    } else {
      if (returnMs.success == true) {

        returnMs.message = "success update [" + digiposData.length.toString() + "] data digipos !";
        var dataDigipos = await this.dataDigiposRepository.findOne({where: {and: [{digipost_id: {inq: rejectBa.digipos}}]}});
        var userId = this.currentUserProfile[securityId];
        var roleNotifSetADB = ControllerConfig.onGetRoleNotifGroup("02020202");
        var infoUser = await this.userRepository.findById(userId)
        await this.dataNotificationRepository.create({
          types: "NOTIFICATION",
          target: roleNotifSetADB.role,
          to: "",
          from_who: infoUser.username,
          subject: roleNotifSetADB.label,
          body: "Data inquiry review summary AD B sudah di tolak oleh BU berjumlah : " + rejectBa.digipos.length,
          path_url: "",
          app: "DIGIPOS",
          code: "02020202",
          code_label: "",
          at_create: ControllerConfig.onCurrentTime().toString(),
          at_read: "",
          periode: dataDigipos?.trx_date_group,
          at_flag: 0
        });

        ControllerConfig.onSendNotif({
          types: "NOTIFICATION",
          target: roleNotifSetADB.role,
          to: "",
          from_who: infoUser.username,
          subject: roleNotifSetADB.label,
          body: "Data inquiry review summary AD B sudah di tolak oleh BU berjumlah : " + rejectBa.digipos.length,
          path_url: "",
          app: "DIGIPOS",
          code: "02020202",
          code_label: "",
          at_create: ControllerConfig.onCurrentTime().toString(),
          at_read: "",
          periode: dataDigipos?.trx_date_group,
          at_flag: 0
        });

        // ControllerConfig.onSendMessageToSubscribe(dataMsg, (r: any) => {
        //   console.log(r);
        // });
      }
    }
    return returnMs;
  }

  @post('/data-digipos/update-reject-bu', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: RejectValidateRequestBody
          }
        },
      },
    },
  })
  async RejetBu(
    @requestBody(RejectValidateRequestBody) rejectBa: {
      reason_id: number,
      digipos: [],
      description: string
    },
  ): Promise<Object> {
    var returnMs: any; returnMs = {success: true, message: ""};

    var today = new Date();
    var digiposData = await this.dataDigiposRepository.find({where: {and: [{digipost_id: {inq: rejectBa.digipos}}, {ar_stat: 1}]}});

    for (var i in digiposData) {
      var infoDigipost = digiposData[i];
      var config = await this.dataconfigRepository.findOne({where: {and: [{id: rejectBa.reason_id}, {config_group_id: 3}]}});
      if (config != null) {
        await this.dataDigiposRejectRepository.create({
          periode: infoDigipost.trx_date_group,
          ad_code: infoDigipost.ad_code,
          ad_name: infoDigipost.ad_name,
          config_id: rejectBa.reason_id,
          reject_code: config?.config_code,
          reject_label: config?.config_desc,
          reject_description: "REJECT BY|BU|" + rejectBa.description,
          create_at: today.toDateString(),
          user_reject: this.currentUserProfile[securityId],
          digipos_id: infoDigipost.digipost_id
        });
        await this.dataDigiposRepository.updateById(infoDigipost.digipost_id, {
          ar_stat: -2
        });
      } else {
        returnMs.success = false;
        returnMs.message = "Reason not found, please entry other reason !";
      }
    }
    if (digiposData.length == 0) {
      returnMs.success = false;
      returnMs.message = "No data digipos rejected !";
    } else {
      if (returnMs.success == true) {
        returnMs.message = "success update [" + digiposData.length.toString() + "] data digipos !";
        var dataDigipos = await this.dataDigiposRepository.findOne({where: {and: [{digipost_id: {inq: rejectBa.digipos}}]}});
        // var messageFb = "Data  Digipos periode " + dataDigipos?.trx_date_group + " telah di tolak oleh bisnis user, tolong segera lakukan upload data ulang !";
        // var dataMsg = {
        //   title: "Data Digipos Reject Bisnis User",
        //   message: messageFb,
        //   type: "DIGIPOS_REJECT_DATA_BU",
        //   to_role: ControllerConfig.getRoleIdMessage("DIGIPOS_REJECT_DATA_BU"),
        //   to_user: "",
        //   at_create: ControllerConfig.onCurrentTime().toString(),
        //   status: "0",
        //   id_target: "",
        //   for_app: "DIGIPOS",
        //   periode: dataDigipos?.trx_date_group
        // };
        // await this.dataNotificationRepository.create(dataMsg);
        // ControllerConfig.onSendMessageToSubscribe(dataMsg, (r: any) => {
        //   console.log(r);
        // });
      }
    }
    return returnMs;
  }

  @post('/data-digipos/update-validate-invoice', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: UpdateValidateRequestBody
          }
        },
      },
    },
  })
  async ValidateInvoice(
    @requestBody(UpdateValidateRequestBody) rejectBa: {
      digipos: []
    },
  ): Promise<Object> {
    var returnMs: any; returnMs = {success: true, message: ""};

    var today = new Date();
    var digiposData = await this.dataDigiposRepository.find({where: {and: [{digipost_id: {inq: rejectBa.digipos}}, {ar_stat: 2}]}});
    var periode: string; periode = "";
    for (var i in digiposData) {
      var infoDigipost = digiposData[i];
      if (infoDigipost.generate_file_invoice == 0 || infoDigipost.generate_file_invoice == null) {
        returnMs.success = false;
        returnMs.message = "Error validate process, because file not generate, please generate / re-generate file before validate process !";
      } else {
        await this.dataDigiposRepository.updateById(infoDigipost.digipost_id, {
          acc_file_invoice: 1
        });
      }
      periode = infoDigipost.trx_date_group;
    }
    if (digiposData.length == 0) {
      returnMs.success = false;
      returnMs.message = "No data digipos rejected !";
    } else {
      if (returnMs.success != false) {

        var validataInvNot = await this.dataDigiposRepository.find({where: {and: [{acc_file_invoice: -1}, {trx_date_group: periode}]}});
        var validataInvProses = await this.dataDigiposRepository.find({where: {and: [{acc_file_invoice: 0}, {trx_date_group: periode}]}});
        var validataInvtrue = await this.dataDigiposRepository.find({where: {and: [{acc_file_invoice: 1}, {trx_date_group: periode}]}});

        if (validataInvNot.length == 0) {
          var userId = this.currentUserProfile[securityId];
          var roleNotifSetADB = ControllerConfig.onGetRoleNotifGroup("11010101");
          var infoUser = await this.userRepository.findById(userId)
          await this.dataNotificationRepository.create({
            types: "NOTIFICATION",
            target: roleNotifSetADB.role,
            to: "",
            from_who: infoUser.username,
            subject: roleNotifSetADB.label,
            body: "Document Invoice " + periode + " Sudah Divalidasi ( " + validataInvtrue.length.toString() + " ), belum divalidasi ( " + validataInvProses.length.toString() + " ) dan tolak ( " + validataInvNot.length.toString() + " ).",
            path_url: "",
            app: "DIGIPOS",
            code: "11010101",
            code_label: "",
            at_create: ControllerConfig.onCurrentTime().toString(),
            at_read: "",
            periode: periode,
            at_flag: 0
          });

          ControllerConfig.onSendNotif({
            types: "NOTIFICATION",
            target: roleNotifSetADB.role,
            to: "",
            from_who: infoUser.username,
            subject: roleNotifSetADB.label,
            body: "Document Invoice periode " + periode + " Sudah Divalidasi ( " + validataInvtrue.length.toString() + " ), belum divalidasi ( " + validataInvProses.length.toString() + " ) dan tolak ( " + validataInvNot.length.toString() + " ).",
            path_url: "",
            app: "DIGIPOS",
            code: "11010101",
            code_label: "",
            at_create: ControllerConfig.onCurrentTime().toString(),
            at_read: "",
            periode: periode,
            at_flag: 0
          });
        }

        returnMs.message = "success validasi invoice [" + digiposData.length.toString() + "] data digipos !";
      }
    }
    return returnMs;
  }


  @post('/data-digipos/update-validate-sign', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: ValidateSignRequestBody
          }
        },
      },
    },
  })
  async ValidateSign(
    @requestBody(ValidateSignRequestBody) rejectBa: {
      type_ad: string,
      digipos: []
    },
  ): Promise<Object> {
    var returnMs: any; returnMs = {success: true, message: ""};

    var today = new Date();
    var digiposData = await this.dataDigiposRepository.find({where: {and: [{digipost_id: {inq: rejectBa.digipos}}, {ar_stat: 2}]}});

    for (var i in digiposData) {
      var infoDigipost = digiposData[i];
      if (rejectBa.type_ad == "AD_A") {
        if (infoDigipost.generate_file_a == 0 || infoDigipost.generate_file_a == null) {
          returnMs.success = false;
          returnMs.message = "Error validate process, because file not generate, please generate / re-generate file before validate process !";
        } else {
          await this.dataDigiposRepository.updateById(infoDigipost.digipost_id, {
            sign_off_a: 0,
            acc_file_ba_a: 1
          });
        }
      }

      if (rejectBa.type_ad == "AD_B") {
        if (infoDigipost.generate_file_b == 0 || infoDigipost.generate_file_b == null) {
          returnMs.success = false;
          returnMs.message = "Error validate process, because file not generate, please generate / re-generate file before validate process !";
        } else {
          await this.dataDigiposRepository.updateById(infoDigipost.digipost_id, {
            sign_off_b: 0,
            acc_file_ba_b: 1
          });
        }

      }
    }
    if (digiposData.length == 0) {
      returnMs.success = false;
      returnMs.message = "No data digipos validate sign !";
    } else {
      if (returnMs.success != false) {
        var infoRaw = await this.dataDigiposRepository.findOne({where: {and: [{digipost_id: {inq: rejectBa.digipos}}, {ar_stat: 2}]}});
        // var countSign =
        var userId = this.currentUserProfile[securityId];
        var roleNotifSetADB = ControllerConfig.onGetRoleNotifGroup(" 04010101");
        var infoUser = await this.userRepository.findById(userId)
        await this.dataNotificationRepository.create({
          types: "NOTIFICATION",
          target: roleNotifSetADB.role,
          to: "",
          from_who: infoUser.username,
          subject: roleNotifSetADB.label,
          body: "Data inquiry review summary BAR Recon sudah siap untuk di sign.",
          path_url: "",
          app: "DIGIPOS",
          code: " 04010101",
          code_label: "",
          at_create: ControllerConfig.onCurrentTime().toString(),
          at_read: "",
          periode: infoRaw?.trx_date_group,
          at_flag: 0
        });

        ControllerConfig.onSendNotif({
          types: "NOTIFICATION",
          target: roleNotifSetADB.role,
          to: "",
          from_who: infoUser.username,
          subject: roleNotifSetADB.label,
          body: "Data inquiry review summary BAR Recon sudah siap untuk di sign.",
          path_url: "",
          app: "DIGIPOS",
          code: " 04010101",
          code_label: "",
          at_create: ControllerConfig.onCurrentTime().toString(),
          at_read: "",
          periode: infoRaw?.trx_date_group,
          at_flag: 0
        });

        returnMs.message = "success update [" + digiposData.length.toString() + "] data digipos !";

      }
    }
    return returnMs;
  }

  @post('/data-digipos/process-sign', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: ValidateSignRequestBody
          }
        },
      },
    },
  })
  async ProcessSign(
    @requestBody(ValidateSignRequestBody) rejectBa: {
      type_ad: string,
      digipos: []
    },
  ): Promise<Object> {
    var returnMs: any; returnMs = {success: true, message: ""};

    var today = new Date();
    var digiposData = await this.dataDigiposRepository.find({where: {and: [{digipost_id: {inq: rejectBa.digipos}}, {ar_stat: 2}]}});

    for (var i in digiposData) {
      const infoDigipost = digiposData[i];

      const DigiposID = infoDigipost.digipost_id;
      const AccFileA = infoDigipost.acc_file_ba_a;
      const AccFileB = infoDigipost.acc_file_ba_b;
      const GenerateA = infoDigipost.generate_file_a;
      const GenerateB = infoDigipost.generate_file_b;

      // console.log("Type : " + rejectBa.type_ad);
      if (rejectBa.type_ad == "AD_A") {
        if (infoDigipost.trx_cluster_out <= 0) {
          console.log("A KOSONG");
        } else {
          if (GenerateA == 1) {
            if (AccFileA == 1) {
              try {
                // console.log("ID : " + DigiposID);
                await this.dataDigiposRepository.updateById(DigiposID, {
                  sign_off_a: 1,
                  acc_file_ba_a: 1,
                  generate_file_a: 0
                });
              } catch (error) {
                returnMs.success = false;
                returnMs.message = error;
              }

            } else {
              returnMs.success = false;
              returnMs.message = "Please validasi Berita Acara before sign berita acara for : " + infoDigipost.ba_number_a + "!";
            }
          } else {
            returnMs.success = false;
            returnMs.message = "Please generate and validasi Berita Acara before sign berita acara for : " + infoDigipost.ba_number_a + "!";
          }
        }
      }

      if (rejectBa.type_ad == "AD_B") {
        if (infoDigipost.trx_paid_invoice <= 0) {
          console.log("B KOSONG");
        } else {
          if (GenerateB == 1) {
            if (AccFileB == 1) {
              // console.log("Type : " + rejectBa.type_ad);
              try {
                console.log(DigiposID);
                await this.dataDigiposRepository.updateById(DigiposID, {
                  sign_off_b: 1,
                  acc_file_ba_b: 1,
                  generate_file_b: 0
                });
              } catch (error) {
                returnMs.success = false;
                returnMs.message = error;
              }
            } else {
              returnMs.success = false;
              returnMs.message = "Please validasi Berita Acara before sign berita acara for : " + infoDigipost.ba_number_b + "!";
            }
          } else {
            returnMs.success = false;
            returnMs.message = "Please generate and validasi Berita Acara before sign berita acara for : " + infoDigipost.ba_number_b + "!";
          }
        }
      }
    }
    if (digiposData.length == 0) {
      returnMs.success = false;
      returnMs.message = "No data digipos sign !";
    } else {
      if (returnMs.success == true) {
        returnMs.message = "success update [" + digiposData.length.toString() + "] data digipos !";
      }
    }
    return returnMs;
  }


  @post('/data-digipos/update-reject-sign', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: RejectValidateSignRequestBody
          }
        },
      },
    },
  })
  async RejetSign(
    @requestBody(RejectValidateSignRequestBody) rejectBa: {
      type_ad: string,
      reason_id: number,
      digipos: [],
      description: string
    },
  ): Promise<Object> {
    var returnMs: any; returnMs = {success: true, message: ""};

    var today = new Date();
    var digiposData = await this.dataDigiposRepository.find({where: {and: [{digipost_id: {inq: rejectBa.digipos}}, {ar_stat: 2}]}});

    for (var i in digiposData) {
      var infoDigipost = digiposData[i];
      var config = await this.dataconfigRepository.findOne({where: {and: [{id: rejectBa.reason_id}, {config_group_id: 6}]}});
      if (config != null) {
        await this.dataDigiposRejectRepository.create({
          periode: infoDigipost.trx_date_group,
          ad_code: infoDigipost.ad_code,
          ad_name: infoDigipost.ad_name,
          config_id: rejectBa.reason_id,
          reject_code: config?.config_code,
          reject_label: config?.config_desc,
          reject_description: "Reject Sign|" + rejectBa.type_ad + "|" + rejectBa.description,
          create_at: today.toDateString(),
          user_reject: this.currentUserProfile[securityId],
          digipos_id: infoDigipost.digipost_id
        });

        if (rejectBa.type_ad == "AD_A") {
          await this.dataDigiposRepository.updateById(infoDigipost.digipost_id, {
            sign_off_a: -1,
            generate_file_a: 0,
            acc_file_ba_a: 0
          });
        }

        if (rejectBa.type_ad == "AD_B") {
          await this.dataDigiposRepository.updateById(infoDigipost.digipost_id, {
            sign_off_b: -1,
            generate_file_b: 0,
            acc_file_ba_b: 0
          });
        }

      } else {
        returnMs.success = false;
        returnMs.message = "Reason not found, please entry other reason !";
      }
    }

    if (digiposData.length == 0) {
      returnMs.success = false;
      returnMs.message = "No data digipos rejected !";
    } else {
      if (returnMs.success == true) {
        var infoRaw = await this.dataDigiposRepository.findOne({where: {and: [{digipost_id: {inq: rejectBa.digipos}}, {ar_stat: 2}]}});
        // var countSign =
        var userId = this.currentUserProfile[securityId];
        var roleNotifSetADB = ControllerConfig.onGetRoleNotifGroup("04010103");
        var infoUser = await this.userRepository.findById(userId)
        await this.dataNotificationRepository.create({
          types: "NOTIFICATION",
          target: roleNotifSetADB.role,
          to: "",
          from_who: infoUser.username,
          subject: roleNotifSetADB.label,
          body: "Data inquiry review summary ditolak untuk di sign, tolong segera check.",
          path_url: "",
          app: "DIGIPOS",
          code: "04010103",
          code_label: "",
          at_create: ControllerConfig.onCurrentTime().toString(),
          at_read: "",
          periode: infoRaw?.trx_date_group,
          at_flag: 0
        });

        ControllerConfig.onSendNotif({
          types: "NOTIFICATION",
          target: roleNotifSetADB.role,
          to: "",
          from_who: infoUser.username,
          subject: roleNotifSetADB.label,
          body: "Data inquiry review summary ditolak untuk di sign, tolong segera check",
          path_url: "",
          app: "DIGIPOS",
          code: "04010103",
          code_label: "",
          at_create: ControllerConfig.onCurrentTime().toString(),
          at_read: "",
          periode: infoRaw?.trx_date_group,
          at_flag: 0
        });
        returnMs.message = "success update [" + digiposData.length.toString() + "] data digipos !";
      }
    }
    return returnMs;
  }


  @post('/data-digipos/update-reject-pain-inv-rec', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: RejectValidatePaidRecvRequestBody
          }
        },
      },
    },
  })
  async RejetSignRecv(
    @requestBody(RejectValidatePaidRecvRequestBody) rejectBa: {
      reason_id: number,
      digipos: [],
      description: string
    },
  ): Promise<Object> {
    var returnMs: any; returnMs = {success: true, message: ""};

    var today = new Date();
    var digiposData = await this.dataDigiposRepository.find({where: {and: [{digipost_id: {inq: rejectBa.digipos}}, {ar_stat: 2}]}});
    var periode: string; periode = "";
    for (var i in digiposData) {
      var infoDigipost = digiposData[i];
      var config = await this.dataconfigRepository.findOne({where: {and: [{id: rejectBa.reason_id}/*, {config_group_id: 6}*/]}});
      if (config != null) {
        await this.dataDigiposRejectRepository.create({
          periode: infoDigipost.trx_date_group,
          ad_code: infoDigipost.ad_code,
          ad_name: infoDigipost.ad_name,
          config_id: rejectBa.reason_id,
          reject_code: config?.config_code,
          reject_label: config?.config_desc,
          reject_description: "REJECT INVOICE RECV|AD_B|" + rejectBa.description,
          create_at: today.toDateString(),
          user_reject: this.currentUserProfile[securityId],
          digipos_id: infoDigipost.digipost_id
        });
        await this.dataDigiposRepository.updateById(infoDigipost.digipost_id, {
          sign_off_b: -1,
          generate_file_b: 0,
          acc_file_ba_b: 0,
          acc_recv: -1
        });
        periode = infoDigipost.trx_date_group;

      } else {
        returnMs.success = false;
        returnMs.message = "Reason not found, please entry other reason !";
      }
    }

    if (digiposData.length == 0) {
      returnMs.success = false;
      returnMs.message = "No data digipos rejected !";
    } else {
      if (returnMs.success == true) {

        var userId = this.currentUserProfile[securityId];
        var roleNotifSetADB = ControllerConfig.onGetRoleNotifGroup("13010102");
        var infoUser = await this.userRepository.findById(userId)
        await this.dataNotificationRepository.create({
          types: "NOTIFICATION",
          target: roleNotifSetADB.role,
          to: "",
          from_who: infoUser.username,
          subject: roleNotifSetADB.label,
          body: "Document Invoice telah di tolak, jumlah yang ditolak : " + digiposData.length.toString() + " data !",
          path_url: "",
          app: "DIGIPOS",
          code: "13010102",
          code_label: "",
          at_create: ControllerConfig.onCurrentTime().toString(),
          at_read: "",
          periode: periode,
          at_flag: 0
        });

        ControllerConfig.onSendNotif({
          types: "NOTIFICATION",
          target: roleNotifSetADB.role,
          to: "",
          from_who: infoUser.username,
          subject: roleNotifSetADB.label,
          body: "Document Invoice telah di tolak, jumlah yang ditolak : " + digiposData.length.toString() + " data !",
          path_url: "",
          app: "DIGIPOS",
          code: "13010102",
          code_label: "",
          at_create: ControllerConfig.onCurrentTime().toString(),
          at_read: "",
          periode: periode,
          at_flag: 0
        });

        returnMs.message = "success update [" + digiposData.length.toString() + "] data digipos !";
      }
    }
    return returnMs;
  }

  @post('/data-digipos/update-valid-pain-inv-rec', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: AccValidatePaidRecvRequestBody
          }
        },
      },
    },
  })
  async ValidSignRecv(
    @requestBody(AccValidatePaidRecvRequestBody) rejectBa: {
      digipos: [],
    },
  ): Promise<Object> {
    var returnMs: any; returnMs = {success: true, message: ""};

    var today = new Date();
    var digiposData = await this.dataDigiposRepository.find({where: {and: [{digipost_id: {inq: rejectBa.digipos}}, {ar_stat: 2}]}});
    var periode: string; periode = "";
    for (var i in digiposData) {
      var infoDigipost = digiposData[i];
      await this.dataDigiposRepository.updateById(infoDigipost.digipost_id, {
        acc_recv: 2
      });
      periode = infoDigipost.trx_date_group;
    }

    if (digiposData.length == 0) {
      returnMs.success = false;
      returnMs.message = "No data digipos validate !";
    } else {
      if (returnMs.success == true) {
        var userId = this.currentUserProfile[securityId];
        var roleNotifSetADB = ControllerConfig.onGetRoleNotifGroup("13010101");
        var infoUser = await this.userRepository.findById(userId)
        await this.dataNotificationRepository.create({
          types: "NOTIFICATION",
          target: roleNotifSetADB.role,
          to: "",
          from_who: infoUser.username,
          subject: roleNotifSetADB.label,
          body: "Document Invoice berhasil di validasi, jumlah yang divalidasi : " + digiposData.length.toString() + "  !",
          path_url: "",
          app: "DIGIPOS",
          code: "13010101",
          code_label: "",
          at_create: ControllerConfig.onCurrentTime().toString(),
          at_read: "",
          periode: periode,
          at_flag: 0
        });

        ControllerConfig.onSendNotif({
          types: "NOTIFICATION",
          target: roleNotifSetADB.role,
          to: "",
          from_who: infoUser.username,
          subject: roleNotifSetADB.label,
          body: "Document Invoice berhasil di validasi, jumlah yang divalidasi : " + digiposData.length.toString() + "  !",
          path_url: "",
          app: "DIGIPOS",
          code: "13010101",
          code_label: "",
          at_create: ControllerConfig.onCurrentTime().toString(),
          at_read: "",
          periode: periode,
          at_flag: 0
        });
        returnMs.message = "success update [" + digiposData.length.toString() + "] data digipos !";
      }
    }
    return returnMs;
  }

  @post('/data-digipos/update-generate-csv', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: UpdateGenerateCSVRequestBody
          }
        },
      },
    },
  })
  async UpdateGenerateCSV(
    @requestBody(UpdateGenerateCSVRequestBody) genDoc: {
      digipos: [],
    },
  ): Promise<Object> {
    var returnMs: any; returnMs = {success: true, message: ""};

    var today = new Date();
    var digiposData = await this.dataDigiposRepository.find({where: {and: [{digipost_id: {inq: genDoc.digipos}}]}});

    for (var i in digiposData) {
      var infoDigipost = digiposData[i];
      await this.dataDigiposRepository.updateById(infoDigipost.digipost_id, {
        at_generate_csv: ControllerConfig.onCurrentTime().toString(),
        flag_generate_csv: 1
      });
    }

    if (digiposData.length == 0) {
      returnMs.success = false;
      returnMs.message = "No data digipos generate csv !";
    } else {
      if (returnMs.success == true) {
        returnMs.message = "success update [" + digiposData.length.toString() + "] data digipos generate csv !";
      }
    }
    return returnMs;
  }


  @post('/data-digipos/update-no-ba', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: UpdateNoBA
          }
        },
      },
    },
  })
  async updateNoBa(
    @requestBody(UpdateNoBA) updateNoBA: {type_ad: string, digipos: number, no_ba: string},
  ): Promise<Object> {
    var returnMs: any; returnMs = {success: true, message: ""};
    var dataupdate = await this.dataDigiposRepository.count({and: [{digipost_id: updateNoBA.digipos}, {ar_stat: 2}]});

    if (dataupdate.count > 0) {
      var digiposInfo = await this.dataDigiposRepository.findById(updateNoBA.digipos);

      var CountBAFound = await this.dataDigiposRepository.count({or: [{ba_number_a: updateNoBA.no_ba}, {ba_number_b: updateNoBA.no_ba}]});
      if (CountBAFound.count == 0) {
        if (updateNoBA.type_ad == "AD_A") {
          try {
            var updateDigiposInfo = await this.dataDigiposRepository.updateById(updateNoBA.digipos, {
              ba_number_a: updateNoBA.no_ba,
              generate_file_a: 0,
              google_drive_a: 0,
              sign_off_a: 0,
              acc_file_ba_a: 0
            });
          } catch (err: any) {
            returnMs["success"] = false;
            returnMs["message"] = err.message;
          }
        }
        if (updateNoBA.type_ad == "AD_B") {
          try {
            var updateDigiposInfo = await this.dataDigiposRepository.updateById(updateNoBA.digipos, {
              ba_number_b: updateNoBA.no_ba,
              generate_file_b: 0,
              google_drive_b: 0,
              sign_off_b: 0,
              acc_file_ba_b: 0
            });
          } catch (err: any) {
            returnMs["success"] = false;
            returnMs["message"] = err.message;
          }
        }
      } else {
        returnMs["success"] = false;
        returnMs["message"] = "Error Duplicate BA Number, No BA have duplicate in BA Number : " + updateNoBA.no_ba;
      }
    } else {
      returnMs["success"] = false;
      returnMs["message"] = "Error, not found Digipos ID !";
    }
    return returnMs;
  }

  @post('/data-digipos/update-no-faktur', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: UpdateNoInvoice
          }
        },
      },
    },
  })
  async updateNoFaktur(
    @requestBody(UpdateNoInvoice) updateNoInvoice: {digipos: number, no_ba: string},
  ): Promise<Object> {
    var returnMs: any; returnMs = {success: true, message: ""};
    var dataupdate = await this.dataDigiposRepository.count({and: [{digipost_id: updateNoInvoice.digipos}, {ar_stat: 2}]});

    if (dataupdate.count > 0) {
      var digiposInfo = await this.dataDigiposRepository.findById(updateNoInvoice.digipos);

      var CountBAFound = await this.dataDigiposRepository.count({or: [{inv_number: updateNoInvoice.no_ba}]});
      if (CountBAFound.count == 0) {
        try {
          var updateDigiposInfo = await this.dataDigiposRepository.updateById(updateNoInvoice.digipos, {
            vat_number: updateNoInvoice.no_ba,
            vat_attachment: ""
          });
          returnMs["success"] = true;
          returnMs["message"] = "Data update nomor faktur pajak success !";
        } catch (err: any) {
          returnMs["success"] = false;
          returnMs["message"] = err.message;
        }
      } else {
        returnMs["success"] = false;
        returnMs["message"] = "Error Duplicate  Faktur Pajak Number,  duplicate in Faktur Pajak Number : " + updateNoInvoice.no_ba;
      }
    } else {
      returnMs["success"] = false;
      returnMs["message"] = "Error, not found Digipos ID !";
    }
    return returnMs;
  }

  @post('/data-digipos/update-no-invoice', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: UpdateNoInvoice
          }
        },
      },
    },
  })
  async updateNoInvoice(
    @requestBody(UpdateNoInvoice) updateNoInvoice: {digipos: number, no_ba: string},
  ): Promise<Object> {
    var returnMs: any; returnMs = {success: true, message: ""};
    var dataupdate = await this.dataDigiposRepository.count({and: [{digipost_id: updateNoInvoice.digipos}, {ar_stat: 2}]});

    if (dataupdate.count > 0) {
      var digiposInfo = await this.dataDigiposRepository.findById(updateNoInvoice.digipos);

      var CountBAFound = await this.dataDigiposRepository.count({or: [{inv_number: updateNoInvoice.no_ba}]});
      if (CountBAFound.count == 0) {
        try {
          var updateDigiposInfo = await this.dataDigiposRepository.updateById(updateNoInvoice.digipos, {
            inv_number: updateNoInvoice.no_ba,
            generate_file_invoice: 0,
            distribute_file_invoice: 0,
            acc_file_invoice: 0
          });
        } catch (err: any) {
          returnMs["success"] = false;
          returnMs["message"] = err.message;
        }
      } else {
        returnMs["success"] = false;
        returnMs["message"] = "Error Duplicate Invoice Number, No Invoice have duplicate in BA Number : " + updateNoInvoice.no_ba;
      }
    } else {
      returnMs["success"] = false;
      returnMs["message"] = "Error, not found Digipos ID !";
    }
    return returnMs;
  }


  @get('/data-digipos/{id}/detail-by-ad', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(DataDigipos, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findDetailByCodeAD(
    @param.path.string('id') id: string,
    @param.filter(DataSumarry, {exclude: ['fields']}) filter?: Filter<DataSumarry>
  ): Promise<Object> {
    // return this.dataDigiposRepository.findById(id, filter);
    var filterWhere: any;
    filterWhere = filter;
    var data: any;
    data = {};
    if (filter?.where == undefined) {
      data = {error: true, message: 'where params not null !! "where" : { "periode" : "", "ad_code" : "" } '};
    } else {
      if (filterWhere.where.periode == undefined) {
        data = {error: true, message: 'where periode params not null !! "where" : { "periode" : "xxxx" } '};
      }

      // "limit": 100,
      //   "skip": 0,
      var skips = "";
      if (filterWhere.limit != undefined && filterWhere.skip != undefined) {
        skips = "LIMIT " + filterWhere.skip + "," + filterWhere.limit;
      }

      if (filterWhere.limit != undefined && filterWhere.skip == undefined) {
        skips = "LIMIT 0," + filterWhere.limit;
      }

      if (filterWhere.where.periode != undefined) {
        data["success"] = true;
        var summaryDetail = await this.dataDigiposRepository.execute(
          "SELECT " +
          "summary.periode, " +
          "summary.ad_code, " +
          "summary.dealer_code, " +
          "summary.location, " +
          "summary.name, " +
          "summary.type_trx, " +
          "summary.group_amount,  " +
          "sum(summary.trx_a) as trx_a_sum, " +
          "sum(summary.tot_mdr_a) as tot_mdr_a_sum, " +
          "sum(summary.trx_b) as trx_b_sum, " +
          "sum(summary.tot_mdr_b) as tot_mdr_b_sum " +
          "FROM " +
          "(SELECT " +
          "DISTINCT(DataSumarry.id), " +
          "AdCluster.ad_code, " +
          "DataSumarry.dealer_code," +
          "DataSumarry.location, " +
          "DataSumarry.name, " +
          "DataSumarry.type_trx, " +
          "IF(DataSumarry.paid_in_amount = 0, DataSumarry.withdraw_amount , DataSumarry.paid_in_amount) as group_amount, " +
          "DataSumarry.trx_a, " +
          "DataSumarry.tot_mdr_a, " +
          "DataSumarry.trx_b, " +
          "DataSumarry.tot_mdr_b, " +
          "DataSumarry.status, " +
          "DataSumarry.periode, " +
          "DataSumarry.create_at " +
          "FROM " +
          "DataSumarry " +
          "INNER JOIN " +
          "AdCluster " +
          "on " +
          "DataSumarry.dealer_code = AdCluster.ad_cluster_id " +
          "WHERE " +
          "DataSumarry.periode = '" + filterWhere.where.periode + "' AND AdCluster.ad_code = '" + id + "') as summary " +
          "GROUP BY summary.dealer_code ,summary.group_amount,summary.type_trx " + skips);

        const configData = await this.dataconfigRepository.find({where: {config_group_id: 1}});
        var configNum: any;
        configNum = {};
        for (var i in configData) {
          var infoConfig = configData[i];

          if (infoConfig.config_code == "0001-01") {
            configNum["persen_fee"] = infoConfig.config_value;
          }

          if (infoConfig.config_code == "0001-02") {
            configNum["persen_paid"] = infoConfig.config_value;
          }

          if (infoConfig.config_code == "0001-03") {
            configNum["num_dpp_fee"] = infoConfig.config_value;
          }

          if (infoConfig.config_code == "0001-04") {
            configNum["num_ppn_fee"] = infoConfig.config_value;
          }

          if (infoConfig.config_code == "0001-05") {
            configNum["num_pph_fee"] = infoConfig.config_value;
          }


          if (infoConfig.config_code == "0001-11") {
            configNum["num_dpp_paid"] = infoConfig.config_value;
          }

          if (infoConfig.config_code == "0001-12") {
            configNum["num_ppn_paid"] = infoConfig.config_value;
          }

          if (infoConfig.config_code == "0001-13") {
            configNum["num_pph_paid"] = infoConfig.config_value;
          }

        }

        var infoSummaryDet: any;
        infoSummaryDet = [];

        for (var i in summaryDetail) {

          var infoDetail = summaryDetail[i];

          var trxFee = infoDetail.type_trx == "PPOB" ? ((infoDetail.trx_a_sum * infoDetail.group_amount)) : ((infoDetail.trx_a_sum * infoDetail.group_amount) * (configNum.persen_fee));
          var feeDPP = (trxFee / configNum.num_dpp_fee);
          var feePPN = (feeDPP * configNum.num_ppn_fee);
          var feeTotal = feeDPP + feePPN;
          var feePPH = (feeDPP * configNum.num_pph_fee);

          var trxPaid = ((infoDetail.trx_b_sum * infoDetail.group_amount) * (configNum.persen_paid));
          var paidDPP = (trxPaid / configNum.num_dpp_paid);
          var paidPPN = (paidDPP * configNum.num_ppn_paid);
          var paidTotal = paidDPP + paidPPN;
          var paidPPH = (paidDPP * configNum.num_pph_paid);

          infoSummaryDet.push({
            trx_date_group: infoDetail.periode,
            dealer_code: infoDetail.dealer_code,
            ad_code: infoDetail.ad_code,
            ad_name: infoDetail.name,
            type_trx: infoDetail.type_trx,
            sales_fee: infoDetail.group_amount,
            trx_cluster_out: infoDetail.trx_a_sum,
            mdr_fee_amount: trxFee,
            dpp_fee_amount: feeDPP,
            vat_fee_amount: feePPN,
            total_fee_amount: feeTotal,
            pph_fee_amount: feePPH,
            trx_paid_invoice: infoDetail.trx_b_sum,
            paid_inv_amount: trxPaid,
            dpp_paid_amount: paidDPP,
            vat_paid_amount: paidPPN,
            total_paid_amount: paidTotal,
            pph_paid_amount: paidPPH
          });
        }

        var summaryTotalAll = await this.dataDigiposRepository.execute(
          "SELECT " +
          "summary.periode, " +
          "summary.ad_code, " +
          "summary.dealer_code, " +
          "summary.location, " +
          "summary.name, " +
          "summary.type_trx, " +
          "summary.group_amount,  " +
          "sum(summary.trx_a) as trx_a_sum, " +
          "sum(summary.tot_mdr_a) as tot_mdr_a_sum, " +
          "sum(summary.trx_b) as trx_b_sum, " +
          "sum(summary.tot_mdr_b) as tot_mdr_b_sum " +
          "FROM " +
          "(SELECT " +
          "DISTINCT(DataSumarry.id), " +
          "AdCluster.ad_code, " +
          "DataSumarry.dealer_code," +
          "DataSumarry.location, " +
          "DataSumarry.name, " +
          "DataSumarry.type_trx, " +
          "IF(DataSumarry.paid_in_amount = 0, DataSumarry.withdraw_amount , DataSumarry.paid_in_amount) as group_amount, " +
          "DataSumarry.trx_a, " +
          "DataSumarry.tot_mdr_a, " +
          "DataSumarry.trx_b, " +
          "DataSumarry.tot_mdr_b, " +
          "DataSumarry.status, " +
          "DataSumarry.periode, " +
          "DataSumarry.create_at " +
          "FROM " +
          "DataSumarry " +
          "INNER JOIN " +
          "AdCluster " +
          "on " +
          "DataSumarry.dealer_code = AdCluster.ad_cluster_id " +
          "WHERE " +
          "DataSumarry.periode = '" + filterWhere.where.periode + "' AND AdCluster.ad_code = '" + id + "') as summary " +
          "GROUP BY summary.ad_code, summary.group_amount, summary.type_trx");

        var TotalAlltrxFee = 0;
        var TotalAllfeeDPP = 0;
        var TotalAllfeePPN = 0;
        var TotalAllfeeTotal = 0;
        var TotalAllfeePPH = 0;

        var TotalAlltrxPaid = 0;
        var TotalAllpaidDPP = 0;
        var TotalAllpaidPPN = 0;
        var TotalAllpaidTotal = 0;
        var TotalAllpaidPPH = 0;

        for (var i in summaryTotalAll) {
          var infoTotal = summaryTotalAll[i];
          var trxFeeTotal = ((infoTotal.trx_a_sum * infoTotal.group_amount) * (configNum.persen_fee));
          var feeDPPTotal = (trxFeeTotal / configNum.num_dpp_fee);
          var feePPNTotal = (feeDPPTotal * configNum.num_ppn_fee);
          var feeTotalTotal = feeDPPTotal + feePPNTotal;
          var feePPHTotal = (feeDPPTotal * configNum.num_pph_fee);

          var trxPaidTotal = ((infoTotal.trx_b_sum * infoTotal.group_amount) * (configNum.persen_paid));
          var paidDPPTotal = (trxPaidTotal / configNum.num_dpp_paid);
          var paidPPNTotal = (paidDPPTotal * configNum.num_ppn_paid);
          var paidTotalTotal = paidDPPTotal + paidPPNTotal;
          var paidPPHTotal = (paidDPPTotal * configNum.num_pph_paid);

          TotalAlltrxFee = TotalAlltrxFee + infoTotal.trx_a_sum;
          TotalAllfeeDPP = TotalAllfeeDPP + feeDPPTotal;
          TotalAllfeePPN = TotalAllfeePPN + feePPNTotal;
          TotalAllfeeTotal = TotalAllfeeTotal + feeTotalTotal;
          TotalAllfeePPH = TotalAllfeePPH + feePPHTotal;

          TotalAlltrxPaid = TotalAlltrxPaid + infoTotal.trx_b_sum;
          TotalAllpaidDPP = TotalAllpaidDPP + paidDPPTotal;
          TotalAllpaidPPN = TotalAllpaidPPN + paidPPNTotal;
          TotalAllpaidTotal = TotalAllpaidTotal + paidTotalTotal;
          TotalAllpaidPPH = TotalAllpaidPPH + paidPPHTotal;
        }



        data["datas"] = infoSummaryDet;

        data["TotalAlltrxFee"] = TotalAlltrxFee;
        data["TotalAllfeeDPP"] = TotalAllfeeDPP;
        data["TotalAllfeePPN"] = TotalAllfeePPN;
        data["TotalAllfeeTotal"] = TotalAllfeeTotal;
        data["TotalAllfeePPH"] = TotalAllfeePPH;

        data["TotalAlltrxPaid"] = TotalAlltrxPaid;
        data["TotalAllpaidDPP"] = TotalAllpaidDPP;
        data["TotalAllpaidPPN"] = TotalAllpaidPPN;
        data["TotalAllpaidTotal"] = TotalAllpaidTotal;
        data["TotalAllpaidPPH"] = TotalAllpaidPPH;

      }
    }
    return data;
  }

  @get('/data-digipos/detail-info', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(DataDigipos, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findDetailInfo(
    @param.filter(DataDigipos, {exclude: ['fields']}) filter?: Filter<DataSumarry>
  ): Promise<Object> {
    // return this.dataDigiposRepository.findById(id, filter);
    var filterWhere: any;
    filterWhere = filter;
    var data: any; data = {};

    if (filter?.where == undefined) {
      data = {error: true, message: 'where params not null !! "where" : { "periode" : "", "ad_code" : "" } '};
    } else {
      if (filterWhere.where.periode == undefined) {
        data = {error: true, message: 'where periode & ad_code params not null !! "where" : { "periode" : "xxxx" } '};
      }

      var thisWhere: string; thisWhere = "";
      if (filterWhere.where.ad_code == undefined) {
        thisWhere = " WHERE DataSumarry.periode = '" + filterWhere.where.periode + "') as summary ";
      } else {
        thisWhere = " WHERE DataSumarry.periode = '" + filterWhere.where.periode + "' AND AdCluster.ad_code = '" + filterWhere.where.ad_code + "') as summary ";
      }

      //   // "limit": 100,
      //   //   "skip": 0,
      var skips = "";
      if (filterWhere.limit != undefined && filterWhere.skip != undefined) {
        skips = "LIMIT " + filterWhere.skip + "," + filterWhere.limit;
      }

      if (filterWhere.limit != undefined && filterWhere.skip == undefined) {
        skips = "LIMIT 0," + filterWhere.limit;
      }

      if (filterWhere.where.periode != undefined) {
        data["success"] = true;
        var summaryDetail = await this.dataDigiposRepository.execute(
          "SELECT " +
          "summary.periode, " +
          "summary.ad_code, " +
          "summary.dealer_code, " +
          "summary.location, " +
          "summary.name, " +
          "summary.type_trx, " +
          "summary.group_amount,  " +
          "sum(summary.trx_a) as trx_a_sum, " +
          "sum(summary.tot_mdr_a) as tot_mdr_a_sum, " +
          "sum(summary.trx_b) as trx_b_sum, " +
          "sum(summary.tot_mdr_b) as tot_mdr_b_sum " +
          "FROM " +
          "(SELECT " +
          "DISTINCT(DataSumarry.id), " +
          "AdCluster.ad_code, " +
          "DataSumarry.dealer_code," +
          "DataSumarry.location, " +
          "DataSumarry.name, " +
          "DataSumarry.type_trx, " +
          "IF(DataSumarry.paid_in_amount = 0, DataSumarry.withdraw_amount , DataSumarry.paid_in_amount) as group_amount, " +
          "DataSumarry.trx_a, " +
          "DataSumarry.tot_mdr_a, " +
          "DataSumarry.trx_b, " +
          "DataSumarry.tot_mdr_b, " +
          "DataSumarry.status, " +
          "DataSumarry.periode, " +
          "DataSumarry.create_at " +
          "FROM " +
          "DataSumarry " +
          "INNER JOIN " +
          "AdCluster " +
          "on " +
          "DataSumarry.dealer_code = AdCluster.ad_cluster_id " +
          thisWhere +
          "GROUP BY summary.dealer_code ,summary.group_amount,summary.type_trx " + skips);

        const configData = await this.dataconfigRepository.find({where: {config_group_id: 1}});
        var configNum: any;
        configNum = {};
        for (var i in configData) {
          var infoConfig = configData[i];
          if (infoConfig.config_code == "0001-01") {
            configNum["persen_fee"] = infoConfig.config_value;
          }
          if (infoConfig.config_code == "0001-02") {
            configNum["persen_paid"] = infoConfig.config_value;
          }
          if (infoConfig.config_code == "0001-03") {
            configNum["num_dpp_fee"] = infoConfig.config_value;
          }
          if (infoConfig.config_code == "0001-04") {
            configNum["num_ppn_fee"] = infoConfig.config_value;
          }
          if (infoConfig.config_code == "0001-05") {
            configNum["num_pph_fee"] = infoConfig.config_value;
          }

          if (infoConfig.config_code == "0001-11") {
            configNum["num_dpp_paid"] = infoConfig.config_value;
          }

          if (infoConfig.config_code == "0001-12") {
            configNum["num_ppn_paid"] = infoConfig.config_value;
          }

          if (infoConfig.config_code == "0001-13") {
            configNum["num_pph_paid"] = infoConfig.config_value;
          }

        }


        var infoSummaryDet: any;
        infoSummaryDet = [];

        for (var i in summaryDetail) {

          var infoDetail = summaryDetail[i];

          var trxFee = ((infoDetail.trx_a_sum * infoDetail.group_amount) * (configNum.persen_fee));
          var feeDPP = (trxFee / configNum.num_dpp_fee);
          var feePPN = (feeDPP * configNum.num_ppn_fee);
          var feeTotal = feeDPP + feePPN;
          var feePPH = (feeDPP * configNum.num_pph_fee);

          var trxPaid = ((infoDetail.trx_b_sum * infoDetail.group_amount) * (configNum.persen_paid));
          var paidDPP = (trxPaid / configNum.num_dpp_paid);
          var paidPPN = (paidDPP * configNum.num_ppn_paid);
          var paidTotal = paidDPP + paidPPN;
          var paidPPH = (paidDPP * configNum.num_pph_paid);

          infoSummaryDet.push({
            trx_date_group: infoDetail.periode,
            dealer_code: infoDetail.dealer_code,
            ad_code: infoDetail.ad_code,
            ad_name: infoDetail.name,
            type_trx: infoDetail.type_trx,
            sales_fee: infoDetail.group_amount,
            trx_cluster_out: infoDetail.trx_a_sum,
            mdr_fee_amount: trxFee,
            dpp_fee_amount: feeDPP,
            vat_fee_amount: feePPN,
            total_fee_amount: feeTotal,
            pph_fee_amount: feePPH,
            trx_paid_invoice: infoDetail.trx_b_sum,
            paid_inv_amount: trxPaid,
            dpp_paid_amount: paidDPP,
            vat_paid_amount: paidPPN,
            total_paid_amount: paidTotal,
            pph_paid_amount: paidPPH
          });
        }

        var summaryTotalAll = await this.dataDigiposRepository.execute(
          "SELECT " +
          "summary.periode, " +
          "summary.ad_code, " +
          "summary.dealer_code, " +
          "summary.location, " +
          "summary.name, " +
          "summary.type_trx, " +
          "summary.group_amount,  " +
          "sum(summary.trx_a) as trx_a_sum, " +
          "sum(summary.tot_mdr_a) as tot_mdr_a_sum, " +
          "sum(summary.trx_b) as trx_b_sum, " +
          "sum(summary.tot_mdr_b) as tot_mdr_b_sum " +
          "FROM " +
          "(SELECT " +
          "DISTINCT(DataSumarry.id), " +
          "AdCluster.ad_code, " +
          "DataSumarry.dealer_code," +
          "DataSumarry.location, " +
          "DataSumarry.name, " +
          "DataSumarry.type_trx, " +
          "IF(DataSumarry.paid_in_amount = 0, DataSumarry.withdraw_amount , DataSumarry.paid_in_amount) as group_amount, " +
          "DataSumarry.trx_a, " +
          "DataSumarry.tot_mdr_a, " +
          "DataSumarry.trx_b, " +
          "DataSumarry.tot_mdr_b, " +
          "DataSumarry.status, " +
          "DataSumarry.periode, " +
          "DataSumarry.create_at " +
          "FROM " +
          "DataSumarry " +
          "INNER JOIN " +
          "AdCluster " +
          "on " +
          "DataSumarry.dealer_code = AdCluster.ad_cluster_id " +
          thisWhere +
          "GROUP BY summary.ad_code, summary.group_amount, summary.type_trx");

        var TotalAlltrxFee = 0;
        var TotalAllfeeDPP = 0;
        var TotalAllfeePPN = 0;
        var TotalAllfeeTotal = 0;
        var TotalAllfeePPH = 0;

        var TotalAlltrxPaid = 0;
        var TotalAllpaidDPP = 0;
        var TotalAllpaidPPN = 0;
        var TotalAllpaidTotal = 0;
        var TotalAllpaidPPH = 0;

        for (var i in summaryTotalAll) {
          var infoTotal = summaryTotalAll[i];
          var trxFeeTotal = ((infoTotal.trx_a_sum * infoTotal.group_amount) * (configNum.persen_fee));
          var feeDPPTotal = (trxFeeTotal / configNum.num_dpp_fee);
          var feePPNTotal = (feeDPPTotal * configNum.num_ppn_fee);
          var feeTotalTotal = feeDPPTotal + feePPNTotal;
          var feePPHTotal = (feeDPPTotal * configNum.num_pph_fee);

          var trxPaidTotal = ((infoTotal.trx_b_sum * infoTotal.group_amount) * (configNum.persen_paid));
          var paidDPPTotal = (trxPaidTotal / configNum.num_dpp_paid);
          var paidPPNTotal = (paidDPPTotal * configNum.num_ppn_paid);
          var paidTotalTotal = paidDPPTotal + paidPPNTotal;
          var paidPPHTotal = (paidDPPTotal * configNum.num_pph_paid);

          TotalAlltrxFee = TotalAlltrxFee + infoTotal.trx_a_sum;
          TotalAllfeeDPP = TotalAllfeeDPP + feeDPPTotal;
          TotalAllfeePPN = TotalAllfeePPN + feePPNTotal;
          TotalAllfeeTotal = TotalAllfeeTotal + feeTotalTotal;
          TotalAllfeePPH = TotalAllfeePPH + feePPHTotal;

          TotalAlltrxPaid = TotalAlltrxPaid + infoTotal.trx_b_sum;
          TotalAllpaidDPP = TotalAllpaidDPP + paidDPPTotal;
          TotalAllpaidPPN = TotalAllpaidPPN + paidPPNTotal;
          TotalAllpaidTotal = TotalAllpaidTotal + paidTotalTotal;
          TotalAllpaidPPH = TotalAllpaidPPH + paidPPHTotal;
        }



        data["datas"] = infoSummaryDet;

        data["TotalAlltrxFee"] = TotalAlltrxFee;
        data["TotalAllfeeDPP"] = TotalAllfeeDPP;
        data["TotalAllfeePPN"] = TotalAllfeePPN;
        data["TotalAllfeeTotal"] = TotalAllfeeTotal;
        data["TotalAllfeePPH"] = TotalAllfeePPH;

        data["TotalAlltrxPaid"] = TotalAlltrxPaid;
        data["TotalAllpaidDPP"] = TotalAllpaidDPP;
        data["TotalAllpaidPPN"] = TotalAllpaidPPN;
        data["TotalAllpaidTotal"] = TotalAllpaidTotal;
        data["TotalAllpaidPPH"] = TotalAllpaidPPH;

      }
    }
    return data;
  }

  @patch('/data-digipos/{id}', {
    responses: {
      '204': {
        description: 'DataDigipos PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(DataDigipos, {partial: true}),
        },
      },
    })
    dataDigipos: DataDigipos,
  ): Promise<void> {
    await this.dataDigiposRepository.updateById(id, dataDigipos);
  }

  @put('/data-digipos/{id}', {
    responses: {
      '204': {
        description: 'DataDigipos PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() dataDigipos: DataDigipos,
  ): Promise<void> {
    await this.dataDigiposRepository.replaceById(id, dataDigipos);
  }

  @del('/data-digipos/{id}', {
    responses: {
      '204': {
        description: 'DataDigipos DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.dataDigiposRepository.deleteById(id);
  }



  @get('/data-digipos/faktur-pajak', {
    responses: {
      '200': {
        description: 'Array of DataDigipos model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(DataDigipos, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async findFakturPajak(
    @param.filter(DataDigipos) filter?: Filter<DataDigipos>,
  ): Promise<Object> {
    var info: any; info = {"datas": new Array(), "size": 0};
    var filterOrder: any; filterOrder = {};
    if (filter?.where !== undefined) {
      filterOrder = {"where": {and: [filter?.where, {vat_number: {neq: ""}}]}};
      filter.where = filterOrder["where"];
    } else {
      filterOrder = {"where": {vat_number: {neq: ""}}};
      var filterWhere: any; filterWhere = {};
      filterWhere = filter;
      filterWhere["where"] = filterOrder["where"];
      filter = filterWhere;
    }

    var infoFileSyc = await this.dataDigiposRepository.find(filter);
    var sizeFileSyc = await this.dataDigiposRepository.find(filterOrder);

    var total_trx_cluster_out = 0;
    var total_mdr_fee_amount = 0;
    var total_dpp_fee_amount = 0;
    var total_vat_fee_amount = 0;
    var total_total_fee_amount = 0;
    var total_pph_fee_amount = 0;
    var total_trx_paid_invoice = 0;
    var total_paid_inv_amount = 0;
    var total_inv_adj_amount = 0;
    var total_dpp_paid_amount = 0;
    var total_vat_paid_amount = 0;
    var total_total_paid_amount = 0;
    var total_pph_paid_amount = 0;

    var DigiposList: any; DigiposList = [];
    var no = 1;
    for (var i in infoFileSyc) {
      var digiposList = infoFileSyc[i];
      var data: any; data = {};
      var adMaster = await this.adMasterRepository.findById(digiposList.ad_code);
      var periode = digiposList.trx_date_group.split("-");
      data["INFO"] = {
        trx_date_group: digiposList.trx_date_group,
        ad_code: digiposList.ad_code,
        ad_name: digiposList.ad_name,
        inv_number: digiposList.inv_number,
        inv_attacment: digiposList.inv_attacment,
        vat_number: digiposList.vat_number,
        vat_attachment: digiposList.vat_attachment,
        ba_number_b: digiposList.ba_number_b,
        ba_attachment_b: digiposList.ba_attachment_b
      };

      var dispute = await this.disputeRepository.find({where: {and: [{periode: digiposList.trx_date_group}, {acc_ba: 1}, {ad_code: digiposList.ad_code}]}});
      var DppDispute = 0;
      var PpnDispute = 0;
      var TotDispute = 0;
      var PphDispute = 0;
      for (var iDispute in dispute) {
        var infoDispute = dispute[iDispute];
        DppDispute = DppDispute + infoDispute.dpp_paid_amount;
        PpnDispute = PpnDispute + infoDispute.ppn_paid_amount;
        PphDispute = PphDispute + infoDispute.pph_paid_amount;
        TotDispute = TotDispute + infoDispute.tot_mdr_b;
      }

      var perfix: string; perfix = "";
      var fakturNo: string; fakturNo = "";
      var fakturTxt = digiposList.vat_number == null || digiposList.vat_number == undefined || digiposList.vat_number == "" ? "" : digiposList.vat_number;
      if (fakturTxt != "") {
        var explodeNoFaktur = fakturTxt.split(".");
        if (explodeNoFaktur.length > 0) {
          perfix = explodeNoFaktur[0];
        }
        fakturNo = fakturTxt.substring(perfix.length, fakturTxt.length).split(".").join("").split("-").join("");
      }

      var PeriodeTrx = digiposList.trx_date_group.split("-");
      var d = new Date(digiposList.trx_date_group + "-" + "26");
      var dateLast = new Date(parseInt(PeriodeTrx[0]), parseInt(PeriodeTrx[1]), 0);
      var dateInvZero = "00";
      var dateInv = dateLast.getDate();
      var dateInvoice = dateInvZero.substring(dateInv.toString().length, 2) + dateLast.getDate() + "/" + dateInvZero.substring((dateLast.getMonth() + 1).toString().length, 2) + (dateLast.getMonth() + 1) + "/" + dateLast.getFullYear();

      data["FK"] = {
        perfix: perfix.substring(0, 2),
        fg_pengganti: 0,
        no_faktur: fakturNo,
        bulan: periode[1],
        tahun: periode[0],
        tanggal: dateInvoice,
        npwp: adMaster.npwp.split(".").join("").split("-").join(""),
        ad_name: adMaster.ad_name.toUpperCase(),
        address: adMaster.npwp_address.toUpperCase(),
        dpp: parseInt((digiposList.dpp_paid_amount + DppDispute).toFixed(0)),
        ppn: parseInt((digiposList.vat_paid_amount + PpnDispute).toFixed(0)),
        referensi: digiposList.inv_number
      }

      var monthSet = parseInt(periode[1]) - 1;
      var dateText = ControllerConfig.onGetMonth(monthSet);
      data["OF"] = {
        no: no,
        name: "SALES FEE " + dateText.toString().toUpperCase() + " " + periode[0],
        satuan: digiposList.dpp_paid_amount + DppDispute,
        jumlah: 1,
        total: parseInt((digiposList.total_paid_amount + DppDispute).toFixed(0)),
        dpp: parseInt((digiposList.dpp_paid_amount + DppDispute).toFixed(0)),
        ppn: parseInt((digiposList.vat_paid_amount + PpnDispute).toFixed(0))
      }
      DigiposList.push(data);
      no++;
    }

    for (var i in sizeFileSyc) {
      var infoData = sizeFileSyc[i];

      var dispute = await this.disputeRepository.find({where: {and: [{periode: infoData.trx_date_group}, {acc_ba: 1}, {ad_code: infoData.ad_code}]}});
      var DppDisputeFee = 0;
      var PpnDisputeFee = 0;
      var PphDisputeFee = 0;
      var TotDisputeFee = 0;
      var TrxFee = 0;

      var DppDisputePaid = 0;
      var PpnDisputePaid = 0;
      var PphDisputePaid = 0;
      var TotDisputePaid = 0;
      var TrxPaid = 0;
      for (var iDispute in dispute) {
        var infoDispute = dispute[iDispute];
        DppDisputeFee = DppDisputeFee + infoDispute.dpp_fee_amount;
        PpnDisputeFee = PpnDisputeFee + infoDispute.ppn_fee_amount;
        PphDisputeFee = PphDisputeFee + infoDispute.pph_fee_amount;
        TotDisputeFee = TotDisputeFee + infoDispute.tot_mdr_a;
        TrxFee = TrxFee + infoDispute.trx_a;

        DppDisputePaid = DppDisputePaid + infoDispute.dpp_paid_amount;
        PpnDisputePaid = PpnDisputePaid + infoDispute.ppn_paid_amount;
        PphDisputePaid = PphDisputePaid + infoDispute.pph_paid_amount;
        TotDisputePaid = TotDisputePaid + infoDispute.tot_mdr_b;
        TrxPaid = TrxPaid + infoDispute.trx_b;
      }


      total_trx_cluster_out = total_trx_cluster_out + infoData.trx_cluster_out + TrxFee;
      total_mdr_fee_amount = total_mdr_fee_amount + infoData.mdr_fee_amount + TotDisputeFee;
      total_dpp_fee_amount = total_dpp_fee_amount + infoData.dpp_fee_amount + DppDisputePaid;
      total_vat_fee_amount = total_vat_fee_amount + infoData.vat_fee_amount + PpnDisputePaid;
      total_total_fee_amount = total_total_fee_amount + infoData.total_fee_amount + TotDisputeFee;
      total_pph_fee_amount = total_pph_fee_amount + infoData.pph_fee_amount + PphDisputeFee;
      total_trx_paid_invoice = total_trx_paid_invoice + infoData.trx_paid_invoice + TrxPaid;
      total_paid_inv_amount = total_paid_inv_amount + infoData.paid_inv_amount + TotDisputePaid;
      total_inv_adj_amount = total_inv_adj_amount + infoData.inv_adj_amount;
      total_dpp_paid_amount = total_dpp_paid_amount + infoData.dpp_paid_amount + DppDisputePaid;
      total_vat_paid_amount = total_vat_paid_amount + infoData.vat_paid_amount + PpnDisputePaid;
      total_total_paid_amount = total_total_paid_amount + infoData.total_paid_amount + TotDisputePaid;
      total_pph_paid_amount = total_pph_paid_amount + infoData.pph_paid_amount + PphDisputePaid;
    }

    info["datas"] = DigiposList;
    info["size"] = sizeFileSyc.length;
    info["total_trx_cluster_out"] = total_trx_cluster_out;
    info["total_mdr_fee_amount"] = total_mdr_fee_amount;
    info["total_dpp_fee_amount"] = total_dpp_fee_amount;
    info["total_vat_fee_amount"] = total_vat_fee_amount;
    info["total_total_fee_amount"] = total_total_fee_amount;
    info["total_pph_fee_amount"] = total_pph_fee_amount;
    info["total_trx_paid_invoice"] = total_trx_paid_invoice;
    info["total_paid_inv_amount"] = total_paid_inv_amount;
    info["total_inv_adj_amount"] = total_inv_adj_amount;
    info["total_dpp_paid_amount"] = total_dpp_paid_amount;
    info["total_vat_paid_amount"] = total_vat_paid_amount;
    info["total_total_paid_amount"] = total_total_paid_amount;
    info["total_pph_paid_amount"] = total_pph_paid_amount;
    return info;
  }

}
