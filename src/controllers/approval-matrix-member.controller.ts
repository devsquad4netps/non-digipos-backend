import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where
} from '@loopback/repository';
import {
  del, get,
  getModelSchemaRef, param,


  patch, post,




  put,

  requestBody
} from '@loopback/rest';
import {ApprovalMatrixMember} from '../models';
import {ApprovalMatrixMemberRepository, UserRepository} from '../repositories';

export class ApprovalMatrixMemberController {
  constructor(
    @repository(ApprovalMatrixMemberRepository)
    public approvalMatrixMemberRepository: ApprovalMatrixMemberRepository,
    @repository(UserRepository)
    public userRepository: UserRepository,
  ) { }

  @post('/approval-matrix-members', {
    responses: {
      '200': {
        description: 'ApprovalMatrixMember model instance',
        content: {'application/json': {schema: getModelSchemaRef(ApprovalMatrixMember)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ApprovalMatrixMember, {
            title: 'NewApprovalMatrixMember',
            exclude: ['amm_id'],
          }),
        },
      },
    })
    approvalMatrixMember: Omit<ApprovalMatrixMember, 'amm_id'>,
  ): Promise<ApprovalMatrixMember> {
    return this.approvalMatrixMemberRepository.create(approvalMatrixMember);
  }

  @get('/approval-matrix-members/count', {
    responses: {
      '200': {
        description: 'ApprovalMatrixMember model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(ApprovalMatrixMember) where?: Where<ApprovalMatrixMember>,
  ): Promise<Count> {
    return this.approvalMatrixMemberRepository.count(where);
  }

  @get('/approval-matrix-members', {
    responses: {
      '200': {
        description: 'Array of ApprovalMatrixMember model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(ApprovalMatrixMember, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(ApprovalMatrixMember) filter?: Filter<ApprovalMatrixMember>,
  ): Promise<Object> {
    // userRepository
    var datas: any; datas = [];
    var matrixMember = await this.approvalMatrixMemberRepository.find(filter);
    for (var i in matrixMember) {
      var infoMatrixMember = matrixMember[i];
      var infoUser = await this.userRepository.findById(infoMatrixMember.amm_userid);
      datas.push({
        amg_id: infoMatrixMember.amg_id,
        amm_id: infoMatrixMember.amm_id,
        amm_state: infoMatrixMember.amm_state,
        amm_userid: infoMatrixMember.amm_userid,
        at_create: infoMatrixMember.at_create,
        usr_info: infoUser
      });
    }
    return datas;
  }

  @patch('/approval-matrix-members', {
    responses: {
      '200': {
        description: 'ApprovalMatrixMember PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ApprovalMatrixMember, {partial: true}),
        },
      },
    })
    approvalMatrixMember: ApprovalMatrixMember,
    @param.where(ApprovalMatrixMember) where?: Where<ApprovalMatrixMember>,
  ): Promise<Count> {
    return this.approvalMatrixMemberRepository.updateAll(approvalMatrixMember, where);
  }

  @get('/approval-matrix-members/{id}', {
    responses: {
      '200': {
        description: 'ApprovalMatrixMember model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(ApprovalMatrixMember, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(ApprovalMatrixMember, {exclude: 'where'}) filter?: FilterExcludingWhere<ApprovalMatrixMember>
  ): Promise<ApprovalMatrixMember> {
    return this.approvalMatrixMemberRepository.findById(id, filter);
  }

  @patch('/approval-matrix-members/{id}', {
    responses: {
      '204': {
        description: 'ApprovalMatrixMember PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ApprovalMatrixMember, {partial: true}),
        },
      },
    })
    approvalMatrixMember: ApprovalMatrixMember,
  ): Promise<void> {
    await this.approvalMatrixMemberRepository.updateById(id, approvalMatrixMember);
  }

  @put('/approval-matrix-members/{id}', {
    responses: {
      '204': {
        description: 'ApprovalMatrixMember PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() approvalMatrixMember: ApprovalMatrixMember,
  ): Promise<void> {
    await this.approvalMatrixMemberRepository.replaceById(id, approvalMatrixMember);
  }

  @del('/approval-matrix-members/{id}', {
    responses: {
      '204': {
        description: 'ApprovalMatrixMember DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.approvalMatrixMemberRepository.deleteById(id);
  }
}
