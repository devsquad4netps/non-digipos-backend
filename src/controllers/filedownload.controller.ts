// Uncomment these imports to begin using these cool features!

// import {inject} from '@loopback/core';

import {BigQuery, BigQueryDate} from '@google-cloud/bigquery';
import {authenticate} from '@loopback/authentication';
import {inject} from '@loopback/core';
import {repository} from '@loopback/repository';
import {get, post, requestBody} from '@loopback/rest';
import fs from 'fs';
import {promisify} from 'util';
import {STORAGE_DIRECTORY} from '../keys';
import {AdClusterRepository, AdMasterRepository, DataconfigRepository, DataDigiposDetailRepository, DataDigiposRepository, DataPppobDetailRepository, DataRechargeDetailRepository, DataSharePercentRepository, DataSumarryRepository} from '../repositories';
import {ControllerConfig} from './controller.config';
const readdir = promisify(fs.readdir);



export const PeriodeRequestBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['periode'],
        properties: {
          periode: {
            type: 'string',
          },
        }
      }
    },
  },
};


export const UniqueKeyRequestBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['unique_key'],
        properties: {
          unique_key: {
            type: 'string',
          },
        }
      }
    },
  },
};



export const PeriodeDetailRequestBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['periode'],
        properties: {
          periode: {
            type: 'string',
          },
          ad_code: {
            type: 'string',
          }
        }
      }
    },
  },
};



//======================================================DECLARE=================
// export const BigQueryReqPeriode: ResponseObject = {
//   description: 'The input of login function',
//   required: true,
//   content: {
//     'application/json': {
//       schema: {
//         type: 'object',
//         required: ['periode'],
//         properties: {
//           periode: {
//             type: 'string',
//           },
//         }
//       }
//     },
//   },
// };


// export const BigQueryReqCreateMultiClusterTmp: ResponseObject = {
//   description: 'The input of login function',
//   required: true,
//   content: {
//     'application/json': {
//       schema: {
//         type: 'object',
//         required: ['cluster'],
//         properties: {
//           cluster: {
//             type: 'array',
//             items: {type: 'string'},
//           }
//         }
//       }
//     },
//   },
// };

// export const BigQueryReqCreateClusterTmp: ResponseObject = {
//   description: 'The input of login function',
//   required: true,
//   content: {
//     'application/json': {
//       schema: {
//         type: 'object',
//         required: ['before_cluster', 'after_cluster'],
//         properties: {
//           before_cluster: {
//             type: 'string',
//           },
//           after_cluster: {
//             type: 'string',
//           },
//         }
//       }
//     },
//   },
// };



// export const BigQueryReqDeleteClusterTmp: ResponseObject = {
//   description: 'The input of login function',
//   required: true,
//   content: {
//     'application/json': {
//       schema: {
//         type: 'object',
//         required: ['before_cluster'],
//         properties: {
//           before_cluster: {
//             type: 'string',
//           },
//         }
//       }
//     },
//   },
// };


// export const BigQueryReqClusterSkip: ResponseObject = {
//   description: 'The input of login function',
//   required: true,
//   content: {
//     'application/json': {
//       schema: {
//         type: 'object',
//         required: ['cluster'],
//         properties: {
//           cluster: {
//             type: 'string',
//           },
//         }
//       }
//     },
//   },
// };

var DataRawRechargeIndex: any; DataRawRechargeIndex = [];
var DataRawRechargeSelected: any; DataRawRechargeSelected = [];
var DataRawRecharge: any; DataRawRecharge = [];

var DataSummaryRecharge: any; DataSummaryRecharge = [];


var DataRawPPOBIndex: any; DataRawPPOBIndex = [];
var DataRawPPOBSelected: any; DataRawPPOBSelected = [];
var DataRawPPOB: any; DataRawPPOB = [];

var DataSummaryPPOB: any; DataSummaryPPOB = [];


var TOTAL_TRANS_AD_A = 0;
var TOTAL_TRANS_AD_B = 0;
var TOTAL_AD: any; TOTAL_AD = [];

var DATA_FINAL: any; DATA_FINAL = {};

var PERIODE: any; PERIODE = null;



var DataDetailForRecharge: any; DataDetailForRecharge = {};
var DataDetailForPPOB: any; DataDetailForPPOB = {};



var CLUSTER_DECLARE: any; CLUSTER_DECLARE = {};
var CLUSTER_SKIP: any; CLUSTER_SKIP = [];

@authenticate('jwt')
export class FiledownloadController {
  constructor(
    @inject(STORAGE_DIRECTORY) private storageDirectory: string,
    @repository(DataconfigRepository) public dataconfigRepository: DataconfigRepository,
    @repository(AdClusterRepository) public adClusterRepository: AdClusterRepository,
    @repository(AdMasterRepository) public adMasterRepository: AdMasterRepository,
    @repository(DataDigiposRepository) public dataDigiposRepository: DataDigiposRepository,
    @repository(DataDigiposDetailRepository) public dataDigiposDetailRepository: DataDigiposDetailRepository,
    @repository(DataSharePercentRepository) public dataSharePercentRepository: DataSharePercentRepository,
    @repository(DataPppobDetailRepository) public dataPppobDetailRepository: DataPppobDetailRepository,
    @repository(DataRechargeDetailRepository) public dataRechargeDetailRepository: DataRechargeDetailRepository,
    @repository(DataSumarryRepository) public dataSumarryRepository: DataSumarryRepository
  ) { }


  @post('/big-query/inquery/import-final', {
    responses: {
      '200': {
        description: 'Invoice model instance',
        content: {
          'application/json': {
            schema: PeriodeRequestBody
          }
        },
      },
    },
  })
  async rawInqueryInportFinalBQ(
    @requestBody(PeriodeRequestBody) periodeReq: {periode: string},
  ): Promise<Object> {
    var returnMs: any; returnMs = {success: true, message: "success"};
    var rawReject = await this.dataDigiposRepository.find({where: {and: [{trx_date_group: PERIODE}, {ar_stat: {inq: [-1, -2]}}]}});
    var rawHave = await this.dataDigiposRepository.find({where: {and: [{trx_date_group: PERIODE}]}});

    if (PERIODE != null) {
      // DATA_FINAL
      if (rawReject.length > 0 || rawHave.length == 0) {

        const configData = await this.dataconfigRepository.find({where: {config_group_id: 1}});
        var configNum: any;
        configNum = {};
        for (var i in configData) {
          var infoConfig = configData[i];
          if (infoConfig.config_code == "0001-01") {
            configNum["persen_fee"] = infoConfig.config_value;
          }
          if (infoConfig.config_code == "0001-02") {
            configNum["persen_paid"] = infoConfig.config_value;
          }

          if (infoConfig.config_code == "0001-03") {
            configNum["num_dpp_fee"] = infoConfig.config_value;
          }
          if (infoConfig.config_code == "0001-04") {
            configNum["num_ppn_fee"] = infoConfig.config_value;
          }
          if (infoConfig.config_code == "0001-05") {
            configNum["num_pph_fee"] = infoConfig.config_value;
          }

          if (infoConfig.config_code == "0001-11") {
            configNum["num_dpp_paid"] = infoConfig.config_value;
          }
          if (infoConfig.config_code == "0001-12") {
            configNum["num_ppn_paid"] = infoConfig.config_value;
          }
          if (infoConfig.config_code == "0001-13") {
            configNum["num_pph_paid"] = infoConfig.config_value;
          }

          if (infoConfig.config_code == "0001-06") {
            configNum["persen_fee_ppob"] = infoConfig.config_value;
          }
          if (infoConfig.config_code == "0001-07") {
            configNum["persen_paid_ppob"] = infoConfig.config_value;
          }
          if (infoConfig.config_code == "0001-08") {
            configNum["num_dpp_ppob"] = infoConfig.config_value;
          }
          if (infoConfig.config_code == "0001-09") {
            configNum["num_ppn_ppob"] = infoConfig.config_value;
          }
          if (infoConfig.config_code == "0001-10") {
            configNum["num_pph_ppob"] = infoConfig.config_value;
          }
        }

        try {
          var InfoShare = await this.dataSharePercentRepository.findOne({where: {periode: PERIODE}});
          if (InfoShare == null) {
            await this.dataSharePercentRepository.create({
              periode: PERIODE,
              fee_ppob_share: parseFloat(configNum.persen_fee_ppob),
              paid_ppob_share: parseFloat(configNum.persen_paid_ppob),
              dpp_ppob_share: parseFloat(configNum.num_dpp_ppob),
              ppn_ppob_share: parseFloat(configNum.num_ppn_ppob),
              pph_ppob_share: parseFloat(configNum.num_pph_ppob),

              fee_recharce_share: parseFloat(configNum.persen_fee),
              paid_recharge_share: parseFloat(configNum.persen_paid),

              dpp_recharge_share_fee: parseFloat(configNum.num_dpp_fee),
              ppn_recharge_share_fee: parseFloat(configNum.num_ppn_fee),
              pph_recharge_share_fee: parseFloat(configNum.num_pph_fee),

              dpp_recharge_share_paid: parseFloat(configNum.num_dpp_paid),
              ppn_recharge_share_paid: parseFloat(configNum.num_ppn_paid),
              pph_recharge_share_paid: parseFloat(configNum.num_pph_paid)
            });
          } else {
            await this.dataSharePercentRepository.updateById(InfoShare.id, {
              periode: PERIODE,
              fee_ppob_share: parseFloat(configNum.persen_fee_ppob),
              paid_ppob_share: parseFloat(configNum.persen_paid_ppob),
              dpp_ppob_share: parseFloat(configNum.num_dpp_ppob),
              ppn_ppob_share: parseFloat(configNum.num_ppn_ppob),
              pph_ppob_share: parseFloat(configNum.num_pph_ppob),

              fee_recharce_share: parseFloat(configNum.persen_fee),
              paid_recharge_share: parseFloat(configNum.persen_paid),

              dpp_recharge_share_fee: parseFloat(configNum.num_dpp_fee),
              ppn_recharge_share_fee: parseFloat(configNum.num_ppn_fee),
              pph_recharge_share_fee: parseFloat(configNum.num_pph_fee),

              dpp_recharge_share_paid: parseFloat(configNum.num_dpp_paid),
              ppn_recharge_share_paid: parseFloat(configNum.num_ppn_paid),
              pph_recharge_share_paid: parseFloat(configNum.num_pph_paid)

            })
          }
        } catch (errorSharePercent) {
          returnMs.success = false;
          returnMs.message = errorSharePercent;
        }

        //clear data
        for (var i in rawHave) {
          await this.dataDigiposRepository.deleteById(rawHave[i].digipost_id);
          await this.dataDigiposDetailRepository.deleteAll({digipost_id: rawHave[i].digipost_id});
        }
        await this.dataRechargeDetailRepository.deleteAll({and: [{periode: PERIODE}]});
        await this.dataRechargeDetailRepository.deleteAll({and: [{periode: PERIODE}]});
        await this.dataSumarryRepository.deleteAll({and: [{periode: PERIODE}]});

        for (var iDF in DATA_FINAL) {
          var rawFinal = DATA_FINAL[iDF];
          try {
            var digiposInsert = await this.dataDigiposRepository.create({
              trx_date_group: PERIODE,
              ad_code: rawFinal.ad_code,
              ad_name: rawFinal.name,
              inv_number: "",
              inv_attacment: "",
              ba_number_a: "",
              ba_attachment_a: "",
              ba_number_b: "",
              ba_attachment_b: "",
              vat_number: "",
              vat_attachment: "",
              inv_desc: "",
              sales_fee: 0,
              trx_cluster_out: rawFinal.trx_a,
              mdr_fee_amount: rawFinal.tot_mdr_a,
              dpp_fee_amount: rawFinal.dpp_fee,
              vat_fee_amount: rawFinal.ppn_fee,
              total_fee_amount: rawFinal.total_fee,
              pph_fee_amount: rawFinal.pph_fee,
              trx_paid_invoice: rawFinal.trx_b,
              paid_inv_amount: rawFinal.tot_mdr_b,
              inv_adj_amount: 0,
              dpp_paid_amount: rawFinal.dpp_paid,
              vat_paid_amount: rawFinal.ppn_paid,
              total_paid_amount: rawFinal.total_paid,
              pph_paid_amount: rawFinal.pph_paid,
              remark: "-",
              ar_stat: 0,
              response_stat: true
            });


            if (rawFinal.PPOB.trx_a != undefined) {
              await this.dataDigiposDetailRepository.create({
                trx_date_group: PERIODE,
                ad_code: rawFinal.ad_code,
                ad_name: rawFinal.name,
                type_trx: "PPOB",
                sales_fee: rawFinal.PPOB.sales_fee,
                trx_cluster_out: rawFinal.PPOB.trx_a,
                mdr_fee_amount: rawFinal.PPOB.tot_mdr_a,
                dpp_fee_amount: rawFinal.PPOB.dpp_fee,
                vat_fee_amount: rawFinal.PPOB.ppn_fee,
                total_fee_amount: rawFinal.PPOB.total_fee,
                pph_fee_amount: rawFinal.PPOB.pph_fee,
                trx_paid_invoice: rawFinal.PPOB.trx_b,
                paid_inv_amount: rawFinal.PPOB.tot_mdr_b,
                inv_adj_amount: 0,
                dpp_paid_amount: rawFinal.PPOB.dpp_paid,
                vat_paid_amount: rawFinal.PPOB.ppn_paid,
                total_paid_amount: rawFinal.PPOB.total_paid,
                pph_paid_amount: rawFinal.PPOB.pph_paid,
                digipost_id: digiposInsert.digipost_id
              });
            }


            if (rawFinal.RECHARGE.trx_a != undefined) {
              await this.dataDigiposDetailRepository.create({
                trx_date_group: PERIODE,
                ad_code: rawFinal.ad_code,
                ad_name: rawFinal.name,
                type_trx: "RECHARGE",
                sales_fee: rawFinal.RECHARGE.sales_fee,
                trx_cluster_out: rawFinal.RECHARGE.trx_a,
                mdr_fee_amount: rawFinal.RECHARGE.tot_mdr_a,
                dpp_fee_amount: rawFinal.RECHARGE.dpp_fee,
                vat_fee_amount: rawFinal.RECHARGE.ppn_fee,
                total_fee_amount: rawFinal.RECHARGE.total_fee,
                pph_fee_amount: rawFinal.RECHARGE.pph_fee,
                trx_paid_invoice: rawFinal.RECHARGE.trx_b,
                paid_inv_amount: rawFinal.RECHARGE.tot_mdr_b,
                inv_adj_amount: 0,
                dpp_paid_amount: rawFinal.RECHARGE.dpp_paid,
                vat_paid_amount: rawFinal.RECHARGE.ppn_paid,
                total_paid_amount: rawFinal.RECHARGE.total_paid,
                pph_paid_amount: rawFinal.RECHARGE.pph_paid,
                digipost_id: digiposInsert.digipost_id
              });
            }

          } catch (error) {
            returnMs.success = false;
            returnMs.message = error;
          }
        }

        //CREATE DETAIL RECHARGE
        var CreateDetailRecharge: any; CreateDetailRecharge = {};
        var KeyCreateRechargeLength = [];
        var NoProcess = 1;
        for (var iDRecharge in DataRawRecharge) {
          var rawDRecharge = DataRawRecharge[iDRecharge];
          if (DataRawRechargeSelected.indexOf(rawDRecharge.unique_key) == -1) {
            continue;
          }

          if (rawDRecharge.ad_code == "-") {
            continue;
          }

          var Code = "-";
          if (rawDRecharge.dealer_code == "-") {
            Code = rawDRecharge.ad_code;
            if (CreateDetailRecharge[Code] == undefined) {
              KeyCreateRechargeLength.push(Code);
              CreateDetailRecharge[Code] = {
                periode: PERIODE,
                dealer_code: Code,
                location: "",
                name: "",
                type_trx: "RECHARGE",
                paid_in_amount: 0,
                withdraw_amount: 0,
                reason_type_id: 0,
                reason_type_name: "",
                trx_a: 0,
                tot_mdr_a: 0,
                trx_b: 0,
                tot_mdr_b: 0,
                status: 0,
                create_at: ControllerConfig.onCurrentTime().toString(),
                code_ad: ""
              };
            }
          } else {
            Code = rawDRecharge.dealer_code;
            if (CreateDetailRecharge[Code] == undefined) {
              KeyCreateRechargeLength.push(Code);
              CreateDetailRecharge[Code] = {
                periode: PERIODE,
                dealer_code: Code,
                location: "",
                name: "",
                type_trx: "RECHARGE",
                paid_in_amount: 0,
                withdraw_amount: 0,
                reason_type_id: 0,
                reason_type_name: "",
                trx_a: 0,
                tot_mdr_a: 0,
                trx_b: 0,
                tot_mdr_b: 0,
                status: 0,
                create_at: ControllerConfig.onCurrentTime().toString(),
                code_ad: ""
              };
            }
          }

          CreateDetailRecharge[Code] = {
            periode: PERIODE,
            dealer_code: Code,
            location: rawDRecharge.location,
            name: rawDRecharge.name,
            type_trx: "RECHARGE",
            paid_in_amount: Number(rawDRecharge.paid_in_amount),
            withdraw_amount: Number(rawDRecharge.withdraw_amount),
            reason_type_id: parseInt(rawDRecharge.reason_type_id),
            reason_type_name: rawDRecharge.reason_type_name,
            trx_a: Number(CreateDetailRecharge[Code].trx_a) + Number(rawDRecharge.trx_a),
            tot_mdr_a: Number(CreateDetailRecharge[Code].tot_mdr_a) + Number(rawDRecharge.tot_mdr_a),
            trx_b: Number(CreateDetailRecharge[Code].trx_b) + Number(rawDRecharge.trx_b),
            tot_mdr_b: Number(CreateDetailRecharge[Code].tot_mdr_b) + Number(rawDRecharge.tot_mdr_b),
            status: 0,
            create_at: ControllerConfig.onCurrentTime().toString(),
            code_ad: rawDRecharge.ad_code
          }

          NoProcess++;

        }


        var CreateDetailPPOB: any; CreateDetailPPOB = {};
        var KeyCreatePPOBLength = [];
        var NoProcessPPOB = 1;
        for (var iDPPOB in DataRawPPOB) {
          var rawDPPOB = DataRawPPOB[iDPPOB];
          if (DataRawPPOBSelected.indexOf(rawDPPOB.unique_key) == -1) {
            continue;
          }

          if (rawDRecharge.ad_code == "-") {
            continue;
          }

          var Code = "-";
          if (rawDPPOB.dealer_code == "-") {
            Code = rawDPPOB.ad_code;
            if (CreateDetailPPOB[Code] == undefined) {
              KeyCreatePPOBLength.push(Code);
              CreateDetailPPOB[Code] = {
                periode: PERIODE,
                dealer_code: Code,
                location: "",
                name: "",
                type_trx: "PPOB",
                paid_in_amount: 150,
                withdraw_amount: 0,
                reason_type_id: 0,
                reason_type_name: "",
                trx_a: 0,
                tot_mdr_a: 0,
                trx_b: 0,
                tot_mdr_b: 0,
                status: 0,
                create_at: ControllerConfig.onCurrentTime().toString(),
                code_ad: rawDPPOB.ad_code
              };
            }
          } else {
            Code = rawDPPOB.dealer_code;
            if (CreateDetailPPOB[Code] == undefined) {
              KeyCreatePPOBLength.push(Code);
              CreateDetailPPOB[Code] = {
                periode: PERIODE,
                dealer_code: Code,
                location: "",
                name: "",
                type_trx: "PPOB",
                paid_in_amount: 150,
                withdraw_amount: 0,
                reason_type_id: 0,
                reason_type_name: "",
                trx_a: 0,
                tot_mdr_a: 0,
                trx_b: 0,
                tot_mdr_b: 0,
                status: 0,
                create_at: ControllerConfig.onCurrentTime().toString(),
                code_ad: rawDPPOB.ad_code
              };
            }
          }


          CreateDetailPPOB[Code] = {
            periode: PERIODE,
            dealer_code: Code,
            location: rawDPPOB.location,
            name: rawDPPOB.name,
            type_trx: "PPOB",
            paid_in_amount: 150,
            withdraw_amount: 0,
            reason_type_id: parseInt(rawDPPOB.reason_type_id),
            reason_type_name: rawDPPOB.reason_type_name,
            trx_a: rawDPPOB.trx_a,
            tot_mdr_a: rawDPPOB.tot_mdr_a,
            trx_b: rawDPPOB.trx_b,
            tot_mdr_b: rawDPPOB.tot_mdr_b,
            status: 0,
            create_at: ControllerConfig.onCurrentTime().toString(),
            code_ad: rawDPPOB.ad_code
          };

          NoProcessPPOB++;
        }

        //DATA INSERT RECHARGE
        // CreateDetailRecharge[Code]
        //CreateDetailPPOB[];
        var DATA_INSERT_RECHARGE: any; DATA_INSERT_RECHARGE = [];
        var NO_RECHARGE = 0;
        for (var indexRecharge in CreateDetailRecharge) {
          var rawInsertRecharge = CreateDetailRecharge[indexRecharge];
          DATA_INSERT_RECHARGE.push(rawInsertRecharge);
          NO_RECHARGE = NO_RECHARGE + 1;
          if ((NO_RECHARGE % 500) == 0) {
            try {
              await this.dataSumarryRepository.createAll(DATA_INSERT_RECHARGE);
              DATA_INSERT_RECHARGE = [];
            } catch (error) {
              returnMs.success = false;
              returnMs.message = JSON.stringify(error);
              break;
            }
          } else {
            if (NO_RECHARGE == KeyCreateRechargeLength.length) {
              try {
                await this.dataSumarryRepository.createAll(DATA_INSERT_RECHARGE);
                DATA_INSERT_RECHARGE = [];
                NO_RECHARGE = 0;
              } catch (error) {
                returnMs.success = false;
                returnMs.message = JSON.stringify(error);
                break;
              }
            }
          }
        }

        var DATA_INSERT_PPOB: any; DATA_INSERT_PPOB = [];
        var NO_PPOB = 0;
        for (var indexPPOB in CreateDetailPPOB) {
          var rawInsertPPOB = CreateDetailPPOB[indexPPOB];
          DATA_INSERT_PPOB.push(rawInsertPPOB);
          NO_PPOB = NO_PPOB + 1;
          if ((NO_PPOB % 500) == 0) {
            try {
              await this.dataSumarryRepository.createAll(DATA_INSERT_PPOB);
              DATA_INSERT_PPOB = [];
            } catch (error) {
              console.log(error);
              returnMs.success = false;
              returnMs.message = JSON.stringify(error);
              break;
            }
          } else {
            if (NO_PPOB == KeyCreatePPOBLength.length) {
              try {
                await this.dataSumarryRepository.createAll(DATA_INSERT_PPOB);
                DATA_INSERT_PPOB = [];
                NO_PPOB = 0;
              } catch (error) {
                returnMs.success = false;
                returnMs.message = JSON.stringify(error);
              }
            }
          }
        }

        if (returnMs.success != false) {
          returnMs.success = true;
          returnMs.message = "Success !";
        }



      } else {
        returnMs.success = false;
        returnMs.message = "Error, no data rejected or new data migration !";
      }
    } else {
      returnMs.success = false;
      returnMs.message = "Error, periode not declare !";
    }
    return returnMs;
  }

  @get('/big-query/inquery/confirm', {
    responses: {
      200: {
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: {
                type: 'string',
              },
            },
          },
        },
        description: 'Inquery Recharge Raw',
      },
    },
  })
  async rawInqueryConfirmBQ() {
    var returnMs: any; returnMs = {success: true, message: "", data: {TOTAL_TRANS_AD_A: 0, TOTAL_TRANS_AD_B: 0, TOTAL_AD: 0}};
    returnMs.data.TOTAL_TRANS_AD_A = TOTAL_TRANS_AD_A;
    returnMs.data.TOTAL_TRANS_AD_B = TOTAL_TRANS_AD_B;
    returnMs.data.TOTAL_AD = TOTAL_AD.length;
    return returnMs;
  }

  @get('/big-query/inquery/calculation', {
    responses: {
      200: {
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: {
                type: 'string',
              },
            },
          },
        },
        description: 'Inquery Recharge Raw',
      },
    },
  })
  async rawInqueryCalculationBQ() {
    var returnMs: any; returnMs = {success: true, message: "", data: []};

    var Datas: any; Datas = {};

    TOTAL_TRANS_AD_A = 0;
    TOTAL_TRANS_AD_B = 0;
    TOTAL_AD = [];
    DATA_FINAL = {};

    for (var iR in DataSummaryRecharge) {
      var rawSummaryRecharge = DataSummaryRecharge[iR];
      if (TOTAL_AD.indexOf(rawSummaryRecharge.ad_code) == -1) {
        TOTAL_AD.push(rawSummaryRecharge.ad_code);
      }

      if (Datas[rawSummaryRecharge.ad_code] == undefined) {
        Datas[rawSummaryRecharge.ad_code] = {
          trx_a: 0,
          tot_mdr_a: 0,
          trx_b: 0,
          tot_mdr_b: 0,
          dpp_fee: 0,
          ppn_fee: 0,
          pph_fee: 0,
          total_fee: 0,
          dpp_paid: 0,
          ppn_paid: 0,
          pph_paid: 0,
          total_paid: 0,
          PPOB: {},
          RECHARGE: {}
        };
      }

      TOTAL_TRANS_AD_A += Number(rawSummaryRecharge.total_fee);
      TOTAL_TRANS_AD_B += Number(rawSummaryRecharge.total_paid);

      Datas[rawSummaryRecharge.ad_code].periode = rawSummaryRecharge.periode;
      Datas[rawSummaryRecharge.ad_code].dealer_code = rawSummaryRecharge.dealer_code;
      Datas[rawSummaryRecharge.ad_code].location = rawSummaryRecharge.location;
      Datas[rawSummaryRecharge.ad_code].name = rawSummaryRecharge.name;
      Datas[rawSummaryRecharge.ad_code].ad_code = rawSummaryRecharge.ad_code;
      Datas[rawSummaryRecharge.ad_code].ad_info = rawSummaryRecharge.ad_info;
      Datas[rawSummaryRecharge.ad_code].trx_a += Number(rawSummaryRecharge.trx_a);
      Datas[rawSummaryRecharge.ad_code].tot_mdr_a += Number(rawSummaryRecharge.tot_mdr_a);
      Datas[rawSummaryRecharge.ad_code].trx_b += Number(rawSummaryRecharge.trx_b);
      Datas[rawSummaryRecharge.ad_code].tot_mdr_b += Number(rawSummaryRecharge.tot_mdr_b);
      Datas[rawSummaryRecharge.ad_code].dpp_fee += Number(rawSummaryRecharge.dpp_fee);
      Datas[rawSummaryRecharge.ad_code].ppn_fee += Number(rawSummaryRecharge.ppn_fee);
      Datas[rawSummaryRecharge.ad_code].pph_fee += Number(rawSummaryRecharge.pph_fee);
      Datas[rawSummaryRecharge.ad_code].total_fee += Number(rawSummaryRecharge.total_fee);
      Datas[rawSummaryRecharge.ad_code].dpp_paid += Number(rawSummaryRecharge.dpp_paid);
      Datas[rawSummaryRecharge.ad_code].ppn_paid += Number(rawSummaryRecharge.ppn_paid);
      Datas[rawSummaryRecharge.ad_code].pph_paid += Number(rawSummaryRecharge.pph_paid);
      Datas[rawSummaryRecharge.ad_code].total_paid += Number(rawSummaryRecharge.total_paid);


      Datas[rawSummaryRecharge.ad_code][rawSummaryRecharge.type] = {
        sales_fee: rawSummaryRecharge.sales_fee,
        paid_in_amount: rawSummaryRecharge.paid_in_amount,
        withdraw_amount: rawSummaryRecharge.withdraw_amount,
        trx_a: rawSummaryRecharge.trx_a,
        tot_mdr_a: rawSummaryRecharge.tot_mdr_a,
        trx_b: rawSummaryRecharge.trx_b,
        tot_mdr_b: rawSummaryRecharge.tot_mdr_b,
        dpp_fee: rawSummaryRecharge.dpp_fee,
        ppn_fee: rawSummaryRecharge.ppn_fee,
        pph_fee: rawSummaryRecharge.pph_fee,
        total_fee: rawSummaryRecharge.total_fee,
        dpp_paid: rawSummaryRecharge.dpp_paid,
        ppn_paid: rawSummaryRecharge.ppn_paid,
        pph_paid: rawSummaryRecharge.pph_paid,
        total_paid: rawSummaryRecharge.total_paid
      };

    }

    for (var iP in DataSummaryPPOB) {
      var rawSummaryPPOB = DataSummaryPPOB[iP];
      if (TOTAL_AD.indexOf(rawSummaryPPOB.ad_code) == -1) {
        TOTAL_AD.push(rawSummaryPPOB.ad_code);
      }
      if (Datas[rawSummaryPPOB.ad_code] == undefined) {
        Datas[rawSummaryPPOB.ad_code] = {
          trx_a: 0,
          tot_mdr_a: 0,
          trx_b: 0,
          tot_mdr_b: 0,
          dpp_fee: 0,
          ppn_fee: 0,
          pph_fee: 0,
          total_fee: 0,
          dpp_paid: 0,
          ppn_paid: 0,
          pph_paid: 0,
          total_paid: 0,
          PPOB: {},
          RECHARGE: {}
        };
      }

      TOTAL_TRANS_AD_A += Number(rawSummaryPPOB.total_fee);

      Datas[rawSummaryPPOB.ad_code].periode = rawSummaryPPOB.periode;
      Datas[rawSummaryPPOB.ad_code].dealer_code = rawSummaryPPOB.dealer_code;
      Datas[rawSummaryPPOB.ad_code].location = rawSummaryPPOB.location;
      Datas[rawSummaryPPOB.ad_code].name = rawSummaryPPOB.name;
      Datas[rawSummaryPPOB.ad_code].ad_code = rawSummaryPPOB.ad_code;
      Datas[rawSummaryPPOB.ad_code].ad_info = rawSummaryPPOB.ad_info;
      Datas[rawSummaryPPOB.ad_code].trx_a += Number(rawSummaryPPOB.trx_a);
      Datas[rawSummaryPPOB.ad_code].tot_mdr_a += Number(rawSummaryPPOB.tot_mdr_a);
      Datas[rawSummaryPPOB.ad_code].trx_b += Number(rawSummaryPPOB.trx_b);
      Datas[rawSummaryPPOB.ad_code].tot_mdr_b += Number(rawSummaryPPOB.tot_mdr_b);
      Datas[rawSummaryPPOB.ad_code].dpp_fee += Number(rawSummaryPPOB.dpp_fee);
      Datas[rawSummaryPPOB.ad_code].ppn_fee += Number(rawSummaryPPOB.ppn_fee);
      Datas[rawSummaryPPOB.ad_code].pph_fee += Number(rawSummaryPPOB.pph_fee);
      Datas[rawSummaryPPOB.ad_code].total_fee += Number(rawSummaryPPOB.total_fee);
      Datas[rawSummaryPPOB.ad_code].dpp_paid += Number(rawSummaryPPOB.dpp_paid);
      Datas[rawSummaryPPOB.ad_code].ppn_paid += Number(rawSummaryPPOB.ppn_paid);
      Datas[rawSummaryPPOB.ad_code].pph_paid += Number(rawSummaryPPOB.pph_paid);
      Datas[rawSummaryPPOB.ad_code].total_paid += Number(rawSummaryPPOB.total_paid);

      Datas[rawSummaryPPOB.ad_code][rawSummaryPPOB.type] = {
        sales_fee: rawSummaryPPOB.sales_fee,
        paid_in_amount: rawSummaryPPOB.paid_in_amount,
        withdraw_amount: rawSummaryPPOB.withdraw_amount,
        trx_a: rawSummaryPPOB.trx_a,
        tot_mdr_a: rawSummaryPPOB.tot_mdr_a,
        trx_b: rawSummaryPPOB.trx_b,
        tot_mdr_b: rawSummaryPPOB.tot_mdr_b,
        dpp_fee: rawSummaryPPOB.dpp_fee,
        ppn_fee: rawSummaryPPOB.ppn_fee,
        pph_fee: rawSummaryPPOB.pph_fee,
        total_fee: rawSummaryPPOB.total_fee,
        dpp_paid: rawSummaryPPOB.dpp_paid,
        ppn_paid: rawSummaryPPOB.ppn_paid,
        pph_paid: rawSummaryPPOB.pph_paid,
        total_paid: rawSummaryPPOB.total_paid
      };
    }

    DATA_FINAL = Datas;
    returnMs.data = Datas;
    return returnMs;
  }



  //TRANSACTION RECHARGE
  @get('/big-query/inquery-recharge/raw', {
    responses: {
      200: {
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: {
                type: 'string',
              },
            },
          },
        },
        description: 'Inquery Recharge Raw',
      },
    },
  })
  async rawInqueryRechargeRawBQ() {
    var returnMs: any; returnMs = {success: true, message: "", data: [], data_selected: []};
    returnMs.data = DataRawRecharge;
    returnMs.data_selected = DataRawRechargeSelected;
    return returnMs;
  }


  @post('/big-query/inquery-recharge/raw/check-uncheck', {
    responses: {
      '200': {
        description: 'Invoice model instance',
        content: {
          'application/json': {
            schema: UniqueKeyRequestBody
          }
        },
      },
    },
  })
  async rawInqueryRechargeUncheckBQ(
    @requestBody(UniqueKeyRequestBody) uniqueReq: {unique_key: string},
  ): Promise<Object> {
    var returnMs: any; returnMs = {success: true, message: ""};

    if (DataRawRechargeIndex.indexOf(uniqueReq.unique_key) != -1) {
      if (DataRawRechargeSelected.indexOf(uniqueReq.unique_key) != -1) {
        DataRawRechargeSelected.splice(DataRawRechargeSelected.indexOf(uniqueReq.unique_key), 1);
        returnMs.success = true;
        returnMs.message = "Success, uncheck " + uniqueReq.unique_key;
      } else {
        DataRawRechargeSelected.push(uniqueReq.unique_key);
        returnMs.success = true;
        returnMs.message = "Success, check " + uniqueReq.unique_key;
      }
    } else {
      returnMs.success = false;
      returnMs.message = "Error, not found unique key !";
    }
    return returnMs;
  }

  @get('/big-query/inquery-recharge/summary', {
    responses: {
      200: {
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: {
                type: 'string',
              },
            },
          },
        },
        description: 'Inquery Recharge Raw',
      },
    },
  })
  async rawInqueryRechargeSummaryBQ() {
    var returnMs: any; returnMs = {success: true, message: "", datas: [], datas_selected: [], data_config: {}};

    const configData = await this.dataconfigRepository.find({where: {config_group_id: 1}});
    var configNum: any;
    configNum = {};
    for (var i in configData) {
      var infoConfig = configData[i];
      if (infoConfig.config_code == "0001-01") {
        configNum["persen_fee"] = infoConfig.config_value;
      }
      if (infoConfig.config_code == "0001-02") {
        configNum["persen_paid"] = infoConfig.config_value;
      }
      if (infoConfig.config_code == "0001-03") {
        configNum["num_dpp_fee"] = infoConfig.config_value;
      }
      if (infoConfig.config_code == "0001-04") {
        configNum["num_ppn_fee"] = infoConfig.config_value;
      }
      if (infoConfig.config_code == "0001-05") {
        configNum["num_pph_fee"] = infoConfig.config_value;
      }

      if (infoConfig.config_code == "0001-11") {
        configNum["num_dpp_paid"] = infoConfig.config_value;
      }
      if (infoConfig.config_code == "0001-12") {
        configNum["num_ppn_paid"] = infoConfig.config_value;
      }
      if (infoConfig.config_code == "0001-13") {
        configNum["num_pph_paid"] = infoConfig.config_value;
      }

      if (infoConfig.config_code == "0001-06") {
        configNum["persen_fee_ppob"] = infoConfig.config_value;
      }
      if (infoConfig.config_code == "0001-07") {
        configNum["persen_paid_ppob"] = infoConfig.config_value;
      }
      if (infoConfig.config_code == "0001-08") {
        configNum["num_dpp_ppob"] = infoConfig.config_value;
      }
      if (infoConfig.config_code == "0001-09") {
        configNum["num_ppn_ppob"] = infoConfig.config_value;
      }
      if (infoConfig.config_code == "0001-10") {
        configNum["num_pph_ppob"] = infoConfig.config_value;
      }
    }

    var Datas: any; Datas = {};

    for (var i in DataRawRecharge) {
      var rawRecharge = DataRawRecharge[i];
      if (DataRawRechargeSelected.indexOf(rawRecharge.unique_key) == -1) {
        continue;
      }
      if (returnMs.data_config[rawRecharge.periode] == undefined) {
        returnMs.data_config[rawRecharge.periode] = {
          periode: rawRecharge.periode,
          fee_ppob_share: parseFloat(configNum.persen_fee_ppob),
          paid_ppob_share: parseFloat(configNum.persen_paid_ppob),
          dpp_ppob_share: parseFloat(configNum.num_dpp_ppob),
          ppn_ppob_share: parseFloat(configNum.num_ppn_ppob),
          pph_ppob_share: parseFloat(configNum.num_pph_ppob),
          fee_recharce_share: parseFloat(configNum.persen_fee),
          paid_recharge_share: parseFloat(configNum.persen_paid),

          dpp_recharge_share_fee: parseFloat(configNum.num_dpp_fee),
          ppn_recharge_share_fee: parseFloat(configNum.num_ppn_fee),
          pph_recharge_share_fee: parseFloat(configNum.num_pph_fee),

          dpp_recharge_share_paid: parseFloat(configNum.num_dpp_paid),
          ppn_recharge_share_paid: parseFloat(configNum.num_ppn_paid),
          pph_recharge_share_paid: parseFloat(configNum.num_pph_paid)

        }
      }

      var keyRaw = rawRecharge.ad_code + "_" + rawRecharge.type + "_" + rawRecharge.sales_fee;
      if (Datas[keyRaw] == undefined) {
        Datas[keyRaw] = {
          periode: rawRecharge.periode,
          dealer_code: rawRecharge.dealer_code,
          location: rawRecharge.location,
          name: rawRecharge.name,
          ad_code: rawRecharge.ad_code,
          ad_info: rawRecharge.ad_info,
          type: rawRecharge.type,
          sales_fee: rawRecharge.sales_fee,
          paid_in_amount: rawRecharge.paid_in_amount,
          withdraw_amount: rawRecharge.withdraw_amount,
          trx_a: 0,//rawRecharge.trx_a,
          tot_mdr_a: 0, //rawRecharge.tot_mdr_a,
          trx_b: 0,//rawRecharge.trx_b,
          tot_mdr_b: 0,//rawRecharge.tot_mdr_b,
          dpp_fee: 0,
          ppn_fee: 0,
          pph_fee: 0,
          total_fee: 0,
          dpp_paid: 0,
          ppn_paid: 0,
          pph_paid: 0,
          total_paid: 0,
          reason_type_id: rawRecharge.reason_type_id,
          reason_type_name: rawRecharge.reason_type_name,
          status: rawRecharge.status,
          message: rawRecharge.message
        };
      }

      Datas[keyRaw] = {
        periode: rawRecharge.periode,
        dealer_code: rawRecharge.dealer_code,
        location: rawRecharge.location,
        name: rawRecharge.name,
        ad_code: rawRecharge.ad_code,
        ad_info: rawRecharge.ad_info,
        type: rawRecharge.type,
        sales_fee: rawRecharge.sales_fee,
        paid_in_amount: rawRecharge.paid_in_amount,
        withdraw_amount: rawRecharge.withdraw_amount,
        trx_a: Number(rawRecharge.trx_a) + Datas[keyRaw].trx_a,
        tot_mdr_a: Number(rawRecharge.tot_mdr_a) + Datas[keyRaw].tot_mdr_a,
        trx_b: Number(rawRecharge.trx_b) + Datas[keyRaw].trx_b,
        tot_mdr_b: Number(rawRecharge.tot_mdr_b) + Datas[keyRaw].tot_mdr_b,
        dpp_fee: 0,
        ppn_fee: 0,
        pph_fee: 0,
        total_fee: 0,
        dpp_paid: 0,
        ppn_paid: 0,
        pph_paid: 0,
        total_paid: 0,
        reason_type_id: rawRecharge.reason_type_id,
        reason_type_name: rawRecharge.reason_type_name,
        status: rawRecharge.status,
        message: rawRecharge.message
      };

    }



    returnMs.data_config = {
      periode: "",
      fee_ppob_share: parseFloat(configNum.persen_fee_ppob),
      paid_ppob_share: parseFloat(configNum.persen_paid_ppob),
      dpp_ppob_share: parseFloat(configNum.num_dpp_ppob),
      ppn_ppob_share: parseFloat(configNum.num_ppn_ppob),
      pph_ppob_share: parseFloat(configNum.num_pph_ppob),
      fee_recharce_share: parseFloat(configNum.persen_fee),
      paid_recharge_share: parseFloat(configNum.persen_paid),

      dpp_recharge_share_fee: parseFloat(configNum.num_dpp_fee),
      ppn_recharge_share_fee: parseFloat(configNum.num_ppn_fee),
      pph_recharge_share_fee: parseFloat(configNum.num_pph_fee),

      dpp_recharge_share_paid: parseFloat(configNum.num_dpp_paid),
      ppn_recharge_share_paid: parseFloat(configNum.num_ppn_paid),
      pph_recharge_share_paid: parseFloat(configNum.num_pph_paid),

    }
    for (var iDatas in Datas) {
      //PERSENTASE CONFIG
      var persenFee = configNum.persen_fee;
      var persenPaid = configNum.persen_paid;

      var dppSetFee = configNum.num_dpp_fee;
      var ppnSetFee = configNum.num_ppn_fee;
      var pphSetFee = configNum.num_pph_fee;

      var dppSetPaid = configNum.num_dpp_paid;
      var ppnSetPaid = configNum.num_ppn_paid;
      var pphSetPaid = configNum.num_pph_paid;

      //DATA TRANSACTION
      var rawDetail = Datas[iDatas];

      var trxFee = ((rawDetail.trx_a * rawDetail.sales_fee) * (persenFee));
      var feeDPP = (trxFee / dppSetFee);
      var feePPN = (feeDPP * ppnSetFee);
      var feeTotal = feeDPP + feePPN;
      var feePPH = (feeDPP * pphSetFee);

      var trxPaid = ((rawDetail.trx_b * rawDetail.sales_fee) * (persenPaid));
      var paidDPP = (trxPaid / dppSetPaid);
      var paidPPN = (paidDPP * ppnSetPaid);
      var paidTotal = paidDPP + paidPPN;
      var paidPPH = (paidDPP * pphSetPaid);

      Datas[iDatas].dpp_fee = Number(feeDPP.toFixed(2));
      Datas[iDatas].ppn_fee = Number(feePPN.toFixed(2));
      Datas[iDatas].pph_fee = Number(feePPH.toFixed(2));
      Datas[iDatas].total_fee = Number(feeTotal.toFixed(2));

      Datas[iDatas].dpp_paid = Number(paidDPP.toFixed(2));
      Datas[iDatas].ppn_paid = Number(paidPPN.toFixed(2));
      Datas[iDatas].pph_paid = Number(paidPPH.toFixed(2));
      Datas[iDatas].total_paid = Number(paidTotal.toFixed(2));

      returnMs.datas.push(Datas[iDatas]);
    }
    DataSummaryRecharge = returnMs.datas;
    returnMs.datas_selected = DataRawRechargeSelected;
    return returnMs;//DataRawRecharge;
  }

  @post('/big-query/inquery-recharge/syncronize', {
    responses: {
      '200': {
        description: 'Invoice model instance',
        content: {
          'application/json': {
            schema: PeriodeRequestBody
          }
        },
      },
    },
  })
  async rawInqueryRechargeSyncBQ(
    @requestBody(PeriodeRequestBody) periodeReq: {periode: string},
  ): Promise<Object> {
    var returnMs: any; returnMs = {success: true, message: "", datas: []};
    // PERIODE = periodeReq.periode;
    if (PERIODE != null) {
      var PeriodeTrx = PERIODE.split("-");
      var dateLast = new Date(Number(PeriodeTrx[0]), Number(PeriodeTrx[1]), 0);
      var invMounth = dateLast.getMonth() + 1;
      var dateMountLast = dateLast.getFullYear() + "-" + "00".substring(invMounth.toString().length) + invMounth + "-" + dateLast.getDate();
      var dateMountFirst = dateLast.getFullYear() + "-" + "00".substring(invMounth.toString().length) + invMounth + "-" + "01";

      const configData = await this.dataconfigRepository.find({where: {config_group_id: 1}});
      var configNum: any;
      configNum = {};
      for (var i in configData) {
        var infoConfig = configData[i];
        if (infoConfig.config_code == "0001-01") {
          configNum["persen_fee"] = infoConfig.config_value;
        }
        if (infoConfig.config_code == "0001-02") {
          configNum["persen_paid"] = infoConfig.config_value;
        }

        if (infoConfig.config_code == "0001-03") {
          configNum["num_dpp_fee"] = infoConfig.config_value;
        }
        if (infoConfig.config_code == "0001-04") {
          configNum["num_ppn_fee"] = infoConfig.config_value;
        }
        if (infoConfig.config_code == "0001-05") {
          configNum["num_pph_fee"] = infoConfig.config_value;
        }


        if (infoConfig.config_code == "0001-11") {
          configNum["num_dpp_paid"] = infoConfig.config_value;
        }
        if (infoConfig.config_code == "0001-12") {
          configNum["num_ppn_paid"] = infoConfig.config_value;
        }
        if (infoConfig.config_code == "0001-13") {
          configNum["num_pph_paid"] = infoConfig.config_value;
        }

        if (infoConfig.config_code == "0001-06") {
          configNum["persen_fee_ppob"] = infoConfig.config_value;
        }
        if (infoConfig.config_code == "0001-07") {
          configNum["persen_paid_ppob"] = infoConfig.config_value;
        }
        if (infoConfig.config_code == "0001-08") {
          configNum["num_dpp_ppob"] = infoConfig.config_value;
        }
        if (infoConfig.config_code == "0001-09") {
          configNum["num_ppn_ppob"] = infoConfig.config_value;
        }
        if (infoConfig.config_code == "0001-10") {
          configNum["num_pph_ppob"] = infoConfig.config_value;
        }
      }

      const bigquery = new BigQuery();
      const sqlQuery = "SELECT GENERATE_UUID() as UniqueKey,* FROM `project-self-service-query.digipos_finance.sum_digipos_revenue` WHERE  mmonth  BETWEEN  @first_date AND @last_date ";
      const options = {
        query: sqlQuery,
        params: {first_date: new BigQueryDate(dateMountFirst), last_date: new BigQueryDate(dateMountLast)},
      }

      const [rows] = await bigquery.query(options);
      DataRawRechargeIndex = [];
      DataRawRechargeSelected = [];
      DataRawRecharge = [];
      var TotalRaw = 0;
      var TotalPaid = 0;
      var TotalFee = 0;
      var AD: any; AD = [];
      var TotalTrxA = 0;
      var TotalTrxB = 0;

      for (var i in rows) {

        var rawData = rows[i];

        if (rawData.credit_party_mnemonic_b.toUpperCase().search("TEST") != -1) {
          continue;
        }

        DataRawRechargeSelected.push(rawData.UniqueKey);
        DataRawRechargeIndex.push(rawData.UniqueKey);

        var paid = Number(rawData.paid_in_amount);
        var withdraw = Number(rawData.withdraw_amount);
        var TRX_A = Number(rawData.trx_a);
        var TRX_B = Number(rawData.trx_b);
        var TOT_A = Number(rawData.tot_mdr_a);
        var TOT_B = Number(rawData.tot_mdr_b);
        var TotalA = (paid + withdraw) < 0 ? (((paid + withdraw) * TRX_A) * -1) * configNum.persen_fee : ((paid + withdraw) * TRX_A) * configNum.persen_fee;
        var TotalB = (paid + withdraw) < 0 ? (((paid + withdraw) * TRX_B) * -1) * configNum.persen_paid : ((paid + withdraw) * TRX_B) * configNum.persen_paid;

        TotalRaw++;
        TotalPaid += TOT_B;
        TotalFee += TOT_A;
        TotalTrxA += TRX_A;
        TotalTrxB += TRX_B;

        var status = 1;
        var message = "";

        if (Number(TOT_A).toFixed(2) != Number(TotalA).toFixed(2)) {
          status = 0;
          message = ",Calculate Total MDR FEE not valid !";
        }

        if (Number(TOT_B).toFixed(2) != Number(TotalB).toFixed(2)) {
          status = 0;
          message += ",Calculate Total MDR PAID not valid !";
        }

        var cluster_raw = rawData.credit_party_mnemonic_b.split(" - ");
        var adCluster = null;
        var adMaster = null;
        if (cluster_raw.length == 2) {
          adCluster = await this.adClusterRepository.findOne({where: {ad_cluster_id: cluster_raw[0].toString()}});
          if (adCluster == null) {
            status = 0;
            message += ",cluster code not found !";
          } else {
            adMaster = await this.adMasterRepository.findById(adCluster.ad_code);
            if (adMaster != null) {
              if (AD.indexOf(adMaster.ad_code) == -1) {
                AD.push(adMaster.ad_code);
              }
            }
          }
        } else {
          status = 0;
          message += ",cluster code not found !";
        }

        DataRawRecharge.push({
          unique_key: rawData.UniqueKey,
          periode: PERIODE,
          dealer_code: adCluster != null ? adCluster.ad_cluster_id : "-",
          location: adCluster != null ? adCluster.ad_cluster_name : "-",
          name: adCluster != null ? (adMaster?.global_name == null ? adMaster?.ad_name : adMaster?.global_name) : "-",
          ad_code: adCluster != null ? adMaster?.ad_code : "-",
          ad_info: {
            alamat: adMaster?.address,
            alamat_npwp: adMaster?.npwp_address,
            attention: adMaster?.attention,
            attention_position: adMaster?.attention_position,
            no_rek: adMaster?.no_rek,
            name_rekening: adMaster?.name_rekening,
            name_bank: adMaster?.name_bank,
            legal_name: adMaster?.legal_name,
            global_name: adMaster?.global_name,
            name: adMaster?.ad_name
          },
          type: "RECHARGE",
          sales_fee: (paid + (withdraw < 0 ? withdraw * -1 : withdraw)),
          paid_in_amount: paid,
          withdraw_amount: withdraw,
          trx_a: TRX_A,
          tot_mdr_a: Number(rawData.tot_mdr_a),
          trx_b: TRX_B,
          tot_mdr_b: Number(rawData.tot_mdr_b),
          reason_type_id: paid == 0 ? "10002409" : "10003465",
          reason_type_name: paid == 0 ? "B2B Transfer Reversal" : "Digipos B2B Transfer Fee",
          status: status,
          message: message.substring(1)
        });

      }

      returnMs.datas = {
        total_raw: TotalRaw,
        total_paid: TotalPaid,
        total_fee: TotalFee,
        total_trx_a: TotalTrxA,
        total_trx_b: TotalTrxB,
        count_ad: AD.length
      };//DataRawRecharge;
    } else {
      returnMs.success = false;
      returnMs.message = "Error, Periode not declare !";
    }
    return returnMs;
  }




  @post('/big-query/inquery-ppob/raw/check-uncheck', {
    responses: {
      '200': {
        description: 'Invoice model instance',
        content: {
          'application/json': {
            schema: UniqueKeyRequestBody
          }
        },
      },
    },
  })
  async rawInqueryPPOBUncheckBQ(
    @requestBody(UniqueKeyRequestBody) uniqueReq: {unique_key: string},
  ): Promise<Object> {
    var returnMs: any; returnMs = {success: true, message: ""};

    if (DataRawPPOBIndex.indexOf(uniqueReq.unique_key) != -1) {
      if (DataRawPPOBSelected.indexOf(uniqueReq.unique_key) != -1) {
        DataRawPPOBSelected.splice(DataRawPPOBSelected.indexOf(uniqueReq.unique_key), 1);
        returnMs.success = true;
        returnMs.message = "Success, uncheck " + uniqueReq.unique_key;
      } else {
        DataRawPPOBSelected.push(uniqueReq.unique_key);
        returnMs.success = true;
        returnMs.message = "Success, check " + uniqueReq.unique_key;
      }
    } else {
      returnMs.success = false;
      returnMs.message = "Error, not found unique key !";
    }
    return returnMs;
  }


  @get('/big-query/inquery-ppob/summary', {
    responses: {
      200: {
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: {
                type: 'string',
              },
            },
          },
        },
        description: 'Inquery Recharge Raw',
      },
    },
  })
  async rawInqueryPPOBSummaryBQ() {
    var returnMs: any; returnMs = {success: true, message: "", datas: [], datas_selected: [], data_config: {}};

    const configData = await this.dataconfigRepository.find({where: {config_group_id: 1}});
    var configNum: any;
    configNum = {};
    for (var i in configData) {
      var infoConfig = configData[i];
      if (infoConfig.config_code == "0001-01") {
        configNum["persen_fee"] = infoConfig.config_value;
      }
      if (infoConfig.config_code == "0001-02") {
        configNum["persen_paid"] = infoConfig.config_value;
      }

      if (infoConfig.config_code == "0001-03") {
        configNum["num_dpp_fee"] = infoConfig.config_value;
      }
      if (infoConfig.config_code == "0001-04") {
        configNum["num_ppn_fee"] = infoConfig.config_value;
      }
      if (infoConfig.config_code == "0001-05") {
        configNum["num_pph_fee"] = infoConfig.config_value;
      }

      if (infoConfig.config_code == "0001-11") {
        configNum["num_dpp_paid"] = infoConfig.config_value;
      }
      if (infoConfig.config_code == "0001-12") {
        configNum["num_ppn_paid"] = infoConfig.config_value;
      }
      if (infoConfig.config_code == "0001-13") {
        configNum["num_pph_paid"] = infoConfig.config_value;
      }

      if (infoConfig.config_code == "0001-06") {
        configNum["persen_fee_ppob"] = infoConfig.config_value;
      }
      if (infoConfig.config_code == "0001-07") {
        configNum["persen_paid_ppob"] = infoConfig.config_value;
      }
      if (infoConfig.config_code == "0001-08") {
        configNum["num_dpp_ppob"] = infoConfig.config_value;
      }
      if (infoConfig.config_code == "0001-09") {
        configNum["num_ppn_ppob"] = infoConfig.config_value;
      }
      if (infoConfig.config_code == "0001-10") {
        configNum["num_pph_ppob"] = infoConfig.config_value;
      }
    }

    var Datas: any; Datas = {};

    for (var i in DataRawPPOB) {
      var rawPPOB = DataRawPPOB[i];
      if (DataRawPPOBSelected.indexOf(rawPPOB.unique_key) == -1) {
        continue;
      }
      if (returnMs.data_config[rawPPOB.periode] == undefined) {
        returnMs.data_config[rawPPOB.periode] = {
          periode: rawPPOB.periode,
          fee_ppob_share: parseFloat(configNum.persen_fee_ppob),
          paid_ppob_share: parseFloat(configNum.persen_paid_ppob),
          dpp_ppob_share: parseFloat(configNum.num_dpp_ppob),
          ppn_ppob_share: parseFloat(configNum.num_ppn_ppob),
          pph_ppob_share: parseFloat(configNum.num_pph_ppob),
          fee_recharce_share: parseFloat(configNum.persen_fee),
          paid_recharge_share: parseFloat(configNum.persen_paid),

          dpp_recharge_share_fee: parseFloat(configNum.num_dpp_fee),
          ppn_recharge_share_fee: parseFloat(configNum.num_ppn_fee),
          pph_recharge_share_fee: parseFloat(configNum.num_pph_fee),


          dpp_recharge_share_paid: parseFloat(configNum.num_dpp_paid),
          ppn_recharge_share_paid: parseFloat(configNum.num_ppn_paid),
          pph_recharge_share_paid: parseFloat(configNum.num_pph_paid)
        }
      }

      var keyRaw = rawPPOB.ad_code + "_" + rawPPOB.type + "_" + rawPPOB.sales_fee;
      if (Datas[keyRaw] == undefined) {
        Datas[keyRaw] = {
          periode: rawPPOB.periode,
          dealer_code: rawPPOB.dealer_code,
          location: rawPPOB.location,
          name: rawPPOB.name,
          ad_code: rawPPOB.ad_code,
          ad_info: rawPPOB.ad_info,
          type: rawPPOB.type,
          sales_fee: rawPPOB.sales_fee,
          paid_in_amount: rawPPOB.paid_in_amount,
          withdraw_amount: rawPPOB.withdraw_amount,
          trx_a: 0,//rawRecharge.trx_a,
          tot_mdr_a: 0, //rawRecharge.tot_mdr_a,
          trx_b: 0,//rawRecharge.trx_b,
          tot_mdr_b: 0,//rawRecharge.tot_mdr_b,
          dpp_fee: 0,
          ppn_fee: 0,
          pph_fee: 0,
          total_fee: 0,
          dpp_paid: 0,
          ppn_paid: 0,
          pph_paid: 0,
          total_paid: 0,
          reason_type_id: rawPPOB.reason_type_id,
          reason_type_name: rawPPOB.reason_type_name,
          status: rawPPOB.status,
          message: rawPPOB.message
        };
      }

      Datas[keyRaw] = {
        periode: rawPPOB.periode,
        dealer_code: rawPPOB.dealer_code,
        location: rawPPOB.location,
        name: rawPPOB.name,
        ad_code: rawPPOB.ad_code,
        ad_info: rawPPOB.ad_info,
        type: rawPPOB.type,
        sales_fee: rawPPOB.sales_fee,
        paid_in_amount: rawPPOB.paid_in_amount,
        withdraw_amount: rawPPOB.withdraw_amount,
        trx_a: Number(rawPPOB.trx_a) + Datas[keyRaw].trx_a,
        tot_mdr_a: Number(rawPPOB.tot_mdr_a) + Datas[keyRaw].tot_mdr_a,
        trx_b: Number(rawPPOB.trx_b) + Datas[keyRaw].trx_b,
        tot_mdr_b: Number(rawPPOB.tot_mdr_b) + Datas[keyRaw].tot_mdr_b,
        dpp_fee: 0,
        ppn_fee: 0,
        pph_fee: 0,
        total_fee: 0,
        dpp_paid: 0,
        ppn_paid: 0,
        pph_paid: 0,
        total_paid: 0,
        reason_type_id: rawPPOB.reason_type_id,
        reason_type_name: rawPPOB.reason_type_name,
        status: rawPPOB.status,
        message: rawPPOB.message
      };

    }



    returnMs.data_config = {
      periode: "",
      fee_ppob_share: parseFloat(configNum.persen_fee_ppob),
      paid_ppob_share: parseFloat(configNum.persen_paid_ppob),
      dpp_ppob_share: parseFloat(configNum.num_dpp_ppob),
      ppn_ppob_share: parseFloat(configNum.num_ppn_ppob),
      pph_ppob_share: parseFloat(configNum.num_pph_ppob),
      fee_recharce_share: parseFloat(configNum.persen_fee),
      paid_recharge_share: parseFloat(configNum.persen_paid),

      dpp_recharge_share_fee: parseFloat(configNum.num_dpp_fee),
      ppn_recharge_share_fee: parseFloat(configNum.num_ppn_fee),
      pph_recharge_share_fee: parseFloat(configNum.num_pph_fee),

      dpp_recharge_share_paid: parseFloat(configNum.num_dpp_paid),
      ppn_recharge_share_paid: parseFloat(configNum.num_ppn_paid),
      pph_recharge_share_paid: parseFloat(configNum.num_pph_paid)

    }

    for (var iDatas in Datas) {
      //PERSENTASE CONFIG
      var persenFee = configNum.persen_fee_ppob;
      var persenPaid = configNum.persen_paid_ppob;

      var dppSet = configNum.num_dpp_ppob;
      var ppnSet = configNum.num_ppn_ppob;
      var pphSet = configNum.num_pph_ppob;

      //DATA TRANSACTION
      var rawDetail = Datas[iDatas];

      var trxFee = ((rawDetail.trx_a * rawDetail.sales_fee) * (persenFee));
      var feeDPP = (trxFee / dppSet);
      var feePPN = (feeDPP * ppnSet);
      var feeTotal = feeDPP + feePPN;
      var feePPH = (feeDPP * pphSet);

      var trxPaid = ((rawDetail.trx_b * rawDetail.sales_fee) * (persenPaid));
      var paidDPP = (trxPaid / dppSet);
      var paidPPN = (paidDPP * ppnSet);
      var paidTotal = paidDPP + paidPPN;
      var paidPPH = (paidDPP * pphSet);

      Datas[iDatas].dpp_fee = Number(feeDPP.toFixed(2));
      Datas[iDatas].ppn_fee = Number(feePPN.toFixed(2));
      Datas[iDatas].pph_fee = Number(feePPH.toFixed(2));
      Datas[iDatas].total_fee = Number(feeTotal.toFixed(2));


      returnMs.datas.push(Datas[iDatas]);
    }
    DataSummaryPPOB = returnMs.datas;
    returnMs.datas_selected = DataRawPPOBSelected;
    return returnMs;//DataRawRecharge;
  }



  @get('/big-query/inquery-ppob/raw', {
    responses: {
      200: {
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: {
                type: 'string',
              },
            },
          },
        },
        description: 'Inquery Recharge Raw',
      },
    },
  })
  async rawInqueryPPOBRawBQ() {
    var returnMs: any; returnMs = {success: true, message: "", data: [], data_selected: []};
    returnMs.data = DataRawPPOB;
    returnMs.data_selected = DataRawPPOBSelected;
    return returnMs;
  }


  @post('/big-query/inquery-ppob/syncronize', {
    responses: {
      '200': {
        description: 'Invoice model instance',
        content: {
          'application/json': {
            schema: PeriodeRequestBody
          }
        },
      },
    },
  })
  async rawInqueryPPOBSyncBQ(
    @requestBody(PeriodeRequestBody) periodeReq: {periode: string},
  ): Promise<Object> {
    var returnMs: any; returnMs = {success: true, message: "", datas: []};

    var CLUSTER_DATA = await this.adClusterRepository.find();
    var CLUSTER: any; CLUSTER = {};
    for (var iCLUSTER in CLUSTER_DATA) {
      var RAW_CLUSTER = CLUSTER_DATA[iCLUSTER];
      CLUSTER[RAW_CLUSTER.ad_cluster_id] = RAW_CLUSTER;
    }

    // PERIODE = periodeReq.periode;

    if (PERIODE != null) {
      var PeriodeTrx = PERIODE.split("-");
      var dateLast = new Date(Number(PeriodeTrx[0]), Number(PeriodeTrx[1]), 0);
      var invMounth = dateLast.getMonth() + 1;
      var dateMountLast = dateLast.getFullYear() + "-" + "00".substring(invMounth.toString().length) + invMounth + "-" + dateLast.getDate();
      var dateMountFirst = dateLast.getFullYear() + "-" + "00".substring(invMounth.toString().length) + invMounth + "-" + "01";

      const configData = await this.dataconfigRepository.find({where: {config_group_id: 1}});
      var configNum: any;
      configNum = {};
      for (var i in configData) {
        var infoConfig = configData[i];
        if (infoConfig.config_code == "0001-01") {
          configNum["persen_fee"] = infoConfig.config_value;
        }
        if (infoConfig.config_code == "0001-02") {
          configNum["persen_paid"] = infoConfig.config_value;
        }

        if (infoConfig.config_code == "0001-03") {
          configNum["num_dpp_fee"] = infoConfig.config_value;
        }
        if (infoConfig.config_code == "0001-04") {
          configNum["num_ppn_fee"] = infoConfig.config_value;
        }
        if (infoConfig.config_code == "0001-05") {
          configNum["num_pph_fee"] = infoConfig.config_value;
        }

        if (infoConfig.config_code == "0001-11") {
          configNum["num_dpp_paid"] = infoConfig.config_value;
        }
        if (infoConfig.config_code == "0001-12") {
          configNum["num_ppn_paid"] = infoConfig.config_value;
        }
        if (infoConfig.config_code == "0001-13") {
          configNum["num_pph_paid"] = infoConfig.config_value;
        }



        if (infoConfig.config_code == "0001-06") {
          configNum["persen_fee_ppob"] = infoConfig.config_value;
        }
        if (infoConfig.config_code == "0001-07") {
          configNum["persen_paid_ppob"] = infoConfig.config_value;
        }
        if (infoConfig.config_code == "0001-08") {
          configNum["num_dpp_ppob"] = infoConfig.config_value;
        }
        if (infoConfig.config_code == "0001-09") {
          configNum["num_ppn_ppob"] = infoConfig.config_value;
        }
        if (infoConfig.config_code == "0001-10") {
          configNum["num_pph_ppob"] = infoConfig.config_value;
        }
      }

      var FEE_PPOB = 150;

      const bigquery = new BigQuery();
      const sqlQuery = "SELECT GENERATE_UUID() as UniqueKey,* FROM `project-self-service-query.digipos_finance.sum_ppob_revenue` WHERE  partition_transaction_end_date_local  BETWEEN  @first_date AND @last_date  ";
      const options = {
        query: sqlQuery,
        params: {first_date: new BigQueryDate(dateMountFirst), last_date: new BigQueryDate(dateMountLast)},
      }

      const [rows] = await bigquery.query(options);

      DataRawPPOBIndex = [];
      DataRawPPOBSelected = [];
      DataRawPPOB = [];
      var AD: any; AD = [];
      var TotalRaw = 0;
      var TotalPaid = 0;
      var TotalFee = 0;
      var AD: any; AD = [];
      var TotalTrxA = 0;
      var TotalTrxB = 0;

      var INFO_AD = await this.adMasterRepository.find();
      var INFO_AD_STORE: any; INFO_AD_STORE = {};
      for (var iAD in INFO_AD) {
        var rawAD = INFO_AD[iAD];
        var storeAlias = rawAD.alias_name == null ? [] : rawAD.alias_name.split("#");
        for (var iAlias in storeAlias) {
          var NameAD = storeAlias[iAlias];
          if (INFO_AD_STORE[NameAD.split(".").join("").toUpperCase()] == undefined) {
            INFO_AD_STORE[NameAD.split(".").join("").toUpperCase()] = rawAD;
          }
        }
      }

      var CLUSTER_SET: any; CLUSTER_SET = [];
      for (var iRESULT in rows) {
        var ROW = rows[iRESULT];
        var ROW_KEY = ROW.top_biz_org_name_a;
        var CLUSTER_EXPLODE = ROW_KEY.split(" - ").join("-").split("-");
        var CLUSTER_ID = CLUSTER_EXPLODE[0];
        if (CLUSTER[CLUSTER_ID] != undefined) {
          if (CLUSTER_SET.indexOf(CLUSTER_ID) == -1) {
            CLUSTER_SET.push(CLUSTER_ID);
          }
        }

      }


      for (var i in rows) {
        var rawData = rows[i];

        if (rawData.top_biz_org_name_a.toUpperCase().search("TEST") != -1 || rawData.top_biz_org_name_a.toUpperCase().search("00101") != -1) {
          continue;
        }


        TotalRaw++;
        TotalFee += Number(rawData.count_trx) * FEE_PPOB;
        TotalTrxA += Number(rawData.count_trx);

        DataRawPPOBSelected.push(rawData.UniqueKey);
        DataRawPPOBIndex.push(rawData.UniqueKey);

        var cluster_raw = rawData.top_biz_org_name_a.split("-");
        var adCluster = null;
        var adMaster = null;
        var status = 1;
        var message = "";


        var ROW_KEY = rawData.top_biz_org_name_a;
        var CLUSTER_EXPLODE = ROW_KEY.split(" - ").join("-").split("-");
        var CLUSTER_ID = CLUSTER_EXPLODE[0];
        if (CLUSTER_SKIP.indexOf(ROW_KEY) != -1) {
          continue;
        }

        var FLAG_AVALIABBLE = 1;
        var MESSAGE = "";
        var D_CLUSTER_ID = "NONE";
        var D_CLUSTER_NAME = "";
        var D_CLUSTER_NAME_AD = "";
        var D_CLUSTER_CODE_AD = "";
        if (CLUSTER[CLUSTER_ID] == undefined) {
          if (CLUSTER_DECLARE[ROW_KEY] == undefined) {
            var SPLIT_NAME = ROW_KEY;//ROW_KEY.split(".").join("").split("PT ").join("").split("CV ").join("");
            var FIND_NAME = await this.adClusterRepository.find({where: {and: [{ad_name: {like: '%' + SPLIT_NAME + '%'}}]}});
            if (FIND_NAME.length > 1 || FIND_NAME.length == 0) {

              if (FIND_NAME.length > 1) {
                var FOUND = 0;
                var FOUND_CLUSTER: any; FOUND_CLUSTER = {}
                var check: any; check = [];
                for (var iNAME in FIND_NAME) {
                  var RAW_NAME = FIND_NAME[iNAME];
                  if (CLUSTER_SET.indexOf(RAW_NAME.ad_cluster_id) == -1) {
                    FOUND_CLUSTER = RAW_NAME;
                    FOUND++;
                  }
                }

                if (FOUND > 1) {
                  FLAG_AVALIABBLE = 0;
                  MESSAGE = "CLUSTER NAME not found !";
                } else {
                  D_CLUSTER_ID = FOUND_CLUSTER.ad_cluster_id;
                  D_CLUSTER_NAME = FOUND_CLUSTER.ad_cluster_name
                  D_CLUSTER_NAME_AD = FOUND_CLUSTER.ad_cluster_name;
                  D_CLUSTER_CODE_AD = FOUND_CLUSTER.ad_code;
                }

              } else {
                FLAG_AVALIABBLE = 0;
                MESSAGE = "CLUSTER NAME not found !";
              }
            } else {
              for (var iNAME in FIND_NAME) {
                var RAW_NAME = FIND_NAME[iNAME];
                D_CLUSTER_ID = RAW_NAME.ad_cluster_id;
                D_CLUSTER_NAME = RAW_NAME.ad_cluster_name
                D_CLUSTER_NAME_AD = RAW_NAME.ad_cluster_name;
                D_CLUSTER_CODE_AD = RAW_NAME.ad_code;
              }
            }

          } else {
            D_CLUSTER_ID = CLUSTER_DECLARE[CLUSTER_ID]["ad_cluster_id"];
            D_CLUSTER_NAME = CLUSTER_DECLARE[CLUSTER_ID]["ad_cluster_name"];
            D_CLUSTER_NAME_AD = CLUSTER_DECLARE[CLUSTER_ID]["ad_name"];
            D_CLUSTER_CODE_AD = CLUSTER_DECLARE[CLUSTER_ID]["ad_code"];
          }
        } else {
          D_CLUSTER_ID = CLUSTER[CLUSTER_ID]["ad_cluster_id"];
          D_CLUSTER_NAME = CLUSTER[CLUSTER_ID]["ad_cluster_name"];
          D_CLUSTER_NAME_AD = CLUSTER[CLUSTER_ID]["ad_name"];
          D_CLUSTER_CODE_AD = CLUSTER[CLUSTER_ID]["ad_code"];
        }

        var INFO_MASTER_AD = await this.adMasterRepository.findOne({where: {ad_code: D_CLUSTER_CODE_AD}});
        var INFO_CLUSTER_AD = await this.adClusterRepository.findOne({where: {and: [{ad_code: D_CLUSTER_CODE_AD}, {ad_cluster_id: D_CLUSTER_ID}]}});

        // if (cluster_raw.length == 1) {
        //   if (INFO_AD_STORE[cluster_raw[0].split(".").join("").toUpperCase()] == null) {
        //     status = 0;
        //     message += ",cluster code not found !";
        //   } else {
        //     adMaster = INFO_AD_STORE[cluster_raw[0].split(".").join("").toUpperCase()];
        //     if (AD.indexOf(adMaster.ad_code) == -1) {
        //       AD.push(adMaster.ad_code);
        //     }
        //   }
        // } else {
        //   if (cluster_raw.length == 2) {
        //     adCluster = await this.adClusterRepository.findOne({where: {ad_cluster_id: cluster_raw[0].toString()}});
        //     if (adCluster == null) {
        //       if (INFO_AD_STORE[cluster_raw[0].split(".").join("").toUpperCase()] == undefined) {
        //         status = 0;
        //         message += ",cluster code not found !";
        //       } else {
        //         adMaster = INFO_AD_STORE[cluster_raw[0].split(".").join("").toUpperCase()];
        //         if (AD.indexOf(adMaster.ad_code) == -1) {
        //           AD.push(adMaster.ad_code);
        //         }
        //       }
        //     } else {
        //       adMaster = await this.adMasterRepository.findById(adCluster.ad_code);
        //       if (adMaster != null) {
        //         if (AD.indexOf(adMaster.ad_code) == -1) {
        //           AD.push(adMaster.ad_code);
        //         }
        //       }
        //     }
        //   } else {
        //     status = 0;
        //     message += ",cluster code not found !";
        //   }
        // }



        DataRawPPOB.push({
          unique_key: rawData.UniqueKey,
          periode: periodeReq.periode,
          dealer_code: INFO_CLUSTER_AD != null ? INFO_CLUSTER_AD.ad_cluster_id : ROW_KEY,//adMaster?.ad_code,
          location: INFO_CLUSTER_AD != null ? INFO_CLUSTER_AD.ad_cluster_name : ROW_KEY,
          name: INFO_MASTER_AD != null ? (INFO_MASTER_AD?.global_name == null ? INFO_MASTER_AD?.ad_name : INFO_MASTER_AD?.global_name) : "-",
          ad_code: INFO_MASTER_AD != null ? INFO_MASTER_AD?.ad_code : "-",
          ad_info: {
            alamat: INFO_MASTER_AD?.address,
            alamat_npwp: INFO_MASTER_AD?.npwp_address,
            attention: INFO_MASTER_AD?.attention,
            attention_position: INFO_MASTER_AD?.attention_position,
            no_rek: INFO_MASTER_AD?.no_rek,
            name_rekening: INFO_MASTER_AD?.name_rekening,
            name_bank: INFO_MASTER_AD?.name_bank,
            legal_name: INFO_MASTER_AD?.legal_name,
            global_name: INFO_MASTER_AD?.global_name,
            name: INFO_MASTER_AD?.ad_name
          },
          type: "PPOB",
          sales_fee: FEE_PPOB,
          paid_in_amount: FEE_PPOB,
          withdraw_amount: 0,
          trx_a: Number(rawData.count_trx),
          tot_mdr_a: Number(rawData.count_trx) * FEE_PPOB,
          trx_b: 0,
          tot_mdr_b: 0,
          reason_type_id: "-1",
          reason_type_name: rawData.reason_name,
          status: FLAG_AVALIABBLE,
          message: MESSAGE
        });

      }

      returnMs.datas = {
        total_raw: TotalRaw,
        total_paid: TotalPaid,
        total_fee: TotalFee,
        total_trx_a: TotalTrxA,
        total_trx_b: TotalTrxB,
        count_ad: AD.length
      };
    } else {
      returnMs.success = false;
      returnMs.message = "Error, Periode not declare !";
    }

    return returnMs;
  }

  @post('/big-query/inquery/detail/recharge', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: PeriodeDetailRequestBody
          }
        },
      },
    },
  })
  async detailRechargeBQ(
    @requestBody(PeriodeDetailRequestBody) rawFilter: {periode: string, ad_code: string},
  ): Promise<Object> {
    var returnMs: any; returnMs = {success: true, message: "", data: []};

    var PeriodeTrx = rawFilter.periode.split("-");
    var dateLast = new Date(Number(PeriodeTrx[0]), Number(PeriodeTrx[1]), 0);
    var invMounth = dateLast.getMonth() + 1;
    var dateMountLast = dateLast.getFullYear() + "-" + "00".substring(invMounth.toString().length) + invMounth + "-" + dateLast.getDate();
    var dateMountFirst = dateLast.getFullYear() + "-" + "00".substring(invMounth.toString().length) + invMounth + "-" + "01";


    var adMaster = await this.adMasterRepository.findById(rawFilter.ad_code);
    if (adMaster.ad_code != undefined) {
      var Name = adMaster.ad_name.split("PT").join("").split("CV").join("").split(".").join("").trim().toUpperCase();
      console.log(Name);
      //FOR RECHARGE
      const bigquery = new BigQuery();
      const sqlQuery = "SELECT * FROM `project-datamart.datamart_digipos.dm_digipos_revenue`WHERE partition_transaction_end_date_local BETWEEN  @first_date AND @last_date AND  ( credit_party_mnemonic_a LIKE @name OR debit_party_mnemonic_a LIKE @name_d )";
      const options = {
        query: sqlQuery,
        params: {name_d: "%" + Name + "%", name: "%" + Name + "%", first_date: new BigQueryDate(dateMountFirst), last_date: new BigQueryDate(dateMountLast)},
      }

      const [rows] = await bigquery.query(options);
      for (var i in rows) {
        var infoRecharge = rows[i];
        var dataRecharge = {
          ddate: infoRecharge.partition_transaction_end_date_local,
          orderid_a: infoRecharge.trans_index,
          order_id_tsel: infoRecharge.order_id_tsel,
          credit_party_mnemonic_a: infoRecharge.credit_party_mnemonic_a,
          debit_party_mnemonic_a: infoRecharge.debit_party_mnemonic_a,
          credit_party_mnemonic_b: infoRecharge.credit_party_mnemonic_b,
          debit_party_mnemonic_b: infoRecharge.debit_party_mnemonic_b,
          orderid_b: infoRecharge.orderid_b,
          paid_in_amount: infoRecharge.paid_in_amount,
          withdraw_amount: infoRecharge.withdraw_amount,
          request_amount_b: infoRecharge.request_amount_b,
          request_amount_a: infoRecharge.request_amount_a,
          reason_type_id: infoRecharge.reason_type_id
        }
        returnMs.data.push(dataRecharge);
      }

    } else {
      returnMs.success = true;
      returnMs.data = [];
    }
    return returnMs;
  }

  @post('/big-query/inquery/detail/ppob', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: PeriodeDetailRequestBody
          }
        },
      },
    },
  })
  async detailPPOBBQ(
    @requestBody(PeriodeDetailRequestBody) rawFilter: {periode: string, ad_code: string},
  ): Promise<Object> {
    var returnMs: any; returnMs = {success: true, message: "", data: []};

    var PeriodeTrx = rawFilter.periode.split("-");
    var dateLast = new Date(Number(PeriodeTrx[0]), Number(PeriodeTrx[1]), 0);
    var invMounth = dateLast.getMonth() + 1;
    var dateMountLast = dateLast.getFullYear() + "-" + "00".substring(invMounth.toString().length) + invMounth + "-" + dateLast.getDate();
    var dateMountFirst = dateLast.getFullYear() + "-" + "00".substring(invMounth.toString().length) + invMounth + "-" + "01";


    var adMaster = await this.adMasterRepository.findById(rawFilter.ad_code);
    if (adMaster.ad_code != undefined) {
      var Name = adMaster.ad_name.split("PT").join("").split("CV").join("").split(".").join("").trim().toUpperCase();

      //FOR PPOB
      const bigquery = new BigQuery();
      const sqlQueryPPOB = "SELECT * FROM `project-self-service-query.digipos_finance.dm_ppob_revenue` WHERE partition_transaction_end_date_local BETWEEN  @first_date_ppob AND @last_date_ppob AND  top_biz_org_name_a LIKE @name_ppob";
      const optionsPPOB = {
        query: sqlQueryPPOB,
        params: {name_ppob: "%" + Name + "%", first_date_ppob: new BigQueryDate(dateMountFirst), last_date_ppob: new BigQueryDate(dateMountLast)},
      }

      const [rowsPPOB] = await bigquery.query(optionsPPOB);
      for (var iPPPOB in rowsPPOB) {
        returnMs.data.push(rowsPPOB[iPPPOB]);
      }

    } else {
      returnMs.success = true;
      returnMs.data = [];
    }
    return returnMs;
  }

  @post('/big-query/inquery/progress-transaction', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: PeriodeRequestBody
          }
        },
      },
    },
  })
  async dashboardBQ(
    @requestBody(PeriodeRequestBody) rawFilter: {periode: string},
  ): Promise<Object> {
    var returnMs: any; returnMs = {success: true, message: "", data: {RECHARGE: {}, PPOB: {}}};

    PERIODE = rawFilter.periode;

    var PeriodeTrx = rawFilter.periode.split("-");
    var dateLast = new Date(Number(PeriodeTrx[0]), Number(PeriodeTrx[1]), 0);
    var invMounth = dateLast.getMonth() + 1;
    var dateMountLast = dateLast.getFullYear() + "-" + "00".substring(invMounth.toString().length) + invMounth + "-" + dateLast.getDate();
    var dateMountFirst = dateLast.getFullYear() + "-" + "00".substring(invMounth.toString().length) + invMounth + "-" + "01";

    //FOR   RECHARGE
    const bigquery = new BigQuery();
    const sqlQueryRecharge = "SELECT " +
      " partition_transaction_end_date_local, credit_party_mnemonic_a, sum(paid_in_amount) sum_paid, sum(withdraw_amount) sum_withdraw, COUNT(trans_index) count_trx " +
      "FROM `project-datamart.datamart_digipos.dm_digipos_revenue` WHERE partition_transaction_end_date_local BETWEEN  @first_date_recharge AND @last_date_recharge " +
      "GROUP BY partition_transaction_end_date_local, credit_party_mnemonic_a";

    const optionsRecharge = {
      query: sqlQueryRecharge,
      params: {first_date_recharge: new BigQueryDate(dateMountFirst), last_date_recharge: new BigQueryDate(dateMountLast)},
    }

    const [rowsRecharge] = await bigquery.query(optionsRecharge);
    var datas: any; datas = {}

    var ad_ppob: any; ad_ppob = {};
    var ad_count: any; ad_count = {};
    for (var iRecharge in rowsRecharge) {
      var infoRecharge = rowsRecharge[iRecharge];
      var periodeDates = infoRecharge.partition_transaction_end_date_local.value;
      if (datas[periodeDates] == undefined) {
        datas[periodeDates] = {RECHARGE: {count_trx: 0, ad_count: 0, paid_in_amount: 0, withdraw_amount: 0}, PPOB: {count_trx: 0, ad_count: 0, paid_in_amount: 0, withdraw_amount: 0}};
        ad_count[periodeDates] = [];
        ad_ppob[periodeDates] = [];
      }
      datas[periodeDates].RECHARGE.count_trx += Number(infoRecharge.count_trx);
      datas[periodeDates].RECHARGE.paid_in_amount += Number(infoRecharge.sum_paid);
      datas[periodeDates].RECHARGE.withdraw_amount += Number(infoRecharge.sum_withdraw);

      if (ad_count[periodeDates].indexOf(infoRecharge.credit_party_mnemonic_a) == -1) {
        ad_count[periodeDates].push(infoRecharge.credit_party_mnemonic_a);
      }

      datas[periodeDates].RECHARGE.ad_count = ad_count[periodeDates].length;

    }


    var sqlPPOB = "SELECT " +
      "partition_transaction_end_date_local, top_biz_org_a, sum(portion_ad) sum_paid, COUNT(trans_index) count_trx " +
      "FROM `project-self-service-query.digipos_finance.dm_ppob_revenue` WHERE partition_transaction_end_date_local BETWEEN  @first_date_ppob AND @last_date_ppob " +
      "GROUP BY partition_transaction_end_date_local, top_biz_org_a ";

    const optionsPPOB = {
      query: sqlPPOB,
      params: {first_date_ppob: new BigQueryDate(dateMountFirst), last_date_ppob: new BigQueryDate(dateMountLast)},
    }
    const [rowsPPOB] = await bigquery.query(optionsPPOB);


    for (var iPPOB in rowsPPOB) {
      var infoPPOB = rowsPPOB[iPPOB];
      var periodeDates = infoPPOB.partition_transaction_end_date_local.value;
      if (datas[periodeDates] == undefined) {
        datas[periodeDates] = {RECHARGE: {count_trx: 0, ad_count: 0, paid_in_amount: 0, withdraw_amount: 0}, PPOB: {count_trx: 0, ad_count: 0, paid_in_amount: 0, withdraw_amount: 0}};
        ad_ppob[periodeDates] = [];
      }

      datas[periodeDates].PPOB.count_trx += Number(infoPPOB.count_trx);
      datas[periodeDates].PPOB.paid_in_amount += Number(infoPPOB.sum_paid);
      datas[periodeDates].PPOB.withdraw_amount += 0;

      if (ad_ppob[periodeDates].indexOf(infoPPOB.top_biz_org_a) == -1) {
        ad_ppob[periodeDates].push(infoPPOB.top_biz_org_a);
      }
      datas[periodeDates].PPOB.ad_count = ad_ppob[periodeDates].length;
    }

    returnMs.data = datas;

    return returnMs;
  }




  // @get('/big-query/inquery-recharge/raw', {
  //   responses: {
  //     200: {
  //       content: {
  //         // string[]
  //         'application/json': {
  //           schema: {
  //             type: 'array',
  //             items: {
  //               type: 'string',
  //             },
  //           },
  //         },
  //       },
  //       description: 'A list of files',
  //     },
  //   },
  // })
  // async bigqueryView() {
  // var ContentFPJP: any; ContentFPJP = {
  //   name_approval: "Yudhistira",
  //   fpjp_number: "1110000",
  //   fpjp_submit: "Bagus Purnomo",
  //   link_approval: "https://digipos.realclickpro.com/dashboard/transaction-bu/generate-fpjp-pdf-ad-b",
  //   fpjp_type: "Nota Debit",
  //   fpjp_name: "Nota Debit - PT-Sentosa",
  //   currency: "Rp ( Rupiah )",
  //   amount: "Rp 200.000.000",
  //   link_list_approval: "https://digipos.realclickpro.com/dashboard/transaction-bu/generate-fpjp-pdf-ad-b"
  // }
  // ControllerConfig.onSendMailFPJP("yudhistira.job@gmail.com", ContentFPJP, (r: any) => {
  //   console.log(r);
  // })

  // var SENDGRID_APY_KEY = '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';
  // // sgMail.setApiKey(process.env.SENDGRID_API_KEY);
  // // console.log(process.env.SENDGRID_API_KEY);
  // const msg = {
  //   from: "gugumnana@gmail.com",
  //   subject: 'Sending with SendGrid is Fun',
  //   templateId: "3380f6ef-09d7-475b-a9b5-47d33715c121",
  //   personalizations: [
  //     {
  //       to: [{"email": "yudhistira.job@gmail.com"}],
  //       subject: "YOUR SUBJECT LINE GOES HERE"
  //     }
  //   ],
  //   substitutions: {
  //     'title': 'SendGrid',
  //     'body': 'John Doe',
  //   },
  // };

  // // sgMail.send(msg, false, (err, result) => {
  // //   if (err) {
  // //     console.log(err);
  // //   } else {
  // //     console.log("That's wassup!");
  // //   }
  // // })

  // sgMail.setApiKey("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");
  // // console.log("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");
  // // const msg = {
  // //   to: "yudhistira.job@gmail.com", // Change to your recipient
  // //   from: "gugumnana@gmail.com", // Change to your verified sender
  // //   subject: 'Sending with SendGrid is Fun',
  // //   text: 'and easy to do anywhere, even with Node.js',
  // //   html: '<strong>and easy to do anywhere, even with Node.js</strong>',
  // // }
  // sgMail
  //   .send(msg)
  //   .then(() => {
  //     console.log('Email sent')
  //   })
  //   .catch((error) => {
  //     console.error(error)
  //   })


  // const bigquery = new BigQuery();
  // const [datasets] = await bigquery.getDatasets();
  // console.log('Datasets:');
  // datasets.forEach(dataset => console.log(dataset.id));

  // const [tables] = await bigquery.dataset("digipos_finance").getTables();
  // console.log('Tables:');
  // // tables.forEach(table => console.log(table.id));


  // // // // const [dataset] = await bigquery.dataset("digipos_finance").get();
  // // // // console.log('Dataset Info:');
  // // // // console.log(dataset.metadata.datasetReference);
  // // // const database = bigquery.dataset("digipos_finance");
  // // const destinationTable = database.table("agg_digipos_revenue_daily");

  // const sqlQuery = "SELECT *  FROM `project-self-service-query.digipos_finance.agg_digipos_revenue_daily`  WHERE partition_transaction_end_date_local LIKE '" + periode + "%' ";
  // const options = {
  //   query: sqlQuery,
  //   // destination: destinationTable,
  //   // params: {ad_name: '42130 - PT. ALCOM'}
  // }

  // const [rows] = await bigquery.query(options);
  // for (var i in rows) {
  //   var rawData = rows[i];
  //   console.log(rawData);
  //   break;
  // }
  //   await this.dataRechargeDetailRepository.create({
  //     periode: "2020-12",
  //     dealer_code: rawData.credit_party_mnemonic,
  //     location: "-",
  //     name: rawData.credit_party_mnemonic_b,
  //     type_trx: "-",
  //     paid_in_amount: Number(rawData.paid_in_amount),
  //     withdraw_amount: Number(rawData.withdraw_amount),
  //     reason_type_id: Number(rawData.reason_type_id),
  //     reason_type_name: "-",
  //     trx_a: Number(rawData.trx_a),
  //     tot_mdr_a: Number(rawData.tot_mdr_a),
  //     trx_b: Number(rawData.trx_b),
  //     tot_mdr_b: Number(rawData.tot_mdr_b),
  //     status: 0,
  //     create_at: rawData.partition_transaction_end_date_local.value,
  //     state: 1,
  //     pos_index: 0,
  //     code_ad: Number(rawData.Row).toString()
  //   })
  //   // console.log(rawData.credit_party_mnemonic_b, Number(rawData.flag_fut), rawData.partition_transaction_end_date_local.value);
  // }

  //  Number(rawData.paid_in_amount)

  // const sqlQuery = `SELECT *
  // FROM \`digipos_finance.agg_digipos_revenue_monthly\`
  // LIMIT 10`;

  // const options = {
  //   query: sqlQuery,
  //   destination: destinationTable,
  // };

  // const [rows] = await bigquery.query(options);
  // console.log('Rows:');
  // rows.forEach(row => console.log(row));


  // const dataset = bigquery.dataset(" project-self-service-query:digipos_finance");
  // const [view] = await dataset.table("dm_digipos_revenue").get();
  // const fullTableId = view.metadata.id;
  // const viewQuery = view.metadata.view.query;

  // // Display view properties
  // console.log(`View at ${fullTableId}`);
  // console.log(`View query: ${viewQuery}`);

  // const database = bigquery.dataset("digipos_finance");
  // const destinationTable = database.table("agg_digipos_revenue_monthly");

  // const sqlQuery = "SELECT * FROM digipos_finance.agg_digipos_revenue_monthly";

  // const options = {
  //   query: sqlQuery,
  //   // Location must match that of the dataset(s) referenced in the query.
  //   // location: 'US',
  //   // params: {corpus: 'romeoandjuliet', min_word_count: 250},
  // };

  // // // // Run the query
  // const [rows] = await bigquery.query(options);
  // console.log('Rows:', rows);
  // rows.forEach(row => console.log(row));

  // const query = `SELECT * FROM
  // \`digipos_finance.agg_digipos_revenue_monthly\`
  // LIMIT 5`;

  // // For all options, see https://cloud.google.com/bigquery/docs/reference/rest/v2/jobs/query
  // const options = {
  //   query: query,
  //   // Location must match that of the dataset(s) referenced in the query.
  //   location: 'US',
  //   destination: destinationTable,
  // };

  // // Run the query as a job
  // const [job] = await bigquery.createQueryJob(options);
  // console.log(`Job ${job.id} started.`);

  // // Wait for the query to finish
  // const [rows] = await job.getQueryResults();

  // // Print the results
  // console.log('Rows:');
  // rows.forEach(row => console.log(row));

  // const query = `SELECT name
  //   FROM \`digipos_finance.agg_digipos_revenue_monthly\`
  //   LIMIT 100`;

  // // For all options, see https://cloud.google.com/bigquery/docs/reference/rest/v2/jobs/query
  // const options = {
  //   query: query,
  //   // location: 'US',
  // };

  // // Run the query as a job
  // const [job] = await bigquery.createQueryJob(options);
  // console.log(`Job ${job.id} started.`);

  // // Wait for the query to finish
  // const [rows] = await job.getQueryResults();

  // // Print the results
  // console.log('Rows:');
  // rows.forEach(row => console.log(row));
  // const options = {maxResults: 10};
  // const [jobs] = await bigquery.getJobs(options);

  // console.log('Jobs:');
  // jobs.forEach(job => console.log(job));

  //   return "work"
  // }


  // @get('/files/ba/{filename}')
  // @oas.response.file()
  // downloadFileBA(
  //   @param.path.string('filename') fileName: string,
  //   @inject(RestBindings.Http.RESPONSE) response: Response,
  // ) {
  //   const filePathA = path.join(__dirname, '../../.digipost/berita_acara/ad_a/' + fileName);
  //   const filePathB = path.join(__dirname, '../../.digipost/berita_acara/ad_b/' + fileName);
  //   var fileFound = false;
  //   var filePathFound = "notfound";

  //   try {
  //     if (fs.existsSync(filePathA)) {
  //       //file exists
  //       fileFound = true;
  //       filePathFound = filePathA;
  //     }
  //   } catch (err) {
  //     console.error(err)
  //   }

  //   try {
  //     if (fs.existsSync(filePathB)) {
  //       //file exists
  //       fileFound = true;
  //       filePathFound = filePathB;
  //     }
  //   } catch (err) {
  //     console.error(err)
  //   }
  //   // // const file = this.validateFileName(filePath, fileName);
  //   // // response.download(file, fileName);
  //   // // var stream = fs.readStream(filePath);
  //   // var file = encodeURIComponent(fileName);
  //   // response.setHeader('Content-disposition', 'inline; filename="' + file + '"');
  //   // response.setHeader('Content-type', 'application/pdf');

  //   fs.readFile(filePathFound, function (err, data) {
  //     if (err) {
  //       response.writeHead(404);
  //       response.end();
  //     } else {
  //       response.contentType("application/pdf");
  //       response.writeHead(200, {'Content-Type': "application/pdf"});
  //       response.end(data, 'utf-8');
  //     }

  //   });
  //   return response;
  // }



  // @get('/files/invoice/{filename}')
  // @oas.response.file()
  // downloadFileInvoice(
  //   @param.path.string('filename') fileName: string,
  //   @inject(RestBindings.Http.RESPONSE) response: Response,
  // ) {
  //   const filePathInvoice = path.join(__dirname, '../../.digipost/invoice/' + fileName);

  //   fs.readFile(filePathInvoice, function (err, data) {
  //     if (err) {
  //       response.writeHead(404);
  //       response.end();
  //     } else {
  //       response.contentType("application/pdf");
  //       response.writeHead(200, {'Content-Type': "application/pdf"});
  //       response.end(data, 'utf-8');
  //     }

  //   });
  //   return response;
  // }

  // onbase64_encode = (file: any) => {
  //   // read binary data
  //   var bitmap = fs.readFileSync(file);
  //   // convert binary data to base64 encoded string
  //   return 'data:image/jpeg;base64, ' + Buffer.from(bitmap).toString('base64');
  // }

  // @get('/files/npwp/{filename}')
  // @oas.response.file()
  // downloadFileNPWP(
  //   @param.path.string('filename') fileName: string,
  //   @inject(RestBindings.Http.RESPONSE) response: Response,
  // ) {
  //   const filePath = path.join(__dirname, '../../.digipost/profile_npwp/' + fileName);
  //   var fileFound = false;
  //   var filePathFound = "notfound";

  //   try {
  //     if (fs.existsSync(filePath)) {
  //       //file exists
  //       fileFound = true;
  //       filePathFound = filePath;
  //     }
  //   } catch (err) {
  //     console.error(err)
  //   }

  //   // filePathFound
  //   fs.readFile(filePathFound, (err, data) => {
  //     if (err) {
  //       response.writeHead(404);
  //       response.end();
  //     } else {
  //       response.contentType("image/png");
  //       response.writeHead(200, {'Content-Type': "image/png"});
  //       // response.writeHead(200, );
  //       // // response.end();
  //       // // response.contentType("application/pdf");
  //       // // response.writeHead(200, {'Content-Type': "image/*"});
  //       // response.end(data, 'utf-8');
  //       // response.writeHead(200, {'Content-Type': 'text/html'});
  //       // response.write('<img src="' + this.onbase64_encode(filePathFound) + '"/>');
  //       response.end(data, 'utf-8');
  //     }

  //   });
  //   return response;
  // }


  // @get('/files/fpjp/{filename}')
  // @oas.response.file()
  // downloadFPJP(
  //   @param.path.string('filename') fileName: string,
  //   @inject(RestBindings.Http.RESPONSE) response: Response,
  // ) {
  //   const filePath = path.join(__dirname, '../../.digipost/fpjp/' + fileName);

  //   // filePathFound
  //   fs.readFile(filePath, function (err, data) {
  //     if (err) {
  //       response.writeHead(404);
  //       response.end();
  //     } else {
  //       response.contentType("application/pdf");
  //       response.writeHead(200, {'Content-Type': "application/pdf"});
  //       response.end(data, 'utf-8');
  //     }

  //   });
  //   return response;
  // }


  // @get('/files/sign/{filename}')
  // @oas.response.file()
  // downloadFileAtten(
  //   @param.path.string('filename') fileName: string,
  //   @inject(RestBindings.Http.RESPONSE) response: Response,
  // ) {
  //   const filePath = path.join(__dirname, '../../.digipost/sign_atten/' + fileName);
  //   var fileFound = false;
  //   var filePathFound = "notfound";

  //   try {
  //     if (fs.existsSync(filePath)) {
  //       //file exists
  //       fileFound = true;
  //       filePathFound = filePath;
  //     }
  //   } catch (err) {
  //     console.error(err)
  //   }

  //   // filePathFound
  //   fs.readFile(filePathFound, (err, data) => {
  //     if (err) {
  //       response.writeHead(404);
  //       response.end();
  //     } else {
  //       response.contentType("image/png");
  //       response.writeHead(200, {'Content-Type': "image/png"});
  //       // response.writeHead(200, );
  //       // // response.end();
  //       // // response.contentType("application/pdf");
  //       // // response.writeHead(200, {'Content-Type': "image/*"});
  //       // response.end(data, 'utf-8');
  //       // response.writeHead(200, {'Content-Type': 'text/html'});
  //       // response.write('<img src="' + this.onbase64_encode(filePathFound) + '"/>');
  //       response.end(data, 'utf-8');
  //     }

  //   });
  //   return response;
  // }

  // /**
  //  * Validate file names to prevent them goes beyond the designated directory
  //  * @param fileName - File name
  //  */
  // private validateFileName(pathFile: string, fileName: string) {
  //   const resolved = path.resolve(pathFile, fileName);
  //   if (resolved.startsWith(pathFile)) return resolved;
  //   // The resolved file is outside sandbox
  //   throw new HttpErrors.BadRequest(`Invalid file name: ${fileName}`);
  // }



  //============================================================================MAPPING NEW CLUSTER TMP
  // @get('/big-query-sycronize/cluster-tmp/view', {
  //   responses: {
  //     '200': {
  //       description: 'DataDigipos model instance',
  //       content: {
  //         'application/json': {
  //           schema: BigQueryReqPeriode
  //         }
  //       },
  //     },
  //   },
  // })
  // async findClusterTmp(): Promise<Object> {
  //   var returnMs: any; returnMs = {success: true, message: "success", data: []};
  //   var data: any; data = [];
  //   for (var i in CLUSTER_DECLARE) {
  //     var RAW_DECLARE = CLUSTER_DECLARE[i];
  //     var THIS_DATA = {
  //       BEFORE_CLUSTER: i,
  //       AFTER_CLUSTER: RAW_DECLARE.ad_cluster_id,
  //       D_CLUSTER_NAME: RAW_DECLARE.ad_cluster_name,
  //       D_CLUSTER_NAME_AD: RAW_DECLARE.ad_name,
  //       D_CLUSTER_CODE_AD: RAW_DECLARE.ad_code,
  //     }
  //     data.push(THIS_DATA);
  //   }
  //   returnMs.data = data;
  //   return returnMs;
  // }

  // @post('/big-query-sycronize/cluster-tmp/create-multi', {
  //   responses: {
  //     '200': {
  //       description: 'BigQuery Create Cluster TMP model instance',
  //       content: {
  //         'application/json': {
  //           schema: BigQueryReqCreateMultiClusterTmp
  //         }
  //       },
  //     },
  //   },
  // })
  // async CreateClusterTmpMulti(
  //   @requestBody(BigQueryReqCreateMultiClusterTmp) reqCreateClusterTmp: {
  //     cluster: any
  //   },
  // ): Promise<Object> {
  //   var returnMs: any; returnMs = {success: true, success_create: [], error_create: [], data: {}};

  //   var errorCluster: any; errorCluster = [];
  //   var successCluster: any; successCluster = [];
  //   for (var i in reqCreateClusterTmp.cluster) {
  //     var textCreate = reqCreateClusterTmp.cluster[i].split("#");
  //     if (CLUSTER_DECLARE[textCreate[0]] == undefined) {
  //       var cluster = await this.adClusterRepository.findById(textCreate[1]);
  //       if (cluster.ad_cluster_id != undefined) {
  //         CLUSTER_DECLARE[textCreate[0]] = cluster;
  //         if (successCluster.indexOf(textCreate[1]) == -1) {
  //           successCluster.push(textCreate[1]);
  //         }
  //       } else {
  //         if (errorCluster.indexOf(textCreate[1]) == -1) {
  //           errorCluster.push(textCreate[1]);
  //         }
  //       }
  //     } else {
  //       continue;
  //     }
  //   }
  //   returnMs.success_create = successCluster;
  //   returnMs.error_create = errorCluster;
  //   return returnMs;
  // }

  // @post('/big-query-sycronize/cluster-tmp/create', {
  //   responses: {
  //     '200': {
  //       description: 'BigQuery Create Cluster TMP model instance',
  //       content: {
  //         'application/json': {
  //           schema: BigQueryReqCreateClusterTmp
  //         }
  //       },
  //     },
  //   },
  // })
  // async CreateClusterTmp(
  //   @requestBody(BigQueryReqCreateClusterTmp) reqCreateClusterTmp: {
  //     before_cluster: string, after_cluster: string
  //   },
  // ): Promise<Object> {
  //   var returnMs: any; returnMs = {success: true, message: "", data: {}};

  //   if (CLUSTER_DECLARE[reqCreateClusterTmp.before_cluster] == undefined) {
  //     var cluster = await this.adClusterRepository.findById(reqCreateClusterTmp.after_cluster);
  //     if (cluster.ad_cluster_id != undefined) {
  //       CLUSTER_DECLARE[reqCreateClusterTmp.before_cluster] = cluster;
  //       returnMs.success = true;
  //       returnMs.message = "success !";
  //     } else {
  //       returnMs.success = false;
  //       returnMs.message = "Error, cluster id not found !";
  //     }
  //   } else {
  //     returnMs.success = false;
  //     returnMs.message = "Error, cluster name already create !";
  //   }
  //   return returnMs;
  // }

  // @post('/big-query-sycronize/cluster-tmp/delete', {
  //   responses: {
  //     '200': {
  //       description: 'BigQuery Create Cluster TMP model instance',
  //       content: {
  //         'application/json': {
  //           schema: BigQueryReqDeleteClusterTmp
  //         }
  //       },
  //     },
  //   },
  // })
  // async DeleteClusterTmp(
  //   @requestBody(BigQueryReqDeleteClusterTmp) reqCreateClusterTmp: {
  //     before_cluster: string,
  //   },
  // ): Promise<Object> {
  //   var returnMs: any; returnMs = {success: true, message: "", data: {}};
  //   if (CLUSTER_DECLARE[reqCreateClusterTmp.before_cluster] != undefined) {
  //     delete CLUSTER_DECLARE[reqCreateClusterTmp.before_cluster];
  //     returnMs = {success: true, message: "success !", data: {}};
  //   } else {
  //     returnMs = {success: false, message: "error, before cluster not found!", data: {}};
  //   }
  //   return returnMs;
  // }

  // @post('/big-query-sycronize/cluster-tmp/clear', {
  //   responses: {
  //     '200': {
  //       description: 'BigQuery Create Cluster TMP model instance',
  //       content: {
  //         'application/json': {
  //           schema: BigQueryReqCreateClusterTmp
  //         }
  //       },
  //     },
  //   },
  // })
  // async ClearClusterTmp(): Promise<Object> {
  //   var returnMs: any; returnMs = {success: true, message: "success !!", data: {}};
  //   CLUSTER_DECLARE = {};
  //   return returnMs;
  // }

  // //============================================================================ CLUSTER SKIP
  // @get('/big-query-sycronize/cluster-skip/view', {
  //   responses: {
  //     '200': {
  //       description: 'BigQuery Cluster SKIP model instance',
  //       content: {
  //         'application/json': {
  //           schema: BigQueryReqClusterSkip
  //         }
  //       },
  //     },
  //   },
  // })
  // async findClusterSkip(): Promise<Object> {
  //   var returnMs: any; returnMs = {success: true, message: "success", data: []};
  //   returnMs.data = CLUSTER_SKIP;
  //   return returnMs;
  // }

  // @post('/big-query-sycronize/cluster-skip/create', {
  //   responses: {
  //     '200': {
  //       description: 'BigQuery Create Cluster SKIP model instance',
  //       content: {
  //         'application/json': {
  //           schema: BigQueryReqClusterSkip
  //         }
  //       },
  //     },
  //   },
  // })
  // async CreateClusterSkip(
  //   @requestBody(BigQueryReqClusterSkip) reqCluster: {
  //     cluster: string,
  //   },
  // ): Promise<Object> {
  //   var returnMs: any; returnMs = {success: true, message: "", data: {}};
  //   if (CLUSTER_SKIP.indexOf(reqCluster.cluster) == -1) {
  //     CLUSTER_SKIP.push(reqCluster.cluster);
  //   } else {
  //     returnMs = {success: false, message: "error, cluster skip already exities create !", data: {}};
  //   }
  //   return returnMs;
  // }

  // @post('/big-query-sycronize/cluster-skip/delete', {
  //   responses: {
  //     '200': {
  //       description: 'BigQuery Create Cluster TMP model instance',
  //       content: {
  //         'application/json': {
  //           schema: BigQueryReqClusterSkip
  //         }
  //       },
  //     },
  //   },
  // })
  // async DeleteClusterSkip(
  //   @requestBody(BigQueryReqClusterSkip) reqClusterSkip: {
  //     cluster: string,
  //   },
  // ): Promise<Object> {
  //   var returnMs: any; returnMs = {success: true, message: "", data: {}};
  //   if (CLUSTER_SKIP.indexOf(reqClusterSkip.cluster) != -1) {
  //     var key = CLUSTER_SKIP.indexOf(reqClusterSkip.cluster);
  //     CLUSTER_SKIP.splice(key, 1);
  //     returnMs = {success: true, message: "success !", data: {}};
  //   } else {
  //     returnMs = {success: false, message: "error,  cluster skip not found !", data: {}};
  //   }
  //   return returnMs;
  // }

  // @post('/big-query-sycronize/cluster-skip/clear', {
  //   responses: {
  //     '200': {
  //       description: 'BigQuery Create Cluster TMP model instance',
  //       content: {
  //         'application/json': {
  //           schema: BigQueryReqClusterSkip
  //         }
  //       },
  //     },
  //   },
  // })
  // async ClearClusterSkip(): Promise<Object> {
  //   var returnMs: any; returnMs = {success: true, message: "success !!", data: {}};
  //   CLUSTER_SKIP = [];
  //   return returnMs;
  // }

}
