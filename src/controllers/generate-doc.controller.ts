// Uncomment these imports to begin using these cool features

// import angkaTerbilang from '@develoka/angka-terbilang-js';
// import {inject} from '@loopback/core';
import {authenticate} from '@loopback/authentication';
import {inject} from '@loopback/core';
import {repository} from '@loopback/repository';
import {get, post, requestBody} from '@loopback/rest';
import {SecurityBindings, securityId, UserProfile} from '@loopback/security';
import ejs from "ejs";
import fs from 'fs';
import pdf from "html-pdf";
import path from 'path';
import PDFMerger from 'pdf-merger-js';
import randomstring from 'randomstring';
import {DataDigipos} from '../models';
import {AdClusterRepository, AdMasterRepository, ApprovalNotaRepository, DataconfigRepository, DataDigiposDetailRepository, DataDigiposRepository, DataDisputeDetailRepository, DataDisputeRepository, DataFpjpRepository, DataNotificationRepository, DataSumarryRepository, NotaDebetDetailRepository, NotaDebetRepository, UserRepository} from '../repositories';
import {DataDigiposAttentionRepository} from '../repositories/data-digipos-attention.repository';
import {ControllerConfig} from './controller.config';

export const GenerateFPJPByIdBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['fpjp_number'],
        properties: {
          fpjp_number: {
            type: 'string',
          },
          // nota_debet_id: {
          //   type: 'array',
          //   items: {type: 'number'},
          // }
          // last_number: {
          //   type: 'number',
          // },
          // periode: {
          //   type: 'string',
          // }
        }
      }
    },
  },
}


export const GenerateFPJPUpdatePPH = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['fpjp_id'],
        properties: {
          fpjp_id: {
            type: 'array',
            items: {type: 'number'},
          }
        }
      }
    },
  },
}


export const GenerateNoInvoiceBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['label', 'last_number', 'periode'],
        properties: {
          label: {
            type: 'string',
          },
          last_number: {
            type: 'number',
          },
          periode: {
            type: 'string',
          }
        }
      }
    },
  },
}

export const GenerateAutoNoBA = {
  description: 'The input of generate Auto BA function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['type_ad', 'periode', 'last_number', 'ba_label'],
        properties: {
          type_ad: {
            type: 'string'
          },
          periode: {
            type: 'string'
          },
          last_number: {
            type: 'number'
          },
          ba_label: {
            type: 'string'
          }
        }
      }
    },
  },
};


export const GenerateAutoFileBA = {
  description: 'The input of generate Auto File BA function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['periode'],
        properties: {
          periode: {
            type: 'string'
          },
        }
      }
    },
  },
};

export const GenerateFileBARequestBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['digipos'],
        properties: {
          type_ad: {
            type: 'string',
          },
          digipos: {
            type: 'array',
            items: {type: 'string'},
          }
        }
      }
    },
  },
};

export const GenerateFileInvoiceRequestBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['digipos'],
        properties: {
          digipos: {
            type: 'array',
            items: {type: 'string'},
          }
        }
      }
    },
  },
};

export const GenerateFileInvoiceID = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['periode'],
        properties: {
          periode: {
            type: 'string',
          }
        }
      }
    },
  },
}

var ProcessGenerate: any; ProcessGenerate = {};
var DigiposGenerateFile: any; DigiposGenerateFile = {};

var GenerateFileInvoice: any; GenerateFileInvoice = {};

var thisContent: any;

var ProcessGenerateDocBarA = 0;
var ProcessGenerateDocBarB = 0;


var ProcessGenerateDocBarASign = 0;
var ProcessGenerateDocBarBSign = 0;


var FpjpGenerateStore: any; FpjpGenerateStore = {};
var runFpjpGenerate = false;

@authenticate('jwt')
export class GenerateDocController {
  constructor(
    @repository(AdMasterRepository)
    public adMasterRepository: AdMasterRepository,
    @repository(DataDigiposRepository)
    public dataDigiposRepository: DataDigiposRepository,
    @repository(DataDigiposAttentionRepository)
    public dataDigiposAttentionRepository: DataDigiposAttentionRepository,
    @repository(DataDigiposDetailRepository)
    public dataDigiposDetailRepository: DataDigiposDetailRepository,
    @repository(AdClusterRepository)
    public adClusterRepository: AdClusterRepository,
    @repository(DataSumarryRepository)
    public dataSumarryRepository: DataSumarryRepository,
    @repository(DataconfigRepository)
    public dataconfigRepository: DataconfigRepository,
    @repository(NotaDebetRepository)
    public notaDebetRepository: NotaDebetRepository,
    @repository(NotaDebetDetailRepository)
    public notaDebetDetailRepository: NotaDebetDetailRepository,
    @repository(ApprovalNotaRepository)
    public approvalNotaRepository: ApprovalNotaRepository,
    @repository(DataDisputeRepository)
    public dataDisputeRepository: DataDisputeRepository,
    @repository(DataDisputeDetailRepository)
    public dataDisputeDetailRepository: DataDisputeDetailRepository,
    @repository(DataFpjpRepository)
    public dataFpjpRepository: DataFpjpRepository,
    @repository(UserRepository)
    public userRepository: UserRepository,
    @inject(SecurityBindings.USER)
    public currentUserProfile: UserProfile,
    @repository(DataNotificationRepository)
    public dataNotificationRepository: DataNotificationRepository,

  ) {
    thisContent = this;
  }

  onReGenerateFileFpjp = async () => {

    var UserID = this.currentUserProfile[securityId];
    for (var i in FpjpGenerateStore) {
      console.log(i, "RUNNING....");
      var indexFile = i;

      var FpjpInfo = FpjpGenerateStore[i];
      var DataFPJP: any; DataFPJP = {
        FPJPNumber: "",
        FPJPType: "",
        Tanngal: "",
        NamaPembuat: "",
        NIKPembuat: "",
        Jabatan: "",
        Divisi: "",
        DataPengeluaran: [],
        TotalAmount: 0,
        JmlTerbilang: "",
        PTName: "",
        NameRekening: "",
        NoRek: "",
        NamaBank: "",
        Diketahui: []
      };

      if (FpjpInfo.transaction_code != null && FpjpInfo.transaction_code != undefined) {
        var typeFPJP = ["", "Invoice", "Note Debit"];
        DataFPJP.FPJPNumber = FpjpInfo.transaction_code;
        DataFPJP.Tanngal = ControllerConfig.onBANowDate()//this.onBANowDate();
        var fpjpPeriode = FpjpInfo.periode.split("-");
        var Periode = ControllerConfig.onGetMonth((parseInt(fpjpPeriode[1]) - 1)) + " " + fpjpPeriode[0];
        var InfoUser = await this.userRepository.findById(FpjpInfo.by_submit_fpjp);
        var InfoAD = await this.adMasterRepository.findById(FpjpInfo.ad_code);
        DataFPJP.PTName = InfoAD.ad_name;
        DataFPJP.NoRek = InfoAD.no_rek;
        DataFPJP.NamaBank = InfoAD.name_bank;
        DataFPJP.NameRekening = InfoAD.name_rekening;

        DataFPJP.NamaPembuat = InfoUser.fullname;
        DataFPJP.NIKPembuat = InfoUser.nik;
        DataFPJP.Jabatan = InfoUser.jabatan;
        DataFPJP.Divisi = InfoUser.divisi;

        DataFPJP.Diketahui.push({Jabatan: InfoUser.jabatan, Name: InfoUser.fullname});

        var DataConfig = await this.dataconfigRepository.find({where: {config_group_id: 4}});
        var InfoPengeluaran: any; InfoPengeluaran = {};
        var PPNFpjp = 0;
        var TotalFpjp = 0;
        for (var i in DataConfig) {
          var infoConfig = DataConfig[i];
          if (FpjpInfo.type_trx == 1) {
            if (infoConfig.config_code == "0004-01") {
              TotalFpjp = FpjpInfo.price;
              InfoPengeluaran[infoConfig.config_code] = {No: 1, Keterangan: infoConfig.config_desc + " " + Periode, KodeAccount: infoConfig.label_code, Jumlah: FpjpInfo.price};
            }
            if (infoConfig.config_code == "0004-02") {
              InfoPengeluaran[infoConfig.config_code] = {No: 2, Keterangan: infoConfig.config_desc, KodeAccount: infoConfig.label_code, Jumlah: 0};
            }
            if (infoConfig.config_code == "0004-03") {
              PPNFpjp = infoConfig.config_value;
            }
            if (infoConfig.config_code == "0004-04") {
              DataFPJP.FPJPType = infoConfig.config_desc;
            }
          }

          if (FpjpInfo.type_trx == 2) {
            if (infoConfig.config_code == "0004-06") {
              DataFPJP.FPJPType = infoConfig.config_desc;
            }

            if (infoConfig.config_code == "0004-05") {
              TotalFpjp = FpjpInfo.price;
              InfoPengeluaran[infoConfig.config_code] = {No: 1, Keterangan: infoConfig.config_desc + " " + Periode, KodeAccount: infoConfig.label_code, Jumlah: FpjpInfo.price};
            }
          }
        }

        if (FpjpInfo.type_trx == 1) {
          // var JmlPPN: number; JmlPPN = InfoPengeluaran["0004-01"].Jumlah * PPNFpjp;
          var JmlPPN = FpjpInfo.pph_amount;
          InfoPengeluaran["0004-02"].Keterangan = InfoPengeluaran["0004-02"].Keterangan + " " + (PPNFpjp * 100).toString() + "%"; //= {No: 1, Keterangan: infoConfig.config_value, Jumlah: 0};
          InfoPengeluaran["0004-02"].Jumlah = ControllerConfig.onGeneratePuluhan(parseFloat(JmlPPN.toFixed(0)));
          InfoPengeluaran["0004-01"].Jumlah = ControllerConfig.onGeneratePuluhan(parseFloat(InfoPengeluaran["0004-01"].Jumlah.toFixed(0)));

          DataFPJP.DataPengeluaran.push(InfoPengeluaran["0004-01"]);
          DataFPJP.DataPengeluaran.push(InfoPengeluaran["0004-02"]);
        }


        if (FpjpInfo.type_trx == 2) {
          InfoPengeluaran["0004-05"].Jumlah = ControllerConfig.onGeneratePuluhan(Number(InfoPengeluaran["0004-05"].Jumlah.toFixed(0)));
          DataFPJP.DataPengeluaran.push(InfoPengeluaran["0004-05"]);
        }


        var TotalAmount = FpjpInfo.type_trx == 1 ? (TotalFpjp + FpjpInfo.pph_amount) : TotalFpjp;
        // var TotalFixed = Math.ceil(Number(TotalAmount));
        // if (TotalFixed != Number(TotalAmount)) {
        //   if (TotalFixed == FpjpInfo.total_amount) {
        //     TotalAmount = TotalFixed;
        //   } else {
        //     TotalAmount = TotalFixed - 1;
        //   }
        // }

        DataFPJP.TotalAmount = ControllerConfig.onGeneratePuluhan(parseFloat(TotalAmount.toFixed(0)));
        DataFPJP.JmlTerbilang = ControllerConfig.onAngkaTerbilang(Number(TotalAmount.toFixed(0))) + " rupiah";

        var InfoApproval = await this.approvalNotaRepository.find({where: {nota_debet_id: FpjpInfo.id}});
        var UserApprove: any; UserApprove = [];
        for (var iUser in InfoApproval) {
          var users = InfoApproval[iUser].user_id;
          UserApprove.push(users);
          var ListApproval = await this.userRepository.findById(users);
          DataFPJP.Diketahui.push({Jabatan: ListApproval.jabatan, Name: ListApproval.fullname});
        }


        const headerImagePath = path.join(__dirname, '../../.digipost/template_ba/inv_header.png');
        const headerImageContents = fs.readFileSync(headerImagePath, {encoding: 'base64'});
        DataFPJP.headerImg = headerImageContents;

        var refToken = randomstring.generate() + ControllerConfig.onCurrentTime().toString();
        await this.dataFpjpRepository.updateById(FpjpInfo.id, {
          doc_ref: refToken
        });
        DataFPJP.refToken = refToken;

        var NameFileFPJP = FpjpInfo.transaction_code + ".pdf";
        var pathFile = path.join(__dirname, '../../.digipost/fpjp/') + NameFileFPJP;
        //MAKE FILE PDF BA
        var compiled = ejs.compile(fs.readFileSync(path.join(__dirname, '../../.digipost/template_ba/') + 'pdf_fpjp.html', 'utf8'));
        var html = compiled(DataFPJP);


        var config: any; config = {
          orientation: "portrait",
          "header": {
            "height": "80px",
            "contents": ""
          },
          "footer": {
            "height": "70px",
            "contents": ''
          }
        }
        pdf.create(html, config).toFile(pathFile, async (err: any, res: any) => {
          if (err) {
            console.log(err);
            await this.dataFpjpRepository.updateById(FpjpInfo.id, {
              fpjp_generate: 3,
              fpjp_generate_err: JSON.stringify(err)
            });
          } else {
            await this.dataFpjpRepository.updateById(FpjpInfo.id, {
              fpjp_generate: 2,
              fpjp_attach: NameFileFPJP,
              fpjp_generate_err: "",
              at_fpjp_generate: ControllerConfig.onCurrentTime().toString()
            })
          }
        });
      }

      delete FpjpGenerateStore[indexFile];
      console.log(FpjpGenerateStore);
      this.onReGenerateFileFpjp();
      break;
    }
    var no = 0;
    for (var i in FpjpGenerateStore) {
      no++;
    }
    if (no == 0) {
      runFpjpGenerate = false;
    }
  }


  onGetDayString = (posisi: number) => {
    var DayString = ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"];
    return DayString[posisi];
  }

  onGetMonth = (posisi: number) => {
    var MonthString = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
    return MonthString[posisi];
  }

  onGetNowDay = () => {
    var d = new Date();
    return d.getDay();
  }

  onGetNowMonth = () => {
    var d = new Date();
    return d.getMonth();
  }

  onGetNowYears = () => {
    var d = new Date();
    return d.getFullYear();
  }

  onBANowDate = () => {
    var d = new Date();
    return this.onGetDayString(d.getDay()) + ", " + d.getDate() + " " + this.onGetMonth(d.getMonth()) + " " + d.getFullYear();
  }




  // ==============================================GENERATE PDF INVOICE NUMBER=============================
  onGenerateInvoicePDF = async () => {
    for (var i in GenerateFileInvoice) {
      var KeyIndex = i;
      var InfoInvoice = GenerateFileInvoice[i]
      if (InfoInvoice.inv_number == null || InfoInvoice.inv_number == "") {
      } else {
        var NameFileInvoice = InfoInvoice.inv_number.split(".").join("_") + "_" + InfoInvoice.trx_date_group + "_" + InfoInvoice.ad_code + ".pdf";
        var pathFile = path.join(__dirname, '../../.digipost/invoice/') + NameFileInvoice;

        var DataInvoice_PDF: any;
        DataInvoice_PDF = {
          headerImg: "",
          footerImg: "",
          PihakICompany: "",
          PihakIAddress: "",
          PihakIName: "",
          PihakIPosition: "",
          PihakIEmail: "",
          PihakINPWP: "",

          PihakIICompany: "",
          PihakIIAddress: "",
          PihakIINPWP: "",
          PihakIIName: "",
          PihakIIAcountNumber: "",
          PihakIIBank: "",
          PihakIIBranch: "",

          InvoiceDate: "",
          InvoiceNumber: "",
          DataInvoice: [],//[{description : "" , IDRAmount : 0}]
          TotalInvoiceAmount: 0
        }

        var infoPihakLinkAja = await this.dataDigiposAttentionRepository.findOne({where: {and: [{set_attention: true}, {group: 2}]}});
        var infoPihakAD = await this.adMasterRepository.findOne({where: {ad_code: InfoInvoice.ad_code}});
        DataInvoice_PDF.PihakICompany = infoPihakAD?.ad_name;
        DataInvoice_PDF.PihakIAddress = infoPihakAD?.npwp_address;
        DataInvoice_PDF.PihakIANpwp = infoPihakAD?.npwp;
        DataInvoice_PDF.PihakIName = infoPihakLinkAja?.attention_name;
        DataInvoice_PDF.PihakIPosition = infoPihakLinkAja?.attention_position;
        DataInvoice_PDF.PihakIEmail = infoPihakLinkAja?.email;
        DataInvoice_PDF.PihakINPWP = infoPihakAD?.npwp;

        DataInvoice_PDF.PihakIICompany = infoPihakLinkAja?.company;
        DataInvoice_PDF.PihakIIAddress = infoPihakLinkAja?.address;
        DataInvoice_PDF.PihakIINPWP = infoPihakLinkAja?.npwp;
        DataInvoice_PDF.PihakIIName = infoPihakLinkAja?.attention_name;
        DataInvoice_PDF.PihakIIAcountNumber = infoPihakLinkAja?.account_number;
        DataInvoice_PDF.PihakIIBank = infoPihakLinkAja?.bank_name;
        DataInvoice_PDF.PihakIIBranch = infoPihakLinkAja?.brach;


        var PeriodeTrx = InfoInvoice.trx_date_group.split("-");

        var d = new Date(InfoInvoice.trx_date_group + "-" + "26");
        var dateLast = new Date(PeriodeTrx[0], PeriodeTrx[1], 0);
        var dateInvoice = dateLast.getDate() + " " + ControllerConfig.onGetMonth(dateLast.getMonth()) + " " + dateLast.getFullYear();

        DataInvoice_PDF.InvoiceDate = dateInvoice;
        DataInvoice_PDF.InvoiceNumber = InfoInvoice.inv_number;


        var dispute = await this.dataDisputeRepository.find({where: {and: [{periode: InfoInvoice?.trx_date_group}, {dispute_status: 2}, {ad_code: InfoInvoice?.ad_code}, {type_trx: 2}]}});
        var DppDispute = 0;
        var PpnDispute = 0;
        var TotDispute = 0;
        for (var iDispute in dispute) {
          var infoDispute = dispute[iDispute];
          DppDispute = DppDispute + (infoDispute.dpp_amount == undefined ? 0 : infoDispute.dpp_amount);
          PpnDispute = PpnDispute + (infoDispute.ppn_amount == undefined ? 0 : infoDispute.ppn_amount);
          TotDispute = TotDispute + (infoDispute.tot_mdr == undefined ? 0 : infoDispute.tot_mdr);
        }


        var DPP_ROUND = (InfoInvoice.dpp_paid_amount + DppDispute);
        var VAT_ROUND = (InfoInvoice.vat_paid_amount + PpnDispute);

        var THIS_VAT = Math.floor(VAT_ROUND);
        if (Number(THIS_VAT) != Number(VAT_ROUND.toFixed(0))) {

        }


        DataInvoice_PDF.DataInvoice.push({
          description: "Sales Fee " + ControllerConfig.onGetMonth(d.getMonth()) + " " + PeriodeTrx[0],
          IDRAmount: ControllerConfig.onGeneratePuluhan((InfoInvoice.dpp_paid_amount + DppDispute).toFixed(0))
        });

        DataInvoice_PDF.DataInvoice.push({
          description: "Sub Total",
          IDRAmount: ControllerConfig.onGeneratePuluhan((InfoInvoice.dpp_paid_amount + DppDispute).toFixed(0))
        });

        DataInvoice_PDF.DataInvoice.push({
          description: "VAT Out 10% : Sales Fee " + ControllerConfig.onGetMonth(d.getMonth()) + " " + PeriodeTrx[0],
          IDRAmount: ControllerConfig.onGeneratePuluhan((InfoInvoice.vat_paid_amount + PpnDispute).toFixed(0))
        });
        DataInvoice_PDF.TotalInvoiceAmount = ControllerConfig.onGeneratePuluhan((InfoInvoice.total_paid_amount + TotDispute).toFixed(0));

        DataInvoice_PDF.TotalInvoiceTerbilang = ControllerConfig.onAngkaTerbilang((InfoInvoice.total_paid_amount + TotDispute).toFixed(0));

        const headerImagePath = path.join(__dirname, '../../.digipost/template_ba/inv_header.png');
        const headerImageContents = fs.readFileSync(headerImagePath, {encoding: 'base64'});
        DataInvoice_PDF.headerImg = headerImageContents;

        const footerImagePath = path.join(__dirname, '../../.digipost/template_ba/inv_footer.png');
        const footerImageContents = fs.readFileSync(footerImagePath, {encoding: 'base64'});
        DataInvoice_PDF.footerImg = footerImageContents;

        // var html = '<img src="data:image/png;base64,' + headerImageContents + '"   width="100" height="100" />';
        // var imgSrc = path.normalize(headerImagePath);
        // console.log(headerImageContents);
        //MAKE FILE PDF BA
        var compiled = ejs.compile(fs.readFileSync(path.join(__dirname, '../../.digipost/template_ba/') + 'pdf_invoice.html', 'utf8'));
        var html = compiled(DataInvoice_PDF);
        var config: any; config = {
          orientation: "portrait",
          "header": {
            "height": "80px",
            "contents": ""
          },
          "footer": {
            "height": "70px",
            "contents": ''
          }
        }
        pdf.create(html, config).toFile(pathFile, async (err: any, res: any) => {
          if (err) {
            delete GenerateFileInvoice[i];
            return console.log(err);
          }

          await this.dataDigiposRepository.updateById(InfoInvoice.digipost_id, {
            inv_attacment: NameFileInvoice,
            generate_file_invoice: 1
          });

          var noInvP = 0;
          for (var pInv in GenerateFileInvoice) {noInvP++;};
          if (noInvP == 1) {
            var generateInv = await this.dataDigiposRepository.count({and: [{generate_file_invoice: 1}, {trx_date_group: InfoInvoice.trx_date_group}]});
            var generateNotInv = await this.dataDigiposRepository.count({and: [{generate_file_invoice: 0}, {trx_date_group: InfoInvoice.trx_date_group}]});

            var userId = this.currentUserProfile[securityId];
            var roleNotifSetADB = ControllerConfig.onGetRoleNotifGroup("16010103");
            var infoUser = await this.userRepository.findById(userId)
            await this.dataNotificationRepository.create({
              types: "NOTIFICATION",
              target: roleNotifSetADB.role,
              to: "",
              from_who: infoUser.username,
              subject: roleNotifSetADB.label,
              body: "Document Invoice berhasil digenerate, Jumlah yang sudah digenerate : " + generateInv.count.toString() + ", Jumlah yang belum digenerate : " + generateNotInv.count.toString(),
              path_url: "",
              app: "DIGIPOS",
              code: "16010103",
              code_label: "",
              at_create: ControllerConfig.onCurrentTime().toString(),
              at_read: "",
              periode: InfoInvoice.trx_date_group,
              at_flag: 0
            });

            ControllerConfig.onSendNotif({
              types: "NOTIFICATION",
              target: roleNotifSetADB.role,
              to: "",
              from_who: infoUser.username,
              subject: roleNotifSetADB.label,
              body: "Document Invoice berhasil digenerate, Jumlah yang sudah digenerate : " + generateInv.count.toString() + ", Jumlah yang belum digenerate : " + generateNotInv.count.toString(),
              path_url: "",
              app: "DIGIPOS",
              code: "16010103",
              code_label: "",
              at_create: ControllerConfig.onCurrentTime().toString(),
              at_read: "",
              periode: InfoInvoice.trx_date_group,
              at_flag: 0
            });
          }

          delete GenerateFileInvoice[KeyIndex];
          this.onGenerateInvoicePDF();

        });

      }
      break;
    }

  }

  // ==============================================GENERATE PDF BERITA ACARA=============================
  onGenerateBASummaryPDF = async (digipos: any, callback: any) => {

    var digiposCluster = await this.adClusterRepository.find({where: {ad_code: digipos.ad_code}});
    var IdCluster = "";
    for (var j in digiposCluster) {
      var cluster = digiposCluster[j];
      IdCluster += ",'" + cluster.ad_cluster_id + "'";
    }
    var datePeriode = digipos.periode.split("-");
    var Periode = digipos.periode;
    var Tahun = datePeriode[0];
    var Bulan = datePeriode[1];
    var sql = "SELECT " +
      "DISTINCT(DataSumarry.id), " +
      "AdCluster.ad_cluster_id, " +
      "AdCluster.ad_code, " +
      "DataSumarry.dealer_code, " +
      "DataSumarry.location, " +
      "DataSumarry.type_trx, " +
      "DataSumarry.name, " +
      "IF(DataSumarry.paid_in_amount = 0, DataSumarry.withdraw_amount , DataSumarry.paid_in_amount) as group_amount, " +
      "sum(DataSumarry.trx_a) as trx_a, " +
      "sum(DataSumarry.tot_mdr_a) as tot_mdr_a, " +
      "sum(DataSumarry.trx_b) as trx_b, " +
      "sum(DataSumarry.tot_mdr_b) as  tot_mdr_b, " +
      "DataSumarry.status, " +
      "DataSumarry.periode, " +
      "DataSumarry.create_at " +
      "FROM " +
      "DataSumarry " +
      "INNER JOIN " +
      "AdCluster " +
      "on " +
      "DataSumarry.dealer_code = AdCluster.ad_cluster_id " +
      "WHERE " +
      "DataSumarry.periode = '" + Periode + "'  AND DataSumarry.dealer_code in (" + IdCluster.substring(1) + ") GROUP BY AdCluster.ad_cluster_id,group_amount,type_trx"

    var dataSummary = await this.dataSumarryRepository.execute(sql);

    var DataBA_PDF: any;

    DataBA_PDF = {
      Periode: this.onGetMonth(parseInt(Bulan)) + " " + Tahun,
      DataTransaction: [],
      // total_trx_fee: 0,
      // total_total_mdr_fee: 0,
      // total_dpp_fee: 0,
      // total_ppn_fee: 0,
      // total_pph_fee: 0,
      // total_trx_paid: 0,
      // total_total_mdr_paid: 0,
      // total_dpp_paid: 0,
      // total_ppn_paid: 0,
      // total_pph_paid: 0
      total_trx: 0,
      total_total_mdr: 0,
      total_dpp: 0,
      total_ppn: 0,
      total_pph: 0
    }

    if (digipos.type_ad == "AD_A") {
      DataBA_PDF.Periode = "SUMMARY AD-A PERIODE " + ControllerConfig.onGetMonth((parseInt(Bulan) - 1)) + " " + Tahun
    }

    if (digipos.type_ad == "AD_B") {
      DataBA_PDF.Periode = "SUMMARY AD-B PERIODE " + ControllerConfig.onGetMonth((parseInt(Bulan) - 1)) + " " + Tahun
    }

    const configData = await this.dataconfigRepository.find({where: {config_group_id: 1}});
    var configNum: any;
    configNum = {};
    for (var i in configData) {
      var infoConfig = configData[i];
      if (infoConfig.config_code == "0001-01") {
        configNum["persen_fee"] = infoConfig.config_value;
      }
      if (infoConfig.config_code == "0001-02") {
        configNum["persen_paid"] = infoConfig.config_value;
      }

      if (infoConfig.config_code == "0001-03") {
        configNum["num_dpp_fee"] = infoConfig.config_value;
      }
      if (infoConfig.config_code == "0001-04") {
        configNum["num_ppn_fee"] = infoConfig.config_value;
      }
      if (infoConfig.config_code == "0001-05") {
        configNum["num_pph_fee"] = infoConfig.config_value;
      }

      if (infoConfig.config_code == "0001-11") {
        configNum["num_dpp_paid"] = infoConfig.config_value;
      }
      if (infoConfig.config_code == "0001-12") {
        configNum["num_ppn_paid"] = infoConfig.config_value;
      }
      if (infoConfig.config_code == "0001-13") {
        configNum["num_pph_paid"] = infoConfig.config_value;
      }

      if (infoConfig.config_code == "0001-06") {
        configNum["persen_fee_ppob"] = infoConfig.config_value;
      }
      if (infoConfig.config_code == "0001-07") {
        configNum["persen_paid_ppob"] = infoConfig.config_value;
      }
      if (infoConfig.config_code == "0001-08") {
        configNum["num_dpp_ppob"] = infoConfig.config_value;
      }
      if (infoConfig.config_code == "0001-09") {
        configNum["num_ppn_ppob"] = infoConfig.config_value;
      }
      if (infoConfig.config_code == "0001-10") {
        configNum["num_pph_ppob"] = infoConfig.config_value;
      }

    }


    for (var iSM in dataSummary) {


      var persenFee = configNum.persen_fee;
      var persenPaid = configNum.persen_paid;

      var dppSetFee = configNum.num_dpp_fee;
      var ppnSetFee = configNum.num_ppn_fee;
      var pphSetFee = configNum.num_pph_fee;

      var dppSetPaid = configNum.num_dpp_paid;
      var ppnSetPaid = configNum.num_ppn_paid;
      var pphSetPaid = configNum.num_pph_paid;


      var infoSum = dataSummary[iSM];


      if (infoSum.type_trx.toUpperCase() == "PPOB") {

        var persenFee = configNum.persen_fee_ppob;
        var persenPaid = configNum.persen_paid_ppob;
        dppSetFee = configNum.num_dpp_ppob;
        ppnSetFee = configNum.num_ppn_ppob;
        pphSetFee = configNum.num_pph_ppob;
      }


      var trxFee = ((infoSum.trx_a * infoSum.group_amount) * (persenFee));
      var feeDPP = (trxFee / dppSetFee);
      var feePPN = (feeDPP * ppnSetFee);
      var feeTotal = feeDPP + feePPN;
      var feePPH = (feeDPP * pphSetFee);

      var trxPaid = ((infoSum.trx_b * infoSum.group_amount) * (persenPaid));
      var paidDPP = (trxPaid / dppSetPaid);
      var paidPPN = (paidDPP * ppnSetPaid);
      var paidTotal = paidDPP + paidPPN;
      var paidPPH = (paidDPP * pphSetPaid);

      var dateSplit = infoSum.periode.split("-");
      var TahunSM = dateSplit[0];
      var BulanSM = dateSplit[1];
      var DataSM: any; DataSM = {
        dealer_code: infoSum.dealer_code,
        dealer_name: infoSum.name,
        bulan: BulanSM,
        tahun: TahunSM,
        fee: ControllerConfig.onGeneratePuluhan(infoSum.group_amount),
        type: "",
        // trx_fee: infoSum.trx_a,
        // total_mdr_fee: trxFee,
        // dpp_fee: feeDPP,
        // ppn_fee: feePPN,
        // pph_fee: feePPH,

        // trx_paid: infoSum.trx_b,
        // total_mdr_paid: trxPaid,
        // dpp_paid: paidDPP,
        // ppn_paid: paidPPN,
        // pph_paid: paidPPH
        trx: 0,
        total_mdr: 0,
        dpp: 0,
        ppn: 0,
        pph: 0
      }

      if (digipos.type_ad == "AD_A") {

        if (infoSum.trx_a == 0) {
          continue;
        }

        DataSM.trx = ControllerConfig.onGeneratePuluhan(infoSum.trx_a);
        DataSM.total_mdr = ControllerConfig.onGeneratePuluhan(trxFee);
        DataSM.dpp = ControllerConfig.onGeneratePuluhan(parseFloat(feeDPP.toFixed(2)));
        DataSM.ppn = ControllerConfig.onGeneratePuluhan(parseFloat(feePPN.toFixed(2)));
        DataSM.pph = ControllerConfig.onGeneratePuluhan(parseFloat(feePPH.toFixed(2)));
        DataSM.type = infoSum.type_trx + " AD-A";

        DataBA_PDF.total_trx = DataBA_PDF.total_trx + infoSum.trx_a;
        DataBA_PDF.total_total_mdr = DataBA_PDF.total_total_mdr + trxFee;
        DataBA_PDF.total_dpp = DataBA_PDF.total_dpp + feeDPP;
        DataBA_PDF.total_ppn = DataBA_PDF.total_ppn + feePPN;
        DataBA_PDF.total_pph = DataBA_PDF.total_pph + feePPH;


      }

      if (digipos.type_ad == "AD_B") {

        if (infoSum.trx_b == 0) {
          continue;
        }

        DataSM.trx = ControllerConfig.onGeneratePuluhan(infoSum.trx_b);
        DataSM.total_mdr = ControllerConfig.onGeneratePuluhan(trxPaid);
        DataSM.dpp = ControllerConfig.onGeneratePuluhan(parseFloat(paidDPP.toFixed(2)));
        DataSM.ppn = ControllerConfig.onGeneratePuluhan(parseFloat(paidPPN.toFixed(2)));
        DataSM.pph = ControllerConfig.onGeneratePuluhan(parseFloat(paidPPH.toFixed(2)));
        DataSM.type = infoSum.type_trx + " AD-B";

        DataBA_PDF.total_trx = DataBA_PDF.total_trx + infoSum.trx_b;
        DataBA_PDF.total_total_mdr = DataBA_PDF.total_total_mdr + trxPaid;
        DataBA_PDF.total_dpp = DataBA_PDF.total_dpp + paidDPP;
        DataBA_PDF.total_ppn = DataBA_PDF.total_ppn + paidPPN;
        DataBA_PDF.total_pph = DataBA_PDF.total_pph + paidPPH;
      }
      DataBA_PDF.DataTransaction.push(DataSM);

      // DataBA_PDF.total_trx_fee = DataBA_PDF.total_trx_fee + infoSum.trx_a;
      // DataBA_PDF.total_total_mdr_fee = DataBA_PDF.total_total_mdr_fee + trxFee;
      // DataBA_PDF.total_dpp_fee = DataBA_PDF.total_dpp_fee + feeDPP;
      // DataBA_PDF.total_ppn_fee = DataBA_PDF.total_ppn_fee + feePPN;
      // DataBA_PDF.total_pph_fee = DataBA_PDF.total_pph_fee + feePPH;
      // DataBA_PDF.total_trx_paid = DataBA_PDF.total_trx_paid + infoSum.trx_b;
      // DataBA_PDF.total_total_mdr_paid = DataBA_PDF.total_total_mdr_paid + trxPaid;
      // DataBA_PDF.total_dpp_paid = DataBA_PDF.total_dpp_paid + paidDPP;
      // DataBA_PDF.total_ppn_paid = DataBA_PDF.total_ppn_paid + paidPPN;
      // DataBA_PDF.total_pph_paid = DataBA_PDF.total_pph_paid + paidPPH;
    }

    var todayDispute = new Date(digipos.periode);
    var todaysDispute = todayDispute.setMonth(todayDispute.getMonth() - 1);
    var DateNewDispute = new Date(todaysDispute);
    var PeriodeDispute = DateNewDispute.getFullYear() + '-' + ('0' + (DateNewDispute.getMonth())).slice(-2);


    var dispute = await this.dataDisputeRepository.find({where: {and: [{periode: PeriodeDispute}, {dispute_status: 2}, {ad_code: digipos.ad_code}]}});

    for (var iDispute in dispute) {
      var infoDispute = dispute[iDispute];
      var disputeID = infoDispute.dispute_id;
      var disputeDetail = await this.dataDisputeDetailRepository.find({where: {dispute_id: disputeID}});
      for (var iDisDet in disputeDetail) {
        var infoDisputeDetail = disputeDetail[iDisDet];
        // var todayDispute = new Date(digipos.periode);
        // var todaysDispute = todayDispute.setMonth(todayDispute.getMonth() - 1);
        // var DateNewDispute = new Date(todaysDispute);
        // var PeriodeDispute = DateNewDispute.getFullYear() + '-' + ('0' + (DateNewDispute.getMonth() + 1)).slice(-2);



        var dateDisputeSplit = PeriodeDispute.split("-");//infoDispute.periode.split("-");
        var TahunDisputeSM = dateDisputeSplit[0];
        var BulanDisputeSM = dateDisputeSplit[1];
        var DataDisputeSM: any; DataDisputeSM = {
          dealer_code: infoDispute.ad_code,
          dealer_name: "SELISIH - " + infoDispute.ad_name,
          bulan: BulanDisputeSM,
          tahun: TahunDisputeSM,
          type: "DISPUTE",
          fee: ControllerConfig.onGeneratePuluhan(infoDisputeDetail.sales_fee == undefined ? 0 : infoDisputeDetail.sales_fee),
          // trx_fee: infoSum.trx_a,
          // total_mdr_fee: trxFee,
          // dpp_fee: feeDPP,
          // ppn_fee: feePPN,
          // pph_fee: feePPH,

          // trx_paid: infoSum.trx_b,
          // total_mdr_paid: trxPaid,
          // dpp_paid: paidDPP,
          // ppn_paid: paidPPN,
          // pph_paid: paidPPH
          trx: 0,
          total_mdr: 0,
          dpp: 0,
          ppn: 0,
          pph: 0
        }

        // if (digipos.type_ad == "AD_A") {
        DataDisputeSM.trx = ControllerConfig.onGeneratePuluhan(infoDisputeDetail.qty_trx == undefined ? 0 : infoDisputeDetail.qty_trx);
        DataDisputeSM.total_mdr = ControllerConfig.onGeneratePuluhan(infoDisputeDetail.tot_mdr == undefined ? 0 : infoDisputeDetail.tot_mdr);
        DataDisputeSM.dpp = ControllerConfig.onGeneratePuluhan(infoDisputeDetail.dpp_amount == undefined ? 0 : infoDisputeDetail.dpp_amount);
        DataDisputeSM.ppn = ControllerConfig.onGeneratePuluhan(infoDisputeDetail.ppn_amount == undefined ? 0 : infoDisputeDetail.ppn_amount);
        DataDisputeSM.pph = ControllerConfig.onGeneratePuluhan(infoDisputeDetail.pph_amount == undefined ? 0 : infoDisputeDetail.pph_amount);

        DataDisputeSM.total_trx = DataBA_PDF.total_trx + infoDisputeDetail.qty_trx;
        DataDisputeSM.total_total_mdr = DataBA_PDF.total_total_mdr + infoDisputeDetail.tot_mdr;
        DataDisputeSM.total_dpp = DataBA_PDF.total_dpp + infoDisputeDetail.dpp_amount;
        DataDisputeSM.total_ppn = DataBA_PDF.total_ppn + infoDisputeDetail.ppn_amount;
        DataDisputeSM.total_pph = DataBA_PDF.total_pph + infoDisputeDetail.pph_amount;
        // }

        // if (digipos.type_ad == "AD_B") {
        //   DataDisputeSM.trx = ControllerConfig.onGeneratePuluhan(infoDisputeDetail.trx_b);
        //   DataDisputeSM.total_mdr = ControllerConfig.onGeneratePuluhan(infoDisputeDetail.tot_mdr_b);
        //   DataDisputeSM.dpp = ControllerConfig.onGeneratePuluhan(parseFloat(infoDisputeDetail.dpp_paid_amount.toFixed(2)));
        //   DataDisputeSM.ppn = ControllerConfig.onGeneratePuluhan(parseFloat(infoDisputeDetail.ppn_paid_amount.toFixed(2)));
        //   DataDisputeSM.pph = ControllerConfig.onGeneratePuluhan(parseFloat(infoDisputeDetail.pph_paid_amount.toFixed(2)));

        //   DataBA_PDF.total_trx = DataBA_PDF.total_trx + infoDisputeDetail.trx_b;
        //   DataBA_PDF.total_total_mdr = DataBA_PDF.total_total_mdr + infoDisputeDetail.tot_mdr_b;
        //   DataBA_PDF.total_dpp = DataBA_PDF.total_dpp + infoDisputeDetail.dpp_paid_amount;
        //   DataBA_PDF.total_ppn = DataBA_PDF.total_ppn + infoDisputeDetail.ppn_paid_amount;
        //   DataBA_PDF.total_pph = DataBA_PDF.total_pph + infoDisputeDetail.pph_paid_amount;
        // }
        DataBA_PDF.DataTransaction.push(DataDisputeSM);
      }
    }


    DataBA_PDF.total_trx = ControllerConfig.onGeneratePuluhan(parseFloat(DataBA_PDF.total_trx.toFixed(2)));
    DataBA_PDF.total_total_mdr = ControllerConfig.onGeneratePuluhan(parseFloat(DataBA_PDF.total_total_mdr.toFixed(2)));
    DataBA_PDF.total_dpp = ControllerConfig.onGeneratePuluhan(parseFloat(DataBA_PDF.total_dpp.toFixed(2)));
    DataBA_PDF.total_ppn = ControllerConfig.onGeneratePuluhan(parseFloat(DataBA_PDF.total_ppn.toFixed(2)));
    DataBA_PDF.total_pph = ControllerConfig.onGeneratePuluhan(parseFloat(DataBA_PDF.total_pph.toFixed(2)));

    //DataBA_PDF.Periode = ControllerConfig.onGetMonth(todayDispute.getMonth() + 1) + " " + todayDispute.getFullYear();

    var NameFileBA = digipos.number_ba.split("/").join("_") + "_" + digipos.periode + "_" + digipos.ad_code + ".pdf";
    var pathFile = "";
    if (digipos.type_ad == "AD_A") {
      pathFile = path.join(__dirname, '../../.digipost/berita_acara/ad_a/summary_') + NameFileBA;
    }
    if (digipos.type_ad == "AD_B") {
      pathFile = path.join(__dirname, '../../.digipost/berita_acara/ad_b/summary_') + NameFileBA;
    }

    //MAKE FILE PDF BA
    var compiled = ejs.compile(fs.readFileSync(path.join(__dirname, '../../.digipost/template_ba/') + 'pdf_ba_summary.html', 'utf8'));
    var html = compiled(DataBA_PDF);

    var config: any; config = {
      format: "A4",
      orientation: "landscape",
      "header": {
        "height": "80px",
        "contents": ""
      },
      "footer": {
        "height": "80px",
        "contents": ''
      }
    }
    pdf.create(html, config).toFile(pathFile, async (err: any, res: any) => {
      if (err) {
        return console.log(err);
        callback(err);
      } else {
        callback({success: true, message: ""});
      }
    });

  }

  onGenerateBAPDF = async () => {
    for (var i in DigiposGenerateFile) {
      var KeyIndex = i;
      var InfoBA = DigiposGenerateFile[i];
      var digiposDetail = await this.dataDigiposDetailRepository.find({where: {digipost_id: InfoBA.digipost_id}});
      var digipos = await this.dataDigiposRepository.findOne({where: {digipost_id: InfoBA.digipost_id}});
      if (InfoBA.number_ba == null || InfoBA.number_ba == "") {

      } else {
        //SETTING NAME BA
        var infoPihakLinkAja = await this.dataDigiposAttentionRepository.findOne({where: {and: [{set_attention: true}, {group: 1}]}});
        var infoPihakAD = await this.adMasterRepository.findOne({where: {ad_code: InfoBA.ad_code}});

        var NameFileBA = InfoBA.number_ba.split("/").join("_") + "_" + InfoBA.periode + "_" + InfoBA.ad_code + ".pdf";
        var pathFile = "";
        var pathFile1 = "";
        var pathFile2 = "";
        const fileBlankPath = path.join(__dirname, '../../.digipost/template_ba/blank_signature.PNG');
        const contentsBlank = fs.readFileSync(fileBlankPath, {encoding: 'base64'});
        var SignImageA = contentsBlank;
        var SignImageB = contentsBlank;



        const filePath = path.join(__dirname, '../../.digipost/sign_atten/' + infoPihakLinkAja?.sign_attention);
        const contents = fs.readFileSync(filePath, {encoding: 'base64'});

        if (InfoBA.type_ad == "AD_A") {
          pathFile = path.join(__dirname, '../../.digipost/berita_acara/ad_a/') + NameFileBA;
          pathFile1 = path.join(__dirname, '../../.digipost/berita_acara/ad_a/1') + NameFileBA;
          pathFile2 = path.join(__dirname, '../../.digipost/berita_acara/ad_a/2') + NameFileBA;

          if (digipos?.sign_off_a == 1) {
            SignImageA = contents;
            console.log("SIGN A ON");
            //"http://45.32.39.203:3000/files/sign/" + infoPihakLinkAja?.sign_attention;
          }
        }

        if (InfoBA.type_ad == "AD_B") {
          pathFile = path.join(__dirname, '../../.digipost/berita_acara/ad_b/') + NameFileBA;
          pathFile1 = path.join(__dirname, '../../.digipost/berita_acara/ad_b/1') + NameFileBA;
          pathFile2 = path.join(__dirname, '../../.digipost/berita_acara/ad_b/2') + NameFileBA;
          if (digipos?.sign_off_b == 1) {
            SignImageB = contents;
            console.log("SIGN B ON");
            //"http://45.32.39.203:3000/files/sign/" + infoPihakLinkAja?.sign_attention;
          }
        }

        //INFORMATION PDF FILE UNTUK BERITA ACARA
        var DataBA_PDF: any;
        DataBA_PDF = {
          TypeAD: InfoBA.type_ad == "AD_A" ? "invoice" : "paid  invoice",
          NoBA: "",
          DateBACreate: "",
          //PIHAK 1
          PihakICompany: "",
          PihakIName: "",
          PihakIPosition: "",
          PihakIAddress: "",
          PihakISign: SignImageA,

          //PIHAK 2
          PihakIICompany: "",
          PihakIIName: "",
          PihakIIPosition: "",
          PihakIIAddress: "",
          PihakIISign: SignImageB,


          //DESCRIPTION
          NoContract: "",
          NominalString: "",

          //TABLE
          TotalTrxCount: 0,
          TotalTrxAmount: 0,
          DataTransaction: {},
          ketStore: [],
          TitleShare: ""
        }

        DataBA_PDF.TitleShare = InfoBA.type_ad == "AD_A" ? "Mitra SBP" : "LinkAja";
        DataBA_PDF.NoBA = InfoBA.number_ba;

        DataBA_PDF.PihakIName = InfoBA.type_ad == "AD_A" ? infoPihakLinkAja?.attention_name : infoPihakAD?.attention;
        DataBA_PDF.PihakIPosition = InfoBA.type_ad == "AD_A" ? infoPihakLinkAja?.attention_position : infoPihakAD?.attention_position;
        DataBA_PDF.PihakIAddress = InfoBA.type_ad == "AD_A" ? infoPihakLinkAja?.address : infoPihakAD?.npwp_address;
        DataBA_PDF.PihakICompany = InfoBA.type_ad == "AD_A" ? infoPihakLinkAja?.company : infoPihakAD?.ad_name;

        DataBA_PDF.PihakIIName = InfoBA.type_ad == "AD_A" ? infoPihakAD?.attention : infoPihakLinkAja?.attention_name;
        DataBA_PDF.PihakIIPosition = InfoBA.type_ad == "AD_A" ? infoPihakAD?.attention_position : infoPihakLinkAja?.attention_position;
        DataBA_PDF.PihakIIAddress = InfoBA.type_ad == "AD_A" ? infoPihakAD?.npwp_address : infoPihakLinkAja?.address;
        DataBA_PDF.PihakIICompany = InfoBA.type_ad == "AD_A" ? infoPihakAD?.ad_name : infoPihakLinkAja?.company;

        DataBA_PDF.NoContract = infoPihakAD?.contract_number;


        //GET DATA DIGIPOS BA
        var transaction: any; transaction = {FEE: {data: {}, TotalTrx: 0, TotalAmout: 0}, PAID: {data: {}, TotalTrx: 0, TotalAmout: 0}};


        var TotalTrxFee = 0;
        var TotalAmoutFee = 0;
        var TotalTrxPaid = 0;
        var TotalAmoutPaid = 0;




        const configData = await this.dataconfigRepository.find({where: {config_group_id: 1}});
        var configNum: any;
        configNum = {};
        for (var i in configData) {
          var infoConfig = configData[i];
          if (infoConfig.config_code == "0001-01") {
            configNum["persen_fee"] = infoConfig.config_value;
          }
          if (infoConfig.config_code == "0001-02") {
            configNum["persen_paid"] = infoConfig.config_value;
          }
          if (infoConfig.config_code == "0001-06") {
            configNum["persen_fee_ppob"] = infoConfig.config_value;
          }
          if (infoConfig.config_code == "0001-07") {
            configNum["persen_paid_ppob"] = infoConfig.config_value;
          }
        }

        var FeeKey: any; FeeKey = [];
        var PaidKey: any; PaidKey = [];
        for (var i in digiposDetail) {
          var TransactionBA = digiposDetail[i];
          DataBA_PDF.TitleDataRaw = TransactionBA.trx_date_group;
          DataBA_PDF.PeriodeData = TransactionBA.trx_date_group;
          var share_for_fee: string; share_for_fee = (configNum["persen_fee"] * 100) + "%";
          var share_for_paid: string; share_for_paid = (configNum["persen_paid"] * 100) + "%";
          if (TransactionBA.type_trx == "PPOB") {
            share_for_fee = (configNum["persen_fee_ppob"] * 100) + "%";
            share_for_paid = (configNum["persen_paid_ppob"] * 100) + "%";
          }

          var thisDateFee = new Date(TransactionBA.trx_date_group + "-01");
          var Mount = ControllerConfig.onGetMonth(thisDateFee.getMonth());
          var DescData = "Transaksi " + Mount + " " + thisDateFee.getFullYear();
          var thisPeriode = TransactionBA.trx_date_group;

          if (transaction.FEE.data[thisPeriode] == undefined) {
            transaction.FEE.data[thisPeriode] = {raw: [], rowspan: 0, desc: DescData};
            FeeKey.push(thisPeriode);
          }

          if (transaction.PAID.data[thisPeriode] == undefined) {
            transaction.PAID.data[thisPeriode] = {raw: [], rowspan: 0, desc: DescData};
            PaidKey.push(thisPeriode)
          }


          // else {
          //   if (TransactionBA.type_trx == "PPOB") {
          //     transaction.FEE.data[thisPeriode].rowspan = transaction.FEE.data[thisPeriode].rowspan + 1;
          //   }
          // }

          // if (PaidKey[thisPeriode] == undefined) {

          //   PaidKey.push(thisPeriode)
          // } else {
          //   if (TransactionBA.type_trx == "PPOB") {
          //     transaction.PAID.data[thisPeriode].rowspan = transaction.PAID.data[thisPeriode].rowspan + 1;
          //   }
          // }

          //NOMINAL FEE
          if (TransactionBA.trx_cluster_out != 0.00) {
            transaction.FEE.data[thisPeriode].raw.push({
              desc: DescData,
              type: TransactionBA.type_trx == "RECHARGE" ? "Recharge" : TransactionBA.type_trx,
              sales: ControllerConfig.onGeneratePuluhan(TransactionBA.sales_fee),
              count_trx: ControllerConfig.onGeneratePuluhan(TransactionBA.trx_cluster_out),
              total_amout: ControllerConfig.onGeneratePuluhan(TransactionBA.total_fee_amount),
              share_for: share_for_fee
            });
            transaction.FEE.data[thisPeriode].rowspan = transaction.FEE.data[thisPeriode].rowspan + 1;
          }

          //NOMINAL PAID
          if (TransactionBA.trx_paid_invoice != 0.00) {

            transaction.PAID.data[thisPeriode].raw.push({
              desc: DescData,
              type: TransactionBA.type_trx == "RECHARGE" ? "Recharge" : TransactionBA.type_trx,
              sales: ControllerConfig.onGeneratePuluhan(TransactionBA.sales_fee),
              count_trx: ControllerConfig.onGeneratePuluhan(TransactionBA.trx_paid_invoice),
              total_amout: ControllerConfig.onGeneratePuluhan(TransactionBA.total_paid_amount),
              share_for: share_for_paid
            });
            transaction.PAID.data[thisPeriode].rowspan = transaction.PAID.data[thisPeriode].rowspan + 1;
          }


          TotalTrxFee = TotalTrxFee + TransactionBA.trx_cluster_out;
          TotalAmoutFee = TotalAmoutFee + TransactionBA.total_fee_amount;
          TotalTrxPaid = TotalTrxPaid + TransactionBA.trx_paid_invoice;
          TotalAmoutPaid = TotalAmoutPaid + TransactionBA.total_paid_amount;
        }

        var todayDispute = new Date();
        digipos?.trx_date_group != undefined ? todayDispute = new Date(digipos?.trx_date_group) : "";
        var todaysDispute = todayDispute.setMonth(todayDispute.getMonth() - 1);
        var DateNewDispute = new Date(todaysDispute);
        var PeriodeDispute = DateNewDispute.getFullYear() + '-' + ('0' + (DateNewDispute.getMonth() + 1)).slice(-2);



        var dispute = await this.dataDisputeRepository.find({where: {and: [{periode: PeriodeDispute}, {dispute_status: 2}, {ad_code: digipos?.ad_code}]}});
        for (var iDispute in dispute) {
          var infoDispute = dispute[iDispute];
          var type = ["", "RECHARGE", "RECHARGE", "PPOB"];

          var share_for_fee: string; share_for_fee = (configNum["persen_fee"] * 100) + "%";
          var share_for_paid: string; share_for_paid = (configNum["persen_paid"] * 100) + "%";
          if (infoDispute.type_trx == 3) {
            share_for_fee = (configNum["persen_fee_ppob"] * 100) + "%";
            share_for_paid = (configNum["persen_paid_ppob"] * 100) + "%";
          }



          var thisDateFee = new Date(infoDispute.periode + "-01");
          var Mount = ControllerConfig.onGetMonth(thisDateFee.getMonth());
          var DescData = "Selisih " + Mount + " " + thisDateFee.getFullYear();
          var thisPeriode = "D-" + infoDispute.periode;


          if (transaction.FEE.data[thisPeriode] == undefined) {
            transaction.FEE.data[thisPeriode] = {raw: [], rowspan: 0, desc: DescData};
            FeeKey.push(thisPeriode);
          }


          if (transaction.PAID.data[thisPeriode] == undefined) {
            transaction.PAID.data[thisPeriode] = {raw: [], rowspan: 0, desc: DescData};
            PaidKey.push(thisPeriode)
          }


          if (infoDispute.type_trx == 1 || infoDispute.type_trx == 3) {
            transaction.FEE.data[thisPeriode].raw.push({
              desc: DescData,
              sales: infoDispute.sales_fee?.substring(4),
              type: (infoDispute.type_trx == 1 ? "Recharge" : "PPOB"),
              count_trx: ControllerConfig.onGeneratePuluhan(infoDispute.qty_trx == undefined ? 0 : infoDispute.qty_trx),
              total_amout: ControllerConfig.onGeneratePuluhan(infoDispute.tot_mdr == undefined ? 0 : infoDispute.tot_mdr),
              share_for: share_for_fee
            });
            transaction.FEE.data[thisPeriode].rowspan = transaction.FEE.data[thisPeriode].rowspan + 1;

            TotalTrxFee = TotalTrxFee + (infoDispute.qty_trx == undefined ? 0 : infoDispute.qty_trx);
            TotalAmoutFee = TotalAmoutFee + (infoDispute.tot_mdr == undefined ? 0 : infoDispute.tot_mdr);
          }

          if (infoDispute.type_trx == 2) {
            transaction.PAID.data[thisPeriode].raw.push({
              desc: DescData,
              sales: infoDispute.sales_fee?.substring(4),
              type: (type[infoDispute.type_trx] == "RECHARGE" ? "Recharge" : "Recharge"),
              count_trx: ControllerConfig.onGeneratePuluhan(infoDispute.qty_trx == undefined ? 0 : infoDispute.qty_trx),
              total_amout: ControllerConfig.onGeneratePuluhan(infoDispute.tot_mdr == undefined ? 0 : infoDispute.tot_mdr),
              share_for: share_for_paid
            });
            transaction.PAID.data[thisPeriode].rowspan = transaction.PAID.data[thisPeriode].rowspan + 1;

            TotalTrxPaid = TotalTrxPaid + (infoDispute.qty_trx == undefined ? 0 : infoDispute.qty_trx);
            TotalAmoutPaid = TotalAmoutPaid + (infoDispute.qty_trx == undefined ? 0 : infoDispute.qty_trx);
          }

        }

        transaction.FEE.TotalTrx = TotalTrxFee;
        transaction.FEE.TotalAmout = TotalAmoutFee;
        transaction.PAID.TotalTrx = TotalTrxPaid;
        transaction.PAID.TotalAmout = TotalAmoutPaid;

        DataBA_PDF.TotalTrxCount = InfoBA.type_ad == "AD_A" ? ControllerConfig.onGeneratePuluhan(transaction.FEE.TotalTrx) : ControllerConfig.onGeneratePuluhan(transaction.PAID.TotalTrx);
        DataBA_PDF.TotalTrxAmount = InfoBA.type_ad == "AD_A" ? ControllerConfig.onGeneratePuluhan(transaction.FEE.TotalAmout) : ControllerConfig.onGeneratePuluhan(transaction.PAID.TotalAmout);
        DataBA_PDF.DataTransaction = InfoBA.type_ad == "AD_A" ? transaction.FEE.data : transaction.PAID.data;
        DataBA_PDF.ketStore = InfoBA.type_ad == "AD_A" ? FeeKey : PaidKey;

        var TrxAmountStringFee = "Rp" + ControllerConfig.onGeneratePuluhan(transaction.FEE.TotalAmout) + " (" + ControllerConfig.onAngkaTerbilang(transaction.FEE.TotalAmout) + " Rupiah)";
        var TrxAmountStringPaid = "Rp" + ControllerConfig.onGeneratePuluhan(transaction.PAID.TotalAmout) + " (" + ControllerConfig.onAngkaTerbilang(transaction.PAID.TotalAmout) + " Rupiah)";

        var PeriodeTrx = DataBA_PDF.PeriodeData.split("-");
        var dateLast = new Date(PeriodeTrx[0], PeriodeTrx[1], 0);
        var dateAD_B = this.onGetDayString(dateLast.getDay()) + ", " + dateLast.getDate() + " " + this.onGetMonth(dateLast.getMonth()) + " " + dateLast.getFullYear();



        DataBA_PDF.NominalString = InfoBA.type_ad == "AD_A" ? TrxAmountStringFee : TrxAmountStringPaid;
        DataBA_PDF.DateBACreate = InfoBA.type_ad == "AD_A" ? this.onBANowDate() : dateAD_B;

        console.log(digipos?.trx_date_group, " | ", PeriodeDispute, "|", InfoBA.number_ba, pathFile1, pathFile2, pathFile);

        //MAKE FILE PDF BA
        var merger = new PDFMerger();
        var compiled = ejs.compile(fs.readFileSync(path.join(__dirname, '../../.digipost/template_ba/') + 'pdf_ba.html', 'utf8'));
        var html = compiled(DataBA_PDF);

        var config: any; config = {
          format: "A4",
          orientation: "portrait",
          "footer": {
            "height": "70px",
            "contents": ''
          }
        }

        pdf.create(html, config).toFile(pathFile1, async (err1: any, res1: any) => {
          if (err1) {
            delete DigiposGenerateFile[i];
            return console.log(err1);
          } else {
            pdf.create(html).toFile(pathFile2, async (err2: any, res2: any) => {
              if (err2) {
                delete DigiposGenerateFile[i];
                return console.log(err2);
              } else {

                merger.add(pathFile1);
                merger.add(pathFile2);
                await merger.save(pathFile);
                fs.unlinkSync(pathFile1);
                fs.unlinkSync(pathFile2);


                if (fs.existsSync(pathFile)) {
                  this.onGenerateBASummaryPDF(InfoBA, async () => {

                    var trxPeriode = InfoBA.trx_date_group;
                    delete DigiposGenerateFile[KeyIndex];
                    var pGenNo = 0;
                    for (var pGen in DigiposGenerateFile) {pGenNo++;}

                    console.log(res2); // { filename: '/app/businesscard.pdf' }
                    if (InfoBA.type_ad == "AD_A") {

                      await thisContent.dataDigiposRepository.updateById(InfoBA.digipost_id, {
                        ba_attachment_a: NameFileBA,
                        generate_file_a: 1
                      });

                      if (digipos?.sign_off_a == 1) {
                        ProcessGenerateDocBarASign = ProcessGenerateDocBarASign + 1;
                      } else {
                        ProcessGenerateDocBarA = ProcessGenerateDocBarA + 1;
                      }


                      // if (pGenNo == 0) {
                      //   if (digipos?.sign_off_a == 1) {
                      //     var generateBarA = await this.dataDigiposRepository.count({and: [{generate_file_a: 1}, {trx_date_group: trxPeriode}]});
                      //     var generateNotBarA = await this.dataDigiposRepository.count({and: [{generate_file_a: 0}, {trx_date_group: trxPeriode}]});
                      //     var userId = this.currentUserProfile[securityId];
                      //     var roleNotifSetADB = ControllerConfig.onGetRoleNotifGroup("16010104");
                      //     var infoUser = await this.userRepository.findById(userId)
                      //     await this.dataNotificationRepository.create({
                      //       types: "NOTIFICATION",
                      //       target: roleNotifSetADB.role,
                      //       to: "",
                      //       from_who: infoUser.username,
                      //       subject: roleNotifSetADB.label,
                      //       body: "Document BAR Recon A berhasil di sign, Jumlah yang sudah di sign : " + generateBarA.count.toString() + ", Jumlah yang belum di sign : " + generateNotBarA.count.toString(),
                      //       path_url: "",
                      //       app: "DIGIPOS",
                      //       code: "16010103",
                      //       code_label: "",
                      //       at_create: ControllerConfig.onCurrentTime().toString(),
                      //       at_read: "",
                      //       periode: trxPeriode,
                      //       at_flag: 0
                      //     });

                      //     ControllerConfig.onSendNotif({
                      //       types: "NOTIFICATION",
                      //       target: roleNotifSetADB.role,
                      //       to: "",
                      //       from_who: infoUser.username,
                      //       subject: roleNotifSetADB.label,
                      //       body: "Document BAR Recon A berhasil di sign, Jumlah yang sudah di sign : " + generateBarA.count.toString() + ", Jumlah yang belum di sign : " + generateNotBarA.count.toString(),
                      //       path_url: "",
                      //       app: "DIGIPOS",
                      //       code: "16010104",
                      //       code_label: "",
                      //       at_create: ControllerConfig.onCurrentTime().toString(),
                      //       at_read: "",
                      //       periode: trxPeriode,
                      //       at_flag: 0
                      //     });

                      //     // ControllerConfig.onSendNotif({
                      //     //   types: "NOTIFICATION",
                      //     //   target: roleNotifSetADB.role,
                      //     //   to: "",
                      //     //   from_who: infoUser.username,
                      //     //   subject: roleNotifSetADB.label,
                      //     //   body: "Generate Document BAR Recon untuk AD A Nomor : " + digipos?.ba_number_a + " sudah di sign berhasil.",
                      //     //   path_url: "",
                      //     //   app: "DIGIPOS",
                      //     //   code: "02020202",
                      //     //   code_label: "",
                      //     //   at_create: ControllerConfig.onCurrentTime().toString(),
                      //     //   at_read: "",
                      //     //   periode: digipos?.trx_date_group,
                      //     //   at_flag: 0
                      //     // });

                      //   } else {
                      //     var generateBarA = await this.dataDigiposRepository.count({and: [{generate_file_a: 1}, {trx_date_group: trxPeriode}]});
                      //     var generateNotBarA = await this.dataDigiposRepository.count({and: [{generate_file_a: 0}, {trx_date_group: trxPeriode}]});
                      //     var userId = this.currentUserProfile[securityId];
                      //     var roleNotifSetADB = ControllerConfig.onGetRoleNotifGroup("16010104");
                      //     var infoUser = await this.userRepository.findById(userId)
                      //     await this.dataNotificationRepository.create({
                      //       types: "NOTIFICATION",
                      //       target: roleNotifSetADB.role,
                      //       to: "",
                      //       from_who: infoUser.username,
                      //       subject: roleNotifSetADB.label,
                      //       body: "Document BAR Recon A berhasil di generate, Jumlah yang tergenerate : " + generateBarA.count.toString() + ", Jumlah yang belum tergenerate : " + generateNotBarA.count.toString(),
                      //       path_url: "",
                      //       app: "DIGIPOS",
                      //       code: "16010104",
                      //       code_label: "",
                      //       at_create: ControllerConfig.onCurrentTime().toString(),
                      //       at_read: "",
                      //       periode: trxPeriode,
                      //       at_flag: 0
                      //     });

                      //     ControllerConfig.onSendNotif({
                      //       types: "NOTIFICATION",
                      //       target: roleNotifSetADB.role,
                      //       to: "",
                      //       from_who: infoUser.username,
                      //       subject: roleNotifSetADB.label,
                      //       body: "Document BAR Recon A berhasil di generate, Jumlah yang tergenerate : " + generateBarA.count.toString() + ", Jumlah yang belum tergenerate : " + generateNotBarA.count.toString(),
                      //       path_url: "",
                      //       app: "DIGIPOS",
                      //       code: "16010104",
                      //       code_label: "",
                      //       at_create: ControllerConfig.onCurrentTime().toString(),
                      //       at_read: "",
                      //       periode: trxPeriode,
                      //       at_flag: 0
                      //     });

                      //     // var userId = this.currentUserProfile[securityId];
                      //     // var roleNotifSetADB = ControllerConfig.onGetRoleNotifGroup("03010101");
                      //     // var infoUser = await this.userRepository.findById(userId)
                      //     // await this.dataNotificationRepository.create({
                      //     //   types: "NOTIFICATION",
                      //     //   target: roleNotifSetADB.role,
                      //     //   to: "",
                      //     //   from_who: infoUser.username,
                      //     //   subject: roleNotifSetADB.label,
                      //     //   body: "Generate Document BAR Recon untuk AD A Nomor : " + digipos?.ba_number_a + " yang belum di sign berhasil.",
                      //     //   path_url: "",
                      //     //   app: "DIGIPOS",
                      //     //   code: "03010101",
                      //     //   code_label: "",
                      //     //   at_create: ControllerConfig.onCurrentTime().toString(),
                      //     //   at_read: "",
                      //     //   periode: digipos?.trx_date_group,
                      //     //   at_flag: 0
                      //     // });

                      //     // ControllerConfig.onSendNotif({
                      //     //   types: "NOTIFICATION",
                      //     //   target: roleNotifSetADB.role,
                      //     //   to: "",
                      //     //   from_who: infoUser.username,
                      //     //   subject: roleNotifSetADB.label,
                      //     //   body: "Generate Document BAR Recon untuk AD A Nomor : " + digipos?.ba_number_a + " yang belum di sign berhasil.",
                      //     //   path_url: "",
                      //     //   app: "DIGIPOS",
                      //     //   code: "03010101",
                      //     //   code_label: "",
                      //     //   at_create: ControllerConfig.onCurrentTime().toString(),
                      //     //   at_read: "",
                      //     //   periode: digipos?.trx_date_group,
                      //     //   at_flag: 0
                      //     // });
                      //   }
                      // }

                    }

                    if (InfoBA.type_ad == "AD_B") {
                      await thisContent.dataDigiposRepository.updateById(InfoBA.digipost_id, {
                        ba_attachment_b: NameFileBA,
                        generate_file_b: 1
                      });

                      if (digipos?.sign_off_b == 1) {
                        ProcessGenerateDocBarBSign = ProcessGenerateDocBarBSign + 1;
                      } else {
                        ProcessGenerateDocBarB = ProcessGenerateDocBarB + 1;
                      }

                      // var userId = this.currentUserProfile[securityId];
                      // if (digipos?.sign_off_b == 1) {
                      //   var generateBarB = await this.dataDigiposRepository.count({and: [{generate_file_b: 1}, {trx_date_group: trxPeriode}]});
                      //   var generateNotBarB = await this.dataDigiposRepository.count({and: [{generate_file_b: 0}, {trx_date_group: trxPeriode}]});
                      //   var userId = this.currentUserProfile[securityId];
                      //   var roleNotifSetADB = ControllerConfig.onGetRoleNotifGroup("16010104");
                      //   var infoUser = await this.userRepository.findById(userId)
                      //   await this.dataNotificationRepository.create({
                      //     types: "NOTIFICATION",
                      //     target: roleNotifSetADB.role,
                      //     to: "",
                      //     from_who: infoUser.username,
                      //     subject: roleNotifSetADB.label,
                      //     body: "Document BAR Recon B berhasil di sign, Jumlah yang sudah di sign : " + generateBarB.count.toString() + ", Jumlah yang belum di sign : " + generateNotBarB.count.toString(),
                      //     path_url: "",
                      //     app: "DIGIPOS",
                      //     code: "16010104",
                      //     code_label: "",
                      //     at_create: ControllerConfig.onCurrentTime().toString(),
                      //     at_read: "",
                      //     periode: trxPeriode,
                      //     at_flag: 0
                      //   });

                      //   ControllerConfig.onSendNotif({
                      //     types: "NOTIFICATION",
                      //     target: roleNotifSetADB.role,
                      //     to: "",
                      //     from_who: infoUser.username,
                      //     subject: roleNotifSetADB.label,
                      //     body: "Document BAR Recon B berhasil di sign, Jumlah yang sudah di sign : " + generateBarB.count.toString() + ", Jumlah yang belum di sign : " + generateNotBarB.count.toString(),
                      //     path_url: "",
                      //     app: "DIGIPOS",
                      //     code: "16010104",
                      //     code_label: "",
                      //     at_create: ControllerConfig.onCurrentTime().toString(),
                      //     at_read: "",
                      //     periode: trxPeriode,
                      //     at_flag: 0
                      //   });
                      //   // var roleNotifSetADB = ControllerConfig.onGetRoleNotifGroup("03010102");
                      //   // var infoUser = await this.userRepository.findById(userId)
                      //   // await this.dataNotificationRepository.create({
                      //   //   types: "NOTIFICATION",
                      //   //   target: roleNotifSetADB.role,
                      //   //   to: "",
                      //   //   from_who: infoUser.username,
                      //   //   subject: roleNotifSetADB.label,
                      //   //   body: "Generate Document BAR Recon untuk AD B Nomor : " + digipos?.ba_number_b + " sudah di sign berhasil.",
                      //   //   path_url: "",
                      //   //   app: "DIGIPOS",
                      //   //   code: "03010102",
                      //   //   code_label: "",
                      //   //   at_create: ControllerConfig.onCurrentTime().toString(),
                      //   //   at_read: "",
                      //   //   periode: digipos?.trx_date_group,
                      //   //   at_flag: 0
                      //   // });

                      //   // ControllerConfig.onSendNotif({
                      //   //   types: "NOTIFICATION",
                      //   //   target: roleNotifSetADB.role,
                      //   //   to: "",
                      //   //   from_who: infoUser.username,
                      //   //   subject: roleNotifSetADB.label,
                      //   //   body: "Generate Document BAR Recon untuk AD B Nomor : " + digipos?.ba_number_b + " sudah di sign berhasil.",
                      //   //   path_url: "",
                      //   //   app: "DIGIPOS",
                      //   //   code: "03010102",
                      //   //   code_label: "",
                      //   //   at_create: ControllerConfig.onCurrentTime().toString(),
                      //   //   at_read: "",
                      //   //   periode: digipos?.trx_date_group,
                      //   //   at_flag: 0
                      //   // });

                      // } else {
                      //   var generateBarB = await this.dataDigiposRepository.count({and: [{generate_file_b: 1}, {trx_date_group: trxPeriode}]});
                      //   var generateNotBarB = await this.dataDigiposRepository.count({and: [{generate_file_b: 0}, {trx_date_group: trxPeriode}]});
                      //   var userId = this.currentUserProfile[securityId];
                      //   var roleNotifSetADB = ControllerConfig.onGetRoleNotifGroup("16010104");
                      //   var infoUser = await this.userRepository.findById(userId)
                      //   await this.dataNotificationRepository.create({
                      //     types: "NOTIFICATION",
                      //     target: roleNotifSetADB.role,
                      //     to: "",
                      //     from_who: infoUser.username,
                      //     subject: roleNotifSetADB.label,
                      //     body: "Document BAR Recon B berhasil di generate, Jumlah yang tergenerate : " + generateBarB.count.toString() + ", Jumlah yang belum tergenerate : " + generateNotBarB.count.toString(),
                      //     path_url: "",
                      //     app: "DIGIPOS",
                      //     code: "16010104",
                      //     code_label: "",
                      //     at_create: ControllerConfig.onCurrentTime().toString(),
                      //     at_read: "",
                      //     periode: trxPeriode,
                      //     at_flag: 0
                      //   });

                      //   ControllerConfig.onSendNotif({
                      //     types: "NOTIFICATION",
                      //     target: roleNotifSetADB.role,
                      //     to: "",
                      //     from_who: infoUser.username,
                      //     subject: roleNotifSetADB.label,
                      //     body: "Document BAR Recon B berhasil di generate, Jumlah yang tergenerate : " + generateBarB.count.toString() + ", Jumlah yang belum tergenerate : " + generateNotBarB.count.toString(),
                      //     path_url: "",
                      //     app: "DIGIPOS",
                      //     code: "16010104",
                      //     code_label: "",
                      //     at_create: ControllerConfig.onCurrentTime().toString(),
                      //     at_read: "",
                      //     periode: trxPeriode,
                      //     at_flag: 0
                      //   });

                      //   // var roleNotifSetADB = ControllerConfig.onGetRoleNotifGroup("03010101");
                      //   // var infoUser = await this.userRepository.findById(userId)
                      //   // await this.dataNotificationRepository.create({
                      //   //   types: "NOTIFICATION",
                      //   //   target: roleNotifSetADB.role,
                      //   //   to: "",
                      //   //   from_who: infoUser.username,
                      //   //   subject: roleNotifSetADB.label,
                      //   //   body: "Generate Document BAR Recon untuk AD B Nomor : " + digipos?.ba_number_b + " yang belum di sign berhasil.",
                      //   //   path_url: "",
                      //   //   app: "DIGIPOS",
                      //   //   code: "03010101",
                      //   //   code_label: "",
                      //   //   at_create: ControllerConfig.onCurrentTime().toString(),
                      //   //   at_read: "",
                      //   //   periode: digipos?.trx_date_group,
                      //   //   at_flag: 0
                      //   // });

                      //   // ControllerConfig.onSendNotif({
                      //   //   types: "NOTIFICATION",
                      //   //   target: roleNotifSetADB.role,
                      //   //   to: "",
                      //   //   from_who: infoUser.username,
                      //   //   subject: roleNotifSetADB.label,
                      //   //   body: "Generate Document BAR Recon untuk AD B Nomor : " + digipos?.ba_number_b + " yang belum di sign berhasil.",
                      //   //   path_url: "",
                      //   //   app: "DIGIPOS",
                      //   //   code: "03010101",
                      //   //   code_label: "",
                      //   //   at_create: ControllerConfig.onCurrentTime().toString(),
                      //   //   at_read: "",
                      //   //   periode: digipos?.trx_date_group,
                      //   //   at_flag: 0
                      //   // });
                      // }

                    }

                    if (pGenNo == 0) {

                      //DOC BAR A
                      if (ProcessGenerateDocBarA != 0) {
                        var userId = this.currentUserProfile[securityId];
                        var roleNotifSetADB = ControllerConfig.onGetRoleNotifGroup("16010104");
                        var infoUser = await this.userRepository.findById(userId);
                        var message = {
                          types: "NOTIFICATION",
                          target: roleNotifSetADB.role,
                          to: "",
                          from_who: infoUser.username,
                          subject: "Document BAR Recon A berhasil di generate",
                          body: "Document BAR Recon A berhasil di generate, Jumlah yang di generate : " + ProcessGenerateDocBarA.toString(),
                          path_url: "",
                          app: "DIGIPOS",
                          code: "16010104",
                          code_label: "",
                          at_create: ControllerConfig.onCurrentTime().toString(),
                          at_read: "",
                          periode: trxPeriode,
                          at_flag: 0
                        }
                        await this.dataNotificationRepository.create(message);
                        ControllerConfig.onSendNotif(message);
                      }

                      //SIGN DOC BAR A
                      if (ProcessGenerateDocBarASign != 0) {
                        var userId = this.currentUserProfile[securityId];
                        var roleNotifSetADB = ControllerConfig.onGetRoleNotifGroup("16010108");
                        var infoUser = await this.userRepository.findById(userId);
                        var message = {
                          types: "NOTIFICATION",
                          target: roleNotifSetADB.role,
                          to: "",
                          from_who: infoUser.username,
                          subject: "Document BAR Recon A berhasil di sign",
                          body: "Document BAR Recon A berhasil di sign, Jumlah yang  di sign : " + ProcessGenerateDocBarASign.toString(),
                          path_url: "",
                          app: "DIGIPOS",
                          code: "16010104",
                          code_label: "",
                          at_create: ControllerConfig.onCurrentTime().toString(),
                          at_read: "",
                          periode: trxPeriode,
                          at_flag: 0
                        }
                        await this.dataNotificationRepository.create(message);
                        ControllerConfig.onSendNotif(message);
                      }

                      // DOC BAR B
                      if (ProcessGenerateDocBarB != 0) {
                        var userId = this.currentUserProfile[securityId];
                        var roleNotifSetADB = ControllerConfig.onGetRoleNotifGroup("16010104");
                        var infoUser = await this.userRepository.findById(userId);
                        var message = {
                          types: "NOTIFICATION",
                          target: roleNotifSetADB.role,
                          to: "",
                          from_who: infoUser.username,
                          subject: "Document BAR Recon B berhasil di generate",
                          body: "Document BAR Recon B berhasil di generate, Jumlah yang di generate : " + ProcessGenerateDocBarB.toString(),
                          path_url: "",
                          app: "DIGIPOS",
                          code: "16010104",
                          code_label: "",
                          at_create: ControllerConfig.onCurrentTime().toString(),
                          at_read: "",
                          periode: trxPeriode,
                          at_flag: 0
                        }
                        await this.dataNotificationRepository.create(message);
                        ControllerConfig.onSendNotif(message);
                      }

                      //SIGN DOC BAR B
                      if (ProcessGenerateDocBarBSign != 0) {
                        var userId = this.currentUserProfile[securityId];
                        var roleNotifSetADB = ControllerConfig.onGetRoleNotifGroup("16010108");
                        var infoUser = await this.userRepository.findById(userId);
                        var message = {
                          types: "NOTIFICATION",
                          target: roleNotifSetADB.role,
                          to: "",
                          from_who: infoUser.username,
                          subject: "Document BAR Recon B berhasil di sign",
                          body: "Document BAR Recon B berhasil di sign, Jumlah yang  di sign : " + ProcessGenerateDocBarBSign.toString(),
                          path_url: "",
                          app: "DIGIPOS",
                          code: "16010104",
                          code_label: "",
                          at_create: ControllerConfig.onCurrentTime().toString(),
                          at_read: "",
                          periode: trxPeriode,
                          at_flag: 0
                        }
                        await this.dataNotificationRepository.create(message);
                        ControllerConfig.onSendNotif(message);
                      }
                      // RESET
                      ProcessGenerateDocBarA = 0;
                      ProcessGenerateDocBarASign = 0;
                      ProcessGenerateDocBarB = 0;
                      ProcessGenerateDocBarBSign = 0;
                    }

                    thisContent.onGenerateBAPDF();
                  });
                } else {
                  delete DigiposGenerateFile[i];
                  return console.log(err2);
                }
              }
            });
          }
        });
      }
      break;
    }
  }

  onGenerateFileBA = async (type_ad: string, dataFileGenerate: DataDigipos[]) => {
    for (var i in dataFileGenerate) {
      var infoDataFileGenerate = dataFileGenerate[i];
      var PortalAd = await this.adMasterRepository.findById(infoDataFileGenerate.ad_code);
      if (PortalAd.contract_number == "" || PortalAd.contract_number == null || PortalAd.contract_number == undefined) {
        continue;
      }

      var DigiposID = infoDataFileGenerate.digipost_id == undefined || infoDataFileGenerate.digipost_id == null ? "0" : infoDataFileGenerate.digipost_id;
      var ID = DigiposID + "_" + type_ad;
      if (DigiposGenerateFile[ID] == undefined) {
        if (type_ad == "AD_A") {
          if (infoDataFileGenerate.ba_number_a == null || infoDataFileGenerate.ba_number_a == undefined || infoDataFileGenerate.ba_number_a == "") {
          } else {
            DigiposGenerateFile[ID] = {
              ad_code: infoDataFileGenerate.ad_code,
              digipost_id: infoDataFileGenerate.digipost_id,
              periode: infoDataFileGenerate.trx_date_group,
              number_ba: infoDataFileGenerate.ba_number_a,
              type_ad: "AD_A"
            };
          }
        }
        if (type_ad == "AD_B") {
          if (infoDataFileGenerate.ba_number_b == null || infoDataFileGenerate.ba_number_b == undefined || infoDataFileGenerate.ba_number_b == "") {
          } else {
            DigiposGenerateFile[ID] = {
              ad_code: infoDataFileGenerate.ad_code,
              digipost_id: infoDataFileGenerate.digipost_id,
              periode: infoDataFileGenerate.trx_date_group,
              number_ba: infoDataFileGenerate.ba_number_b,
              type_ad: "AD_B"
            };
          }
        }
      } else {
        continue;
      }
    }
    this.onGenerateBAPDF();
  }

  //GENERATE FILE BA BY ID
  @post('/generate-doc/generate-ba-file-by-id', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: GenerateFileBARequestBody
          }
        },
      },
    },
  })
  async GenerateFileBAByID(
    @requestBody(GenerateFileBARequestBody) generateFileBARequestBody: {digipos: [], type_ad: string},
  ): Promise<Object> {
    var returnData: any; returnData = {};

    ProcessGenerateDocBarA = 0;
    ProcessGenerateDocBarASign = 0;
    ProcessGenerateDocBarB = 0;
    ProcessGenerateDocBarBSign = 0;

    var FoundDigipost = await this.dataDigiposRepository.count({and: [{digipost_id: {inq: generateFileBARequestBody.digipos}}, {ar_stat: 2}]});
    if (FoundDigipost.count > 0) {
      var dataFileGenerate = await this.dataDigiposRepository.find({where: {and: [{digipost_id: {inq: generateFileBARequestBody.digipos}}, {ar_stat: 2}]}});
      if (generateFileBARequestBody.type_ad == "AD_A" || generateFileBARequestBody.type_ad == "AD_B") {
        this.onGenerateFileBA(generateFileBARequestBody.type_ad, dataFileGenerate);
        returnData["success"] = true;
        returnData["message"] = "Success generate file BA, count file BA success create : [" + FoundDigipost.count + "] !";
      } else {
        returnData["error"] = true;
        returnData["message"] = "Error, type ad " + generateFileBARequestBody.type_ad + " not found : " + JSON.stringify(generateFileBARequestBody.digipos) + " !";
      }
    } else {
      returnData["error"] = true;
      returnData["message"] = "Error, Can't found data digipos : " + JSON.stringify(generateFileBARequestBody.digipos) + " !";
    }
    return returnData;
  }

  //COUNT PROCESS GENERATE FILE
  @get('/generate-doc/count-process-generate-file-ba', {
    responses: {
      '200': {
        description: 'Generate BA model instance',
        content: {
          'application/json': {
            schema: {}
          },
        },
      },
    },
  })
  async CounProcesstGenerateFileBA(

  ): Promise<Object> {
    var returnData: any; returnData = {};
    returnData["success"] = true;
    var no = 0;
    for (var i in DigiposGenerateFile) {
      no++;
    }
    returnData["process_count"] = no;
    return returnData;
  }



  @post('/generate-doc/regenerate-pph23-value', {
    responses: {
      '200': {
        description: 'Generate BA model instance',
        content: {
          'application/json': {
            schema: GenerateFPJPUpdatePPH
          },
        },
      },
    },
  })
  async ReGenerateFPJPPph(
    @requestBody(GenerateFPJPUpdatePPH) generateFPJPUpdatePPH: {fpjp_id: string},
  ): Promise<Object> {
    var returnData: any; returnData = {};

    return returnData;
  }

  //GENERATE FILE BERITA ACARA AUTO
  @post('/generate-doc/generate-ba-file-auto', {
    responses: {
      '200': {
        description: 'Generate BA model instance',
        content: {
          'application/json': {
            schema: GenerateAutoFileBA
          },
        },
      },
    },
  })
  async GenerateAutoFileBA(
    @requestBody(GenerateAutoFileBA) generateAutoFileBA: {periode: string},
  ): Promise<Object> {
    var returnData: any; returnData = {};

    ProcessGenerateDocBarA = 0;
    ProcessGenerateDocBarASign = 0;
    ProcessGenerateDocBarB = 0;
    ProcessGenerateDocBarBSign = 0;

    var FoundDigipost = await this.dataDigiposRepository.count({and: [{trx_date_group: generateAutoFileBA.periode}, {ar_stat: 2}]});
    if (FoundDigipost.count > 0) {
      var dataFileGenerate = await this.dataDigiposRepository.find({where: {and: [{trx_date_group: generateAutoFileBA.periode}, {ar_stat: 2}]}});
      this.onGenerateFileBA("AD_A", dataFileGenerate);
      this.onGenerateFileBA("AD_B", dataFileGenerate);
      returnData["success"] = true;
      returnData["message"] = "Success generate file BA, count file BA success create : [" + FoundDigipost.count + "] !";
    } else {
      returnData["error"] = true;
      returnData["message"] = "Error, Can't found data digipos :  !";
    }
    return returnData;
  }


  //GENERATE NOMOR BERITA ACARA AUTO
  @post('/generate-doc/generate-ba-no-auto', {
    responses: {
      '200': {
        description: 'Generate BA model instance',
        content: {
          'application/json': {
            schema: GenerateAutoNoBA
          },
        },
      },
    },
  })
  async GenerateAutoNoBA(
    @requestBody(GenerateAutoNoBA) generateAutoBA: {type_ad: string, periode: string, last_number: number, ba_label: string},
  ): Promise<Object> {
    var returnMsg: any; returnMsg = {success: true, message: "Success generate ba number !"};
    var infoAttention = await this.dataDigiposAttentionRepository.findOne({where: {and: [{set_attention: true}, {group: 1}]}});
    if (infoAttention != null) {
      var NotSuccessPeriode = await this.dataDigiposRepository.count({and: [{trx_date_group: generateAutoBA.periode}, {ar_stat: {inq: [-1, -2, 1]}}]});
      if (NotSuccessPeriode.count == 0) {
        var StartNoBA = generateAutoBA.last_number + 1;
        var RawDigipost = await this.dataDigiposRepository.find({where: {and: [{ar_stat: 2}, {trx_date_group: generateAutoBA.periode}]}});

        var CheckNoBA = generateAutoBA.last_number + 1;
        var ErrorBaNumber = false;
        for (var i in RawDigipost) {
          var infoDigipos = RawDigipost[i];
          var PortalAd = await this.adMasterRepository.findById(infoDigipos.ad_code);
          if (PortalAd.contract_number == "" || PortalAd.contract_number == null || PortalAd.contract_number == undefined) {
            continue;
          }


          var NoBA = CheckNoBA.toString() + "/" + generateAutoBA.ba_label;
          var CountBAFound = await this.dataDigiposRepository.count({or: [{ba_number_a: NoBA}, {ba_number_b: NoBA}]});
          if (CountBAFound.count != 0) {
            ErrorBaNumber = true;
            returnMsg.success = false;
            returnMsg.message = "Error Duplicate BA Number, No BA have duplicate in BA Number : " + NoBA;
            break;
          }
          CheckNoBA++;
        }
        if (ErrorBaNumber == false) {
          if (generateAutoBA.type_ad == "AD_A") {
            for (var i in RawDigipost) {
              var infoDigipos = RawDigipost[i];

              var PortalAd = await this.adMasterRepository.findById(infoDigipos.ad_code);
              if (PortalAd.contract_number == "" || PortalAd.contract_number == null || PortalAd.contract_number == undefined) {
                continue;
              }

              if (infoDigipos.trx_cluster_out <= 0) {
                continue;
              }
              await this.dataDigiposRepository.updateById(infoDigipos.digipost_id, {
                ba_number_a: StartNoBA.toString() + "/" + generateAutoBA.ba_label,
                google_drive_a: 0,
                generate_file_a: 0,
                acc_file_ba_a: 0
              });
              StartNoBA++;
            }
          } else if (generateAutoBA.type_ad == "AD_B") {
            for (var i in RawDigipost) {
              var infoDigipos = RawDigipost[i];

              var PortalAd = await this.adMasterRepository.findById(infoDigipos.ad_code);
              if (PortalAd.contract_number == "" || PortalAd.contract_number == null || PortalAd.contract_number == undefined) {
                continue;
              }

              if (infoDigipos.trx_paid_invoice <= 0) {
                continue;
              }
              await this.dataDigiposRepository.updateById(infoDigipos.digipost_id, {
                ba_number_b: StartNoBA.toString() + "/" + generateAutoBA.ba_label,
                google_drive_b: 0,
                generate_file_b: 0,
                acc_file_ba_b: 0
              });
              StartNoBA++;
            }
          } else {
            returnMsg.success = false;
            returnMsg.message = "Error generate BA number, Please input other Type AD !";
          }
        }
      } else {
        returnMsg.success = false;
        returnMsg.message = "Error generate BA number, Please validate & check all data digipos periode : " + generateAutoBA.periode;
      }
    } else {
      returnMsg.success = false;
      returnMsg.message = "Error generate BA number, Please set attention link aja ! ";
    }
    return returnMsg;
  }


  // =======================INVOICE========================

  // GenerateFileInvoiceRequestBody
  //COUNT PROCESS GENERATE FILE
  @get('/generate-doc/count-process-generate-file-invoice', {
    responses: {
      '200': {
        description: 'Generate BA model instance',
        content: {
          'application/json': {
            schema: {}
          },
        },
      },
    },
  })
  async CounProcesstGenerateFileInvoice(

  ): Promise<Object> {
    var returnData: any; returnData = {};
    returnData["success"] = true;
    var no = 0;
    for (var i in GenerateFileInvoice) {
      no++;
    }
    returnData["process_count"] = no;
    return returnData;
  }

  @post('/generate-doc/generate-file-invoice', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: GenerateFileInvoiceRequestBody
          }
        },
      },
    },
  })
  async generateFileInvoice(
    @requestBody(GenerateFileInvoiceRequestBody) docInvoice: {digipos: []},
  ): Promise<Object> {
    var returnBody: any; returnBody = {success: true, message: ""};
    var dataDigipos = await this.dataDigiposRepository.find({where: {and: [{digipost_id: {inq: docInvoice.digipos}}, {generate_file_b: 1}, {acc_file_ba_b: 1}]}});
    returnBody.success = true;
    returnBody.message = "Generate no invoice success, count invoice generate : [" + dataDigipos.length + "] !";
    if (dataDigipos.length > 0) {
      for (var i in dataDigipos) {
        var infoDigipost = dataDigipos[i];

        var PortalAd = await this.adMasterRepository.findById(infoDigipost.ad_code);
        if (PortalAd.contract_number == "" || PortalAd.contract_number == null || PortalAd.contract_number == undefined) {
          continue;
        }

        if (infoDigipost.inv_number != "") {
          var DigiposID = infoDigipost.digipost_id == null ? "0" : infoDigipost.digipost_id;
          GenerateFileInvoice[DigiposID] = infoDigipost;
        } else {
          returnBody.success = false;
          returnBody.message = "Error generate invoice, check and set number invoice before generate invoice file !";
        }
      }
      this.onGenerateInvoicePDF();
    } else {
      returnBody.success = false;
      returnBody.message = "No file invoice generate !";
    }
    return returnBody;
  }

  @post('/generate-doc/generate-file-invoice-all', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: GenerateFileInvoiceID
          }
        },
      },
    },
  })
  async generateFileInvoiceAll(
    @requestBody(GenerateFileInvoiceID) docInvoice: {periode: string},
  ): Promise<Object> {
    var returnBody: any; returnBody = {success: true, message: ""};
    var dataDigipos = await this.dataDigiposRepository.find({where: {and: [{trx_date_group: docInvoice.periode}, {generate_file_b: 1}, {acc_file_ba_b: 1}]}});

    var no = 0;
    if (dataDigipos.length > 0) {
      for (var i in dataDigipos) {
        var infoDigipost = dataDigipos[i];

        var PortalAd = await this.adMasterRepository.findById(infoDigipost.ad_code);
        if (PortalAd.contract_number == "" || PortalAd.contract_number == null || PortalAd.contract_number == undefined) {
          continue;
        }


        if (infoDigipost.inv_number != "") {
          var DigiposID = infoDigipost.digipost_id == null ? "-1" : infoDigipost.digipost_id;
          GenerateFileInvoice[DigiposID] = infoDigipost;
          no++;
        }
      }
      if (no == 0) {
        returnBody.success = false;
        returnBody.message = "Nothing invoice file generate, count invoice generate : [" + no + "] !";
      } else {
        returnBody.success = true;
        returnBody.message = "Generate file invoice success, count invoice generate : [" + no + "] !";
      }
      this.onGenerateInvoicePDF();
    } else {
      returnBody.success = false;
      returnBody.message = "No file invoice generate !";
    }
    return returnBody;
  }


  @post('/generate-doc/generate-no-invoice-all', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: GenerateNoInvoiceBody
          }
        },
      },
    },
  })
  async generateNoInvoice(
    @requestBody(GenerateNoInvoiceBody) docInvoice: {label: string, last_number: number, periode: string},
  ): Promise<Object> {
    var returnBody: any; returnBody = {success: true, message: ""};
    var dataDigipos = await this.dataDigiposRepository.find({where: {and: [{trx_date_group: docInvoice.periode}, {generate_file_b: 1}, {acc_file_ba_b: 1}, {inv_number: ""}]}});



    if (dataDigipos.length > 0) {
      var Found = false;
      var CheckNoInvoice = docInvoice.last_number + 1;
      for (var i in dataDigipos) {
        var rawDigiposCheck = dataDigipos[i];

        //No Infoice Skip
        var PortalAd = await this.adMasterRepository.findById(rawDigiposCheck.ad_code);
        if (PortalAd.contract_number == "" || PortalAd.contract_number == null || PortalAd.contract_number == undefined) {
          continue;
        }

        if (rawDigiposCheck.trx_paid_invoice <= 0) {
          continue;
        }
        var Zero = "0000";
        var invoiceNo = docInvoice.label + "-" + Zero.substring(CheckNoInvoice.toString().length, 4) + CheckNoInvoice.toString();
        var CountInv = await this.dataDigiposRepository.count({inv_number: invoiceNo});
        if (CountInv.count > 0) {
          Found = true;
          returnBody.success = false;
          returnBody.message = "Error generate no invoice, because no invoice has duplicate number is : [" + invoiceNo + "] !";
          break;
        }
        CheckNoInvoice++;
      }

      var no = 0;
      var NoInvoice = docInvoice.last_number + 1;
      if (Found == false) {
        for (var i in dataDigipos) {
          var infoDigipost = dataDigipos[i];
          try {
            var Zero = "0000";
            var UpdateNoInvoice = await this.dataDigiposRepository.updateById(infoDigipost.digipost_id, {
              inv_number: docInvoice.label + "-" + Zero.substring(NoInvoice.toString().length, 4) + NoInvoice.toString()
            });
            no++;
          } catch (error) {
            returnBody.success = false;
            returnBody.message = error;
            break;
          }
          NoInvoice++;
        }
        var infoDigiposData = await this.dataDigiposRepository.find({where: {and: [{trx_date_group: docInvoice.periode}, {generate_file_b: 1}, {acc_file_ba_b: 1}]}});
        // GenerateFileInvoice = infoDigiposData;
        // this.onGenerateInvoicePDF();
        returnBody.success = true;
        returnBody.message = "Generate number invoice success, count invoice number generate : [" + no + "] !";
      }
    }
    return returnBody;
  }




  @post('/generate-doc/generate-file-fpjp', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: GenerateFPJPByIdBody
          }
        },
      },
    },
  })
  async generateFPJP(
    @requestBody(GenerateFPJPByIdBody) docFPJP: {fpjp_number: string},
  ): Promise<Object> {
    var returnBody: any; returnBody = {success: true, message: ""};
    var fpjpRaw = await this.dataFpjpRepository.findOne({where: {transaction_code: docFPJP.fpjp_number}});
    if (fpjpRaw != null) {
      if (fpjpRaw.id != undefined) {
        if (fpjpRaw.acc_ba_state != 1) {
          returnBody.success = false;
          returnBody.message = "Error generate, FPJP no validate by BA !";
        } else if (fpjpRaw.acc_bu_state != 1) {
          returnBody.success = false;
          returnBody.message = "Error generate, FPJP no validate by BU !";
        } else if (fpjpRaw.state != 3) {
          returnBody.success = false;
          returnBody.message = "Error generate, FPJP no validate !";
        } else {
          FpjpGenerateStore[fpjpRaw.id] = fpjpRaw;
          console.log("RIINING START...");
          this.onReGenerateFileFpjp();
          runFpjpGenerate = true;
          returnBody.success = true;
          returnBody.message = "Success !";
        }
      } else {
        returnBody.success = false;
        returnBody.message = "Error generate, not found data nota for FPJP number :" + docFPJP.fpjp_number + " !";
      }
    } else {
      returnBody.success = false;
      returnBody.message = "Error generate, not found data nota for FPJP !";
    }
    // return /;
    // returnBody.message = DataFPJP;
    return returnBody;
  }


}
