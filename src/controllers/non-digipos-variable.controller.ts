// // Uncomment these imports to begin using these cool features!

// import {Filter, FilterExcludingWhere, repository} from '@loopback/repository';
// import {del, get, getModelSchemaRef, param, post, put, requestBody} from '@loopback/rest';
// import {MerchantVariable} from '../models';
// import {MerchantVariableRepository} from '../repositories';
// import {ControllerConfig} from './controller.config';

// // import {inject} from '@loopback/core';


// export class NonDigiposVariableController {
//   constructor(
//     @repository(MerchantVariableRepository)
//     public merchantVariableRepository: MerchantVariableRepository,
//   ) { }

//   @get('/non-digipos-variable', {
//     responses: {
//       '200': {
//         description: 'Array of non digipos variable type model instances',
//         content: {
//           'application/json': {
//             schema: {
//               type: 'array',
//               items: getModelSchemaRef(MerchantVariable, {includeRelations: true}),
//             },
//           },
//         },
//       },
//     },
//   })
//   async find(
//     @param.filter(MerchantVariable) filter?: Filter<MerchantVariable>,
//   ): Promise<MerchantVariable[]> {
//     return this.merchantVariableRepository.find(filter);
//   }

//   @get('/non-digipos-variable/{id}', {
//     responses: {
//       '200': {
//         description: 'non digipos variable type model instance',
//         content: {
//           'application/json': {
//             schema: getModelSchemaRef(MerchantVariable, {includeRelations: true}),
//           },
//         },
//       },
//     },
//   })
//   async findById(
//     @param.path.number('id') id: number,
//     @param.filter(MerchantVariable, {exclude: 'where'}) filter?: FilterExcludingWhere<MerchantVariable>
//   ): Promise<MerchantVariable> {
//     return this.merchantVariableRepository.findById(id, filter);
//   }



//   @post('/non-digipos-variable/create', {
//     responses: {
//       '200': {
//         description: 'Menu model instance',
//         content: {'application/json': {schema: getModelSchemaRef(MerchantVariable)}},
//       },
//     },
//   })
//   async create(
//     @requestBody({
//       content: {
//         'application/json': {
//           schema: getModelSchemaRef(MerchantVariable, {
//             title: 'NewMerchanVariable',
//             exclude: ['mv_id', 'mv_code', 'mv_at_create', 'mv_at_update'],
//           }),
//         },
//       },
//     })
//     variable: Omit<MerchantVariable, 'mv_id'>,
//   ): Promise<Object> {
//     var result: any; result = {success: false, message: "", data: {}};
//     try {
//       var infoCreate = await this.merchantVariableRepository.create(variable);
//       var id = infoCreate.mv_id == undefined ? "0" : infoCreate.mv_id;
//       var Zero = "0000"
//       var ColumnId = "C" + Zero.substring(id.toString().length, 4) + id.toString();
//       var infoUpdate = await this.merchantVariableRepository.updateById(infoCreate.mv_id, {
//         mv_code: ColumnId,
//         mv_at_create: ControllerConfig.onCurrentTime().toString()
//       });
//       result.success = true;
//       result.message = "Success, create merchant variable.";
//       result.data = infoUpdate;
//     } catch (error) {
//       result.success = false;
//       result.message = error;
//     }
//     return result;
//   }

//   @put('/non-digipos-variable/update/{id}', {
//     responses: {
//       '200': {
//         description: 'Merchan Variable model instance',
//         content: {'application/json': {schema: getModelSchemaRef(MerchantVariable)}},
//       },
//     },
//   })
//   async updateById(
//     @param.path.number('id') id: number,
//     @requestBody({
//       content: {
//         'application/json': {
//           schema: getModelSchemaRef(MerchantVariable, {
//             title: 'UpdateMerchanVariable',
//             exclude: ['mv_id', 'mv_code', 'mv_at_create', 'mv_at_update'],
//           }),
//         },
//       },
//     }) product: any,
//   ): Promise<Object> {
//     var result: any; result = {success: false, message: "", data: {}};
//     try {
//       var dataUpdate: any; dataUpdate = {};
//       for (var i in product) {
//         dataUpdate[i] = product[i];
//       }
//       dataUpdate["mv_at_update"] = ControllerConfig.onCurrentTime().toString();
//       var updateProduct = await this.merchantVariableRepository.updateById(id, dataUpdate);
//       result.success = true;
//       result.message = "Success, update merchant variable.";
//       result.data = updateProduct;
//     } catch (error) {
//       result.success = false;
//       result.message = error;
//     }
//     return result;
//   }

//   @del('/non-digipos-variable/delete/{id}', {
//     responses: {
//       '200': {
//         description: 'Merchant Variable DELETE success',
//       },
//     },
//   })
//   async deleteById(@param.path.number('id') id: number): Promise<Object> {
//     var result: any; result = {success: false, message: ""};
//     await this.merchantVariableRepository.deleteById(id);
//     result.success = true;
//     result.message = "Success, delete  merchant variable.";
//     return result;
//   }


// }
