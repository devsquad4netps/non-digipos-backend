import {authenticate} from '@loopback/authentication';
import {inject} from '@loopback/core';
import {Filter, repository} from '@loopback/repository';
import {get, getModelSchemaRef, param, post, requestBody, ResponseObject} from '@loopback/rest';
import {SecurityBindings, securityId, UserProfile} from '@loopback/security';
import ejs from "ejs";
import fs from 'fs';
import pdf from "html-pdf";
import path from 'path';
import {MerchantProductFeedback, MerchantProductMaster} from '../models';
import {InvoiceTypeRepository, MerchantFinaryaPicRepository, MerchantProductDatastoreRepository, MerchantProductDistribusiRepository, MerchantProductDocumentRepository, MerchantProductFeedbackRepository, MerchantProductGroupRepository, MerchantProductMasterRepository, MerchantProductSkemaRepository, MerchantProductSummaryGroupRepository, MerchantProductSummaryRepository, MerchantProductTribeRepository, MerchantProductVariableRepository, MpvTypeKeyRepository, MpvTypeRepository, NoFakturPajakRepository, SycronizeProcessLogRepository, ValidasiEvalScriptRepository} from '../repositories';
import {MerchantProductRepository} from '../repositories/merchant-product.repository';
import {MerchantRepository} from '../repositories/merchant.repository';
import {UserRepository} from '../repositories/user.repository';
import {ControllerConfig} from './controller.config';
import {MerchantTemplateDoc} from './merchant-template-doc';
import {MerchantTemplateDocInv} from './merchant-template-doc-inv';
var createHTML = require('create-html');

export const DistribusiDocRequestBody: ResponseObject = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ["MPM_ID", "CC_MAIL", "NOTE", "BAR", "BAR_SIGN", "INVOICE", "INVOICE_SIGN", "FAKTUR", "BUKPOT", "OTHERS"],
        properties: {
          MPM_ID: {
            type: 'string',
          },
          CC_MAIL: {
            type: 'string'
          },
          NOTE: {
            type: 'string'
          },
          BAR: {
            type: 'number'
          },
          BAR_SIGN: {
            type: 'number'
          },
          INVOICE: {
            type: 'number'
          },
          INVOICE_SIGN: {
            type: 'number'
          },
          FAKTUR: {
            type: 'number'
          },
          BUKPOT: {
            type: 'number'
          },
          OTHERS: {
            type: 'number'
          }
        }
      }
    },
  },
};



export const MPM_ID_RequestBody: ResponseObject = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ["MPM_ID"],
        properties: {
          MPM_ID: {
            type: 'array',
            items: {type: 'string'}
          },
        }
      }
    },
  },
};

export const RejectedTRXRequestBody: ResponseObject = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ["MPM_ID", "NOTE"],
        properties: {
          MPM_ID: {
            type: 'array',
            items: {type: 'string'}
          },
          NOTE: {
            type: 'string',
          }
        }
      }
    },
  },
};


export const DistributeDocCheckingRequestBody: ResponseObject = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ["MPM_ID", "DISTRIBUTE_MERCHANT", "FEEDBACK_MERCHANT"],
        properties: {
          MPM_ID: {
            type: 'array',
            items: {type: 'string'}
          },
          DISTRIBUTE_MERCHANT: {
            type: 'number',
          },
          FEEDBACK_MERCHANT: {
            type: 'number',
          }
        }
      }
    },
  },
};

export const DistributeDocRequestBody: ResponseObject = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ["MPM_ID", "DISTRIBUTE_MERCHANT", "FEEDBACK_MERCHANT", "DOC_DISTRIBUTE", "NOTE", "SEND_CC"],
        properties: {
          MPM_ID: {
            type: 'array',
            items: {type: 'string'}
          },
          DISTRIBUTE_MERCHANT: {
            type: 'number',
          },
          FEEDBACK_MERCHANT: {
            type: 'number',
          },
          DOC_DISTRIBUTE: {
            type: 'array',
            items: {type: 'number'}
          },
          NOTE: {
            type: 'string',
          },
          SEND_CC: {
            type: 'string',
          },
        }
      }
    },
  },
};



export const GenerateNumberInvRequestBody: ResponseObject = {
  description: 'The input of generate number invoice function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ["MPM_ID", "LABEL", "LAST_NUMBER"],
        properties: {
          MPM_ID: {
            type: 'array',
            items: {type: 'string'}
          },
          LABEL: {
            type: 'string',
          },
          LAST_NUMBER: {
            type: 'number',
          }
        }
      }
    },
  },
};

export const FeedbackDefaultRequestBody: ResponseObject = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ["MPM_ID", "MESSAGE"],
        properties: {
          MPM_ID: {
            type: 'string',
          },
          MESSAGE: {
            type: 'string',
          },
        }
      }
    },
  },
};

export const GenerateNumberBARRequestBody: ResponseObject = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ["MPM_ID", "LAST_NUMBER", "LABEL"],
        properties: {
          MPM_ID: {
            type: 'array',
            items: {type: 'string'}
          },
          START_NUMBER: {
            type: 'number',
          },
          LABEL: {
            type: 'string',
          },
        }
      }
    },
  },
};


var STORE_GENERATE_BAR: any; STORE_GENERATE_BAR = {};
var results: any; results = [];
var CHARACTER_STRING: any; CHARACTER_STRING = "";


var STORE_GENERATE_INVOICE: any; STORE_GENERATE_INVOICE = {};

var STORE_DISTRIBUTE: any; STORE_DISTRIBUTE = {};

var getFromBetween = {
  results: results,
  CHARACTER_STRING: "",
  getFromBetween: function (sub1: any, sub2: any) {
    if (this.CHARACTER_STRING.indexOf(sub1) < 0 || this.CHARACTER_STRING.indexOf(sub2) < 0) return false;
    var SP = this.CHARACTER_STRING.indexOf(sub1) + sub1.length;
    var string1 = this.CHARACTER_STRING.substring(0, SP);
    var string2 = this.CHARACTER_STRING.substring(SP);
    var TP = string1.length + string2.indexOf(sub2);
    return this.CHARACTER_STRING.substring(SP, TP);
  },
  removeFromBetween: function (sub1: any, sub2: any) {
    if (this.CHARACTER_STRING.indexOf(sub1) < 0 || this.CHARACTER_STRING.indexOf(sub2) < 0) return false;
    var removal = sub1 + this.getFromBetween(sub1, sub2) + sub2;
    this.CHARACTER_STRING = this.CHARACTER_STRING.replace(removal, "");
  },
  getAllResults: function (sub1: any, sub2: any) {
    // first check to see if we do have both substrings
    if (this.CHARACTER_STRING.indexOf(sub1) < 0 || this.CHARACTER_STRING.indexOf(sub2) < 0) return;

    // find one result
    var result = this.getFromBetween(sub1, sub2);
    // push it to the results array
    this.results.push(result);
    // remove the most recently found one from the string
    this.removeFromBetween(sub1, sub2);

    // if there's more substrings
    if (this.CHARACTER_STRING.indexOf(sub1) > -1 && this.CHARACTER_STRING.indexOf(sub2) > -1) {
      this.getAllResults(sub1, sub2);
    }
    else return;
  },
  get: function (CHARACTER_STRING: string, sub1: any, sub2: any) {
    this.results = [];
    this.CHARACTER_STRING = CHARACTER_STRING;
    this.getAllResults(sub1, sub2);
    return this.results;
  }
};
var thisContent: any;

@authenticate('jwt')
export class MerchantTransaksiMasterActionController {
  constructor(
    @repository(MerchantRepository)
    public merchantRepository: MerchantRepository,
    @repository(MerchantProductRepository)
    public merchantProductRepository: MerchantProductRepository,
    @repository(MerchantProductGroupRepository)
    public merchantProductGroupRepository: MerchantProductGroupRepository,
    @repository(MerchantProductVariableRepository)
    public merchantProductVariableRepository: MerchantProductVariableRepository,
    @repository(MerchantProductDatastoreRepository)
    public merchantProductDatastoreRepository: MerchantProductDatastoreRepository,
    @repository(MerchantProductSummaryRepository)
    public merchantProductSummaryRepository: MerchantProductSummaryRepository,
    @repository(MpvTypeRepository)
    public mpvTypeRepository: MpvTypeRepository,
    @repository(MerchantProductMasterRepository)
    public merchantProductMasterRepository: MerchantProductMasterRepository,
    @repository(MpvTypeKeyRepository)
    public mpvTypeKeyRepository: MpvTypeKeyRepository,
    @repository(SycronizeProcessLogRepository)
    public sycronizeProcessLogRepository: SycronizeProcessLogRepository,
    @repository(MerchantProductFeedbackRepository)
    public merchantProductFeedbackRepository: MerchantProductFeedbackRepository,
    @repository(MerchantProductSkemaRepository)
    public merchantProductSkemaRepository: MerchantProductSkemaRepository,
    @repository(MerchantProductDocumentRepository)
    public merchantProductDocumentRepository: MerchantProductDocumentRepository,
    @repository(MerchantProductTribeRepository)
    public merchantProductTribeRepository: MerchantProductTribeRepository,
    @repository(ValidasiEvalScriptRepository)
    public validasiEvalScriptRepository: ValidasiEvalScriptRepository,
    @repository(MerchantFinaryaPicRepository)
    public merchantFinaryaPicRepository: MerchantFinaryaPicRepository,
    @repository(MerchantProductDistribusiRepository)
    public merchantProductDistribusiRepository: MerchantProductDistribusiRepository,
    @repository(MerchantProductSummaryGroupRepository)
    public merchantProductSummaryGroupRepository: MerchantProductSummaryGroupRepository,
    @repository(NoFakturPajakRepository)
    public noFakturPajakRepository: NoFakturPajakRepository,
    @repository(InvoiceTypeRepository)
    public invoiceTypeRepository: InvoiceTypeRepository,
    @inject(SecurityBindings.USER)
    public currentUserProfile: UserProfile,
    @repository(UserRepository)
    public userRepository: UserRepository,
  ) {
    thisContent = this;
  }

  NUMBER_TERBILANG = (ANGKA: number) => {
    return ControllerConfig.onAngkaTerbilang(ANGKA);
  }

  NUMBER_SPARATOR = (ANGKA: number) => {
    return ControllerConfig.onGeneratePuluhan(ANGKA);
  }



  EXE_GENERATE_BAR = async () => {
    var USER_ID = this.currentUserProfile[securityId];
    for (var MPM_ID in STORE_GENERATE_BAR) {
      var INFO_MPM = STORE_GENERATE_BAR[MPM_ID];
      STORE_GENERATE_BAR[MPM_ID].STATE = 1;

      var MASTER_DATA = await this.merchantProductMasterRepository.findById(MPM_ID);

      var SUMMARY_DATA = await this.merchantProductSummaryRepository.find({where: {and: [{MPM_ID: MASTER_DATA.MPM_ID}]}});
      var SUMMARY_DATA_GROUP = await this.merchantProductSummaryGroupRepository.find({where: {and: [{MPM_ID: MASTER_DATA.MPM_ID}]}});

      var MERCHANT_PRODUCT = await this.merchantProductRepository.findById(MASTER_DATA.MP_ID);
      var MERCHANT = await this.merchantRepository.findById(MASTER_DATA.M_ID);
      var MERCHANT_SKEMA = await this.merchantProductSkemaRepository.findById(MERCHANT_PRODUCT.MP_TYPE_SKEMA);
      var MERCHANT_TRIBE = await this.merchantProductTribeRepository.findById(MERCHANT_PRODUCT.MPT_ID);
      var DATE_BAR_DOC_DEFAULT = ControllerConfig.onDateNow();
      var PIC_FINARYA = await this.merchantFinaryaPicRepository.findOne({where: {and: [{AT_FLAG: 1}, {PIC_TYPE: 1}]}});
      var INVOICE_TYPE = await this.invoiceTypeRepository.find();

      var MERCHANT_INVOICE_TYPE: any; MERCHANT_INVOICE_TYPE = {};
      for (var I_INV_TYPE in INVOICE_TYPE) {
        var ID = INVOICE_TYPE[I_INV_TYPE].ID == undefined ? -1 : INVOICE_TYPE[I_INV_TYPE].ID;
        if (MERCHANT_INVOICE_TYPE[ID == undefined ? -1 : ID] == undefined) {
          MERCHANT_INVOICE_TYPE[ID == undefined ? -1 : ID] = {};
        }
        MERCHANT_INVOICE_TYPE[ID == undefined ? -1 : ID] = INVOICE_TYPE[I_INV_TYPE];
      }

      var MERCHANT_GROUP: any; MERCHANT_GROUP = {};
      for (var I_SM in SUMMARY_DATA) {
        var RAW_SUMMARY = SUMMARY_DATA[I_SM];
        var RAW_MPG = await this.merchantProductGroupRepository.findById(RAW_SUMMARY.MPG_ID);
        var MPG_ID = RAW_MPG.MPG_ID == undefined || RAW_MPG.MPG_ID == null ? "-1" : RAW_MPG.MPG_ID;
        if (MERCHANT_GROUP[MPG_ID] == undefined) {
          MERCHANT_GROUP[MPG_ID] = {};
        }
        MERCHANT_GROUP[MPG_ID] = RAW_MPG;
      }

      var DESIGN_HTML = '<div id="pageHeader">';
      // if (MASTER_DATA.GENERATE_BAR_SIGN != 1) {
      //   DESIGN_HTML += "<div class='draft-icon'><p>DRAFT</p></div>";
      // }
      DESIGN_HTML += '</div>';
      DESIGN_HTML += '<div id="pageFooter"></div>';



      DESIGN_HTML += "<div class='header_doc'>";
      DESIGN_HTML += "  <div class='title_doc'>BERITA ACARA REKONSILIASI</div>";
      DESIGN_HTML += "  <div>No. BA: " + MASTER_DATA.MPM_NO_BAR + " - " + MERCHANT_TRIBE.MPT_LABEL + "</div>";
      DESIGN_HTML += "</div>";

      DESIGN_HTML += '<div class="content-bar">';
      // if (MASTER_DATA.IS_SELISIH != 0) {

      // } else {
      // DESIGN_HTML += MERCHANT_SKEMA.MPS_PATH_TEMPLATE;
      switch (MERCHANT_SKEMA.MPS_PATH_TEMPLATE) {
        case "TEMPLATE_DOC_BAR_1_DEFAULT":
          DESIGN_HTML += MerchantTemplateDoc.TEMPLATE_DOC_BAR_1_DEFAULT(PIC_FINARYA, MERCHANT, MERCHANT_PRODUCT, MERCHANT_SKEMA, MERCHANT_TRIBE, MERCHANT_GROUP, MASTER_DATA, SUMMARY_DATA, MERCHANT_INVOICE_TYPE);
          break;
        case "TEMPLATE_DOC_BAR_2_DEFAULT":
          DESIGN_HTML += MerchantTemplateDoc.TEMPLATE_DOC_BAR_2_DEFAULT(PIC_FINARYA, MERCHANT, MERCHANT_PRODUCT, MERCHANT_SKEMA, MERCHANT_TRIBE, MERCHANT_GROUP, MASTER_DATA, SUMMARY_DATA, MERCHANT_INVOICE_TYPE);
          break;
        case "TEMPLATE_DOC_BAR_3_DEFAULT":
          DESIGN_HTML += MerchantTemplateDoc.TEMPLATE_DOC_BAR_3_DEFAULT(PIC_FINARYA, MERCHANT, MERCHANT_PRODUCT, MERCHANT_SKEMA, MERCHANT_TRIBE, MERCHANT_GROUP, MASTER_DATA, SUMMARY_DATA, MERCHANT_INVOICE_TYPE);
          break;
        default:
          DESIGN_HTML += MerchantTemplateDoc.TEMPLATE_DOC_BAR_1_DEFAULT(PIC_FINARYA, MERCHANT, MERCHANT_PRODUCT, MERCHANT_SKEMA, MERCHANT_TRIBE, MERCHANT_GROUP, MASTER_DATA, SUMMARY_DATA, MERCHANT_INVOICE_TYPE);
          break;
      }
      DESIGN_HTML += '</div>';

      var LAST_HTML = '<div class="content">';
      LAST_HTML += DESIGN_HTML;
      LAST_HTML += '<div class="page-break"></div>';
      LAST_HTML += DESIGN_HTML;
      LAST_HTML += '<div class="page-break"></div>';
      LAST_HTML += '    <div style="text-align:center;font-size:12px;margin-bottom:20px;"><b><u>LAMPIRAN</u></b></div>';
      LAST_HTML += '    <table class="table-trx bordered" style="width:50%;margin-left:auto;margin-right:auto;">';
      LAST_HTML += '      <thead>';
      LAST_HTML += '        <tr>';
      LAST_HTML += '          <td>LABEL</td>';
      LAST_HTML += '          <td>TOTAL TRX</td>';
      LAST_HTML += '          <td>AMOUNT TRX</td>';
      LAST_HTML += '          <td>AMOUNT MDR</td>';
      LAST_HTML += '        </tr>';
      LAST_HTML += '      </thead>';
      LAST_HTML += '      <tbody>';

      var TOTAL_COUNT = 0;
      var TOTAL_AMOUNT = 0;
      var TOTAL_MDR = 0;
      for (var I_SDG in SUMMARY_DATA_GROUP) {
        var RAW_SDG = SUMMARY_DATA_GROUP[I_SDG];

        TOTAL_COUNT += RAW_SDG.APP_COUNT_TRX;
        TOTAL_AMOUNT += RAW_SDG.APP_AMOUNT;
        TOTAL_MDR += RAW_SDG.APP_MDR;

        LAST_HTML += '      <tr>';
        LAST_HTML += '        <td>' + RAW_SDG.MPS_LABEL + '</td>';
        LAST_HTML += '        <td>' + ControllerConfig.onGeneratePuluhan(RAW_SDG.APP_COUNT_TRX) + '</td>';
        LAST_HTML += '        <td>Rp. ' + ControllerConfig.onGeneratePuluhan(RAW_SDG.APP_AMOUNT) + '</td>';
        LAST_HTML += '        <td>Rp. ' + ControllerConfig.onGeneratePuluhan(RAW_SDG.APP_MDR) + '</td>';
        LAST_HTML += '      </tr>';
      }

      LAST_HTML += '        <tr>';
      LAST_HTML += '          <td><b>TOTAL</b></td>';
      LAST_HTML += '          <td><b>' + ControllerConfig.onGeneratePuluhan(TOTAL_COUNT) + '</b></td>';
      LAST_HTML += '          <td><b>Rp. ' + ControllerConfig.onGeneratePuluhan(TOTAL_AMOUNT) + '</b></td>';
      LAST_HTML += '          <td><b>Rp. ' + ControllerConfig.onGeneratePuluhan(TOTAL_MDR) + '</b>9</td>';
      LAST_HTML += '        </tr>';

      LAST_HTML += '      </tbody>';
      LAST_HTML += '    </table>';

      LAST_HTML += '</div>';

      // }

      var html = createHTML({
        title: 'PDF BAR ' + MERCHANT_PRODUCT.MP_BRAND + " " + MASTER_DATA.PERIODE,
        // script: 'example.js',
        scriptAsync: true,
        // css: [path.join(__dirname, '../../.non-digipos/template-pdf/bar_doc.css')],
        lang: 'en',
        head: '<meta name="DOC_BAR_"' + MERCHANT_PRODUCT.MP_BRAND + ' content="DOC_BAR_"' + MERCHANT_PRODUCT.MP_BRAND + '>' + ControllerConfig.onStyleBAR(),
        body: LAST_HTML,
        // favicon: 'favicon.png'
      });

      //GENERATE DESIGN HTML
      fs.writeFile(path.join(__dirname, '../../.non-digipos/template-pdf/BAR_DEFAULT_' + MPM_ID + ".html"), html, async (err: any) => {
        if (err) {
          console.log(err);
        } else {
          var NUMBER_BAR_SET = MASTER_DATA.MPM_NO_BAR.toString().split("/").join(".");
          var NameFileBAR = NUMBER_BAR_SET + ".pdf";
          var pathFile = path.join(__dirname, '../../.non-digipos/document/' + MPM_ID + '/bar/') + NameFileBAR;
          var DIR_MPM = path.join(__dirname, '../../.non-digipos/document/' + MPM_ID + '/bar/');
          if (!fs.existsSync(DIR_MPM)) {
            await new Promise<object>((resolve, reject) => {
              fs.mkdir(DIR_MPM, {recursive: true}, (err) => {
                if (err) {
                  resolve({success: true, err});
                } else {
                  resolve({success: true, message: "rename success !"});
                }
              });
            });
          }
          var compiled = ejs.compile(fs.readFileSync(path.join(__dirname, '../../.non-digipos/template-pdf/BAR_DEFAULT_' + MPM_ID + ".html"), 'utf8'));
          var html = compiled({});
          var config: any; config = {
            orientation: "portrait",
            "header": {
              "height": "30px",
              "contents": ""
            },
            "footer": {
              "height": "30px",
              "contents": ''
            }
          }
          pdf.create(html, config).toFile(pathFile, async (errGenerateBAR: any, res: any) => {
            if (errGenerateBAR) {
              console.log(errGenerateBAR);
            } else {
              var DOCUMENT_STORE = await this.merchantProductDocumentRepository.findOne({
                where: {
                  and: [
                    {MPDOC_NAME: NameFileBAR}, {MPDOC_TYPE: 1}
                  ]
                }
              });

              if (DOCUMENT_STORE != null) {
                var DOC_ID = DOCUMENT_STORE.MPDOC_ID == undefined || DOCUMENT_STORE.MPDOC_ID == null ? "" : DOCUMENT_STORE.MPDOC_ID;
                if (DOC_ID != "") {
                  try {
                    await this.merchantProductDocumentRepository.updateAll({
                      AT_FLAG: 0
                    }, {
                      and: [
                        {MPDOC_TYPE: 1},
                        {MPM_ID: MPM_ID}
                      ]
                    });
                    await this.merchantProductDocumentRepository.updateById(DOC_ID, {
                      AT_FLAG: 1,
                      AT_CREATE: ControllerConfig.onCurrentTime().toString(),
                      AT_UPDATE: ControllerConfig.onCurrentTime().toString(),
                      MPDOC_NAME: NameFileBAR,
                      MPDOC_PATH: pathFile
                    });

                    await this.merchantProductMasterRepository.updateById(MPM_ID, {
                      GENERATE_BAR: 1,
                      AT_UPDATE: ControllerConfig.onCurrentTime().toString(),
                    });

                  } catch (errorUpdateBAR) {
                    console.log(errorUpdateBAR);
                  }
                }
              } else {
                try {

                  await this.merchantProductDocumentRepository.create({
                    MPDOC_NAME: NameFileBAR,
                    MPDOC_PATH: pathFile,
                    MPDOC_TYPE: 1,
                    MPM_ID: MPM_ID,
                    START_TIME_RUN: MASTER_DATA.START_TIME_RUN,
                    AT_CREATE: ControllerConfig.onCurrentTime().toString(),
                    AT_UPDATE: undefined,
                    AT_FLAG: 1,
                    AT_WHO: USER_ID
                  });

                  await this.merchantProductMasterRepository.updateById(MPM_ID, {
                    GENERATE_BAR: 1,
                    AT_UPDATE: ControllerConfig.onCurrentTime().toString()
                  });
                } catch (errorCreateBar) {
                  console.log(errorCreateBar);
                }
              }
              console.log("SUCCESS GENERATE");
            }
            fs.unlink(path.join(__dirname, '../../.non-digipos/template-pdf/BAR_DEFAULT_' + MPM_ID + ".html"), (errorDelete: any) => {
              if (errorDelete) {
                console.log(errorDelete);
              }
              delete STORE_GENERATE_BAR[MPM_ID];
              this.EXE_GENERATE_BAR();
            });
          });
        }
      });

      break;
    }
  }

  EXE_GENERATE_INVOICE = async () => {
    var USER_ID = this.currentUserProfile[securityId];
    for (var MPM_ID in STORE_GENERATE_INVOICE) {
      var INFO_MPM = STORE_GENERATE_INVOICE[MPM_ID];
      STORE_GENERATE_INVOICE[MPM_ID].STATE = 1;

      var MASTER_DATA = await this.merchantProductMasterRepository.findById(MPM_ID);
      // var SUMMARY_ID = JSON.parse(MASTER_DATA.START_TIME_RUN);
      var SUMMARY_DATA = await this.merchantProductSummaryRepository.find({where: {and: [{MPM_ID: MASTER_DATA.MPM_ID}]}});
      var MERCHANT_PRODUCT = await this.merchantProductRepository.findById(MASTER_DATA.MP_ID);
      var MERCHANT = await this.merchantRepository.findById(MASTER_DATA.M_ID);
      var MERCHANT_SKEMA = await this.merchantProductSkemaRepository.findById(MERCHANT_PRODUCT.MP_TYPE_SKEMA);
      var MERCHANT_TRIBE = await this.merchantProductTribeRepository.findById(MERCHANT_PRODUCT.MPT_ID);
      var DATE_BAR_DOC_DEFAULT = ControllerConfig.onDateNow();
      var PIC_FINARYA = await this.merchantFinaryaPicRepository.findOne({where: {and: [{AT_FLAG: 1}, {PIC_TYPE: 2}]}});
      var INVOICE_TYPE = await this.invoiceTypeRepository.find();

      var MERCHANT_INVOICE_TYPE: any; MERCHANT_INVOICE_TYPE = {};
      for (var I_INV_TYPE in INVOICE_TYPE) {
        var ID = INVOICE_TYPE[I_INV_TYPE].ID == undefined ? -1 : INVOICE_TYPE[I_INV_TYPE].ID;
        if (MERCHANT_INVOICE_TYPE[ID == undefined ? -1 : ID] == undefined) {
          MERCHANT_INVOICE_TYPE[ID == undefined ? -1 : ID] = {};
        }
        MERCHANT_INVOICE_TYPE[ID == undefined ? -1 : ID] = INVOICE_TYPE[I_INV_TYPE];
      }

      var MERCHANT_GROUP: any; MERCHANT_GROUP = {};
      for (var I_SM in SUMMARY_DATA) {
        var RAW_SUMMARY = SUMMARY_DATA[I_SM];
        var RAW_MPG = await this.merchantProductGroupRepository.findById(RAW_SUMMARY.MPG_ID);
        var MPG_ID = RAW_MPG.MPG_ID == undefined || RAW_MPG.MPG_ID == null ? "-1" : RAW_MPG.MPG_ID;
        if (MERCHANT_GROUP[MPG_ID] == undefined) {
          MERCHANT_GROUP[MPG_ID] = {};
        }
        MERCHANT_GROUP[MPG_ID] = RAW_MPG;
      }

      const HEADER_INVOICE_PATH = path.join(__dirname, '../../.non-digipos/assets/inv_header.png');
      const HEADER_INVOICE = fs.readFileSync(HEADER_INVOICE_PATH, {encoding: 'base64'});

      const FOOTER_INVOICE_PATH = path.join(__dirname, '../../.non-digipos/assets/inv_footer.png');
      const FOOTER_INVOICE = fs.readFileSync(FOOTER_INVOICE_PATH, {encoding: 'base64'});

      var DESIGN_HTML = '<div id="pageHeader">';
      DESIGN_HTML += "  <div class='header-invoice'>";
      DESIGN_HTML += '    <img src="data:image/png;base64,' + HEADER_INVOICE + '"   />';
      DESIGN_HTML += "  </div>";
      DESIGN_HTML += '</div>';
      DESIGN_HTML += '<div id="pageFooter">';
      DESIGN_HTML += "  <div class='footer-invoice'>";
      DESIGN_HTML += "    <div class='footer-pt' style='margin-top:10px;'>";
      DESIGN_HTML += PIC_FINARYA?.PT_NAME;
      DESIGN_HTML += "    </div>";
      DESIGN_HTML += "    <div class='footer-address'>";
      DESIGN_HTML += PIC_FINARYA?.PT_ALAMAT;
      DESIGN_HTML += "    </div>";
      DESIGN_HTML += '    <img src="data:image/png;base64,' + FOOTER_INVOICE + '"  />';
      DESIGN_HTML += "  </div>";
      DESIGN_HTML += '</div>';



      DESIGN_HTML += "<div class='header_doc'>";
      DESIGN_HTML += "<table>";
      DESIGN_HTML += "  <tr>";
      DESIGN_HTML += "    <td width='50%'>TO : </td>";
      DESIGN_HTML += "    <td width='50%' style='padding-left:30px;'>TO : </td>";
      DESIGN_HTML += "  </tr>";
      DESIGN_HTML += "  <tr>";
      DESIGN_HTML += "    <td><b>" + MERCHANT.M_LEGAL_NAME + "</b></td>";
      DESIGN_HTML += "    <td style='padding-left:30px;'><b>" + PIC_FINARYA?.PT_NAME + "</b></td>";
      DESIGN_HTML += "  </tr>";
      DESIGN_HTML += "  <tr>";
      DESIGN_HTML += "    <td class='address-inv' >" + MERCHANT.M_ALAMAT_NPWP + "</td>";
      DESIGN_HTML += "    <td class='address-inv' style='padding-left:30px;'>" + PIC_FINARYA?.PT_ALAMAT + "</td>";
      DESIGN_HTML += "  </tr>";
      DESIGN_HTML += "  <tr>";
      DESIGN_HTML += "    <td><b>NPWP : </b></td>";
      DESIGN_HTML += "    <td style='padding-left:30px;'><b>NPWP : </b></td>";
      DESIGN_HTML += "  </tr>";
      DESIGN_HTML += "  <tr>";
      DESIGN_HTML += "    <td>" + MERCHANT.M_NPWP + "</td>";
      DESIGN_HTML += "    <td style='padding-left:30px;'>" + PIC_FINARYA?.PT_NPWP + "</td>";
      DESIGN_HTML += "  </tr>";
      DESIGN_HTML += "</table>";
      DESIGN_HTML += "</div>";

      DESIGN_HTML += '<div class="content-inv">';
      // if (MASTER_DATA.IS_SELISIH != 0) {

      // } else {
      // DESIGN_HTML += MERCHANT_SKEMA.MPS_PATH_TEMPLATE;
      switch (MERCHANT_PRODUCT.MP_TYPE_INVOICE) {
        case 1:
          DESIGN_HTML += MerchantTemplateDocInv.TEMPLATE_DOC_PAID(PIC_FINARYA, MERCHANT, MERCHANT_PRODUCT, MERCHANT_SKEMA, MERCHANT_TRIBE, MERCHANT_GROUP, MASTER_DATA, SUMMARY_DATA, MERCHANT_INVOICE_TYPE);
          break;
        case 2:
          DESIGN_HTML += MerchantTemplateDocInv.TEMPLATE_DOC_REGULAR(PIC_FINARYA, MERCHANT, MERCHANT_PRODUCT, MERCHANT_SKEMA, MERCHANT_TRIBE, MERCHANT_GROUP, MASTER_DATA, SUMMARY_DATA, MERCHANT_INVOICE_TYPE);
          break;
        default:
          DESIGN_HTML += MerchantTemplateDocInv.TEMPLATE_DOC_PAID(PIC_FINARYA, MERCHANT, MERCHANT_PRODUCT, MERCHANT_SKEMA, MERCHANT_TRIBE, MERCHANT_GROUP, MASTER_DATA, SUMMARY_DATA, MERCHANT_INVOICE_TYPE);
          break;
      }
      DESIGN_HTML += '</div>';

      var LAST_HTML = '<div class="content">';
      LAST_HTML += DESIGN_HTML;
      LAST_HTML += '</div>';

      // }

      var html = createHTML({
        title: 'PDF INVOICE ' + MERCHANT_PRODUCT.MP_BRAND + " " + MASTER_DATA.PERIODE,
        // script: 'example.js',
        scriptAsync: true,
        // css: [path.join(__dirname, '../../.non-digipos/template-pdf/bar_doc.css')],
        lang: 'en',
        head: '<meta name="DOC_INVOICE_"' + MERCHANT_PRODUCT.MP_BRAND + ' content="DOC_INVOICE_"' + MERCHANT_PRODUCT.MP_BRAND + '>' + ControllerConfig.onStyleINVOICE(),
        body: LAST_HTML,
        // favicon: 'favicon.png'
      });

      //GENERATE DESIGN HTML
      fs.writeFile(path.join(__dirname, '../../.non-digipos/template-pdf/INVOICE_DEFAULT_' + MPM_ID + ".html"), html, async (err: any) => {
        if (err) {
          console.log(err);
        } else {

          var NameFileINV = "DOC_INV_" + MASTER_DATA.MPM_NO_INVOICE + ".pdf";
          var pathFile = path.join(__dirname, '../../.non-digipos/document/' + MPM_ID + '/invoice/') + NameFileINV;
          var DIR_MPM = path.join(__dirname, '../../.non-digipos/document/' + MPM_ID + '/invoice/');
          if (!fs.existsSync(DIR_MPM)) {
            await new Promise<object>((resolve, reject) => {
              fs.mkdir(DIR_MPM, {recursive: true}, (err) => {
                if (err) {
                  resolve({success: true, err});
                } else {
                  resolve({success: true, message: "rename success !"});
                }
              });
            });
          }

          var compiled = ejs.compile(fs.readFileSync(path.join(__dirname, '../../.non-digipos/template-pdf/INVOICE_DEFAULT_' + MPM_ID + ".html"), 'utf8'));
          var html = compiled({});
          var config: any; config = {
            orientation: "portrait",
            "header": {
              "height": "65px",
              "contents": ""
            },
            "footer": {
              "height": "65px",
              "contents": ''
            }
          }
          pdf.create(html, config).toFile(pathFile, async (errGenerateBAR: any, res: any) => {
            if (errGenerateBAR) {
              console.log(errGenerateBAR);
            } else {
              var DOCUMENT_STORE = await this.merchantProductDocumentRepository.findOne({
                where: {
                  and: [
                    {MPDOC_NAME: NameFileINV}, {MPDOC_TYPE: 3}
                  ]
                }
              });

              if (DOCUMENT_STORE != null) {
                var DOC_ID = DOCUMENT_STORE.MPDOC_ID == undefined || DOCUMENT_STORE.MPDOC_ID == null ? "" : DOCUMENT_STORE.MPDOC_ID;
                if (DOC_ID != "") {
                  try {
                    await this.merchantProductDocumentRepository.updateAll({
                      AT_FLAG: 0
                    }, {
                      and: [
                        {MPDOC_TYPE: 3},
                        {MPM_ID: MPM_ID}
                      ]
                    });
                    await this.merchantProductDocumentRepository.updateById(DOC_ID, {
                      AT_FLAG: 1,
                      AT_CREATE: ControllerConfig.onCurrentTime().toString(),
                      AT_UPDATE: ControllerConfig.onCurrentTime().toString(),
                      MPDOC_NAME: NameFileINV,
                      MPDOC_PATH: pathFile
                    });

                    await this.merchantProductMasterRepository.updateById(MPM_ID, {
                      GENERATE_INVOICE: 1,
                      AT_UPDATE: ControllerConfig.onCurrentTime().toString(),
                    });

                  } catch (errorUpdateBAR) {
                    console.log(errorUpdateBAR);
                  }
                }
              } else {
                try {

                  await this.merchantProductDocumentRepository.create({
                    MPDOC_NAME: NameFileINV,
                    MPDOC_PATH: pathFile,
                    MPDOC_TYPE: 3,
                    MPM_ID: MPM_ID,
                    START_TIME_RUN: MASTER_DATA.START_TIME_RUN,
                    AT_CREATE: ControllerConfig.onCurrentTime().toString(),
                    AT_UPDATE: undefined,
                    AT_FLAG: 1,
                    AT_WHO: USER_ID
                  });

                  await this.merchantProductMasterRepository.updateById(MPM_ID, {
                    GENERATE_INVOICE: 1,
                    AT_UPDATE: ControllerConfig.onCurrentTime().toString()
                  });
                } catch (errorCreateBar) {
                  console.log(errorCreateBar);
                }
              }
              console.log("SUCCESS GENERATE INVOICE");
            }
            fs.unlink(path.join(__dirname, '../../.non-digipos/template-pdf/INVOICE_DEFAULT_' + MPM_ID + ".html"), (errorDelete: any) => {
              if (errorDelete) {
                console.log(errorDelete);
              }
              delete STORE_GENERATE_INVOICE[MPM_ID];
              this.EXE_GENERATE_INVOICE();
            });
          });
        }
      });

      break;
    }
  }

  @get('/merchant-transaksi-master-action/views', {
    responses: {
      '200': {
        description: 'Array of Merchant Product model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(MerchantProductMaster, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async findMerchantTransaksiMaster(
    @param.filter(MerchantProductMaster) filter?: Filter<MerchantProductMaster>,
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};


    var FILTER_SET: any; FILTER_SET = {};
    var FIXFILTER: any; FIXFILTER = filter;
    for (var IFilter in FIXFILTER) {
      FILTER_SET[IFilter] = FIXFILTER[IFilter];
    }

    if (FILTER_SET.order == undefined) {
      FILTER_SET.order = "AT_CREATE ASC";
    }
    var TRANSAKSI_MASTER = await this.merchantProductMasterRepository.find(FILTER_SET);
    var DATA_MASTER: any; DATA_MASTER = [];
    for (var i in TRANSAKSI_MASTER) {
      var RAW_MASTER: any; RAW_MASTER = TRANSAKSI_MASTER[i];

      var DATA: any; DATA = {};
      var SKIP_FIELD: any; SKIP_FIELD = ["AT_WHO", "START_TIME_RUN"];
      for (var F_MASTER in RAW_MASTER) {
        if (SKIP_FIELD.indexOf(F_MASTER) != -1) {
          continue;
        }
        DATA[F_MASTER] = RAW_MASTER[F_MASTER];
      }

      DATA["MERCHANT"] = await this.merchantRepository.findById(RAW_MASTER.M_ID, {
        fields: {
          M_ID: true,
          M_CODE: true,
          M_NAME: true,
          M_LEGAL_NAME: true,
        }
      });

      DATA["MERCHANT_PRODUCT"] = await this.merchantProductRepository.findById(RAW_MASTER.MP_ID, {
        fields: {
          MP_ID: true,
          MP_CODE: true,
          MP_BRAND: true,
        }
      });

      var NOTE_MESSAGE: any; NOTE_MESSAGE = await this.merchantProductFeedbackRepository.find({
        order: ["AT_CREATE ASC"],
        where: {
          and: [{TYPE_FEEDBACK: {inq: [RAW_MASTER.AT_POSISI]}}, {MPM_ID: RAW_MASTER.MPM_ID}]
        }
      });


      DATA["M_CODE"] = DATA["MERCHANT"]["M_CODE"];
      DATA["M_NAME"] = DATA["MERCHANT"]["M_NAME"];
      DATA["M_LEGAL_NAME"] = DATA["MERCHANT"]["M_LEGAL_NAME"];
      DATA["MP_CODE"] = DATA["MERCHANT_PRODUCT"]["MP_CODE"];
      DATA["MP_BRAND"] = DATA["MERCHANT_PRODUCT"]["MP_BRAND"];
      DATA["DOCUMENT"] = [];

      var DOCUMENT = await this.merchantProductDocumentRepository.find({where: {and: [{AT_FLAG: 1}, {MPM_ID: RAW_MASTER.MPM_ID}]}});
      for (var IDOC in DOCUMENT) {
        var ROW_DOC = DOCUMENT[IDOC];
        DATA["DOCUMENT"].push({
          MPDOC_ID: ROW_DOC.MPDOC_ID,
          MPDOC_NAME: ROW_DOC.MPDOC_NAME,
          MPDOC_TYPE: ROW_DOC.MPDOC_TYPE,
          AT_CREATE: ROW_DOC.AT_UPDATE == null || ROW_DOC.AT_UPDATE == "" ? ROW_DOC.AT_CREATE : ROW_DOC.AT_UPDATE
        });
      }

      DATA_MASTER.push(DATA);

    }
    result.message = "Success !";
    result.datas = DATA_MASTER;
    return result
  }


  @get('/merchant-transaksi-master-action/note/views', {
    responses: {
      '200': {
        description: 'Array of Merchant Product model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(MerchantProductFeedback, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async findMerchantTransaksiMasterNote(
    @param.query.string("MPM_ID") MPM_ID: string,
    @param.filter(MerchantProductFeedback) filter?: Filter<MerchantProductFeedback>,
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};
    var USER_INFORMATION: any; USER_INFORMATION = {};
    var USER = await this.userRepository.find();
    for (var i in USER) {
      var USER_DATA = USER[i];
      USER_INFORMATION[USER_DATA.id] = USER_DATA;
    }

    var TRANSAKSI_MASTER = await this.merchantProductMasterRepository.findOne({where: {MPM_ID: MPM_ID}});
    if (TRANSAKSI_MASTER != null) {
      if (TRANSAKSI_MASTER.AT_POSISI != undefined) {
        var NOTE_MESSAGE: any; NOTE_MESSAGE = await this.merchantProductFeedbackRepository.find({
          order: ["AT_CREATE ASC"],
          where: {
            and: [{TYPE_FEEDBACK: {inq: [TRANSAKSI_MASTER.AT_POSISI]}}, {MPM_ID: TRANSAKSI_MASTER.MPM_ID}, {AT_FLAG: 1}]
          }
        });

        var NOTE_MSG: any; NOTE_MSG = [];
        for (var IMSG in NOTE_MESSAGE) {
          var ROW_MESSAGE = NOTE_MESSAGE[IMSG];
          var DATA_MSG: any; DATA_MSG = {};
          DATA_MSG["MPF_ID"] = ROW_MESSAGE.MPF_ID;
          DATA_MSG["MPF_MESSAGE"] = ROW_MESSAGE.MPF_MESSAGE;
          DATA_MSG["AT_CREATE"] = ROW_MESSAGE.AT_CREATE;
          DATA_MSG["AT_UPDATE"] = ROW_MESSAGE.AT_UPDATE;
          DATA_MSG["AT_FLAG"] = ROW_MESSAGE.AT_FLAG;
          DATA_MSG["AT_USER"] = USER_INFORMATION[ROW_MESSAGE.AT_WHO] == undefined ? null : {
            NAME: USER_INFORMATION[ROW_MESSAGE.AT_WHO]["fullname"],
            EMAIL: USER_INFORMATION[ROW_MESSAGE.AT_WHO]["email"],
            JABATAN: USER_INFORMATION[ROW_MESSAGE.AT_WHO]["jabatan"],
            DIVISI: USER_INFORMATION[ROW_MESSAGE.AT_WHO]["divisi"],
          }
          DATA_MSG["SEND_MAIL"] = ROW_MESSAGE.SEND_FLAG == -1 ? 0 : 1;
          DATA_MSG["SEND_MAIL_INFO"] = ROW_MESSAGE.SEND_FLAG == -1 ? null : {
            SEND_TO: ROW_MESSAGE.SEND_TO,
            SEND_CC: ROW_MESSAGE.SEND_CC,
            SEND_FLAG: ROW_MESSAGE.SEND_FLAG,
            SEND_MESSAGE: ROW_MESSAGE.SEND_MESSAGE
          }

          NOTE_MSG.push(DATA_MSG);
        }
        result["datas"] = NOTE_MSG;
      }
    }
    return result;
  }


  @post('/merchant-transaksi-master-action/validation/data-master/bussines-assurace/reject', {
    responses: {
      '200': {
        description: 'Validate Data Master Bussines Assurance model instance',
        content: {
          'application/json': {
            schema: RejectedTRXRequestBody
          }
        },
      },
    },
  })
  async merchantTransaksiMasterValidasiReject(
    @requestBody(RejectedTRXRequestBody) rejectedBA: {MPM_ID: [], NOTE: string},
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: 'Success !', datas: [], LOG: {SUCCESS: [], FAILED: []}};
    var TRANSAKSI_MASTER = await this.merchantProductMasterRepository.find({
      where: {
        and: [
          {
            MPM_ID: {inq: rejectedBA.MPM_ID},
            AT_POSISI: 1
          }]
      }
    });

    var SUCCESS_REJECTED = 0;
    var VAILED_REJECTED = 0;

    for (var i in TRANSAKSI_MASTER) {
      var RAW_MASTER = TRANSAKSI_MASTER[i];
      try {
        await this.merchantProductFeedbackRepository.create({
          MPF_MESSAGE: rejectedBA.NOTE,
          MPM_ID: RAW_MASTER.MPM_ID,
          START_TIME_RUN: RAW_MASTER.START_TIME_RUN,
          AT_FLAG: 1,
          AT_CREATE: ControllerConfig.onCurrentTime().toString(),
          AT_SEND: ControllerConfig.onCurrentTime().toString(),
          AT_WHO: USER_ID,
          AT_UPDATE: undefined,
          TOKEN_KEY: "0000000001",
          TYPE_FEEDBACK: 1,
          SEND_TO: "-",
          SEND_CC: "-",
          SEND_FLAG: -1,
          SEND_MESSAGE: rejectedBA.NOTE,
          REQ_FILE_BAR: 0,
          REQ_FILE_SUMMARY: 0,
          REQ_FILE_BUKPOT: 0,
          REQ_FILE_BUKTI_BAYAR: 0,
          REQ_FILE_FAKTUR: 0,
          REQ_FILE_INVOICE: 0,
          FILE_BAR: undefined,
          FILE_BUKPOT: undefined,
          FILE_BUKTIBAYAR: undefined,
          FILE_FAKTUR: undefined,
          FILE_INVOICE: undefined,
          FILE_OTHERS: undefined,
          FILE_SUMMARY: undefined,
        });

        try {
          await this.merchantProductMasterRepository.updateById(RAW_MASTER.MPM_ID, {
            AT_FLAG: 2,
            AT_POSISI: 1,
            AT_WHO: USER_ID
          });
          RAW_MASTER.MPM_ID == undefined ? "" : result.LOG.SUCCESS.push(RAW_MASTER.MPM_ID);
          SUCCESS_REJECTED++;
          result.success = true;
          result.message = "Success rejected !";
        } catch (error) {
          VAILED_REJECTED++;
          RAW_MASTER.MPM_ID == undefined ? "" : result.LOG.FAILED.push(RAW_MASTER.MPM_ID);
          result.success = false;
          result.message = error;
        }
      } catch (error) {
        VAILED_REJECTED++;
        RAW_MASTER.MPM_ID == undefined ? "" : result.LOG.FAILED.push(RAW_MASTER.MPM_ID);
        result.success = false;
        result.message = error;
      }
    }
    result.message = "Success rejected (" + SUCCESS_REJECTED + "), vailed rejected (" + VAILED_REJECTED + ")";
    return result;
  }

  @post('/merchant-transaksi-master-action/validation/data-master/bussines-assurace/approved', {
    responses: {
      '200': {
        description: 'Validate Data Master Bussines Assurance approve master model instance',
        content: {
          'application/json': {
            schema: MPM_ID_RequestBody
          }
        },
      },
    },
  })
  async merchantTransaksiMasterValidasiAcc(
    @requestBody(MPM_ID_RequestBody) mPM_ID_RequestBody: {MPM_ID: []},
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};

    var TRANSAKSI_MASTER = await this.merchantProductMasterRepository.find({
      where: {
        and: [
          {
            MPM_ID: {inq: mPM_ID_RequestBody.MPM_ID},
            AT_POSISI: 1,
            AT_FLAG: {inq: [0, 2]}
          }]
      }
    });

    for (var i in TRANSAKSI_MASTER) {
      var RAW_MASTER = TRANSAKSI_MASTER[i];
      try {
        await this.merchantProductMasterRepository.updateById(RAW_MASTER.MPM_ID, {
          AT_UPDATE: ControllerConfig.onCurrentTime().toString(),
          AT_WHO: USER_ID,
          AT_POSISI: 2,
          AT_FLAG: 0
        });
      } catch (error) {
        result.success = false;
        result
      }
    }
    return result;
  }



  @post('/merchant-transaksi-master-action/re-generate/document-bar', {
    responses: {
      '200': {
        description: 'Approve step1 transaksi master model instance',
        content: {
          'application/json': {
            schema: MPM_ID_RequestBody
          }
        },
      },
    },
  })
  async merchantTransaksiMasterReGenerateDocBar(
    @requestBody(MPM_ID_RequestBody) generateNumberBARRequestBody: {MPM_ID: []},
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: 'Success !', datas: []};
    var TRANSAKSI_MASTER = await this.merchantProductMasterRepository.find({
      where: {
        and: [
          {
            MPM_ID: {inq: generateNumberBARRequestBody.MPM_ID},
            GENERATE_BAR: 1,
            AT_POSISI: 2,
            AT_FLAG: 0
          }]
      }
    });
    for (var i in TRANSAKSI_MASTER) {
      var RAW_MASTER = TRANSAKSI_MASTER[i];
      if (RAW_MASTER.MPM_NO_BAR == "" || RAW_MASTER.MPM_NO_BAR == null) {
        continue;
      }

      var MPM_ID = RAW_MASTER.MPM_ID == undefined ? -1 : RAW_MASTER.MPM_ID;
      if (STORE_GENERATE_BAR[MPM_ID] == undefined) {
        STORE_GENERATE_BAR[MPM_ID] = {STATE: 0, ID: MPM_ID};
      }
    }
    this.EXE_GENERATE_BAR();
    return result;
  }

  @post('/merchant-transaksi-master-action/generate/number-bar', {
    responses: {
      '200': {
        description: 'Approve step1 transaksi master model instance',
        content: {
          'application/json': {
            schema: GenerateNumberBARRequestBody
          }
        },
      },
    },
  })
  async merchantTransaksiMasterGenerateNumberBar(
    @requestBody(GenerateNumberBARRequestBody) generateNumberBARRequestBody: {MPM_ID: [], LAST_NUMBER: number, LABEL: string},
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: 'Success !', datas: []};

    var TRANSAKSI_MASTER = await this.merchantProductMasterRepository.find({
      where: {
        and: [
          {
            MPM_ID: {inq: generateNumberBARRequestBody.MPM_ID},
            GENERATE_BAR: 0,
            AT_POSISI: 2,
            AT_FLAG: {inq: [0, 1, 2]}
          }]
      }
    });

    var LABEL_BAR = generateNumberBARRequestBody.LABEL.substring(1, 1) == "/" ? generateNumberBARRequestBody.LABEL.substring(1) : generateNumberBARRequestBody.LABEL;
    var NO = (generateNumberBARRequestBody.LAST_NUMBER + 1);

    for (var i in TRANSAKSI_MASTER) {
      var RAW_MASTER = TRANSAKSI_MASTER[i];

      if (RAW_MASTER.MPM_NO_BAR != "" && RAW_MASTER.MPM_NO_BAR != null) {
        continue;
      }
      var M_CODE = "";
      var AVALIABLE_CODE: boolean; AVALIABLE_CODE = false;
      do {
        var CODE = NO + LABEL_BAR;
        var VALID_CODE = await this.merchantProductMasterRepository.findOne({where: {and: [{MPM_NO_BAR: CODE}]}});
        if (VALID_CODE == null) {
          M_CODE = CODE;
          AVALIABLE_CODE = true;
        }
        NO++;
      }
      while (AVALIABLE_CODE == false);

      try {
        await this.merchantProductMasterRepository.updateById(RAW_MASTER.MPM_ID, {
          MPM_NO_BAR: M_CODE
        });
        var MPM_ID = RAW_MASTER.MPM_ID == undefined ? -1 : RAW_MASTER.MPM_ID;
        if (STORE_GENERATE_BAR[MPM_ID] == undefined) {
          STORE_GENERATE_BAR[MPM_ID] = {STATE: 0, ID: MPM_ID};
        }
      } catch (error) {

      }
    }

    this.EXE_GENERATE_BAR();
    return result;
  }

  @post('/merchant-transaksi-master-action/validation/data-master/bussines-assurace/step-2/reject', {
    responses: {
      '200': {
        description: 'Validate Data Master Bussines Assurance model instance',
        content: {
          'application/json': {
            schema: RejectedTRXRequestBody
          }
        },
      },
    },
  })
  async merchantTransaksiMasterValidasiRejectStep2(
    @requestBody(RejectedTRXRequestBody) rejectedBA: {MPM_ID: [], NOTE: string},
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: 'Success !', datas: [], LOG: {SUCCESS: [], FAILED: []}};
    var TRANSAKSI_MASTER = await this.merchantProductMasterRepository.find({
      where: {
        and: [
          {
            MPM_ID: {inq: rejectedBA.MPM_ID},
            AT_POSISI: 2
          }]
      }
    });

    var SUCCESS_REJECTED = 0;
    var VAILED_REJECTED = 0;
    for (var i in TRANSAKSI_MASTER) {
      var RAW_MASTER = TRANSAKSI_MASTER[i];

      try {
        await this.merchantProductFeedbackRepository.create({
          MPF_MESSAGE: rejectedBA.NOTE,
          MPM_ID: RAW_MASTER.MPM_ID,
          START_TIME_RUN: RAW_MASTER.START_TIME_RUN,
          AT_FLAG: 1,
          AT_CREATE: ControllerConfig.onCurrentTime().toString(),
          AT_SEND: ControllerConfig.onCurrentTime().toString(),
          AT_WHO: USER_ID,
          AT_UPDATE: undefined,
          TOKEN_KEY: "0000000001",
          TYPE_FEEDBACK: 2,
          SEND_TO: "-",
          SEND_CC: "-",
          SEND_FLAG: -1,
          SEND_MESSAGE: rejectedBA.NOTE,
          REQ_FILE_BAR: 0,
          REQ_FILE_SUMMARY: 0,
          REQ_FILE_BUKPOT: 0,
          REQ_FILE_BUKTI_BAYAR: 0,
          REQ_FILE_FAKTUR: 0,
          REQ_FILE_INVOICE: 0,
          FILE_BAR: undefined,
          FILE_BUKPOT: undefined,
          FILE_BUKTIBAYAR: undefined,
          FILE_FAKTUR: undefined,
          FILE_INVOICE: undefined,
          FILE_OTHERS: undefined,
          FILE_SUMMARY: undefined,
        });
        try {
          await this.merchantProductMasterRepository.updateById(RAW_MASTER.MPM_ID, {
            AT_FLAG: 2,
            AT_POSISI: 1,
            AT_WHO: USER_ID
          });
          SUCCESS_REJECTED++;
          RAW_MASTER.MPM_ID == undefined ? "" : result.LOG.SUCCESS.push(RAW_MASTER.MPM_ID);
        } catch (error) {
          result.success = false;
          result.message = error;
          VAILED_REJECTED++;
          RAW_MASTER.MPM_ID == undefined ? "" : result.LOG.FAILED.push(RAW_MASTER.MPM_ID);
        }
      } catch (error) {
        result.success = false;
        result.message = error;
        VAILED_REJECTED++;
        RAW_MASTER.MPM_ID == undefined ? "" : result.LOG.FAILED.push(RAW_MASTER.MPM_ID);
      }

    }

    result.message = "Success rejected (" + SUCCESS_REJECTED + "), vailed rejected (" + VAILED_REJECTED + ")";
    return result;
  }

  @post('/merchant-transaksi-master-action/validation/data-master/bussines-assurace/step-2/approved', {
    responses: {
      '200': {
        description: 'Validate Data Master Bussines Assurance approve master model instance',
        content: {
          'application/json': {
            schema: MPM_ID_RequestBody
          }
        },
      },
    },
  })
  async merchantTransaksiMasterValidasiAccStep2(
    @requestBody(MPM_ID_RequestBody) mPM_ID_RequestBody: {MPM_ID: []},
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};

    var TRANSAKSI_MASTER = await this.merchantProductMasterRepository.find({
      where: {
        and: [
          {
            MPM_ID: {inq: mPM_ID_RequestBody.MPM_ID},
            GENERATE_BAR: 1,
            AT_POSISI: 2,
            AT_FLAG: {inq: [0, 2]}
          }]
      }
    });

    for (var i in TRANSAKSI_MASTER) {
      var RAW_MASTER = TRANSAKSI_MASTER[i];
      try {
        await this.merchantProductMasterRepository.updateById(RAW_MASTER.MPM_ID, {
          AT_UPDATE: ControllerConfig.onCurrentTime().toString(),
          AT_WHO: USER_ID,
          AT_POSISI: 3,
          AT_FLAG: 0
        });
      } catch (error) {
        result.success = false;
        result.message = error;
      }
    }
    return result;
  }


  @post('/merchant-transaksi-master-action/distribusi/checking', {
    responses: {
      '200': {
        description: 'Return transaksi master to Acoount Rechivable  model instance',
        content: {
          'application/json': {
            schema: DistributeDocCheckingRequestBody
          }
        },
      },
    },
  })
  async merchantTransaksiMasterDistribusiChecking(
    @requestBody(DistributeDocCheckingRequestBody) checkingDistributeRequestBody: {MPM_ID: [], DISTRIBUTE_MERCHANT: number, FEEDBACK_MERCHANT: number},
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};

    var SELECTED_MPM_ID = checkingDistributeRequestBody.MPM_ID;
    var DISTRIBUTE_MERCHANT = 0;
    var FEEDBACK_MERCHANT = 0;

    var FILTERS: any; FILTERS = {where: {and: [{MPM_ID: {inq: SELECTED_MPM_ID}}]}};

    if (checkingDistributeRequestBody.DISTRIBUTE_MERCHANT == 0) {
      DISTRIBUTE_MERCHANT = 0;
      FEEDBACK_MERCHANT = 0;
      FILTERS["where"]["and"].push({or: [{HAS_AR_REJECTED: 1}, {AT_POSISI: {inq: [5, 6, 7]}}]});
    } else {
      FEEDBACK_MERCHANT = checkingDistributeRequestBody.FEEDBACK_MERCHANT;
      if (FEEDBACK_MERCHANT == 0) {
        FILTERS["where"]["and"].push({or: [{HAS_AR_REJECTED: 1}, {AT_POSISI: {inq: [5, 6, 7]}}]});
      }
    }

    var DATA_MASTER = await this.merchantProductMasterRepository.find(FILTERS);
    var DATA_RESULT = {BAR: 0, BAR_SIGN: 0, INVOICE: 0, INVOICE_SIGN: 0, FAKTUR_PAJAK: 0, BUKPOT: 0, OTHERS: 0};
    var DATA_ID: any; DATA_ID = {BAR: [], BAR_SIGN: [], INVOICE: [], INVOICE_SIGN: [], FAKTUR_PAJAK: [], BUKPOT: [], OTHERS: []};


    for (var i in DATA_MASTER) {
      var RAW_MASTER = DATA_MASTER[i];
      var ID = RAW_MASTER.MPM_ID;
      if (RAW_MASTER.GENERATE_BAR == 1) {
        DATA_RESULT.BAR++;
        DATA_ID.BAR.push(ID);
      }
      if (RAW_MASTER.GENERATE_BAR_SIGN == 1) {
        DATA_RESULT.BAR_SIGN++;
        DATA_ID.BAR_SIGN.push(ID);
      }
      if (RAW_MASTER.GENERATE_INVOICE == 1) {
        DATA_RESULT.INVOICE++;
        DATA_ID.INVOICE.push(ID);
      }
      if (RAW_MASTER.GENERATE_INVOICE_SIGN == 1) {
        DATA_RESULT.INVOICE_SIGN++;
        DATA_ID.INVOICE_SIGN.push(ID);
      }
      if (RAW_MASTER.GENERATE_FAKTUR_PAJAK == 1) {
        DATA_RESULT.FAKTUR_PAJAK++;
        DATA_ID.FAKTUR_PAJAK.push(ID);
      }
      if (RAW_MASTER.GENERATE_BUKPOT == 1) {
        DATA_RESULT.BUKPOT++;
        DATA_ID.BUKPOT.push(ID);
      }
      var DOCUMENT_OTHERS = await this.merchantProductDocumentRepository.find({where: {and: [{AT_FLAG: 1}, {MPDOC_TYPE: 7}, {MPM_ID: RAW_MASTER.MPM_ID}]}});
      if (DOCUMENT_OTHERS.length > 0) {
        DATA_RESULT.OTHERS = DATA_RESULT.OTHERS + DOCUMENT_OTHERS.length;
        DATA_ID.OTHERS.push(ID);
      }
    }

    result.datas = {SELECTED: SELECTED_MPM_ID.length, AVALIABLE: DATA_MASTER.length, AVALUABLE_ID: DATA_ID, AVALIABLE_DOC: DATA_RESULT};
    result.message = "Success !";
    result.success = true;
    return result;
  }

  @post('/merchant-transaksi-master-action/distribusi/process-distribute', {
    responses: {
      '200': {
        description: 'Return transaksi master to Acoount Rechivable  model instance',
        content: {
          'application/json': {
            schema: DistributeDocRequestBody
          }
        },
      },
    },
  })
  async merchantTransaksiMasterDistribusi(
    @requestBody(DistributeDocRequestBody) checkingDistributeRequestBody: {MPM_ID: [], DISTRIBUTE_MERCHANT: number, FEEDBACK_MERCHANT: number, DOC_DISTRIBUTE: [0, 0, 0, 0, 0, 0, 0], NOTE: string, SEND_CC: string},
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};

    var SELECTED_MPM_ID = checkingDistributeRequestBody.MPM_ID;
    var DISTRIBUTE_MERCHANT = 0;
    var FEEDBACK_MERCHANT = 0;

    if (checkingDistributeRequestBody.MPM_ID.length == 0) {
      result.message = "Error, please select transaction for distiribute document !";
      result.success = false;
      return result;
    }

    if (checkingDistributeRequestBody.NOTE == "") {
      result.message = "Error, please write note before distribute document !";
      result.success = false;
      return result;
    }

    if (checkingDistributeRequestBody.SEND_CC == "") {
      result.message = "Error, please write Send CC before distribute document !";
      result.success = false;
      return result;
    }


    var FILTERS: any; FILTERS = {where: {and: [{MPM_ID: {inq: SELECTED_MPM_ID}}]}};

    if (checkingDistributeRequestBody.DISTRIBUTE_MERCHANT == 0) {
      DISTRIBUTE_MERCHANT = 0;
      FEEDBACK_MERCHANT = 0;
      FILTERS["where"]["and"].push({or: [{HAS_AR_REJECTED: 1}, {AT_POSISI: {inq: [5, 6, 7]}}]});
    } else {
      FEEDBACK_MERCHANT = checkingDistributeRequestBody.FEEDBACK_MERCHANT;
      if (FEEDBACK_MERCHANT == 0) {
        FILTERS["where"]["and"].push({or: [{HAS_AR_REJECTED: 1}, {AT_POSISI: {inq: [5, 6, 7]}}]});
      }
    }

    var DATA_MASTER = await this.merchantProductMasterRepository.find(FILTERS);
    var DATA_RESULT: any; DATA_RESULT = {}


    for (var i in DATA_MASTER) {
      var RAW_MASTER = DATA_MASTER[i];
      var ID = RAW_MASTER.MPM_ID == undefined ? "0" : RAW_MASTER.MPM_ID;
      if (DATA_RESULT[ID] == undefined) {
        DATA_RESULT[ID] = {BAR: 0, BAR_SIGN: 0, INVOICE: 0, INVOICE_SIGN: 0, FAKTUR_PAJAK: 0, BUKPOT: 0, OTHERS: 0};
      }

      if (RAW_MASTER.GENERATE_BAR == 1) {
        DATA_RESULT[ID].BAR = 1;
      }
      if (RAW_MASTER.GENERATE_BAR_SIGN == 1) {
        DATA_RESULT[ID].BAR_SIGN = 1;
      }
      if (RAW_MASTER.GENERATE_INVOICE == 1) {
        DATA_RESULT[ID].INVOICE = 1;
      }
      if (RAW_MASTER.GENERATE_INVOICE_SIGN == 1) {
        DATA_RESULT[ID].INVOICE_SIGN = 1;
      }
      if (RAW_MASTER.GENERATE_FAKTUR_PAJAK == 1) {
        DATA_RESULT[ID].FAKTUR_PAJAK = 1;
      }
      if (RAW_MASTER.GENERATE_BUKPOT == 1) {
        DATA_RESULT[ID].BUKPOT = 1;
      }
      var DOCUMENT_OTHERS = await this.merchantProductDocumentRepository.find({where: {and: [{AT_FLAG: 1}, {MPDOC_TYPE: 7}, {MPM_ID: RAW_MASTER.MPM_ID}]}});
      if (DOCUMENT_OTHERS.length > 0) {
        DATA_RESULT[ID].OTHERS = 1;
      }

      if (checkingDistributeRequestBody.DISTRIBUTE_MERCHANT != 0) {

        var DIS_BAR = checkingDistributeRequestBody.DOC_DISTRIBUTE[0] == 0 || checkingDistributeRequestBody.DOC_DISTRIBUTE[0] == undefined ? 0 : DATA_RESULT[ID].BAR;
        var DIS_BAR_SIGN = checkingDistributeRequestBody.DOC_DISTRIBUTE[1] == 0 || checkingDistributeRequestBody.DOC_DISTRIBUTE[1] == undefined ? 0 : DATA_RESULT[ID].BAR_SIGN;
        var DIS_INVOICE = checkingDistributeRequestBody.DOC_DISTRIBUTE[2] == 0 || checkingDistributeRequestBody.DOC_DISTRIBUTE[2] == undefined ? 0 : DATA_RESULT[ID].INVOICE;
        var DIS_INVOICE_SIGN = checkingDistributeRequestBody.DOC_DISTRIBUTE[3] == 0 || checkingDistributeRequestBody.DOC_DISTRIBUTE[3] == undefined ? 0 : DATA_RESULT[ID].INVOICE_SIGN;
        var DIS_FAKTUR = checkingDistributeRequestBody.DOC_DISTRIBUTE[4] == 0 || checkingDistributeRequestBody.DOC_DISTRIBUTE[4] == undefined ? 0 : DATA_RESULT[ID].FAKTUR_PAJAK;
        var DIS_BUKPOT = checkingDistributeRequestBody.DOC_DISTRIBUTE[5] == 0 || checkingDistributeRequestBody.DOC_DISTRIBUTE[5] == undefined ? 0 : DATA_RESULT[ID].BUKPOT;
        var DIS_OTHERS = checkingDistributeRequestBody.DOC_DISTRIBUTE[6] == 0 || checkingDistributeRequestBody.DOC_DISTRIBUTE[6] == undefined ? 0 : DATA_RESULT[ID].OTHERS;


        var MERCHANT = await this.merchantRepository.findById(RAW_MASTER.M_ID);
        var DISTRIBUTE = await this.merchantProductDistribusiRepository.find({where: {and: [{SEND_FLAG: 0}, {MPM_ID: ID}]}});
        if (DISTRIBUTE.length > 0) {
          for (var J in DISTRIBUTE) {
            var RAW_DISTRIBUTE = DISTRIBUTE[J];
            await this.merchantProductDistribusiRepository.updateById(RAW_DISTRIBUTE.MPDIST_ID, {
              MPDIST_MESSAGE: checkingDistributeRequestBody.NOTE,
              AT_FEEDBACK_MERCHANT: checkingDistributeRequestBody.FEEDBACK_MERCHANT,
              SEND_BAR: DIS_BAR,
              SEND_BAR_SIGN: DIS_BAR_SIGN,
              SEND_INVOICE: DIS_INVOICE,
              SEND_INVOICE_SIGN: DIS_INVOICE_SIGN,
              SEND_FAKTUR: DIS_FAKTUR,
              SEND_BUKPOT: DIS_BUKPOT,
              SEND_OTHERS: DIS_OTHERS,
              SEND_MESSAGE: undefined
            });
          }
        } else {
          await this.merchantProductDistribusiRepository.create({
            MPDIST_MESSAGE: checkingDistributeRequestBody.NOTE,
            SEND_TO: MERCHANT.M_PIC_EMAIL,
            SEND_CC: checkingDistributeRequestBody.SEND_CC,
            SEND_FLAG: 0,
            CREATE_SEND: ControllerConfig.onCurrentTime().toString(),
            MPM_ID: RAW_MASTER.MPM_ID,
            START_TIME_RUN: RAW_MASTER.START_TIME_RUN,
            AT_CREATE: ControllerConfig.onCurrentTime().toString(),
            AT_UPDATE: undefined,
            AT_FLAG: 0,
            AT_WHO: USER_ID,
            SEND_MESSAGE: undefined,
            AT_FEEDBACK_MERCHANT: checkingDistributeRequestBody.FEEDBACK_MERCHANT,
            SEND_BAR: DIS_BAR,
            SEND_BAR_SIGN: DIS_BAR_SIGN,
            SEND_INVOICE: DIS_INVOICE,
            SEND_INVOICE_SIGN: DIS_INVOICE_SIGN,
            SEND_FAKTUR: DIS_FAKTUR,
            SEND_BUKPOT: DIS_BUKPOT,
            SEND_OTHERS: DIS_OTHERS,
            AT_POSISI_BEFORE: RAW_MASTER.AT_POSISI,
            AT_FLAG_BEFORE: RAW_MASTER.AT_FLAG
          });
        }

        if (checkingDistributeRequestBody.FEEDBACK_MERCHANT == 1) {
          await this.merchantProductMasterRepository.updateById(RAW_MASTER.MPM_ID, {
            AT_POSISI: 4,
            AT_FLAG: 0,
            WAITING_SEND_MAIL: 1
          });
        }
      } else {
        var SEND_AR = [5, 6, 7];
        if (SEND_AR.indexOf(RAW_MASTER.AT_POSISI == undefined ? 0 : RAW_MASTER.AT_POSISI) == -1) {
          await this.merchantProductMasterRepository.updateById(RAW_MASTER.MPM_ID, {
            AT_POSISI: 5,
            AT_FLAG: 0
          });
        }
      }
    }

    result.message = "Success processed " + DATA_MASTER.length + " transaction distribute !";
    result.success = true;
    return result;
  }


  @post('/merchant-transaksi-master-action/validation/data-master/merchant/approved', {
    responses: {
      '200': {
        description: 'Validate Data Master Merchantapprove master model instance',
        content: {
          'application/json': {
            schema: MPM_ID_RequestBody
          }
        },
      },
    },
  })
  async merchantTransaksiMasterValidasiAccMerchant(
    @requestBody(MPM_ID_RequestBody) mPM_ID_RequestBody: {MPM_ID: []},
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};

    var TRANSAKSI_MASTER = await this.merchantProductMasterRepository.find({
      where: {
        and: [
          {
            MPM_ID: {inq: mPM_ID_RequestBody.MPM_ID},
            AT_POSISI: 4,
            AT_FLAG: {inq: [0, 2]}
          }]
      }
    });

    var CountWaiting = 0;
    var CountError = 0;
    var CountSuccess = 0;
    for (var i in TRANSAKSI_MASTER) {
      var RAW_MASTER = TRANSAKSI_MASTER[i];
      if (RAW_MASTER.WAITING_SEND_MAIL == 0) {
        try {
          await this.merchantProductMasterRepository.updateById(RAW_MASTER.MPM_ID, {
            AT_UPDATE: ControllerConfig.onCurrentTime().toString(),
            AT_WHO: USER_ID,
            AT_POSISI: 5,
            AT_FLAG: 0
          });
          CountSuccess++;
        } catch (error) {
          result.success = false;
          result.message = error;
          CountError++;
        }
      } else {
        CountWaiting++;
      }
    }

    result.success = true;
    result.message = "Success Validate : " + CountSuccess + ", Waiting Mail : " + CountWaiting + ", Error : " + CountError;
    return result;
  }


  @post('/merchant-transaksi-master-action/validation/data-master/merchant/rejected', {
    responses: {
      '200': {
        description: 'Validate Data Master Bussines Assurance model instance',
        content: {
          'application/json': {
            schema: RejectedTRXRequestBody
          }
        },
      },
    },
  })
  async merchantTransaksiMasterValidasiRejectMerchant(
    @requestBody(RejectedTRXRequestBody) rejectedBA: {MPM_ID: [], NOTE: string},
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: 'Success !', datas: [], LOG: {SUCCESS: [], FAILED: []}};
    var TRANSAKSI_MASTER = await this.merchantProductMasterRepository.find({
      where: {
        and: [
          {
            MPM_ID: {inq: rejectedBA.MPM_ID},
            AT_POSISI: 4
          }]
      }
    });

    var SUCCESS_REJECTED = 0;
    var VAILED_REJECTED = 0;
    for (var i in TRANSAKSI_MASTER) {
      var RAW_MASTER = TRANSAKSI_MASTER[i];
      if (RAW_MASTER.WAITING_SEND_MAIL == 1) {
        continue;
      }
      try {
        await this.merchantProductFeedbackRepository.create({
          MPF_MESSAGE: rejectedBA.NOTE,
          MPM_ID: RAW_MASTER.MPM_ID,
          START_TIME_RUN: RAW_MASTER.START_TIME_RUN,
          AT_FLAG: 1,
          AT_CREATE: ControllerConfig.onCurrentTime().toString(),
          AT_SEND: ControllerConfig.onCurrentTime().toString(),
          AT_WHO: USER_ID,
          AT_UPDATE: undefined,
          TOKEN_KEY: "0000000001",
          TYPE_FEEDBACK: 4,
          SEND_TO: "-",
          SEND_CC: "-",
          SEND_FLAG: -1,
          SEND_MESSAGE: rejectedBA.NOTE,
          REQ_FILE_BAR: 0,
          REQ_FILE_SUMMARY: 0,
          REQ_FILE_BUKPOT: 0,
          REQ_FILE_BUKTI_BAYAR: 0,
          REQ_FILE_FAKTUR: 0,
          REQ_FILE_INVOICE: 0,
          FILE_BAR: undefined,
          FILE_BUKPOT: undefined,
          FILE_BUKTIBAYAR: undefined,
          FILE_FAKTUR: undefined,
          FILE_INVOICE: undefined,
          FILE_OTHERS: undefined,
          FILE_SUMMARY: undefined,
        });
        try {
          await this.merchantProductMasterRepository.updateById(RAW_MASTER.MPM_ID, {
            AT_FLAG: 2,
            AT_POSISI: 1,
            AT_WHO: USER_ID
          });
          SUCCESS_REJECTED++;
          RAW_MASTER.MPM_ID == undefined ? "" : result.LOG.SUCCESS.push(RAW_MASTER.MPM_ID);
        } catch (error) {
          result.success = false;
          result.message = error;
          VAILED_REJECTED++;
          RAW_MASTER.MPM_ID == undefined ? "" : result.LOG.FAILED.push(RAW_MASTER.MPM_ID);
        }
      } catch (error) {
        result.success = false;
        result.message = error;
        VAILED_REJECTED++;
        RAW_MASTER.MPM_ID == undefined ? "" : result.LOG.FAILED.push(RAW_MASTER.MPM_ID);
      }
    }
    result.message = "Success rejected (" + SUCCESS_REJECTED + "), vailed rejected (" + VAILED_REJECTED + ")";
    return result;
  }

  @post('/merchant-transaksi-master-action/validation/data-master/account-recv/validate', {
    responses: {
      '200': {
        description: 'Approve step1 transaksi master model instance',
        content: {
          'application/json': {
            schema: MPM_ID_RequestBody
          }
        },
      },
    },
  })
  async merchantTransaksiMasterValidateAccountRecv(
    @requestBody(MPM_ID_RequestBody) validateBARSign: {MPM_ID: []},
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: 'Success !', datas: []};
    var TRANSAKSI_MASTER = await this.merchantProductMasterRepository.find({
      where: {
        and: [
          {
            MPM_ID: {inq: validateBARSign.MPM_ID},
            AT_POSISI: 5,
            AT_FLAG: 0
          }]
      }
    });
    for (var i in TRANSAKSI_MASTER) {
      var RAW_MASTER = TRANSAKSI_MASTER[i];
      try {
        await this.merchantProductMasterRepository.updateById(RAW_MASTER.MPM_ID, {
          AT_FLAG: 0,
          AT_POSISI: 6,
          AT_WHO: USER_ID
        });
      } catch (error) {

      }
    }
    return result;
  }

  @post('/merchant-transaksi-master-action/validation/data-master/account-recv/reject-ba', {
    responses: {
      '200': {
        description: 'Reject To BA transaksi master model instance',
        content: {
          'application/json': {
            schema: RejectedTRXRequestBody
          }
        },
      },
    },
  })
  async merchantTransaksiMasterRejectToBAAccountRecv(
    @requestBody(RejectedTRXRequestBody) rejectedToBA: {MPM_ID: [], NOTE: string},
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: 'Success !', datas: [], LOG: {SUCCESS: [], FAILED: []}};

    var TRANSAKSI_MASTER = await this.merchantProductMasterRepository.find({
      where: {
        and: [
          {
            MPM_ID: {inq: rejectedToBA.MPM_ID},
            AT_POSISI: {inq: [5, 6]}
          }]
      }
    });


    var SUCCESS_REJECTED = 0;
    var VAILED_REJECTED = 0;
    for (var i in TRANSAKSI_MASTER) {
      var RAW_MASTER = TRANSAKSI_MASTER[i];
      try {
        await this.merchantProductFeedbackRepository.create({
          MPF_MESSAGE: rejectedToBA.NOTE,
          MPM_ID: RAW_MASTER.MPM_ID,
          START_TIME_RUN: RAW_MASTER.START_TIME_RUN,
          AT_FLAG: 1,
          AT_CREATE: ControllerConfig.onCurrentTime().toString(),
          AT_SEND: ControllerConfig.onCurrentTime().toString(),
          AT_WHO: USER_ID,
          AT_UPDATE: undefined,
          TOKEN_KEY: "0000000001",
          TYPE_FEEDBACK: 5,
          SEND_TO: "-",
          SEND_CC: "-",
          SEND_FLAG: -1,
          SEND_MESSAGE: rejectedToBA.NOTE,
          REQ_FILE_BAR: 0,
          REQ_FILE_SUMMARY: 0,
          REQ_FILE_BUKPOT: 0,
          REQ_FILE_BUKTI_BAYAR: 0,
          REQ_FILE_FAKTUR: 0,
          REQ_FILE_INVOICE: 0,
          FILE_BAR: undefined,
          FILE_BUKPOT: undefined,
          FILE_BUKTIBAYAR: undefined,
          FILE_FAKTUR: undefined,
          FILE_INVOICE: undefined,
          FILE_OTHERS: undefined,
          FILE_SUMMARY: undefined,
        });

        try {
          await this.merchantProductMasterRepository.updateById(RAW_MASTER.MPM_ID, {
            AT_FLAG: 2,
            AT_POSISI: 1,
            HAS_AR_REJECTED: 1,
            AT_WHO: USER_ID
          });
          SUCCESS_REJECTED++;
          RAW_MASTER.MPM_ID == undefined ? "" : result.LOG.SUCCESS.push(RAW_MASTER.MPM_ID);
        } catch (error) {
          result.success = false;
          result.message = error;
          VAILED_REJECTED++;
          RAW_MASTER.MPM_ID == undefined ? "" : result.LOG.FAILED.push(RAW_MASTER.MPM_ID);
        }
      } catch (error) {
        result.success = false;
        result.message = error;
        VAILED_REJECTED++;
        RAW_MASTER.MPM_ID == undefined ? "" : result.LOG.FAILED.push(RAW_MASTER.MPM_ID);
      }

    }
    result.message = "Success rejected (" + SUCCESS_REJECTED + "), vailed rejected (" + VAILED_REJECTED + ")";
    return result;
  }


  @post('/merchant-transaksi-master-action/validation/data-master/account-recv/reject-step2', {
    responses: {
      '200': {
        description: 'Reject To BA step 2 transaksi master model instance',
        content: {
          'application/json': {
            schema: RejectedTRXRequestBody
          }
        },
      },
    },
  })
  async merchantTransaksiMasterRejectToStep2AcountRecv(
    @requestBody(RejectedTRXRequestBody) rejectedToBA: {MPM_ID: [], NOTE: string},
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: 'Success !', datas: [], LOG: {SUCCESS: [], FAILED: []}};

    var TRANSAKSI_MASTER = await this.merchantProductMasterRepository.find({
      where: {
        and: [
          {
            MPM_ID: {inq: rejectedToBA.MPM_ID},
            AT_POSISI: {inq: [5]}
          }]
      }
    });


    var SUCCESS_REJECTED = 0;
    var VAILED_REJECTED = 0;
    for (var i in TRANSAKSI_MASTER) {
      var RAW_MASTER = TRANSAKSI_MASTER[i];
      try {
        await this.merchantProductFeedbackRepository.create({
          MPF_MESSAGE: rejectedToBA.NOTE,
          MPM_ID: RAW_MASTER.MPM_ID,
          START_TIME_RUN: RAW_MASTER.START_TIME_RUN,
          AT_FLAG: 1,
          AT_CREATE: ControllerConfig.onCurrentTime().toString(),
          AT_SEND: ControllerConfig.onCurrentTime().toString(),
          AT_WHO: USER_ID,
          AT_UPDATE: undefined,
          TOKEN_KEY: "0000000001",
          TYPE_FEEDBACK: 6,
          SEND_TO: "-",
          SEND_CC: "-",
          SEND_FLAG: -1,
          SEND_MESSAGE: rejectedToBA.NOTE,
          REQ_FILE_BAR: 0,
          REQ_FILE_SUMMARY: 0,
          REQ_FILE_BUKPOT: 0,
          REQ_FILE_BUKTI_BAYAR: 0,
          REQ_FILE_FAKTUR: 0,
          REQ_FILE_INVOICE: 0,
          FILE_BAR: undefined,
          FILE_BUKPOT: undefined,
          FILE_BUKTIBAYAR: undefined,
          FILE_FAKTUR: undefined,
          FILE_INVOICE: undefined,
          FILE_OTHERS: undefined,
          FILE_SUMMARY: undefined,
        });

        try {
          await this.merchantProductMasterRepository.updateById(RAW_MASTER.MPM_ID, {
            AT_FLAG: 2,
            AT_POSISI: 5,
            AT_WHO: USER_ID
          });
          SUCCESS_REJECTED++;
          RAW_MASTER.MPM_ID == undefined ? "" : result.LOG.SUCCESS.push(RAW_MASTER.MPM_ID);
        } catch (error) {
          result.success = false;
          result.message = error;
          VAILED_REJECTED++;
          RAW_MASTER.MPM_ID == undefined ? "" : result.LOG.FAILED.push(RAW_MASTER.MPM_ID);
        }
      } catch (error) {
        result.success = false;
        result.message = error;
        VAILED_REJECTED++;
        RAW_MASTER.MPM_ID == undefined ? "" : result.LOG.FAILED.push(RAW_MASTER.MPM_ID);
      }

    }
    result.message = "Success rejected (" + SUCCESS_REJECTED + "), vailed rejected (" + VAILED_REJECTED + ")";
    return result;
  }


  @post('/merchant-transaksi-master-action/validation/data-master/account-recv/validate-step2', {
    responses: {
      '200': {
        description: 'Approve step1 transaksi master model instance',
        content: {
          'application/json': {
            schema: MPM_ID_RequestBody
          }
        },
      },
    },
  })
  async merchantTransaksiMasterValidateAccountRecvStep2(
    @requestBody(MPM_ID_RequestBody) validateBARSign: {MPM_ID: []},
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: 'Success !', datas: []};
    var TRANSAKSI_MASTER = await this.merchantProductMasterRepository.find({
      where: {
        and: [
          {
            MPM_ID: {inq: validateBARSign.MPM_ID},
            AT_POSISI: 6,
            AT_FLAG: 0
          }]
      }
    });
    for (var i in TRANSAKSI_MASTER) {
      var RAW_MASTER = TRANSAKSI_MASTER[i];
      try {
        await this.merchantProductMasterRepository.updateById(RAW_MASTER.MPM_ID, {
          AT_FLAG: 1,
          AT_POSISI: 6,
          AT_WHO: USER_ID
        });
      } catch (error) {

      }
    }
    return result;
  }


  @post('/merchant-transaksi-master-action/generate-number/document-invoice-number', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: GenerateNumberInvRequestBody
          }
        },
      },
    },
  })
  async generateNoInvoice(
    @requestBody(GenerateNumberInvRequestBody) docInvoice: {MPM_ID: [], LABEL: string, LAST_NUMBER: number},
  ): Promise<Object> {

    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: 'Success !', datas: []};
    var TRANSAKSI_MASTER = await this.merchantProductMasterRepository.find({
      where: {
        and: [
          {
            MPM_ID: {inq: docInvoice.MPM_ID},
            AT_FLAG: {inq: [0, 1]},
            AT_POSISI: 6
          }]
      }
    });

    var LABEL_INVOICE = docInvoice.LABEL.substring(1, 1) == "." ? docInvoice.LABEL.substring(1) : docInvoice.LABEL;
    var NO = docInvoice.LAST_NUMBER + 1;

    var GENERATE_SUCCESS = 0;
    var ERROR_GENERATE = 0;
    for (var i in TRANSAKSI_MASTER) {
      var RAW_MASTER = TRANSAKSI_MASTER[i];

      if (RAW_MASTER.MPM_NO_INVOICE != "" && RAW_MASTER.MPM_NO_INVOICE != null) {
        continue;
      }

      var M_CODE = "";
      var AVALIABLE_CODE: boolean; AVALIABLE_CODE = false;
      do {

        var Zero = "0000";
        var INVOICE_NUMBER = LABEL_INVOICE + "-" + Zero.substring(NO.toString().length, 4) + NO.toString();
        var VALID_CODE = await this.merchantProductMasterRepository.findOne({where: {and: [{MPM_NO_INVOICE: INVOICE_NUMBER}]}});
        if (VALID_CODE == null) {
          M_CODE = INVOICE_NUMBER;
          AVALIABLE_CODE = true;
        }
        NO++;
      }
      while (AVALIABLE_CODE == false);

      try {
        await this.merchantProductMasterRepository.updateById(RAW_MASTER.MPM_ID, {
          MPM_NO_INVOICE: M_CODE
        });

        var MPM_ID = RAW_MASTER.MPM_ID == undefined ? -1 : RAW_MASTER.MPM_ID;
        if (STORE_GENERATE_INVOICE[MPM_ID] == undefined) {
          STORE_GENERATE_INVOICE[MPM_ID] = {STATE: 0, ID: MPM_ID};
        }
        GENERATE_SUCCESS++;
      } catch (error) {
        ERROR_GENERATE++;
      }
    }
    this.EXE_GENERATE_INVOICE();
    result.message = "Success Generate Number : " + GENERATE_SUCCESS + ", Error Generate Number : " + ERROR_GENERATE;
    return result;
  }

  @post('/merchant-transaksi-master-action/re-generate/document-invoice', {
    responses: {
      '200': {
        description: 'Document invoice re generate master model instance',
        content: {
          'application/json': {
            schema: MPM_ID_RequestBody
          }
        },
      },
    },
  })
  async merchantTransaksiMasterReGenerateDocInv(
    @requestBody(MPM_ID_RequestBody) generateNumberBARRequestBody: {MPM_ID: []},
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: 'Success !', datas: []};
    var TRANSAKSI_MASTER = await this.merchantProductMasterRepository.find({
      where: {
        and: [
          {
            MPM_ID: {inq: generateNumberBARRequestBody.MPM_ID},
            GENERATE_INVOICE: 1,
            AT_POSISI: 6,
            AT_FLAG: {inq: [0, 1]}
          }]
      }
    });
    for (var i in TRANSAKSI_MASTER) {
      var RAW_MASTER = TRANSAKSI_MASTER[i];
      if (RAW_MASTER.MPM_NO_INVOICE == "" || RAW_MASTER.MPM_NO_INVOICE == null) {
        continue;
      }

      var MPM_ID = RAW_MASTER.MPM_ID == undefined ? -1 : RAW_MASTER.MPM_ID;
      if (STORE_GENERATE_INVOICE[MPM_ID] == undefined) {
        STORE_GENERATE_INVOICE[MPM_ID] = {STATE: 0, ID: MPM_ID};
      }
    }
    this.EXE_GENERATE_INVOICE();
    return result;
  }


  @post('/merchant-transaksi-master-action/maping-faktur-pajak', {
    responses: {
      '200': {
        description: 'Approve step1 transaksi master model instance',
        content: {
          'application/json': {
            schema: MPM_ID_RequestBody
          }
        },
      },
    },
  })
  async merchantTransaksiMasterMapingFaktur(
    @requestBody(MPM_ID_RequestBody) requestFaktur: {MPM_ID: []},
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: 'Success !', datas: []};

    var FILTER_SET: any; FILTER_SET = {where: {and: [{GENERATE_INVOICE: 1}, {AT_POSISI: 6}]}};
    if (requestFaktur.MPM_ID.length > 0) {
      FILTER_SET["where"]["and"].push({MPM_ID: {inq: requestFaktur.MPM_ID}});
    }

    var DATA_TRANSAKSI = await this.merchantProductMasterRepository.find(FILTER_SET);
    for (var i in DATA_TRANSAKSI) {
      var RAW_TRX = DATA_TRANSAKSI[i];
      if (RAW_TRX.MPM_NO_FAKTUR != "" && RAW_TRX.MPM_NO_FAKTUR != null) {
        continue;
      }

      var PERIODE = new Date(RAW_TRX.PERIODE + "-01");
      var YEARS = PERIODE.getFullYear().toString();

      var MERCHANT = await this.merchantRepository.findById(RAW_TRX.M_ID);
      var NO_FAKTUR = MERCHANT.M_NO_PERFIX + ".";
      var NO_FAKTUR_ID: any; NO_FAKTUR_ID = 0;
      var FakturPajak = await this.noFakturPajakRepository.findOne({order: ["number_fp ASC"], where: {and: [{fp_state: 0}, {fp_buffer: false}, {periode: YEARS}]}});
      if (FakturPajak != null) {
        NO_FAKTUR += FakturPajak.no_faktur_pajak;
        NO_FAKTUR_ID = FakturPajak.id;
      } else {
        var FakturPajakBufer = await this.noFakturPajakRepository.findOne({order: ["number_fp ASC"], where: {and: [{fp_state: 0}, {fp_buffer: true}, {periode: YEARS}]}});
        if (FakturPajakBufer != null) {
          NO_FAKTUR += FakturPajakBufer.no_faktur_pajak;
          NO_FAKTUR_ID = FakturPajakBufer.id;
        }
      }

      if (NO_FAKTUR_ID == 0 || NO_FAKTUR_ID == undefined) {
        result.success = false;
        result.message = "Error, No Faktur Not Avaliable, please register no faktur !";
        break;
      } else {
        try {
          await this.merchantProductMasterRepository.updateById(RAW_TRX.MPM_ID, {
            MPM_NO_FAKTUR: NO_FAKTUR
          });

          try {
            await this.noFakturPajakRepository.updateById(NO_FAKTUR_ID, {
              fp_state: 1,
              no_invoice_link: RAW_TRX.MPM_NO_INVOICE,
              no_faktur_link: NO_FAKTUR,
              description: "NEW TRANSAKSI REVENUE TOOLS",
              at_used: ControllerConfig.onCurrentTime().toString()
            });
            result.success = true;
            result.message = "Success maping Faktur !";
          } catch (error) {
            await this.merchantProductMasterRepository.updateById(RAW_TRX.MPM_ID, {
              MPM_NO_FAKTUR: undefined
            });
            result.success = false;
            result.message = error;
          }
        } catch (error) {
          result.success = false;
          result.message = error;
        }
      }
    }

    return result;
  }






}
