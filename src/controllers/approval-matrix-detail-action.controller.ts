import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {ApprovalMatrixDetail} from '../models';
import {ApprovalMatrixDetailRepository} from '../repositories';

export class ApprovalMatrixDetailActionController {
  constructor(
    @repository(ApprovalMatrixDetailRepository)
    public approvalMatrixDetailRepository : ApprovalMatrixDetailRepository,
  ) {}

  @post('/approval-matrix-details', {
    responses: {
      '200': {
        description: 'ApprovalMatrixDetail model instance',
        content: {'application/json': {schema: getModelSchemaRef(ApprovalMatrixDetail)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ApprovalMatrixDetail, {
            title: 'NewApprovalMatrixDetail',
            exclude: ['amd_id'],
          }),
        },
      },
    })
    approvalMatrixDetail: Omit<ApprovalMatrixDetail, 'amd_id'>,
  ): Promise<ApprovalMatrixDetail> {
    return this.approvalMatrixDetailRepository.create(approvalMatrixDetail);
  }

  @get('/approval-matrix-details/count', {
    responses: {
      '200': {
        description: 'ApprovalMatrixDetail model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(ApprovalMatrixDetail) where?: Where<ApprovalMatrixDetail>,
  ): Promise<Count> {
    return this.approvalMatrixDetailRepository.count(where);
  }

  @get('/approval-matrix-details', {
    responses: {
      '200': {
        description: 'Array of ApprovalMatrixDetail model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(ApprovalMatrixDetail, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(ApprovalMatrixDetail) filter?: Filter<ApprovalMatrixDetail>,
  ): Promise<ApprovalMatrixDetail[]> {
    return this.approvalMatrixDetailRepository.find(filter);
  }

  @patch('/approval-matrix-details', {
    responses: {
      '200': {
        description: 'ApprovalMatrixDetail PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ApprovalMatrixDetail, {partial: true}),
        },
      },
    })
    approvalMatrixDetail: ApprovalMatrixDetail,
    @param.where(ApprovalMatrixDetail) where?: Where<ApprovalMatrixDetail>,
  ): Promise<Count> {
    return this.approvalMatrixDetailRepository.updateAll(approvalMatrixDetail, where);
  }

  @get('/approval-matrix-details/{id}', {
    responses: {
      '200': {
        description: 'ApprovalMatrixDetail model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(ApprovalMatrixDetail, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(ApprovalMatrixDetail, {exclude: 'where'}) filter?: FilterExcludingWhere<ApprovalMatrixDetail>
  ): Promise<ApprovalMatrixDetail> {
    return this.approvalMatrixDetailRepository.findById(id, filter);
  }

  @patch('/approval-matrix-details/{id}', {
    responses: {
      '204': {
        description: 'ApprovalMatrixDetail PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ApprovalMatrixDetail, {partial: true}),
        },
      },
    })
    approvalMatrixDetail: ApprovalMatrixDetail,
  ): Promise<void> {
    await this.approvalMatrixDetailRepository.updateById(id, approvalMatrixDetail);
  }

  @put('/approval-matrix-details/{id}', {
    responses: {
      '204': {
        description: 'ApprovalMatrixDetail PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() approvalMatrixDetail: ApprovalMatrixDetail,
  ): Promise<void> {
    await this.approvalMatrixDetailRepository.replaceById(id, approvalMatrixDetail);
  }

  @del('/approval-matrix-details/{id}', {
    responses: {
      '204': {
        description: 'ApprovalMatrixDetail DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.approvalMatrixDetailRepository.deleteById(id);
  }
}
