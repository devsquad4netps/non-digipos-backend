import fs from 'fs';
import path from 'path';
import {ControllerConfig} from './controller.config';

var NAMA_PIC_FINARYA = "";
var JABATAN_PIC_FINARYA = "";
var ALAMAT_FINARYA = "";

export class MerchantTemplateDoc {
  constructor() { }

  //TEMPLATE BAR SKEMA 1

  static TEMPLATE_DOC_BAR_1_DEFAULT(PIC_FINARYA: any, MERCHANT: any, MERCHANT_PRODUCT: any, MERCHANT_SKEMA: any, MERCHANT_TRIBE: any, MERCHANT_GROUP: any, MASTER_DATA: any, SUMMARY_DATA: any, MERCHANT_INVOICE_TYPE: any) {
    var HTML = '<p>Pada ' + ControllerConfig.onDateNow() + ', yang bertanda-tangan dibawah ini:</p>';
    HTML += '<p class="table-pic"><table class="no-border" width="400px">';
    HTML += '   <tbody>';
    HTML += '     <tr>';
    HTML += '       <td width="100px">Nama</td>';
    HTML += '       <td width="20px">:</td>';
    HTML += '       <td>' + PIC_FINARYA.PIC_NAME + '</td>';
    HTML += '     </tr>';

    HTML += '     <tr>';
    HTML += '       <td width="100px">Jabatan</td>';
    HTML += '       <td width="20px">:</td>';
    HTML += '       <td>' + PIC_FINARYA.PIC_JABATAN + '</td>';
    HTML += '     </tr>';

    HTML += '     <tr>';
    HTML += '       <td width="100px">Alamat</td>';
    HTML += '       <td width="20px">:</td>';
    HTML += '       <td>' + PIC_FINARYA.PT_NAME + '<br/>' + PIC_FINARYA.PT_ALAMAT + '</td>';
    HTML += '     </tr>';

    HTML += '   </tbody>';
    HTML += '</table></p>';

    HTML += '<p>Untuk selanjutnya disebut PIHAK I</p>';

    HTML += '<p class="table-pic"><table class="no-border" width="400px">';
    HTML += '   <tbody>';
    HTML += '     <tr>';
    HTML += '       <td width="100px">Nama</td>';
    HTML += '       <td width="20px">:</td>';
    HTML += '       <td>' + MERCHANT.M_PIC_NAME + '</td>';
    HTML += '     </tr>';

    HTML += '     <tr>';
    HTML += '       <td width="100px">Jabatan</td>';
    HTML += '       <td width="20px">:</td>';
    HTML += '       <td>' + MERCHANT.M_PIC_JABATAN + '</td>';
    HTML += '     </tr>';

    HTML += '     <tr>';
    HTML += '       <td width="100px">Alamat</td>';
    HTML += '       <td width="20px">:</td>';
    HTML += '       <td>' + MERCHANT.M_LEGAL_NAME + '<br/>' + MERCHANT.M_ALAMAT_NPWP + '</td>';
    HTML += '     </tr>';

    HTML += '   </tbody>';
    HTML += '</table></p>';

    HTML += '<p>Untuk selanjutnya disebut PIHAK II</p>';

    HTML += '<p>Berdasarkan Surat Perjanjian dengan nomor ' + MERCHANT.M_NO_CONTRACT + ' perihal kerjasama ' + MERCHANT_PRODUCT.MP_DESCRIPTION + ', kedua belah pihak sepakat hasil rekonsiliasi sebagai berikut:</p>';
    HTML += '<p style="text-align:center;">';
    HTML += ' <table class="table-trx bordered" width="100%">';
    HTML += '   <thead>';
    HTML += '     <tr>';
    HTML += '       <td width="150px;">Periode</td>';
    HTML += '       <td>Nilai Transaksi Berhasil</td>';
    HTML += '       <td>Nominal Transaksi Berhasil</td>';
    HTML += '       <td>MDR/Fee/Komisi</td>';
    HTML += '       <td>Total MDR/Fee/Komisi</td>';
    HTML += '     </tr>';
    HTML += '   </thead>';
    HTML += '   <tbody>';

    var TOTAL: number; TOTAL = 0;
    var PPN = 0;
    var DPP = 0;
    var PPH = 0;
    var AMOUNT_TRX = 0;
    var COUNT_TRX = 0;
    var MDR_TRX = 0;

    for (var i in SUMMARY_DATA) {
      var DATA_SUMMARY = SUMMARY_DATA[i];
      var thisDateBAR = new Date(DATA_SUMMARY.PERIODE + "-01");
      var Mount = ControllerConfig.onGetMonth(thisDateBAR.getMonth());

      DPP = Number(MERCHANT_GROUP[DATA_SUMMARY.MPG_ID].MPG_DPP) > DPP ? Number(MERCHANT_GROUP[DATA_SUMMARY.MPG_ID].MPG_DPP) : DPP;
      PPN = Number(MERCHANT_GROUP[DATA_SUMMARY.MPG_ID].MPG_PPN) > DPP ? Number(MERCHANT_GROUP[DATA_SUMMARY.MPG_ID].MPG_PPN) : PPN;
      PPH = Number(MERCHANT_GROUP[DATA_SUMMARY.MPG_ID].MPG_PPH) > PPH ? Number(MERCHANT_GROUP[DATA_SUMMARY.MPG_ID].MPG_PPH) : PPH;


      HTML += '     <tr>';
      HTML += '       <td width="150px;" class="text-string">' + MERCHANT_GROUP[DATA_SUMMARY.MPG_ID].MPG_LABEL + ' ' + Mount + ' ' + thisDateBAR.getFullYear() + '</td>';
      HTML += '       <td class="text-number">Rp. ' + ControllerConfig.onGeneratePuluhan(DATA_SUMMARY.APP_AMOUNT) + '</td>';
      HTML += '       <td class="text-number">' + ControllerConfig.onGeneratePuluhan(DATA_SUMMARY.APP_COUNT_TRX) + '</td>';
      HTML += '       <td class="text-number">Rp. ' + ControllerConfig.onGeneratePuluhan(DATA_SUMMARY.APP_MDR) + '</td>';
      HTML += '       <td class="text-number">Rp. ' + ControllerConfig.onGeneratePuluhan(DATA_SUMMARY.APP_TOTAL) + '</td>';
      HTML += '     </tr>';

      TOTAL += Number(DATA_SUMMARY.APP_TOTAL);
      AMOUNT_TRX += Number(DATA_SUMMARY.APP_AMOUNT);
      COUNT_TRX += Number(DATA_SUMMARY.APP_COUNT_TRX);
      MDR_TRX += Number(DATA_SUMMARY.APP_TOTAL);
    }

    HTML += '   </tbody>';
    HTML += ' </table>';
    HTML += '</p>';
    HTML += '<p>';
    HTML += '<div> Sehingga total tagihan dari PIHAK I kepada PIHAK II  adalah sebesar <b>Rp. ' + ControllerConfig.onGeneratePuluhan(DATA_SUMMARY.APP_TOTAL) + ' <span style="text-transform: capitalize;">( ' + ControllerConfig.onAngkaTerbilang(DATA_SUMMARY.APP_TOTAL) + ' Rupiah ) </span></b> termasuk PPN ' + PPN + ' %';
    HTML += '</div>';
    HTML += '</p>';
    HTML += '<p>';
    HTML += '   <div> Atas total tagihan di atas, akan diterbitkan  <b><i>' + MERCHANT_INVOICE_TYPE[MERCHANT_PRODUCT.MP_TYPE_INVOICE].INVOICE_TYPE_LABEL + '</i></b> dan Faktur Pajak dari PIHAK I ke PIHAK II dan PIHAK II menerbitkan Bukti Potong PPH 23 dengan nilai sebagai berikut : ';
    HTML += '   </div>';
    HTML += '</p>';

    HTML += '<p style="text-align:center;">';
    HTML += ' <table class="table-trx bordered" width="100%">';
    HTML += '   <thead>';
    HTML += '     <tr>';
    HTML += '       <td width:100px;>Pendapatan</td>';
    HTML += '       <td>PPN ' + PPN + '%</td>';
    HTML += '       <td>Nilai MDR</td>';
    HTML += '       <td>PPh 23 ' + PPH + '%</td>';
    HTML += '     </tr>';
    HTML += '   </thead>';
    HTML += '   <tbody>';
    var DPP_TOTAL = Number(TOTAL / DPP);
    var PPN_TOTAL = Number(DPP_TOTAL * (PPN / 100));
    var TOTAL_ALL = DPP_TOTAL + PPN_TOTAL;
    var PPH_TOTAL = DPP_TOTAL * (PPH / 100);
    HTML += '     <tr>';
    HTML += '       <td class="text-number">Rp. ' + ControllerConfig.onGeneratePuluhan(Number(DPP_TOTAL.toFixed(0))) + '</td>';
    HTML += '       <td class="text-number">Rp. ' + ControllerConfig.onGeneratePuluhan(Number(PPN_TOTAL.toFixed(0))) + '</td>';
    HTML += '       <td class="text-number">Rp. ' + ControllerConfig.onGeneratePuluhan(Number(TOTAL_ALL)) + '</td>';
    HTML += '       <td class="text-number">' + ControllerConfig.onGeneratePuluhan(Number(PPH_TOTAL.toFixed(0))) + '</td>';
    HTML += '     </tr>';
    HTML += '   </tbody>';
    HTML += ' </table>';
    HTML += '</p>';

    HTML += '<p>';
    HTML += '   <div>';
    HTML += '     Demikian Berita Acara Rekonsiliasi ini dibuat dalam rangkap 2 (dua) asli dimana masing-masing mempunyai kekuatan hukum yang sama dan ditandatangani kedua belah pihak agar dapat dipergunakan sebagaimana mestinya.';
    HTML += '   </div>';
    HTML += '</p>';


    var CONTENT_SIGN = "";
    const filePath = path.join(__dirname, './../../.non-digipos/attention/' + PIC_FINARYA.PIC_SIGN);
    CONTENT_SIGN = fs.readFileSync(filePath, {encoding: 'base64'});

    HTML += '<p>';
    HTML += '<div style="margin-top:20px;">';
    HTML += ' <div style="float:left; width:50%">';
    HTML += '   <div style="text-align:center;font-weight: bold;">PIHAK I</div>';
    HTML += '   <div style="font-weight: bold;text-align: center;">' + PIC_FINARYA.PT_NAME + '</div>';
    HTML += '   <div style="height:100px;text-align:center;padding:10px;">';
    HTML += CONTENT_SIGN == "" ? "" : '<img src="data:image/png;base64,' + CONTENT_SIGN + '" width="100" height="100" />'
    HTML += '   </div>';
    HTML += '   <div style="font-weight: bold;text-align: center;padding:5px;"><u>' + PIC_FINARYA.PIC_NAME + '</u></div>';
    HTML += '   <div style="font-weight: bold;text-align: center;">' + PIC_FINARYA.PIC_JABATAN + '</div>';
    HTML += ' </div>';
    HTML += ' <div style="float:rightwidth:50%">';
    HTML += '   <div  style="text-align:center;font-weight: bold;">PIHAK II</div>';
    HTML += '   <div style="font-weight: bold;text-align: center;">' + MERCHANT.M_LEGAL_NAME + '</div>';
    HTML += '   <div  style="height:100px;text-align:center;padding:10px;">&nbsp;';
    HTML += '   </div>';
    HTML += '   <div style="font-weight: bold;text-align: center;padding:5px;"><u>' + MERCHANT.M_PIC_NAME + '</u></div>';
    HTML += '   <div style="font-weight: bold;text-align: center;">' + MERCHANT.M_PIC_JABATAN + '</div>';
    HTML += ' </div>';
    HTML += ' <div style="clear:both;"></div>';
    HTML += '<div>';
    HTML += '</p>';

    return HTML;
  }

  static TEMPLATE_DOC_BAR_2_DEFAULT(PIC_FINARYA: any, MERCHANT: any, MERCHANT_PRODUCT: any, MERCHANT_SKEMA: any, MERCHANT_TRIBE: any, MERCHANT_GROUP: any, MASTER_DATA: any, SUMMARY_DATA: any, MERCHANT_INVOICE_TYPE: any) {
    var HTML = '<p>Pada ' + ControllerConfig.onDateNow() + ', yang bertanda-tangan dibawah ini:</p>';
    HTML += '<p class="table-pic"><table class="no-border" width="400px">';
    HTML += '   <tbody>';
    HTML += '     <tr>';
    HTML += '       <td width="100px">Nama</td>';
    HTML += '       <td width="20px">:</td>';
    HTML += '       <td>' + PIC_FINARYA.PIC_NAME + '</td>';
    HTML += '     </tr>';

    HTML += '     <tr>';
    HTML += '       <td width="100px">Jabatan</td>';
    HTML += '       <td width="20px">:</td>';
    HTML += '       <td>' + PIC_FINARYA.PIC_JABATAN + '</td>';
    HTML += '     </tr>';

    HTML += '     <tr>';
    HTML += '       <td width="100px">Alamat</td>';
    HTML += '       <td width="20px">:</td>';
    HTML += '       <td>' + PIC_FINARYA.PT_NAME + '<br/>' + PIC_FINARYA.PT_ALAMAT + '</td>';
    HTML += '     </tr>';

    HTML += '   </tbody>';
    HTML += '</table></p>';

    HTML += '<p>Untuk selanjutnya disebut PIHAK I</p>';

    HTML += '<p class="table-pic"><table class="no-border" width="400px">';
    HTML += '   <tbody>';
    HTML += '     <tr>';
    HTML += '       <td width="100px">Nama</td>';
    HTML += '       <td width="20px">:</td>';
    HTML += '       <td>' + MERCHANT.M_PIC_NAME + '</td>';
    HTML += '     </tr>';

    HTML += '     <tr>';
    HTML += '       <td width="100px">Jabatan</td>';
    HTML += '       <td width="20px">:</td>';
    HTML += '       <td>' + MERCHANT.M_PIC_JABATAN + '</td>';
    HTML += '     </tr>';

    HTML += '     <tr>';
    HTML += '       <td width="100px">Alamat</td>';
    HTML += '       <td width="20px">:</td>';
    HTML += '       <td>' + MERCHANT.M_LEGAL_NAME + '<br/>' + MERCHANT.M_ALAMAT_NPWP + '</td>';
    HTML += '     </tr>';

    HTML += '   </tbody>';
    HTML += '</table></p>';

    HTML += '<p>Untuk selanjutnya disebut PIHAK II</p>';

    HTML += '<p>Berdasarkan Surat Perjanjian dengan nomor ' + MERCHANT.M_NO_CONTRACT + ' perihal kerjasama ' + MERCHANT_PRODUCT.MP_DESCRIPTION + ', kedua belah pihak sepakat hasil rekonsiliasi sebagai berikut:</p>';
    HTML += '<p style="text-align:center;">';
    HTML += ' <table class="table-trx bordered" width="100%">';
    HTML += '   <thead>';
    HTML += '     <tr>';
    HTML += '       <td width="150px;">Periode</td>';
    HTML += '       <td>Nilai Transaksi Berhasil</td>';
    HTML += '       <td>Nominal Transaksi Berhasil</td>';
    HTML += '       <td>Total Nominal MDR setelah dipotong PPh 23</td>';
    HTML += '     </tr>';
    HTML += '   </thead>';
    HTML += '   <tbody>';

    var TOTAL: number; TOTAL = 0;
    var PPN = 0;
    var DPP = 0;
    var PPH = 0;
    var AMOUNT_TRX = 0;
    var COUNT_TRX = 0;
    var MDR_TRX = 0;
    var TOTAL_PPH_NOMINAL = 0;

    for (var i in SUMMARY_DATA) {
      var DATA_SUMMARY = SUMMARY_DATA[i];
      var thisDateBAR = new Date(DATA_SUMMARY.PERIODE + "-01");
      var Mount = ControllerConfig.onGetMonth(thisDateBAR.getMonth());

      DPP = Number(MERCHANT_GROUP[DATA_SUMMARY.MPG_ID].MPG_DPP) > DPP ? Number(MERCHANT_GROUP[DATA_SUMMARY.MPG_ID].MPG_DPP) : DPP;
      PPN = Number(MERCHANT_GROUP[DATA_SUMMARY.MPG_ID].MPG_PPN) > DPP ? Number(MERCHANT_GROUP[DATA_SUMMARY.MPG_ID].MPG_PPN) : PPN;
      PPH = Number(MERCHANT_GROUP[DATA_SUMMARY.MPG_ID].MPG_PPH) > PPH ? Number(MERCHANT_GROUP[DATA_SUMMARY.MPG_ID].MPG_PPH) : PPH;

      var PPH_NOMINAL = DATA_SUMMARY.APP_TOTAL - DATA_SUMMARY.APP_PPH;

      HTML += '     <tr>';
      HTML += '       <td width="150px;" class="text-string">' + MERCHANT_GROUP[DATA_SUMMARY.MPG_ID].MPG_LABEL + ' ' + Mount + ' ' + thisDateBAR.getFullYear() + '</td>';
      HTML += '       <td class="text-number">' + ControllerConfig.onGeneratePuluhan(DATA_SUMMARY.APP_COUNT_TRX) + '</td>';
      HTML += '       <td class="text-number">Rp. ' + ControllerConfig.onGeneratePuluhan(DATA_SUMMARY.APP_MDR) + '</td>';
      HTML += '       <td class="text-number">Rp. ' + ControllerConfig.onGeneratePuluhan(Number(PPH_NOMINAL.toFixed(0))) + '</td>';
      HTML += '     </tr>';

      TOTAL_PPH_NOMINAL += PPH_NOMINAL;
      TOTAL += Number(DATA_SUMMARY.APP_TOTAL);
      AMOUNT_TRX += Number(DATA_SUMMARY.APP_AMOUNT);
      COUNT_TRX += Number(DATA_SUMMARY.APP_COUNT_TRX);
      MDR_TRX += Number(DATA_SUMMARY.APP_TOTAL);
    }

    HTML += '   </tbody>';
    HTML += ' </table>';
    HTML += '</p>';
    HTML += '<p>';
    HTML += '<div> Sehingga total tagihan dari PIHAK I kepada PIHAK II adalah sebesar  <b>Rp. ' + ControllerConfig.onGeneratePuluhan(Number(TOTAL_PPH_NOMINAL.toFixed(0))) + ' <span style="text-transform: capitalize;">( ' + ControllerConfig.onAngkaTerbilang(Number(TOTAL_PPH_NOMINAL.toFixed(0))) + ' Rupiah ) </span></b> setelah dipotong PPh 23.';
    HTML += '</div>';
    HTML += '</p>';
    HTML += '<p>';
    HTML += '   <div> Atas pendapatan PIHAK I yang disebutkan di atas sudah diperoleh melalui auto deduction akan diterbitkan  <b><i>' + MERCHANT_INVOICE_TYPE[MERCHANT_PRODUCT.MP_TYPE_INVOICE].INVOICE_TYPE_LABEL + '</i></b> dan faktur pajak dari PIHAK I dimana PIHAK II akan menerbitkan bukti potong PPh 23 dengan rincian sebagai berikut:';
    HTML += '   </div>';
    HTML += '</p>';

    HTML += '<p style="text-align:center;">';
    HTML += ' <table class="table-trx bordered" width="100%">';
    HTML += '   <thead>';
    HTML += '     <tr>';
    HTML += '       <td width="150px;">Pendapatan</td>';
    HTML += '       <td>Pendapatan Gross Up</td>';
    HTML += '       <td>PPN ' + PPN + '%</td>';
    HTML += '       <td>Pendapatan termasuk PPN ' + PPN + '%</td>';
    HTML += '     </tr>';
    HTML += '   </thead>';
    HTML += '   <tbody>';
    var DPP_TOTAL = Number(TOTAL / DPP);
    var PPN_TOTAL = Number(DPP_TOTAL * (PPN / 100));
    var TOTAL_ALL = DPP_TOTAL + PPN_TOTAL;
    HTML += '     <tr>';
    HTML += '       <td class="text-number">Rp. ' + ControllerConfig.onGeneratePuluhan(Number(DPP_TOTAL.toFixed(0))) + '</td>';
    HTML += '       <td class="text-number">' + ControllerConfig.onGeneratePuluhan(Number(DPP.toFixed(0))) + '</td>';
    HTML += '       <td class="text-number">Rp. ' + ControllerConfig.onGeneratePuluhan(Number(PPN_TOTAL.toFixed(0))) + '</td>';
    HTML += '       <td class="text-number">Rp. ' + ControllerConfig.onGeneratePuluhan(Number(TOTAL_ALL.toFixed(0))) + '</td>';
    HTML += '     </tr>';
    HTML += '   </tbody>';
    HTML += ' </table>';
    HTML += '</p>';

    HTML += '<p>';
    HTML += '   <div>';
    HTML += '     Demikian Berita Acara Rekonsiliasi ini dibuat dalam rangkap 2 (dua) asli dimana masing-masing mempunyai kekuatan hukum yang sama dan ditandatangani kedua belah pihak agar dapat dipergunakan sebagaimana mestinya.';
    HTML += '   </div>';
    HTML += '</p>';


    var CONTENT_SIGN = "";
    // if (MASTER_DATA.GENERATE_BAR_SIGN != 0) {
    const filePath = path.join(__dirname, './../../.non-digipos/attention/' + PIC_FINARYA.PIC_SIGN);
    CONTENT_SIGN = fs.readFileSync(filePath, {encoding: 'base64'});
    // }

    HTML += '<p>';
    HTML += '<div style="margin-top:20px;">';
    HTML += ' <div style="float:left; width:50%">';
    HTML += '   <div style="text-align:center;font-weight: bold;">PIHAK I</div>';
    HTML += '   <div style="font-weight: bold;text-align: center;">' + PIC_FINARYA.PT_NAME + '</div>';
    HTML += '   <div style="height:100px;text-align:center;padding:10px;">';
    HTML += CONTENT_SIGN == "" ? "" : '<img src="data:image/png;base64,' + CONTENT_SIGN + '" width="100" height="100" />'
    HTML += '   </div>';
    HTML += '   <div style="font-weight: bold;text-align: center;padding:5px;"><u>' + PIC_FINARYA.PIC_NAME + '</u></div>';
    HTML += '   <div style="font-weight: bold;text-align: center;">' + PIC_FINARYA.PIC_JABATAN + '</div>';
    HTML += ' </div>';
    HTML += ' <div style="float:rightwidth:50%">';
    HTML += '   <div  style="text-align:center;font-weight: bold;">PIHAK II</div>';
    HTML += '   <div style="font-weight: bold;text-align: center;">' + MERCHANT.M_LEGAL_NAME + '</div>';
    HTML += '   <div  style="height:100px;text-align:center;padding:10px;">&nbsp;';
    HTML += '   </div>';
    HTML += '   <div style="font-weight: bold;text-align: center;padding:5px;"><u>' + MERCHANT.M_PIC_NAME + '</u></div>';
    HTML += '   <div style="font-weight: bold;text-align: center;">' + MERCHANT.M_PIC_JABATAN + '</div>';
    HTML += ' </div>';
    HTML += ' <div style="clear:both;"></div>';
    HTML += '<div>';
    HTML += '</p>';

    return HTML;
  }

  static TEMPLATE_DOC_BAR_3_DEFAULT(PIC_FINARYA: any, MERCHANT: any, MERCHANT_PRODUCT: any, MERCHANT_SKEMA: any, MERCHANT_TRIBE: any, MERCHANT_GROUP: any, MASTER_DATA: any, SUMMARY_DATA: any, MERCHANT_INVOICE_TYPE: any) {
    var HTML = '<p>Pada ' + ControllerConfig.onDateNow() + ', yang bertanda-tangan dibawah ini:</p>';
    HTML += '<p class="table-pic"><table class="no-border" width="400px">';
    HTML += '   <tbody>';
    HTML += '     <tr>';
    HTML += '       <td width="100px">Nama</td>';
    HTML += '       <td width="20px">:</td>';
    HTML += '       <td>' + PIC_FINARYA.PIC_NAME + '</td>';
    HTML += '     </tr>';

    HTML += '     <tr>';
    HTML += '       <td width="100px">Jabatan</td>';
    HTML += '       <td width="20px">:</td>';
    HTML += '       <td>' + PIC_FINARYA.PIC_JABATAN + '</td>';
    HTML += '     </tr>';

    HTML += '     <tr>';
    HTML += '       <td width="100px">Alamat</td>';
    HTML += '       <td width="20px">:</td>';
    HTML += '       <td>' + PIC_FINARYA.PT_NAME + '<br/>' + PIC_FINARYA.PT_ALAMAT + '</td>';
    HTML += '     </tr>';

    HTML += '   </tbody>';
    HTML += '</table></p>';

    HTML += '<p>Untuk selanjutnya disebut PIHAK I</p>';

    HTML += '<p class="table-pic"><table class="no-border" width="400px">';
    HTML += '   <tbody>';
    HTML += '     <tr>';
    HTML += '       <td width="100px">Nama</td>';
    HTML += '       <td width="20px">:</td>';
    HTML += '       <td>' + MERCHANT.M_PIC_NAME + '</td>';
    HTML += '     </tr>';

    HTML += '     <tr>';
    HTML += '       <td width="100px">Jabatan</td>';
    HTML += '       <td width="20px">:</td>';
    HTML += '       <td>' + MERCHANT.M_PIC_JABATAN + '</td>';
    HTML += '     </tr>';

    HTML += '     <tr>';
    HTML += '       <td width="100px">Alamat</td>';
    HTML += '       <td width="20px">:</td>';
    HTML += '       <td>' + MERCHANT.M_LEGAL_NAME + '<br/>' + MERCHANT.M_ALAMAT_NPWP + '</td>';
    HTML += '     </tr>';

    HTML += '   </tbody>';
    HTML += '</table></p>';

    HTML += '<p>Untuk selanjutnya disebut PIHAK II</p>';

    HTML += '<p>Berdasarkan Surat Perjanjian dengan nomor ' + MERCHANT.M_NO_CONTRACT + ' perihal kerjasama ' + MERCHANT_PRODUCT.MP_DESCRIPTION + ', kedua belah pihak sepakat hasil rekonsiliasi sebagai berikut:</p>';
    HTML += '<p style="text-align:center;">';
    HTML += ' <table class="table-trx bordered" width="100%">';
    HTML += '   <thead>';
    HTML += '     <tr>';
    HTML += '       <td width="150px;">Periode</td>';
    HTML += '       <td>MDR</td>';
    HTML += '       <td>Nominal Transaksi Berhasil</td>';
    HTML += '       <td>Total Pendapatan</td>';
    HTML += '     </tr>';
    HTML += '   </thead>';
    HTML += '   <tbody>';

    var TOTAL: number; TOTAL = 0;
    var PPN = 0;
    var DPP = 0;
    var PPH = 0;
    var AMOUNT_TRX = 0;
    var COUNT_TRX = 0;
    var MDR_TRX = 0;

    for (var i in SUMMARY_DATA) {
      var DATA_SUMMARY = SUMMARY_DATA[i];
      var thisDateBAR = new Date(DATA_SUMMARY.PERIODE + "-01");
      var Mount = ControllerConfig.onGetMonth(thisDateBAR.getMonth());

      DPP = Number(MERCHANT_GROUP[DATA_SUMMARY.MPG_ID].MPG_DPP) > DPP ? Number(MERCHANT_GROUP[DATA_SUMMARY.MPG_ID].MPG_DPP) : DPP;
      PPN = Number(MERCHANT_GROUP[DATA_SUMMARY.MPG_ID].MPG_PPN) > DPP ? Number(MERCHANT_GROUP[DATA_SUMMARY.MPG_ID].MPG_PPN) : PPN;
      PPH = Number(MERCHANT_GROUP[DATA_SUMMARY.MPG_ID].MPG_PPH) > PPH ? Number(MERCHANT_GROUP[DATA_SUMMARY.MPG_ID].MPG_PPH) : PPH;


      HTML += '     <tr>';
      HTML += '       <td width="150px;" class="text-string">' + MERCHANT_GROUP[DATA_SUMMARY.MPG_ID].MPG_LABEL + ' ' + Mount + ' ' + thisDateBAR.getFullYear() + '</td>';
      HTML += '       <td class="text-number">' + ControllerConfig.onGeneratePuluhan(DATA_SUMMARY.APP_COUNT_TRX) + '</td>';
      HTML += '       <td class="text-number">Rp. ' + ControllerConfig.onGeneratePuluhan(DATA_SUMMARY.APP_MDR) + '</td>';
      HTML += '       <td class="text-number">Rp. ' + ControllerConfig.onGeneratePuluhan(DATA_SUMMARY.APP_TOTAL) + '</td>';
      HTML += '     </tr>';

      TOTAL += Number(DATA_SUMMARY.APP_TOTAL);
      AMOUNT_TRX += Number(DATA_SUMMARY.APP_AMOUNT);
      COUNT_TRX += Number(DATA_SUMMARY.APP_COUNT_TRX);
      MDR_TRX += Number(DATA_SUMMARY.APP_TOTAL);
    }

    HTML += '   </tbody>';
    HTML += ' </table>';
    HTML += '</p>';
    HTML += '<p>';
    HTML += '<div> Sehingga total tagihan dari PIHAK I kepada PIHAK II  dalam bentuk <b><i>' + MERCHANT_INVOICE_TYPE[MERCHANT_PRODUCT.MP_TYPE_INVOICE].INVOICE_TYPE_LABEL + '</i></b>  adalah sebesar <b>Rp. ' + ControllerConfig.onGeneratePuluhan(DATA_SUMMARY.APP_TOTAL) + ' <span style="text-transform: capitalize;">( ' + ControllerConfig.onAngkaTerbilang(DATA_SUMMARY.APP_TOTAL) + ' Rupiah ) </span></b> belum termasuk PPN dan PPH 23.';
    HTML += '</div>';
    HTML += '</p>';

    HTML += '<p style="text-align:center;">';
    HTML += ' <table class="table-trx bordered" width="100%">';
    HTML += '   <thead>';
    HTML += '     <tr>';
    HTML += '       <td width="150px;">Pendapatan</td>';
    HTML += '       <td>Pendapatan Gross Up</td>';
    HTML += '       <td>PPN ' + PPN + '%</td>';
    HTML += '       <td>Pendapatan termasuk PPN ' + PPN + '%</td>';
    HTML += '     </tr>';
    HTML += '   </thead>';
    HTML += '   <tbody>';
    var DPP_TOTAL = Number(TOTAL / DPP);
    var PPN_TOTAL = Number(DPP_TOTAL * (PPN / 100));
    var TOTAL_ALL = DPP_TOTAL + PPN_TOTAL;
    HTML += '     <tr>';
    HTML += '       <td class="text-number">Rp. ' + ControllerConfig.onGeneratePuluhan(Number(DPP_TOTAL.toFixed(0))) + '</td>';
    HTML += '       <td class="text-number">' + ControllerConfig.onGeneratePuluhan(Number(DPP.toFixed(0))) + '</td>';
    HTML += '       <td class="text-number">Rp. ' + ControllerConfig.onGeneratePuluhan(Number(PPN_TOTAL.toFixed(0))) + '</td>';
    HTML += '       <td class="text-number">Rp. ' + ControllerConfig.onGeneratePuluhan(Number(TOTAL_ALL.toFixed(0))) + '</td>';
    HTML += '     </tr>';
    HTML += '   </tbody>';
    HTML += ' </table>';
    HTML += '</p>';

    HTML += '<p>';
    HTML += '   <div>';
    HTML += '     Demikian Berita Acara Rekonsiliasi ini dibuat dalam rangkap 2 (dua) asli dimana masing-masing mempunyai kekuatan hukum yang sama dan ditandatangani kedua belah pihak agar dapat dipergunakan sebagaimana mestinya.';
    HTML += '   </div>';
    HTML += '</p>';


    var CONTENT_SIGN = "";
    // if (MASTER_DATA.GENERATE_BAR_SIGN != 0) {
    const filePath = path.join(__dirname, './../../.non-digipos/attention/' + PIC_FINARYA.PIC_SIGN);
    CONTENT_SIGN = fs.readFileSync(filePath, {encoding: 'base64'});
    // }

    HTML += '<p>';
    HTML += '<div style="margin-top:20px;">';
    HTML += ' <div style="float:left; width:50%">';
    HTML += '   <div style="text-align:center;font-weight: bold;">PIHAK I</div>';
    HTML += '   <div style="font-weight: bold;text-align: center;">' + PIC_FINARYA.PT_NAME + '</div>';
    HTML += '   <div style="height:100px;text-align:center;padding:10px;">';
    HTML += CONTENT_SIGN == "" ? "" : '<img src="data:image/png;base64,' + CONTENT_SIGN + '" width="100" height="100" />'
    HTML += '   </div>';
    HTML += '   <div style="font-weight: bold;text-align: center;padding:5px;"><u>' + PIC_FINARYA.PIC_NAME + '</u></div>';
    HTML += '   <div style="font-weight: bold;text-align: center;">' + PIC_FINARYA.PIC_JABATAN + '</div>';
    HTML += ' </div>';
    HTML += ' <div style="float:rightwidth:50%">';
    HTML += '   <div  style="text-align:center;font-weight: bold;">PIHAK II</div>';
    HTML += '   <div style="font-weight: bold;text-align: center;">' + MERCHANT.M_LEGAL_NAME + '</div>';
    HTML += '   <div  style="height:100px;text-align:center;padding:10px;">&nbsp;';
    HTML += '   </div>';
    HTML += '   <div style="font-weight: bold;text-align: center;padding:5px;"><u>' + MERCHANT.M_PIC_NAME + '</u></div>';
    HTML += '   <div style="font-weight: bold;text-align: center;">' + MERCHANT.M_PIC_JABATAN + '</div>';
    HTML += ' </div>';
    HTML += ' <div style="clear:both;"></div>';
    HTML += '<div>';
    HTML += '</p>';

    return HTML;
  }



}
