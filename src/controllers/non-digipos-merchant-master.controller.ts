// // Uncomment these imports to begin using these cool features!

// import {Filter, FilterExcludingWhere, repository} from '@loopback/repository';
// import {del, get, getModelSchemaRef, param, post, put, requestBody} from '@loopback/rest';
// import {MerchantMaster} from '../models';
// import {MerchantMasterRepository} from '../repositories';
// import {ControllerConfig} from './controller.config';

// // import {inject} from '@loopback/core';


// export class NonDigiposMerchantMasterController {
//   constructor(
//     @repository(MerchantMasterRepository)
//     public merchantMasterRepository: MerchantMasterRepository,
//   ) { }


//   @get('/non-digipos-merchant-master', {
//     responses: {
//       '200': {
//         description: 'Array of non digipos merchant master type model instances',
//         content: {
//           'application/json': {
//             schema: {
//               type: 'array',
//               items: getModelSchemaRef(MerchantMaster, {includeRelations: true}),
//             },
//           },
//         },
//       },
//     },
//   })
//   async find(
//     @param.filter(MerchantMaster) filter?: Filter<MerchantMaster>,
//   ): Promise<MerchantMaster[]> {
//     return this.merchantMasterRepository.find(filter);
//   }

//   @get('/non-digipos-merchant-master/{id}', {
//     responses: {
//       '200': {
//         description: 'non digipos variable type model instance',
//         content: {
//           'application/json': {
//             schema: getModelSchemaRef(MerchantMaster, {includeRelations: true}),
//           },
//         },
//       },
//     },
//   })
//   async findById(
//     @param.path.number('id') id: number,
//     @param.filter(MerchantMaster, {exclude: 'where'}) filter?: FilterExcludingWhere<MerchantMaster>
//   ): Promise<MerchantMaster> {
//     return this.merchantMasterRepository.findById(id, filter);
//   }



//   @post('/non-digipos-merchant-master/create', {
//     responses: {
//       '200': {
//         description: 'Merchan Master model instance',
//         content: {'application/json': {schema: getModelSchemaRef(MerchantMaster)}},
//       },
//     },
//   })
//   async create(
//     @requestBody({
//       content: {
//         'application/json': {
//           schema: getModelSchemaRef(MerchantMaster, {
//             title: 'NewMerchanMaster',
//             exclude: ['mm_id', 'mm_merchant_id', 'mm_at_create', 'mm_at_update'],
//           }),
//         },
//       },
//     })
//     variable: Omit<MerchantMaster, 'mm_id'>,
//   ): Promise<Object> {
//     var result: any; result = {success: false, message: "", data: {}};
//     try {
//       var infoCreate = await this.merchantMasterRepository.create(variable);
//       var id = infoCreate.mm_id == undefined ? "0" : infoCreate.mm_id;
//       var Zero = "0000"
//       var ColumnId = "M" + Zero.substring(id.toString().length, 4) + id.toString();
//       var infoUpdate = await this.merchantMasterRepository.updateById(infoCreate.mm_id, {
//         mm_merchant_id: ColumnId,
//         mm_at_create: ControllerConfig.onCurrentTime().toString()
//       });
//       result.success = true;
//       result.message = "Success, create merchant master.";
//       result.data = infoUpdate;
//     } catch (error) {
//       result.success = false;
//       result.message = error;
//     }
//     return result;
//   }

//   @put('/non-digipos-merchant-master/update/{id}', {
//     responses: {
//       '200': {
//         description: 'Merchan Variable model instance',
//         content: {'application/json': {schema: getModelSchemaRef(MerchantMaster)}},
//       },
//     },
//   })
//   async updateById(
//     @param.path.number('id') id: number,
//     @requestBody({
//       content: {
//         'application/json': {
//           schema: getModelSchemaRef(MerchantMaster, {
//             title: 'NewMerchanMaster',
//             exclude: ['mm_id', 'mm_merchant_id', 'mm_at_create', 'mm_at_update'],
//           }),
//         },
//       },
//     }) merchanMaster: any,
//   ): Promise<Object> {
//     var result: any; result = {success: false, message: "", data: {}};
//     try {
//       var dataUpdate: any; dataUpdate = {};
//       for (var i in merchanMaster) {
//         dataUpdate[i] = merchanMaster[i];
//       }
//       dataUpdate["mm_at_update"] = ControllerConfig.onCurrentTime().toString();
//       var updateProduct = await this.merchantMasterRepository.updateById(id, dataUpdate);
//       result.success = true;
//       result.message = "Success, update merchant master.";
//       result.data = updateProduct;
//     } catch (error) {
//       result.success = false;
//       result.message = error;
//     }
//     return result;
//   }

//   @del('/non-digipos-merchant-master/delete/{id}', {
//     responses: {
//       '200': {
//         description: 'Merchant master DELETE success',
//       },
//     },
//   })
//   async deleteById(@param.path.number('id') id: number): Promise<Object> {
//     var result: any; result = {success: false, message: ""};
//     await this.merchantMasterRepository.deleteById(id);
//     result.success = true;
//     result.message = "Success, delete  merchant master.";
//     return result;
//   }


// }
