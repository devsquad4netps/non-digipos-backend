import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {ApprovalNota} from '../models';
import {ApprovalNotaRepository} from '../repositories';

export class ApprovalNotaActionController {
  constructor(
    @repository(ApprovalNotaRepository)
    public approvalNotaRepository : ApprovalNotaRepository,
  ) {}

  @post('/approval-notas', {
    responses: {
      '200': {
        description: 'ApprovalNota model instance',
        content: {'application/json': {schema: getModelSchemaRef(ApprovalNota)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ApprovalNota, {
            title: 'NewApprovalNota',
            exclude: ['id'],
          }),
        },
      },
    })
    approvalNota: Omit<ApprovalNota, 'id'>,
  ): Promise<ApprovalNota> {
    return this.approvalNotaRepository.create(approvalNota);
  }

  @get('/approval-notas/count', {
    responses: {
      '200': {
        description: 'ApprovalNota model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(ApprovalNota) where?: Where<ApprovalNota>,
  ): Promise<Count> {
    return this.approvalNotaRepository.count(where);
  }

  @get('/approval-notas', {
    responses: {
      '200': {
        description: 'Array of ApprovalNota model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(ApprovalNota, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(ApprovalNota) filter?: Filter<ApprovalNota>,
  ): Promise<ApprovalNota[]> {
    return this.approvalNotaRepository.find(filter);
  }

  @patch('/approval-notas', {
    responses: {
      '200': {
        description: 'ApprovalNota PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ApprovalNota, {partial: true}),
        },
      },
    })
    approvalNota: ApprovalNota,
    @param.where(ApprovalNota) where?: Where<ApprovalNota>,
  ): Promise<Count> {
    return this.approvalNotaRepository.updateAll(approvalNota, where);
  }

  @get('/approval-notas/{id}', {
    responses: {
      '200': {
        description: 'ApprovalNota model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(ApprovalNota, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(ApprovalNota, {exclude: 'where'}) filter?: FilterExcludingWhere<ApprovalNota>
  ): Promise<ApprovalNota> {
    return this.approvalNotaRepository.findById(id, filter);
  }

  @patch('/approval-notas/{id}', {
    responses: {
      '204': {
        description: 'ApprovalNota PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ApprovalNota, {partial: true}),
        },
      },
    })
    approvalNota: ApprovalNota,
  ): Promise<void> {
    await this.approvalNotaRepository.updateById(id, approvalNota);
  }

  @put('/approval-notas/{id}', {
    responses: {
      '204': {
        description: 'ApprovalNota PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() approvalNota: ApprovalNota,
  ): Promise<void> {
    await this.approvalNotaRepository.replaceById(id, approvalNota);
  }

  @del('/approval-notas/{id}', {
    responses: {
      '204': {
        description: 'ApprovalNota DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.approvalNotaRepository.deleteById(id);
  }
}
