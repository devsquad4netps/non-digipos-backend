import {
  repository
} from '@loopback/repository';
import {TblAdMasterRepository} from '../repositories';



export class AdmasterController {
  constructor(
    @repository(TblAdMasterRepository)
    public tblAdMasterRepository: TblAdMasterRepository,
  ) {}

  // @post('/admaster', {
  //   responses: {
  //     '200': {
  //       description: 'TblAdMaster model instance',
  //       content: {'application/json': {schema: getModelSchemaRef(TblAdMaster)}},
  //     },
  //   },
  // })
  // async create(
  //   // @requestBody({
  //   //   content: {
  //   //     'application/json': {
  //   //       schema: getModelSchemaRef(TblAdMaster, {
  //   //         title: 'NewTblAdMaster',
  //   //         exclude: ['ad_code'],
  //   //       }),
  //   //     },
  //   //   },
  //   // })
  //   // tblAdMaster: Omit<TblAdMaster, 'ad_code'>,
  //   @requestBody.file()
  //   request: Request,
  //   @inject(RestBindings.Http.RESPONSE) response: Response,
  // ): Promise<Object> {
  //   // return this.tblAdMasterRepository.create(tblAdMaster);
  //   console.log(request);
  //   console.log(response);
  //   return {};
  // }

  // @get('/admaster/count', {
  //   responses: {
  //     '200': {
  //       description: 'TblAdMaster model count',
  //       content: {'application/json': {schema: CountSchema}},
  //     },
  //   },
  // })

  // async count(
  //   @param.where(TblAdMaster) where?: Where<TblAdMaster>,
  // ): Promise<Count> {
  //   return this.tblAdMasterRepository.count(where);
  // }

  // @get('/admaster', {
  //   responses: {
  //     '200': {
  //       description: 'Array of TblAdMaster model instances',
  //       content: {
  //         'application/json': {
  //           schema: {
  //             type: 'array',
  //             items: getModelSchemaRef(TblAdMaster, {includeRelations: true}),
  //           },
  //         },
  //       },
  //     },
  //   },
  // })
  // async find(
  //   @param.filter(TblAdMaster) filter?: Filter<TblAdMaster>,
  // ): Promise<Object> {
  //   var info = {"datas": new Array(), "size": 0};
  //   var filterOrder = {};
  //   if (filter?.where !== undefined) {
  //     filterOrder = {"where": filter?.where};
  //   }
  //   var infoMasterAD = await this.tblAdMasterRepository.find(filter);
  //   var sizeMasterAD = await this.tblAdMasterRepository.find(filterOrder);
  //   info["datas"] = infoMasterAD;
  //   info["size"] = sizeMasterAD.length;
  //   return info;
  // }

  // @patch('/admaster', {
  //   responses: {
  //     '200': {
  //       description: 'TblAdMaster PATCH success count',
  //       content: {'application/json': {schema: CountSchema}},
  //     },
  //   },
  // })
  // async updateAll(
  //   @requestBody({
  //     content: {
  //       'application/json': {
  //         schema: getModelSchemaRef(TblAdMaster, {partial: true}),
  //       },
  //     },
  //   })
  //   tblAdMaster: TblAdMaster,
  //   @param.where(TblAdMaster) where?: Where<TblAdMaster>,
  // ): Promise<Count> {
  //   return this.tblAdMasterRepository.updateAll(tblAdMaster, where);
  // }

  // @get('/admaster/{id}', {
  //   responses: {
  //     '200': {
  //       description: 'TblAdMaster model instance',
  //       content: {
  //         'application/json': {
  //           schema: getModelSchemaRef(TblAdMaster, {includeRelations: true}),
  //         },
  //       },
  //     },
  //   },
  // })
  // async findById(
  //   @param.path.number('id') id: string,
  //   @param.filter(TblAdMaster, {exclude: 'where'}) filter?: FilterExcludingWhere<TblAdMaster>
  // ): Promise<TblAdMaster> {
  //   return this.tblAdMasterRepository.findById(id, filter);
  // }

  // @patch('/admaster/{id}', {
  //   responses: {
  //     '204': {
  //       description: 'TblAdMaster PATCH success',
  //     },
  //   },
  // })
  // async updateById(
  //   @param.path.number('id') id: string,
  //   @requestBody({
  //     content: {
  //       'application/json': {
  //         schema: getModelSchemaRef(TblAdMaster, {partial: true}),
  //       },
  //     },
  //   })
  //   tblAdMaster: TblAdMaster,
  // ): Promise<void> {
  //   await this.tblAdMasterRepository.updateById(id, tblAdMaster);
  // }

  // @put('/admaster/{id}', {
  //   responses: {
  //     '204': {
  //       description: 'TblAdMaster PUT success',
  //     },
  //   },
  // })
  // async replaceById(
  //   @param.path.number('id') id: string,
  //   @requestBody() tblAdMaster: TblAdMaster,
  // ): Promise<void> {
  //   await this.tblAdMasterRepository.replaceById(id, tblAdMaster);
  // }

  // @del('/admaster/{id}', {
  //   responses: {
  //     '204': {
  //       description: 'TblAdMaster DELETE success',
  //     },
  //   },
  // })
  // async deleteById(@param.path.number('id') id: string): Promise<void> {
  //   await this.tblAdMasterRepository.deleteById(id);
  // }
}
