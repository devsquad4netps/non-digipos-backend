import {authenticate} from '@loopback/authentication';
import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where
} from '@loopback/repository';
import {
  del, get,
  getModelSchemaRef, param,


  patch, post,




  put,

  requestBody
} from '@loopback/rest';
import {Menuaccess} from '../models';
import {MenuaccessRepository} from '../repositories';


@authenticate('jwt')
export class MenuaccessController {
  constructor(
    @repository(MenuaccessRepository)
    public menuaccessRepository: MenuaccessRepository,
  ) {}

  @post('/MenuAccess', {
    responses: {
      '200': {
        description: 'Menuaccess model instance',
        content: {'application/json': {schema: getModelSchemaRef(Menuaccess)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Menuaccess, {
            title: 'NewMenuaccess',
            exclude: ['id'],
          }),
        },
      },
    })
    menuaccess: Omit<Menuaccess, 'id'>,
  ): Promise<Menuaccess> {
    return this.menuaccessRepository.create(menuaccess);
  }

  @get('/MenuAccess/count', {
    responses: {
      '200': {
        description: 'Menuaccess model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(Menuaccess) where?: Where<Menuaccess>,
  ): Promise<Count> {
    return this.menuaccessRepository.count(where);
  }

  @get('/MenuAccess', {
    responses: {
      '200': {
        description: 'Array of Menuaccess model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Menuaccess, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(Menuaccess) filter?: Filter<Menuaccess>,
  ): Promise<Menuaccess[]> {
    return this.menuaccessRepository.find(filter);
  }

  @patch('/MenuAccess', {
    responses: {
      '200': {
        description: 'Menuaccess PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Menuaccess, {partial: true}),
        },
      },
    })
    menuaccess: Menuaccess,
    @param.where(Menuaccess) where?: Where<Menuaccess>,
  ): Promise<Count> {
    return this.menuaccessRepository.updateAll(menuaccess, where);
  }

  @get('/MenuAccess/{id}', {
    responses: {
      '200': {
        description: 'Menuaccess model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Menuaccess, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(Menuaccess, {exclude: 'where'}) filter?: FilterExcludingWhere<Menuaccess>
  ): Promise<Menuaccess> {
    return this.menuaccessRepository.findById(id, filter);
  }

  @patch('/MenuAccess/{id}', {
    responses: {
      '204': {
        description: 'Menuaccess PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Menuaccess, {partial: true}),
        },
      },
    })
    menuaccess: Menuaccess,
  ): Promise<void> {
    await this.menuaccessRepository.updateById(id, menuaccess);
  }

  @put('/MenuAccess/{id}', {
    responses: {
      '204': {
        description: 'Menuaccess PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() menuaccess: Menuaccess,
  ): Promise<void> {
    await this.menuaccessRepository.replaceById(id, menuaccess);
  }

  @del('/MenuAccess/{id}', {
    responses: {
      '204': {
        description: 'Menuaccess DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.menuaccessRepository.deleteById(id);
  }
}
