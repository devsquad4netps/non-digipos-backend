import {authenticate} from '@loopback/authentication';
import {inject} from '@loopback/core';
import {get, Request, ResponseObject, RestBindings} from '@loopback/rest';
import {ControllerConfig} from './controller.config';
// import {MailConfig} from './mail.config';
/**
 * OpenAPI response for ping()
 */
const PING_RESPONSE: ResponseObject = {
  description: 'Ping Response',
  content: {
    'application/json': {
      schema: {
        type: 'object',
        title: 'PingResponse',
        properties: {
          greeting: {type: 'string'},
          date: {type: 'string'},
          url: {type: 'string'},
          headers: {
            type: 'object',
            properties: {
              'Content-Type': {type: 'string'},
            },
            additionalProperties: true,
          },
        },
      },
    },
  },
};

/**
 * A simple controller to bounce back http requests
 */
export class PingController {
  constructor(@inject(RestBindings.Http.REQUEST) private req: Request) { }

  // Map to `GET /ping`


  @authenticate('jwt')
  @get('/ping', {
    responses: {
      '200': PING_RESPONSE,
    },
  })
  async ping(): Promise<object> {
    // Reply with a greeting, the current time, the url, and request headers
    ControllerConfig.onHelloAll();

    // let testAccount = await nodemailer.createTestAccount();
    // let transporter = nodemailer.createTransport({
    //   host: "smtp.gmail.com",
    //   port: 465,
    //   secure: true, // true for 465, false for other ports
    //   auth: {
    //     user: 'digipos@linkaja-ba.tools', // generated ethereal user
    //     pass: 'W3lc0m3#1234', // generated ethereal password
    //   },
    // });

    // var message = {
    //   sender: "digipos@linkaja-ba.tools",
    //   to: 'yudhistira.job@gmail.com',
    //   subject: 'test',
    //   html: '<h1>test</h1>',
    //   attachments: []
    // };

    // let info = await transporter.sendMail({
    //   from: '"Fred Foo 👻" <digipos@linkaja-ba.tools>', // sender address
    //   to: "yudhistira.job@gmail.com", // list of receivers
    //   subject: "Hello ✔", // Subject line
    //   text: "Hello world?", // plain text body
    //   html: "<b>Hello world?</b>", // html body
    // });

    // console.log("Message sent: %s", info.messageId);
    // await MailConfig.onSendMailForgotPassword("yudhistira.job@gmail.com", {
    //   username: "@zzzzzzz",
    //   token: "UUJSHS",
    //   link_forgot: "AASJSH"
    // }, (r: any) => {
    //   console.log(r);
    // });

    return {
      greeting: 'Hello from LoopBack V 2.0',
      date: new Date(),
      url: this.req.url,
      headers: Object.assign({}, this.req.headers),
    };
  }
}
