import {authenticate} from '@loopback/authentication';
import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where
} from '@loopback/repository';
import {
  del, get,
  getModelSchemaRef, param,


  patch, post,




  put,

  requestBody
} from '@loopback/rest';
import {NotaDebet} from '../models';
import {AdMasterRepository, NotaDebetRepository} from '../repositories';


@authenticate('jwt')
export class NotaDebetActionController {
  constructor(
    @repository(NotaDebetRepository)
    public notaDebetRepository: NotaDebetRepository,
    @repository(AdMasterRepository)
    public adMasterRepository: AdMasterRepository,
  ) { }


  onNotaDebet = async () => {
    var DataMaster = await this.adMasterRepository.find();
    // for (var i in DataMaster) {
    //   var infoDataMaster = DataMaster[i];
    //   var NotaDirectory = infoDataMaster.gd_code_nota_debet;
    //   if (NotaDirectory != "" || NotaDirectory != null) {

    //   }
    // }
  }

  @post('/nota-debets', {
    responses: {
      '200': {
        description: 'NotaDebet model instance',
        content: {'application/json': {schema: getModelSchemaRef(NotaDebet)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(NotaDebet, {
            title: 'NewNotaDebet',
            exclude: ['nota_debet_id'],
          }),
        },
      },
    })
    notaDebet: Omit<NotaDebet, 'nota_debet_id'>,
  ): Promise<NotaDebet> {
    return this.notaDebetRepository.create(notaDebet);
  }

  @get('/nota-debets/count', {
    responses: {
      '200': {
        description: 'NotaDebet model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(NotaDebet) where?: Where<NotaDebet>,
  ): Promise<Count> {
    return this.notaDebetRepository.count(where);
  }

  @get('/nota-debets', {
    responses: {
      '200': {
        description: 'Array of NotaDebet model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(NotaDebet, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(NotaDebet) filter?: Filter<NotaDebet>,
  ): Promise<NotaDebet[]> {
    return this.notaDebetRepository.find(filter);
  }

  @patch('/nota-debets', {
    responses: {
      '200': {
        description: 'NotaDebet PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(NotaDebet, {partial: true}),
        },
      },
    })
    notaDebet: NotaDebet,
    @param.where(NotaDebet) where?: Where<NotaDebet>,
  ): Promise<Count> {
    return this.notaDebetRepository.updateAll(notaDebet, where);
  }

  @get('/nota-debets/{id}', {
    responses: {
      '200': {
        description: 'NotaDebet model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(NotaDebet, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(NotaDebet, {exclude: 'where'}) filter?: FilterExcludingWhere<NotaDebet>
  ): Promise<NotaDebet> {
    return this.notaDebetRepository.findById(id, filter);
  }

  @patch('/nota-debets/{id}', {
    responses: {
      '204': {
        description: 'NotaDebet PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(NotaDebet, {partial: true}),
        },
      },
    })
    notaDebet: NotaDebet,
  ): Promise<void> {
    await this.notaDebetRepository.updateById(id, notaDebet);
  }

  @put('/nota-debets/{id}', {
    responses: {
      '204': {
        description: 'NotaDebet PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() notaDebet: NotaDebet,
  ): Promise<void> {
    await this.notaDebetRepository.replaceById(id, notaDebet);
  }

  @del('/nota-debets/{id}', {
    responses: {
      '204': {
        description: 'NotaDebet DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.notaDebetRepository.deleteById(id);
  }
}
