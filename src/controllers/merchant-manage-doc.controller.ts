import {authenticate} from '@loopback/authentication';
import {inject} from '@loopback/core';
import {Filter, repository} from '@loopback/repository';
import {
  get,
  getModelSchemaRef,
  param,
  post
} from '@loopback/rest';
import {SecurityBindings, securityId, UserProfile} from '@loopback/security';
import fs from 'fs';
import path from 'path';
import {MerchantProductDocument} from '../models';
import {MerchantProductDistribusiRepository, MerchantProductDocumentRepository, MerchantProductMasterRepository, MerchantProductRepository, MerchantRepository, UserRepository} from '../repositories';

@authenticate('jwt')
export class MerchantManageDocController {
  constructor(
    @repository(MerchantProductMasterRepository)
    public merchantProductMasterRepository: MerchantProductMasterRepository,
    @repository(MerchantProductDistribusiRepository)
    public merchantProductDistribusiRepository: MerchantProductDistribusiRepository,
    @repository(MerchantProductDocumentRepository)
    public merchantProductDocumentRepository: MerchantProductDocumentRepository,
    @repository(MerchantRepository)
    public merchantRepository: MerchantRepository,
    @repository(MerchantProductRepository)
    public merchantProductRepository: MerchantProductRepository,

    @inject(SecurityBindings.USER)
    public currentUserProfile: UserProfile,
    @repository(UserRepository)
    public userRepository: UserRepository,
  ) { }

  @get('/merchant-manage-doc/views/{MPM_ID}/{type}', {
    responses: {
      '200': {
        description: 'Merchant Document Views type model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(MerchantProductDocument, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findMerchantDocument(
    @param.path.string('MPM_ID') MPM_ID: string,
    @param.path.string('type') type: string,
    @param.filter(MerchantProductDocument) filter?: Filter<MerchantProductDocument>,
  ): Promise<Object> {
    var result: any; result = {success: true, message: '', datas: []};
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};

    var TYPE = 0;
    var MESSAGE = "";

    switch (type) {
      case "bar":
        TYPE = 1;
        break;
      case "bar-sign":
        TYPE = 2;
        break;
      case "invoice":
        TYPE = 3;
        break;
      case "invoice-sign":
        TYPE = 4;
        break;
      case "faktur-pajak":
        TYPE = 5;
        break;
      case "bukti-potong":
        TYPE = 6;
        break;
      case "others":
        TYPE = 7;
        break;
      default:
        MESSAGE = "Error, type path not found !";
        break;
    }


    if (TYPE != 0) {

      var FILTER_DATA: any; FILTER_DATA = filter;
      var FILTER_CLEAR: any; FILTER_CLEAR = {};
      for (var i in filter) {
        FILTER_CLEAR[i] = FILTER_DATA[i];
      }

      if (FILTER_DATA?.where != undefined) {
        var WHERE = {and: [{MPDOC_TYPE: TYPE}, {MPM_ID: MPM_ID}, FILTER_DATA?.where]};
        FILTER_CLEAR["where"] = WHERE;
      } else {
        FILTER_CLEAR["where"] = {and: [{MPDOC_TYPE: TYPE}, {MPM_ID: MPM_ID}]};
      }

      var MANAGE_DOC = await this.merchantProductDocumentRepository.find(FILTER_CLEAR);
      var DATAS: any; DATAS = [];
      for (var i in MANAGE_DOC) {
        var RAW_DOC = MANAGE_DOC[i];
        var RAW = {
          MPDOC_ID: RAW_DOC.MPDOC_ID,
          MPDOC_NAME: RAW_DOC.MPDOC_NAME,
          MPDOC_TYPE: RAW_DOC.MPDOC_TYPE,
          AT_FLAG: RAW_DOC.AT_FLAG,
          AT_CREATE: RAW_DOC.AT_CREATE,
          AT_UPDATE: RAW_DOC.AT_UPDATE
        }
        DATAS.push(RAW);
      }
      result.success = true;
      result.datas = DATAS;
      result.message = "Success !";
    } else {
      result.success = false;
      result.message = "Error, path " + type + " not found !";
    }

    return result;
  }

  @post('/merchant-manage-doc/active/{id}', {
    responses: {
      '200': {
        description: 'Delete Document Manage model instance',
        content: {'application/json': {schema: getModelSchemaRef(MerchantProductDocument)}},
      },
    },
  })
  async activeMerchantDocument(
    @param.path.string('id') id: string,
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};
    var Document = await this.merchantProductDocumentRepository.findById(id);

    if (Document.MPDOC_ID != undefined) {
      try {
        await this.merchantProductDocumentRepository.updateAll({AT_FLAG: 0}, {and: [{MPM_ID: Document.MPM_ID}, {MPDOC_TYPE: Document.MPDOC_TYPE}]});
        await this.merchantProductDocumentRepository.updateById(Document.MPDOC_ID, {
          AT_FLAG: 1
        });

        result.success = true;
        result.message = "Success Actived !";
      } catch (error) {
        result.success = false;
        result.message = error;
      }

    } else {
      result.success = false;
      result.message = "Error, document not found !";
    }

    return result;
  }

  @post('/merchant-manage-doc/delete/{id}', {
    responses: {
      '200': {
        description: 'Delete Document Manage model instance',
        content: {'application/json': {schema: getModelSchemaRef(MerchantProductDocument)}},
      },
    },
  })
  async deleteMerchantDocument(
    @param.path.string('id') id: string,
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};

    var Document = await this.merchantProductDocumentRepository.findById(id);

    if (Document.MPDOC_ID != undefined) {

      var PATH = "";
      switch (Document.MPDOC_TYPE) {
        case 1:
          PATH = path.join(__dirname, '../../.non-digipos/document/' + Document.MPM_ID + '/bar/') + Document.MPDOC_NAME;
          break;
        case 2:
          PATH = path.join(__dirname, '../../.non-digipos/document/' + Document.MPM_ID + '/bar-sign/') + Document.MPDOC_NAME;
          break;
        case 3:
          PATH = path.join(__dirname, '../../.non-digipos/document/' + Document.MPM_ID + '/invoice/') + Document.MPDOC_NAME;
          break;
        case 4:
          PATH = path.join(__dirname, '../../.non-digipos/document/' + Document.MPM_ID + '/invoice-sign/') + Document.MPDOC_NAME;
          break;
        case 5:
          PATH = path.join(__dirname, '../../.non-digipos/document/' + Document.MPM_ID + '/faktur-pajak/') + Document.MPDOC_NAME;
          break;
        case 6:
          PATH = path.join(__dirname, '../../.non-digipos/document/' + Document.MPM_ID + '/bukti-potong/') + Document.MPDOC_NAME;
          break;
        case 7:
          PATH = path.join(__dirname, '../../.non-digipos/document/' + Document.MPM_ID + '/others/') + Document.MPDOC_NAME;
          break;
        default:
          PATH = "";
          break;
      }

      if (PATH != "") {

        if (Document.AT_FLAG != 1) {

          if (fs.existsSync(PATH)) {
            fs.unlink(PATH, async (errorDelete: any) => {
              if (errorDelete) {
                result.success = false;
                result.message = errorDelete;
              } else {
                try {
                  await this.merchantProductDocumentRepository.deleteById(id);
                  result.success = true;
                  result.message = "Success delete !";

                } catch (error) {
                  result.success = false;
                  result.message = error;
                }
              }
            });
          } else {
            try {
              await this.merchantProductDocumentRepository.deleteById(id);
              result.success = true;
              result.message = "Success delete !";
            } catch (error) {
              result.success = false;
              result.message = error;
            }
          }

        } else {
          result.success = false;
          result.message = "Error, document is actived !";
        }

      } else {
        result.success = false;
        result.message = "Error, document not found !";
      }

    } else {
      result.success = false;
      result.message = "Error, document not found !";
    }


    return result;
  }

}
