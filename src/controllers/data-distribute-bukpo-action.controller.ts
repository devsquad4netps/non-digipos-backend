// Uncomment these imports to begin using these cool features!

import {Filter, repository} from '@loopback/repository';
import {get, getModelSchemaRef, param, post, requestBody} from '@loopback/rest';
import fs from 'fs';
import path from 'path';
import {DataDistributeBukpot} from '../models';
import {AdMasterRepository, DataDigiposRepository, DataDistributeBukpotRepository} from '../repositories';
import {ControllerConfig} from './controller.config';
const GDCONFIG = path.join(__dirname, '../../.config/directory.json');
// import {inject} from '@loopback/core';

export const PeriodeDistributeBukpot = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['periode'],
        properties: {
          periode: {
            type: 'string',
          }
        }
      }
    },
  },
}

export const SetFileTargetBukpotBukpot = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['bukpot_id', 'file_id'],
        properties: {
          bukpot_id: {
            type: 'number',
          },
          file_id: {
            type: 'string',
          }
        }
      }
    },
  },
}

export const ShareBukpotBukpot = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['periode'],
        properties: {
          periode: {
            type: 'string',
          }
        }
      }
    },
  },
}


var ShareBukpotProcess: any; ShareBukpotProcess = {};
export class DataDistributeBukpoActionController {

  constructor(
    @repository(DataDistributeBukpotRepository)
    public dataDistributeBukpotRepository: DataDistributeBukpotRepository,
    @repository(AdMasterRepository)
    public adMasterRepository: AdMasterRepository,
    @repository(DataDigiposRepository)
    public dataDigiposRepository: DataDigiposRepository,
  ) { }


  onShareBukpot = async (periode: string) => {
    var periodeSet = periode;
    if (ShareBukpotProcess[periode] != undefined) {
      for (var i in ShareBukpotProcess[periode]) {
        var bukpot_id = i;
        var rawBukpot = ShareBukpotProcess[periode][i];
        var FileId = rawBukpot.bukpot_attachment_set;
        var code = rawBukpot.ad_code == undefined || rawBukpot.ad_code == "" || rawBukpot.ad_code == null ? 0 : rawBukpot.ad_code;
        var masterAD = await this.adMasterRepository.findById(code);
        if (masterAD == null) {

        } else {
          var GDBukPot = masterAD.gd_bukpot;
          console.log(GDBukPot);
          var resultShare: any; resultShare = {};
          resultShare = await new Promise((resolve, reject) => {
            fs.readFile(GDCONFIG, async (errConfig, contentConfig: any) => {
              if (errConfig) {
                console.log('Error loading root folder:', errConfig);
              }
              var configGD = JSON.parse(contentConfig);
              const ProcessDistribute = configGD.BUKPOT_PROCESS;
              const SuccessDistribute = configGD.BUKPOT_SUCCESS;
              const FailedDistribute = configGD.BUKPOT_FAILED;
              ControllerConfig.onAPIGoogleDrive(async (rConnect: any) => {
                if (rConnect.success == true) {
                  const drive = rConnect.drive;
                  const auth = rConnect.auth;
                  var nameFileDownload = rawBukpot.periode + "_" + masterAD.npwp + "_" + masterAD.ad_name + "_" + masterAD.ad_code + ".pdf";
                  var pathDest = path.join(__dirname, '../../.digipost/bukpot/' + nameFileDownload);
                  var dest = fs.createWriteStream(pathDest);
                  console.log(FileId);
                  try {
                    drive.files.get({fileId: FileId, alt: 'media'}, {responseType: 'stream'},
                      (err: any, res: any) => {
                        res.data
                          .on('end', async () => {
                            ControllerConfig.onGD_Search(auth, "'" + GDBukPot + "' in parents and	name = '" + nameFileDownload + "'", async (rSearchFp: any) => {
                              var fileFp = rSearchFp.data.data.files;
                              console.log(fileFp);
                              if (fileFp.length == 0) {
                                ControllerConfig.onUploadPDFGD(auth, GDBukPot, pathDest, nameFileDownload, async (rUp: any) => {
                                  if (rUp.success == true) {
                                    ControllerConfig.onGD_MoveFile(auth, FileId, ProcessDistribute, SuccessDistribute, (r: any) => {
                                      try {
                                        // ControllerConfig.onAPISendMail([{email: masterAD.email}],
                                        //   "Faktur Pajak From Digipos",
                                        //   [{
                                        //     "type": "text/plain",
                                        //     "value": "\n You have new Faktur Pajak File : " + NameFile + ", please chek your google drive digipos, " + '<a href="https://drive.google.com/drive/my-drive" >' + " Open Google Drvice</a> ! \n "
                                        //   }],
                                      } catch (error) {

                                      }
                                    });
                                    resolve({success: true, message: "Share bukpot " + nameFileDownload + " success !", file_name: nameFileDownload});
                                  } else {
                                    resolve({success: false, message: "Error share bukpot " + nameFileDownload});
                                    ControllerConfig.onGD_MoveFile(auth, FileId, ProcessDistribute, FailedDistribute, (r: any) => {
                                      try {
                                        // ControllerConfig.onAPISendMail([{email: masterAD.email}],
                                        //   "Faktur Pajak From Digipos",
                                        //   [{
                                        //     "type": "text/plain",
                                        //     "value": "\n You have new Faktur Pajak File : " + NameFile + ", please chek your google drive digipos, " + '<a href="https://drive.google.com/drive/my-drive" >' + " Open Google Drvice</a> ! \n "
                                        //   }],
                                      } catch (error) {

                                      }
                                    });
                                  }
                                });
                              } else {
                                var field: string; field = "";
                                for (var j in fileFp) {
                                  var rFileInv = fileFp[j];
                                  field = rFileInv.id
                                }
                                ControllerConfig.onUploadPDFGDReplace(auth, field, GDBukPot, pathDest, nameFileDownload, async (rUp: any) => {
                                  if (rUp.success == true) {
                                    ControllerConfig.onGD_MoveFile(auth, FileId, ProcessDistribute, SuccessDistribute, (r: any) => {
                                      try {
                                        // ControllerConfig.onAPISendMail([{email: masterAD.email}],
                                        //   "Faktur Pajak From Digipos",
                                        //   [{
                                        //     "type": "text/plain",
                                        //     "value": "\n You have new Faktur Pajak File : " + NameFile + ", please chek your google drive digipos, " + '<a href="https://drive.google.com/drive/my-drive" >' + " Open Google Drvice</a> ! \n "
                                        //   }],
                                      } catch (error) {

                                      }
                                    });
                                    resolve({success: true, message: "Share bukpot " + nameFileDownload + " success !", file_name: nameFileDownload});
                                  } else {
                                    resolve({success: false, message: "Error share bukpot " + nameFileDownload});
                                    ControllerConfig.onGD_MoveFile(auth, FileId, ProcessDistribute, FailedDistribute, (r: any) => {
                                      try {
                                        // ControllerConfig.onAPISendMail([{email: masterAD.email}],
                                        //   "Faktur Pajak From Digipos",
                                        //   [{
                                        //     "type": "text/plain",
                                        //     "value": "\n You have new Faktur Pajak File : " + NameFile + ", please chek your google drive digipos, " + '<a href="https://drive.google.com/drive/my-drive" >' + " Open Google Drvice</a> ! \n "
                                        //   }],
                                      } catch (error) {

                                      }
                                    });
                                  }
                                });
                              }
                            });
                            // console.log('Done : ', nameFileDownload);
                          })
                          .on('error', async (err: any) => {
                            console.log('Error', err);
                            resolve({success: false, message: err});
                          })
                          .pipe(dest);
                      }
                    );
                  } catch (error) {
                    console.log(error);
                    resolve({success: false, message: error});
                  }

                } else {
                  resolve({success: false, message: "not connect google api"});
                }
              });
            });
          });
          console.log(resultShare);
          if (resultShare.success != undefined) {
            if (resultShare.success == true) {
              await this.dataDistributeBukpotRepository.updateById(parseInt(bukpot_id), {
                bukpot_attachment: resultShare.file_name,
                at_distribute: ControllerConfig.onCurrentTime().toString(),
                bukpot_status: 2
              });
            }
          }
        }
        delete ShareBukpotProcess[periodeSet][bukpot_id];
        this.onShareBukpot(periodeSet);
        break;
      }
    }
  }

  @post('/data-distribute-bukpot/share-distribute-bukpot', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: SetFileTargetBukpotBukpot
          }
        },
      },
    },
  })
  async share_distribute_bukpot(
    @requestBody(ShareBukpotBukpot) ShareBukpotBukpot: {periode: string},
  ): Promise<Object> {
    var result: any; result = {success: false, message: ""};
    var infoBukpot = await this.dataDistributeBukpotRepository.find({where: {and: [{periode: ShareBukpotBukpot.periode}, {bukpot_status: 1}]}});
    if (infoBukpot.length > 0) {
      if (ShareBukpotProcess[ShareBukpotBukpot.periode] == undefined) {
        ShareBukpotProcess[ShareBukpotBukpot.periode] = {};
      }
      for (var i in infoBukpot) {
        var rawBukpot: any; rawBukpot = infoBukpot[i];

        try {
          await this.dataDistributeBukpotRepository.updateById(rawBukpot.bukpot_id, {
            at_process: ControllerConfig.onCurrentTime().toString()
          });
        } catch (error) { }

        var id = rawBukpot.bukpot_id == undefined ? 0 : rawBukpot.bukpot_id;
        if (ShareBukpotProcess[ShareBukpotBukpot.periode][id] == undefined) {
          ShareBukpotProcess[ShareBukpotBukpot.periode][id] = {};
        }
        if (ShareBukpotProcess[ShareBukpotBukpot.periode][id]["process"] == undefined) {
          ShareBukpotProcess[ShareBukpotBukpot.periode][id]["process"] = 0;
          for (var j in rawBukpot) {
            ShareBukpotProcess[ShareBukpotBukpot.periode][id][j] = rawBukpot[j];
          }
        } else {
          if (ShareBukpotProcess[ShareBukpotBukpot.periode][id]["process"] == 0) {
            for (var j in rawBukpot) {
              ShareBukpotProcess[ShareBukpotBukpot.periode][id][j] = rawBukpot[j];
            }
            ShareBukpotProcess[ShareBukpotBukpot.periode][id]["process"] = 0;
          }
        }
      }
      var no = 0;
      for (var i in ShareBukpotProcess[ShareBukpotBukpot.periode]) {no++;}
      // if (no == 0) {
      console.log("RUN SHARE");
      this.onShareBukpot(ShareBukpotBukpot.periode);
      // } else {
      //   console.log("ALREADY RUN SHARE");
      // }
      result.success = true;
      result.message = "set proccess share bukpot ";
    } else {
      result.success = false;
      result.message = "No data bukpot distribute in periode " + ShareBukpotBukpot.periode;
    }
    return result;
  }



  @post('/data-distribute-bukpot/progress-distribute-bukpot', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: ShareBukpotBukpot
          }
        },
      },
    },
  })
  async progress_distribute_bukpot(
    @requestBody(ShareBukpotBukpot) ShareBukpotBukpot: {periode: string, },
  ): Promise<Object> {
    var result: any; result = {success: false, progress: 0, message: ""};
    if (ShareBukpotProcess[ShareBukpotBukpot.periode] != undefined) {
      var no = 0;
      for (var i in ShareBukpotProcess[ShareBukpotBukpot.periode]) {
        no++;
      }
      result.success = true;
      result.progress = no;
      result.message = "success";
    } else {
      result.success = true;
      result.progress = 0;
      result.message = "success";
    }
    return result;
  }


  @post('/data-distribute-bukpot/set-distribute-bukpot', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: SetFileTargetBukpotBukpot
          }
        },
      },
    },
  })
  async set_distribute_bukpot(
    @requestBody(SetFileTargetBukpotBukpot) SetFileTargetBukpotBukpot: {bukpot_id: number, file_id: string},
  ): Promise<Object> {
    var result: any; result = {success: false, message: ""};
    try {
      await this.dataDistributeBukpotRepository.updateById(SetFileTargetBukpotBukpot.bukpot_id, {
        bukpot_attachment_set: SetFileTargetBukpotBukpot.file_id,
        bukpot_status: 1
      });
      result.success = true;
      result.message = "success set bukpot !";
    } catch (error) {
      result.success = false;
      result.message = error;
    }
    return result;
  }


  @post('/data-distribute-bukpot/add-distribute-bukpot', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: PeriodeDistributeBukpot
          }
        },
      },
    },
  })
  async add_distribute_bukpot(
    @requestBody(PeriodeDistributeBukpot) PeriodeDistributeBukpot: {periode: string},
  ): Promise<Object> {
    var result: any; result = {};
    var infoDistributeBukpot = await this.dataDistributeBukpotRepository.find({where: {periode: PeriodeDistributeBukpot.periode}});
    if (infoDistributeBukpot.length > 0) {
      result.success = false;
      result.message = "Data Distribute Bukpot Periode " + PeriodeDistributeBukpot.periode + " already exities !";
    } else {
      var infoAD = await this.adMasterRepository.find();
      for (var i in infoAD) {
        var rawAD = infoAD[i];
        var dataAva = await this.dataDigiposRepository.findOne({where: {and: [{trx_date_group: PeriodeDistributeBukpot.periode}, {ad_code: rawAD.ad_code}]}});
        if (dataAva == null) {
          continue;
        }
        this.dataDistributeBukpotRepository.create({
          ad_code: rawAD.ad_code,
          ad_name: rawAD.ad_name,
          npwp: rawAD.npwp,
          periode: PeriodeDistributeBukpot.periode,
          bukpot_status: 0,
          bukpot_attachment: "",
          bukpot_attachment_set: "",
          at_process: "",
          by_process: "",
          at_distribute: ""
        });
      }
      result.success = true;
      result.message = "success";
    }
    return result;
  }



  @get('/data-distribute-bukpot', {
    responses: {
      '200': {
        description: 'Array of DataDigipos model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(DataDistributeBukpot, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(DataDistributeBukpot) filter?: Filter<DataDistributeBukpot>,
  ): Promise<[]> {


    var InfoFileBukpot: any; InfoFileBukpot = {};
    InfoFileBukpot = await new Promise((resolve, reject) => {
      var data: any; data = {};
      fs.readFile(GDCONFIG, async (errConfig, contentConfig: any) => {
        if (errConfig) {
          console.log('Error loading root folder:', errConfig);
          resolve({});
        }
        var configGD = JSON.parse(contentConfig);
        const ProcessDistribute = configGD.BUKPOT_PROCESS;
        const SuccessDistribute = configGD.BUKPOT_SUCCESS;
        const FailedDistribute = configGD.BUKPOT_FAILED;
        ControllerConfig.onAPIGoogleDrive(async (rConnect: any) => {
          if (rConnect.success == true) {
            const drive = rConnect.drive;
            const auth = rConnect.auth;
            ControllerConfig.onGetFileOnFolder(auth, ProcessDistribute, (rFiles: any) => {
              if (rFiles.length) {
                rFiles.map(async (file: any) => {
                  var ID = file.id;
                  var NameFile = file.name.split("_");
                  if (NameFile.length == 2) {
                    var Periode = NameFile[0];
                    var Npwp = NameFile[1].substring(0, NameFile[1].length - 4);
                    if (data[Npwp] == undefined) {
                      data[Npwp] = [];
                    }
                    data[Npwp].push({id: ID, file_name: file.name});
                    console.log(Npwp);
                  }
                });
                resolve(data);
              } else {
                resolve({});
              }
            });
          } else {
            resolve({});
          }
        });
      });
    });
    // console.log(InfoFileBukpot);
    // var infoMasterAD = await this.adMasterRepository.find();
    // for (var i in infoMasterAD) {
    //   var rawMasterAD = infoMasterAD[i];
    //   var npwp =
    // }
    var result: any; result = [];
    var data = await this.dataDistributeBukpotRepository.find(filter);
    for (var i in data) {
      var rawNpwp: any; rawNpwp = {};
      var rawDistribute: any; rawDistribute = data[i];
      for (var j in rawDistribute) {
        rawNpwp[j] = rawDistribute[j];
      }
      rawNpwp["file_bukpot"] = [];
      var infoAD = await this.adMasterRepository.findById(rawDistribute.ad_code);
      if (infoAD.ad_code != undefined) {
        var npwp = infoAD.npwp;
        if (InfoFileBukpot[npwp] != undefined) {
          rawNpwp["file_bukpot"] = InfoFileBukpot[npwp];
        }
        await this.dataDistributeBukpotRepository.updateById(rawDistribute.bukpot_id, {
          ad_code: infoAD.ad_code,
          ad_name: infoAD.ad_name,
          npwp: infoAD.npwp
        });
        result.push(rawNpwp);
      }
    }
    return result;
  }


}
