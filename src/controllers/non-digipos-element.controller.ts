// // Uncomment these imports to begin using these cool features!

// import {Filter, FilterExcludingWhere, repository} from '@loopback/repository';
// import {del, get, getModelSchemaRef, param, post, put, requestBody} from '@loopback/rest';
// import {MerchantElement} from '../models';
// import {MerchantElementRepository} from '../repositories';
// import {ControllerConfig} from './controller.config';

// // import {inject} from '@loopback/core';


// export class NonDigiposElementController {
//   constructor(
//     @repository(MerchantElementRepository)
//     public merchantElementRepository: MerchantElementRepository,
//   ) { }


//   @get('/non-digipos-element', {
//     responses: {
//       '200': {
//         description: 'Array of non digipos element type model instances',
//         content: {
//           'application/json': {
//             schema: {
//               type: 'array',
//               items: getModelSchemaRef(MerchantElement, {includeRelations: true}),
//             },
//           },
//         },
//       },
//     },
//   })
//   async find(
//     @param.filter(MerchantElement) filter?: Filter<MerchantElement>,
//   ): Promise<MerchantElement[]> {
//     return this.merchantElementRepository.find(filter);
//   }

//   @get('/non-digipos-element/{id}', {
//     responses: {
//       '200': {
//         description: 'non digipos elemet type model instance',
//         content: {
//           'application/json': {
//             schema: getModelSchemaRef(MerchantElement, {includeRelations: true}),
//           },
//         },
//       },
//     },
//   })
//   async findById(
//     @param.path.number('id') id: number,
//     @param.filter(MerchantElement, {exclude: 'where'}) filter?: FilterExcludingWhere<MerchantElement>
//   ): Promise<MerchantElement> {
//     return this.merchantElementRepository.findById(id, filter);
//   }


//   @post('/non-digipos-element/create', {
//     responses: {
//       '200': {
//         description: 'Nondigipos Element model instance',
//         content: {'application/json': {schema: getModelSchemaRef(MerchantElement)}},
//       },
//     },
//   })
//   async create(
//     @requestBody({
//       content: {
//         'application/json': {
//           schema: getModelSchemaRef(MerchantElement, {
//             title: 'NewNonDigiposElementItem',
//             exclude: ['me_id', 'me_at_create', 'me_at_update'],
//           }),
//         },
//       },
//     })
//     elementItem: Omit<MerchantElement, 'me_id'>,
//   ): Promise<Object> {
//     var result: any; result = {success: false, message: "", data: {}};
//     try {
//       var infoCreate = await this.merchantElementRepository.create(elementItem);
//       var infoUpdate = await this.merchantElementRepository.updateById(infoCreate.me_id, {
//         me_at_create: ControllerConfig.onCurrentTime().toString()
//       });
//       result.success = true;
//       result.message = "Success, create merchant element.";
//       result.data = infoUpdate;
//     } catch (error) {
//       result.success = false;
//       result.message = error;
//     }
//     return result;
//   }

//   @put('/non-digipos-element/update/{id}', {
//     responses: {
//       '200': {
//         description: 'Merchan Element model instance',
//         content: {'application/json': {schema: getModelSchemaRef(MerchantElement)}},
//       },
//     },
//   })
//   async updateById(
//     @param.path.number('id') id: number,
//     @requestBody({
//       content: {
//         'application/json': {
//           schema: getModelSchemaRef(MerchantElement, {
//             title: 'NewMerchanElementItem',
//             exclude: ['me_id', 'me_at_create', 'me_at_update'],
//           }),
//         },
//       },
//     }) product: any,
//   ): Promise<Object> {
//     var result: any; result = {success: false, message: "", data: {}};
//     try {
//       var dataUpdate: any; dataUpdate = {};
//       for (var i in product) {
//         dataUpdate[i] = product[i];
//       }
//       dataUpdate["me_at_update"] = ControllerConfig.onCurrentTime().toString();
//       var updateProduct = await this.merchantElementRepository.updateById(id, dataUpdate);
//       result.success = true;
//       result.message = "Success, update merchant element.";
//       result.data = updateProduct;
//     } catch (error) {
//       result.success = false;
//       result.message = error;
//     }
//     return result;
//   }

//   @del('/non-digipos-element/delete/{id}', {
//     responses: {
//       '200': {
//         description: 'Merchant Element DELETE success',
//       },
//     },
//   })
//   async deleteById(@param.path.number('id') id: number): Promise<Object> {
//     var result: any; result = {success: false, message: ""};
//     await this.merchantElementRepository.deleteById(id);
//     result.success = true;
//     result.message = "Success, delete  merchant element.";
//     return result;
//   }

// }
