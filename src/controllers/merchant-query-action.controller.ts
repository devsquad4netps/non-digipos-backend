import {BigQuery} from '@google-cloud/bigquery';
import {authenticate} from '@loopback/authentication';
import {inject} from '@loopback/core';
import {Filter, repository} from '@loopback/repository';
import {get, getModelSchemaRef, param, post, requestBody} from '@loopback/rest';
import {SecurityBindings, securityId, UserProfile} from '@loopback/security';
import {MerchantProductQuery} from '../models';
import {MerchantProductDatastoreRepository, MerchantProductGroupRepository, MerchantProductVariableRepository, MpvTypeKeyRepository, MpvTypeRepository} from '../repositories';
import {MerchantProductQueryRepository} from '../repositories/merchant-product-query.repository';
import {MerchantProductRepository} from '../repositories/merchant-product.repository';
import {MerchantRepository} from '../repositories/merchant.repository';
import {UserRepository} from '../repositories/user.repository';
import {ControllerConfig} from './controller.config';


@authenticate('jwt')
export class MerchantQueryActionController {
  constructor(
    @repository(MerchantRepository)
    public merchantRepository: MerchantRepository,
    @repository(MerchantProductRepository)
    public merchantProductRepository: MerchantProductRepository,
    @repository(MerchantProductGroupRepository)
    public merchantProductGroupRepository: MerchantProductGroupRepository,
    @repository(MerchantProductVariableRepository)
    public merchantProductVariableRepository: MerchantProductVariableRepository,
    @repository(MerchantProductDatastoreRepository)
    public merchantProductDatastoreRepository: MerchantProductDatastoreRepository,
    @repository(MpvTypeRepository)
    public mpvTypeRepository: MpvTypeRepository,
    @repository(MpvTypeKeyRepository)
    public mpvTypeKeyRepository: MpvTypeKeyRepository,
    @repository(MerchantProductQueryRepository)
    public merchantProductQueryRepository: MerchantProductQueryRepository,
    @inject(SecurityBindings.USER)
    public currentUserProfile: UserProfile,
    @repository(UserRepository)
    public userRepository: UserRepository,
  ) { }

  @get('/merchant-product-query-action/{MP_ID}/views', {
    responses: {
      '200': {
        description: 'Array of Merchant Product variable model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(MerchantProductQuery, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async findMerchantProductQuery(
    @param.path.number('MP_ID') id: number,
    @param.filter(MerchantProductQuery) filter?: Filter<MerchantProductQuery>,
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};

    var MERCHANT_PRODUCT = await this.merchantProductRepository.findOne({where: {MP_ID: id}});

    if (MERCHANT_PRODUCT != null) {

      var FOUND = await this.merchantProductQueryRepository.findOne({where: {and: [{MP_ID: id}]}});
      if (FOUND != null) {
        result.datas = {
          MPQ_ID: FOUND.MPQ_ID,
          MPQ_QUERY: FOUND.MPQ_QUERY,
          MPQ_GROUP_SUMMARY: FOUND.MPQ_GROUP_SUMMARY,
          M_ID: FOUND.M_ID,
          MP_ID: FOUND.MP_ID,
          AT_FLAG: FOUND.AT_FLAG
        }
        result.success = true;
        result.message = "Success !";
      } else {

        try {
          var CREATE_QUERY = await this.merchantProductQueryRepository.create({
            MPQ_QUERY: "",
            MPQ_GROUP_SUMMARY: "",
            M_ID: MERCHANT_PRODUCT.M_ID,
            MP_ID: MERCHANT_PRODUCT.MP_ID,
            AT_CREATE: ControllerConfig.onCurrentTime().toString(),
            AT_UPDATE: ControllerConfig.onCurrentTime().toString(),
            AT_FLAG: 0,
            AT_WHO: USER_ID
          });

          result.datas = {
            MPQ_ID: CREATE_QUERY.MPQ_ID,
            MPQ_QUERY: CREATE_QUERY.MPQ_QUERY,
            MPQ_GROUP_SUMMARY: CREATE_QUERY.MPQ_GROUP_SUMMARY,
            M_ID: CREATE_QUERY.M_ID,
            MP_ID: CREATE_QUERY.MP_ID,
            AT_FLAG: CREATE_QUERY.AT_FLAG
          }
          result.success = true;
          result.message = "Success !";
        } catch (error) {

          result.success = false;
          result.message = error;
        }

      }

    } else {
      result.success = false;
      result.message = "Error, merchant product " + id + " not found !";
    }

    return result;
  }


  @post('/merchant-product-query-action/update/{id}', {
    responses: {
      '200': {
        description: 'Product Query model instance',
        content: {'application/json': {schema: getModelSchemaRef(MerchantProductQuery)}},
      },
    },
  })
  async updateMerchantProductQuery(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(MerchantProductQuery, {
            title: 'PRODUCT_QUERY_UPDATE',
            exclude: ['MPQ_ID', 'AT_CREATE', 'AT_UPDATE', 'AT_WHO', 'AT_FLAG'],
          }),
        },
      },
    })
    dataMerchantProductQuery: any,
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};

    var REQUIRED_FIELDS = ['MPQ_QUERY', 'MPQ_GROUP_SUMMARY', 'M_ID', 'MP_ID'];
    var BODY_PARAM: any; BODY_PARAM = {};
    for (var i in dataMerchantProductQuery) {
      if (REQUIRED_FIELDS.indexOf(i) != -1) {
        REQUIRED_FIELDS.splice(REQUIRED_FIELDS.indexOf(i), 1);
      }
      BODY_PARAM[i] = dataMerchantProductQuery[i];
    }

    if (REQUIRED_FIELDS.length == 0) {
      var MERCHANT = await this.merchantRepository.count({M_ID: BODY_PARAM["M_ID"]});
      var MERCHANT_PRODUCT = await this.merchantProductRepository.count({and: [{MP_ID: BODY_PARAM["MP_ID"]}, {M_ID: BODY_PARAM["M_ID"]}]});
      var MERCHANT_QUERY = await this.merchantProductQueryRepository.count({and: [{MP_ID: BODY_PARAM["MP_ID"]}, {M_ID: BODY_PARAM["M_ID"]}, {MPQ_ID: id}]});

      if (MERCHANT.count > 0) {
        if (MERCHANT_PRODUCT.count > 0) {
          if (MERCHANT_QUERY.count > 0) {

            var QUERY_DENIED = false;
            var QUERY_DENIED_LIST = ['DROP', 'DELETE', 'UPDATE', 'TRUNCATE', 'INSERT'];
            var QUERY_REQ = dataMerchantProductQuery.MPQ_QUERY.toUpperCase();
            for (var i_SQL in QUERY_DENIED_LIST) {
              if (QUERY_REQ.search(QUERY_DENIED_LIST[i_SQL]) != -1) {
                QUERY_DENIED = true;
              }
            }

            if (QUERY_DENIED == false) {
              try {

                if (dataMerchantProductQuery.MPQ_QUERY != undefined && dataMerchantProductQuery.MPQ_QUERY != "" && dataMerchantProductQuery.MPQ_QUERY != null) {
                  var QUERY = dataMerchantProductQuery.MPQ_QUERY.split("$FILTER_PERIODE").join(" ").split("$AND").join(" ").split("$OR").join(" ").split("$WHERE").join(" ");
                  const BIG_QUERY = new BigQuery();
                  var SQL_QUERY = QUERY + " " + " LIMIT 1";
                  const OPTION_BIG_QUERY = {
                    query: SQL_QUERY
                  }
                  const [RESULT_BIG_QUERY] = await BIG_QUERY.query(OPTION_BIG_QUERY);
                  var FIELD_AVALIABLE: any; FIELD_AVALIABLE = [];
                  for (var I_KEY in RESULT_BIG_QUERY) {
                    var RAW_QUERY = RESULT_BIG_QUERY[I_KEY];
                    for (var FIELD_KEY in RAW_QUERY) {
                      if (FIELD_AVALIABLE.indexOf(FIELD_KEY) == -1) {
                        FIELD_AVALIABLE.push(FIELD_KEY);
                      }
                    }
                  }

                  if (FIELD_AVALIABLE.length > 0) {
                    await this.merchantProductQueryRepository.updateById(id, {
                      MPQ_QUERY: dataMerchantProductQuery.MPQ_QUERY,
                      MPQ_GROUP_SUMMARY: dataMerchantProductQuery.MPQ_GROUP_SUMMARY,
                      AT_FLAG: 1
                    });
                  } else {
                    await this.merchantProductQueryRepository.updateById(id, {
                      MPQ_QUERY: dataMerchantProductQuery.MPQ_QUERY,
                      MPQ_GROUP_SUMMARY: dataMerchantProductQuery.MPQ_GROUP_SUMMARY,
                      AT_FLAG: 0
                    });
                  }
                } else {
                  await this.merchantProductQueryRepository.updateById(id, {
                    MPQ_QUERY: undefined,
                    MPQ_GROUP_SUMMARY: dataMerchantProductQuery.MPQ_GROUP_SUMMARY,
                    AT_FLAG: 1
                  });
                }

                result.success = true;
                result.message = "Success UPDATE query !";
                result.datas = FIELD_AVALIABLE;

              } catch (error) {
                result.success = false;
                result.message = error;
              }
            } else {
              result.success = false;
              result.message = "Error, Query SELECT Only !";
            }

          } else {
            result.success = false;
            result.message = "Error, merchant product query " + id + " not found !";
          }
        } else {
          result.success = false;
          result.message = "Error, merchant product " + BODY_PARAM["MP_ID"] + " not found !";
        }
      } else {
        result.success = false;
        result.message = "Error, merchant " + BODY_PARAM["M_ID"] + " not found !";
      }
    } else {
      result.success = false;
      result.message = "Error, Field " + JSON.stringify(REQUIRED_FIELDS) + " must entry !";
    }
    return result;
  }



}
