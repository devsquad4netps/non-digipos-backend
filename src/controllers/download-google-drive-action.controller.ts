import {
  repository
} from '@loopback/repository';
import {get, getModelSchemaRef, post, requestBody} from '@loopback/rest';
import csv from 'csv-parser';
import fs from 'fs';
import path from 'path';
import {DownloadGoogleDrive} from '../models';
import {AdClusterRepository, AdMasterRepository, DataconfigRepository, DataDigiposRepository, DataDistributeBukpotRepository, DisputeDetailRepository, DisputeRepository, DownloadGoogleDriveRepository, NotaDebetDetailRepository, NotaDebetRepository} from '../repositories';
import {ControllerConfig} from './controller.config';

export const GetGoogleDriveFile = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['gd_file_group'],
        properties: {
          gd_file_group: {
            type: 'number'
          }
        }
      }
    },
  },
};

const GDCONFIG = path.join(__dirname, '../../.config/directory.json');

export class DownloadGoogleDriveActionController {
  constructor(
    @repository(DownloadGoogleDriveRepository)
    public downloadGoogleDriveRepository: DownloadGoogleDriveRepository,
    @repository(AdMasterRepository)
    public adMasterRepository: AdMasterRepository,
    @repository(AdClusterRepository)
    public adClusterRepository: AdClusterRepository,
    @repository(NotaDebetRepository)
    public notaDebetRepository: NotaDebetRepository,
    @repository(NotaDebetDetailRepository)
    public notaDebetDetailRepository: NotaDebetDetailRepository,
    @repository(DataDigiposRepository)
    public dataDigiposRepository: DataDigiposRepository,
    @repository(DisputeRepository)
    public disputeRepository: DisputeRepository,
    @repository(DisputeDetailRepository)
    public disputeDetailRepository: DisputeDetailRepository,
    @repository(DataconfigRepository)
    public dataconfigRepository: DataconfigRepository,
    @repository(DataDistributeBukpotRepository)
    public dataDistributeBukpotRepository: DataDistributeBukpotRepository,
  ) { }


  onRunningDispute = async (infoMasterAD: any, path: any) => {
    var filePath = path;
    var KeyRow: any; KeyRow = {
      DEALER_CODE: '',
      NAME: '',
      YEARS: '',
      MONTH: '',
      SALES_FEE: '1',
      TRX_A: '',
      TOT_MDR_A: '',
      TRX_B: '',
      TOT_MDR_B: ''
    }

    const configData = await this.dataconfigRepository.find({where: {config_group_id: 1}});
    var configNum: any;
    configNum = {};
    for (var iC in configData) {
      var infoConfig = configData[iC];
      if (infoConfig.config_code == "0001-01") {
        configNum["persen_fee"] = infoConfig.config_value;
      }
      if (infoConfig.config_code == "0001-02") {
        configNum["persen_paid"] = infoConfig.config_value;
      }
      if (infoConfig.config_code == "0001-03") {
        configNum["num_dpp_fee"] = infoConfig.config_value;
      }
      if (infoConfig.config_code == "0001-04") {
        configNum["num_ppn_fee"] = infoConfig.config_value;
      }
      if (infoConfig.config_code == "0001-05") {
        configNum["num_pph_fee"] = infoConfig.config_value;
      }

      if (infoConfig.config_code == "0001-11") {
        configNum["num_dpp_paid"] = infoConfig.config_value;
      }

      if (infoConfig.config_code == "0001-12") {
        configNum["num_ppn_paid"] = infoConfig.config_value;
      }

      if (infoConfig.config_code == "0001-13") {
        configNum["num_pph_paid"] = infoConfig.config_value;
      }

    }


    var DisputeAdCode: string; DisputeAdCode = infoMasterAD.ad_code;
    var DisputeAdName: string; DisputeAdName = infoMasterAD.ad_name;
    var DataDiputeCollection: any; DataDiputeCollection = {};

    var error: boolean; error = false;
    var erroMsg: any; erroMsg = "";
    fs.createReadStream(filePath).pipe(csv()).on('data', async (row: any) => {

      var keyAva: any; keyAva = {};
      for (var i in row) {
        var key = i.toLocaleUpperCase().trim();
        keyAva[key] = row[i];
      }

      var FieldNotFound = false;
      var FieldError: any; FieldError = [];
      for (var j in KeyRow) {
        if (keyAva[j] == undefined) {
          error = true;
          FieldNotFound = true;
          FieldError.push(j);
          continue;
        }
      }


      if (FieldNotFound == true) {
        erroMsg = erroMsg + ",Error can't found field " + JSON.stringify(FieldError);
      }


      var text = /^[0-9]+$/;
      if ((keyAva["YEARS"] != "") && (!text.test(keyAva["YEARS"]))) {
        error = true;
        erroMsg = erroMsg + ", Error format  years " + keyAva["YEARS"] + " not numeric";
      }

      if ((keyAva["MONTH"] != "") && (!text.test(keyAva["MONTH"]))) {
        error = true;
        erroMsg = erroMsg + ", Error format  month " + keyAva["MONTH"] + " not numeric";
      }

      var Month: number; Month = parseInt(keyAva["MONTH"]) * 1 == parseInt(keyAva["MONTH"]) ? parseInt(keyAva["MONTH"]) : -1;
      var SetMount = ControllerConfig.onGetMonth(Month);
      if (Month == -1 || SetMount == undefined) {
        error = true;
        erroMsg = erroMsg + ", Error format month " + parseInt(keyAva["MONTH"]);
      }

      if ((keyAva["SALES_FEE"] != "") && (!text.test(keyAva["SALES_FEE"]))) {
        error = true;
        erroMsg = erroMsg + ", Error format sales_fee " + keyAva["SALES_FEE"] + " not numeric";
      }

      if ((keyAva["TRX_A"] != "") && (!text.test(keyAva["TRX_A"]))) {
        error = true;
        erroMsg = erroMsg + ", Error format trx_a " + keyAva["TRX_A"] + " not numeric";
      }

      if ((keyAva["TOT_MDR_A"] != "") && (!text.test(keyAva["TOT_MDR_A"]))) {
        error = true;
        erroMsg = erroMsg + ", Error format tot_mdr_a " + keyAva["TOT_MDR_A"] + " not numeric";
      }

      if ((keyAva["TRX_B"] != "") && (!text.test(keyAva["TRX_B"]))) {
        error = true;
        erroMsg = erroMsg + ", Error format trx_b " + keyAva["TRX_B"] + " not numeric";
      }

      if ((keyAva["TOT_MDR_B"] != "") && (!text.test(keyAva["TOT_MDR_B"]))) {
        error = true;
        erroMsg = erroMsg + ", Error format tot_mdr_b " + keyAva["TOT_MDR_B"] + " not numeric";
      }

      // var cluster = await this.adClusterRepository.findById(keyAva["DEALER_CODE"]);
      // if (cluster == null || cluster.ad_cluster_id == undefined) {
      //   error = true;
      //   erroMsg = erroMsg + ", Error can't find dealer_code " + keyAva["DEALER_CODE"] + ", please contact admin !";
      // }


      if (FieldNotFound == false && error == false) {



        const PeriodeDispute = keyAva["YEARS"] + "-" + keyAva["MONTH"];
        console.log(PeriodeDispute);
        if (DataDiputeCollection[PeriodeDispute] == undefined) {
          DataDiputeCollection[PeriodeDispute] = {
            DataDispute: [],
            DataDisputeSalesFeeMin: 0,
            DataDisputeSalesFeeMax: 0,
            DisputePeriode: PeriodeDispute,
            DisputeTrxA: 0,
            DisputeTotMdrA: 0,
            DisputeDppA: 0,
            DisputePpnA: 0,
            DisputePphA: 0,
            DisputeTrxB: 0,
            DisputeTotMdrB: 0,
            DisputeDppB: 0,
            DisputePpnB: 0,
            DisputePphB: 0,
          }
        }

        if (DataDiputeCollection[PeriodeDispute]["DataDisputeSalesFeeMin"] == 0) {
          DataDiputeCollection[PeriodeDispute]["DataDisputeSalesFeeMin"] = keyAva["SALES_FEE"];
          DataDiputeCollection[PeriodeDispute]["DataDisputeSalesFeeMax"] = keyAva["SALES_FEE"];
        } else {
          if (keyAva["SALES_FEE"] > DataDiputeCollection[PeriodeDispute]["DataDisputeSalesFeeMax"]) {
            DataDiputeCollection[PeriodeDispute]["DataDisputeSalesFeeMax"] = keyAva["SALES_FEE"];
          } else if (keyAva["SALES_FEE"] < DataDiputeCollection[PeriodeDispute]["DataDisputeSalesFeeMin"]) {
            DataDiputeCollection[PeriodeDispute]["DataDisputeSalesFeeMin"] = keyAva["SALES_FEE"];
          }
        }

        var trxFee = ((parseFloat(keyAva["TRX_A"]) * parseFloat(keyAva["SALES_FEE"])) * (configNum.persen_fee));
        var feeDPP = (trxFee / configNum.num_dpp_fee);
        var feePPN = (feeDPP * configNum.num_ppn_fee);
        var feeTotal = feeDPP + feePPN;
        var feePPH = (feeDPP * configNum.num_pph_fee);

        var trxPaid = ((parseFloat(keyAva["TRX_B"]) * parseFloat(keyAva["SALES_FEE"])) * (configNum.persen_fee));
        var paidDPP = (trxPaid / configNum.num_dpp_paid);
        var paidPPN = (paidDPP * configNum.num_ppn_paid);
        var paidTotal = paidDPP + paidPPN;
        var paidPPH = (paidDPP * configNum.num_pphpaid);


        // if (DataDiputeCollection[PeriodeDispute]["DataDisputeSalesFee"].indexOf(keyAva["SALES_FEE"]) == -1) {
        //   DataDiputeCollection[PeriodeDispute]["DataDisputeSalesFee"].push(keyAva["SALES_FEE"]);
        // }
        DataDiputeCollection[PeriodeDispute]["DisputeTrxA"] = DataDiputeCollection[PeriodeDispute]["DisputeTrxA"] + parseFloat(keyAva["TRX_A"]);
        DataDiputeCollection[PeriodeDispute]["DisputeTotMdrA"] = DataDiputeCollection[PeriodeDispute]["DisputeTotMdrA"] + parseFloat(keyAva["TOT_MDR_A"]);
        DataDiputeCollection[PeriodeDispute]["DisputeDppA"] = DataDiputeCollection[PeriodeDispute]["DisputeDppA"] + feeDPP;
        DataDiputeCollection[PeriodeDispute]["DisputePpnA"] = DataDiputeCollection[PeriodeDispute]["DisputePpnA"] + feePPN;
        DataDiputeCollection[PeriodeDispute]["DisputePphA"] = DataDiputeCollection[PeriodeDispute]["DisputePphA"] + feePPH;



        DataDiputeCollection[PeriodeDispute]["DisputeTrxB"] = DataDiputeCollection[PeriodeDispute]["DisputeTrxB"] + parseFloat(keyAva["TRX_B"]);
        DataDiputeCollection[PeriodeDispute]["DisputeTotMdrB"] = DataDiputeCollection[PeriodeDispute]["DisputeTotMdrB"] + parseFloat(keyAva["TOT_MDR_B"]);
        DataDiputeCollection[PeriodeDispute]["DisputeDppB"] = DataDiputeCollection[PeriodeDispute]["DisputeDppB"] + paidDPP;
        DataDiputeCollection[PeriodeDispute]["DisputePpnB"] = DataDiputeCollection[PeriodeDispute]["DisputePpnB"] + paidPPN;
        DataDiputeCollection[PeriodeDispute]["DisputePphB"] = DataDiputeCollection[PeriodeDispute]["DisputePphB"] + paidPPH;

        DataDiputeCollection[PeriodeDispute]["DataDispute"].push({
          DEALER_CODE: keyAva["DEALER_CODE"],
          NAME: keyAva["NAME"],
          YEARS: keyAva["YEARS"],
          MONTH: keyAva["MONTH"],
          SALES_FEE: keyAva["SALES_FEE"],
          TRX_A: keyAva["TRX_A"],
          TOT_MDR_A: keyAva["TOT_MDR_A"],
          TRX_B: keyAva["TRX_B"],
          TOT_MDR_B: keyAva["TOT_MDR_B"]
        });

      }


    }).on("end", async () => {

      if (error == false) {
        for (var i in DataDiputeCollection) {
          for (var j in DataDiputeCollection[i]["DataDispute"]) {
            var cluster = await this.adClusterRepository.findOne({where: {and: [{ad_code: DisputeAdCode}, {ad_cluster_id: DataDiputeCollection[i]["DataDispute"][j]["DEALER_CODE"]}]}});
            if (cluster == null || cluster.ad_cluster_id == undefined) {
              error = true;
              erroMsg = erroMsg + ", Error can't find dealer_code " + DataDiputeCollection[i]["DataDispute"][j]["DEALER_CODE"] + ", please contact admin !";
            }
          }
        }

        if (error == true) {
          console.log(erroMsg);
          ControllerConfig.onAPISendMail(
            [{email: infoMasterAD.email}],
            "Error Format Excel Dispute !",
            [{
              "type": "text/plain",
              "value": "\n Error Format Excel  !, Please check your template for dispute digipos ! \n " + erroMsg
            }], function (r: any) {

            });
        } else {
          for (var i in DataDiputeCollection) {
            var infoCollectionDispute = DataDiputeCollection[i];
            var SalesFeeDispute: string; SalesFeeDispute = "";
            if (infoCollectionDispute.DataDisputeSalesFeeMin == infoCollectionDispute.DataDisputeSalesFeeMax) {
              SalesFeeDispute = "" + infoCollectionDispute.DataDisputeSalesFeeMin;
            } else {
              SalesFeeDispute = "" + infoCollectionDispute.DataDisputeSalesFeeMin + " - Rp." + infoCollectionDispute.DataDisputeSalesFeeMax;
            }

            var today = new Date(i);
            var todays = today.setMonth(today.getMonth() + 1);
            var DateNew = new Date(todays);
            var datePeriode = DateNew.getFullYear() + '-' + ('0' + (DateNew.getMonth() + 1)).slice(-2);

            var hasDispute = await this.disputeRepository.findOne({where: {and: [{periode: datePeriode}, {ad_code: DisputeAdCode}]}});
            if (hasDispute != null) {
              if (hasDispute.acc_ba == 0) {

                await this.disputeRepository.updateById(hasDispute.dispute_id, {
                  ad_code: DisputeAdCode,
                  ad_name: DisputeAdName,
                  remarks: "",
                  acc_ba: 0,
                  periode: datePeriode,
                  at_create: ControllerConfig.onCurrentTime().toString(),
                  at_acc: "",
                  sales_fee: SalesFeeDispute,
                  trx_a: infoCollectionDispute.DisputeTrxA,
                  tot_mdr_a: infoCollectionDispute.DisputeTotMdrA,
                  trx_b: infoCollectionDispute.DisputeTrxB,
                  tot_mdr_b: infoCollectionDispute.DisputeTotMdrB,
                  dpp_fee_amount: infoCollectionDispute.DisputeDppA,
                  ppn_fee_amount: infoCollectionDispute.DisputePpnA,
                  pph_fee_amount: infoCollectionDispute.DisputePphA,
                  dpp_paid_amount: infoCollectionDispute.DisputeDppB,
                  ppn_paid_amount: infoCollectionDispute.DisputePpnB,
                  pph_paid_amount: infoCollectionDispute.DisputePphB
                });




                await this.disputeDetailRepository.deleteAll({dispute_id: hasDispute.dispute_id});
                for (var j in DataDiputeCollection[i]["DataDispute"]) {
                  var infoDispute = DataDiputeCollection[i]["DataDispute"][j];

                  var trxFee = ((infoDispute.TRX_A * infoDispute.SALES_FEE) * (configNum.persen_fee));
                  var feeDPP = (trxFee / configNum.num_dpp_fee);
                  var feePPN = (feeDPP * configNum.num_ppn_fee);
                  var feeTotal = feeDPP + feePPN;
                  var feePPH = (feeDPP * configNum.num_pph_fee);

                  var trxPaid = ((infoDispute.TRX_B * infoDispute.SALES_FEE) * (configNum.persen_fee));
                  var paidDPP = (trxPaid / configNum.num_dpp_paid);
                  var paidPPN = (paidDPP * configNum.num_ppn_paid);
                  var paidTotal = paidDPP + paidPPN;
                  var paidPPH = (paidDPP * configNum.num_pph_paid);


                  await this.disputeDetailRepository.create({
                    ad_cluster_id: infoDispute.DEALER_CODE,
                    ad_cluster_name: infoDispute.NAME,
                    periode: datePeriode,
                    sales_fee: infoDispute.SALES_FEE,
                    trx_a: infoDispute.TRX_A,
                    tot_mdr_a: trxFee,
                    dpp_fee_amount: feeDPP,
                    ppn_fee_amount: feePPN,
                    pph_fee_amount: feePPH,
                    trx_b: infoDispute.TRX_B,
                    tot_mdr_b: trxPaid,
                    dpp_paid_amount: paidDPP,
                    ppn_paid_amount: paidPPN,
                    pph_paid_amount: paidPPH,
                    dispute_id: hasDispute.dispute_id
                  });

                }
              }
            } else {
              await this.disputeRepository.create({
                ad_code: DisputeAdCode,
                ad_name: DisputeAdName,
                remarks: "",
                acc_ba: 0,
                periode: datePeriode,
                at_create: ControllerConfig.onCurrentTime().toString(),
                sales_fee: SalesFeeDispute,
                trx_a: infoCollectionDispute.DisputeTrxA,
                tot_mdr_a: infoCollectionDispute.DisputeTotMdrA,
                trx_b: infoCollectionDispute.DisputeTrxB,
                tot_mdr_b: infoCollectionDispute.DisputeTotMdrB,
                dpp_fee_amount: infoCollectionDispute.DisputeDppA,
                ppn_fee_amount: infoCollectionDispute.DisputePpnA,
                pph_fee_amount: infoCollectionDispute.DisputePphA,
                dpp_paid_amount: infoCollectionDispute.DisputeDppB,
                ppn_paid_amount: infoCollectionDispute.DisputePpnB,
                pph_paid_amount: infoCollectionDispute.DisputePphB
              }).then(async (r) => {
                for (var j in DataDiputeCollection[i]["DataDispute"]) {
                  var infoDispute = DataDiputeCollection[i]["DataDispute"][j];

                  var trxFee = ((infoDispute.TRX_A * infoDispute.SALES_FEE) * (configNum.persen_fee));
                  var feeDPP = (trxFee / configNum.num_dpp_fee);
                  var feePPN = (feeDPP * configNum.num_ppn_fee);
                  var feeTotal = feeDPP + feePPN;
                  var feePPH = (feeDPP * configNum.num_pph_fee);

                  var trxPaid = ((infoDispute.TRX_B * infoDispute.SALES_FEE) * (configNum.persen_fee));
                  var paidDPP = (trxPaid / configNum.num_dpp_paid);
                  var paidPPN = (paidDPP * configNum.num_ppn_paid);
                  var paidTotal = paidDPP + paidPPN;
                  var paidPPH = (paidDPP * configNum.num_pph_paid);

                  await this.disputeDetailRepository.create({
                    ad_cluster_id: infoDispute.DEALER_CODE,
                    ad_cluster_name: infoDispute.NAME,
                    periode: datePeriode,
                    sales_fee: infoDispute.SALES_FEE,
                    trx_a: infoDispute.TRX_A,
                    tot_mdr_a: trxFee,
                    dpp_fee_amount: feeDPP,
                    ppn_fee_amount: feePPN,
                    pph_fee_amount: feePPH,
                    trx_b: infoDispute.TRX_B,
                    tot_mdr_b: trxPaid,
                    dpp_paid_amount: paidDPP,
                    ppn_paid_amount: paidPPN,
                    pph_paid_amount: paidPPH,
                    dispute_id: r.dispute_id
                  });
                }
              });
            }
          }
        }

      } else {
        console.log(erroMsg);
        ControllerConfig.onAPISendMail(
          [{email: infoMasterAD.email}],
          "Error Format Excel Dispute !",
          [{
            "type": "text/plain",
            "value": "\n Error Format Excel  !, Please check your template for dispute digipos ! \n " + erroMsg
          }], function (r: any) {

          });
      }
    });
  }

  onRunningNotaDebet = (infoMasterAD: any, path: any) => {
    var filePath = path;
    var KeyRow: any; KeyRow = {
      DATE: '',
      NOMOR: '',
      NOMOR_BAST: '',
      NOMOR_PERJANJIAN: '',
      NO: '1',
      DESCRIPTION: '',
      QTY: '',
      UM: '',
      UNIT_PRICE: ''
    }
    var InfoNota: any; InfoNota = {
      DATE: '',
      NOMOR: '',
      NOMOR_BAST: '',
      NOMOR_PERJANJIAN: '',
    }
    var DataNota: any; DataNota = [];
    var error: boolean; error = false;
    var erroMsg: any; erroMsg = "";
    fs.createReadStream(filePath).pipe(csv()).on('data', async (row: any) => {
      // console.log(row);
      var keyAva: any; keyAva = {};
      for (var i in row) {
        var key = i.toLocaleUpperCase().trim();
        keyAva[key] = row[i];
      }
      // console.log(keyAva);

      var FieldNotFound = false;
      var FieldError: any; FieldError = [];
      for (var j in KeyRow) {
        if (keyAva[j] == undefined) {
          error = true;
          FieldNotFound = true;
          FieldError.push(j);
          continue;
        }
      }
      if (FieldNotFound == true) {
        erroMsg = erroMsg + ",Error can't found field " + JSON.stringify(FieldError);
      }

      if (keyAva["DATE"] != "" && InfoNota["DATE"] == "") {
        InfoNota["DATE"] = keyAva["DATE"];
      }
      if (keyAva["NOMOR"] != "" && InfoNota["NOMOR"] == "") {
        InfoNota["NOMOR"] = keyAva["NOMOR"];
      }
      if (keyAva["NOMOR_BAST"] != "" && InfoNota["NOMOR_BAST"] == "") {
        InfoNota["NOMOR_BAST"] = keyAva["NOMOR_BAST"];
      }
      if (keyAva["NOMOR_PERJANJIAN"] != "" && InfoNota["NOMOR_PERJANJIAN"] == "") {
        InfoNota["NOMOR_PERJANJIAN"] = keyAva["NOMOR_PERJANJIAN"];
      }

      if (FieldNotFound == false) {
        DataNota.push({
          DESCRIPTION: keyAva["DESCRIPTION"],
          QTY: keyAva["QTY"],
          UM: keyAva["UM"],
          UNIT_PRICE: keyAva["UNIT_PRICE"],
          TOTAL_PRICE: (parseInt(keyAva["QTY"]) * parseFloat(keyAva["UNIT_PRICE"])).toFixed(2)
        });
      }

    }).on("end", async () => {
      console.log(error);
      // console.log(InfoNota);
      // console.log(DataNota);
      if (error == false) {
        var dataDebet = await this.notaDebetRepository.findOne({where: {bast_number: InfoNota.NOMOR_BAST}});
        var datePeriode = InfoNota.DATE.toString().split("-");
        var DateNotaPeriode = "";
        if (datePeriode[1] == undefined || datePeriode[2] == undefined) {

        } else {
          var hasBA = await this.dataDigiposRepository.count({ba_number_a: InfoNota.NOMOR_BAST});
          if (hasBA.count > 0) {
            DateNotaPeriode = datePeriode[2] + "-" + datePeriode[1];
            if (dataDebet?.bast_number != null || dataDebet?.bast_number != undefined) {
              await this.notaDebetRepository.updateById(dataDebet.nota_debet_id, {
                periode: DateNotaPeriode,
                nota_date: InfoNota.DATE,
                nota_number: InfoNota.NOMOR,
                bast_number: InfoNota.NOMOR_BAST,
                perjanjian_number: InfoNota.NOMOR_PERJANJIAN,
                ad_code: infoMasterAD.ad_code,
                nota_state: 0,
                total_price: 0
              });
              await this.notaDebetDetailRepository.deleteAll({nota_debet_id: dataDebet.nota_debet_id});
              var TotalPrice: number; TotalPrice = 0;
              for (var iDetail in DataNota) {
                var info = DataNota[iDetail];
                await this.notaDebetDetailRepository.create({
                  description: info.DESCRIPTION,
                  qty: parseInt(info.QTY),
                  unit: info.UM,
                  unit_price: info.UNIT_PRICE,
                  total_price: info.TOTAL_PRICE,
                  nota_debet_id: dataDebet.nota_debet_id
                });
                TotalPrice = TotalPrice + parseFloat(info.TOTAL_PRICE);
              }
              await this.notaDebetRepository.updateById(dataDebet.nota_debet_id, {
                total_price: TotalPrice
              });
            } else {
              await this.notaDebetRepository.create({
                periode: DateNotaPeriode,
                nota_date: InfoNota.DATE,
                nota_number: InfoNota.NOMOR,
                bast_number: InfoNota.NOMOR_BAST,
                perjanjian_number: InfoNota.NOMOR_PERJANJIAN,
                ad_code: infoMasterAD.ad_code,
                nota_state: 0,
                total_price: 0
              });
              var dataDebet = await this.notaDebetRepository.findOne({where: {bast_number: InfoNota.NOMOR_BAST}});
              var TotalPrice: number; TotalPrice = 0;
              for (var iDetail in DataNota) {
                var info = DataNota[iDetail];
                await this.notaDebetDetailRepository.create({
                  description: info.DESCRIPTION,
                  qty: parseInt(info.QTY),
                  unit: info.UM,
                  unit_price: info.UNIT_PRICE,
                  total_price: info.TOTAL_PRICE,
                  nota_debet_id: dataDebet?.nota_debet_id
                });
                TotalPrice = TotalPrice + parseFloat(info.TOTAL_PRICE);
              }
              await this.notaDebetRepository.updateById(dataDebet?.nota_debet_id, {
                total_price: TotalPrice
              });
            }
          } else {
            ControllerConfig.onAPISendMail(
              [{email: infoMasterAD.email}],
              "Error Format Excel Nota Debet !",
              [{
                "type": "text/plain",
                "value": "\n Error Format Excel Nota Debet !, data number berita acara not exities : [" + InfoNota.NOMOR_BAST + "] ! \n "
              }], function (r: any) {

              });
          }
        }
      } else {
        ControllerConfig.onAPISendMail(
          [{email: infoMasterAD.email}],
          "Error Format Excel Nota Debet !",
          [{
            "type": "text/plain",
            "value": "\n Error Format Excel Nota Debet !, Please check your template for nota debet digipos ! \n " + erroMsg
          }], function (r: any) {

          });
      }
    });
  }

  // @get('/download-google-drives/distribute-faktur-pajak', {
  //   responses: {
  //     '200': {
  //       description: 'DownloadGoogleDrive model instance',
  //       content: {'application/json': {schema: {}}},
  //     },
  //   },
  // })
  // async distributeFakturPajak(
  // ): Promise<Object> {
  //   fs.readFile(GDCONFIG, async (errConfig, contentConfig: any) => {
  //     if (errConfig) return console.log('Error loading root folder:', errConfig);
  //     var configGD = JSON.parse(contentConfig);
  //     const ProcessDistribute = configGD.FAKTUR_PAJAK_PROCESS;
  //     const SuccessDistribute = configGD.FAKTUR_PAJAK_SUCCESS;
  //     const FailedDistribute = configGD.FAKTUR_PAJAK_FAILED;
  //     // "process_distribute": "1vN4EtF95RwZhEFb_sLTE9BmSQoBtW5yT",
  //     // "success_distribute": "1VykcsgeI6EYMDvEd8-KbT6w3BVg5CnCK",
  //     // "failed_distribute": "1yDMf_L5izyG3jUq3USlqNXWXXEngq2EP"
  //     ControllerConfig.onAPIGoogleDrive(async (rConnect: any) => {
  //       if (rConnect.success == true) {
  //         const drive = rConnect.drive;
  //         const auth = rConnect.auth;
  //         ControllerConfig.onGetFileOnFolder(auth, ProcessDistribute, (rFiles: any) => {
  //           if (rFiles.length) {
  //             rFiles.map(async (file: any) => {
  //               var ID = file.id;
  //               var NameFile = file.name;
  //               var NameInfo = NameFile.split("_");
  //               if (NameInfo.length == 3) {
  //                 var Periode = NameInfo[0];
  //                 var CodeAD = NameInfo[1];
  //                 var Faktur = NameInfo[2];
  //                 var FakturCode = Faktur.substring(0, Faktur.length - 4);
  //                 console.log(Periode + " - " + CodeAD + " - " + Faktur.substring(0, Faktur.length - 4));
  //                 const MasterAD = await this.adMasterRepository.findOne({where: {ad_code: CodeAD}});
  //                 const infoDigi = await this.dataDigiposRepository.findOne({where: {and: [{trx_date_group: Periode}, {ad_code: CodeAD}, {vat_number: FakturCode}]}});
  //                 if (infoDigi?.digipost_id != null && infoDigi?.digipost_id != undefined) {
  //                   const fileId = ID;//'1sTWaJ_j7PkjzaBWtNc3IzovK5hQf21FbOw9yLeeLPNQ'
  //                   const folderId = MasterAD?.gd_faktur_pajak;//'0BwwA4oUTeiV1TGRPeTVjaWRDY1E'
  //                   // Retrieve the existing parents to remove
  //                   drive.files.get({
  //                     fileId: fileId,
  //                     fields: 'parents'
  //                   }, (err: any, file: any) => {
  //                     if (err) {
  //                       // Handle error
  //                       // console.error(err);
  //                     } else {
  //                       // console.log(file);
  //                       // Move the file to the new folder
  //                       var previousParents = file.data.parents[0];
  //                       drive.files.update({
  //                         fileId: fileId,
  //                         addParents: folderId,
  //                         removeParents: previousParents,
  //                         fields: 'id, parents'
  //                       }, async (err: any, file: any) => {
  //                         if (err) {
  //                           // Handle error
  //                         } else {
  //                           // File moved.
  //                           try {
  //                             await this.dataDigiposRepository.updateById(infoDigi.digipost_id, {
  //                               vat_attachment: NameFile + "--" + fileId
  //                             });
  //                             ControllerConfig.onAPISendMail([{email: MasterAD?.email}],
  //                               "Faktur Pajak From Digipos",
  //                               [{
  //                                 "type": "text/plain",
  //                                 "value": "\n You have new Faktur Pajak File : " + NameFile + ", please chek your google drive digipos, " + '<a href="https://drive.google.com/drive/my-drive" >' + " Open Google Drvice</a> ! \n "
  //                               }],
  //                               function (r: any) {

  //                               });
  //                           } catch (error) {

  //                           }
  //                         }
  //                       });
  //                     }
  //                   });
  //                 }
  //               }
  //             });
  //           }
  //         });
  //       }
  //     });
  //   });
  //   return {success: true, message: "success"};
  // }


  @get('/download-google-drives/distribute-faktur-pajak', {
    responses: {
      '200': {
        description: 'DownloadGoogleDrive model instance',
        content: {'application/json': {schema: {}}},
      },
    },
  })
  async distributeFakturPajak(
  ): Promise<Object> {
    fs.readFile(GDCONFIG, async (errConfig, contentConfig: any) => {
      if (errConfig) return console.log('Error loading root folder:', errConfig);
      var configGD = JSON.parse(contentConfig);
      const ProcessDistribute = configGD.FAKTUR_PAJAK_PROCESS;
      const SuccessDistribute = configGD.FAKTUR_PAJAK_SUCCESS;
      const FailedDistribute = configGD.FAKTUR_PAJAK_FAILED;
      // "process_distribute": "1vN4EtF95RwZhEFb_sLTE9BmSQoBtW5yT",
      // "success_distribute": "1VykcsgeI6EYMDvEd8-KbT6w3BVg5CnCK",
      // "failed_distribute": "1yDMf_L5izyG3jUq3USlqNXWXXEngq2EP"
      ControllerConfig.onAPIGoogleDrive(async (rConnect: any) => {
        if (rConnect.success == true) {
          const drive = rConnect.drive;
          const auth = rConnect.auth;
          ControllerConfig.onGetFileOnFolder(auth, ProcessDistribute, (rFiles: any) => {
            if (rFiles.length) {
              rFiles.map(async (file: any) => {
                var ID = file.id;
                var NameFile = file.name;
                var NameInfo = NameFile.substring(0, NameFile.length - 4);
                const DigiposData = await this.dataDigiposRepository.findOne({where: {vat_number: NameInfo}});
                if (DigiposData != null) {
                  await this.dataDigiposRepository.updateById(DigiposData.digipost_id, {
                    vat_distribute: 1,
                    vat_attachment: "",
                    at_vat_distibute: ""
                  });
                  const masterAD = await this.adMasterRepository.findById(DigiposData.ad_code);
                  var nameFileDownload = "FP_" + DigiposData.vat_number?.split("/").join("_") + "_" + DigiposData.ad_name + "_" + DigiposData.ad_code + "_" + DigiposData.trx_date_group + ".pdf";
                  var pathDest = path.join(__dirname, '../../.digipost/faktur-pajak/' + nameFileDownload);
                  var dest = fs.createWriteStream(pathDest);
                  try {
                    drive.files.get({fileId: ID, alt: 'media'}, {responseType: 'stream'},
                      (err: any, res: any) => {
                        res.data
                          .on('end', async () => {
                            ControllerConfig.onGD_Search(auth, "'" + masterAD.gd_faktur_pajak + "' in parents and	name = '" + nameFileDownload + "'", async (rSearchFp: any) => {
                              var fileFp = rSearchFp.data.data.files;
                              if (fileFp.length == 0) {
                                ControllerConfig.onUploadPDFGD(auth, masterAD.gd_faktur_pajak, pathDest, nameFileDownload, async (rUp: any) => {
                                  if (rUp.success == true) {
                                    await this.dataDigiposRepository.updateById(DigiposData.digipost_id, {
                                      vat_distribute: 2,
                                      vat_attachment: nameFileDownload,
                                      at_vat_distibute: ControllerConfig.onCurrentTime().toString()
                                    });
                                    ControllerConfig.onGD_MoveFile(auth, ID, ProcessDistribute, SuccessDistribute, (r: any) => {
                                      try {
                                        // ControllerConfig.onAPISendMail([{email: masterAD.email}],
                                        //   "Faktur Pajak From Digipos",
                                        //   [{
                                        //     "type": "text/plain",
                                        //     "value": "\n You have new Faktur Pajak File : " + NameFile + ", please chek your google drive digipos, " + '<a href="https://drive.google.com/drive/my-drive" >' + " Open Google Drvice</a> ! \n "
                                        //   }],
                                      } catch (error) {

                                      }
                                    });
                                  } else {
                                    await this.dataDigiposRepository.updateById(DigiposData.digipost_id, {
                                      vat_distribute: 0
                                    });
                                    ControllerConfig.onGD_MoveFile(auth, ID, ProcessDistribute, FailedDistribute, (r: any) => {
                                      try {
                                        // ControllerConfig.onAPISendMail([{email: masterAD.email}],
                                        //   "Faktur Pajak From Digipos",
                                        //   [{
                                        //     "type": "text/plain",
                                        //     "value": "\n You have new Faktur Pajak File : " + NameFile + ", please chek your google drive digipos, " + '<a href="https://drive.google.com/drive/my-drive" >' + " Open Google Drvice</a> ! \n "
                                        //   }],
                                      } catch (error) {

                                      }
                                    });
                                  }
                                });
                              } else {
                                var field: string; field = "";
                                for (var j in fileFp) {
                                  var rFileInv = fileFp[j];
                                  field = rFileInv.id
                                }
                                ControllerConfig.onUploadPDFGDReplace(auth, field, masterAD.gd_faktur_pajak, pathDest, nameFileDownload, async (rUp: any) => {
                                  if (rUp.success == true) {
                                    await this.dataDigiposRepository.updateById(DigiposData.digipost_id, {
                                      vat_distribute: 2,
                                      vat_attachment: nameFileDownload,
                                      at_vat_distibute: ControllerConfig.onCurrentTime().toString()
                                    });
                                    ControllerConfig.onGD_MoveFile(auth, ID, ProcessDistribute, SuccessDistribute, (r: any) => {
                                      try {
                                        // ControllerConfig.onAPISendMail([{email: masterAD.email}],
                                        //   "Faktur Pajak From Digipos",
                                        //   [{
                                        //     "type": "text/plain",
                                        //     "value": "\n You have new Faktur Pajak File : " + NameFile + ", please chek your google drive digipos, " + '<a href="https://drive.google.com/drive/my-drive" >' + " Open Google Drvice</a> ! \n "
                                        //   }],
                                      } catch (error) {

                                      }
                                    });
                                  } else {
                                    await this.dataDigiposRepository.updateById(DigiposData.digipost_id, {
                                      vat_distribute: 0
                                    });
                                    ControllerConfig.onGD_MoveFile(auth, ID, ProcessDistribute, FailedDistribute, (r: any) => {
                                      try {
                                        // ControllerConfig.onAPISendMail([{email: masterAD.email}],
                                        //   "Faktur Pajak From Digipos",
                                        //   [{
                                        //     "type": "text/plain",
                                        //     "value": "\n You have new Faktur Pajak File : " + NameFile + ", please chek your google drive digipos, " + '<a href="https://drive.google.com/drive/my-drive" >' + " Open Google Drvice</a> ! \n "
                                        //   }],
                                      } catch (error) {

                                      }
                                    });
                                  }
                                });
                              }
                            });
                            console.log('Done : ', nameFileDownload);
                          })
                          .on('error', async (err: any) => {
                            console.log('Error', err);
                            await this.dataDigiposRepository.updateById(DigiposData.digipost_id, {
                              vat_distribute: 0
                            });
                          })
                          .pipe(dest);
                      }
                    );
                  } catch (error) {
                    console.log(error);
                    await this.dataDigiposRepository.updateById(DigiposData.digipost_id, {
                      vat_distribute: 0
                    });
                  }
                } else {
                  ControllerConfig.onGD_MoveFile(auth, ID, ProcessDistribute, FailedDistribute, (r: any) => {
                    try {
                      // ControllerConfig.onAPISendMail([{email: masterAD.email}],
                      //   "Faktur Pajak From Digipos",
                      //   [{
                      //     "type": "text/plain",
                      //     "value": "\n You have new Faktur Pajak File : " + NameFile + ", please chek your google drive digipos, " + '<a href="https://drive.google.com/drive/my-drive" >' + " Open Google Drvice</a> ! \n "
                      //   }],
                    } catch (error) {

                    }
                  });
                }
              });
            }
          });
        }
      });
    });
    return {success: true, message: "success"};
  }



  @get('/download-google-drives/distribute-invoice-sign', {
    responses: {
      '200': {
        description: 'DownloadGoogleDrive model instance',
        content: {'application/json': {schema: {}}},
      },
    },
  })
  async distributeInvoiceSign(
  ): Promise<Object> {
    fs.readFile(GDCONFIG, async (errConfig, contentConfig: any) => {
      if (errConfig) return console.log('Error loading root folder:', errConfig);
      var configGD = JSON.parse(contentConfig);
      const ProcessDistribute = configGD.INVOICE_PROCESS;
      const SuccessDistribute = configGD.INVOICE_SUCCESS;
      const FailedDistribute = configGD.INVOICE_FAILED;
      // "process_distribute": "1vN4EtF95RwZhEFb_sLTE9BmSQoBtW5yT",
      // "success_distribute": "1VykcsgeI6EYMDvEd8-KbT6w3BVg5CnCK",
      // "failed_distribute": "1yDMf_L5izyG3jUq3USlqNXWXXEngq2EP"
      ControllerConfig.onAPIGoogleDrive(async (rConnect: any) => {
        if (rConnect.success == true) {
          const drive = rConnect.drive;
          const auth = rConnect.auth;
          ControllerConfig.onGetFileOnFolder(auth, ProcessDistribute, (rFiles: any) => {
            if (rFiles.length) {
              rFiles.map(async (file: any) => {
                var ID = file.id;
                var NameFile = file.name;
                var NameInfo = NameFile.substring(0, NameFile.length - 4);
                const DigiposData = await this.dataDigiposRepository.findOne({where: {inv_number: NameInfo}});
                if (DigiposData != null) {
                  await this.dataDigiposRepository.updateById(DigiposData.digipost_id, {
                    distribute_invoice_ttd: 1,
                    attach_invoice_ttd: "",
                    at_distribute_invoice_ttd: ""
                  });
                  const masterAD = await this.adMasterRepository.findById(DigiposData.ad_code);
                  var nameFileDownload = "SIGN_" + DigiposData.inv_number?.split("/").join("_") + "_" + DigiposData.ad_name + "_" + DigiposData.ad_code + "_" + DigiposData.trx_date_group + ".pdf";
                  var pathDest = path.join(__dirname, '../../.digipost/invoice/' + nameFileDownload);
                  var dest = fs.createWriteStream(pathDest);
                  try {
                    drive.files.get({fileId: ID, alt: 'media'}, {responseType: 'stream'},
                      (err: any, res: any) => {
                        res.data
                          .on('end', async () => {
                            ControllerConfig.onGD_Search(auth, "'" + masterAD.gd_invoice + "' in parents and	name = '" + nameFileDownload + "'", async (rSearchInvoice: any) => {
                              var filesinvSign = rSearchInvoice.data.data.files;
                              if (filesinvSign.length == 0) {
                                ControllerConfig.onUploadPDFGD(auth, masterAD.gd_invoice, pathDest, nameFileDownload, async (rUp: any) => {
                                  if (rUp.success == true) {
                                    await this.dataDigiposRepository.updateById(DigiposData.digipost_id, {
                                      distribute_invoice_ttd: 2,
                                      attach_invoice_ttd: nameFileDownload,
                                      at_distribute_invoice_ttd: ControllerConfig.onCurrentTime().toString()
                                    });
                                    ControllerConfig.onGD_MoveFile(auth, ID, ProcessDistribute, SuccessDistribute, (r: any) => {
                                      try {
                                        // ControllerConfig.onAPISendMail([{email: masterAD.email}],
                                        //   "Faktur Pajak From Digipos",
                                        //   [{
                                        //     "type": "text/plain",
                                        //     "value": "\n You have new Faktur Pajak File : " + NameFile + ", please chek your google drive digipos, " + '<a href="https://drive.google.com/drive/my-drive" >' + " Open Google Drvice</a> ! \n "
                                        //   }],
                                      } catch (error) {

                                      }
                                    });
                                  } else {
                                    await this.dataDigiposRepository.updateById(DigiposData.digipost_id, {
                                      distribute_invoice_ttd: 0
                                    });
                                    ControllerConfig.onGD_MoveFile(auth, ID, ProcessDistribute, FailedDistribute, (r: any) => {
                                      try {
                                        // ControllerConfig.onAPISendMail([{email: masterAD.email}],
                                        //   "Faktur Pajak From Digipos",
                                        //   [{
                                        //     "type": "text/plain",
                                        //     "value": "\n You have new Faktur Pajak File : " + NameFile + ", please chek your google drive digipos, " + '<a href="https://drive.google.com/drive/my-drive" >' + " Open Google Drvice</a> ! \n "
                                        //   }],
                                      } catch (error) {

                                      }
                                    });
                                  }
                                });
                              } else {
                                var field: string; field = "";
                                for (var j in filesinvSign) {
                                  var rFileInv = filesinvSign[j];
                                  field = rFileInv.id
                                }
                                ControllerConfig.onUploadPDFGDReplace(auth, field, masterAD.gd_invoice, pathDest, nameFileDownload, async (rUp: any) => {
                                  if (rUp.success == true) {
                                    await this.dataDigiposRepository.updateById(DigiposData.digipost_id, {
                                      distribute_invoice_ttd: 2,
                                      attach_invoice_ttd: nameFileDownload,
                                      at_distribute_invoice_ttd: ControllerConfig.onCurrentTime().toString()
                                    });
                                    ControllerConfig.onGD_MoveFile(auth, ID, ProcessDistribute, SuccessDistribute, (r: any) => {
                                      try {
                                        // ControllerConfig.onAPISendMail([{email: masterAD.email}],
                                        //   "Faktur Pajak From Digipos",
                                        //   [{
                                        //     "type": "text/plain",
                                        //     "value": "\n You have new Faktur Pajak File : " + NameFile + ", please chek your google drive digipos, " + '<a href="https://drive.google.com/drive/my-drive" >' + " Open Google Drvice</a> ! \n "
                                        //   }],
                                      } catch (error) {

                                      }
                                    });
                                  } else {
                                    await this.dataDigiposRepository.updateById(DigiposData.digipost_id, {
                                      distribute_invoice_ttd: 0
                                    });
                                    ControllerConfig.onGD_MoveFile(auth, ID, ProcessDistribute, FailedDistribute, (r: any) => {
                                      try {
                                        // ControllerConfig.onAPISendMail([{email: masterAD.email}],
                                        //   "Faktur Pajak From Digipos",
                                        //   [{
                                        //     "type": "text/plain",
                                        //     "value": "\n You have new Faktur Pajak File : " + NameFile + ", please chek your google drive digipos, " + '<a href="https://drive.google.com/drive/my-drive" >' + " Open Google Drvice</a> ! \n "
                                        //   }],
                                      } catch (error) {

                                      }
                                    });
                                  }
                                });
                              }
                            });
                            console.log('Done : ', nameFileDownload);
                          })
                          .on('error', async (err: any) => {
                            console.log('Error', err);
                            await this.dataDigiposRepository.updateById(DigiposData.digipost_id, {
                              distribute_invoice_ttd: 0
                            });
                          })
                          .pipe(dest);
                      }
                    );
                  } catch (error) {
                    console.log(error);
                    await this.dataDigiposRepository.updateById(DigiposData.digipost_id, {
                      distribute_invoice_ttd: 0
                    });
                  }
                } else {
                  ControllerConfig.onGD_MoveFile(auth, ID, ProcessDistribute, FailedDistribute, (r: any) => {
                    try {
                      // ControllerConfig.onAPISendMail([{email: masterAD.email}],
                      //   "Faktur Pajak From Digipos",
                      //   [{
                      //     "type": "text/plain",
                      //     "value": "\n You have new Faktur Pajak File : " + NameFile + ", please chek your google drive digipos, " + '<a href="https://drive.google.com/drive/my-drive" >' + " Open Google Drvice</a> ! \n "
                      //   }],
                    } catch (error) {

                    }
                  });
                }
              });
            }
          });
        }
      });
    });
    return {success: true, message: "success"};
  }


  @get('/download-google-drives/distribute-bukpot', {
    responses: {
      '200': {
        description: 'DownloadGoogleDrive model instance',
        content: {'application/json': {schema: {}}},
      },
    },
  })
  async distributeBukpot(
  ): Promise<Object> {
    fs.readFile(GDCONFIG, async (errConfig, contentConfig: any) => {
      if (errConfig) return console.log('Error loading root folder:', errConfig);
      var configGD = JSON.parse(contentConfig);
      const ProcessDistribute = configGD.BUKPOT_PROCESS;
      const SuccessDistribute = configGD.BUKPOT_SUCCESS;
      const FailedDistribute = configGD.BUKPOT_FAILED;
      ControllerConfig.onAPIGoogleDrive(async (rConnect: any) => {
        if (rConnect.success == true) {
          const drive = rConnect.drive;
          const auth = rConnect.auth;

          ControllerConfig.onGetFileOnFolder(auth, ProcessDistribute, (rFiles: any) => {
            if (rFiles.length) {
              rFiles.map(async (file: any) => {
                var ID = file.id;
                var NameFile = file.name.split("_");
                if (NameFile.length == 2) {
                  var Periode = NameFile[0];
                  var Npwp = NameFile[1].substring(0, NameFile[1].length - 4);
                  var masterAD = await this.adMasterRepository.findOne({where: {npwp: Npwp}});
                  if (masterAD != null) {
                    var GDBukPot = masterAD.gd_bukpot;
                    var nameFileDownload = file.name;
                    var pathDest = path.join(__dirname, '../../.digipost/bukpot/' + nameFileDownload);
                    var dest = fs.createWriteStream(pathDest);
                    try {
                      drive.files.get({fileId: ID, alt: 'media'}, {responseType: 'stream'},
                        (err: any, res: any) => {
                          res.data
                            .on('end', async () => {
                              ControllerConfig.onGD_Search(auth, "'" + GDBukPot + "' in parents and	name = '" + nameFileDownload + "'", async (rSearchFp: any) => {
                                var fileFp = rSearchFp.data.data.files;
                                if (fileFp.length == 0) {
                                  ControllerConfig.onUploadPDFGD(auth, GDBukPot, pathDest, nameFileDownload, async (rUp: any) => {
                                    if (rUp.success == true) {
                                      ControllerConfig.onGD_MoveFile(auth, ID, ProcessDistribute, SuccessDistribute, (r: any) => {
                                        try {
                                          // ControllerConfig.onAPISendMail([{email: masterAD.email}],
                                          //   "Faktur Pajak From Digipos",
                                          //   [{
                                          //     "type": "text/plain",
                                          //     "value": "\n You have new Faktur Pajak File : " + NameFile + ", please chek your google drive digipos, " + '<a href="https://drive.google.com/drive/my-drive" >' + " Open Google Drvice</a> ! \n "
                                          //   }],
                                        } catch (error) {

                                        }
                                      });
                                    } else {
                                      ControllerConfig.onGD_MoveFile(auth, ID, ProcessDistribute, FailedDistribute, (r: any) => {
                                        try {
                                          // ControllerConfig.onAPISendMail([{email: masterAD.email}],
                                          //   "Faktur Pajak From Digipos",
                                          //   [{
                                          //     "type": "text/plain",
                                          //     "value": "\n You have new Faktur Pajak File : " + NameFile + ", please chek your google drive digipos, " + '<a href="https://drive.google.com/drive/my-drive" >' + " Open Google Drvice</a> ! \n "
                                          //   }],
                                        } catch (error) {

                                        }
                                      });
                                    }
                                  });
                                } else {
                                  var field: string; field = "";
                                  for (var j in fileFp) {
                                    var rFileInv = fileFp[j];
                                    field = rFileInv.id
                                  }
                                  ControllerConfig.onUploadPDFGDReplace(auth, field, GDBukPot, pathDest, nameFileDownload, async (rUp: any) => {
                                    if (rUp.success == true) {
                                      ControllerConfig.onGD_MoveFile(auth, ID, ProcessDistribute, SuccessDistribute, (r: any) => {
                                        try {
                                          // ControllerConfig.onAPISendMail([{email: masterAD.email}],
                                          //   "Faktur Pajak From Digipos",
                                          //   [{
                                          //     "type": "text/plain",
                                          //     "value": "\n You have new Faktur Pajak File : " + NameFile + ", please chek your google drive digipos, " + '<a href="https://drive.google.com/drive/my-drive" >' + " Open Google Drvice</a> ! \n "
                                          //   }],
                                        } catch (error) {

                                        }
                                      });
                                    } else {
                                      ControllerConfig.onGD_MoveFile(auth, ID, ProcessDistribute, FailedDistribute, (r: any) => {
                                        try {
                                          // ControllerConfig.onAPISendMail([{email: masterAD.email}],
                                          //   "Faktur Pajak From Digipos",
                                          //   [{
                                          //     "type": "text/plain",
                                          //     "value": "\n You have new Faktur Pajak File : " + NameFile + ", please chek your google drive digipos, " + '<a href="https://drive.google.com/drive/my-drive" >' + " Open Google Drvice</a> ! \n "
                                          //   }],
                                        } catch (error) {

                                        }
                                      });
                                    }
                                  });
                                }
                              });
                              console.log('Done : ', nameFileDownload);
                            })
                            .on('error', async (err: any) => {
                              console.log('Error', err);
                            })
                            .pipe(dest);
                        }
                      );
                    } catch (error) {
                      console.log(error);
                    }
                  } else {
                    console.log("NOT FOUNT AD");
                  }

                } else {
                  console.log(file.name);
                }
              });
            }
          });
        }
      });
    });
    return {success: true, message: "success"};
  }

  @post('/download-google-drives/file', {
    responses: {
      '200': {
        description: 'DownloadGoogleDrive model instance',
        content: {'application/json': {schema: getModelSchemaRef(DownloadGoogleDrive)}},
      },
    },
  })
  async find(
    @requestBody(GetGoogleDriveFile) googleDriveFile: {gd_file_group: number},
  ): Promise<Object> {

    //NOTA DEBET
    // if (googleDriveFile.gd_file_group == 1) {
    //   var DataMasterAD = await this.adMasterRepository.find();
    //   for (var i in DataMasterAD) {
    //     const infoMasterAD = DataMasterAD[i];
    //     const NotaPath = infoMasterAD.gd_code_nota_debet;
    //     if (NotaPath != "" && NotaPath != undefined) {
    //       fs.readFile(GDCONFIG, async (errConfig, contentConfig: any) => {
    //         if (errConfig) return console.log('Error loading root folder:', errConfig);
    //         var configGD = JSON.parse(contentConfig);
    //         var RootID = configGD.root_directory;
    //         ControllerConfig.onAPIGoogleDrive(async (rConnect: any) => {
    //           if (rConnect.success == true) {
    //             var drive = rConnect.drive;
    //             var auth = rConnect.auth;
    //             ControllerConfig.onGetFileOnFolder(auth, NotaPath, (rFiles: any) => {
    //               console.log(NotaPath);
    //               console.log(rFiles);
    //               if (rFiles.length) {
    //                 rFiles.map(async (file: any) => {
    //                   var filePath = path.join(__dirname, '../../.digipost/nota_debet/' + file.id + ".csv");
    //                   console.log(file.id);

    //                   // console.log(r);

    //                   var fileName = file.id + ".csv";
    //                   var dataGoogle = await this.downloadGoogleDriveRepository.count({and: [{gd_file_name: fileName}, {gd_file_state: 1}]});
    //                   if (dataGoogle.count == 0) {
    //                     ControllerConfig.onDownloadFileCSV(auth, file.id, filePath, async (r: any) => {
    //                       if (r.success == true) {
    //                         this.downloadGoogleDriveRepository.create({
    //                           gd_file_name: fileName,
    //                           gd_file_code: file.id,
    //                           gd_file_group: googleDriveFile.gd_file_group,
    //                           gd_file_state: 1,
    //                           gd_file_run: 0,
    //                           ad_code: infoMasterAD.ad_code,
    //                           gd_date: Date.now().toString(),
    //                         })
    //                         this.onRunningNotaDebet(infoMasterAD, filePath);

    //                       }
    //                     });
    //                   }
    //                   // console.log(`${file.name} (${file.id})`);
    //                   // var fileId = '1ZdR3L3qP4Bkq8noWLJHSr_iBau0DNT4Kli4SxNc2YEo';
    //                   // var dest = fs.createWriteStream('/tmp/resume.pdf');
    //                   // drive.files.export({
    //                   //   fileId: fileId,
    //                   //   mimeType: 'application/pdf'
    //                   // })
    //                   //     .on('end', function () {
    //                   //       console.log('Done');
    //                   //     })
    //                   //     .on('error', function (err) {
    //                   //       console.log('Error during download', err);
    //                   //     })
    //                   //     .pipe(dest);
    //                 });
    //               } else {
    //                 console.log('No files found.');
    //               }
    //             });
    //           }
    //         });
    //       });
    //     }
    //   }
    // }

    //INVOICE
    // if (googleDriveFile.gd_file_group == 2) {
    //   var DataMasterAD = await this.adMasterRepository.find();
    //   for (var i in DataMasterAD) {
    //     const infoMasterAD = DataMasterAD[i];
    //     const InvoicePath = infoMasterAD.gd_code_paid_inv;
    //     if (InvoicePath != "" && InvoicePath != undefined) {
    //       fs.readFile(GDCONFIG, async (errConfig, contentConfig: any) => {
    //         if (errConfig) return console.log('Error loading root folder:', errConfig);
    //         var configGD = JSON.parse(contentConfig);
    //         var RootID = configGD.root_directory;
    //         ControllerConfig.onAPIGoogleDrive(async (rConnect: any) => {
    //           if (rConnect.success == true) {
    //             var drive = rConnect.drive;
    //             var auth = rConnect.auth;
    //             ControllerConfig.onGetFileOnFolder(auth, InvoicePath, (rFiles: any) => {
    //               if (rFiles.length) {
    //                 rFiles.map(async (file: any) => {
    //                   var fileName = file.id + ".csv";
    //                   var dataGoogle = await this.downloadGoogleDriveRepository.count({and: [{gd_file_name: fileName}, {gd_file_state: 1}]});
    //                   if (dataGoogle.count == 0) {
    //                     console.log(file);
    //                     var fileNameGD = file.name;
    //                     var dataDigipost = await this.dataDigiposRepository.findOne({where: {inv_attacment: fileNameGD}});
    //                     this.downloadGoogleDriveRepository.create({
    //                       gd_file_name: fileName,
    //                       gd_file_code: file.id,
    //                       gd_file_group: googleDriveFile.gd_file_group,
    //                       gd_file_state: 1,
    //                       gd_file_run: 0,
    //                       ad_code: infoMasterAD.ad_code,
    //                       gd_date: Date.now().toString(),
    //                     })
    //                     this.onRunningInvoice(infoMasterAD, dataDigipost);
    //                   }

    //                 });
    //               }
    //             });
    //           }
    //         });
    //       });
    //     }

    //   }
    // }

    //DISPUTE
    // if (googleDriveFile.gd_file_group == 3) {
    //   var DataMasterAD = await this.adMasterRepository.find();
    //   for (var i in DataMasterAD) {
    //     const infoMasterAD = DataMasterAD[i];
    //     const NotaPath = infoMasterAD.gd_code_upload_dispute;
    //     if (NotaPath != "" && NotaPath != undefined) {
    //       fs.readFile(GDCONFIG, async (errConfig, contentConfig: any) => {
    //         if (errConfig) return console.log('Error loading root folder:', errConfig);
    //         var configGD = JSON.parse(contentConfig);
    //         var RootID = configGD.root_directory;
    //         ControllerConfig.onAPIGoogleDrive(async (rConnect: any) => {
    //           if (rConnect.success == true) {
    //             var drive = rConnect.drive;
    //             var auth = rConnect.auth;
    //             ControllerConfig.onGetFileOnFolder(auth, NotaPath, (rFiles: any) => {
    //               console.log(NotaPath);
    //               console.log(rFiles);
    //               if (rFiles.length) {
    //                 rFiles.map(async (file: any) => {
    //                   var filePath = path.join(__dirname, '../../.digipost/nota_debet/' + file.id + ".csv");
    //                   console.log(file.id);

    //                   // console.log(r);

    //                   var fileName = file.id + ".csv";
    //                   var dataGoogle = await this.downloadGoogleDriveRepository.count({and: [{gd_file_name: fileName}, {gd_file_state: 1}]});
    //                   if (dataGoogle.count == 0) {
    //                     ControllerConfig.onDownloadFileCSV(auth, file.id, filePath, async (r: any) => {
    //                       if (r.success == true) {
    //                         this.downloadGoogleDriveRepository.create({
    //                           gd_file_name: fileName,
    //                           gd_file_code: file.id,
    //                           gd_file_group: googleDriveFile.gd_file_group,
    //                           gd_file_state: 1,
    //                           gd_file_run: 0,
    //                           ad_code: infoMasterAD.ad_code,
    //                           gd_date: Date.now().toString(),
    //                         })
    //                         this.onRunningDispute(infoMasterAD, filePath);
    //                       }
    //                     });
    //                   }
    //                   // console.log(`${file.name} (${file.id})`);
    //                   // var fileId = '1ZdR3L3qP4Bkq8noWLJHSr_iBau0DNT4Kli4SxNc2YEo';
    //                   // var dest = fs.createWriteStream('/tmp/resume.pdf');
    //                   // drive.files.export({
    //                   //   fileId: fileId,
    //                   //   mimeType: 'application/pdf'
    //                   // })
    //                   //     .on('end', function () {
    //                   //       console.log('Done');
    //                   //     })
    //                   //     .on('error', function (err) {
    //                   //       console.log('Error during download', err);
    //                   //     })
    //                   //     .pipe(dest);
    //                 });
    //               } else {
    //                 console.log('No files found. ');
    //               }
    //             });
    //           }
    //         });
    //       });
    //     }
    //   }
    // }

    return {}
  }


  onRunningInvoice = async (infoMasterAD: any, digipos: any) => {
    console.log(digipos);
    var dataDebet = await this.notaDebetRepository.findOne({where: {bast_number: digipos.ba_number_b}});
    if (dataDebet?.bast_number != null || dataDebet?.bast_number != undefined) {
      await this.notaDebetRepository.updateById(dataDebet.nota_debet_id, {
        periode: digipos.trx_date_group,
        nota_date: ControllerConfig.onDateNow(),
        nota_number: digipos.inv_number,
        bast_number: digipos.ba_number_b,
        perjanjian_number: infoMasterAD.contract_number,
        ad_code: infoMasterAD.ad_code,
        nota_state: 0,
        total_price: 0
      });
      await this.notaDebetDetailRepository.deleteAll({nota_debet_id: dataDebet.nota_debet_id});
      var TotalPrice: number; TotalPrice = 0;
      // for (var iDetail in DataNota) {
      //   var info = DataNota[iDetail];
      await this.notaDebetDetailRepository.create({
        description: "SALES FEE",
        qty: 1,
        unit: "UNIT",
        unit_price: digipos.dpp_paid_amount,
        total_price: digipos.dpp_paid_amount,
        nota_debet_id: dataDebet.nota_debet_id
      });
      TotalPrice = digipos.dpp_paid_amount;
      // }
      await this.notaDebetRepository.updateById(dataDebet.nota_debet_id, {
        total_price: TotalPrice
      });
    } else {

      await this.notaDebetRepository.create({
        periode: digipos.trx_date_group,
        nota_date: ControllerConfig.onDateNow(),
        nota_number: digipos.inv_number,
        bast_number: digipos.ba_number_b,
        perjanjian_number: infoMasterAD.contract_number,
        ad_code: infoMasterAD.ad_code,
        nota_state: 0,
        total_price: 0,
        type_ad: "AD_B"
      });
      var dataDebet = await this.notaDebetRepository.findOne({where: {bast_number: digipos.ba_number_b}});
      var TotalPrice: number; TotalPrice = 0;
      // for (var iDetail in DataNota) {
      //   var info = DataNota[iDetail];
      await this.notaDebetDetailRepository.create({
        description: "SALES FEE",
        qty: 1,
        unit: "UNIT",
        unit_price: digipos.dpp_paid_amount,
        total_price: digipos.dpp_paid_amount,
        nota_debet_id: dataDebet?.nota_debet_id
      });
      TotalPrice = digipos.dpp_paid_amount;
      // }
      await this.notaDebetRepository.updateById(dataDebet?.nota_debet_id, {
        total_price: TotalPrice
      });
    }
  }

  // ====================================================================================
  // @post('/download-google-drives', {
  //   responses: {
  //     '200': {
  //       description: 'DownloadGoogleDrive model instance',
  //       content: {'application/json': {schema: getModelSchemaRef(DownloadGoogleDrive)}},
  //     },
  //   },
  // })
  // async create(
  //   @requestBody({
  //     content: {
  //       'application/json': {
  //         schema: getModelSchemaRef(DownloadGoogleDrive, {
  //           title: 'NewDownloadGoogleDrive',
  //           exclude: ['gd_id'],
  //         }),
  //       },
  //     },
  //   })
  //   downloadGoogleDrive: Omit<DownloadGoogleDrive, 'gd_id'>,
  // ): Promise<DownloadGoogleDrive> {
  //   return this.downloadGoogleDriveRepository.create(downloadGoogleDrive);
  // }

  // @get('/download-google-drives/count', {
  //   responses: {
  //     '200': {
  //       description: 'DownloadGoogleDrive model count',
  //       content: {'application/json': {schema: CountSchema}},
  //     },
  //   },
  // })
  // async count(
  //   @param.where(DownloadGoogleDrive) where?: Where<DownloadGoogleDrive>,
  // ): Promise<Count> {
  //   return this.downloadGoogleDriveRepository.count(where);
  // }

  // @get('/download-google-drives', {
  //   responses: {
  //     '200': {
  //       description: 'Array of DownloadGoogleDrive model instances',
  //       content: {
  //         'application/json': {
  //           schema: {
  //             type: 'array',
  //             items: getModelSchemaRef(DownloadGoogleDrive, {includeRelations: true}),
  //           },
  //         },
  //       },
  //     },
  //   },
  // })
  // async find(
  //   @param.filter(DownloadGoogleDrive) filter?: Filter<DownloadGoogleDrive>,
  // ): Promise<DownloadGoogleDrive[]> {
  //   return this.downloadGoogleDriveRepository.find(filter);
  // }

  // @patch('/download-google-drives', {
  //   responses: {
  //     '200': {
  //       description: 'DownloadGoogleDrive PATCH success count',
  //       content: {'application/json': {schema: CountSchema}},
  //     },
  //   },
  // })
  // async updateAll(
  //   @requestBody({
  //     content: {
  //       'application/json': {
  //         schema: getModelSchemaRef(DownloadGoogleDrive, {partial: true}),
  //       },
  //     },
  //   })
  //   downloadGoogleDrive: DownloadGoogleDrive,
  //   @param.where(DownloadGoogleDrive) where?: Where<DownloadGoogleDrive>,
  // ): Promise<Count> {
  //   return this.downloadGoogleDriveRepository.updateAll(downloadGoogleDrive, where);
  // }

  // @get('/download-google-drives/{id}', {
  //   responses: {
  //     '200': {
  //       description: 'DownloadGoogleDrive model instance',
  //       content: {
  //         'application/json': {
  //           schema: getModelSchemaRef(DownloadGoogleDrive, {includeRelations: true}),
  //         },
  //       },
  //     },
  //   },
  // })
  // async findById(
  //   @param.path.number('id') id: number,
  //   @param.filter(DownloadGoogleDrive, {exclude: 'where'}) filter?: FilterExcludingWhere<DownloadGoogleDrive>
  // ): Promise<DownloadGoogleDrive> {
  //   return this.downloadGoogleDriveRepository.findById(id, filter);
  // }

  // @patch('/download-google-drives/{id}', {
  //   responses: {
  //     '204': {
  //       description: 'DownloadGoogleDrive PATCH success',
  //     },
  //   },
  // })
  // async updateById(
  //   @param.path.number('id') id: number,
  //   @requestBody({
  //     content: {
  //       'application/json': {
  //         schema: getModelSchemaRef(DownloadGoogleDrive, {partial: true}),
  //       },
  //     },
  //   })
  //   downloadGoogleDrive: DownloadGoogleDrive,
  // ): Promise<void> {
  //   await this.downloadGoogleDriveRepository.updateById(id, downloadGoogleDrive);
  // }

  // @put('/download-google-drives/{id}', {
  //   responses: {
  //     '204': {
  //       description: 'DownloadGoogleDrive PUT success',
  //     },
  //   },
  // })
  // async replaceById(
  //   @param.path.number('id') id: number,
  //   @requestBody() downloadGoogleDrive: DownloadGoogleDrive,
  // ): Promise<void> {
  //   await this.downloadGoogleDriveRepository.replaceById(id, downloadGoogleDrive);
  // }

  // @del('/download-google-drives/{id}', {
  //   responses: {
  //     '204': {
  //       description: 'DownloadGoogleDrive DELETE success',
  //     },
  //   },
  // })
  // async deleteById(@param.path.number('id') id: number): Promise<void> {
  //   await this.downloadGoogleDriveRepository.deleteById(id);
  // }
}
