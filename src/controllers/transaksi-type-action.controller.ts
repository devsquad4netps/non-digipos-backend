// Uncomment these imports to begin using these cool features!

import {authenticate} from '@loopback/authentication';
import {inject} from '@loopback/core';
import {Filter, FilterExcludingWhere, repository} from '@loopback/repository';
import {get, getModelSchemaRef, param, post, requestBody, ResponseObject} from '@loopback/rest';
import {SecurityBindings, securityId, UserProfile} from '@loopback/security';
import {TransaksiType} from '../models';
import {TransaksiTypeDetailRepository, TransaksiTypeRepository} from '../repositories';
import {ControllerConfig} from './controller.config';

// import {inject} from '@loopback/core';

//CREATE TRANSAKSI TYPE
export const RequestCreateTrxType: ResponseObject = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['TT_LABEL', 'TT_BOBOT_A', 'TT_BOBOT_DPP_A', 'TT_BOBOT_PPN_A', 'TT_BOBOT_PPH23_A', 'TT_AD_A_HAS', 'TT_BOBOT_B', 'TT_BOBOT_DPP_B', 'TT_BOBOT_PPN_B', 'TT_BOBOT_PPH23_B', 'TT_AD_B_HAS', 'TT_FLAG'],
        properties: {
          TT_LABEL: {
            type: 'string'
          },
          TT_BOBOT_A: {
            type: 'number'
          },
          TT_BOBOT_DPP_A: {
            type: 'number'
          },
          TT_BOBOT_PPN_A: {
            type: 'number'
          },
          TT_BOBOT_PPH23_A: {
            type: 'number'
          },
          TT_AD_A_HAS: {
            type: 'number'
          },
          TT_BOBOT_B: {
            type: 'number'
          },
          TT_BOBOT_DPP_B: {
            type: 'number'
          },
          TT_BOBOT_PPN_B: {
            type: 'number'
          },
          TT_BOBOT_PPH23_B: {
            type: 'number'
          },
          TT_AD_B_HAS: {
            type: 'number'
          },
          TT_FLAG: {
            type: 'number'
          }
        }
      }
    },
  },
};

export const RequestUpdateConfigTrxType: ResponseObject = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['TT_TABLE', 'TT_TABLE_KEY', 'TT_TABLE_DATE', 'TT_SALES_A', 'TT_COUNT_TRX_A', 'TT_SALES_B', 'TT_COUNT_TRX_B', 'TT_TABLE_FILTER'],
        properties: {
          TT_TABLE: {
            type: 'string'
          },
          TT_TABLE_KEY: {
            type: 'string'
          },
          TT_TABLE_DATE: {
            type: 'string'
          },
          TT_SALES_A: {
            type: 'string'
          },
          TT_COUNT_TRX_A: {
            type: 'string'
          },
          TT_SALES_B: {
            type: 'string'
          },
          TT_COUNT_TRX_B: {
            type: 'string'
          },
          TT_TABLE_FILTER: {
            type: 'string'
          }
        }
      }
    },
  },
};


export const RequestCreateConfigTrxTypeTable: ResponseObject = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['TTD_KEY', 'TTD_LABEL', 'TTD_FLAG'],
        properties: {
          TTD_KEY: {
            type: 'string'
          },
          TTD_LABEL: {
            type: 'string'
          },
          TTD_FLAG: {
            type: 'number'
          },
        }
      }
    },
  },
};

@authenticate('jwt')
export class TransaksiTypeActionController {
  constructor(
    @repository(TransaksiTypeRepository)
    public transaksiTypeRepository: TransaksiTypeRepository,
    @repository(TransaksiTypeDetailRepository)
    public transaksiTypeDetailRepository: TransaksiTypeDetailRepository,
    @inject(SecurityBindings.USER)
    public currentUserProfile: UserProfile,
  ) { }


  @get('/transaksi-type/views', {
    responses: {
      '200': {
        description: 'Array of transaksi type model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(TransaksiType, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async findTransaksiType(
    @param.filter(TransaksiType) filter?: Filter<TransaksiType>,
  ): Promise<Object> {
    var result: any; result = {success: true, message: "", data: []};
    var trxType = await this.transaksiTypeRepository.find(filter);
    for (var i in trxType) {
      var rawTrxType = trxType[i];
      var data = {
        TT_ID: rawTrxType.TT_ID,
        TT_LABEL: rawTrxType.TT_LABEL,
        TT_AD_A_HAS: rawTrxType.TT_AD_A_HAS,
        TT_AD_B_HAS: rawTrxType.TT_AD_B_HAS,
        TT_BOBOT_A: rawTrxType.TT_BOBOT_A,
        TT_BOBOT_DPP_A: rawTrxType.TT_BOBOT_DPP_A,
        TT_BOBOT_PPN_A: rawTrxType.TT_BOBOT_PPN_A,
        TT_BOBOT_PPH23_A: rawTrxType.TT_BOBOT_PPH23_A,
        TT_BOBOT_B: rawTrxType.TT_BOBOT_B,
        TT_BOBOT_DPP_B: rawTrxType.TT_BOBOT_DPP_B,
        TT_BOBOT_PPN_B: rawTrxType.TT_BOBOT_PPN_B,
        TT_BOBOT_PPH23_B: rawTrxType.TT_BOBOT_PPH23_B,
        TT_FLAG: rawTrxType.TT_FLAG
      }
      result.data.push(data);
    }
    return result;
  }

  @get('/transaksi-type/view/{id}', {
    responses: {
      '200': {
        description: 'Array of transaksi type model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(TransaksiType, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async findTransaksiTypeById(
    @param.path.number('id') id: number,
    @param.filter(TransaksiType, {exclude: 'where'}) filter?: FilterExcludingWhere<TransaksiType>
  ): Promise<Object> {
    var result: any; result = {success: true, message: "", data: {}};
    var trxType = await this.transaksiTypeRepository.findById(id);
    result.data = trxType;
    return result;
  }


  @post('/transaksi-type/create', {
    responses: {
      '200': {
        description: 'Array of DataFpjp model instances',
        content: {
          'application/json': {
            schema: RequestCreateTrxType,
          },
        },
      },
    },
  })
  async createTransaksiType(
    @requestBody(RequestCreateTrxType) regBody: {TT_LABEL: string, TT_BOBOT_A: number, TT_BOBOT_DPP_A: number, TT_BOBOT_PPN_A: number, TT_BOBOT_PPH23_A: number, TT_AD_A_HAS: number, TT_BOBOT_B: number, TT_BOBOT_DPP_B: number, TT_BOBOT_PPN_B: number, TT_BOBOT_PPH23_B: number, TT_AD_B_HAS: number, TT_FLAG: number},
  ): Promise<Object> {
    var userId = this.currentUserProfile[securityId];
    var result: any; result = {success: false, message: "", data: {}};
    try {
      var dataCreate = await this.transaksiTypeRepository.create({
        TT_ID: undefined,
        TT_LABEL: regBody.TT_LABEL,
        TT_BOBOT_A: regBody.TT_BOBOT_A,
        TT_BOBOT_DPP_A: regBody.TT_BOBOT_DPP_A,
        TT_BOBOT_PPN_A: regBody.TT_BOBOT_PPN_A,
        TT_BOBOT_PPH23_A: regBody.TT_BOBOT_PPH23_A,
        TT_AD_A_HAS: regBody.TT_AD_A_HAS,
        TT_BOBOT_B: regBody.TT_BOBOT_B,
        TT_BOBOT_DPP_B: regBody.TT_BOBOT_DPP_B,
        TT_BOBOT_PPN_B: regBody.TT_BOBOT_PPN_B,
        TT_BOBOT_PPH23_B: regBody.TT_BOBOT_PPH23_B,
        TT_AD_B_HAS: regBody.TT_AD_B_HAS,
        TT_FLAG: regBody.TT_FLAG,
        AT_CREATE: ControllerConfig.onCurrentTime().toString(),
        AT_UPDATE: undefined,
        AT_USER: userId
      });
      result.success = true;
      result.message = "success !";
      result.data = {
        TT_ID: dataCreate.TT_ID,
        TT_LABEL: dataCreate.TT_LABEL,
        TT_BOBOT_A: dataCreate.TT_BOBOT_A,
        TT_BOBOT_DPP_A: dataCreate.TT_BOBOT_DPP_A,
        TT_BOBOT_PPN_A: dataCreate.TT_BOBOT_PPN_A,
        TT_BOBOT_PPH23_A: dataCreate.TT_BOBOT_PPH23_A,
        TT_AD_A_HAS: dataCreate.TT_AD_A_HAS,
        TT_BOBOT_B: dataCreate.TT_BOBOT_B,
        TT_BOBOT_DPP_B: dataCreate.TT_BOBOT_DPP_B,
        TT_BOBOT_PPN_B: dataCreate.TT_BOBOT_PPN_B,
        TT_BOBOT_PPH23_B: dataCreate.TT_BOBOT_PPH23_B,
        TT_AD_B_HAS: dataCreate.TT_AD_B_HAS,
        TT_FLAG: dataCreate.TT_FLAG,
        AT_CREATE: dataCreate.AT_CREATE
      }
    } catch (error) {
      result = {success: false, message: error, data: {}};
    }
    return result;
  }

  @post('/transaksi-type/update/{id}', {
    responses: {
      '200': {
        description: 'Array of DataFpjp model instances',
        content: {
          'application/json': {
            schema: RequestCreateTrxType,
          },
        },
      },
    },
  })
  async updateTransaksiType(
    @param.path.number('id') id: number,
    @requestBody(RequestCreateTrxType) regBody: {TT_LABEL: string, TT_BOBOT_A: number, TT_BOBOT_DPP_A: number, TT_BOBOT_PPN_A: number, TT_BOBOT_PPH23_A: number, TT_AD_A_HAS: number, TT_BOBOT_B: number, TT_BOBOT_DPP_B: number, TT_BOBOT_PPN_B: number, TT_BOBOT_PPH23_B: number, TT_AD_B_HAS: number, TT_FLAG: number},
  ): Promise<Object> {
    var userId = this.currentUserProfile[securityId];
    var result: any; result = {success: false, message: "", data: {}};

    var hasId = await this.transaksiTypeRepository.findById(id);
    if (hasId != null) {
      try {
        var dataUpdate = await this.transaksiTypeRepository.updateById(id, {
          TT_LABEL: regBody.TT_LABEL,
          TT_BOBOT_A: regBody.TT_BOBOT_A,
          TT_BOBOT_DPP_A: regBody.TT_BOBOT_DPP_A,
          TT_BOBOT_PPN_A: regBody.TT_BOBOT_PPN_A,
          TT_BOBOT_PPH23_A: regBody.TT_BOBOT_PPH23_A,
          TT_AD_A_HAS: regBody.TT_AD_A_HAS,
          TT_BOBOT_B: regBody.TT_BOBOT_B,
          TT_BOBOT_DPP_B: regBody.TT_BOBOT_DPP_B,
          TT_BOBOT_PPN_B: regBody.TT_BOBOT_PPN_B,
          TT_BOBOT_PPH23_B: regBody.TT_BOBOT_PPH23_B,
          TT_AD_B_HAS: regBody.TT_AD_B_HAS,
          TT_FLAG: regBody.TT_FLAG,
          AT_UPDATE: ControllerConfig.onCurrentTime().toString(),
          AT_USER: userId
        });
        result.success = true;
        result.message = "success !";
        result.data = {
          TT_LABEL: regBody.TT_LABEL,
          TT_BOBOT_A: regBody.TT_BOBOT_A,
          TT_BOBOT_DPP_A: regBody.TT_BOBOT_DPP_A,
          TT_BOBOT_PPN_A: regBody.TT_BOBOT_PPN_A,
          TT_BOBOT_PPH23_A: regBody.TT_BOBOT_PPH23_A,
          TT_AD_A_HAS: regBody.TT_AD_A_HAS,
          TT_BOBOT_B: regBody.TT_BOBOT_B,
          TT_BOBOT_DPP_B: regBody.TT_BOBOT_DPP_B,
          TT_BOBOT_PPN_B: regBody.TT_BOBOT_PPN_B,
          TT_BOBOT_PPH23_B: regBody.TT_BOBOT_PPH23_B,
          TT_AD_B_HAS: regBody.TT_AD_B_HAS,
          TT_FLAG: regBody.TT_FLAG,
          AT_UPDATE: ControllerConfig.onCurrentTime().toString()
        }
      } catch (error) {
        result = {success: false, message: error, data: {}};
      }
    } else {
      result = {success: false, message: "Error, Transaksi Type not found !", data: {}};
    }
    return result;
  }



  @get('/transaksi-type/view/{id}/detail/views', {
    responses: {
      '200': {
        description: 'Array of transaksi type model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(TransaksiType, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async findTransaksiTypeDetailById(
    @param.path.number('id') id: number,
    @param.filter(TransaksiType) filter?: Filter<TransaksiType>,
  ): Promise<Object> {
    var result: any; result = {success: false, message: "", data: {}};
    var trxType = await this.transaksiTypeRepository.findById(id);
    if (trxType != null) {

      var filterSet: any; filterSet = {}
      var filters: any; filters = filter;
      for (var i in filters) {
        filterSet[i] = filters[i];
      }
      if (filterSet.where != undefined) {
        var thisWhere = filterSet.where;
        filterSet.where = {and: [{TT_ID: id}, thisWhere]};
      } else {
        filterSet.where = {and: [{TT_ID: id}]}
      }

      var trxTypeDetail = await this.transaksiTypeDetailRepository.find(filterSet);
      var dataDetail: any; dataDetail = [];
      for (var iDetail in trxTypeDetail) {
        var rawTrxDetail = trxTypeDetail[iDetail];
        var thisData = {
          TTD_ID: rawTrxDetail.TTD_ID,
          TTD_KEY: rawTrxDetail.TTD_KEY,
          TTD_LABEL: rawTrxDetail.TTD_LABEL,
          TTD_FLAG: rawTrxDetail.TTD_FLAG
        };
        dataDetail.push(thisData);
      }
      result.success = true;
      result.message = "success !";
      result.data = {
        TT_TABLE: trxType.TT_TABLE,
        TT_TABLE_KEY: trxType.TT_TABLE_KEY,
        TT_TABLE_DATE: trxType.TT_TABLE_DATE,
        TT_SALES_A: trxType.TT_SALES_A,
        TT_COUNT_TRX_A: trxType.TT_COUNT_TRX_A,
        TT_SALES_B: trxType.TT_SALES_B,
        TT_COUNT_TRX_B: trxType.TT_COUNT_TRX_B,
        TT_TABLE_FILTER: trxType.TT_TABLE_FILTER,
        TT_DETAIL: dataDetail
      }

    } else {
      result = {success: false, message: "Error, Transaksi Type not found !", data: {}};
    }
    return result;
  }


  @post('/transaksi-type/view/{id}/detail/update/config', {
    responses: {
      '200': {
        description: 'Array of DataFpjp model instances',
        content: {
          'application/json': {
            schema: RequestCreateTrxType,
          },
        },
      },
    },
  })
  async updateTransaksiDetailConfig(
    @param.path.number('id') id: number,
    @requestBody(RequestUpdateConfigTrxType) regBody: {TT_TABLE: string, TT_TABLE_KEY: string, TT_TABLE_DATE: string, TT_SALES_A: string, TT_COUNT_TRX_A: string, TT_SALES_B: string, TT_COUNT_TRX_B: string, TT_TABLE_FILTER: string},
  ): Promise<Object> {
    var userId = this.currentUserProfile[securityId];
    var result: any; result = {success: false, message: "", data: {}};
    var hasId = await this.transaksiTypeRepository.findById(id);
    if (hasId != null) {
      try {
        var dataUpdate = await this.transaksiTypeRepository.updateById(id, {
          TT_TABLE: regBody.TT_TABLE,
          TT_TABLE_KEY: regBody.TT_TABLE_KEY,
          TT_TABLE_DATE: regBody.TT_TABLE_DATE,
          TT_SALES_A: regBody.TT_SALES_A,
          TT_COUNT_TRX_A: regBody.TT_COUNT_TRX_A,
          TT_SALES_B: regBody.TT_SALES_B,
          TT_COUNT_TRX_B: regBody.TT_COUNT_TRX_B,
          TT_TABLE_FILTER: regBody.TT_TABLE_FILTER,
          AT_UPDATE: ControllerConfig.onCurrentTime().toString(),
          AT_USER: userId
        });
        result.success = true;
        result.message = "success !";
        result.data = {
          TT_ID: id,
          TT_TABLE: regBody.TT_TABLE,
          TT_TABLE_KEY: regBody.TT_TABLE_KEY,
          TT_TABLE_DATE: regBody.TT_TABLE_DATE,
          TT_SALES_A: regBody.TT_SALES_A,
          TT_COUNT_TRX_A: regBody.TT_COUNT_TRX_A,
          TT_SALES_B: regBody.TT_SALES_B,
          TT_COUNT_TRX_B: regBody.TT_COUNT_TRX_B,
          TT_TABLE_FILTER: regBody.TT_TABLE_FILTER,
          AT_UPDATE: ControllerConfig.onCurrentTime().toString(),
        }
      } catch (error) {
        result = {success: false, message: error, data: {}};
      }
    } else {
      result = {success: false, message: "Error, Update Transaksi Configurasi not found !", data: {}};
    }
    return result;
  }


  @post('/transaksi-type/view/{id}/detail/create-table', {
    responses: {
      '200': {
        description: 'Array of Create Table model instances',
        content: {
          'application/json': {
            schema: RequestCreateTrxType,
          },
        },
      },
    },
  })
  async createTransaksiDetailBigQuery(
    @param.path.number('id') id: number,
    @requestBody(RequestCreateConfigTrxTypeTable) regBody: {TTD_KEY: string, TTD_LABEL: string, TTD_FLAG: number},
  ): Promise<Object> {
    var userId = this.currentUserProfile[securityId];
    var result: any; result = {success: false, message: "", data: {}};
    var hasId = await this.transaksiTypeRepository.findById(id);
    if (hasId != null) {
      try {
        var createDetail = await this.transaksiTypeDetailRepository.create({
          TTD_ID: undefined,
          TTD_KEY: regBody.TTD_KEY,
          TTD_LABEL: regBody.TTD_LABEL,
          TTD_FLAG: regBody.TTD_FLAG,
          AT_CREATE: ControllerConfig.onCurrentTime().toString(),
          AT_UPDATE: undefined,
          AT_USER: userId,
          TT_ID: id
        });
        result.success = true;
        result.message = "success !";
        result.data = {
          TTD_ID: createDetail.TTD_ID,
          TTD_KEY: createDetail.TTD_KEY,
          TTD_LABEL: createDetail.TTD_LABEL,
          TTD_FLAG: createDetail.TTD_FLAG,
          TT_ID: id
        }
      } catch (error) {
        result = {success: false, message: error, data: {}};
      }
    } else {
      result = {success: false, message: "Error, Update Transaksi Type not found !", data: {}};
    }
    return result;
  }


  @post('/transaksi-type/view/{id}/detail/update-table/{id_table}', {
    responses: {
      '200': {
        description: 'Array of Create Table model instances',
        content: {
          'application/json': {
            schema: RequestCreateTrxType,
          },
        },
      },
    },
  })
  async updateTransaksiDetailBigQuery(
    @param.path.number('id') id: number,
    @param.path.number('id_table') id_table: number,
    @requestBody(RequestCreateConfigTrxTypeTable) regBody: {TTD_KEY: string, TTD_LABEL: string, TTD_FLAG: number},
  ): Promise<Object> {
    var userId = this.currentUserProfile[securityId];
    var result: any; result = {success: false, message: "", data: {}};
    var hasId = await this.transaksiTypeRepository.findById(id);
    if (hasId != null) {
      try {
        var updateDetail = await this.transaksiTypeDetailRepository.updateById(id_table, {
          TTD_KEY: regBody.TTD_KEY,
          TTD_LABEL: regBody.TTD_LABEL,
          TTD_FLAG: regBody.TTD_FLAG,
          AT_UPDATE: ControllerConfig.onCurrentTime().toString(),
          AT_USER: userId,
          TT_ID: id
        });
        result.success = true;
        result.message = "success !";
        result.data = {
          TTD_ID: id_table,
          TTD_KEY: regBody.TTD_KEY,
          TTD_LABEL: regBody.TTD_LABEL,
          TTD_FLAG: regBody.TTD_FLAG,
          TT_ID: id
        }
      } catch (error) {
        result = {success: false, message: error, data: {}};
      }
    } else {
      result = {success: false, message: "Error, Update Transaksi Type not found !", data: {}};
    }
    return result;
  }

  @post('/transaksi-type/view/{id}/detail/delete-table/{id_table}', {
    responses: {
      '200': {
        description: 'Array of Create Table model instances',
        content: {
          'application/json': {
            schema: RequestCreateTrxType,
          },
        },
      },
    },
  })
  async deleteTransaksiDetailBigQuery(
    @param.path.number('id') id: number,
    @param.path.number('id_table') id_table: number,
  ): Promise<Object> {
    var userId = this.currentUserProfile[securityId];
    var result: any; result = {success: false, message: ""};
    var hasId = await this.transaksiTypeRepository.findById(id);
    if (hasId != null) {
      try {
        var deleteDetail = await this.transaksiTypeDetailRepository.deleteById(id_table);
        result.success = true;
        result.message = "success !";
      } catch (error) {
        result = {success: false, message: error};
      }
    } else {
      result = {success: false, message: "Error, Update Transaksi Type not found !", data: {}};
    }
    return result;
  }


}
