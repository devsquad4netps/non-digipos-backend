import {authenticate} from '@loopback/authentication';
import {repository} from '@loopback/repository';
import {post, requestBody} from '@loopback/rest';
import fs from 'fs';
import path from 'path';
import PDFMerger from 'pdf-merger-js';
import {AdMasterRepository, DataDigiposRepository, DataFpjpRepository} from '../repositories';
// const mergePDF = require('easy-pdf-merge');
const mergePDF = require('pdf-merge');

export const FileViewPortalADRequestBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['folder', 'names'],
        properties: {
          folder: {
            type: 'string',
          },
          names: {
            type: 'string',
          }
        }
      }
    },
  },
};

export const FileViewFPJPALLRequestBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['fpjp'],
        properties: {
          fpjp: {
            type: 'number',
          }
        }
      }
    },
  },
};

export const FileViewRequestBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['names'],
        properties: {
          names: {
            type: 'string',
          }
        }
      }
    },
  },
};


export const FileViewBarPeriodeRequestBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['periode', "type_ad"],
        properties: {
          periode: {
            type: 'string',
          }
        }
      }
    },
  },
};

@authenticate('jwt')
export class FileviewController {
  constructor(
    @repository(DataDigiposRepository)
    public dataDigiposRepository: DataDigiposRepository,
    @repository(DataFpjpRepository)
    public dataFpjpRepository: DataFpjpRepository,
    @repository(AdMasterRepository)
    public adMasterRepository: AdMasterRepository,
  ) { }

  @post('/files/barrecon-periode-ad-a', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: FileViewBarPeriodeRequestBody
          }
        },
      },
    },
  })
  async FileViewBarReconAPeriode(
    @requestBody(FileViewBarPeriodeRequestBody) fileViewDoc: {periode: string},
  ): Promise<Object> {
    return new Promise<object>(async (resolve, reject) => {
      var result: any; result = {success: false, data: "", message: ""};

      var merger = new PDFMerger();
      var infoData = await this.dataDigiposRepository.find({where: {and: [{trx_date_group: fileViewDoc.periode}, {generate_file_a: 1}]}});
      var noAvaliable: number; noAvaliable = 0;
      for (var i in infoData) {
        var infoBar = infoData[i];
        var FilePathBarA = "";
        try {
          var filePath = path.join(__dirname, '../../.digipost/berita_acara/ad_a/' + infoBar.ba_attachment_a)
          if (fs.existsSync(filePath)) {
            FilePathBarA = filePath;
          }
        } catch (err) {
          console.error(err)
        }
        if (FilePathBarA != "") {
          noAvaliable++;
          merger.add(path.join(__dirname, '../../.digipost/berita_acara/ad_a/' + infoBar.ba_attachment_a));
        }
      }
      if (noAvaliable > 0) {
        await merger.save(path.join(__dirname, '../../.digipost/berita_acara/ad_a/' + "BAR_RECON_AD_A_" + fileViewDoc.periode + "_ALL.pdf")); //save under given name
        var pathAll = path.join(__dirname, '../../.digipost/berita_acara/ad_a/' + "BAR_RECON_AD_A_" + fileViewDoc.periode + "_ALL.pdf")
        fs.readFile(pathAll, function (err, data) {
          if (err) {
            result.success = false;
            result.message = err;
            resolve(result);
            return result;
          } else {
            result.success = true;
            result.data = data.toString("base64");
            result.message = "success";
            resolve(result);
          }
        });
      } else {
        result.success = false;
        result.data = "";
        result.message = "not found bar recon avaliable !";
        resolve(result);
      }
    });
  }

  @post('/files/barrecon-periode-ad-b', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: FileViewBarPeriodeRequestBody
          }
        },
      },
    },
  })
  async FileViewBarReconBPeriode(
    @requestBody(FileViewBarPeriodeRequestBody) fileViewDoc: {periode: string},
  ): Promise<Object> {
    return new Promise<object>(async (resolve, reject) => {
      var result: any; result = {success: false, data: "", message: ""};

      var merger = new PDFMerger();
      var infoData = await this.dataDigiposRepository.find({where: {and: [{trx_date_group: fileViewDoc.periode}, {generate_file_b: 1}]}});
      var noAvaliable: number; noAvaliable = 0;
      for (var i in infoData) {
        var infoBar = infoData[i];
        var FilePathBarA = "";
        try {
          var filePath = path.join(__dirname, '../../.digipost/berita_acara/ad_b/' + infoBar.ba_attachment_b)
          if (fs.existsSync(filePath)) {
            FilePathBarA = filePath;
          }
        } catch (err) {
          console.error(err)
        }
        if (FilePathBarA != "") {
          noAvaliable++;
          merger.add(path.join(__dirname, '../../.digipost/berita_acara/ad_b/' + infoBar.ba_attachment_b));
        }
      }
      if (noAvaliable > 0) {
        await merger.save(path.join(__dirname, '../../.digipost/berita_acara/ad_b/' + "BAR_RECON_AD_B_" + fileViewDoc.periode + "_ALL.pdf")); //save under given name
        var pathAll = path.join(__dirname, '../../.digipost/berita_acara/ad_b/' + "BAR_RECON_AD_B_" + fileViewDoc.periode + "_ALL.pdf")
        fs.readFile(pathAll, function (err, data) {
          if (err) {
            result.success = false;
            result.message = err;
            resolve(result);
            return result;
          } else {
            result.success = true;
            result.data = data.toString("base64");
            result.message = "success";
            resolve(result);
          }
        });
      } else {
        result.success = false;
        result.data = "";
        result.message = "not found bar recon avaliable !";
        resolve(result);
      }
    });
  }

  @post('/files/barrecon', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: FileViewRequestBody
          }
        },
      },
    },
  })
  async FileViewBarRecon(
    @requestBody(FileViewRequestBody) fileViewDoc: {names: string},
  ): Promise<Object> {
    return await new Promise<object>((resolve, reject) => {
      var result: any; result = {success: false, data: "", message: ""};

      var fileNames = fileViewDoc.names.split("/").join("_");
      const filePathA = path.join(__dirname, '../../.digipost/berita_acara/ad_a/' + fileNames);
      const filePathB = path.join(__dirname, '../../.digipost/berita_acara/ad_b/' + fileNames);
      var fileFound = false;
      var filePathFound = "notfound";

      try {
        if (fs.existsSync(filePathA)) {
          //file exists
          fileFound = true;
          filePathFound = filePathA;
        }
      } catch (err) {
        console.error(err)
      }

      try {
        if (fs.existsSync(filePathB)) {
          //file exists
          fileFound = true;
          filePathFound = filePathB;
        }
      } catch (err) {
        console.error(err)
      }

      if (fileFound == true) {
        fs.readFile(filePathFound, function (err, data) {
          if (err) {
            result.success = false;
            result.message = err;
            resolve(result);
            return result;
          } else {
            result.success = true;
            result.data = data.toString("base64");
            result.message = "success";
            resolve(result);
          }
        });
      } else {
        result.success = false;
        result.data = []
        result.message = "File bar recon not found !";
        resolve(result);
      }
    });
  }

  @post('/files/invoice', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: FileViewRequestBody
          }
        },
      },
    },
  })
  async FileViewInvoice(
    @requestBody(FileViewRequestBody) fileViewDoc: {names: string},
  ): Promise<Object> {
    return new Promise<object>((resolve, reject) => {
      var result: any; result = {success: false, data: "", message: ""};
      var fileNames = fileViewDoc.names.split("/").join("_");
      const filePathInvoice = path.join(__dirname, '../../.digipost/invoice/' + fileNames);
      fs.readFile(filePathInvoice, function (err, data) {
        if (err) {
          result.success = false;
          result.message = err;
          resolve(result);
        } else {
          result.success = true;
          result.data = data.toString("base64");
          result.message = "success";
          resolve(result);
        }
      });
    });
  }



  @post('/files/fpjp-all-doc-ad', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: FileViewFPJPALLRequestBody
          }
        },
      },
    },
  })
  async FileFPJPViewAllAD(
    @requestBody(FileViewFPJPALLRequestBody) fileViewDoc: {fpjp: number},
  ): Promise<Object> {
    return new Promise<object>(async (resolve, reject) => {

      var result: any; result = {success: false, data: "", message: ""};
      var no = 0;
      var infoFPJP = await this.dataFpjpRepository.findOne({where: {id: fileViewDoc.fpjp}});

      // const mergePDF = require('easy-pdf-merge');
      var MergePDFFile: any; MergePDFFile = [];

      //DOC FPJP
      if (infoFPJP != null) {

        var merger = new PDFMerger();
        const filePathFPJP = path.join(__dirname, '../../.digipost/fpjp/' + infoFPJP.fpjp_attach);
        await new Promise((resolve, reject) => {
          if (fs.existsSync(filePathFPJP)) {
            MergePDFFile.push(filePathFPJP);
            // merger.add(filePathFPJP);
            resolve({success: true, message: filePathFPJP});
          } else {
            resolve({success: true, message: "Not Found " + filePathFPJP});
          }
        });

        if (infoFPJP.type_trx == 2) {
          //DOC NOTA DEBIT
          const filePathNota = path.join(__dirname, '../../../portal-endpoint/.uploads/nota_debit/nota/' + infoFPJP.inv_attahment);
          if (infoFPJP.inv_attahment != null && infoFPJP.inv_attahment != "" && infoFPJP.inv_attahment != undefined) {
            await new Promise((resolve, reject) => {
              if (fs.existsSync(filePathNota)) {
                MergePDFFile.push(filePathNota);
                // merger.add(filePathNota);
                resolve({success: true, message: filePathNota});
              } else {
                resolve({success: true, message: "Not Found " + filePathNota});
              }
            });
          }
          // DOcC NOTA BAR
          const filePathNotaBar = path.join(__dirname, '../../../portal-endpoint/.uploads/nota_debit/bar_recon/' + infoFPJP.bar_attachment);
          if (infoFPJP.bar_attachment != null && infoFPJP.bar_attachment != "" && infoFPJP.bar_attachment != undefined) {
            await new Promise((resolve, reject) => {
              if (fs.existsSync(filePathNotaBar)) {
                MergePDFFile.push(filePathNotaBar);
                // merger.add(filePathNotaBar);
                resolve({success: true, message: filePathNotaBar});
              } else {
                resolve({success: true, message: "Not Found " + filePathNotaBar});
              }
            });
          }

          //DOC BUKPOT
          const filePathNotaBukpot = path.join(__dirname, '../../../portal-endpoint/.uploads/nota_debit/bukpot/' + infoFPJP.wht_attachment);
          if (infoFPJP.wht_attachment != null && infoFPJP.wht_attachment != "" && infoFPJP.wht_attachment != undefined) {
            await new Promise((resolve, reject) => {
              if (fs.existsSync(filePathNotaBukpot)) {
                MergePDFFile.push(filePathNotaBukpot);
                // merger.add(filePathNotaBukpot);
                resolve({success: true, message: filePathNotaBukpot});
              } else {
                resolve({success: true, message: "Not Found " + filePathNotaBukpot});
              }
            });
          }

          //DOC BUKTI BAYAR
          const filePathNotaBB = path.join(__dirname, '../../../portal-endpoint/.uploads/nota_debit/bukti_bayar/' + infoFPJP.bb_attachment);
          if (infoFPJP.bb_attachment != null && infoFPJP.bb_attachment != "" && infoFPJP.bb_attachment != undefined) {
            await new Promise((resolve, reject) => {
              if (fs.existsSync(filePathNotaBB)) {
                MergePDFFile.push(filePathNotaBB);
                // merger.add(filePathNotaBB);
                resolve({success: true, message: filePathNotaBB});
              } else {
                resolve({success: true, message: "Not Found " + filePathNotaBB});
              }
            });
          }

          //DOC SUMMARY NOTA
          const filePathSummaryNota = path.join(__dirname, '../../../portal-endpoint/.uploads/nota_debit/summary/' + infoFPJP.summary_attachment);
          if (infoFPJP.summary_attachment != null && infoFPJP.summary_attachment != "" && infoFPJP.summary_attachment != undefined) {
            await new Promise((resolve, reject) => {
              if (fs.existsSync(filePathSummaryNota)) {
                MergePDFFile.push(filePathSummaryNota);
                // merger.add(filePathSummaryNota);
                resolve({success: true, message: filePathSummaryNota});
              } else {
                resolve({success: true, message: "Not Found " + filePathSummaryNota});
              }
            });
          }

        }


        if (infoFPJP.type_trx == 1) {
          //DOC INVOICE
          const filePathInv = path.join(__dirname, '../../../portal-endpoint/.uploads/invoice/paid/' + infoFPJP.inv_attahment);
          if (infoFPJP.inv_attahment != null && infoFPJP.inv_attahment != "" && infoFPJP.inv_attahment != undefined) {
            await new Promise((resolve, reject) => {
              if (fs.existsSync(filePathInv)) {
                MergePDFFile.push(filePathInv);
                // merger.add(filePathInv);
                resolve({success: true, message: filePathInv});
              } else {
                resolve({success: true, message: "Not Found " + filePathInv});
              }
            });
          }

          //DOC BAR INVOICE
          const filePathInvBar = path.join(__dirname, '../../../portal-endpoint/.uploads/invoice/bar_recon/' + infoFPJP.bar_attachment);
          if (infoFPJP.bar_attachment != null && infoFPJP.bar_attachment != "" && infoFPJP.bar_attachment != undefined) {
            await new Promise((resolve, reject) => {
              if (fs.existsSync(filePathInvBar)) {
                MergePDFFile.push(filePathInvBar);
                // merger.add(filePathInvBar);
                resolve({success: true, message: filePathInvBar});
              } else {
                resolve({success: true, message: "Not Found " + filePathInvBar});
              }
            });
          }

          //DOC FAKTUR
          const filePathInvFP = path.join(__dirname, '../../../portal-endpoint/.uploads/invoice/faktur_pajak/' + infoFPJP.fp_attachment);
          if (infoFPJP.fp_attachment != null && infoFPJP.fp_attachment != "" && infoFPJP.fp_attachment != undefined) {
            await new Promise((resolve, reject) => {
              if (fs.existsSync(filePathInvFP)) {
                MergePDFFile.push(filePathInvFP);
                // merger.add(filePathInvFP);
                resolve({success: true, message: filePathInvFP});
              } else {
                resolve({success: true, message: "Not Found " + filePathInvFP});
              }
            });
          }

          //DOC SUMMARY INVOICE
          const filePathSummaryInvoice = path.join(__dirname, '../../../portal-endpoint/.uploads/invoice/summary/' + infoFPJP.summary_attachment);
          if (infoFPJP.summary_attachment != null && infoFPJP.summary_attachment != "" && infoFPJP.summary_attachment != undefined) {
            await new Promise((resolve, reject) => {
              if (fs.existsSync(filePathSummaryInvoice)) {
                MergePDFFile.push(filePathSummaryInvoice);
                // merger.add(filePathSummaryInvoice);
                resolve({success: true, message: filePathSummaryInvoice});
              } else {
                resolve({success: true, message: "Not Found " + filePathSummaryInvoice});
              }
            });
          }
        }

        var infoAd = await this.adMasterRepository.findById(infoFPJP.ad_code);
        const filePathPKS = path.join(__dirname, '../../.digipost/profile_npwp/' + infoAd.pks_attachment);
        if (infoAd.pks_attachment != null && infoAd.pks_attachment != "" && infoAd.pks_attachment != undefined) {
          await new Promise((resolve, reject) => {
            if (fs.existsSync(filePathPKS)) {
              MergePDFFile.push(filePathPKS);
              // merger.add(filePathPKS);
              resolve({success: true, message: filePathPKS});
            } else {
              resolve({success: true, message: "Not Found " + filePathPKS});
            }
          });
        }

        const filePathNPWP = path.join(__dirname, '../../.digipost/profile_npwp/' + infoAd.npwp_attachment);
        if (infoAd.npwp_attachment != null && infoAd.npwp_attachment != "" && infoAd.npwp_attachment != undefined) {
          await new Promise((resolve, reject) => {
            if (fs.existsSync(filePathNPWP)) {
              MergePDFFile.push(filePathNPWP);
              // merger.add(filePathNPWP);
              resolve({success: true, message: filePathNPWP});
            } else {
              resolve({success: true, message: "Not Found " + filePathNPWP});
            }
          });
        }

        if (infoFPJP.type_trx == 2) {
          var infoDigipos = await this.dataDigiposRepository.findOne({where: {and: [{ad_code: infoFPJP.ad_code}, {trx_date_group: infoFPJP.periode}]}});
          if (infoDigipos != null) {
            const filePathPaidInvoice = path.join(__dirname, '../../.digipost/doc-distribute/invoice-sign/' + infoDigipos.attach_invoice_ttd);
            if (infoDigipos.attach_invoice_ttd != "" && infoDigipos.attach_invoice_ttd != undefined && infoDigipos.attach_invoice_ttd != null) {
              await new Promise((resolve, reject) => {
                if (fs.existsSync(filePathPaidInvoice)) {
                  MergePDFFile.push(filePathPaidInvoice);
                  // merger.add(filePathPaidInvoice);
                  resolve({success: true, message: filePathPaidInvoice});
                } else {
                  resolve({success: true, message: "Not Found " + filePathPaidInvoice});
                }
              });
            }
          }
        }

        // await merger.save(path.join(__dirname, '../../.digipost/fpjp/ALL_' + "FPJP_" + infoFPJP.transaction_code + "_ALL.pdf")); //save under given name
        var pathAll = path.join(__dirname, '../../.digipost/fpjp/ALL_' + "FPJP_" + infoFPJP.transaction_code + "_ALL.pdf");
        try {
          mergePDF(MergePDFFile, {output: pathAll})
            .then((buffer: any) => {
              fs.readFile(pathAll, function (errRead, data) {
                if (errRead) {
                  result.success = false;
                  result.message = errRead;
                  resolve(result);
                } else {
                  result.success = true;
                  result.data = data.toString("base64");
                  result.message = "success";
                  resolve(result);
                }
              });
            });
        } catch (error) {
          result.success = false;
          result.message = error;
          resolve(result);
        }


        // const mergeOpts = {
        //   maxBuffer: 1024 * 500, // 500kb
        //   maxHeap: '2g' // for setting JVM heap limits to 2GB
        // };
        // var pathMergePDF = path.join(__dirname, '../../.digipost/fpjp/ALL_' + "FPJP_" + infoFPJP.transaction_code + "_ALL.pdf");
        // const FPJPCODE = infoFPJP.transaction_code;
        // await mergePDF(MergePDFFile, pathMergePDF, mergeOpts, (err: any) => {
        //   if (err) {
        //     result.success = false;
        //     result.message = err;
        //     resolve(result);
        //   } else {
        //     // await merger.save(path.join(__dirname, '../../.digipost/fpjp/ALL_' + "FPJP_" + infoFPJP.transaction_code + "_ALL.pdf")); //save under given name
        //     var pathAll = path.join(__dirname, '../../.digipost/fpjp/ALL_' + "FPJP_" + FPJPCODE + "_ALL.pdf");
        //     fs.readFile(pathAll, function (errRead, data) {
        //       if (errRead) {
        //         result.success = false;
        //         result.message = err;
        //         resolve(result);
        //       } else {
        //         result.success = true;
        //         result.data = data.toString("base64");
        //         result.message = "success";
        //         resolve(result);
        //       }
        //     });
        //   }
        // });


      } else {
        result.success = false;
        result.message = "Fpjp Document Not Exities !";
        resolve(result);
      }
    });
  }

  @post('/files/invoice-periode', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: FileViewBarPeriodeRequestBody
          }
        },
      },
    },
  })
  async FileViewInvoicePeriode(
    @requestBody(FileViewBarPeriodeRequestBody) fileViewDoc: {periode: string},
  ): Promise<Object> {
    return new Promise<object>(async (resolve, reject) => {
      var result: any; result = {success: false, data: "", message: ""};
      var merger = new PDFMerger();
      var infoData = await this.dataDigiposRepository.find({where: {and: [{trx_date_group: fileViewDoc.periode}, {generate_file_invoice: 1}]}});
      var noAvaliable: number; noAvaliable = 0;
      for (var i in infoData) {
        var infoBar = infoData[i];
        var FilePathInvoice = "";
        try {
          var filePath = path.join(__dirname, '../../.digipost/invoice/' + infoBar.inv_attacment);
          if (fs.existsSync(filePath)) {
            FilePathInvoice = filePath;
          }
        } catch (err) {
          console.error(err)
        }
        if (FilePathInvoice != "") {
          noAvaliable++;
          merger.add(path.join(__dirname, '../../.digipost/invoice/' + infoBar.inv_attacment));
        }
      }

      if (noAvaliable > 0) {
        await merger.save(path.join(__dirname, '../../.digipost/invoice/' + "INVOICE_" + fileViewDoc.periode + "_ALL.pdf")); //save under given name
        var pathAll = path.join(__dirname, '../../.digipost/invoice/' + "INVOICE_" + fileViewDoc.periode + "_ALL.pdf")
        fs.readFile(pathAll, function (err, data) {
          if (err) {
            result.success = false;
            result.message = err;
            resolve(result);
            return result;
          } else {
            result.success = true;
            result.data = data.toString("base64");
            result.message = "success";
            resolve(result);
          }
        });
      } else {
        result.success = false;
        result.data = "";
        result.message = "not found bar recon avaliable !";
        resolve(result);
      }
    });
  }


  @post('/files/faktur-pajak', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: FileViewRequestBody
          }
        },
      },
    },
  })
  async FileViewFakturPajak(
    @requestBody(FileViewRequestBody) fileViewDoc: {names: string},
  ): Promise<Object> {
    return new Promise<object>((resolve, reject) => {
      var result: any; result = {success: false, data: "", message: ""};
      var fileNames = fileViewDoc.names.split("/").join("_");
      const filePath = path.join(__dirname, '../../.digipost/faktur-pajak/' + fileNames);
      fs.readFile(filePath, function (err, data) {
        if (err) {
          result.success = false;
          result.message = err;
          resolve(result);
        } else {
          result.success = true;
          result.data = data.toString("base64");
          result.message = "success";
          resolve(result);
        }
      });
    });
  }

  // SKB_PPH23_FINARYA_PERIODE_27_AGT31_DES_2021
  @post('/files/doc_information', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: FileViewRequestBody
          }
        },
      },
    },
  })
  async FileViewDocInv(
    @requestBody(FileViewRequestBody) fileViewDoc: {names: string},
  ): Promise<Object> {
    return new Promise<object>((resolve, reject) => {
      var result: any; result = {success: false, data: "", message: ""};
      var fileNames = fileViewDoc.names.split("/").join("_");
      const filePath = path.join(__dirname, '../../.digipost/doc_information/' + fileNames);
      fs.readFile(filePath, function (err, data) {
        if (err) {
          result.success = false;
          result.message = err;
          resolve(result);
        } else {
          result.success = true;
          result.data = data.toString("base64");
          result.message = "success";
          resolve(result);
        }
      });
    });
  }



  @post('/files/fpjp', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: FileViewRequestBody
          }
        },
      },
    },
  })
  async FileViewFpjp(
    @requestBody(FileViewRequestBody) fileViewDoc: {names: string},
  ): Promise<Object> {
    return new Promise<object>((resolve, reject) => {
      var result: any; result = {success: false, data: "", message: ""};
      var fileNames = fileViewDoc.names.split("/").join("_");
      const filePath = path.join(__dirname, '../../.digipost/fpjp/' + fileNames);
      fs.readFile(filePath, function (err, data) {
        if (err) {
          result.success = false;
          result.message = err;
          resolve(result);
        } else {
          result.success = true;
          result.data = data.toString("base64");
          result.message = "success";
          resolve(result);
        }
      });
    });
  }

  @post('/files/npwp', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: FileViewRequestBody
          }
        },
      },
    },
  })
  async FileViewNpwp(
    @requestBody(FileViewRequestBody) fileViewDoc: {names: string},
  ): Promise<Object> {
    return new Promise<object>((resolve, reject) => {
      var result: any; result = {success: false, data: "", message: ""};
      var fileNames = fileViewDoc.names.split("/").join("_");
      const filePath = path.join(__dirname, '../../.digipost/profile_npwp/' + fileNames);
      fs.readFile(filePath, function (err, data) {
        if (err) {
          result.success = false;
          result.message = err;
          resolve(result);
        } else {
          result.success = true;
          result.data = data.toString("base64");
          result.message = "success";
          resolve(result);
        }
      });
    });
  }

  @post('/files/sign', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: FileViewRequestBody
          }
        },
      },
    },
  })
  async FileViewSign(
    @requestBody(FileViewRequestBody) fileViewDoc: {names: string},
  ): Promise<Object> {
    return new Promise<object>((resolve, reject) => {
      var result: any; result = {success: false, data: "", message: ""};
      var fileNames = fileViewDoc.names.split("/").join("_");
      const filePath = path.join(__dirname, '../../.digipost/sign_atten/' + fileNames);
      fs.readFile(filePath, function (err, data) {
        if (err) {
          result.success = false;
          result.message = err;
          resolve(result);
        } else {
          result.success = true;
          result.data = data.toString("base64");
          result.message = "success";
          resolve(result);
        }
      });
    });
  }


  @post('/files/invoice-portal-ad', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: FileViewPortalADRequestBody
          }
        },
      },
    },
  })
  async FileViewInvoicePortalAD(
    @requestBody(FileViewPortalADRequestBody) fileViewDoc: {folder: string, names: string},
  ): Promise<Object> {

    return new Promise<object>((resolve, reject) => {
      var result: any; result = {success: false, data: "", message: ""};
      var fileNames = fileViewDoc.names.split("/").join("_");
      var folderNames = fileViewDoc.folder.split("/").join("_");
      const filePathBarB = path.join(__dirname, '../../../portal-endpoint/.uploads/invoice/' + folderNames + "/" + fileNames);
      fs.readFile(filePathBarB, function (err, data) {
        if (err) {
          result.success = false;
          result.message = err;
          resolve(result);
        } else {
          result.success = true;
          result.data = data.toString("base64");
          result.message = "success";
          resolve(result);
        }
      });
    });
  }

  @post('/files/nota-portal-ad', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: FileViewPortalADRequestBody
          }
        },
      },
    },
  })
  async FileViewNotaPortalAD(
    @requestBody(FileViewPortalADRequestBody) fileViewDoc: {folder: string, names: string},
  ): Promise<Object> {
    return new Promise<object>((resolve, reject) => {
      var result: any; result = {success: false, data: "", message: ""};
      var fileNames = fileViewDoc.names.split("/").join("_");
      var folderNames = fileViewDoc.folder.split("/").join("_");
      const filePathBarB = path.join(__dirname, '../../../portal-endpoint/.uploads/nota_debit/' + folderNames + "/" + fileNames);
      fs.readFile(filePathBarB, function (err, data) {
        if (err) {
          result.success = false;
          result.message = err;
          resolve(result);
        } else {
          result.success = true;
          result.data = data.toString("base64");
          result.message = "success";
          resolve(result);
        }
      });
    });
  }




  @post('/files/distribute/bukpot', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: FileViewRequestBody
          }
        },
      },
    },
  })
  async FileViewDistributeBupot(
    @requestBody(FileViewRequestBody) fileViewDoc: {names: string},
  ): Promise<Object> {
    return new Promise<object>((resolve, reject) => {
      var result: any; result = {success: false, data: "", message: ""};
      var fileNames = fileViewDoc.names.split("/").join("_");
      const filePathInvoice = path.join(__dirname, '../../.digipost/doc-distribute/bukpot/' + fileNames);
      fs.readFile(filePathInvoice, function (err, data) {
        if (err) {
          result.success = false;
          result.message = err;
          resolve(result);
        } else {
          result.success = true;
          result.data = data.toString("base64");
          result.message = "success";
          resolve(result);
        }
      });
    });
  }

  @post('/files/distribute/invoice-sign', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: FileViewRequestBody
          }
        },
      },
    },
  })
  async FileViewDistributeInvoiceSign(
    @requestBody(FileViewRequestBody) fileViewDoc: {names: string},
  ): Promise<Object> {
    return new Promise<object>((resolve, reject) => {
      var result: any; result = {success: false, data: "", message: ""};
      var fileNames = fileViewDoc.names.split("/").join("_");
      const filePathInvoice = path.join(__dirname, '../../.digipost/doc-distribute/invoice-sign/' + fileNames);
      fs.readFile(filePathInvoice, function (err, data) {
        if (err) {
          result.success = false;
          result.message = err;
          resolve(result);
        } else {
          result.success = true;
          result.data = data.toString("base64");
          result.message = "success";
          resolve(result);
        }
      });
    });
  }

  @post('/files/distribute/faktur-pajak', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: FileViewRequestBody
          }
        },
      },
    },
  })
  async FileViewDistributeFakturPajak(
    @requestBody(FileViewRequestBody) fileViewDoc: {names: string},
  ): Promise<Object> {
    return new Promise<object>((resolve, reject) => {
      var result: any; result = {success: false, data: "", message: ""};
      var fileNames = fileViewDoc.names.split("/").join("_");
      const filePathInvoice = path.join(__dirname, '../../.digipost/doc-distribute/faktur-pajak/' + fileNames);
      fs.readFile(filePathInvoice, function (err, data) {
        if (err) {
          result.success = false;
          result.message = err;
          resolve(result);
        } else {
          result.success = true;
          result.data = data.toString("base64");
          result.message = "success";
          resolve(result);
        }
      });
    });
  }


}
