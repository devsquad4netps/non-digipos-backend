import {authenticate} from '@loopback/authentication';
import {inject} from '@loopback/core';
// Uncomment these imports to begin using these cool features!
import {Entity, Filter, model, property, repository} from '@loopback/repository';
import {del, get, getModelSchemaRef, param, post, Request, requestBody, Response, RestBindings} from '@loopback/rest';
import csv from 'csv-parser';
// import {inject} from '@loopback/core';
import fs from 'fs';
import multer from 'multer';
import path from 'path';
import progressStream from 'progress-stream';
import {ErrorLog, FileSycronize} from '../models';
import {AdClusterRepository, AdMasterRepository, DataconfigRepository, DataDigiposDetailRepository, DataDigiposRepository, DataPppobDetailRepository, DataRechargeDetailRepository, DataSharePercentRepository, DataSumarryRepository, ErrorLogRepository, FileSycronizeRepository, SummaryTmpRepository} from '../repositories';
import {ControllerConfig} from './controller.config';

@model()
export class ComponentFileSyc extends Entity {
  @property({
    type: 'string',
    required: true,
  })
  periode: string;
}

var SycProcess: any; SycProcess = {};
var ProcessValidate: any; ProcessValidate = {};
var error: any; error = new Array();


var excelProcess: any; excelProcess = {PPOB: false, RECHARGE: false};
var excelProcessProgres: any; excelProcessProgres = {PPOB: {Datas: [], MaxProcess: 0, CurrentProcess: 0}, RECHARGE: {Datas: [], MaxProcess: 0, CurrentProcess: 0}};




export const ProgresMigrateRequestBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ['periode'],
        properties: {
          periode: {
            type: 'string',
          }
        }
      }
    },
  },
};


@authenticate('jwt')
export class FileSycActionController {
  constructor(
    @repository(FileSycronizeRepository)
    public fileSycronizeRepository: FileSycronizeRepository,
    @repository(SummaryTmpRepository)
    public summaryTmpRepository: SummaryTmpRepository,
    @repository(AdClusterRepository)
    public adClusterRepository: AdClusterRepository,
    @repository(DataSumarryRepository)
    public dataSumarryRepository: DataSumarryRepository,
    @repository(DataDigiposRepository)
    public dataDigiposRepository: DataDigiposRepository,
    @repository(DataDigiposDetailRepository)
    public dataDigiposDetailRepository: DataDigiposDetailRepository,
    @repository(DataconfigRepository)
    public dataconfigRepository: DataconfigRepository,
    @repository(ErrorLogRepository)
    public errorLogRepository: ErrorLogRepository,
    @repository(DataSharePercentRepository)
    public dataSharePercentRepository: DataSharePercentRepository,
    @repository(DataPppobDetailRepository)
    public dataPppobDetailRepository: DataPppobDetailRepository,
    @repository(DataRechargeDetailRepository)
    public dataRechargeDetailRepository: DataRechargeDetailRepository,
    @repository(AdMasterRepository)
    public adMasterRepository: AdMasterRepository,
  ) { }


  SycFileValidate = async (infoFileSyc: FileSycronize, Periode: string) => {

    var Index = ProcessValidate[Periode].index;
    var Datas = ProcessValidate[Periode].data;
    var DataError = ProcessValidate[Periode].error;

    if (Datas[Index] == undefined) {

    } else {
      let persentProcess = ((Index + 1) / infoFileSyc.process_total) * 100;
      var updateProcess = await this.fileSycronizeRepository.updateById(infoFileSyc.file_syc_id, {
        process_count: (Index + 1),
        process_percent: persentProcess
      });

      var DataSummary = Datas[Index];
      var foundCLuster = await this.adClusterRepository.findOne({where: {ad_cluster_id: DataSummary.dealer_code}});
      if (foundCLuster?.ad_cluster_id == undefined) {
        DataError.push({message: "error cluster code not found in data master !!", Error: DataSummary});
      } else {
        var FoundSummary = await this.summaryTmpRepository.findOne({where: {runnig_group: infoFileSyc.periode}});
        if (FoundSummary?.id == undefined) {
          try {
            let addSummaryTmp = await this.summaryTmpRepository.create(DataSummary);
          } catch (err) {
            DataError.push({message: err, Error: DataSummary});
          }
        } else {
          try {
            let addSummaryTmp = await this.summaryTmpRepository.updateById(FoundSummary?.id, DataSummary);
          } catch (err) {
            DataError.push({message: err, Error: DataSummary});
          }
        }
      }
      ProcessValidate[Periode].index = Index + 1;
      this.SycFileValidate(infoFileSyc, Periode);
    }

    if (Index == (Datas.length - 1)) {
      console.log(DataError);
      if (DataError.length > 0) {
        for (var iErr in DataError) {
          var infError = DataError[iErr];
          var today = new Date();
          var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
          var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
          var dateTime = date + ' ' + time;

          var InsertErrorLog = await this.errorLogRepository.create({
            create_at: dateTime,
            error_log_message: infError.message,
            error_log_info: JSON.stringify(infError.Error),
            error_log_type: "MIGRATION_DATA_VALIDATION",
            error_log_source: infoFileSyc.file_syc_id
          });
        }

        var today = new Date();
        var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        var dateTime = date + ' ' + time;
        var updateProcess = await this.fileSycronizeRepository.updateById(infoFileSyc.file_syc_id, {
          error_state: true,
          error_message: "Error migration data validation !!",
          code_state: 1,
          end_at: dateTime
        });

      } else {

        const deleteSumary = await this.dataSumarryRepository.deleteAll({periode: Periode});
        const deleteDataDigipost = await this.dataDigiposRepository.deleteAll({trx_date_group: Periode});
        const deleteDataDigipostDetail = await this.dataDigiposDetailRepository.deleteAll({trx_date_group: Periode});

        var DDigiposDetail: any; DDigiposDetail = {};
        var DDigipos: any; DDigipos = {};
        var noDatas = 0;
        for (var iData in Datas) {
          try {
            var infoData = Datas[iData];
            var today = new Date();
            var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
            var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
            var dateTime = date + ' ' + time;

            let InsertSumarry = await this.dataSumarryRepository.create({
              dealer_code: infoData.dealer_code,
              location: infoData.location,
              name: infoData.name,
              type_trx: infoData.type_trx,
              paid_in_amount: infoData.paid_in_amount,
              withdraw_amount: infoData.withdraw_amount,
              reason_type_id: infoData.reason_type_id,
              reason_type_name: infoData.reason_type_id,
              trx_a: infoData.trx_a,
              tot_mdr_a: infoData.tot_mdr_a,
              trx_b: infoData.trx_b,
              tot_mdr_b: infoData.tot_mdr_b,
              status: 0,
              periode: Periode,
              create_at: dateTime
            });

          } catch (err) {
            console.log(err);
          }
          noDatas++;
          if (noDatas == Datas.length) {

            const dataSumary = await this.dataSumarryRepository.execute("SELECT " +
              "summary.ad_code, " +
              "summary.location, " +
              "summary.name, " +
              "summary.type_trx, " +
              "summary.group_amount,  " +
              "sum(summary.trx_a) as trx_a_sum, " +
              "sum(summary.tot_mdr_a) as tot_mdr_a_sum, " +
              "sum(summary.trx_b) as trx_b_sum, " +
              "sum(summary.tot_mdr_b) as tot_mdr_b_sum " +
              "FROM " +
              "(SELECT " +
              "DISTINCT(DataSumarry.id), " +
              "AdCluster.ad_code, " +
              "DataSumarry.location, " +
              "DataSumarry.name, " +
              "DataSumarry.type_trx, " +
              "IF(DataSumarry.paid_in_amount = 0, DataSumarry.withdraw_amount , DataSumarry.paid_in_amount) as group_amount, " +
              "DataSumarry.trx_a, " +
              "DataSumarry.tot_mdr_a, " +
              "DataSumarry.trx_b, " +
              "DataSumarry.tot_mdr_b, " +
              "DataSumarry.status, " +
              "DataSumarry.periode, " +
              "DataSumarry.create_at " +
              "FROM " +
              "DataSumarry " +
              "INNER JOIN " +
              "AdCluster " +
              "on " +
              "DataSumarry.dealer_code = AdCluster.ad_cluster_id" +
              " WHERE " +
              "DataSumarry.periode = '" + Periode + "') as summary " +
              "GROUP BY summary.ad_code ,summary.type_trx,summary.group_amount ");

            const configData = await this.dataconfigRepository.find({where: {config_group_id: 1}});
            var configNum: any;
            configNum = {};
            for (var i in configData) {
              var infoConfig = configData[i];
              if (infoConfig.config_code == "0001-01") {
                configNum["persen_fee"] = infoConfig.config_value;
              }
              if (infoConfig.config_code == "0001-02") {
                configNum["persen_paid"] = infoConfig.config_value;
              }
              if (infoConfig.config_code == "0001-03") {
                configNum["num_dpp_fee"] = infoConfig.config_value;
              }
              if (infoConfig.config_code == "0001-04") {
                configNum["num_ppn_fee"] = infoConfig.config_value;
              }
              if (infoConfig.config_code == "0001-05") {
                configNum["num_pph_fee"] = infoConfig.config_value;
              }

              if (infoConfig.config_code == "0001-11") {
                configNum["num_dpp_paid"] = infoConfig.config_value;
              }
              if (infoConfig.config_code == "0001-12") {
                configNum["num_ppn_paid"] = infoConfig.config_value;
              }
              if (infoConfig.config_code == "0001-13") {
                configNum["num_pph_paid"] = infoConfig.config_value;
              }


              if (infoConfig.config_code == "0001-06") {
                configNum["persen_fee_ppob"] = infoConfig.config_value;
              }
              if (infoConfig.config_code == "0001-07") {
                configNum["persen_paid_ppob"] = infoConfig.config_value;
              }
              if (infoConfig.config_code == "0001-08") {
                configNum["num_dpp_ppob"] = infoConfig.config_value;
              }
              if (infoConfig.config_code == "0001-09") {
                configNum["num_ppn_ppob"] = infoConfig.config_value;
              }
              if (infoConfig.config_code == "0001-10") {
                configNum["num_pph_ppob"] = infoConfig.config_value;
              }
            }

            try {
              var InfoShare = await this.dataSharePercentRepository.findOne({where: {periode: Periode}});
              if (InfoShare == null) {
                await this.dataSharePercentRepository.create({
                  periode: Periode,
                  fee_ppob_share: parseFloat(configNum.persen_fee_ppob),
                  paid_ppob_share: parseFloat(configNum.persen_paid_ppob),
                  dpp_ppob_share: parseFloat(configNum.num_dpp_ppob),
                  ppn_ppob_share: parseFloat(configNum.num_ppn_ppob),
                  pph_ppob_share: parseFloat(configNum.num_pph_ppob),

                  fee_recharce_share: parseFloat(configNum.persen_fee),
                  paid_recharge_share: parseFloat(configNum.persen_paid),
                  dpp_recharge_share_fee: parseFloat(configNum.num_dpp_fee),
                  ppn_recharge_share_fee: parseFloat(configNum.num_ppn_fee),
                  pph_recharge_share_fee: parseFloat(configNum.num_pph_fee),

                  dpp_recharge_share_paid: parseFloat(configNum.num_dpp_paid),
                  ppn_recharge_share_paid: parseFloat(configNum.num_ppn_paid),
                  pph_recharge_share_paid: parseFloat(configNum.num_pph_paid)
                });
              } else {
                await this.dataSharePercentRepository.updateById(InfoShare.id, {
                  periode: Periode,
                  fee_ppob_share: parseFloat(configNum.persen_fee_ppob),
                  paid_ppob_share: parseFloat(configNum.persen_paid_ppob),
                  dpp_ppob_share: parseFloat(configNum.num_dpp_ppob),
                  ppn_ppob_share: parseFloat(configNum.num_ppn_ppob),
                  pph_ppob_share: parseFloat(configNum.num_pph_ppob),

                  fee_recharce_share: parseFloat(configNum.persen_fee),
                  paid_recharge_share: parseFloat(configNum.persen_paid),

                  dpp_recharge_share_fee: parseFloat(configNum.num_dpp_fee),
                  ppn_recharge_share_fee: parseFloat(configNum.num_ppn_fee),
                  pph_recharge_share_fee: parseFloat(configNum.num_pph_fee),

                  dpp_recharge_share_paid: parseFloat(configNum.num_dpp_paid),
                  ppn_recharge_share_paid: parseFloat(configNum.num_ppn_paid),
                  pph_recharge_share_paid: parseFloat(configNum.num_pph_paid)

                })
              }
            } catch (error) {

            }


            var processInsertDigipostDet = 0;
            for (var i in dataSumary) {
              var persenFee = configNum.persen_fee;
              var persenPaid = configNum.persen_paid;

              var dppSetFee = configNum.num_dpp_fee;
              var ppnSetFee = configNum.num_ppn_fee;
              var pphSetFee = configNum.num_pph_fee;

              var dppSetPaid = configNum.num_dpp_paid;
              var ppnSetPaid = configNum.num_ppn_paid;
              var pphSetPaid = configNum.num_pph_paid;


              var infoAr = dataSumary[i];
              if (infoAr.type_trx.toUpperCase() == "PPOB") {
                var persenFee = configNum.persen_fee_ppob;
                var persenPaid = configNum.persen_paid_ppob;
                dppSetFee = configNum.num_dpp_ppob;
                ppnSetFee = configNum.num_ppn_ppob;
                pphSetFee = configNum.num_pph_ppob;
              }

              var trxFee = ((infoAr.trx_a_sum * infoAr.group_amount) * (persenFee));
              var feeDPP = (trxFee / dppSetFee);
              var feePPN = (feeDPP * ppnSetFee);
              var feeTotal = feeDPP + feePPN;
              var feePPH = (feeDPP * pphSetFee);

              var trxPaid = ((infoAr.trx_b_sum * infoAr.group_amount) * (persenPaid));
              var paidDPP = (trxPaid / dppSetPaid);
              var paidPPN = (paidDPP * ppnSetPaid);
              var paidTotal = paidDPP + paidPPN;
              var paidPPH = (paidDPP * pphSetPaid);

              try {
                var InsertDataDigiposDetail = await this.dataDigiposDetailRepository.create({
                  trx_date_group: Periode,
                  ad_code: infoAr.ad_code,
                  ad_name: infoAr.name,
                  type_trx: infoAr.type_trx,
                  sales_fee: infoAr.group_amount,
                  trx_cluster_out: infoAr.trx_a_sum,
                  mdr_fee_amount: trxFee,
                  dpp_fee_amount: feeDPP,
                  vat_fee_amount: feePPN,
                  total_fee_amount: feeTotal,
                  pph_fee_amount: feePPH,
                  trx_paid_invoice: infoAr.trx_b_sum,
                  paid_inv_amount: trxPaid,
                  inv_adj_amount: 0,
                  dpp_paid_amount: paidDPP,
                  vat_paid_amount: paidPPN,
                  total_paid_amount: paidTotal,
                  pph_paid_amount: paidPPH,
                });
              } catch (err) {
                console.log(err);
              }
              processInsertDigipostDet++;
              if (processInsertDigipostDet == dataSumary.length) {
                const dataDigipos = await this.dataDigiposDetailRepository.execute(
                  "SELECT " +
                  "ad_code, " +
                  "ad_name, " +
                  "trx_date_group, " +
                  "GROUP_CONCAT(digipos_detail_id SEPARATOR ',' ) as StoreId,   " +
                  "COUNT(DISTINCT ad_code) as CountAD, " +
                  "SUM(trx_cluster_out) SumTrxFee,  " +
                  "SUM(mdr_fee_amount) SumFee, " +
                  "SUM(dpp_fee_amount) SumDppFee,  " +
                  "SUM(vat_fee_amount) SumVatFee, " +
                  "SUM(total_fee_amount) SumTotalFee, " +
                  "SUM(pph_fee_amount) SumPphFee, " +

                  "SUM(trx_paid_invoice) SumTrxPaid,  " +
                  "SUM(paid_inv_amount) SumPaid, " +
                  "SUM(inv_adj_amount) SumInvAdjAmount, " +
                  "SUM(dpp_paid_amount) SumDppPaid, " +
                  "SUM(vat_paid_amount) SumVatPaid, " +
                  "SUM(total_paid_amount) SumTotalPaid, " +
                  "SUM(pph_paid_amount) SumPphPaid " +
                  "FROM " +
                  "DataDigiposDetail WHERE trx_date_group = '" + Periode + "' GROUP BY ad_code,trx_date_group "
                );

                var ProcessDigipos = 0;
                for (var iDigipos in dataDigipos) {
                  try {
                    var infoDataDigipos = dataDigipos[iDigipos];
                    var InserDigiposDetail = await this.dataDigiposRepository.create({
                      trx_date_group: Periode,
                      ad_code: infoDataDigipos.ad_code,
                      ad_name: infoDataDigipos.ad_name,
                      inv_number: "",
                      inv_attacment: "",
                      ba_number_a: "",
                      ba_attachment_a: "",
                      ba_number_b: "",
                      ba_attachment_b: "",
                      vat_number: "",
                      vat_attachment: "",
                      inv_desc: "",
                      sales_fee: 0,
                      trx_cluster_out: infoDataDigipos.SumTrxFee,
                      mdr_fee_amount: infoDataDigipos.SumFee,
                      dpp_fee_amount: infoDataDigipos.SumDppFee,
                      vat_fee_amount: infoDataDigipos.SumVatFee,
                      total_fee_amount: infoDataDigipos.SumTotalFee,
                      pph_fee_amount: infoDataDigipos.SumPphFee,
                      trx_paid_invoice: infoDataDigipos.SumTrxPaid,
                      paid_inv_amount: infoDataDigipos.SumPaid,
                      inv_adj_amount: infoDataDigipos.SumInvAdjAmount,
                      dpp_paid_amount: infoDataDigipos.SumDppPaid,
                      vat_paid_amount: infoDataDigipos.SumVatPaid,
                      total_paid_amount: infoDataDigipos.SumTotalPaid,
                      pph_paid_amount: infoDataDigipos.SumPphPaid,
                      remark: "-",
                      ar_stat: 0,
                      response_stat: true
                    });
                    var findDigipos = await this.dataDigiposRepository.findOne({where: {and: [{trx_date_group: Periode}, {ad_code: infoDataDigipos.ad_code}]}});
                    if (findDigipos?.digipost_id == undefined) {

                    } else {
                      var StoreDetID = infoDataDigipos.StoreId.split(",");
                      for (var iDet in StoreDetID) {
                        try {
                          var det_id = StoreDetID[iDet];
                          var updateDetail = await this.dataDigiposDetailRepository.updateById(det_id, {
                            digipost_id: findDigipos?.digipost_id
                          });
                        } catch (err) {
                          console.log(err);
                        }
                      }
                    }
                  } catch (err) {
                    console.log(err);
                  }
                  ProcessDigipos++;
                }
              }
            }
            // console.log(configNum);
            // console.log(dataSumary);
            console.log("FINISH");
            delete SycProcess[Periode];
          }
        }

        if (Datas.length == 0) {
          delete SycProcess[Periode];
        }

        var today = new Date();
        var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        var dateTime = date + ' ' + time;

        var dataDigiposJmlAd = await this.dataDigiposRepository.execute("SELECT trx_date_group , type_trx, SUM(mdr_fee_amount) MDR_A, SUM(paid_inv_amount) MDR_B, COUNT(DISTINCT(ad_code)) ImportAD FROM DataDigiposDetail WHERE trx_date_group = '" + infoFileSyc.periode + "' GROUP BY type_trx, trx_date_group");
        var rawInfo: any; rawInfo = {ImportAD: 0, ShareRechargeA: 0, ShareRechargeB: 0, SharePpobA: 0, SharePpobB: 0}
        for (var i in dataDigiposJmlAd) {
          var rawDigipos = dataDigiposJmlAd[i];
          rawInfo.ImportAD = rawDigipos.ImportAD;
          if (rawDigipos.type_trx.toUpperCase() == "RECHARGE") {
            rawInfo.ShareRechargeA = rawDigipos.MDR_A;
            rawInfo.ShareRechargeB = rawDigipos.MDR_B;
          }
          if (rawDigipos.type_trx.toUpperCase() == "PPOB") {
            rawInfo.SharePpobA = rawDigipos.MDR_A;
            rawInfo.SharePpobB = rawDigipos.MDR_B;
          }
        }

        var updateProcess = await this.fileSycronizeRepository.updateById(infoFileSyc.file_syc_id, {
          error_state: false,
          code_state: 1,
          end_at: dateTime,
          mitra_ad: rawInfo.ImportAD,
          total_fee: (rawInfo.ShareRechargeA + rawInfo.SharePpobA),
          total_paid: (rawInfo.ShareRechargeB + rawInfo.SharePpobB)
        });

        // var updateProcess = await this.fileSycronizeRepository.updateById(infoFileSyc.file_syc_id, {
        //   error_state: false,
        //   code_state: 1,
        //   end_at: dateTime
        // });

      }
    }
  }

  SycFileExcel = async () => {

    var fileSyc = await this.fileSycronizeRepository.find();
    for (var i in fileSyc) {

      var infoFileSyc = fileSyc[i];

      if (infoFileSyc.error_state == false && infoFileSyc.code_state == 0) {
        var Periode = infoFileSyc.periode;
        if (SycProcess[Periode] == undefined) {
          SycProcess[Periode] = new Promise((resolve, reject) => {
            var d = new Date();
            var date = d.getDate();
            var month = d.getMonth() + 1;
            var year = d.getFullYear();
            var filePath = path.join(__dirname, '../../.filesyc/' + infoFileSyc.file_name);
            var MapingData: any;
            MapingData = {
              "dealer_code": "dealer_code",
              "location": "location",
              "name": "name",
              "paid_in_amount": "paid_in_amount",
              "withdraw_amount": "withdraw_amount",
              "reason_type_id": "reason_type_id",
              "reason_type_name": "reason_type_name",
              "type": "type",
              "trx_a": "trx_a",
              "tot_mdr_a": "tot_mdr_a",
              "trx_b": "trx_b",
              "tot_mdr_b": "tot_mdr_b"
              // "trx_a_ppob": "trx_a_ppob",
              // "tot_mdr_a_ppob": "tot_mdr_a_ppob",
              // "trx_b_ppob": "trx_b_ppob",
              // "tot_mdr_b_ppob": "tot_mdr_b_ppob",
            }
            error = new Array();
            var DataStore: any; DataStore = new Array();
            let no = 0;
            fs.createReadStream(filePath).pipe(csv()).on('data', async (row: any) => {
              var Datas: any;
              Datas = {
                "dealer_code": "",
                "location": "",
                "name": "",
                "paid_in_amount": "",
                "withdraw_amount": "",
                "reason_type_id": "",
                "reason_type_name": "",
                "trx_a": "",
                "tot_mdr_a": "",
                "trx_b": "",
                "tot_mdr_b": "",
                "status": 0
              };
              var errorField: any;
              errorField = {Error: {}, message: new Array()};
              var thisError = 0;
              var DataCSV: any; DataCSV = {};
              for (var i in row) {
                if (MapingData[i.trim()] == undefined) {
                  errorField.message.push("Field " + i.trim() + " not exities in file csv periode " + infoFileSyc.periode);
                  errorField["Error"] = row;
                  thisError = 1;
                } else {
                  DataCSV[MapingData[i.trim()]] = row[i];
                }
              }

              no = no + 1;
              if (thisError == 1) {
                error.push(errorField);
              } else {
                //RECHARGE
                Datas = {};
                Datas["id"] = null;
                Datas["status"] = thisError;
                Datas["runnig_date"] = year + "-" + month + "-" + date;
                Datas["runnig_group"] = infoFileSyc.periode;
                Datas["dealer_code"] = DataCSV["dealer_code"];
                Datas["location"] = DataCSV["location"];
                Datas["name"] = DataCSV["name"];
                Datas["type_trx"] = DataCSV["type"];
                Datas["paid_in_amount"] = DataCSV["paid_in_amount"];
                Datas["withdraw_amount"] = DataCSV["withdraw_amount"];
                Datas["reason_type_id"] = DataCSV["reason_type_id"];
                Datas["reason_type_name"] = DataCSV["reason_type_name"];
                Datas["trx_a"] = DataCSV["trx_a"] == "" ? 0 : DataCSV["trx_a"];
                Datas["tot_mdr_a"] = DataCSV["tot_mdr_a"] == "" ? 0 : DataCSV["tot_mdr_a"];
                Datas["trx_b"] = DataCSV["trx_b"] == "" ? 0 : DataCSV["trx_b"];
                Datas["tot_mdr_b"] = DataCSV["tot_mdr_b"] == "" ? 0 : DataCSV["tot_mdr_b"];
                Datas["status"] = 0
                DataStore.push(Datas);

                // //PPOB
                // Datas = {};
                // Datas["id"] = null;
                // Datas["status"] = thisError;
                // Datas["runnig_date"] = year + "-" + month + "-" + date;
                // Datas["runnig_group"] = infoFileSyc.periode;
                // Datas["dealer_code"] = DataCSV["dealer_code"];
                // Datas["location"] = DataCSV["location"];
                // Datas["name"] = DataCSV["name"];
                // Datas["type_trx"] = DataCSV["type"];
                // Datas["paid_in_amount"] = DataCSV["paid_in_amount"];
                // Datas["withdraw_amount"] = DataCSV["withdraw_amount"];
                // Datas["reason_type_id"] = DataCSV["reason_type_id"];
                // Datas["reason_type_name"] = DataCSV["reason_type_name"];
                // Datas["trx_a"] = DataCSV["trx_a_ppob"] == "" ? 0 : DataCSV["trx_a_ppob"];
                // Datas["tot_mdr_a"] = DataCSV["tot_mdr_a_ppob"] == "" ? 0 : DataCSV["tot_mdr_a_ppob"];
                // Datas["trx_b"] = DataCSV["trx_b_ppob"] == "" ? 0 : DataCSV["trx_b_ppob"];
                // Datas["tot_mdr_b"] = DataCSV["tot_mdr_b_ppob"] == "" ? 0 : DataCSV["tot_mdr_b_ppob"];
                // Datas["status"] = 0
                // DataStore.push(Datas);
              }

            }).on('end', async () => {
              // console.log(DataStore.length);
              // console.log(error);
              if (error.length > 0) {

                for (var iError in error) {

                  var InfoErr = error[iError];
                  var today = new Date();
                  var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
                  var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
                  var dateTime = date + ' ' + time;

                  var InsertErrorLog = await this.errorLogRepository.create({
                    create_at: dateTime,
                    error_log_message: InfoErr.message,
                    error_log_info: JSON.stringify(InfoErr.Error),
                    error_log_type: "MIGRATION_DATA",
                    error_log_source: infoFileSyc.file_syc_id
                  });

                }

                var today = new Date();
                var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
                var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
                var dateTime = date + ' ' + time;
                var updateProcess = await this.fileSycronizeRepository.updateById(infoFileSyc.file_syc_id, {
                  error_state: true,
                  error_message: "Error File CSV Field !!",
                  code_state: 1,
                  end_at: dateTime
                });

              } else {
                ProcessValidate[Periode] = {index: 0, data: DataStore, error: error};
                this.SycFileValidate(infoFileSyc, Periode);
              }

              // var errorNow = [];
              // while (processState < infoFileSyc.process_total) {
              //   errorNow = error;
              // }
              // delete SycProcess[Periode];
              // console.log("FINISH");
              // if (error.length > 0) {
              //   var updateProcess = await this.fileSycronizeRepository.updateById(infoFileSyc.file_syc_id, {
              //     error_state: true,
              //     code_state: 2
              //   });
              // } else {
              //   var updateProcess = await this.fileSycronizeRepository.updateById(infoFileSyc.file_syc_id, {
              //     error_state: false,
              //     code_state: 1
              //   });
              //   for (var iProcessInsert in DataStore) {

              //   }
              // }

            });

          });
        }
      }

    }
  }


  OnRunningDataSummry = async (syc_id: number, periode: string, DataMigrasi: any) => {
    for (var dealer_code in DataMigrasi) {
      var rawDataMirasi = DataMigrasi[dealer_code];
      var rawSummary = await this.dataSumarryRepository.findOne({where: {and: [{dealer_code: rawDataMirasi.dealer_code}, {periode: periode}, {type_trx: rawDataMirasi.type_trx}]}});
      if (rawSummary == null) {
        //CREATE
        try {
          await this.dataSumarryRepository.create(rawDataMirasi);
        } catch (error) {
          console.log(error);
        }
      } else {
        //UPDATE
        try {
          await this.dataSumarryRepository.updateById(rawSummary.id, rawDataMirasi);
        } catch (error) {
          console.log(error);
        }
      }
    }
    //RUNNING DATA DIGIPOS
    const dataSumary = await this.dataSumarryRepository.execute("SELECT " +
      "summary.ad_code, " +
      "summary.location, " +
      "summary.name, " +
      "summary.type_trx, " +
      "summary.group_amount,  " +
      "sum(summary.trx_a) as trx_a_sum, " +
      "sum(summary.tot_mdr_a) as tot_mdr_a_sum, " +
      "sum(summary.trx_b) as trx_b_sum, " +
      "sum(summary.tot_mdr_b) as tot_mdr_b_sum " +
      "FROM " +
      "(SELECT " +
      "DISTINCT(DataSumarry.id), " +
      "AdCluster.ad_code, " +
      "DataSumarry.location, " +
      "DataSumarry.name, " +
      "DataSumarry.type_trx, " +
      "IF(DataSumarry.paid_in_amount = 0, DataSumarry.withdraw_amount , DataSumarry.paid_in_amount) as group_amount, " +
      "DataSumarry.trx_a, " +
      "DataSumarry.tot_mdr_a, " +
      "DataSumarry.trx_b, " +
      "DataSumarry.tot_mdr_b, " +
      "DataSumarry.status, " +
      "DataSumarry.periode, " +
      "DataSumarry.create_at " +
      "FROM " +
      "DataSumarry " +
      "INNER JOIN " +
      "AdCluster " +
      "on " +
      "DataSumarry.dealer_code = AdCluster.ad_cluster_id" +
      " WHERE " +
      "DataSumarry.periode = '" + periode + "') as summary " +
      "GROUP BY summary.ad_code ,summary.type_trx,summary.group_amount ");

    const configData = await this.dataconfigRepository.find({where: {config_group_id: 1}});
    var configNum: any;
    configNum = {};
    for (var i in configData) {
      var infoConfig = configData[i];
      if (infoConfig.config_code == "0001-01") {
        configNum["persen_fee"] = infoConfig.config_value;
      }
      if (infoConfig.config_code == "0001-02") {
        configNum["persen_paid"] = infoConfig.config_value;
      }
      if (infoConfig.config_code == "0001-03") {
        configNum["num_dpp_fee"] = infoConfig.config_value;
      }
      if (infoConfig.config_code == "0001-04") {
        configNum["num_ppn_fee"] = infoConfig.config_value;
      }
      if (infoConfig.config_code == "0001-05") {
        configNum["num_pph_fee"] = infoConfig.config_value;
      }

      if (infoConfig.config_code == "0001-11") {
        configNum["num_dpp_paid"] = infoConfig.config_value;
      }
      if (infoConfig.config_code == "0001-12") {
        configNum["num_ppn_paid"] = infoConfig.config_value;
      }
      if (infoConfig.config_code == "0001-13") {
        configNum["num_pph_paid"] = infoConfig.config_value;
      }

      if (infoConfig.config_code == "0001-06") {
        configNum["persen_fee_ppob"] = infoConfig.config_value;
      }
      if (infoConfig.config_code == "0001-07") {
        configNum["persen_paid_ppob"] = infoConfig.config_value;
      }
      if (infoConfig.config_code == "0001-08") {
        configNum["num_dpp_ppob"] = infoConfig.config_value;
      }
      if (infoConfig.config_code == "0001-09") {
        configNum["num_ppn_ppob"] = infoConfig.config_value;
      }
      if (infoConfig.config_code == "0001-10") {
        configNum["num_pph_ppob"] = infoConfig.config_value;
      }
    }

    try {
      var InfoShare = await this.dataSharePercentRepository.findOne({where: {periode: periode}});
      if (InfoShare == null) {
        await this.dataSharePercentRepository.create({
          periode: periode,
          fee_ppob_share: parseFloat(configNum.persen_fee_ppob),
          paid_ppob_share: parseFloat(configNum.persen_paid_ppob),
          dpp_ppob_share: parseFloat(configNum.num_dpp_ppob),
          ppn_ppob_share: parseFloat(configNum.num_ppn_ppob),
          pph_ppob_share: parseFloat(configNum.num_pph_ppob),

          fee_recharce_share: parseFloat(configNum.persen_fee),
          paid_recharge_share: parseFloat(configNum.persen_paid),

          dpp_recharge_share_fee: parseFloat(configNum.num_dpp_fee),
          ppn_recharge_share_fee: parseFloat(configNum.num_ppn_fee),
          pph_recharge_share_fee: parseFloat(configNum.num_pph_fee),

          dpp_recharge_share_paid: parseFloat(configNum.num_dpp_paid),
          ppn_recharge_share_paid: parseFloat(configNum.num_ppn_paid),
          pph_recharge_share_paid: parseFloat(configNum.num_pph_paid)


        });
      } else {
        await this.dataSharePercentRepository.updateById(InfoShare.id, {
          periode: periode,
          fee_ppob_share: parseFloat(configNum.persen_fee_ppob),
          paid_ppob_share: parseFloat(configNum.persen_paid_ppob),
          dpp_ppob_share: parseFloat(configNum.num_dpp_ppob),
          ppn_ppob_share: parseFloat(configNum.num_ppn_ppob),
          pph_ppob_share: parseFloat(configNum.num_pph_ppob),

          fee_recharce_share: parseFloat(configNum.persen_fee),
          paid_recharge_share: parseFloat(configNum.persen_paid),

          dpp_recharge_share_fee: parseFloat(configNum.num_dpp_fee),
          ppn_recharge_share_fee: parseFloat(configNum.num_ppn_fee),
          pph_recharge_share_fee: parseFloat(configNum.num_pph_fee),

          dpp_recharge_share_paid: parseFloat(configNum.num_dpp_paid),
          ppn_recharge_share_paid: parseFloat(configNum.num_ppn_paid),
          pph_recharge_share_paid: parseFloat(configNum.num_pph_paid)

        })
      }
    } catch (error) {

    }


    var processInsertDigipostDet = 0;
    for (var i in dataSumary) {
      var persenFee = configNum.persen_fee;
      var persenPaid = configNum.persen_paid;

      var dppSetFee = configNum.num_dpp_fee;
      var ppnSetFee = configNum.num_ppn_fee;
      var pphSetFee = configNum.num_pph_fee;

      var dppSetPaid = configNum.num_dpp_paid;
      var ppnSetPaid = configNum.num_ppn_paid;
      var pphSetPaid = configNum.num_pph_paid;


      var infoAr = dataSumary[i];
      if (infoAr.type_trx.toUpperCase() == "PPOB") {
        var persenFee = configNum.persen_fee_ppob;
        var persenPaid = configNum.persen_paid_ppob;

        dppSetFee = configNum.num_dpp_ppob;
        ppnSetFee = configNum.num_ppn_ppob;
        pphSetFee = configNum.num_pph_ppob;
      }

      var trxFee = ((infoAr.trx_a_sum * infoAr.group_amount) * (persenFee));
      var feeDPP = (trxFee / dppSetFee);
      var feePPN = (feeDPP * ppnSetFee);
      var feeTotal = feeDPP + feePPN;
      var feePPH = (feeDPP * pphSetFee);

      var trxPaid = ((infoAr.trx_b_sum * infoAr.group_amount) * (persenPaid));
      var paidDPP = (trxPaid / dppSetPaid);
      var paidPPN = (paidDPP * ppnSetPaid);
      var paidTotal = paidDPP + paidPPN;
      var paidPPH = (paidDPP * pphSetPaid);

      try {
        var findDigiposDet = await this.dataDigiposDetailRepository.findOne({where: {and: [{trx_date_group: periode}, {ad_code: infoAr.ad_code}, {type_trx: infoAr.type_trx}]}});
        if (findDigiposDet == null) {
          var InsertDataDigiposDetail = await this.dataDigiposDetailRepository.create({
            trx_date_group: periode,
            ad_code: infoAr.ad_code,
            ad_name: infoAr.name,
            type_trx: infoAr.type_trx,
            sales_fee: infoAr.group_amount,
            trx_cluster_out: infoAr.trx_a_sum,
            mdr_fee_amount: trxFee,
            dpp_fee_amount: feeDPP,
            vat_fee_amount: feePPN,
            total_fee_amount: feeTotal,
            pph_fee_amount: feePPH,
            trx_paid_invoice: infoAr.trx_b_sum,
            paid_inv_amount: trxPaid,
            inv_adj_amount: 0,
            dpp_paid_amount: paidDPP,
            vat_paid_amount: paidPPN,
            total_paid_amount: paidTotal,
            pph_paid_amount: paidPPH,
          });
        } else {
          var UpdateDataDigiposDetail = await this.dataDigiposDetailRepository.updateById(findDigiposDet.digipos_detail_id, {
            trx_date_group: periode,
            ad_code: infoAr.ad_code,
            ad_name: infoAr.name,
            type_trx: infoAr.type_trx,
            sales_fee: infoAr.group_amount,
            trx_cluster_out: infoAr.trx_a_sum,
            mdr_fee_amount: trxFee,
            dpp_fee_amount: feeDPP,
            vat_fee_amount: feePPN,
            total_fee_amount: feeTotal,
            pph_fee_amount: feePPH,
            trx_paid_invoice: infoAr.trx_b_sum,
            paid_inv_amount: trxPaid,
            inv_adj_amount: 0,
            dpp_paid_amount: paidDPP,
            vat_paid_amount: paidPPN,
            total_paid_amount: paidTotal,
            pph_paid_amount: paidPPH,
          });
        }
      } catch (err) {
        console.log(err);
      }
      processInsertDigipostDet++;
      if (processInsertDigipostDet == dataSumary.length) {
        const dataDigipos = await this.dataDigiposDetailRepository.execute(
          "SELECT " +
          "ad_code, " +
          "ad_name, " +
          "trx_date_group, " +
          "GROUP_CONCAT(digipos_detail_id SEPARATOR ',' ) as StoreId,   " +
          "COUNT(DISTINCT ad_code) as CountAD, " +
          "SUM(trx_cluster_out) SumTrxFee,  " +
          "SUM(mdr_fee_amount) SumFee, " +
          "SUM(dpp_fee_amount) SumDppFee,  " +
          "SUM(vat_fee_amount) SumVatFee, " +
          "SUM(total_fee_amount) SumTotalFee, " +
          "SUM(pph_fee_amount) SumPphFee, " +

          "SUM(trx_paid_invoice) SumTrxPaid,  " +
          "SUM(paid_inv_amount) SumPaid, " +
          "SUM(inv_adj_amount) SumInvAdjAmount, " +
          "SUM(dpp_paid_amount) SumDppPaid, " +
          "SUM(vat_paid_amount) SumVatPaid, " +
          "SUM(total_paid_amount) SumTotalPaid, " +
          "SUM(pph_paid_amount) SumPphPaid " +
          "FROM " +
          "DataDigiposDetail WHERE trx_date_group = '" + periode + "' GROUP BY ad_code,trx_date_group "
        );

        var ProcessDigipos = 0;
        for (var iDigipos in dataDigipos) {
          try {
            var infoDataDigipos = dataDigipos[iDigipos];
            var findDigipos = await this.dataDigiposRepository.findOne({where: {and: [{trx_date_group: periode}, {ad_code: infoDataDigipos.ad_code}]}});
            if (findDigipos == null) {
              var InserDigiposDetail = await this.dataDigiposRepository.create({
                trx_date_group: periode,
                ad_code: infoDataDigipos.ad_code,
                ad_name: infoDataDigipos.ad_name,
                inv_number: "",
                inv_attacment: "",
                ba_number_a: "",
                ba_attachment_a: "",
                ba_number_b: "",
                ba_attachment_b: "",
                vat_number: "",
                vat_attachment: "",
                inv_desc: "",
                sales_fee: 0,
                trx_cluster_out: infoDataDigipos.SumTrxFee,
                mdr_fee_amount: infoDataDigipos.SumFee,
                dpp_fee_amount: infoDataDigipos.SumDppFee,
                vat_fee_amount: infoDataDigipos.SumVatFee,
                total_fee_amount: infoDataDigipos.SumTotalFee,
                pph_fee_amount: infoDataDigipos.SumPphFee,
                trx_paid_invoice: infoDataDigipos.SumTrxPaid,
                paid_inv_amount: infoDataDigipos.SumPaid,
                inv_adj_amount: infoDataDigipos.SumInvAdjAmount,
                dpp_paid_amount: infoDataDigipos.SumDppPaid,
                vat_paid_amount: infoDataDigipos.SumVatPaid,
                total_paid_amount: infoDataDigipos.SumTotalPaid,
                pph_paid_amount: infoDataDigipos.SumPphPaid,
                remark: "-",
                ar_stat: 0,
                ar_valid_a: 0,
                ar_valid_b: 0,
                response_stat: true,
                generate_file_a: 0,
                generate_file_b: 0,
                google_drive_a: 0,
                google_drive_b: 0,
                sign_off_a: 0,
                sign_off_b: 0,
                acc_file_ba_a: 0,
                acc_file_ba_b: 0,
                acc_file_invoice: 0,
                generate_file_invoice: 0,
                distribute_file_invoice: 0,
                vat_distribute: 0,
                at_vat_distibute: "",
                attach_invoice_ttd: "",
                distribute_invoice_ttd: 0,
                at_distribute_invoice_ttd: "",
                acc_recv: 0,
                flag_generate_csv: 0,
                at_generate_csv: ""
              });
            } else {
              var UpdateDigiposDetail = await this.dataDigiposRepository.updateById(findDigipos.digipost_id, {
                trx_date_group: periode,
                ad_code: infoDataDigipos.ad_code,
                ad_name: infoDataDigipos.ad_name,
                inv_number: "",
                inv_attacment: "",
                ba_number_a: "",
                ba_attachment_a: "",
                ba_number_b: "",
                ba_attachment_b: "",
                vat_number: "",
                vat_attachment: "",
                inv_desc: "",
                sales_fee: 0,
                trx_cluster_out: infoDataDigipos.SumTrxFee,
                mdr_fee_amount: infoDataDigipos.SumFee,
                dpp_fee_amount: infoDataDigipos.SumDppFee,
                vat_fee_amount: infoDataDigipos.SumVatFee,
                total_fee_amount: infoDataDigipos.SumTotalFee,
                pph_fee_amount: infoDataDigipos.SumPphFee,
                trx_paid_invoice: infoDataDigipos.SumTrxPaid,
                paid_inv_amount: infoDataDigipos.SumPaid,
                inv_adj_amount: infoDataDigipos.SumInvAdjAmount,
                dpp_paid_amount: infoDataDigipos.SumDppPaid,
                vat_paid_amount: infoDataDigipos.SumVatPaid,
                total_paid_amount: infoDataDigipos.SumTotalPaid,
                pph_paid_amount: infoDataDigipos.SumPphPaid,
                remark: "-",
                ar_stat: 0,
                ar_valid_a: 0,
                ar_valid_b: 0,
                response_stat: true,
                generate_file_a: 0,
                generate_file_b: 0,
                google_drive_a: 0,
                google_drive_b: 0,
                sign_off_a: 0,
                sign_off_b: 0,
                acc_file_ba_a: 0,
                acc_file_ba_b: 0,
                acc_file_invoice: 0,
                generate_file_invoice: 0,
                distribute_file_invoice: 0,
                vat_distribute: 0,
                at_vat_distibute: "",
                attach_invoice_ttd: "",
                distribute_invoice_ttd: 0,
                at_distribute_invoice_ttd: "",
                acc_recv: 0,
                flag_generate_csv: 0,
                at_generate_csv: ""
              });
            }

            if (findDigipos?.digipost_id == undefined) {

            } else {
              var StoreDetID = infoDataDigipos.StoreId.split(",");
              for (var iDet in StoreDetID) {
                try {
                  var det_id = StoreDetID[iDet];
                  var updateDetail = await this.dataDigiposDetailRepository.updateById(det_id, {
                    digipost_id: findDigipos?.digipost_id
                  });
                } catch (err) {
                  console.log(err);
                }
              }
            }
          } catch (err) {
            console.log(err);
          }
          ProcessDigipos++;
        }
      }
    }
  }

  OnRunningDataRecharge = async (syc_id: number, periode: string) => {
    //Data Summary Database
    var dataSyc = await this.fileSycronizeRepository.findById(syc_id);
    var DeletePPOB = await this.dataRechargeDetailRepository.deleteAll({periode: periode});


    var DealerCodeNotExities: any; DealerCodeNotExities = [];

    //Data Summary Excel
    var Data = excelProcessProgres.RECHARGE.Datas;
    var AD: any; AD = [];
    var MitraAd: any; MitraAd = [];
    var no = 1;
    var errorState = false;
    var total_fee = dataSyc.total_fee == undefined ? 0 : dataSyc.total_fee;
    var total_paid = dataSyc.total_paid == undefined ? 0 : dataSyc.total_paid;

    var DatasMigrasi: any; DatasMigrasi = {};

    excelProcessProgres.RECHARGE.MaxProcess = Data.length;
    for (var i in Data) {
      var rawRechargeExcel = Data[i];
      const rawCluster = await this.adClusterRepository.findOne({where: {ad_cluster_id: rawRechargeExcel.dealer_code}});
      if (rawCluster == null) {
        if (DealerCodeNotExities.indexOf(rawRechargeExcel.dealer_code) == -1) {
          await this.errorLogRepository.create({
            create_at: ControllerConfig.onCurrentTime().toString(),
            error_log_message: "Error Dealer Code Not Exities !",
            error_log_info: "Dealer Code " + rawRechargeExcel.dealer_code + " Not Exities.",
            error_log_type: "DEALER_CODE_NOT_EXITIES",
            error_log_source: syc_id
          });
          DealerCodeNotExities.push(rawRechargeExcel.dealer_code);
        }
        errorState = true;
      } else {
        try {
          const rawCluster = await this.adClusterRepository.findById(rawRechargeExcel.dealer_code);
          if (AD.indexOf(rawCluster.ad_code) == -1) {
            AD.push(rawCluster.ad_code);
          }
          if (MitraAd.indexOf(rawRechargeExcel.dealer_code) == -1) {
            MitraAd.push(rawRechargeExcel.dealer_code);
          }

          if (DatasMigrasi[rawRechargeExcel.dealer_code] == undefined) {
            DatasMigrasi[rawRechargeExcel.dealer_code] = {
              dealer_code: rawRechargeExcel.dealer_code,
              location: rawRechargeExcel.location,
              name: rawCluster.ad_name,
              paid_in_amount: 0,
              withdraw_amount: 0,
              reason_type_id: rawRechargeExcel.reason_type_id,
              reason_type_name: rawRechargeExcel.reason_type_name,
              trx_a: 0,
              tot_mdr_a: 0,
              trx_b: 0,
              tot_mdr_b: 0,
              status: 0,
              periode: periode,
              create_at: ControllerConfig.onCurrentTime().toString(),
              type_trx: "RECHARGE",
              code_ad: rawCluster.ad_code
            }
          }

          DatasMigrasi[rawRechargeExcel.dealer_code]["paid_in_amount"] = parseInt(rawRechargeExcel.paid_in_amount);
          DatasMigrasi[rawRechargeExcel.dealer_code]["withdraw_amount"] = parseInt(rawRechargeExcel.withdraw_amount);
          DatasMigrasi[rawRechargeExcel.dealer_code]["trx_a"] += parseInt(rawRechargeExcel.trx_a);
          DatasMigrasi[rawRechargeExcel.dealer_code]["tot_mdr_a"] += parseInt(rawRechargeExcel.tot_mdr_a);
          DatasMigrasi[rawRechargeExcel.dealer_code]["trx_b"] += parseInt(rawRechargeExcel.trx_b);
          DatasMigrasi[rawRechargeExcel.dealer_code]["tot_mdr_b"] += parseInt(rawRechargeExcel.tot_mdr_b);
          DatasMigrasi[rawRechargeExcel.dealer_code]["create_at"] = ControllerConfig.onCurrentTime().toString();


          await this.dataRechargeDetailRepository.create({
            periode: periode,
            dealer_code: rawRechargeExcel.dealer_code,
            location: rawRechargeExcel.location,
            name: rawRechargeExcel.name,
            type_trx: rawRechargeExcel.type,
            paid_in_amount: rawRechargeExcel.paid_in_amount,
            withdraw_amount: rawRechargeExcel.withdraw_amount,
            reason_type_id: rawRechargeExcel.reason_type_id,
            reason_type_name: rawRechargeExcel.reason_type_name,
            trx_a: rawRechargeExcel.trx_a,
            tot_mdr_a: rawRechargeExcel.tot_mdr_a,
            trx_b: rawRechargeExcel.trx_b,
            tot_mdr_b: rawRechargeExcel.tot_mdr_b,
            status: rawRechargeExcel.status,
            create_at: ControllerConfig.onCurrentTime().toString(),
            state: 0,
            pos_index: parseInt(i),
            code_ad: rawCluster.ad_code
          });
          total_fee = total_fee + ((parseInt(rawRechargeExcel.trx_a) * parseInt(rawRechargeExcel.paid_in_amount)) + (parseInt(rawRechargeExcel.trx_a) * parseInt(rawRechargeExcel.withdraw_amount)));
          total_paid = total_paid + ((parseInt(rawRechargeExcel.trx_b) * parseInt(rawRechargeExcel.paid_in_amount))) + ((parseInt(rawRechargeExcel.trx_b) * parseInt(rawRechargeExcel.withdraw_amount)));


        } catch (error) {
          await this.errorLogRepository.create({
            create_at: ControllerConfig.onCurrentTime().toString(),
            error_log_message: "Error Insert Data",
            error_log_info: JSON.stringify(error),
            error_log_type: "TYPE_DATA_EXCEL",
            error_log_source: syc_id
          });
          errorState = true;
        }
      }

      if (parseInt(i) == (Data.length - 1)) {

        //TEST SKIP
        if (total_fee == total_paid) {
          var process_percent = (no / Data.length) * 100;
          await this.fileSycronizeRepository.updateById(syc_id, {
            process_count: no,
            process_percent: process_percent,
            error_state: errorState,
            code_state: 1,
            state: 1,
            end_at: ControllerConfig.onCurrentTime().toString(),
            total_fee: total_fee,
            total_paid: total_paid
          });
          excelProcessProgres.RECHARGE.CurrentProcess = no;
          excelProcess.RECHARGE = false;
          this.OnRunningDataSummry(syc_id, periode, DatasMigrasi);
        } else {
          await this.errorLogRepository.create({
            create_at: ControllerConfig.onCurrentTime().toString(),
            error_log_message: "Error Nilai Nominal Transaksi AD-A Tidak 80% Dari Taransaksi AD-B",
            error_log_info: "Error Nilai Nominal Transaksi AD-A Tidak 80% Dari Taransaksi AD-B : AD-A = " + (total_fee * 0.8) + " , AD-B = " + total_paid,
            error_log_type: "TRANSAKSI_TIDAK_SESUAI",
            error_log_source: syc_id
          });
          DealerCodeNotExities.push(rawRechargeExcel.dealer_code);
          errorState = true;
        }
      }

      var process_percent = (no / parseInt(Data.length)) * 100;
      await this.fileSycronizeRepository.updateById(syc_id, {
        process_count: no,
        process_percent: process_percent,
        error_state: errorState,
        ad: AD.length,
        mitra_ad: MitraAd.length,
        total_fee: total_fee,
        total_paid: total_paid
      });
      excelProcessProgres.RECHARGE.CurrentProcess = no;
      no++;
    }
    // excelProcessProgres.RECHARGE.MaxProcess = DataExcel.length;
    // excelProcessProgres.RECHARGE.CurrentProcess = 0;
  }

  OnRunningDataPpob = async (syc_id: number, periode: string) => {

    const configData = await this.dataconfigRepository.find({where: {and: [{config_group_id: 19}, {state: 1}]}});
    var configNum: any;
    configNum = {};
    for (var i in configData) {
      var infoConfig = configData[i];
      if (infoConfig.config_code == "0014-01") {
        configNum["mdr_ppob"] = infoConfig.config_value;
      }
    }

    var dataSyc = await this.fileSycronizeRepository.findById(syc_id);
    var DeletePPOB = await this.dataPppobDetailRepository.deleteAll({periode: periode});

    var Data = excelProcessProgres.PPOB.Datas;
    var AD: any; AD = [];
    var MitraAd: any; MitraAd = [];
    var no = 1;
    var errorState = false;
    var total_fee = dataSyc.total_fee == undefined ? 0 : dataSyc.total_fee;
    var total_paid = dataSyc.total_paid == undefined ? 0 : dataSyc.total_paid;

    var DatasMigrasi: any; DatasMigrasi = {};

    var DealerCodeNotExities: any; DealerCodeNotExities = [];

    excelProcessProgres.PPOB.MaxProcess = Data.length;
    for (var i in Data) {
      var rawPpobExcel = Data[i];
      //CREATE DATA PPPOB
      const rawCluster = await this.adClusterRepository.findOne({where: {ad_cluster_id: rawPpobExcel.dealer_code}});
      if (rawCluster == null) {
        if (DealerCodeNotExities.indexOf(rawPpobExcel.dealer_code) == -1) {
          await this.errorLogRepository.create({
            create_at: ControllerConfig.onCurrentTime().toString(),
            error_log_message: "Error Dealer Code Not Exities !",
            error_log_info: "Dealer Code " + rawPpobExcel.dealer_code + " Not Exities.",
            error_log_type: "DEALER_CODE_NOT_EXITIES",
            error_log_source: syc_id
          });
          DealerCodeNotExities.push(rawPpobExcel.dealer_code);
        }
        errorState = true;
      } else {
        try {
          const rawCluster = await this.adClusterRepository.findById(rawPpobExcel.dealer_code);
          if (AD.indexOf(rawCluster.ad_code) == -1) {
            AD.push(rawCluster.ad_code);
          }
          if (MitraAd.indexOf(rawPpobExcel.dealer_code) == -1) {
            MitraAd.push(rawPpobExcel.dealer_code);
          }

          if (DatasMigrasi[rawPpobExcel.dealer_code] == undefined) {
            DatasMigrasi[rawPpobExcel.dealer_code] = {
              dealer_code: rawPpobExcel.dealer_code,
              location: rawPpobExcel.location,
              name: rawCluster.ad_name,
              paid_in_amount: 0,
              withdraw_amount: 0,
              reason_type_id: rawPpobExcel.reason_type_id,
              reason_type_name: rawPpobExcel.reason_type_name,
              trx_a: 0,
              tot_mdr_a: 0,
              trx_b: 0,
              tot_mdr_b: 0,
              status: 0,
              periode: periode,
              create_at: ControllerConfig.onCurrentTime().toString(),
              type_trx: "PPOB",
              code_ad: rawCluster.ad_code
            }
          }

          DatasMigrasi[rawPpobExcel.dealer_code]["paid_in_amount"] = configNum["mdr_ppob"] == undefined ? parseInt(rawPpobExcel.ad) : configNum["mdr_ppob"];
          DatasMigrasi[rawPpobExcel.dealer_code]["trx_a"] += parseInt(rawPpobExcel.count_trx);
          var numMdrPPOB = (configNum["mdr_ppob"] == undefined ? parseInt(rawPpobExcel.ad) : configNum["mdr_ppob"]);
          DatasMigrasi[rawPpobExcel.dealer_code]["tot_mdr_a"] += parseInt(numMdrPPOB);
          DatasMigrasi[rawPpobExcel.dealer_code]["create_at"] = ControllerConfig.onCurrentTime().toString();

          await this.dataPppobDetailRepository.create({
            orderid: rawPpobExcel.orderid,
            count_trx: rawPpobExcel.count_trx,
            ddate: rawPpobExcel.ddate,
            billrefnumber: rawPpobExcel.billrefnumber,
            reason_type_id: rawPpobExcel.reason_type_id,
            reason_type_name: rawPpobExcel.reason_type_name,
            dealer_code: rawPpobExcel.dealer_code,
            location: rawPpobExcel.location,
            entity_name: rawPpobExcel.entity_name,
            trx_amount: parseInt(rawPpobExcel.trx_amount),
            shortcode: rawPpobExcel.shortcode,
            services: rawPpobExcel.services,
            reseller: rawPpobExcel.reseller,
            biller: parseInt(rawPpobExcel.biller),
            finarya: parseInt(rawPpobExcel.finarya),
            tsel: parseInt(rawPpobExcel.tsel),
            ad: (configNum["mdr_ppob"] == undefined ? parseInt(rawPpobExcel.ad) : configNum["mdr_ppob"]),
            net_finarya: parseInt(rawPpobExcel.net_finarya),
            create_at: ControllerConfig.onCurrentTime().toString(),
            state: 0,
            periode: periode,
            pos_index: parseInt(i),
            code_ad: rawCluster.ad_code
          });

          var NumFee = (configNum["mdr_ppob"] == undefined ? parseInt(rawPpobExcel.ad) : configNum["mdr_ppob"]);
          total_fee = total_fee + parseInt(NumFee);
          //total_fee = total_fee + parseInt(rawPpobExcel.tsel);

        } catch (error) {
          await this.errorLogRepository.create({
            create_at: ControllerConfig.onCurrentTime().toString(),
            error_log_message: "Error Insert Data",
            error_log_info: JSON.stringify(error),
            error_log_type: "TYPE_DATA_EXCEL",
            error_log_source: syc_id
          });
          errorState = true;
        }
      }

      if (parseInt(i) == (Data.length - 1)) {
        var process_percent = (no / Data.length) * 100;
        await this.fileSycronizeRepository.updateById(syc_id, {
          process_count: no,
          process_percent: process_percent,
          error_state: errorState,
          code_state: 1,
          state: 1,
          end_at: ControllerConfig.onCurrentTime().toString(),
          total_fee: total_fee,
          total_paid: total_paid
        });
        excelProcessProgres.PPOB.CurrentProcess = no;
        excelProcess.PPOB = false;
        this.OnRunningDataSummry(syc_id, periode, DatasMigrasi);
      }

      var process_percent = (no / parseInt(Data.length)) * 100;
      await this.fileSycronizeRepository.updateById(syc_id, {
        process_count: no,
        process_percent: process_percent,
        error_state: errorState,
        ad: AD.length,
        mitra_ad: MitraAd.length,
        total_fee: total_fee,
        total_paid: total_paid
      });
      excelProcessProgres.PPOB.CurrentProcess = no;
      no++;
    }
    // excelProcessProgres.PPOB.MaxProcess = DataExcel.length;
    // excelProcessProgres.PPOB.CurrentProcess = 0;
  }

  @post('/FileSycUpload', {
    responses: {
      '200': {
        description: 'SummaryTmp model instance',
        content: {'application/json': {schema: getModelSchemaRef(ComponentFileSyc)}},
      },
    },
  })
  async FileSycUpload(
    @requestBody.file()
    requestFile: Request,
    @inject(RestBindings.Http.RESPONSE) response: Response,
  ): Promise<object> {
    return await new Promise(async (resolve, reject) => {
      var todayPeriode = new Date();
      var todaysPeriode = todayPeriode.setMonth(todayPeriode.getMonth() - 1);
      var DateNewPeriode = new Date(todaysPeriode);
      var PeriodeNew = DateNewPeriode.getFullYear() + '-' + ('0' + (DateNewPeriode.getMonth() + 1)).slice(-2);

      var dataDigiposExis = await this.dataDigiposRepository.find({where: {and: [{trx_date_group: PeriodeNew}]}});
      var dataDigipos = await this.dataDigiposRepository.find({where: {and: [{trx_date_group: PeriodeNew}, {ar_stat: 2}]}});
      if (dataDigipos.length == dataDigiposExis.length && dataDigiposExis.length != 0) {
        resolve({success: false, message: "Error migrassi, all data this periode already validate !"});
      } else {

        var deactiveMaster = false;
        var adMaster = await this.adMasterRepository.find();
        for (var i in adMaster) {
          var rowMaster = adMaster[i];
          if (rowMaster.ad_status == false) {
            deactiveMaster = true;
          }
        }

        if (deactiveMaster) {
          resolve({success: false, message: "Error , please active all master ad "});
        } else {
          const storage = multer.diskStorage({
            destination: path.join(__dirname, '../../.filesyc'),
            filename: (req, file, cb) => {
              cb(null, Date.now() + "-" + file.originalname);
            },
          });

          const upload = multer({
            storage: storage,
          });

          upload.single('files')(requestFile, response, async (err: any) => {
            if (err) {

            } else {
              var progress: any; progress = progressStream({length: 0});
              requestFile.pipe(progress);
              progress.headers = requestFile.headers;

              progress.on('length', function nowIKnowMyLength(actualLength: any) {
                console.log('actualLength: %s', actualLength);
                progress.setLength(actualLength);
              });
              // get the upload progress
              progress.on('progress', (obj: any) => {
                console.log('progress: %s', obj.percentage);
              });

              var FileInfo = requestFile.file;
              if (FileInfo.mimetype != "application/vnd.ms-excel") {
                fs.unlink(FileInfo.path, (errDel) => {
                  if (errDel) {
                    resolve({success: false, message: errDel});
                  }
                });
              } else {
                var today = new Date();
                var todays = today.setMonth(today.getMonth() - 1);
                var DateNew = new Date(todays);
                var Periode = DateNew.getFullYear() + '-' + ('0' + (DateNew.getMonth() + 1)).slice(-2);
                var InfoBody = requestFile.body;

                var DataExcel: any; DataExcel = [];
                var type: any; type = [];
                var typeExcel = "";
                var Header: any; Header = {};
                fs.createReadStream(FileInfo.path).pipe(csv()).on('data', (row: any) => {
                  // DataExcel = row;
                  var data: any; data = [];
                  for (var i in row) {
                    data[i.trim().toLowerCase().split(" ").join("_")] = i.trim().toLowerCase() == "type" ? row[i].trim().toUpperCase() : row[i];
                    Header[i.trim().toLowerCase().split(" ").join("_")] = false;
                  }

                  if (data.type != undefined) {
                    if (type.indexOf(row.type.trim().toUpperCase()) == -1) {
                      type.push(row.type.trim().toUpperCase());
                    }
                    typeExcel = row.type.trim().toUpperCase();
                    if (typeExcel == "PPOB") {
                      if (data.location != undefined) {
                        console.log(data.location, data.location.toUpperCase().search("TEST"));
                        if (data.location.toUpperCase().search("TEST") == -1) {
                          DataExcel.push(data);
                        }
                      }
                    }

                    if (typeExcel == "RECHARGE") {
                      if (data.name != undefined) {
                        console.log(data.name, data.name.toUpperCase().search("TEST"));
                        if (data.name.toUpperCase().search("TEST") == -1) {
                          DataExcel.push(data);
                        }
                      }
                    }
                  }
                }).on('end', async () => {
                  // var excelProcessProgres: any; excelProcessProgres = {PPOB: {Datas: [], MaxProcess: 0, CurrentProcess: 0}, RECHARGE: {Datas: [], MaxProcess: 0, CurrentProcess: 0}};
                  if (type.length == 1) {

                    //Running Excel PPPOB
                    if (typeExcel == "PPOB") {
                      var HeaderRequeredPpob = ["orderid", "count_trx", "ddate", "billrefnumber", "reason_type_id", "reason_type_name", "dealer_code", "location", "entity_name", "trx_amount", "shortcode", "services", "reseller", "biller", "finarya", "tsel", "ad", "net_finarya", "type"];
                      var ColumnNotExities = "";
                      var StateColumnNotExities = false;
                      for (var hPPOB in HeaderRequeredPpob) {
                        if (Header[HeaderRequeredPpob[hPPOB]] == undefined) {
                          ColumnNotExities += "," + HeaderRequeredPpob[hPPOB];
                          StateColumnNotExities = true;
                        }
                      }
                      if (StateColumnNotExities == false) {
                        if (excelProcess.PPOB == false) {
                          excelProcess.PPOB = true;
                          excelProcessProgres.PPOB.Datas = DataExcel;
                          excelProcessProgres.PPOB.MaxProcess = DataExcel.length;
                          excelProcessProgres.PPOB.CurrentProcess = 0;
                          //Create Add Info Upload Excel
                          try {
                            var message: any; message = {};
                            await this.fileSycronizeRepository.create({
                              periode: Periode,
                              file_name: FileInfo.filename,
                              process_total: DataExcel.length,
                              process_count: 0,
                              process_percent: 0,
                              error_state: false,
                              error_attach: "",
                              error_message: "",
                              code_state: 0,
                              create_at: ControllerConfig.onCurrentTime().toString(),
                              end_at: ControllerConfig.onCurrentTime().toString(),
                              mitra_ad: 0,
                              total_fee: 0,
                              total_paid: 0,
                              type_excel: "PPOB"
                            }).then((r) => {
                              if (r.file_syc_id != undefined) {
                                this.OnRunningDataPpob(r.file_syc_id, Periode);
                                message = {success: true, message: "Success, upload data csv PPOB success, please wait after porocess status finished !"};
                              } else {
                                message = {success: false, message: r};
                              }
                            });
                            resolve(message);
                          } catch (error) {
                            excelProcess.PPOB = false;
                            excelProcessProgres.PPOB.Datas = [];
                            excelProcessProgres.PPOB.MaxProcess = 0;
                            excelProcessProgres.PPOB.CurrentProcess = 0;
                            resolve({success: false, message: error});
                          }
                        } else {
                          resolve({success: false, message: "Error , system already running data PPOB please wait after running finished !"});
                        }
                      } else {
                        resolve({success: false, message: "Error , please insert column : " + ColumnNotExities.substring(1)});
                      }
                    }

                    //Running Excel Recharge
                    if (typeExcel == "RECHARGE") {
                      var HeaderRequeredRecharge = ["dealer_code", "location", "name", "type", "paid_in_amount", "withdraw_amount", "reason_type_id", "reason_type_name", "trx_a", "tot_mdr_a", "trx_b", "tot_mdr_b"];
                      var ColumnNotExities = "";
                      var StateColumnNotExities = false;
                      for (var hRECHARGE in HeaderRequeredRecharge) {
                        if (Header[HeaderRequeredRecharge[hRECHARGE]] == undefined) {
                          ColumnNotExities += "," + HeaderRequeredRecharge[hRECHARGE];
                          StateColumnNotExities = true;
                        }
                      }

                      if (StateColumnNotExities == false) {
                        if (excelProcess.RECHARGE == false) {
                          excelProcess.RECHARGE = true;
                          excelProcessProgres.RECHARGE.Datas = DataExcel;
                          excelProcessProgres.RECHARGE.MaxProcess = DataExcel.length;
                          excelProcessProgres.RECHARGE.CurrentProcess = 0;
                          //Create Add Info Upload Excel
                          try {
                            var message: any; message = {};
                            await this.fileSycronizeRepository.create({
                              periode: Periode,
                              file_name: FileInfo.filename,
                              process_total: DataExcel.length,
                              process_count: 0,
                              process_percent: 0,
                              error_state: false,
                              error_attach: "",
                              error_message: "",
                              code_state: 0,
                              create_at: ControllerConfig.onCurrentTime().toString(),
                              end_at: ControllerConfig.onCurrentTime().toString(),
                              mitra_ad: 0,
                              total_fee: 0,
                              total_paid: 0,
                              type_excel: "RECHARGE"
                            }).then((r) => {
                              if (r.file_syc_id != undefined) {
                                this.OnRunningDataRecharge(r.file_syc_id, Periode);
                                message = {success: true, message: "Success, upload data csv RECHARGE success, please wait after porocess status finished !"};
                              } else {
                                message = {success: false, message: r};
                              }
                            });
                            resolve(message);
                          } catch (error) {
                            excelProcess.RECHARGE = false;
                            excelProcessProgres.RECHARGE.Datas = [];
                            excelProcessProgres.RECHARGE.MaxProcess = 0;
                            excelProcessProgres.RECHARGE.CurrentProcess = 0;
                            resolve({success: false, message: error});
                          }
                        } else {
                          resolve({success: false, message: "Error , system already running data RECHARGE please wait after running finished !"});
                        }
                      } else {
                        resolve({success: false, message: "Error , please insert column : " + ColumnNotExities.substring(1)});
                      }
                    }

                    if (typeExcel != "PPOB" && typeExcel != "RECHARGE") {
                      resolve({success: false, message: "Error format excel, system can't found column type RECHARE / PPOB !"});
                    }

                  } else {
                    if (type.length == 0) {
                      resolve({success: false, message: "Error format excel, system can't found column type !"});
                    } else {
                      resolve({success: false, message: "Error format excel, system can't read multiple column type : " + JSON.stringify(type)});
                    }
                  }
                });
                fs.unlink(FileInfo.path, (errDel) => {
                  if (errDel) {
                    resolve({success: false, message: errDel});
                  }
                });
              }
            }
          });
        }
      }
    });



    // var data: any;
    // data = {success: false, message: "no return"};
    // await upload.single('files')(requestFile, response, async (err: any) => {

    //   if (err) {

    //   } else {

    //   }
    // var FileInfo = requestFile.file;
    // // var d = new Date();
    // // var date = d.getDate();
    // // var month = d.getMonth();
    // // var year = d.getFullYear();
    // // var strMount = month.toString();
    // // var strZero = "00";
    // // var Periode = year + "-" + strZero.substring(strMount.length) + strMount;

    // var today = new Date();
    // var todays = today.setMonth(today.getMonth() - 1);
    // var DateNew = new Date(todays);
    // var Periode = DateNew.getFullYear() + '-' + ('0' + (DateNew.getMonth() + 1)).slice(-2);


    // if (FileInfo.mimetype != "application/vnd.ms-excel") {

    //   data["error"] = {
    //     message: "Error, type file upload for sycronize must csv file !! ",
    //     code: 201
    //   }

    //   fs.unlink(FileInfo.path, function (err) {
    //     if (err) {
    //       data["error"]["message"] = data["error"]["message"] + " & " + err;
    //     }
    //   });
    //   return data;
    // } else {

    //   var InfoBody = requestFile.body;
    //   if (InfoBody.periode != undefined) {
    //     Periode = InfoBody.periode;
    //   }

    //   var found = await this.fileSycronizeRepository.findOne({where: {and: [{periode: Periode}, {code_state: 0}]}});
    //   if (found?.file_syc_id == undefined) {
    //     var Total = 0;
    //     fs.createReadStream(FileInfo.path).pipe(csv()).on('data', (row: any) => {
    //       Total++;
    //     }).on('end', async () => {

    //       var today = new Date();
    //       var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    //       var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    //       var dateTime = date + ' ' + time;

    //       await this.fileSycronizeRepository.create({
    //         periode: Periode,
    //         file_name: FileInfo.filename,
    //         process_total: Total,
    //         process_count: 0,
    //         process_percent: 0,
    //         error_state: false,
    //         error_attach: "",
    //         error_message: "",
    //         code_state: 0,
    //         create_at: dateTime
    //       });

    //       delete SycProcess[Periode];
    //       this.SycFileExcel();

    //     });

    //     data["code"] = 200;
    //     data["message"] = "success upload file";
    //     return data;
    //   } else {
    //     data["error"]["message"] = "File periode already process, please wait finising process, before sycronize !!";
    //     return data;
    //   }
    // }
    // });
    // return data;
  }

  @post('/FileSycAction/progres-migrasi', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {
          'application/json': {
            schema: ProgresMigrateRequestBody
          }
        },
      },
    },
  })
  async updateNoFaktur(
    @requestBody(ProgresMigrateRequestBody) progresMigrate: {periode: string},
  ): Promise<{}> {
    var dataDigipos = await this.dataDigiposDetailRepository.count({and: [{trx_date_group: progresMigrate.periode}, {type_trx: "PPOB"}]});
    var result: any; result = {success: true, PPOB: false, RECHARGE: false, datas: {}};
    for (var i in excelProcess) {
      result[i] = excelProcess[i];
    }
    if (result.PPOB == true) {
      result.datas["PPOB"] = {};
      result.datas["PPOB"]["MaxProcess"] = excelProcessProgres.PPOB.MaxProcess;
      result.datas["PPOB"]["CurrentProcess"] = excelProcessProgres.PPOB.CurrentProcess;
    }
    if (result.RECHARGE == true) {
      result.datas["RECHARGE"] = {};
      result.datas["RECHARGE"]["MaxProcess"] = excelProcessProgres.RECHARGE.MaxProcess;
      result.datas["RECHARGE"]["CurrentProcess"] = excelProcessProgres.RECHARGE.CurrentProcess;
    }
    return result;
  }




  @get('/FileSycAction', {
    responses: {
      '200': {
        description: 'Array of File Sycronize model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(FileSycronize),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(FileSycronize) filter?: Filter<FileSycronize>,
  ): Promise<Object> {
    var info = {"datas": new Array(), "size": 0};
    var filterOrder = {};
    if (filter?.where !== undefined) {
      filterOrder = {"where": filter?.where};
    }
    var infoFileSyc = await this.fileSycronizeRepository.find(filter);
    var sizeFileSyc = await this.fileSycronizeRepository.find(filterOrder);
    info["datas"] = infoFileSyc;
    info["size"] = sizeFileSyc.length;
    return info;
  }

  @del('/FileSycAction/{id}', {
    responses: {
      '204': {
        description: 'File Sycronize DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.fileSycronizeRepository.deleteById(id);
  }

  private static getFilesAndFields(request: Request) {
    return request;
  }

  @get('/FileSycAction/Error', {
    responses: {
      '200': {
        description: 'Array of File Sycronize Error model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(ErrorLog),
            },
          },
        },
      },
    },
  })
  async findErrorLog(
    @param.filter(ErrorLog) filter?: Filter<ErrorLog>,
  ): Promise<ErrorLog[]> {
    return this.errorLogRepository.find(filter);
  }

}
