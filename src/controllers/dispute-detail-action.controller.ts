import {authenticate} from '@loopback/authentication';
import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where
} from '@loopback/repository';
import {
  del, get,
  getModelSchemaRef, param,


  patch, post,




  put,

  requestBody
} from '@loopback/rest';
import {DisputeDetail} from '../models';
import {DisputeDetailRepository} from '../repositories';


@authenticate('jwt')
export class DisputeDetailActionController {
  constructor(
    @repository(DisputeDetailRepository)
    public disputeDetailRepository: DisputeDetailRepository,
  ) { }

  @post('/dispute-details', {
    responses: {
      '200': {
        description: 'DisputeDetail model instance',
        content: {'application/json': {schema: getModelSchemaRef(DisputeDetail)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(DisputeDetail, {
            title: 'NewDisputeDetail',
            exclude: ['dispute_detail_id'],
          }),
        },
      },
    })
    disputeDetail: Omit<DisputeDetail, 'dispute_detail_id'>,
  ): Promise<DisputeDetail> {
    return this.disputeDetailRepository.create(disputeDetail);
  }

  @get('/dispute-details/count', {
    responses: {
      '200': {
        description: 'DisputeDetail model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(DisputeDetail) where?: Where<DisputeDetail>,
  ): Promise<Count> {
    return this.disputeDetailRepository.count(where);
  }

  @get('/dispute-details', {
    responses: {
      '200': {
        description: 'Array of DisputeDetail model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(DisputeDetail, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(DisputeDetail) filter?: Filter<DisputeDetail>,
  ): Promise<DisputeDetail[]> {
    return this.disputeDetailRepository.find(filter);
  }

  @patch('/dispute-details', {
    responses: {
      '200': {
        description: 'DisputeDetail PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(DisputeDetail, {partial: true}),
        },
      },
    })
    disputeDetail: DisputeDetail,
    @param.where(DisputeDetail) where?: Where<DisputeDetail>,
  ): Promise<Count> {
    return this.disputeDetailRepository.updateAll(disputeDetail, where);
  }

  @get('/dispute-details/{id}', {
    responses: {
      '200': {
        description: 'DisputeDetail model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(DisputeDetail, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(DisputeDetail, {exclude: 'where'}) filter?: FilterExcludingWhere<DisputeDetail>
  ): Promise<DisputeDetail> {
    return this.disputeDetailRepository.findById(id, filter);
  }

  @patch('/dispute-details/{id}', {
    responses: {
      '204': {
        description: 'DisputeDetail PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(DisputeDetail, {partial: true}),
        },
      },
    })
    disputeDetail: DisputeDetail,
  ): Promise<void> {
    await this.disputeDetailRepository.updateById(id, disputeDetail);
  }

  @put('/dispute-details/{id}', {
    responses: {
      '204': {
        description: 'DisputeDetail PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() disputeDetail: DisputeDetail,
  ): Promise<void> {
    await this.disputeDetailRepository.replaceById(id, disputeDetail);
  }

  @del('/dispute-details/{id}', {
    responses: {
      '204': {
        description: 'DisputeDetail DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.disputeDetailRepository.deleteById(id);
  }
}
