// Uncomment these imports to begin using these cool features!

// import {inject} from '@loopback/core';
import {authenticate} from '@loopback/authentication';
import {inject} from '@loopback/core';
import {Filter, FilterExcludingWhere, repository} from '@loopback/repository';
import {get, getModelSchemaRef, param, post, requestBody, ResponseObject} from '@loopback/rest';
import {SecurityBindings, securityId, UserProfile} from '@loopback/security';
import {MerchantProductGroup, MpvType, MpvTypeKey} from '../models';
import {MerchantProductGroupRepository, MerchantProductSummaryRepository, MerchantProductVariableRepository, MpvTypeKeyRepository, MpvTypeRepository} from '../repositories';
import {MerchantProductRepository} from '../repositories/merchant-product.repository';
import {MerchantRepository} from '../repositories/merchant.repository';
import {UserRepository} from '../repositories/user.repository';
import {ControllerConfig} from './controller.config';

export const RunQueryRequestBody: ResponseObject = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {
      schema: {
        type: 'object',
        required: ["MPG_QUERY", "MPG_ID"],
        properties: {
          MPG_ID: {
            type: 'number',
          },
          MPG_QUERY: {
            type: 'string',
          },
        }
      }
    },
  },
};


@authenticate('jwt')
export class MerchantProductGroupActionController {
  constructor(
    @repository(MerchantRepository)
    public merchantRepository: MerchantRepository,
    @repository(MerchantProductRepository)
    public merchantProductRepository: MerchantProductRepository,
    @repository(MerchantProductGroupRepository)
    public merchantProductGroupRepository: MerchantProductGroupRepository,
    @repository(MerchantProductVariableRepository)
    public merchantProductVariableRepository: MerchantProductVariableRepository,
    @repository(MpvTypeKeyRepository)
    public mpvTypeKeyRepository: MpvTypeKeyRepository,
    @repository(MpvTypeRepository)
    public mpvTypeRepository: MpvTypeRepository,
    @repository(MerchantProductSummaryRepository)
    public merchantProductSummaryRepository: MerchantProductSummaryRepository,
    @inject(SecurityBindings.USER)
    public currentUserProfile: UserProfile,
    @repository(UserRepository)
    public userRepository: UserRepository,
  ) { }

  //GET ALL MERCHANT PRODUCT DATA
  @get('/merchant-product-group-action/views', {
    responses: {
      '200': {
        description: 'Array of Merchant Product model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(MerchantProductGroup, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async findMerchantProductGroup(
    @param.filter(MerchantProductGroup) filter?: Filter<MerchantProductGroup>,
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};

    var MerchantProductGroupStore: any; MerchantProductGroupStore = [];
    var MerchantProductGroupInfo = await this.merchantProductGroupRepository.find(filter);
    for (var i in MerchantProductGroupInfo) {
      var infoMerchantProductGroup: any; infoMerchantProductGroup = MerchantProductGroupInfo[i];
      var RAW: any; RAW = {};
      var FIELDS = ["MPG_ID", "MPG_LABEL", "MPG_MDR_TYPE", "MPG_MDR_AMOUNT", "MPG_DPP", "MPG_PPN", "MPG_PPH", "M_ID", "MP_ID", "AT_FLAG"];
      for (var key in infoMerchantProductGroup) {
        if (FIELDS.indexOf(key) == -1) {
          continue;
        }
        RAW[key] = infoMerchantProductGroup[key];
      }

      RAW["MERCHANT"] = await this.merchantRepository.findById(infoMerchantProductGroup.M_ID, {
        fields: {
          M_ID: true,
          M_CODE: true,
          M_NAME: true,
          M_LEGAL_NAME: true,
        }
      });

      RAW["MERCHANT_PRODUCT"] = await this.merchantProductRepository.findById(infoMerchantProductGroup.MP_ID, {
        fields: {
          MP_ID: true,
          MP_CODE: true,
          MP_BRAND: true,
        }
      });

      MerchantProductGroupStore.push(RAW);
    }
    result.datas = MerchantProductGroupStore;
    return result;
  }

  //GET ALL MERCHANT PRODUCT DATA
  @get('/merchant-product-group-action/view/{id}', {
    responses: {
      '200': {
        description: 'Array of Merchant Product model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(MerchantProductGroup, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async findMerchantProductGroupById(
    @param.path.number('id') id: number,
    @param.filter(MerchantProductGroup, {exclude: 'where'}) filter?: FilterExcludingWhere<MerchantProductGroup>
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};

    var MPG_COUNT = await this.merchantProductGroupRepository.count({MPG_ID: id});

    if (MPG_COUNT.count > 0) {
      var infoMerchantProductGroup: any; infoMerchantProductGroup = await this.merchantProductGroupRepository.findById(id);
      var RAW: any; RAW = {};
      var FIELDS = ["MPG_ID", "MPG_LABEL", "MPG_MDR_TYPE", "MPG_MDR_AMOUNT", "MPG_DPP", "MPG_PPN", "MPG_PPH", "M_ID", "MP_ID", "AT_FLAG"];
      for (var key in infoMerchantProductGroup) {
        if (FIELDS.indexOf(key) == -1) {
          continue;
        }
        RAW[key] = infoMerchantProductGroup[key];
      }

      RAW["MERCHANT"] = await this.merchantRepository.findById(infoMerchantProductGroup.M_ID, {
        fields: {
          M_ID: true,
          M_CODE: true,
          M_NAME: true,
          M_LEGAL_NAME: true,
        }
      });

      RAW["MERCHANT_PRODUCT"] = await this.merchantProductRepository.findById(infoMerchantProductGroup.MP_ID, {
        fields: {
          MP_ID: true,
          MP_CODE: true,
          MP_BRAND: true,
        }
      });

      result.success = true;
      result.datas = RAW;
      result.message = "Success !";
    } else {
      result.success = false;
      result.message = "Error, Merchant product group not found !";
      result.datas = [];
    }
    return result;
  }

  @post('/merchant-product-group-action/create', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {'application/json': {schema: getModelSchemaRef(MerchantProductGroup)}},
      },
    },
  })
  async createMerchantProductGroup(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(MerchantProductGroup, {
            title: 'MERCHANT_GROUP_CREATE',
            exclude: ['MPG_ID', 'AT_CREATE', 'AT_UPDATE', 'AT_WHO'],
          }),
        },
      },
    })
    dataMerchantProductGroup: any,
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};

    var REQUIRED_FIELDS = ['MPG_LABEL', 'MPG_MDR_TYPE', 'MPG_MDR_AMOUNT', 'MPG_DPP', 'MPG_PPN', 'MPG_PPH', 'M_ID', 'MP_ID', 'AT_FLAG'];

    var BODY_PARAM: any; BODY_PARAM = {};
    for (var i in dataMerchantProductGroup) {
      if (REQUIRED_FIELDS.indexOf(i) != -1) {
        REQUIRED_FIELDS.splice(REQUIRED_FIELDS.indexOf(i), 1);
      }
      BODY_PARAM[i] = dataMerchantProductGroup[i];
    }

    if (REQUIRED_FIELDS.length == 0) {
      var MERCHANT = await this.merchantRepository.count({M_ID: BODY_PARAM["M_ID"]});
      var MERCHANT_PRODUCT = await this.merchantProductRepository.count({and: [{MP_ID: BODY_PARAM["MP_ID"]}, {M_ID: BODY_PARAM["M_ID"]}]});
      if (MERCHANT.count > 0) {
        if (MERCHANT_PRODUCT.count > 0) {

          BODY_PARAM["AT_CREATE"] = ControllerConfig.onCurrentTime().toString();
          BODY_PARAM["AT_UPDATE"] = undefined;
          BODY_PARAM["AT_WHO"] = USER_ID;

          try {
            var PRODUCT_GROUP = await this.merchantProductGroupRepository.create(BODY_PARAM);
            result.success = true;
            result.message = "Success create merchant product group !";
            result.datas = PRODUCT_GROUP;
          } catch (error) {
            result.success = false;
            result.message = error;
          }

        } else {
          result.success = false;
          result.message = "Error, merchant product " + BODY_PARAM["MP_ID"] + " not found !";
        }
      } else {
        result.success = false;
        result.message = "Error, merchant " + BODY_PARAM["M_ID"] + " not found !";
      }

    } else {
      result.success = false;
      result.message = "Error, Field " + JSON.stringify(REQUIRED_FIELDS) + " must entry !";
    }

    return result;
  }


  @post('/merchant-product-group-action/update/{id}', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {'application/json': {schema: getModelSchemaRef(MerchantProductGroup)}},
      },
    },
  })
  async updateMerchantProductGroup(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(MerchantProductGroup, {
            title: 'MERCHANT_GROUP_UPDATE',
            exclude: ['MPG_ID', 'AT_CREATE', 'AT_UPDATE', 'AT_WHO'],
          }),
        },
      },
    })
    dataMerchantProductGroup: any,
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};

    var BODY_PARAM: any; BODY_PARAM = {};
    var DELETE_ARR = ["MPG_ID", "AT_CREATE", "AT_UPDATE", "AT_WHO"];
    for (var i in dataMerchantProductGroup) {
      if (DELETE_ARR.indexOf(i) != -1) {
        continue;
      }
      BODY_PARAM[i] = dataMerchantProductGroup[i];
    }

    var MERCHANT_GROUP = await this.merchantProductGroupRepository.count({MPG_ID: id});
    if (MERCHANT_GROUP.count > 0) {
      var MERCHANT = await this.merchantRepository.count({M_ID: BODY_PARAM["M_ID"]});
      var MERCHANT_PRODUCT = await this.merchantProductRepository.count({MP_ID: BODY_PARAM["MP_ID"]});
      if (MERCHANT.count > 0) {
        if (MERCHANT_PRODUCT.count > 0) {

          BODY_PARAM["AT_UPDATE"] = ControllerConfig.onCurrentTime().toString();
          BODY_PARAM["AT_WHO"] = USER_ID;

          try {
            var PRODUCT_GROUP = await this.merchantProductGroupRepository.updateById(id, BODY_PARAM);
            result.success = true;
            result.message = "Success update merchant product group !";
          } catch (error) {
            result.succes = false;
            result.message = error;
          }
        } else {
          result.success = false;
          result.message = "Error, merchant product " + BODY_PARAM["MP_ID"] + " not found !";
        }
      } else {
        result.success = false;
        result.message = "Error, merchant " + BODY_PARAM["M_ID"] + " not found !";
      }

    } else {
      result.success = false;
      result.message = "Error, merchant product group not found !";
    }

    return result;
  }


  @post('/merchant-product-group-action/delete/{id}', {
    responses: {
      '200': {
        description: 'DataDigipos model instance',
        content: {'application/json': {schema: getModelSchemaRef(MerchantProductGroup)}},
      },
    },
  })
  async deleteMerchantProductGroup(
    @param.path.number('id') id: number
  ): Promise<Object> {
    var USER_ID = this.currentUserProfile[securityId];
    var result: any; result = {success: true, message: '', datas: []};

    var PRODUCT_VARIABLE = await this.merchantProductSummaryRepository.count({MPG_ID: id});
    if (PRODUCT_VARIABLE.count == 0) {
      try {
        await this.merchantProductGroupRepository.deleteById(id);
        result.success = true;
        result.message = "Success delete merchant product group !";
      } catch (error) {
        result.success = false;
        result.message = error;
      }
    } else {
      result.success = false;
      result.message = "Error, merchant product group have transaksi !";
    }

    return result;
  }


  // @post('/merchant-product-group-action/run-query', {
  //   responses: {
  //     '200': {
  //       description: 'DataDigipos model instance',
  //       content: {
  //         'application/json': {
  //           schema: RunQueryRequestBody
  //         }
  //       },
  //     },
  //   },
  // })
  // async merchantProductGroupRunQuery(
  //   @requestBody(RunQueryRequestBody) runQueryRequestBody: {MPG_ID: number, MPG_QUERY: string},
  // ): Promise<Object> {
  //   var USER_ID = this.currentUserProfile[securityId];
  //   var result: any; result = {success: true, message: '', datas: []};
  //   var MERCHANT_GROUP_COUNT = await this.merchantProductGroupRepository.count({MPG_ID: runQueryRequestBody.MPG_ID});
  //   if (MERCHANT_GROUP_COUNT.count > 0) {

  //     var QUERY_DENIED = false;
  //     var QUERY_DENIED_LIST = ['DROP', 'DELETE', 'UPDATE', 'TRUNCATE', 'INSERT'];
  //     var QUERY_REQ = runQueryRequestBody.MPG_QUERY.toUpperCase();
  //     for (var i_SQL in QUERY_DENIED_LIST) {
  //       if (QUERY_REQ.search(QUERY_DENIED_LIST[i_SQL]) != -1) {
  //         QUERY_DENIED = true;
  //       }
  //     }

  //     if (QUERY_DENIED == false) {
  //       try {

  //         var QUERY = runQueryRequestBody.MPG_QUERY.split("$FILTER_PERIODE").join(" ").split("$AND").join(" ").split("$OR").join(" ").split("$WHERE").join(" ");

  //         const BIG_QUERY = new BigQuery();
  //         var SQL_QUERY = QUERY + " " + " LIMIT 1";
  //         const OPTION_BIG_QUERY = {
  //           query: SQL_QUERY
  //         }
  //         const [RESULT_BIG_QUERY] = await BIG_QUERY.query(OPTION_BIG_QUERY);
  //         var FIELD_AVALIABLE: any; FIELD_AVALIABLE = [];
  //         for (var I_KEY in RESULT_BIG_QUERY) {
  //           var RAW_QUERY = RESULT_BIG_QUERY[I_KEY];
  //           for (var FIELD_KEY in RAW_QUERY) {
  //             if (FIELD_AVALIABLE.indexOf(FIELD_KEY) == -1) {
  //               FIELD_AVALIABLE.push(FIELD_KEY);
  //             }
  //           }
  //         }

  //         await this.merchantProductGroupRepository.updateById(runQueryRequestBody.MPG_ID, {
  //           MPG_QUERY: runQueryRequestBody.MPG_QUERY
  //         });

  //         result.success = true;
  //         result.message = "Success run query !";
  //         result.datas = FIELD_AVALIABLE;

  //       } catch (error) {
  //         result.success = false;
  //         result.message = error;
  //       }
  //     } else {
  //       result.success = false;
  //       result.message = "Error, Query SELECT Only !";
  //     }
  //   } else {
  //     result.success = false;
  //     result.message = "Error, merchant product group not found !";
  //   }
  //   return result;
  // }


  @get('/merchant-product-group-action/mvp-type-key/views', {
    responses: {
      '200': {
        description: 'Merchant Document Views type model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(MpvTypeKey, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findMpvTypeKeyViews(
    @param.filter(MpvTypeKey) filter?: Filter<MpvTypeKey>,
  ): Promise<Object> {
    var result: any; result = {success: true, message: '', datas: []};
    var USER_ID = this.currentUserProfile[securityId];
    var MVP_TYPE_KEY = await this.mpvTypeKeyRepository.find(filter);
    var DATAS: any; DATAS = [];
    for (var i in MVP_TYPE_KEY) {
      var RAW_KEY = MVP_TYPE_KEY[i];
      DATAS.push({
        ID: RAW_KEY.MPV_TYPE_KEY_ID,
        LABEL: RAW_KEY.MPV_TYPE_KEY_LABEL,
        AT_FLAG: RAW_KEY.AT_FLAG
      });
    }
    result.success = true;
    result.message = "Success !";
    result.datas = DATAS;
    return result;
  }

  @get('/merchant-product-group-action/mvp-type/views', {
    responses: {
      '200': {
        description: 'Merchant Document Views type model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(MpvType, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findMpvTypeViews(
    @param.filter(MpvType) filter?: Filter<MpvType>,
  ): Promise<Object> {
    var result: any; result = {success: true, message: '', datas: []};
    var USER_ID = this.currentUserProfile[securityId];
    var MVP_TYPE = await this.mpvTypeRepository.find(filter);
    var DATAS: any; DATAS = [];
    for (var i in MVP_TYPE) {
      var RAW = MVP_TYPE[i];
      DATAS.push({
        ID: RAW.MPV_TYPE_ID,
        LABEL: RAW.MPV_TYPE_LABEL,
        AT_FLAG: RAW.AT_FLAG
      });
    }
    result.success = true;
    result.message = "Success !";
    result.datas = DATAS;
    return result;
  }


}
