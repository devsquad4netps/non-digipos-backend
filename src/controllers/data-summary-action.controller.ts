import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {DataSumarry} from '../models';
import {DataSumarryRepository} from '../repositories';

export class DataSummaryActionController {
  constructor(
    @repository(DataSumarryRepository)
    public dataSumarryRepository : DataSumarryRepository,
  ) {}

  @post('/data-sumarries', {
    responses: {
      '200': {
        description: 'DataSumarry model instance',
        content: {'application/json': {schema: getModelSchemaRef(DataSumarry)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(DataSumarry, {
            title: 'NewDataSumarry',
            exclude: ['id'],
          }),
        },
      },
    })
    dataSumarry: Omit<DataSumarry, 'id'>,
  ): Promise<DataSumarry> {
    return this.dataSumarryRepository.create(dataSumarry);
  }

  @get('/data-sumarries/count', {
    responses: {
      '200': {
        description: 'DataSumarry model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(DataSumarry) where?: Where<DataSumarry>,
  ): Promise<Count> {
    return this.dataSumarryRepository.count(where);
  }

  @get('/data-sumarries', {
    responses: {
      '200': {
        description: 'Array of DataSumarry model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(DataSumarry, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(DataSumarry) filter?: Filter<DataSumarry>,
  ): Promise<DataSumarry[]> {
    return this.dataSumarryRepository.find(filter);
  }

  @patch('/data-sumarries', {
    responses: {
      '200': {
        description: 'DataSumarry PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(DataSumarry, {partial: true}),
        },
      },
    })
    dataSumarry: DataSumarry,
    @param.where(DataSumarry) where?: Where<DataSumarry>,
  ): Promise<Count> {
    return this.dataSumarryRepository.updateAll(dataSumarry, where);
  }

  @get('/data-sumarries/{id}', {
    responses: {
      '200': {
        description: 'DataSumarry model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(DataSumarry, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(DataSumarry, {exclude: 'where'}) filter?: FilterExcludingWhere<DataSumarry>
  ): Promise<DataSumarry> {
    return this.dataSumarryRepository.findById(id, filter);
  }

  @patch('/data-sumarries/{id}', {
    responses: {
      '204': {
        description: 'DataSumarry PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(DataSumarry, {partial: true}),
        },
      },
    })
    dataSumarry: DataSumarry,
  ): Promise<void> {
    await this.dataSumarryRepository.updateById(id, dataSumarry);
  }

  @put('/data-sumarries/{id}', {
    responses: {
      '204': {
        description: 'DataSumarry PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() dataSumarry: DataSumarry,
  ): Promise<void> {
    await this.dataSumarryRepository.replaceById(id, dataSumarry);
  }

  @del('/data-sumarries/{id}', {
    responses: {
      '204': {
        description: 'DataSumarry DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.dataSumarryRepository.deleteById(id);
  }
}
